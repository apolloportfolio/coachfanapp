package com.aps.coachfanapp.core.business.domain.model.factories


import com.aps.coachfanapp.common.util.DateUtil
import com.aps.coachfanapp.common.util.UserUniqueID
import com.aps.coachfanapp.core.business.domain.model.entities.ProjectUser
import java.util.*
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class UserFactory
@Inject
constructor(
    private val dateUtil: DateUtil
) {
    fun createSingleEntity(
        id: UserUniqueID?,
        emailAddress: String,
        password: String,
        profilePhotoImageURI: String?,
        name: String?,
        surname: String?,
        description : String?,
        city: String?,
        emailAddressVerified: Boolean,
        phoneNumberVerified: Boolean,
    ): ProjectUser {
        return ProjectUser(
            id,
            dateUtil.getCurrentTimestamp(),
            dateUtil.getCurrentTimestamp(),
            emailAddress,
            password,
            profilePhotoImageURI,
            name,
            surname,
            description,
            city,
            emailAddressVerified,
            phoneNumberVerified,
        )
    }

    fun createEntitiesList(numEntities: Int): List<ProjectUser> {
        val list: ArrayList<ProjectUser> = ArrayList()
        for(i in 0 until numEntities){ // exclusive on upper bound
            list.add(
                createSingleEntity(
                    UserUniqueID.generateFakeRandomID(),
                    "",
                    "",
                    null,
                    "",
                    "",
                    null,
                    "",
                    false,
                    false,
                )
            )
        }
        return list
    }


    fun generateEmpty(): ProjectUser {
        return createSingleEntity(
            UserUniqueID.generateFakeRandomID(),
            "",
            "",
            null,
            "",
            "",
            null,
            "",
            false,
            false,
        )
    }
    fun generateRandomEntity(): ProjectUser {
        return createSingleEntity(
            UserUniqueID.generateFakeRandomID(),
            UUID.randomUUID().toString(),
            UUID.randomUUID().toString(),
            UUID.randomUUID().toString(),
            UUID.randomUUID().toString(),
            UUID.randomUUID().toString(),
            UUID.randomUUID().toString(),
            UUID.randomUUID().toString(),
            false,
            false,
        )
    }
}