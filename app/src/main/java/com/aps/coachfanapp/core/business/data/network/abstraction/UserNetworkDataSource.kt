package com.aps.coachfanapp.core.business.data.network.abstraction

import android.net.Uri
import com.aps.coachfanapp.common.business.data.network.StandardNetworkDataSource
import com.aps.coachfanapp.common.util.UniqueID
import com.aps.coachfanapp.common.util.UserUniqueID
import com.aps.coachfanapp.core.business.domain.model.entities.ProjectUser
import com.aps.coachfanapp.core.business.domain.model.entities.ProjectUsersRating
import com.google.firebase.auth.FirebaseUser

interface UserNetworkDataSource: StandardNetworkDataSource<ProjectUser> {
    suspend fun registerNewUser(entity: ProjectUser): FirebaseUser?
    
    suspend fun updateUser(entity: ProjectUser): ProjectUser?
    
    suspend fun loginUser(entity: ProjectUser): FirebaseUser?

    suspend fun logoutUser(entity: ProjectUser)
    
    override suspend fun insertOrUpdateEntity(entity: ProjectUser): ProjectUser?

    suspend fun getEntityByCredentials(email: String?, password: String?): ProjectUser?

    override suspend fun deleteEntity(primaryKey: UniqueID?)

    override suspend fun insertDeletedEntity(entity: ProjectUser)

    override suspend fun insertDeletedEntities(Entities: List<ProjectUser>)

    override suspend fun deleteDeletedEntity(entity: ProjectUser)

    override suspend fun getDeletedEntities(): List<ProjectUser>

    override suspend fun deleteAllEntities()

    override suspend fun searchEntity(entity: ProjectUser): ProjectUser?

    override suspend fun getAllEntities(): List<ProjectUser>

    override suspend fun insertOrUpdateEntities(Entities: List<ProjectUser>): List<ProjectUser>?

    suspend fun checkIfEmailExistsInRepository(email: String?): Boolean

    suspend fun getIdOfUserWithTheseCredentials(email: String?, password: String?): UserUniqueID?

    override suspend fun getEntityById(id: UniqueID): ProjectUser?

    suspend fun deleteAccount(projectUser: ProjectUser)

    suspend fun uploadUsersProfilePhotoToFirestore(
        projectUser : ProjectUser,
        usersProfilePhotoUri : Uri,
    ) : String?

    suspend fun uploadUsersProfilePhotoToFirestore(
        userID : UserUniqueID,
        usersProfilePhotoUri : Uri,
    ) : String?

    suspend fun getUsersRating(userID: UserUniqueID): ProjectUsersRating?

    suspend fun insertUsersRating(usersUsersRating: ProjectUsersRating)
}