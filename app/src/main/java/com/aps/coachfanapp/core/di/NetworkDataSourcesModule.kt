package com.aps.coachfanapp.core.di

import com.aps.coachfanapp.core.business.data.network.abstraction.*
import com.aps.coachfanapp.core.business.data.network.implementation.*
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
abstract class NetworkDataSourcesModule {

    @Binds
    abstract fun bindUserNetworkDataSource(implementation: UserNetworkDataSourceImpl): UserNetworkDataSource

//    @Binds
//    abstract fun bindEntity1NetworkDataSource(implementation: Entity1NetworkDataSourceImpl): Entity1NetworkDataSource
//
//    @Binds
//    abstract fun bindEntity2NetworkDataSource(implementation: Entity2NetworkDataSourceImpl): Entity2NetworkDataSource
//
//    @Binds
//    abstract fun bindEntity3NetworkDataSource(implementation: Entity3NetworkDataSourceImpl): Entity3NetworkDataSource
//
//    @Binds
//    abstract fun bindEntity4NetworkDataSource(implementation: Entity4NetworkDataSourceImpl): Entity4NetworkDataSource

}