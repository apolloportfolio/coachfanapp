package com.aps.coachfanapp.core.framework.presentation

import android.os.Bundle
import com.aps.coachfanapp.common.framework.presentation.BaseComposableActivity

abstract class ProjectComposableActivity : BaseComposableActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onStart() {
        super.onStart()

    }

    override fun onResume() {
        super.onResume()

    }

    override fun onPause() {
        super.onPause()

    }

    override fun onStop() {
        super.onStop()

    }

    override fun onRestart() {
        super.onRestart()

    }

    override fun onDestroy() {
        super.onDestroy()

    }
}