package com.aps.coachfanapp.core.framework.presentation

import android.Manifest
import com.aps.coachfanapp.R
import com.aps.coachfanapp.common.framework.presentation.PermissionMediaProvider
import com.aps.coachfanapp.common.framework.presentation.TAG_PERMISSION_ASKING
import com.aps.coachfanapp.common.util.ALog
import java.security.InvalidParameterException

private const val TAG = "ProjectPermissionMediaProvider"
private const val LOG_ME = true

sealed class ProjectPermissionMediaProvider: PermissionMediaProvider {
    companion object {
        fun choosePermissionMediaProvider(permission: String): PermissionMediaProvider {
            return when (permission) {
                Manifest.permission.ACCESS_COARSE_LOCATION -> {
                    CoarseLocationPermissionMediaProvider
                }
                Manifest.permission.ACCESS_FINE_LOCATION -> {
                    FineLocationPermissionMediaProvider
                }
                Manifest.permission.CAMERA -> {
                    CameraPermissionMediaProvider
                }
                Manifest.permission.RECORD_AUDIO -> {
                    RecordAudioPermissionMediaProvider
                }
                Manifest.permission.CALL_PHONE -> {
                    PhoneCallPermissionMediaProvider
                }
                Manifest.permission.ACCESS_BACKGROUND_LOCATION -> {
                    AccessBackgroundLocationPermissionMediaProvider
                }
                Manifest.permission.ACCESS_NETWORK_STATE -> {
                    AccessNetworkStatePermissionMediaProvider
                }
                Manifest.permission.ACCESS_WIFI_STATE -> {
                    AccessWiFiStatePermissionMediaProvider
                }
                Manifest.permission.BLUETOOTH -> {
                    BluetoothPermissionMediaProvider
                }
                Manifest.permission.READ_EXTERNAL_STORAGE -> {
                    ReadExternalStoragePermissionMediaProvider
                }
                Manifest.permission.WRITE_EXTERNAL_STORAGE -> {
                    WriteExternalStoragePermissionMediaProvider
                }
                Manifest.permission.READ_PHONE_STATE -> {
                    ReadPhoneStatePermissionMediaProvider
                }
                Manifest.permission.GET_ACCOUNTS -> {
                    GetAccountsPermissionMediaProvider
                }
                else -> {
                    if(LOG_ME) ALog.e(
                        TAG_PERMISSION_ASKING + TAG, "choosePermissionMediaProvider(): " +
                                            "permission $permission not found")
                    throw InvalidParameterException("permission $permission not found")
                }
            }
        }
    }

    object CoarseLocationPermissionMediaProvider: ProjectPermissionMediaProvider() {
        override fun getPermissionName(): String = Manifest.permission.ACCESS_COARSE_LOCATION

        override fun getDescriptionID(isPermanentlyDeclined: Boolean): Int {
            return if(isPermanentlyDeclined) {
                R.string.homescreen_coarse_location_permission_description_perm_declined
            } else {
                R.string.homescreen_coarse_location_permission_description
            }
        }

        override fun getDialogTitleID(isPermanentlyDeclined: Boolean): Int {
            return if(isPermanentlyDeclined) {
                R.string.homescreen_coarse_location_permission_dialog_title_perm_declined
            } else {
                R.string.homescreen_coarse_location_permission_dialog_title
            }
        }

        override fun getImageID(isPermanentlyDeclined: Boolean): Int {
            return if(isPermanentlyDeclined) {
                R.drawable.homescreen_access_background_location_permission_image
            } else {
                R.drawable.homescreen_coarse_location_permission_image
            }
        }
    }

    object FineLocationPermissionMediaProvider: ProjectPermissionMediaProvider() {
        override fun getPermissionName(): String = Manifest.permission.ACCESS_FINE_LOCATION

        override fun getDescriptionID(isPermanentlyDeclined: Boolean): Int {
            return if(isPermanentlyDeclined) {
                R.string.homescreen_fine_location_permission_description_perm_declined
            } else {
                R.string.homescreen_fine_location_permission_description
            }
        }

        override fun getDialogTitleID(isPermanentlyDeclined: Boolean): Int {
            return if(isPermanentlyDeclined) {
                R.string.homescreen_fine_location_permission_dialog_title_perm_declined
            } else {
                R.string.homescreen_fine_location_permission_dialog_title
            }
        }

        override fun getImageID(isPermanentlyDeclined: Boolean): Int {
            return if(isPermanentlyDeclined) {
                R.drawable.homescreen_fine_location_permission_image_perm_declined
            } else {
                R.drawable.homescreen_fine_location_permission_image
            }
        }
    }

    object CameraPermissionMediaProvider: ProjectPermissionMediaProvider() {
        override fun getPermissionName(): String = Manifest.permission.CAMERA

        override fun getDescriptionID(isPermanentlyDeclined: Boolean): Int {
            return if(isPermanentlyDeclined) {
                R.string.homescreen_camera_permission_description_perm_declined
            } else {
                R.string.homescreen_camera_permission_description
            }
        }

        override fun getDialogTitleID(isPermanentlyDeclined: Boolean): Int {
            return if(isPermanentlyDeclined) {
                R.string.homescreen_camera_permission_dialog_title_perm_declined
            } else {
                R.string.homescreen_camera_permission_dialog_title
            }
        }

        override fun getImageID(isPermanentlyDeclined: Boolean): Int {
            return if(isPermanentlyDeclined) {
                R.drawable.homescreen_camera_permission_image_perm_declined
            } else {
                R.drawable.homescreen_camera_permission_image
            }
        }
    }

    object RecordAudioPermissionMediaProvider: ProjectPermissionMediaProvider() {
        override fun getPermissionName(): String = Manifest.permission.RECORD_AUDIO

        override fun getDescriptionID(isPermanentlyDeclined: Boolean): Int {
            return if(isPermanentlyDeclined) {
                R.string.homescreen_camera_permission_description_perm_declined
            } else {
                R.string.homescreen_camera_permission_description
            }
        }

        override fun getDialogTitleID(isPermanentlyDeclined: Boolean): Int {
            return if(isPermanentlyDeclined) {
                R.string.homescreen_camera_permission_dialog_title_perm_declined
            } else {
                R.string.homescreen_camera_permission_dialog_title
            }
        }

        override fun getImageID(isPermanentlyDeclined: Boolean): Int {
            return if(isPermanentlyDeclined) {
                R.drawable.homescreen_camera_permission_image_perm_declined
            } else {
                R.drawable.homescreen_camera_permission_image
            }
        }
    }

    object PhoneCallPermissionMediaProvider: ProjectPermissionMediaProvider() {
        override fun getPermissionName(): String = Manifest.permission.CALL_PHONE

        override fun getDescriptionID(isPermanentlyDeclined: Boolean): Int {
            return if(isPermanentlyDeclined) {
                R.string.homescreen_phone_call_permission_description_perm_declined
            } else {
                R.string.homescreen_phone_call_permission_description
            }
        }

        override fun getDialogTitleID(isPermanentlyDeclined: Boolean): Int {
            return if(isPermanentlyDeclined) {
                R.string.homescreen_phone_call_permission_dialog_title_perm_declined
            } else {
                R.string.homescreen_phone_call_permission_dialog_title
            }
        }

        override fun getImageID(isPermanentlyDeclined: Boolean): Int {
            return if(isPermanentlyDeclined) {
                R.drawable.homescreen_phone_call_permission_image_perm_declined
            } else {
                R.drawable.homescreen_phone_call_permission_image
            }
        }
    }

    object AccessBackgroundLocationPermissionMediaProvider: ProjectPermissionMediaProvider() {
        override fun getPermissionName(): String = Manifest.permission.ACCESS_BACKGROUND_LOCATION

        override fun getDescriptionID(isPermanentlyDeclined: Boolean): Int {
            return if(isPermanentlyDeclined) {
                R.string.homescreen_access_background_location_permission_description_perm_declined
            } else {
                R.string.homescreen_access_background_location_permission_description
            }
        }

        override fun getDialogTitleID(isPermanentlyDeclined: Boolean): Int {
            return if(isPermanentlyDeclined) {
                R.string.homescreen_access_background_location_permission_dialog_title_perm_declined
            } else {
                R.string.homescreen_access_background_location_permission_dialog_title
            }
        }

        override fun getImageID(isPermanentlyDeclined: Boolean): Int {
            return if(isPermanentlyDeclined) {
                R.drawable.homescreen_access_background_location_permission_image_perm_declined
            } else {
                R.drawable.homescreen_access_background_location_permission_image
            }
        }
    }

    object AccessNetworkStatePermissionMediaProvider: ProjectPermissionMediaProvider() {
        override fun getPermissionName(): String = Manifest.permission.ACCESS_NETWORK_STATE

        override fun getDescriptionID(isPermanentlyDeclined: Boolean): Int {
            return if(isPermanentlyDeclined) {
                R.string.homescreen_access_network_permission_description_perm_declined
            } else {
                R.string.homescreen_access_network_permission_description
            }
        }

        override fun getDialogTitleID(isPermanentlyDeclined: Boolean): Int {
            return if(isPermanentlyDeclined) {
                R.string.homescreen_access_network_permission_dialog_title_perm_declined
            } else {
                R.string.homescreen_access_network_permission_dialog_title
            }
        }

        override fun getImageID(isPermanentlyDeclined: Boolean): Int {
            return if(isPermanentlyDeclined) {
                R.drawable.homescreen_access_network_permission_image_perm_declined
            } else {
                R.drawable.homescreen_access_network_permission_image
            }
        }
    }

    object AccessWiFiStatePermissionMediaProvider: ProjectPermissionMediaProvider() {
        override fun getPermissionName(): String = Manifest.permission.ACCESS_WIFI_STATE

        override fun getDescriptionID(isPermanentlyDeclined: Boolean): Int {
            return if(isPermanentlyDeclined) {
                R.string.homescreen_access_wifi_permission_description_perm_declined
            } else {
                R.string.homescreen_access_wifi_permission_description
            }
        }

        override fun getDialogTitleID(isPermanentlyDeclined: Boolean): Int {
            return if(isPermanentlyDeclined) {
                R.string.homescreen_access_wifi_permission_dialog_title_perm_declined
            } else {
                R.string.homescreen_access_wifi_permission_dialog_title
            }
        }

        override fun getImageID(isPermanentlyDeclined: Boolean): Int {
            return if(isPermanentlyDeclined) {
                R.drawable.homescreen_access_wifi_permission_image_perm_declined
            } else {
                R.drawable.homescreen_access_wifi_permission_image
            }
        }
    }

    object BluetoothPermissionMediaProvider: ProjectPermissionMediaProvider() {
        override fun getPermissionName(): String = Manifest.permission.BLUETOOTH

        override fun getDescriptionID(isPermanentlyDeclined: Boolean): Int {
            return if(isPermanentlyDeclined) {
                R.string.homescreen_bluetooth_permission_description_perm_declined
            } else {
                R.string.homescreen_bluetooth_permission_description
            }
        }

        override fun getDialogTitleID(isPermanentlyDeclined: Boolean): Int {
            return if(isPermanentlyDeclined) {
                R.string.homescreen_bluetooth_permission_dialog_title_perm_declined
            } else {
                R.string.homescreen_bluetooth_permission_dialog_title
            }
        }

        override fun getImageID(isPermanentlyDeclined: Boolean): Int {
            return if(isPermanentlyDeclined) {
                R.drawable.homescreen_bluetooth_permission_image_perm_declined
            } else {
                R.drawable.homescreen_bluetooth_permission_image
            }
        }
    }

    object ReadExternalStoragePermissionMediaProvider: ProjectPermissionMediaProvider() {
        override fun getPermissionName(): String = Manifest.permission.READ_EXTERNAL_STORAGE

        override fun getDescriptionID(isPermanentlyDeclined: Boolean): Int {
            return if(isPermanentlyDeclined) {
                R.string.homescreen_read_external_storage_permission_description_perm_declined
            } else {
                R.string.homescreen_read_external_storage_permission_description
            }
        }

        override fun getDialogTitleID(isPermanentlyDeclined: Boolean): Int {
            return if(isPermanentlyDeclined) {
                R.string.homescreen_read_external_storage_permission_dialog_title_perm_declined
            } else {
                R.string.homescreen_read_external_storage_permission_dialog_title
            }
        }

        override fun getImageID(isPermanentlyDeclined: Boolean): Int {
            return if(isPermanentlyDeclined) {
                R.drawable.homescreen_read_external_storage_permission_image_perm_declined
            } else {
                R.drawable.homescreen_read_external_storage_permission_image
            }
        }
    }

    object WriteExternalStoragePermissionMediaProvider: ProjectPermissionMediaProvider() {
        override fun getPermissionName(): String = Manifest.permission.WRITE_EXTERNAL_STORAGE

        override fun getDescriptionID(isPermanentlyDeclined: Boolean): Int {
            return if(isPermanentlyDeclined) {
                R.string.homescreen_write_external_storage_permission_description_perm_declined
            } else {
                R.string.homescreen_write_external_storage_permission_description
            }
        }

        override fun getDialogTitleID(isPermanentlyDeclined: Boolean): Int {
            return if(isPermanentlyDeclined) {
                R.string.homescreen_write_external_storage_permission_dialog_title_perm_declined
            } else {
                R.string.homescreen_write_external_storage_permission_dialog_title
            }
        }

        override fun getImageID(isPermanentlyDeclined: Boolean): Int {
            return if(isPermanentlyDeclined) {
                R.drawable.homescreen_write_external_storage_permission_image_perm_declined
            } else {
                R.drawable.homescreen_write_external_storage_permission_image
            }
        }
    }

    object ReadPhoneStatePermissionMediaProvider: ProjectPermissionMediaProvider() {
        override fun getPermissionName(): String = Manifest.permission.READ_PHONE_STATE

        override fun getDescriptionID(isPermanentlyDeclined: Boolean): Int {
            return if(isPermanentlyDeclined) {
                R.string.homescreen_read_phone_state_permission_description_perm_declined
            } else {
                R.string.homescreen_read_phone_state_permission_description
            }
        }

        override fun getDialogTitleID(isPermanentlyDeclined: Boolean): Int {
            return if(isPermanentlyDeclined) {
                R.string.homescreen_read_phone_state_permission_dialog_title_perm_declined
            } else {
                R.string.homescreen_read_phone_state_permission_dialog_title
            }
        }

        override fun getImageID(isPermanentlyDeclined: Boolean): Int {
            return if(isPermanentlyDeclined) {
                R.drawable.homescreen_write_external_storage_permission_image_perm_declined
            } else {
                R.drawable.homescreen_write_external_storage_permission_image
            }
        }
    }

    object GetAccountsPermissionMediaProvider: ProjectPermissionMediaProvider() {
        override fun getPermissionName(): String = Manifest.permission.GET_ACCOUNTS

        override fun getDescriptionID(isPermanentlyDeclined: Boolean): Int {
            return if(isPermanentlyDeclined) {
                R.string.homescreen_get_accounts_permission_description_perm_declined
            } else {
                R.string.homescreen_get_accounts_permission_description
            }
        }

        override fun getDialogTitleID(isPermanentlyDeclined: Boolean): Int {
            return if(isPermanentlyDeclined) {
                R.string.homescreen_get_accounts_permission_dialog_title_perm_declined
            } else {
                R.string.homescreen_get_accounts_permission_dialog_title
            }
        }

        override fun getImageID(isPermanentlyDeclined: Boolean): Int {
            return if(isPermanentlyDeclined) {
                R.drawable.homescreen_get_accounts_permission_image_perm_declined
            } else {
                R.drawable.homescreen_get_accounts_permission_image
            }
        }
    }

}