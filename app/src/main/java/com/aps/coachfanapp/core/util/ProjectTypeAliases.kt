package com.aps.coachfanapp.core.util

import com.aps.coachfanapp.cfahomescreen.business.data.cache.abstraction.SportsGameCacheDataSource
import com.aps.coachfanapp.cfahomescreen.business.data.cache.abstraction.SportsNewsMessageCacheDataSource
import com.aps.coachfanapp.cfahomescreen.business.data.cache.abstraction.SportsTeamCacheDataSource
import com.aps.coachfanapp.cfahomescreen.business.data.cache.abstraction.SportsTeamPlayerCacheDataSource
import com.aps.coachfanapp.cfahomescreen.business.data.cache.implementation.SportsGameCacheDataSourceImpl
import com.aps.coachfanapp.cfahomescreen.business.data.cache.implementation.SportsNewsMessageCacheDataSourceImpl
import com.aps.coachfanapp.cfahomescreen.business.data.cache.implementation.SportsTeamCacheDataSourceImpl
import com.aps.coachfanapp.cfahomescreen.business.data.cache.implementation.SportsTeamPlayerCacheDataSourceImpl
import com.aps.coachfanapp.cfahomescreen.business.data.network.abs.SportsGameNetworkDataSource
import com.aps.coachfanapp.cfahomescreen.business.data.network.abs.SportsNewsMessageNetworkDataSource
import com.aps.coachfanapp.cfahomescreen.business.data.network.abs.SportsTeamNetworkDataSource
import com.aps.coachfanapp.cfahomescreen.business.data.network.abs.SportsTeamPlayerNetworkDataSource
import com.aps.coachfanapp.cfahomescreen.business.domain.model.entities.SportsGame
import com.aps.coachfanapp.cfahomescreen.business.domain.model.entities.SportsNewsMessage
import com.aps.coachfanapp.cfahomescreen.business.domain.model.entities.SportsTeam
import com.aps.coachfanapp.cfahomescreen.business.domain.model.entities.SportsTeamPlayer
import com.aps.coachfanapp.cfahomescreen.business.domain.model.factories.SportsGameFactory
import com.aps.coachfanapp.cfahomescreen.business.domain.model.factories.SportsNewsMessageFactory
import com.aps.coachfanapp.cfahomescreen.business.domain.model.factories.SportsTeamFactory
import com.aps.coachfanapp.cfahomescreen.business.domain.model.factories.SportsTeamPlayerFactory
import com.aps.coachfanapp.cfahomescreen.framework.datasources.cachedatasources.room.abs.SportsGameDaoService
import com.aps.coachfanapp.cfahomescreen.framework.datasources.cachedatasources.room.abs.SportsNewsMessageDaoService
import com.aps.coachfanapp.cfahomescreen.framework.datasources.cachedatasources.room.abs.SportsTeamDaoService
import com.aps.coachfanapp.cfahomescreen.framework.datasources.cachedatasources.room.abs.SportsTeamPlayerDaoService
import com.aps.coachfanapp.cfahomescreen.framework.datasources.cachedatasources.room.daos.SportsGameDao
import com.aps.coachfanapp.cfahomescreen.framework.datasources.cachedatasources.room.daos.SportsNewsMessageDao
import com.aps.coachfanapp.cfahomescreen.framework.datasources.cachedatasources.room.daos.SportsTeamDao
import com.aps.coachfanapp.cfahomescreen.framework.datasources.cachedatasources.room.daos.SportsTeamPlayerDao
import com.aps.coachfanapp.cfahomescreen.framework.datasources.cachedatasources.room.impl.SportsGameDaoServiceImpl
import com.aps.coachfanapp.cfahomescreen.framework.datasources.cachedatasources.room.impl.SportsNewsMessageDaoServiceImpl
import com.aps.coachfanapp.cfahomescreen.framework.datasources.cachedatasources.room.impl.SportsTeamDaoServiceImpl
import com.aps.coachfanapp.cfahomescreen.framework.datasources.cachedatasources.room.impl.SportsTeamPlayerDaoServiceImpl
import com.aps.coachfanapp.cfahomescreen.framework.datasources.cachedatasources.room.mappers.SportsGameCacheMapper
import com.aps.coachfanapp.cfahomescreen.framework.datasources.cachedatasources.room.mappers.SportsNewsMessageCacheMapper
import com.aps.coachfanapp.cfahomescreen.framework.datasources.cachedatasources.room.mappers.SportsTeamCacheMapper
import com.aps.coachfanapp.cfahomescreen.framework.datasources.cachedatasources.room.mappers.SportsTeamPlayerCacheMapper
import com.aps.coachfanapp.cfahomescreen.framework.datasources.networkdatasources.firebase.abs.SportsGameFirestoreService
import com.aps.coachfanapp.cfahomescreen.framework.datasources.networkdatasources.firebase.abs.SportsNewsMessageFirestoreService
import com.aps.coachfanapp.cfahomescreen.framework.datasources.networkdatasources.firebase.abs.SportsTeamFirestoreService
import com.aps.coachfanapp.cfahomescreen.framework.datasources.networkdatasources.firebase.abs.SportsTeamPlayerFirestoreService
import com.aps.coachfanapp.cfahomescreen.framework.datasources.networkdatasources.firebase.impl.SportsGameFirestoreServiceImpl
import com.aps.coachfanapp.cfahomescreen.framework.datasources.networkdatasources.firebase.impl.SportsNewsMessageFirestoreServiceImpl
import com.aps.coachfanapp.cfahomescreen.framework.datasources.networkdatasources.firebase.impl.SportsTeamFirestoreServiceImpl
import com.aps.coachfanapp.cfahomescreen.framework.datasources.networkdatasources.firebase.impl.SportsTeamPlayerFirestoreServiceImpl
import com.aps.coachfanapp.cfahomescreen.framework.datasources.networkdatasources.firebase.mappers.SportsGameFirestoreMapper
import com.aps.coachfanapp.cfahomescreen.framework.datasources.networkdatasources.firebase.mappers.SportsNewsMessageFirestoreMapper
import com.aps.coachfanapp.cfahomescreen.framework.datasources.networkdatasources.firebase.mappers.SportsTeamFirestoreMapper
import com.aps.coachfanapp.cfahomescreen.framework.datasources.networkdatasources.firebase.mappers.SportsTeamPlayerFirestoreMapper


typealias Entity1 = SportsGame
typealias Entity1NetworkDataSource = SportsGameNetworkDataSource
typealias Entity1CacheDataSource = SportsGameCacheDataSource
typealias Entity1CacheDataSourceImpl = SportsGameCacheDataSourceImpl
typealias Entity1Factory = SportsGameFactory
typealias Entity1FirestoreServiceImpl = SportsGameFirestoreServiceImpl
typealias Entity1FirestoreService = SportsGameFirestoreService
typealias Entity1FirestoreMapper = SportsGameFirestoreMapper
typealias Entity1DaoService = SportsGameDaoService
typealias Entity1DaoServiceImpl = SportsGameDaoServiceImpl
typealias Entity1Dao = SportsGameDao
typealias Entity1CacheMapper = SportsGameCacheMapper

typealias Entity2 = SportsTeamPlayer
typealias Entity2NetworkDataSource = SportsTeamPlayerNetworkDataSource
typealias Entity2CacheDataSource = SportsTeamPlayerCacheDataSource
typealias Entity2CacheDataSourceImpl = SportsTeamPlayerCacheDataSourceImpl
typealias Entity2Factory = SportsTeamPlayerFactory
typealias Entity2FirestoreServiceImpl = SportsTeamPlayerFirestoreServiceImpl
typealias Entity2FirestoreService = SportsTeamPlayerFirestoreService
typealias Entity2FirestoreMapper = SportsTeamPlayerFirestoreMapper
typealias Entity2DaoService = SportsTeamPlayerDaoService
typealias Entity2DaoServiceImpl = SportsTeamPlayerDaoServiceImpl
typealias Entity2Dao = SportsTeamPlayerDao
typealias Entity2CacheMapper = SportsTeamPlayerCacheMapper


typealias Entity3 = SportsTeam
typealias Entity3NetworkDataSource = SportsTeamNetworkDataSource
typealias Entity3CacheDataSource = SportsTeamCacheDataSource
typealias Entity3CacheDataSourceImpl = SportsTeamCacheDataSourceImpl
typealias Entity3Factory = SportsTeamFactory
typealias Entity3FirestoreServiceImpl = SportsTeamFirestoreServiceImpl
typealias Entity3FirestoreService = SportsTeamFirestoreService
typealias Entity3FirestoreMapper = SportsTeamFirestoreMapper
typealias Entity3DaoService = SportsTeamDaoService
typealias Entity3DaoServiceImpl = SportsTeamDaoServiceImpl
typealias Entity3Dao = SportsTeamDao
typealias Entity3CacheMapper = SportsTeamCacheMapper

typealias Entity4 = SportsNewsMessage
typealias Entity4NetworkDataSource = SportsNewsMessageNetworkDataSource
typealias Entity4CacheDataSource = SportsNewsMessageCacheDataSource
typealias Entity4CacheDataSourceImpl = SportsNewsMessageCacheDataSourceImpl
typealias Entity4Factory = SportsNewsMessageFactory
typealias Entity4FirestoreServiceImpl = SportsNewsMessageFirestoreServiceImpl
typealias Entity4FirestoreService = SportsNewsMessageFirestoreService
typealias Entity4FirestoreMapper = SportsNewsMessageFirestoreMapper
typealias Entity4DaoService = SportsNewsMessageDaoService
typealias Entity4DaoServiceImpl = SportsNewsMessageDaoServiceImpl
typealias Entity4Dao = SportsNewsMessageDao
typealias Entity4CacheMapper = SportsNewsMessageCacheMapper