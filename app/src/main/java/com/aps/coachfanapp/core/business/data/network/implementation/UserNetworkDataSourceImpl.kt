package com.aps.coachfanapp.core.business.data.network.implementation

import android.net.Uri
import com.aps.coachfanapp.common.util.UniqueID
import com.aps.coachfanapp.common.util.UserUniqueID
import com.aps.coachfanapp.core.business.data.network.abstraction.UserNetworkDataSource
import com.aps.coachfanapp.core.business.domain.model.entities.ProjectUser
import com.aps.coachfanapp.core.business.domain.model.entities.ProjectUsersRating
import com.aps.coachfanapp.core.framework.datasources.networkdatasources.firebase.abstraction.UserFirestoreService
import com.google.firebase.auth.FirebaseUser
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class UserNetworkDataSourceImpl
@Inject
constructor(
    private val firestoreService: UserFirestoreService
): UserNetworkDataSource {
    override suspend fun registerNewUser(entity: ProjectUser): FirebaseUser? {
        return firestoreService.registerNewUser(entity)
    }

    override suspend fun updateUser(entity: ProjectUser): ProjectUser? {
        return firestoreService.updateUser(entity)
    }

    override suspend fun loginUser(entity: ProjectUser): FirebaseUser? {
        return firestoreService.loginUser(entity)
    }

    override suspend fun logoutUser(entity: ProjectUser) {
        firestoreService.logoutUser(entity)
    }

    override suspend fun insertOrUpdateEntity(entity: ProjectUser): ProjectUser? {
        return firestoreService.insertOrUpdateEntity(entity)
    }

    override suspend fun getEntityByCredentials(email: String?, password: String?
    ): ProjectUser? {
        return firestoreService.getEntityByCredentials(email, password)
    }

    override suspend fun deleteEntity(primaryKey: UniqueID?) {
        return firestoreService.deleteEntity(primaryKey)
    }

    override suspend fun insertDeletedEntity(entity: ProjectUser) {
        return firestoreService.insertDeletedEntity(entity)
    }

    override suspend fun insertDeletedEntities(entities: List<ProjectUser>) {
        return firestoreService.insertDeletedEntities(entities)
    }

    override suspend fun deleteDeletedEntity(entity: ProjectUser) {
        return firestoreService.deleteDeletedEntity(entity)
    }

    override suspend fun getDeletedEntities(): List<ProjectUser> {
        return firestoreService.getAllDeletedEntities()
    }

    override suspend fun deleteAllEntities() {
        firestoreService.deleteAllEntities()
    }

    override suspend fun searchEntity(entity: ProjectUser): ProjectUser? {
        return firestoreService.searchEntity(entity)
    }

    override suspend fun getAllEntities(): List<ProjectUser> {
        return firestoreService.getAllEntities()
    }

    override suspend fun insertOrUpdateEntities(entities: List<ProjectUser>): List<ProjectUser>? {
        return firestoreService.insertOrUpdateEntities(entities)
    }

    override suspend fun checkIfEmailExistsInRepository(email: String?): Boolean {
        return firestoreService.checkIfEmailExistsInRepository(email)
    }

    override suspend fun getIdOfUserWithTheseCredentials(email: String?, password: String?): UserUniqueID? {
        return firestoreService.checkIfPasswordMatchesEmail(email, password)
    }

    override suspend fun getEntityById(id: UniqueID): ProjectUser? {
        return firestoreService.getEntityById(id)
    }

    override suspend fun deleteAccount(projectUser: ProjectUser) {
        firestoreService.deleteAccount(projectUser)
    }

    override suspend fun uploadUsersProfilePhotoToFirestore(
        projectUser : ProjectUser,
        usersProfilePhotoUri : Uri,
    ) : String? {
        return firestoreService.uploadUsersProfilePhotoToFirestore(projectUser, usersProfilePhotoUri)
    }

    override suspend fun uploadUsersProfilePhotoToFirestore(
        userID : UserUniqueID,
        usersProfilePhotoUri : Uri,
    ) : String? {
        return firestoreService.uploadUsersProfilePhotoToFirestore(userID, usersProfilePhotoUri)
    }

    override suspend fun getUsersRating(userID: UserUniqueID): ProjectUsersRating? {
        return firestoreService.getUsersRating(userID)
    }

    override suspend fun insertUsersRating(usersRating: ProjectUsersRating) {
        return firestoreService.insertUsersRating(usersRating)
    }
}