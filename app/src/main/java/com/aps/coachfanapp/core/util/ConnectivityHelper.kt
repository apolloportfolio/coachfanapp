package com.aps.coachfanapp.core.util

import android.content.Context
import android.net.ConnectivityManager
import javax.inject.Inject

class ConnectivityHelper
@Inject
constructor(
    private val applicationContextProvider: ApplicationContextProvider
) {
    fun getContext(): Context {
        return applicationContextProvider.applicationContext("ConnectivityManagerExtensions")
    }

    fun internetConnectionIsAvailable(context: Context): Boolean {
        val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetworkInfo = connectivityManager.activeNetworkInfo
        return activeNetworkInfo != null && activeNetworkInfo.isConnected
    }
}