package com.aps.coachfanapp.core.framework.datasources.cachedatasources.room

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.aps.coachfanapp.cfahomescreen.framework.datasources.cachedatasources.room.daos.SportsGameDao
import com.aps.coachfanapp.cfahomescreen.framework.datasources.cachedatasources.room.daos.SportsNewsMessageDao
import com.aps.coachfanapp.cfahomescreen.framework.datasources.cachedatasources.room.daos.SportsTeamDao
import com.aps.coachfanapp.cfahomescreen.framework.datasources.cachedatasources.room.daos.SportsTeamPlayerDao
import com.aps.coachfanapp.cfahomescreen.framework.datasources.cachedatasources.room.model.SportsFanMessageCacheEntity
import com.aps.coachfanapp.cfahomescreen.framework.datasources.cachedatasources.room.model.SportsGameCacheEntity
import com.aps.coachfanapp.cfahomescreen.framework.datasources.cachedatasources.room.model.SportsNewsMessageCacheEntity
import com.aps.coachfanapp.cfahomescreen.framework.datasources.cachedatasources.room.model.SportsTeamCacheEntity
import com.aps.coachfanapp.cfahomescreen.framework.datasources.cachedatasources.room.model.SportsTeamPlayerCacheEntity
import com.aps.coachfanapp.core.framework.datasources.cachedatasources.room.daos.*
import com.aps.coachfanapp.core.framework.datasources.cachedatasources.room.model.*

@Database(entities = [
    UserCacheEntity::class,

    SportsGameCacheEntity::class,
    SportsTeamPlayerCacheEntity::class,
    SportsTeamCacheEntity::class,
    SportsNewsMessageCacheEntity::class,
    SportsFanMessageCacheEntity::class,

], version = 1, exportSchema = false)
@TypeConverters(RoomTypeConverters::class)
abstract class ProjectRoomDatabase : RoomDatabase() {
    abstract fun userDao() : UserDao

    // CoachFanApp
    abstract fun sportsGameDao() : SportsGameDao

    abstract fun sportsTeamPlayerDao() : SportsTeamPlayerDao

    abstract fun sportsTeamDao() : SportsTeamDao

    abstract fun sportsNewsMessageDao() : SportsNewsMessageDao

//    abstract fun sportsFanMessageDao() : SportsFanMessageDao

    companion object {
        const val DATABASE_NAME: String = "APSDB"
    }
}