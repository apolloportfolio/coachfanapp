package com.aps.coachfanapp.core

import android.content.Context
import com.aps.coachfanapp.common.BaseApplication
import com.aps.coachfanapp.common.util.ALog
import dagger.hilt.android.HiltAndroidApp
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview

private const val TAG = "ProjectApplication"
private const val LOG_ME = true

@HiltAndroidApp
@FlowPreview
@ExperimentalCoroutinesApi
open class ProjectApplication : BaseApplication() {
    init {
        instance = this
    }
    companion object {
        private lateinit var instance: ProjectApplication
        fun applicationContext(tag: String?): Context? {
            if(LOG_ME) ALog.d(TAG, ".applicationContext(): " +
                    "App Context requested by $tag")
            return if(this::instance.isInitialized) {
                return instance as Context
            } else {
                return null
            }
        }
        fun application(tag: String?): ProjectApplication {
            if(LOG_ME) ALog.d(TAG, ".applicationContext(): " +
                    "App Context requested by $tag")

            return instance
        }
    }
}