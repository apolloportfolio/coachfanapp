package com.aps.coachfanapp.core.framework.datasources.cachedatasources.room.abstraction

import com.aps.coachfanapp.common.util.UniqueID
import com.aps.coachfanapp.common.util.UserUniqueID
import com.aps.coachfanapp.core.business.domain.model.entities.ProjectUser

interface UserDaoService {
    suspend fun insertOrUpdateEntity(entity: ProjectUser): ProjectUser

    suspend fun insertEntity(entity: ProjectUser): Long

    suspend fun insertEntities(Entities: List<ProjectUser>): LongArray

    suspend fun getEntityById(id: UniqueID?): ProjectUser?

    suspend fun updateEntity(
        id: UserUniqueID?,
        updated_at: String?,
        created_at: String?,
        emailAddress: String,
        password: String,
        profilePhotoImageURI: String?,
        name: String?,
        surname: String?,
        description: String?,
        city: String?,
        emailAddressVerified: Boolean = false,
        phoneNumberVerified: Boolean = false,
    ): Int

    suspend fun updateUsersEmail(
        id: UniqueID?,
        updated_at: String?,
        emailAddress: String?,
    ): Int

    suspend fun deleteEntity(primaryKey: UniqueID?): Int

    suspend fun deleteEntities(Entities: List<ProjectUser>): Int

    suspend fun deleteAllEntities()

    suspend fun searchEntities(): List<ProjectUser>

    suspend fun getAllEntities(): List<ProjectUser>

    suspend fun getNumEntities(): Int

    suspend fun returnOrderedQuery(
        query: String,
        filterAndOrder: String,
        page: Int
    ): List<ProjectUser>
}