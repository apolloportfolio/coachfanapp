package com.aps.coachfanapp.core.framework.datasources.networkdatasources.firebase.abstraction

import android.net.Uri
import com.aps.coachfanapp.common.util.UserUniqueID
import com.aps.coachfanapp.core.business.domain.model.entities.ProjectUser
import com.aps.coachfanapp.core.business.domain.model.entities.ProjectUsersRating
import com.aps.coachfanapp.core.framework.datasources.networkdatasources.firebase.mappers.UserFirestoreMapper
import com.aps.coachfanapp.core.framework.datasources.networkdatasources.firebase.model.UserFirestoreEntity
import com.google.firebase.auth.FirebaseUser

interface UserFirestoreService:
    BasicFirestoreService<ProjectUser, UserFirestoreEntity, UserFirestoreMapper> {
    suspend fun registerNewUser(entity: ProjectUser): FirebaseUser?

    suspend fun updateUser(entity: ProjectUser): ProjectUser?

    suspend fun loginUser(entity: ProjectUser): FirebaseUser?

    suspend fun logoutUser(entity: ProjectUser)

    suspend fun checkIfEmailExistsInRepository(email: String?): Boolean

    suspend fun checkIfPasswordMatchesEmail(email: String?, password: String?): UserUniqueID?

    suspend fun getEntityByCredentials(email: String?, password: String?): ProjectUser?

    suspend fun deleteAccount(entity: ProjectUser)

    suspend fun uploadUsersProfilePhotoToFirestore(
        projectUser : ProjectUser,
        usersProfilePhotoUri : Uri,
    ) : String?

    suspend fun uploadUsersProfilePhotoToFirestore(
        userID : UserUniqueID,
        usersProfilePhotoUri : Uri,
    ) : String?

    suspend fun getUsersRating(userID: UserUniqueID): ProjectUsersRating?

    suspend fun insertUsersRating(usersUsersRating: ProjectUsersRating)
}