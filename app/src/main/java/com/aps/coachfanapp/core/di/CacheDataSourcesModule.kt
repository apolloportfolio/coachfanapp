package com.aps.coachfanapp.core.di

import com.aps.coachfanapp.core.business.data.cache.abstraction.*
import com.aps.coachfanapp.core.business.data.cache.implementation.*
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
abstract class CacheDataSourcesModule {

    @Binds
    abstract fun bindUserCacheDataSource(implementation: UserCacheDataSourceImpl): UserCacheDataSource

}