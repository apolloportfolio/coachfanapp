package com.aps.coachfanapp.core.di

import android.content.Context
import androidx.room.Room
import com.aps.coachfanapp.core.framework.datasources.cachedatasources.room.ProjectRoomDatabase
import com.aps.coachfanapp.core.framework.datasources.cachedatasources.room.daos.*
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
class RoomModule {
    @Singleton
    @Provides
    fun provideProjectRoomDatabase(@ApplicationContext context : Context) : ProjectRoomDatabase {
        return Room.databaseBuilder(
                context,
                ProjectRoomDatabase::class.java,
                ProjectRoomDatabase.DATABASE_NAME
        )
                .fallbackToDestructiveMigration()
                .build()
    }

    @Singleton
    @Provides
    fun provideUserDao(database : ProjectRoomDatabase) : UserDao {
        return database.userDao()
    }
}