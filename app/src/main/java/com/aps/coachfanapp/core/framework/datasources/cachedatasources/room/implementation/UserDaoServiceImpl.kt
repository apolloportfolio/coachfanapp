package com.aps.coachfanapp.core.framework.datasources.cachedatasources.room.implementation

import com.aps.coachfanapp.common.util.ALog
import com.aps.coachfanapp.common.util.DateUtil
import com.aps.coachfanapp.common.util.UniqueID
import com.aps.coachfanapp.common.util.UserUniqueID
import com.aps.coachfanapp.core.business.domain.model.entities.ProjectUser
import com.aps.coachfanapp.core.framework.datasources.cachedatasources.room.abstraction.UserDaoService
import com.aps.coachfanapp.core.framework.datasources.cachedatasources.room.daos.UserDao
import com.aps.coachfanapp.core.framework.datasources.cachedatasources.room.mappers.UserCacheMapper
import javax.inject.Inject
import javax.inject.Singleton

private const val TAG = "UserDaoServiceImpl"
private const val LOG_ME = true
@Singleton
class UserDaoServiceImpl
@Inject
constructor(
    private val dao: UserDao,
    private val mapper: UserCacheMapper,
    private val dateUtil: DateUtil
): UserDaoService {

    override suspend fun insertOrUpdateEntity(entity: ProjectUser): ProjectUser {
        val methodName: String = "insertOrUpdateEntity"
        var result: ProjectUser
        if (LOG_ME) ALog.d(TAG, "Method start: $methodName")
        try {
            dao.insert(mapper.mapToEntity(entity))
            result = entity
            return result
        } catch (e: Exception) {
            ALog.e(TAG, methodName, e)
            return entity
        } finally {
            if (LOG_ME) ALog.d(TAG, "Method end: $methodName")
        }

    }

    override suspend fun insertEntity(entity: ProjectUser): Long {
        return dao.insert(mapper.mapToEntity(entity))
    }

    override suspend fun insertEntities(entities: List<ProjectUser>): LongArray {
        return dao.insertEntities(
            mapper.mapToEntityList(entities)
        )
    }

    override suspend fun getEntityById(id: UniqueID?): ProjectUser? {
        return dao.getEntityById(id)?.let { entity ->
            mapper.mapFromEntity(entity)
        }
    }

    override suspend fun updateEntity(
        id: UserUniqueID?,
        updated_at: String?,
        created_at: String?,
        emailAddress: String,
        password: String,
        profilePhotoImageURI: String?,
        name: String?,
        surname: String?,
        description: String?,
        city: String?,
        emailAddressVerified: Boolean,
        phoneNumberVerified: Boolean,
    ): Int {
        return if(updated_at != null){
            dao.updateEntity(
                id,
                updated_at,
                created_at,
                emailAddress,
                password,
                profilePhotoImageURI,
                name,
                surname,
                description,
                city,
                emailAddressVerified,
                phoneNumberVerified,
            )
        } else {
            dao.updateEntity(
                id,
                dateUtil.getCurrentTimestamp(),
                created_at,
                emailAddress,
                password,
                profilePhotoImageURI,
                name,
                surname,
                description,
                city,
                emailAddressVerified,
                phoneNumberVerified,
            )
        }

    }

    override suspend fun updateUsersEmail(
        id: UniqueID?,
        updated_at: String?,
        emailAddress: String?,
    ): Int {
        return dao.updateUsersEmail(id, updated_at, emailAddress)
    }

    override suspend fun deleteEntity(primaryKey: UniqueID?): Int {
        return dao.deleteEntity(primaryKey)
    }

    override suspend fun deleteEntities(entities: List<ProjectUser>): Int {
        val ids = entities.mapIndexed {index, value -> value.id}
        return dao.deleteEntities(ids)
    }

    override suspend fun deleteAllEntities() {
        dao.deleteAllEntities()
    }

    override suspend fun searchEntities(): List<ProjectUser> {
        return mapper.mapFromEntityList(
            dao.searchEntities()
        )
    }

    override suspend fun getAllEntities(): List<ProjectUser> {
        return mapper.mapFromEntityList(dao.getAllEntities())
    }

    override suspend fun getNumEntities(): Int {
        return dao.getNumEntities()
    }

    override suspend fun returnOrderedQuery(
        query: String,
        filterAndOrder: String,
        page: Int
    ): List<ProjectUser> {
        return mapper.mapFromEntityList(
            dao.returnOrderedQuery(
                query = query,
                page = page,
                filterAndOrder = filterAndOrder
            )
        )
    }
}