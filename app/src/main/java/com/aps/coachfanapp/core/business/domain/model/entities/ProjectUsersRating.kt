package com.aps.coachfanapp.core.business.domain.model.entities

import android.annotation.SuppressLint
import android.os.Parcel
import android.os.Parcelable
import com.aps.coachfanapp.common.util.UserUniqueID

@SuppressLint("ParcelCreator")
data class ProjectUsersRating(
    var usersID: UserUniqueID,
    var usersRatingEntity: HashMap<String, String?>,
) : Parcelable {
    var averageRating: Float? = usersRatingEntity.let {
        it[FIELD_NAME_AVERAGE_RATING]
    }?.toFloatOrNull()
    var numberOfRatings: Int? = usersRatingEntity.let {
        it[FIELD_NAME_NUMBER_OF_RATINGS]
    }?.toIntOrNull()







    constructor(parcel: Parcel) : this(
        TODO("usersID"),
        TODO("usersRatingEntity")
    ) {
        averageRating = parcel.readValue(Float::class.java.classLoader) as? Float
        numberOfRatings = parcel.readValue(Int::class.java.classLoader) as? Int
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeValue(averageRating)
        parcel.writeValue(numberOfRatings)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ProjectUsersRating> {
        const val FIELD_NAME_AVERAGE_RATING = "averageRating"
        const val FIELD_NAME_NUMBER_OF_RATINGS = "numberOfRatings"
        override fun createFromParcel(parcel: Parcel): ProjectUsersRating {
            return ProjectUsersRating(parcel)
        }

        override fun newArray(size: Int): Array<ProjectUsersRating?> {
            return arrayOfNulls(size)
        }
    }
}