package com.aps.coachfanapp.core

import android.app.Application
import android.content.SharedPreferences
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.aps.coachfanapp.core.util.ApplicationContextProvider
import com.aps.coachfanapp.common.framework.presentation.ApplicationViewModel
import com.aps.coachfanapp.common.util.DateUtil
import com.aps.coachfanapp.core.business.data.cache.implementation.UserCacheDataSourceImpl
import com.aps.coachfanapp.core.business.data.network.abstraction.UserNetworkDataSource
import com.aps.coachfanapp.core.business.domain.model.entities.ProjectUser
import com.aps.coachfanapp.core.business.domain.model.factories.UserFactory
import com.aps.coachfanapp.core.util.Entity1
import com.aps.coachfanapp.core.util.Entity1CacheDataSourceImpl
import com.aps.coachfanapp.core.util.Entity1Factory
import com.aps.coachfanapp.core.util.Entity1NetworkDataSource
import com.aps.coachfanapp.feature01.business.interactors.registerlogin.RegisterLoginInteractors
import com.aps.coachfanapp.feature01.framework.presentation.activity01.RegisterLoginActivityViewModel
import com.aps.coachfanapp.feature01.framework.presentation.activity01.state.RegisterLoginActivityViewState
import com.aps.coachfanapp.feature03.business.interactors.ActivityEntity1DetailsInteractors
import com.aps.coachfanapp.feature03.framework.presentation.activity01.ActivityEntity1DetailsViewModel
import com.aps.coachfanapp.feature03.framework.presentation.activity01.state.ActivityEntity1DetailsViewState
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview

private const val TAG = "ProjectViewModelFactory"

@FlowPreview
@ExperimentalCoroutinesApi
class ProjectViewModelFactory
constructor(
    private val registerLoginInteractors: RegisterLoginInteractors<
            ProjectUser,
            UserCacheDataSourceImpl,
            UserNetworkDataSource,
            RegisterLoginActivityViewState<ProjectUser>
            >,
    private val activityEntityDetailsInteractors: ActivityEntity1DetailsInteractors<
            Entity1,
            Entity1CacheDataSourceImpl,
            Entity1NetworkDataSource,
            ActivityEntity1DetailsViewState<Entity1>
            >,
    private val dateUtil: DateUtil,
    private val userFactory: UserFactory,
    private val sportsGameFactory: Entity1Factory,
    private val editor: SharedPreferences.Editor,
    private val applicationContextProvider: ApplicationContextProvider,
    //private val sharedPreferences: SharedPreferences
) : ViewModelProvider.Factory {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return when(modelClass){

            RegisterLoginActivityViewModel::class.java -> {
                RegisterLoginActivityViewModel(
                    interactors = registerLoginInteractors,
                    dateUtil = dateUtil,
                    entityFactory = userFactory,
                    applicationContextProvider = applicationContextProvider,
                ) as T
            }

            ActivityEntity1DetailsViewModel::class.java -> {
                ActivityEntity1DetailsViewModel(
                    interactors = activityEntityDetailsInteractors,
                    dateUtil = dateUtil,
                    entityFactory = sportsGameFactory,
                    applicationContextProvider = applicationContextProvider,
                ) as T
            }




            ApplicationViewModel::class.java -> {
                ApplicationViewModel(
                application = applicationContextProvider.applicationContext(TAG) as Application
                ) as T
            }

            else -> {
                throw IllegalArgumentException("unknown model class $modelClass")
            }
        }
    }
}