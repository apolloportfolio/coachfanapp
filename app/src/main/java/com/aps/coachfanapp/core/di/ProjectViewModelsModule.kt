package com.aps.coachfanapp.core.di

import android.content.SharedPreferences
import androidx.lifecycle.ViewModelProvider
import com.aps.coachfanapp.common.util.DateUtil
import com.aps.coachfanapp.core.ProjectViewModelFactory
import com.aps.coachfanapp.core.business.data.cache.implementation.UserCacheDataSourceImpl
import com.aps.coachfanapp.core.business.data.network.abstraction.UserNetworkDataSource
import com.aps.coachfanapp.core.business.domain.model.entities.ProjectUser
import com.aps.coachfanapp.core.business.domain.model.factories.UserFactory
import com.aps.coachfanapp.core.util.ApplicationContextProvider
import com.aps.coachfanapp.core.util.Entity1
import com.aps.coachfanapp.core.util.Entity1CacheDataSourceImpl
import com.aps.coachfanapp.core.util.Entity1Factory
import com.aps.coachfanapp.core.util.Entity1NetworkDataSource
import com.aps.coachfanapp.feature01.business.interactors.registerlogin.RegisterLoginInteractors
import com.aps.coachfanapp.feature01.framework.presentation.activity01.state.RegisterLoginActivityViewState
import com.aps.coachfanapp.feature03.business.interactors.ActivityEntity1DetailsInteractors
import com.aps.coachfanapp.feature03.framework.presentation.activity01.state.ActivityEntity1DetailsViewState
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import javax.inject.Singleton

@ExperimentalCoroutinesApi
@FlowPreview
@Module
@InstallIn(SingletonComponent::class)
object ProjectViewModelsModule {
    @Singleton
    @JvmStatic
    @Provides
    fun provideProjectViewModelFactory(
        registerLoginInteractors: RegisterLoginInteractors<
                ProjectUser,
                UserCacheDataSourceImpl,
                UserNetworkDataSource,
                RegisterLoginActivityViewState<ProjectUser>
                >,
        activityEntityDetailsInteractors: ActivityEntity1DetailsInteractors<
                Entity1,
                Entity1CacheDataSourceImpl,
                Entity1NetworkDataSource,
                ActivityEntity1DetailsViewState<Entity1>
                >,
        dateUtil: DateUtil,
        entityFactory: UserFactory,
        entity1Factory: Entity1Factory,
        editor: SharedPreferences.Editor,
        applicationContextProvider: ApplicationContextProvider,
        //sharedPreferences: SharedPreferences
    ): ViewModelProvider.Factory {
        return ProjectViewModelFactory(
            registerLoginInteractors = registerLoginInteractors,
            activityEntityDetailsInteractors = activityEntityDetailsInteractors,
            dateUtil = dateUtil,
            userFactory = entityFactory,
            sportsGameFactory = entity1Factory,
            editor = editor,
            applicationContextProvider = applicationContextProvider,
        )
    }
}