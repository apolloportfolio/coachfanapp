package com.aps.coachfanapp.core.business.data.cache.implementation

import com.aps.coachfanapp.common.util.UniqueID
import com.aps.coachfanapp.common.util.UserUniqueID
import com.aps.coachfanapp.core.business.data.cache.abstraction.UserCacheDataSource
import com.aps.coachfanapp.core.business.domain.model.entities.ProjectUser
import com.aps.coachfanapp.core.framework.datasources.cachedatasources.room.abstraction.UserDaoService
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class UserCacheDataSourceImpl
@Inject
constructor(
    private val entityDaoService: UserDaoService
): UserCacheDataSource {

    override suspend fun insertOrUpdateEntity(entity: ProjectUser): ProjectUser? {
        return entityDaoService.insertOrUpdateEntity(entity)
    }

    override suspend fun insertEntity(entity: ProjectUser): Long {
        return entityDaoService.insertEntity(entity)
    }

    override suspend fun deleteEntity(primaryKey: UniqueID?): Int {
        return entityDaoService.deleteEntity(primaryKey)
    }

    override suspend fun deleteEntities(entities: List<ProjectUser>): Int {
        return entityDaoService.deleteEntities(entities)
    }

    override suspend fun deleteAllEntities() {
        entityDaoService.deleteAllEntities()
    }

    override suspend fun updateEntity(
        id: UserUniqueID?,
        updated_at: String?,
        created_at: String?,
        emailAddress: String,
        password: String,
        profilePhotoImageURI: String?,
        name: String?,
        surname: String?,
        description: String?,
        city: String?,
        emailAddressVerified: Boolean,
        phoneNumberVerified: Boolean,
    ): Int {
        return entityDaoService.updateEntity(
                id,
                updated_at,
                created_at,
                emailAddress,
                password,
                profilePhotoImageURI,
                name,
                surname,
                description,
                city,
                emailAddressVerified,
                phoneNumberVerified,
        )
    }

    override suspend fun updateUsersEmail(
        id: UniqueID?,
        updated_at: String?,
        emailAddress: String?,
    ): Int {
        return entityDaoService.updateUsersEmail(id, updated_at, emailAddress)
    }

    override suspend fun searchEntities(
        query: String,
        filterAndOrder: String,
        page: Int
    ): List<ProjectUser> {
        return entityDaoService.returnOrderedQuery(
            query, filterAndOrder, page
        )
    }

    override suspend fun getAllEntities(): List<ProjectUser> {
        return entityDaoService.getAllEntities()
    }

    override suspend fun getEntityById(id: UniqueID?): ProjectUser? {
        return entityDaoService.getEntityById(id)
    }

    override suspend fun getNumEntities(): Int {
        return entityDaoService.getNumEntities()
    }

    override suspend fun insertEntities(entities: List<ProjectUser>): LongArray{
        return entityDaoService.insertEntities(entities)
    }
}