package com.aps.coachfanapp.core.di

import com.aps.coachfanapp.core.framework.datasources.cachedatasources.room.abstraction.*
import com.aps.coachfanapp.core.framework.datasources.cachedatasources.room.implementation.*
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
abstract class RoomDAOsModule {

    @Binds
    abstract fun bindUserDaoService(implementation: UserDaoServiceImpl): UserDaoService

}