package com.aps.coachfanapp.core.util

import com.aps.coachfanapp.common.util.ALog
import com.google.firebase.functions.ktx.functions
import com.google.firebase.ktx.Firebase
import kotlinx.coroutines.tasks.await

private const val TAG = "SecureKeyStorage"
private const val LOG_ME = true

// Hiding keys with NDK:
// https://medium.com/droid-by-me/android-hide-api-keys-in-native-libraries-using-ndk-e1e32c177cd3
// https://www.youtube.com/watch?v=nkYatwVQJFA
// https://stackoverflow.com/questions/49610269/securing-api-key-using-ndk
// https://www.geeksforgeeks.org/securing-api-keys-using-android-ndk/
// Hiding keys from GitHub:
// https://www.youtube.com/watch?v=-2ckvIzs0nU
// https://www.youtube.com/watch?v=X8lYNW_Or2o
class SecureKeyStorage {

    private suspend fun getStringFromFirebase(functionName: String, methodName: String) : String? {
//        val methodName: String = "getStringFromFirebase"
        if (LOG_ME) ALog.d(TAG, "Method start: $methodName")
        var result: String? = null
        try {
            // Firebase functions are declared in index.js
            Firebase.functions
                .getHttpsCallable(functionName)
                .call()
                .continueWith { task ->
                    // This continuation runs on either success or failure, but if the task
                    // has failed then result will throw an Exception which will be
                    // propagated down.
                    val firebaseFunctionResult = task.result?.data as String
                    result = firebaseFunctionResult
                    firebaseFunctionResult
                }
                .await()
        } catch (e: Exception) {
            ALog.e(TAG, methodName, e)
        } finally {
            if (LOG_ME) ALog.d(TAG, "Method end: $methodName")
        }
        return result
    }

    // https://github.com/firebase/functions-samples/tree/main/quickstarts/email-users
    suspend fun getEmailAddressForEmailVerification() : String? {
        return getStringFromFirebase(
            "getEmailAddressForEmailVerification",
        "getEmailAddressForEmailVerification",
        )
    }

    suspend fun getEmailPasswordForEmailVerification() : String? {
        return getStringFromFirebase(
            "getEmailPasswordForEmailVerification",
            "getEmailPasswordForEmailVerification"
        )
    }

    suspend fun getGatewayNameForProject() : String? {
        return getStringFromFirebase(
            "getGatewayNameForProject",
            "getGatewayNameForProject",
        )
    }

    suspend fun getGatewayMerchantID() : String? {
        return getStringFromFirebase(
            "getGatewayMerchantID",
            "getGatewayMerchantID",
        )
    }

    suspend fun getMerchantName() : String? {
        return getStringFromFirebase(
            "getMerchantName",
        "getMerchantName",
        )
    }

    // https://stackoverflow.com/questions/57489351/how-to-get-public-key-in-google-pay-integration-with-direct-token-specification
    suspend fun getDirectTokenizationPublicKey() : String? {
        return getStringFromFirebase(
            "getDirectTokenizationPublicKey",
            "getDirectTokenizationPublicKey",
        )
//            return "przelewy24"
    }

    fun getRetrofit2APIKey(): String {
        // TODO: Change API key to specific, used in project RESTful API.
        return "RETROFIT2_API_KEY_HERE"
    }


    companion object {
        const val STANDARD_OFFLINE_TEST_PERCENTAGE_FEE_FOR_RENTAL = 5L
    }
}