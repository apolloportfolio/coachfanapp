package com.aps.coachfanapp.core.business.domain.model.currencies

import android.os.Parcelable
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass
import kotlinx.android.parcel.Parcelize

@Parcelize
@JsonClass(generateAdapter = true)
data class Rate(
    @field:Json(name = "name") val currency: Currency,
    @field:Json(name = "value") val value: Float
) : Parcelable
