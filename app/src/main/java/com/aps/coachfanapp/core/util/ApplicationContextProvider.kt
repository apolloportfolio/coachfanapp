package com.aps.coachfanapp.core.util

import android.content.Context

interface ApplicationContextProvider {
    fun applicationContext(tag: String?): Context
}