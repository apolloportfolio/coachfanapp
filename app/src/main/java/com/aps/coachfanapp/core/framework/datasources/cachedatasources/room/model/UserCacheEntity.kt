package com.aps.coachfanapp.core.framework.datasources.cachedatasources.room.model

import androidx.annotation.NonNull
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.aps.coachfanapp.R
import com.aps.coachfanapp.common.util.UserUniqueID
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.util.*

@Entity(tableName = "users")
data class UserCacheEntity(
    @PrimaryKey(autoGenerate = false)
    @NonNull
    @SerializedName("id")
    @Expose
    var id: UserUniqueID,

    @SerializedName("updated_at")
    @Expose
    val updated_at: String?,

    @SerializedName("created_at")
    @Expose
    val created_at: String?,

    @SerializedName("emailAddress")
    @Expose
    var emailAddress: String,

    @SerializedName("password")
    @Expose
    var password: String,

    @SerializedName("profilePhotoImageURI")
    @Expose
    var profilePhotoImageURI: String?,

    @SerializedName("name")
    @Expose
    var name: String?,

    @SerializedName("surname")
    @Expose
    var surname: String?,

    @SerializedName("description")
    @Expose
    var description: String?,

    @SerializedName("city")
    @Expose
    var city: String?,

    @SerializedName("emailAddressVerified")
    @Expose
    var emailAddressVerified: Boolean,

    @SerializedName("phoneNumberVerified")
    @Expose
    var phoneNumberVerified: Boolean,

    ) {
    companion object{
        fun nullEmailError(): Int {
            return R.string.you_must_enter_an_email
        }

        fun nullIdError(): Int{
            return R.string.error_null_id
        }

        fun nullPasswordError(): Int {
            return R.string.you_must_enter_a_password
        }
    }
}