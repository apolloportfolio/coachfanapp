package com.aps.coachfanapp.core.di

import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
object RetrofitModule {

//    @Singleton
//    @Provides
//    fun provideGsonBuilder() : Gson {
//        return GsonBuilder()
//                .excludeFieldsWithoutExposeAnnotation()
//                .create();
//    }

/*
    @Singleton
    @Provides
    fun provideRetrofit(gson : Gson) : Retrofit.Builder {
        return Retrofit.Builder()
                .baseUrl("")
                .addConverterFactory(GsonConverterFactory.create(gson))
                .create();
    }


    @Singleton
    @Provides
    fun provideEntity1Retrofit(retrofit : Retrofit.Builder) : Entity1Retrofit {
        return retrofit
                .build()
                .create(Entity1Retrofit::class.java)
    }
     */

}