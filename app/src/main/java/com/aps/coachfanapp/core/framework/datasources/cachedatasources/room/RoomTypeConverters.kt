package com.aps.coachfanapp.core.framework.datasources.cachedatasources.room

import androidx.room.TypeConverter
import com.aps.coachfanapp.common.util.*
import com.google.firebase.Timestamp
import com.google.firebase.firestore.GeoPoint
import java.text.SimpleDateFormat
import java.util.*

private const val TAG = "RoomTypeConverters"
private const val LOG_ME = false

class RoomTypeConverters {
    @TypeConverter
    fun toUserUniqueID(id: String): UserUniqueID {
        var firestoreIdInDocumentIndexStart = 0
        var firestoreIdInDocumentIndexEnd = id.indexOf(STRING_SEPARATOR)
        val firestoreDocumentIDIndex = firestoreIdInDocumentIndexEnd + STRING_SEPARATOR.length
        return if(firestoreIdInDocumentIndexEnd == -1) {
            UserUniqueID(id, id)
        } else {
            UserUniqueID(
                id.substring(firestoreDocumentIDIndex),
                id.substring(firestoreIdInDocumentIndexStart, firestoreIdInDocumentIndexEnd))
        }
    }

    @TypeConverter
    fun fromUserUniqueID(id: UserUniqueID?): String {
        return if(id != null) {
            if(id.firestoreIdInDocument != null) {
                id.firestoreIdInDocument + STRING_SEPARATOR + id.firestoreDocumentID
            } else {
                id.firestoreDocumentID
            }
        } else {
            ""
        }
    }

    @TypeConverter
    fun toUniqueID(id: String): UniqueID {
        return UniqueID(id)
    }

    @TypeConverter
    fun fromUniqueID(id: UniqueID): String {
        return id.firestoreDocumentID
    }

    @TypeConverter
    fun fromDate(value: Long?): Date? {
        return value?.let { Date(it) }
    }

    @TypeConverter
    fun fromDate(date: Date?): Long? {
        return date?.time?.toLong()
    }

    @TypeConverter
    fun fromGregorianCalendar(value: Long?): GregorianCalendar? = value?.let { value ->
        GregorianCalendar().also { calendar ->
            calendar.timeInMillis = value
        }
    }

    @TypeConverter
    fun toGregorianCalendar(timestamp: GregorianCalendar?): Long? = timestamp?.timeInMillis


    @TypeConverter
    fun fromTimestamp(stringTimestamp: String?): Timestamp? {
        if(stringTimestamp == "") return null
        stringTimestamp?.let{
//            val sdf = SimpleDateFormat("yyyy-MM-dd hh:mm:ss a", Locale.ENGLISH)
//            sdf.timeZone = TimeZone.getTimeZone("UTC-7")
            return DateUtil.convertStringDateToFirebaseTimestamp(stringTimestamp)
        }
        return null
    }

    @TypeConverter
    fun toTimestamp(timestamp: Timestamp?): String? {
        timestamp?.let {
            val sdf = SimpleDateFormat("yyyy-MM-dd hh:mm:ss a", Locale.ENGLISH)
            sdf.timeZone = TimeZone.getTimeZone("UTC-7")
            return DateUtil.convertFirebaseTimestampToStringData(timestamp) }
        return null
    }
    
    companion object {
        val STRING_SEPARATOR = "<=>"
    }

    @TypeConverter
    fun toGeoPoint(string: String?): GeoPoint? {
        val methodName: String = "toGeoPoint"
        var result: GeoPoint? = null
        if (LOG_ME) ALog.d(TAG, "Method start: $methodName")
        try {
            if(string != null) {
                val latitudeIndexStart = 0
                val latitudeIndexEnd = string.indexOf(STRING_SEPARATOR)
                val longitudeIndex = latitudeIndexEnd + STRING_SEPARATOR.length
                result = GeoPoint(
                    string.substring(latitudeIndexStart, latitudeIndexEnd).toDouble(),
                    string.substring(longitudeIndex).toDouble()
                )
            } else {
                if (LOG_ME) ALog.w(TAG, "$methodName(): string == null")
            }
        } catch (e: Exception) {
            ALog.e(TAG, methodName, e)
        } finally {
            if (LOG_ME) ALog.d(TAG, "Method end: $methodName")
        }
        return result
    }

    @TypeConverter
    fun fromGeoPoint(geoPoint: GeoPoint?): String? {
        return if(geoPoint != null)"${geoPoint?.latitude}$STRING_SEPARATOR${geoPoint?.longitude}"
        else null
    }

    @TypeConverter
    fun toGeoPointButParcelable(string: String?): ParcelableGeoPoint? {
        val methodName: String = "toGeoPointButParcelable"
        var result: ParcelableGeoPoint? = null
        if (LOG_ME) ALog.d(TAG, "Method start: $methodName")
        try {
            if(string != null) {
                val latitudeIndexStart = 0
                val latitudeIndexEnd = string.indexOf(STRING_SEPARATOR)
                val longitudeIndex = latitudeIndexEnd + STRING_SEPARATOR.length
                result = ParcelableGeoPoint(
                    string.substring(latitudeIndexStart, latitudeIndexEnd).toDouble(),
                    string.substring(longitudeIndex).toDouble()
                )
            } else {
                if (LOG_ME) ALog.w(TAG, "$methodName(): string == null")
            }
        } catch (e: Exception) {
            ALog.e(TAG, methodName, e)
        } finally {
            if (LOG_ME) ALog.d(TAG, "Method end: $methodName")
        }
        return result
    }

    @TypeConverter
    fun fromGeoPointButParcelable(geoPoint: ParcelableGeoPoint?): String? {
        return if(geoPoint != null)"${geoPoint?.latitude}$STRING_SEPARATOR${geoPoint?.longitude}"
        else null
    }
}
