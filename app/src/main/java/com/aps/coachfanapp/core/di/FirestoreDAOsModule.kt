package com.aps.coachfanapp.core.di

import com.aps.coachfanapp.core.framework.datasources.networkdatasources.firebase.abstraction.*
import com.aps.coachfanapp.core.framework.datasources.networkdatasources.firebase.implementation.*
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
abstract class FirestoreDAOsModule {

    @Binds
    abstract fun bindUserFirestoreService(implementation: UserFirestoreServiceImpl): UserFirestoreService


}