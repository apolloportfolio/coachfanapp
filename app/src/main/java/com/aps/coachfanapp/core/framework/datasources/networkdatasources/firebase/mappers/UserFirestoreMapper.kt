package com.aps.coachfanapp.core.framework.datasources.networkdatasources.firebase.mappers

import com.aps.coachfanapp.common.business.domain.model.EntityMapper
import com.aps.coachfanapp.core.business.domain.model.entities.ProjectUser
import com.aps.coachfanapp.core.framework.datasources.networkdatasources.firebase.model.UserFirestoreEntity
import javax.inject.Inject

class UserFirestoreMapper
@Inject
constructor() : EntityMapper<UserFirestoreEntity, ProjectUser> {
    override fun mapFromEntity(entity: UserFirestoreEntity): ProjectUser {
        return ProjectUser(
            entity.id,
            entity.updated_at,
            entity.created_at,
            entity.emailAddress,
            entity.password,
            entity.profilePhotoImageURI,
            entity.name,
            entity.surname,
            entity.description,
            entity.city,
            entity.emailAddressVerified,
            entity.phoneNumberVerified,
        )
    }

    override fun mapToEntity(domainModel: ProjectUser): UserFirestoreEntity {
        return UserFirestoreEntity(
            domainModel.id,
            domainModel.updated_at,
            domainModel.created_at,
            domainModel.emailAddress,
            domainModel.password,
            domainModel.profilePhotoImageURI,
            domainModel.name,
            domainModel.surname,
            domainModel.description,
            domainModel.city,
            domainModel.emailAddressVerified,
            domainModel.phoneNumberVerified,
        )
    }

    override fun mapFromEntityList(entities : List<UserFirestoreEntity>) : List<ProjectUser> {
        return entities.map{mapFromEntity(it)}
    }

    override fun mapToEntityList(entities : List<ProjectUser>) : List<UserFirestoreEntity> {
        return entities.map{mapToEntity(it)}
    }
}