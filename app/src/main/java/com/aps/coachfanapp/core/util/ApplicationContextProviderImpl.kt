package com.aps.coachfanapp.core.util

import android.content.Context
import com.aps.coachfanapp.common.util.ALog
import com.aps.coachfanapp.core.ProjectApplication
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview

private const val TAG = "ApplicationContextProviderImpl"
private const val LOG_ME = true

open class ApplicationContextProviderImpl: ApplicationContextProvider {

    @OptIn(FlowPreview::class, ExperimentalCoroutinesApi::class)
    override fun applicationContext(tag: String?): Context {
        if(LOG_ME) ALog.d(
            TAG, ".applicationContext(): " +
                    "App Context requested by $tag")

        if (LOG_ME) ALog.d(TAG, "Method start: applicationContext()")
        ProjectApplication.applicationContext(tag)?.let {
            return it
        } ?: run {
            throw IllegalStateException("Application context is not available")
        }
    }
}