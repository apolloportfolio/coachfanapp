package com.aps.coachfanapp.core.framework.datasources.networkdatasources.firebase.implementation

import android.net.Uri
import com.aps.coachfanapp.common.business.data.util.cLog
import com.aps.coachfanapp.common.util.ALog
import com.aps.coachfanapp.common.util.DateUtil
import com.aps.coachfanapp.common.util.UniqueID
import com.aps.coachfanapp.common.util.UserUniqueID
import com.aps.coachfanapp.core.business.domain.model.entities.ProjectUser
import com.aps.coachfanapp.core.business.domain.model.entities.ProjectUsersRating
import com.aps.coachfanapp.core.business.domain.model.entities.ProjectUsersRating.CREATOR.FIELD_NAME_AVERAGE_RATING
import com.aps.coachfanapp.core.business.domain.model.entities.ProjectUsersRating.CREATOR.FIELD_NAME_NUMBER_OF_RATINGS
import com.aps.coachfanapp.core.framework.datasources.networkdatasources.firebase.abstraction.UserFirestoreService
import com.aps.coachfanapp.core.framework.datasources.networkdatasources.firebase.mappers.UserFirestoreMapper
import com.aps.coachfanapp.core.framework.datasources.networkdatasources.firebase.model.FirestoreMetaEntity
import com.aps.coachfanapp.core.framework.datasources.networkdatasources.firebase.model.UserFirestoreEntity
import com.aps.coachfanapp.core.util.ProjectConstants
import com.google.firebase.Timestamp
import com.google.firebase.auth.AuthCredential
import com.google.firebase.auth.EmailAuthProvider
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.StorageReference
import com.google.firebase.storage.ktx.storage
import kotlinx.coroutines.tasks.await
import javax.inject.Inject
import javax.inject.Singleton


@Singleton
class UserFirestoreServiceImpl
@Inject
constructor(
//    private val firebaseAuth: FirebaseAuth,          // For some reason (bug) if Hilt injects firestore, calls to database last for ages and never finish.
//    private val firestore: FirebaseFirestore,
    private val networkMapper: UserFirestoreMapper,
    private val dateUtil: DateUtil
): UserFirestoreService,
    BasicFirestoreServiceImpl<
            ProjectUser,
            UserFirestoreEntity,
            UserFirestoreMapper>(
//        firebaseAuth,
//        firestore,
        networkMapper,
        dateUtil
    ) {

    override fun getCollectionName(): String {
        return COLLECTION_NAME
    }

    override fun getDeletesCollectionName(): String {
        return DELETES_COLLECTION_NAME
    }

    override fun setEntityID(entity: ProjectUser, id: UniqueID) {
        if(id is UserUniqueID)entity.id = id else throw IllegalArgumentException("id is not UserUniqueID")
    }

    override fun getEntityID(entity: ProjectUser): UniqueID? {
        return entity.id
    }

    override fun setFirestoreEntityID(entity: UserFirestoreEntity, id: UniqueID) {
        if(id is UserUniqueID)entity.id = id else throw IllegalArgumentException("id is not UserUniqueID")
    }

    override fun updateEntityTimestamp(entity: ProjectUser, timestamp: String){
        entity.updated_at = timestamp
    }

    override fun updateFirestoreEntityTimestamp(entity: UserFirestoreEntity, timestamp: String){
        entity.updated_at = timestamp
    }

    override fun getFirestoreEntityType(): Class<UserFirestoreEntity> {
        return UserFirestoreEntity::class.java
    }

    override fun getFirestoreEntityID(entity: UserFirestoreEntity): UniqueID? {
        return entity.id
    }


    // https://firebase.googleblog.com/2019/03/increment-server-side-cloud-firestore.html
    // https://stackoverflow.com/questions/28915706/auto-increment-a-value-in-firebase
    // https://firebase.google.com/docs/firestore/manage-data/transactions
    // Method inserts or updates User in Firestore while also creating additional document with just phoneNumber
    override suspend fun insertOrUpdateEntity(entity: ProjectUser): ProjectUser? {
        val methodName = "insertOrUpdateEntity"
        if(LOG_ME) ALog.d(TAG, "Method start: $methodName")
        try {
            val networkEntity = networkMapper.mapToEntity(entity)
            val timestampNow = dateUtil.convertFirebaseTimestampToStringData(Timestamp.now())
            updateEntityTimestamp(entity, timestampNow)
            updateFirestoreEntityTimestamp(networkEntity, timestampNow)
            val userID = getEntityID(entity) as UserUniqueID
            if(userID == null) {
                throw NullPointerException("userID == null")
            }

            // All users have their id not null because it is set after registering user in Firebase.
            // However userID.firestoreIdInDocument isn't set until user's entity is added to database.
            if(userID.firestoreIdInDocument == null) {
                if(LOG_ME) ALog.d(TAG, "$methodName(): Inserting new entity")
//                val newlyGeneratedID = firestore.collection(getCollectionName()).document().id
                val metaDoc = firestore.collection(getCollectionName()).document(getMetaDocumentName())
                val newEntityDoc = firestore
                    .collection(getCollectionName())
                    .document(userID.firestoreDocumentID)

                firestore.runTransaction { transaction ->
                    if(LOG_ME)ALog.d(
                        TAG, ".$methodName(): " +
                            "Creating new User ${userID.firestoreDocumentID}")
                    // User entity
                    var newDocumentCount: Double
                    val metaDocSnapshot = transaction.get(metaDoc)
                    if(!metaDocSnapshot.exists()) {
                        val metaEntity = FirestoreMetaEntity()
                        newDocumentCount = 1.0
                        metaEntity.count = newDocumentCount
                        transaction.set(metaDoc, metaEntity)
                    } else {
                        newDocumentCount = metaDocSnapshot.getDouble(getMetaFieldNameWithCount())!!+1
                        transaction.update(metaDoc, getMetaFieldNameWithCount(), newDocumentCount)
                    }

                    userID.apply { firestoreIdInDocument = newDocumentCount.toString() }
                    setEntityID(entity, userID)
                    setFirestoreEntityID(networkEntity, userID)
                    transaction.set(newEntityDoc, networkEntity!!)

                    // Users ratings entity
                    val newUsersRatingEntityDoc = firestore
                        .collection(USERS_RATINGS_COLLECTION_NAME)
                        .document(userID.firestoreDocumentID)
                    val usersRatingEntity = hashMapOf(
                        ProjectUsersRating.FIELD_NAME_AVERAGE_RATING to 0f,
                        ProjectUsersRating.FIELD_NAME_NUMBER_OF_RATINGS to 0,
                    )
                    if(LOG_ME)ALog.d(TAG, ".$methodName(): " +
                            "Creating new entity with user's rating: $usersRatingEntity"
                    )
                    transaction.set(newUsersRatingEntityDoc, usersRatingEntity)

                    null
                }
                    .addOnSuccessListener { if(LOG_ME) ALog.d(TAG, "$methodName(): Successfully set entity.") }
                    .addOnFailureListener {
                        cLog(it.message)
                        ALog.w(TAG, "$methodName(): Failed to set entity.") }
                    .await()
            } else {
                if(LOG_ME) ALog.d(TAG, "$methodName(): Updating existing entity")
                firestore.runTransaction { transaction ->
                    // User entity
                    val userFirestoreID = getEntityID(entity)!!.getFirestoreId()
                    if(LOG_ME)ALog.d(TAG, ".$methodName(): Updating existing User $userFirestoreID")
                    val userDoc = firestore
                        .collection(getCollectionName())
                        .document(userFirestoreID)
                    transaction.set(userDoc, networkEntity)

                    null
                }
                    .addOnSuccessListener { if(LOG_ME) ALog.d(TAG, "$methodName(): Successfully updated user.") }
                    .addOnFailureListener {
                        cLog(it.message)
                        ALog.w(TAG, "$methodName(): Failed to update user.") }
                    .await()
            }
            return entity
        } catch (e: Exception) {
            ALog.e(TAG, methodName, e)
            return null
        } finally {
            if(LOG_ME) ALog.d(TAG, "Method end: $methodName")
        }
    }

    override suspend fun registerNewUser(entity: ProjectUser): FirebaseUser? {
        val methodName: String = "registerNewUser"
        if (LOG_ME) ALog.d(TAG, "Method start: $methodName")
        return try {
            firebaseAuth.createUserWithEmailAndPassword(entity.emailAddress, entity.password)
                .addOnSuccessListener { if (LOG_ME) ALog.d("$TAG", ".$methodName: User register success") }
                .addOnFailureListener {
                    ALog.w("$TAG.$methodName: ", "User register failure: ${it.message}")
                }
                .await()
            firebaseAuth.currentUser
        } catch (e: Exception) {
            ALog.e(TAG, methodName, e)
            null
        } finally {
            if (LOG_ME) ALog.d(TAG, "Method end: $methodName")
        }
    }

    override suspend fun updateUser(entity: ProjectUser):ProjectUser? {
        val methodName: String = "updateUser"
        var result: ProjectUser? = null
        if (LOG_ME) ALog.d(TAG, "Method start: $methodName")
        try {
            if(LOG_ME)ALog.d(TAG, ".$methodName(): " +
                    "${this.hashCode()}\n $this\n${firebaseAuth}\n currentUser = ${firebaseAuth.currentUser}")
            firestore.runBatch { batch ->
                if(firebaseAuth.currentUser != null) {
                    firebaseAuth.currentUser!!
                        .updatePassword(entity.password)
                        .addOnFailureListener {
                            cLog(it.message)
                            ALog.w(TAG, "$methodName(): Failed to update users password.")
                        }
                        .addOnSuccessListener {
                            if(LOG_ME) ALog.d(TAG, "$methodName(): Successfully updated users password.")
                        }
                    firebaseAuth.currentUser!!
                        .updateEmail(entity.emailAddress)
                        .addOnFailureListener {
                            cLog(it.message)
                            ALog.w(TAG, "$methodName(): Failed to update users email.")
                        }
                        .addOnSuccessListener {
                            if(LOG_ME) ALog.d(TAG, "$methodName(): Successfully updated users email.")
                        }
                    firebaseAuth.currentUser!!.let {
                        firebaseAuth
                            .updateCurrentUser(it)
                            .addOnFailureListener {
                                cLog(it.message)
                                ALog.w(TAG, "$methodName(): Failed to update user in firebaseAuth.")
                            }
                            .addOnSuccessListener {
                                if(LOG_ME) ALog.d(TAG, "$methodName(): Successfully updated user in firebaseAuth.")
                            }
                    }
                    val networkEntity = networkMapper.mapToEntity(entity)
                    val timestampNow = dateUtil.convertFirebaseTimestampToStringData(Timestamp.now())
                    updateEntityTimestamp(entity, timestampNow)
                    updateFirestoreEntityTimestamp(networkEntity, timestampNow)
                    firestore
                        .collection(getCollectionName())
                        .document(getEntityID(entity)!!.getFirestoreId())
                        .set(networkEntity)
                        .addOnFailureListener {
                            // send error reports to Firebase Crashlytics
                            cLog(it.message)
                            ALog.w(TAG, "$methodName(): Failed to set entity.")
                        }
                        .addOnSuccessListener {
                            if(LOG_ME) ALog.d(TAG, "$methodName(): Successfully set entity.")
                        }
                    result = entity
                } else {
                    ALog.w(TAG, ".$methodName(): firebaseAuth.currentUser == null")
                }
            }.await()
            return result
        } catch (e: Exception) {
            ALog.e(TAG, methodName, e)
            return null
        } finally {
            if (LOG_ME) ALog.d(TAG, "Method end: $methodName")
        }
    }

    override suspend fun loginUser(entity: ProjectUser): FirebaseUser? {
        val methodName: String = "loginUser"
        if (LOG_ME) ALog.d(TAG, "Method start: $methodName")
        return try {
            val auth = firebaseAuth
                .signInWithEmailAndPassword(entity.emailAddress, entity.password)
                .addOnSuccessListener { if (LOG_ME) ALog.d("$TAG.$methodName: ", "User login success") }
                .addOnFailureListener { ALog.w("$TAG.$methodName: ", "User login failure") }
                .await()

            if(LOG_ME)ALog.d(TAG, ".$methodName(): currentUser == ${firebaseAuth.currentUser}")
            firebaseAuth.currentUser
        } catch (e: Exception) {
            ALog.e(TAG, methodName, e)
            null
        } finally {
            if(LOG_ME)ALog.d(TAG, ".$methodName(): ${this.hashCode()}\n $this\n${firebaseAuth}\n currentUser = ${firebaseAuth.currentUser}")
            if (LOG_ME) ALog.d(TAG, "Method end: $methodName")
        }
    }

    override suspend fun logoutUser(entity: ProjectUser) {
        val methodName: String = "logoutUser"
        if (LOG_ME) ALog.d(TAG, "Method start: $methodName")
        try {
            FirebaseAuth.getInstance().signOut()
        } catch (e: Exception) {
            ALog.e(TAG, methodName, e)
        } finally {
            if (LOG_ME) ALog.d(TAG, "Method end: $methodName")
        }
    }

    override suspend fun checkIfEmailExistsInRepository(email: String?): Boolean {
        val methodName: String = "checkIfEmailExistsInRepository"
        var result = false
        if (LOG_ME) ALog.d(TAG, "Method start: $methodName")
        try {
            result = firestore
                .collection(getCollectionName())
                .whereEqualTo("email", email)
                .get()
                .addOnFailureListener {
                    // send error reports to Firebase Crashlytics
                    cLog(it.message)
                    ALog.w(TAG, "$methodName(): Failed to check if email is in repository.")
                }
                .addOnSuccessListener {
                    if(LOG_ME) ALog.d(TAG, "$methodName(): Successfully checked if email is in repository.")
                }
                .await()
                .toObjects(getFirestoreEntityType())
                .isNotEmpty()
            return result
        } catch (e: Exception) {
            ALog.e(TAG, methodName, e)
            return false
        } finally {
            if (LOG_ME) ALog.d(TAG, "Method end: $methodName")
        }
    }

    override suspend fun checkIfPasswordMatchesEmail(email: String?, password: String?): UserUniqueID? {
        val methodName: String = "checkIfPasswordMatchesEmail"
        if (LOG_ME) ALog.d(TAG, "Method start: $methodName")
        try {
            val entityArray = firestore
                .collection(getCollectionName())
                .whereEqualTo("email", email)
                .whereEqualTo("password", password)
                .get()
                .addOnFailureListener {
                    // send error reports to Firebase Crashlytics
                    cLog(it.message)
                    ALog.w(TAG, "$methodName(): Failed to check if email is in repository.")
                }
                .addOnSuccessListener {
                    if(LOG_ME) ALog.d(TAG, "$methodName(): Successfully checked if email is in repository.")
                }
                .await()
                .toObjects(getFirestoreEntityType())
            if(entityArray.isNotEmpty())return networkMapper.mapFromEntity(entityArray[0]).id!!
            return UserUniqueID.INVALID_ID
        } catch (e: Exception) {
            ALog.e(TAG, methodName, e)
            return UserUniqueID.INVALID_ID
        } finally {
            if (LOG_ME) ALog.d(TAG, "Method end: $methodName")
        }
    }

    override suspend fun getEntityByCredentials(email: String?, password: String?): ProjectUser? {
        val methodName: String = "getEntityByCredentials"
        if (LOG_ME) ALog.d(TAG, "Method start: $methodName")
        try {
            val entityArray = firestore
                .collection(getCollectionName())
                .whereEqualTo("email", email)
                .whereEqualTo("password", password)
                .get()
                .addOnFailureListener {
                    // send error reports to Firebase Crashlytics
                    cLog(it.message)
                    ALog.w(TAG, "$methodName(): Failed to get entity by credentials.")
                }
                .addOnSuccessListener {
                    if(LOG_ME) {
                        ALog.d(TAG, "$methodName(): Successfully got entity by credentials.")
                        val entityType = getFirestoreEntityType()
                        val entityArray = it.toObjects(entityType)
                        ALog.d(TAG, "$methodName(): entityArray == $entityArray")
                        for((i, entity) in entityArray.withIndex()) {
                            ALog.d(TAG, "$methodName(): entity nr$i: $entity")
                        }
                    }
                }
                .await()
                .toObjects(getFirestoreEntityType())
            if(entityArray.isNotEmpty())return networkMapper.mapFromEntity(entityArray[0])
            return null
        } catch (e: Exception) {
            ALog.e(TAG, methodName, e)
            return null
        } finally {
            if (LOG_ME) ALog.d(TAG, "Method end: $methodName")
        }
    }

    // https://stackoverflow.com/questions/38114689/how-to-delete-a-firebase-user-from-android-app
    override suspend fun deleteAccount(entity: ProjectUser) {
        val methodName: String = "deleteAccount"
        if (LOG_ME) ALog.d(TAG, "Method start: $methodName")
        try {
            val firebaseUser : FirebaseUser? = FirebaseAuth.getInstance().currentUser;
            if(firebaseUser != null) {
                val credential : AuthCredential =
                    EmailAuthProvider.getCredential(entity.emailAddress, entity.password)

                // Prompt the user to re-provide their sign-in credentials
                firebaseUser.reauthenticate(credential)
                    .addOnFailureListener {
                        cLog(it.message)
                        ALog.d(TAG, "$methodName(): Unsuccessfully re-authenticated firebaseUser.")
                    }
                    .addOnSuccessListener {
                        if(LOG_ME) ALog.d(TAG, "$methodName(): Successfully re-authenticated firebaseUser.")
                    }
                    .await()
                firebaseUser.delete()
                    .addOnFailureListener {
                        cLog(it.message)
                        ALog.d(TAG, "$methodName(): Unsuccessfully deleted firebaseUser.")
                    }
                    .addOnSuccessListener {
                        if(LOG_ME)ALog.d(TAG, "$methodName(): Successfully deleted firebaseUser.")
                    }
                    .await()
            } else {
                if (LOG_ME) ALog.w(TAG, "$methodName(): firebaseUser == null")
            }
        } catch (e: Exception) {
            ALog.e(TAG, methodName, e)
        } finally {
            if (LOG_ME) ALog.d(TAG, "Method end: $methodName")
        }
    }

    // https://firebase.google.com/docs/storage/android/upload-files#kotlin+ktx
    override suspend fun uploadUsersProfilePhotoToFirestore(
        user : ProjectUser,
        usersProfilePhotoUri : Uri,
    ) : String? {
        val methodName: String = "uploadUsersProfilePhotoToFirestore"
        if (LOG_ME) ALog.d(TAG, "Method start: $methodName")
        var result : String? = null
        var profilePictureRef: StorageReference? = null
        try {
            val storage = Firebase.storage
            val storageRef = storage.reference
            if(usersProfilePhotoUri.path != null) {
                profilePictureRef = storageRef.child(
                    "${ProjectConstants.FIRESTORE_IMAGES_COLLECTION}/" +
                            "${ProjectConstants.FIRESTORE_PROFILE_PHOTOS_SUB_COLLECTION}/" +
                            user.id!!.firestoreDocumentID
                )
                val uploadTask = profilePictureRef.putFile(usersProfilePhotoUri)

                val urlTask = uploadTask.continueWithTask { task ->
                    if (!task.isSuccessful) {
                        task.exception?.let {
                            throw it
                        }
                    }
                    profilePictureRef.downloadUrl
                }.addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        if(LOG_ME)ALog.d(
                            TAG, ".$methodName(): Upload successful. " +
                                "task.result.path == ${task.result.path}")
                    } else {
                        ALog.e(TAG, ".$methodName(): Failed to upload user's profile picture.")
                    }
                }.await()

                result = user.id!!.firestoreDocumentID
            } else {
                ALog.e(TAG, ".$methodName(): usersProfilePhotoUri.path == null")
            }
        } catch (e: Exception) {
            ALog.e(TAG, methodName, e)
        } finally {
            if(LOG_ME)ALog.d(
                TAG, "Method end: $methodName: " +
                    "\n usersProfilePhotoUri == $usersProfilePhotoUri" +
                    "\n profilePictureRef == $profilePictureRef" +
                    "\n result == $result")
        }
        return result
    }

    // https://firebase.google.com/docs/storage/android/upload-files#kotlin+ktx
    override suspend fun uploadUsersProfilePhotoToFirestore(
        userID : UserUniqueID,
        usersProfilePhotoUri : Uri,
    ) : String? {
        val methodName: String = "uploadUsersProfilePhotoToFirestore"
        if (LOG_ME) ALog.d(TAG, "Method start: $methodName")
        var result : String? = null
        var profilePictureRef: StorageReference? = null
        try {
            val storage = Firebase.storage
            val storageRef = storage.reference
            if(usersProfilePhotoUri.path != null) {
                profilePictureRef = storageRef.child(
                    "${ProjectConstants.FIRESTORE_IMAGES_COLLECTION}/" +
                            "${ProjectConstants.FIRESTORE_PROFILE_PHOTOS_SUB_COLLECTION}/" +
                            userID.firestoreDocumentID
                )
                val uploadTask = profilePictureRef.putFile(usersProfilePhotoUri)

                val urlTask = uploadTask.continueWithTask { task ->
                    if (!task.isSuccessful) {
                        task.exception?.let {
                            throw it
                        }
                    }
                    profilePictureRef.downloadUrl
                }.addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        if(LOG_ME)ALog.d(
                            TAG, ".$methodName(): Upload successful." +
                                "task.result.path == ${task.result.path}")
                        result = task.result.path
                    } else {
                        ALog.e(TAG, ".$methodName(): Failed to upload user's profile picture.")
                    }
                }.await()
                result = urlTask.path
            } else {
                ALog.e(TAG, ".$methodName(): usersProfilePhotoUri.path == null")
            }
        } catch (e: Exception) {
            ALog.e(TAG, methodName, e)
        } finally {
            if(LOG_ME)ALog.d(
                TAG, "Method end: $methodName: " +
                    "\n usersProfilePhotoUri == $usersProfilePhotoUri" +
                    "\n profilePictureRef == $profilePictureRef" +
                    "\n result == $result")
        }
        return result
    }

    // Downloads user's rating by using a subcollection that contains only ratings
    override suspend fun getUsersRating(userID: UserUniqueID): ProjectUsersRating? {
        val methodName: String = "getUsersRating"
        if (LOG_ME) ALog.d(TAG, "Method start: $methodName")
        var usersRating: ProjectUsersRating? = null
        try {
            val usersRatingDocumentReference =
                firestore
                    .collection(USERS_RATINGS_COLLECTION_NAME)
                    .document(userID.getFirestoreId())
            if(LOG_ME)ALog.d(TAG, "$methodName(): " +
                    "userID.getFirestoreId() == ${userID.getFirestoreId()}\n" +
                    "usersRatingDocumentReference.path == ${usersRatingDocumentReference.path}")

            val result = usersRatingDocumentReference
                .get()
                .addOnFailureListener {
                    // send error reports to Firebase Crashlytics
                    cLog(it.message)
                    ALog.w(TAG, "$methodName(): Failed to get user's rating number.")
                }
                .addOnSuccessListener { documentSnapshot ->
                    if(LOG_ME)ALog.d(TAG, "$methodName(): " +
                            "Successfully got user's rating.")
                    if(documentSnapshot == null)if(LOG_ME)ALog.d(TAG, ".getUsersRating(): " +
                            "documentSnapshot == null")
                    documentSnapshot?.let {
                        ALog.w(TAG, "$methodName(): Successfully got user's rating number.")
                    } ?: kotlin.run {
                        ALog.e(TAG, ".$methodName(): documentSnapshot == null")
                    }
                }
                .await()
            val averageRating = result.getDouble(FIELD_NAME_AVERAGE_RATING)
            val numberOfRatings = result.get(FIELD_NAME_NUMBER_OF_RATINGS)
            val usersRatingHashMap: HashMap<String, String?> = hashMapOf(
                FIELD_NAME_AVERAGE_RATING to averageRating.toString(),
                FIELD_NAME_NUMBER_OF_RATINGS to numberOfRatings.toString(),
            )
            usersRating = ProjectUsersRating(
                userID,
                usersRatingHashMap,
            )
            if(LOG_ME)ALog.d(TAG, ".getUsersRating(): " +
                    "usersRating == $usersRating")
        } catch (e: java.lang.Exception) {
            ALog.e(TAG, methodName, e)
        } finally {
            if (LOG_ME) ALog.d(TAG, "Method end: $methodName")
        }
        return usersRating
    }

    override suspend fun insertUsersRating(usersRating: ProjectUsersRating) {
        val methodName: String = "insertUsersRating"
        if (LOG_ME) ALog.d(TAG, "Method start: $methodName")
        try {
            val usersRatingEntityReference = firestore
                .collection(USERS_RATINGS_COLLECTION_NAME)
                .document(usersRating.usersID.firestoreDocumentID)
            val usersRatingEntity = usersRating.usersRatingEntity
            if(LOG_ME)ALog.d(TAG, ".$methodName(): " +
                    "Creating new entity with user's ratings: $usersRatingEntity"
            )

            usersRatingEntityReference
                .set(usersRatingEntity)
                .addOnFailureListener {
                    // send error reports to Firebase Crashlytics
                    cLog(it.message)
                    ALog.w(TAG, "$methodName(): Failed to set entity: ${it.message}")
                }
                .addOnSuccessListener {
                    if(LOG_ME) ALog.d(TAG, "$methodName(): Successfully set entity.")
                }
                .await()
        } catch (e: java.lang.Exception) {
            ALog.e(TAG, methodName, e)
        } finally {
            if (LOG_ME) ALog.d(TAG, "Method end: $methodName")
        }
    }

    companion object {
        const val TAG = "UserFirestoreServiceImpl"
        const val LOG_ME = true
        const val COLLECTION_NAME = "user"
        const val DELETES_COLLECTION_NAME = "user_d"
        const val PHONE_NUMBERS_COLLECTION_NAME = "phones"
        const val USERS_RATINGS_COLLECTION_NAME = "usersRatings"
    }
}