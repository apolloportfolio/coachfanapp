package com.aps.coachfanapp.core.framework.datasources.cachedatasources.room.daos

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.aps.coachfanapp.common.util.UniqueID
import com.aps.coachfanapp.common.util.UserUniqueID
import com.aps.coachfanapp.core.framework.datasources.cachedatasources.room.model.UserCacheEntity
import java.util.*

const val ORDER_ASC: String = ""
const val ORDER_DESC: String = "-"
const val FILTER_TITLE = "title"
const val FILTER_DATE_CREATED = "created_at"

const val ORDER_BY_ASC_DATE_UPDATED = ORDER_ASC + FILTER_DATE_CREATED
const val ORDER_BY_DESC_DATE_UPDATED = ORDER_DESC + FILTER_DATE_CREATED
const val ORDER_BY_ASC_TITLE = ORDER_ASC + FILTER_TITLE
const val ORDER_BY_DESC_TITLE = ORDER_DESC + FILTER_TITLE
const val PAGINATION_PAGE_SIZE = 30


@Dao
interface UserDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(entity : UserCacheEntity) : Long

    @Query("SELECT * FROM users")
    suspend fun get() : List<UserCacheEntity>
    
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insertEntities(entities: List<UserCacheEntity>): LongArray

    @Query("SELECT * FROM users WHERE id = :id")
    suspend fun getEntityById(id: UniqueID?): UserCacheEntity?

    @Query("DELETE FROM users WHERE id IN (:ids)")
    suspend fun deleteEntities(ids: List<UniqueID?>): Int

    @Query("DELETE FROM users")
    suspend fun deleteAllEntities()

    @Query("SELECT * FROM users")
    suspend fun getAllEntities(): List<UserCacheEntity>

    @Query("""
        UPDATE users 
        SET 
        updated_at = :updated_at,
        created_at = :created_at,
        emailAddress = :emailAddress,
        password = :password,
        profilePhotoImageURI = :profilePhotoImageURI,
        name = :name,
        surname = :surname,
        description = :description,
        city = :city,
        emailAddressVerified = :emailAddressVerified,
        phoneNumberVerified = :phoneNumberVerified
        WHERE id = :id
        """)
    suspend fun updateEntity(
        id: UserUniqueID?,
        updated_at: String?,
        created_at: String?,
        emailAddress: String,
        password: String,
        profilePhotoImageURI: String?,
        name: String?,
        surname: String?,
        description: String?,
        city: String?,
        emailAddressVerified: Boolean,
        phoneNumberVerified: Boolean,
    ): Int

    @Query("UPDATE users SET updated_at = :updated_at, emailAddress = :emailAddress WHERE id = :id")
    suspend fun updateUsersEmail(
        id: UniqueID?,
        updated_at: String?,
        emailAddress: String?,
    ): Int

    @Query("DELETE FROM users WHERE id = :primaryKey")
    suspend fun deleteEntity(primaryKey: UniqueID?): Int

    @Query("SELECT * FROM users")
    suspend fun searchEntities(): List<UserCacheEntity>
    
    @Query("SELECT COUNT(*) FROM users")
    suspend fun getNumEntities(): Int
    
    suspend fun returnOrderedQuery(
        query: String,
        filterAndOrder: String,
        page: Int
    ): List<UserCacheEntity> {

        when {
            filterAndOrder.contains(ORDER_BY_DESC_DATE_UPDATED) -> {
                return searchEntitiesOrderByDateDESC(
                    query = query,
                    page = page
                )
            }

            filterAndOrder.contains(ORDER_BY_ASC_DATE_UPDATED) -> {
                return searchEntitiesOrderByDateASC(
                    query = query,
                    page = page
                )
            }

            else ->
                return searchEntitiesOrderByDateDESC(
                    query = query,
                    page = page
                )
        }
    }

    
    
    @Query("""
        SELECT * FROM users
        WHERE surname LIKE '%' || :query || '%' 
        ORDER BY updated_at DESC LIMIT (:page * :pageSize)
        """)
    suspend fun searchEntitiesOrderByDateDESC(
        query: String,
        page: Int,
        pageSize: Int = PAGINATION_PAGE_SIZE
    ): List<UserCacheEntity>

    @Query("""
        SELECT * FROM users
        WHERE surname LIKE '%' || :query || '%' 
        ORDER BY updated_at ASC LIMIT (:page * :pageSize)
        """)
    suspend fun searchEntitiesOrderByDateASC(
        query: String,
        page: Int,
        pageSize: Int = PAGINATION_PAGE_SIZE
    ): List<UserCacheEntity>
}