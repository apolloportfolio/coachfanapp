package com.aps.coachfanapp.core.util

class Config {

    companion object {
        const val APP_NAME = "Coach Fan App"
        const val LOGGING = true
        const val LOGGING_TAG = "ALog"
        const val DEBUGGING_MODE = false
    }
}