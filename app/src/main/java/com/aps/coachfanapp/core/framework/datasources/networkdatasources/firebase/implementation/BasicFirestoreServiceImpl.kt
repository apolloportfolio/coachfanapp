package com.aps.coachfanapp.core.framework.datasources.networkdatasources.firebase.implementation

import com.aps.coachfanapp.common.business.data.util.cLog
import com.aps.coachfanapp.common.business.domain.model.EntityMapper
import com.aps.coachfanapp.common.util.ALog
import com.aps.coachfanapp.common.util.DateUtil
import com.aps.coachfanapp.common.util.UniqueID
import com.aps.coachfanapp.core.framework.datasources.networkdatasources.firebase.abstraction.BasicFirestoreService
import com.aps.coachfanapp.core.framework.datasources.networkdatasources.firebase.model.FirestoreMetaEntity
import com.google.firebase.Timestamp
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.coroutines.tasks.await
import javax.inject.Singleton

/**
 * Firestore doc refs:
 * 1. add:  https://firebase.google.com/docs/firestore/manage-data/add-data
 * 2. delete: https://firebase.google.com/docs/firestore/manage-data/delete-data
 * 3. update: https://firebase.google.com/docs/firestore/manage-data/add-data#update-data
 * 4. query: https://firebase.google.com/docs/firestore/query-data/queries
 * https://codinginflow.com/tutorials/android/cloud-firestore/part-2-preparations-set-document
 * https://www.youtube.com/watch?v=35RlydUf6xo
 * https://firebase.google.com/docs/auth
 * https://firebase.google.com/docs/firestore/solutions/geoqueries
 */

private const val TAG = "BasicFirestoreServiceImpl"
private const val LOG_ME = true
@Singleton
abstract class BasicFirestoreServiceImpl<EntityType, FirestoreEntityType, NetworkMapper: EntityMapper<FirestoreEntityType, EntityType>>
constructor(
//    private val firebaseAuth: FirebaseAuth,
//    private val firestore: FirebaseFirestore,
    private val networkMapper: NetworkMapper,
    private val dateUtil: DateUtil
): BasicFirestoreService<EntityType, FirestoreEntityType, NetworkMapper> {
    val firebaseAuth: FirebaseAuth = FirebaseAuth.getInstance()
    val firestore: FirebaseFirestore = FirebaseFirestore.getInstance()

    override fun getMetaDocumentName() = getCollectionName() + META_DOCUMENT_SUFIX
    override fun getMetaFieldNameWithCount() = FirestoreMetaEntity.META_FIELD_NAME_COUNT

    override suspend fun insertOrUpdateEntity(entity: EntityType): EntityType? {
        val methodName = "insertOrUpdateEntity"
        if(LOG_ME) ALog.d(TAG, "Method start: $methodName")
        try {
            val networkEntity = networkMapper.mapToEntity(entity)
            val timestampNow = dateUtil.convertFirebaseTimestampToStringData(Timestamp.now())
            updateEntityTimestamp(entity, timestampNow)
            updateFirestoreEntityTimestamp(networkEntity, timestampNow)
            if(getEntityID(entity) == null) {
                val newlyGeneratedID = firestore.collection(getCollectionName()).document().id
                val entityId = UniqueID(newlyGeneratedID)
                setEntityID(entity, entityId)
                setFirestoreEntityID(networkEntity, entityId)
                if(LOG_ME)ALog.d(
                    TAG, ".$methodName(): entity's id is null,\n" +
                        "newlyGeneratedID == $newlyGeneratedID\n" +
                        "entityId == $entityId\n" +
                        "entity == $entity\n" +
                        "networkEntity == $networkEntity")
            }
            firestore
                .collection(getCollectionName())
                .document(getEntityID(entity)!!.getFirestoreId())
                .set(networkEntity!!)
                .addOnFailureListener {
                    // send error reports to Firebase Crashlytics
                    cLog(it.message)
                    ALog.w(TAG, "$methodName(): Failed to set entity.")
                }
                .addOnSuccessListener {
                    if(LOG_ME) ALog.d(TAG, "$methodName(): Successfully set entity.")
                }
                .await()
            return entity
        } catch (e: Exception) {
            ALog.e(TAG, methodName, e)
            return null
        } finally {
            if(LOG_ME) ALog.d(TAG, "Method end: $methodName")
        }
    }

    override suspend fun deleteEntity(primaryKey: UniqueID?) {
        val methodName: String = "deleteEntity"
        ALog.d(TAG, "Method start: $methodName")
        try {
            firestore
                .collection(getCollectionName())
                .document(primaryKey!!.getFirestoreId())
                .delete()
                .addOnFailureListener {
                    // send error reports to Firebase Crashlytics
                    cLog(it.message)
                    ALog.w(TAG, "$methodName(): Failed to delete entity.")
                }
                .addOnSuccessListener {
                    if(LOG_ME)ALog.d(TAG, "$methodName(): Successfully deleted entity.")
                }
                .await()
        } catch (e: Exception) {
            ALog.e(TAG, methodName, e)
            return
        } finally {
            ALog.d(TAG, "Method end: $methodName")
        }
    }

    override suspend fun insertDeletedEntity(entity: EntityType) {
        val methodName: String = "insertDeletedEntity"
        ALog.d(TAG, "Method start: $methodName")
        try {
            val networkEntity = networkMapper.mapToEntity(entity)
            firestore
                .collection(getDeletesCollectionName())
                .document(getEntityID(entity)!!.getFirestoreId())
                .set(networkEntity!!)
                .addOnFailureListener {
                    // send error reports to Firebase Crashlytics
                    cLog(it.message)
                    ALog.w(TAG, "$methodName(): Failed to insert deleted entity.")
                }
                .addOnSuccessListener {
                    if(LOG_ME)ALog.d(TAG, "$methodName(): Successfully inserted deleted entity.")
                }
                .await()
        } catch (e: Exception) {
            ALog.e(TAG, methodName, e)
            return
        } finally {
            ALog.d(TAG, "Method end: $methodName")
        }
    }

    suspend fun insertDeletedEntites(entities: List<EntityType>) {
        val methodName: String = "insertDeletedEntites"
        ALog.d(TAG, "Method start: $methodName")
        try {
            if(entities.size > 500){
                throw Exception("Cannot delete more than 500 entities at a time in firestore.")
            }

            val collectionRef = firestore
                .collection(getDeletesCollectionName())

            firestore.runBatch { batch ->
                for(entity in entities){
                    val documentRef = collectionRef.document(getEntityID(entity)!!.getFirestoreId())
                    batch.set(documentRef, networkMapper.mapToEntity(entity)!!)
                }
            }
            .addOnFailureListener {
                // send error reports to Firebase Crashlytics
                cLog(it.message)
                ALog.w(TAG, "$methodName(): Failed to insert deleted entities.")
            }
            .addOnSuccessListener {
                if(LOG_ME)ALog.d(TAG, "$methodName(): Successfully inserted deleted entities.")
            }
            .await()
        } catch (e: Exception) {
            ALog.e(TAG, methodName, e)
            return
        } finally {
            ALog.d(TAG, "Method end: $methodName")
        }
    }

    override suspend fun deleteDeletedEntity(entity: EntityType) {
        val methodName: String = "deleteDeletedEntity"
        ALog.d(TAG, "Method start: $methodName")
        try {
            firestore
                .collection(getDeletesCollectionName())
                .document(getEntityID(entity)!!.getFirestoreId())
                .delete()
                .addOnFailureListener {
                    // send error reports to Firebase Crashlytics
                    cLog(it.message)
                    ALog.w(TAG, "$methodName(): Failed to delete deleted entity.")
                }
                .addOnSuccessListener {
                    if(LOG_ME)ALog.d(TAG, "$methodName(): Successfully inserted deleted entities.")
                }
                .await()
        } catch (e: Exception) {
            ALog.e(TAG, methodName, e)
            return
        } finally {
            ALog.d(TAG, "Method end: $methodName")
        }
    }

    // For testing purposes only
    override suspend fun deleteAllEntities() {
        val methodName: String = "deleteAllEntities"
        ALog.d(TAG, "Method start: $methodName")
        try {
            val entitiesList = this.getAllEntities()
            val deletedEntitiesList = this.getAllDeletedEntities()

            firestore.runBatch { batch ->
                for(entity in entitiesList){
                    val entityDocumentReference = firestore.document(getCollectionName() + "/" + getEntityID(entity)!!.getFirestoreId())
                    batch.delete(entityDocumentReference)
                }
                for(entity in deletedEntitiesList){
                    val entityDocumentReference = firestore.document(getDeletesCollectionName() + "/" + getEntityID(entity)!!.getFirestoreId())
                    batch.delete(entityDocumentReference)
                }
            }
            .addOnFailureListener {
                // send error reports to Firebase Crashlytics
                cLog(it.message)
                ALog.w(TAG, "$methodName(): Failed to delete all entities.")
            }
            .addOnSuccessListener {
                    if(LOG_ME)ALog.d(TAG, "$methodName(): Successfully  deleted all entities.")
            }
            .await()
        } catch (e: Exception) {
            ALog.e(TAG, methodName, e)
            return
        } finally {
            ALog.d(TAG, "Method end: $methodName")
        }
    }

    override suspend fun getAllDeletedEntities(): List<EntityType> {
        val methodName: String = "getAllDeletedEntities"
        var result: List<EntityType>
        ALog.d(TAG, "Method start: $methodName")
        try {
            result = networkMapper.mapFromEntityList(
                firestore
                    .collection(getDeletesCollectionName())
                    .get()
                    .addOnFailureListener {
                        // send error reports to Firebase Crashlytics
                        cLog(it.message)
                        ALog.w(TAG, "$methodName(): Failed to get all deleted entities.")
                    }
                    .addOnSuccessListener {
                        if(LOG_ME)ALog.d(TAG, "$methodName(): Successfully got all deleted entities.")
                    }
                    .await()
                    .toObjects(getFirestoreEntityType())
            )
            return result
        } catch (e: Exception) {
            ALog.e(TAG, methodName, e)
            return emptyList()
        } finally {
            ALog.d(TAG, "Method end: $methodName")
        }
    }

    override suspend fun searchEntity(entity: EntityType): EntityType? {
        val methodName: String = "searchEntity"
        var result: EntityType?
        ALog.d(TAG, "Method start: $methodName")
        try {
            result = firestore
                .collection(getCollectionName())
                .document(getEntityID(entity)!!.getFirestoreId())
                .get()
                .addOnFailureListener {
                    // send error reports to Firebase Crashlytics
                    cLog(it.message)
                    ALog.w(TAG, "$methodName(): Failed to search entities.")
                }
                .addOnSuccessListener {
                    if(LOG_ME)ALog.d(TAG, "$methodName(): Successfully search entities.")
                }
                .await()
                .toObject(getFirestoreEntityType())?.let {
                    networkMapper.mapFromEntity(it)
                }
            return result
        } catch (e: Exception) {
            ALog.e(TAG, methodName, e)
            return null
        } finally {
            ALog.d(TAG, "Method end: $methodName")
        }
    }

    override suspend fun getAllEntities(): List<EntityType> {
        val methodName: String = "getAllEntities"
        var result: List<EntityType>
        ALog.d(TAG, "Method start: $methodName")
        try {
            result = networkMapper.mapFromEntityList(
                firestore
                    .collection(getCollectionName())
                    .get()
                    .addOnFailureListener {
                        // send error reports to Firebase Crashlytics
                        cLog(it.message)
                        ALog.w(TAG, "$methodName(): Failed to get all entities.")
                    }
                    .addOnSuccessListener {
                        if(LOG_ME)ALog.d(TAG, "$methodName(): Successfully got all entities.")
                    }
                    .await()
                    .toObjects(getFirestoreEntityType())
            )
            return result
        } catch (e: Exception) {
            ALog.e(TAG, methodName, e)
            return emptyList()
        } finally {
            ALog.d(TAG, "Method end: $methodName")
        }
    }

    override suspend fun insertOrUpdateEntities(entities: List<EntityType>): List<EntityType>? {
        val methodName: String = "insertOrUpdateEntities"
        ALog.d(TAG, "Method start: $methodName")
        var result: List<EntityType>? = null
        try {
            if(entities.size > 500){
                throw Exception("Cannot insert more than 500 entities at a time into firestore.")
            }

            val collectionRef = firestore.collection(getCollectionName())

            firestore.runBatch { batch ->
                ALog.d(TAG, "Running batch to insert or update entities.")
                for((i, entity) in entities.withIndex()){
                    if(getEntityID(entity) == null) {
                        val newlyGeneratedID = firestore.collection(getCollectionName()).document().id
                        val entityId = UniqueID(newlyGeneratedID)
                        setEntityID(entity, entityId)
                        if(LOG_ME)ALog.d(
                            TAG, ".$methodName(): Entity #$i: entity's id is null,\n" +
                                "newlyGeneratedID == $newlyGeneratedID\n" +
                                "entityId == $entityId\n" +
                                "entity == $entity\n")
                    }
                    val firestoreEntity = networkMapper.mapToEntity(entity)
                    updateFirestoreEntityTimestamp(firestoreEntity, dateUtil.convertFirebaseTimestampToStringData(Timestamp.now()))
                    val documentRef = collectionRef.document(getFirestoreEntityID(firestoreEntity)!!.getFirestoreId())
                    batch.set(documentRef, firestoreEntity!!)
                }
            }
            .addOnFailureListener {
                // send error reports to Firebase Crashlytics
                cLog(it.message)
                ALog.w(TAG, "$methodName(): Failed to insert or update entities.")
            }
            .addOnSuccessListener {
                if(LOG_ME)ALog.d(TAG, "$methodName(): Successfully inserted or updated all entities.")
                result = entities
            }
            .await()
            if(LOG_ME)ALog.d(TAG, "Awaiting")
        } catch (e: Exception) {
            ALog.e(TAG, methodName, e)
        } finally {
            ALog.d(TAG, "Method end: $methodName")
        }
        return result
    }

    override suspend fun insertDeletedEntities(entities: List<EntityType>) {
        val methodName: String = "insertDeletedEntities"
        if (LOG_ME) ALog.d(TAG, "Method start: $methodName")
        try {
            if(entities.size > 500){
                throw Exception("Cannot delete more than 500 notes at a time in firestore.")
            }

            val collectionRef = firestore.collection(getDeletesCollectionName())

            firestore.runBatch { batch ->
                for(entity in entities){
                    val documentRef = collectionRef.document(getEntityID(entity)!!.getFirestoreId())
                    batch.set(documentRef, networkMapper.mapToEntity(entity)!!)
                }
            }
            .addOnFailureListener {
                // send error reports to Firebase Crashlytics
                cLog(it.message)
                ALog.w(TAG, "$methodName(): Failed to insert all deleted entities.")
            }
            .addOnSuccessListener {
                if(LOG_ME)ALog.d(TAG, "$methodName(): Successfully inserted all deleted entities.")
            }
            .await()
        } catch (e: Exception) {
            ALog.e(TAG, methodName, e)
            return
        } finally {
            if (LOG_ME) ALog.d(TAG, "Method end: $methodName")
        }
    }

    override suspend fun getEntityById(id: UniqueID): EntityType? {
        val methodName: String = "getEntityById"
        var result: EntityType?
        if (LOG_ME) ALog.d(TAG, "Method start: $methodName")
        try {
            result = firestore
                .collection(getCollectionName())
                .document(id.getFirestoreId())
                .get()
                .addOnFailureListener {
                    // send error reports to Firebase Crashlytics
                    cLog(it.message)
                    ALog.w(TAG, "$methodName(): Failed to get entity by ID.")
                }
                .addOnSuccessListener {
                    if(LOG_ME)ALog.d(TAG, "$methodName(): Successfully got entity by ID.")
                }
                .await()
                .toObject(getFirestoreEntityType())?.let {
                    networkMapper.mapFromEntity(it)
                }
            return result
        } catch (e: Exception) {
            ALog.e(TAG, methodName, e)
            return null
        } finally {
            if (LOG_ME) ALog.d(TAG, "Method end: $methodName")
        }
    }

    companion object {
        val META_DOCUMENT_SUFIX = "_META"
    }
}