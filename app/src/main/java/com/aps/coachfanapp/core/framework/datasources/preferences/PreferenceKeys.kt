package com.aps.coachfanapp.core.framework.datasources.preferences

class PreferenceKeys {

    companion object{

        // Shared Preference Files:
        const val PROJECT_PREFERENCES: String = "com.aps.catemplateapp.preferences"

    }
}