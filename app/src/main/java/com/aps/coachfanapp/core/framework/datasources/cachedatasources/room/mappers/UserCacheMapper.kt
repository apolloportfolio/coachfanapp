package com.aps.coachfanapp.core.framework.datasources.cachedatasources.room.mappers

import com.aps.coachfanapp.common.business.domain.model.EntityMapper
import com.aps.coachfanapp.core.business.domain.model.entities.ProjectUser
import com.aps.coachfanapp.core.framework.datasources.cachedatasources.room.model.UserCacheEntity
import javax.inject.Inject

class UserCacheMapper
@Inject
constructor() : EntityMapper<UserCacheEntity, ProjectUser> {
    override fun mapFromEntity(entity: UserCacheEntity): ProjectUser {
        return ProjectUser(
            entity.id,
            entity.updated_at,
            entity.created_at,
            entity.emailAddress,
            entity.password,
            entity.profilePhotoImageURI,
            entity.name,
            entity.surname,
            entity.description,
            entity.city,
            entity.emailAddressVerified,
            entity.phoneNumberVerified,
        )
    }

    override fun mapToEntity(domainModel: ProjectUser): UserCacheEntity {
        return UserCacheEntity(
            domainModel.id!!,
            domainModel.updated_at,
            domainModel.created_at,
            domainModel.emailAddress,
            domainModel.password,
            domainModel.profilePhotoImageURI,
            domainModel.name,
            domainModel.surname,
            domainModel.description,
            domainModel.city,
            domainModel.emailAddressVerified,
            domainModel.phoneNumberVerified,
        )
    }

    override fun mapFromEntityList(entities : List<UserCacheEntity>) : List<ProjectUser> {
        return entities.map{mapFromEntity(it)}
    }

    override fun mapToEntityList(entities : List<ProjectUser>) : List<UserCacheEntity> {
        return entities.map{mapToEntity(it)}
    }
}