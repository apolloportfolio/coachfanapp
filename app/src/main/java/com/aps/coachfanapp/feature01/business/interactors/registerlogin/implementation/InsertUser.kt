package com.aps.coachfanapp.feature01.business.interactors.registerlogin.implementation

import com.aps.coachfanapp.R
import com.aps.coachfanapp.common.business.data.network.ApiResponseHandler
import com.aps.coachfanapp.common.business.data.util.safeApiCall
import com.aps.coachfanapp.common.business.domain.state.*
import com.aps.coachfanapp.core.business.data.cache.abstraction.UserCacheDataSource
import com.aps.coachfanapp.core.business.data.network.abstraction.UserNetworkDataSource
import com.aps.coachfanapp.core.business.domain.model.entities.ProjectUser
import com.aps.coachfanapp.core.business.domain.model.factories.UserFactory
import com.aps.coachfanapp.core.util.ApplicationContextProvider
import com.aps.coachfanapp.core.util.ConnectivityHelper
import com.aps.coachfanapp.feature01.business.interactors.registerlogin.abstraction.InsertUser
import com.aps.coachfanapp.feature01.framework.presentation.activity01.state.RegisterLoginActivityViewState
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

private const val TAG = "InsertUser"
private const val LOG_ME = true

class InsertUserImpl
@Inject
constructor(
    private val cacheDataSource: UserCacheDataSource,
    private val networkDataSource: UserNetworkDataSource,
    private val entityFactory: UserFactory,
    private val applicationContextProvider: ApplicationContextProvider,
    private val connectivityHelper: ConnectivityHelper,
): InsertUser {

    override fun insertNewEntity(
        newEntity: ProjectUser,
        stateEvent: StateEvent
    ):Flow<DataState<RegisterLoginActivityViewState<ProjectUser>>?> = flow {
        val registerResult = safeApiCall(Dispatchers.IO){
            networkDataSource.insertOrUpdateEntity(newEntity)
        }

        val cacheResponse = object: ApiResponseHandler<RegisterLoginActivityViewState<ProjectUser>, ProjectUser?>(
            response = registerResult,
            stateEvent = stateEvent,
            internetConnectionIsAvailable = connectivityHelper
                .internetConnectionIsAvailable(applicationContextProvider.applicationContext(TAG)),
        ){
            override suspend fun handleSuccess(resultObj: ProjectUser?): DataState<RegisterLoginActivityViewState<ProjectUser>>? {
                if(resultObj != null){
                    val viewState =
                        RegisterLoginActivityViewState<ProjectUser>(
                            appProjectUser = resultObj
                        )
                    return DataState.data(
                        response = Response(
                            messageId = R.string.error,
                            message = INSERT_ENTITY_SUCCESS,
                            uiComponentType = UIComponentType.Toast(),
                            messageType = MessageType.Success()
                        ),
                        data = viewState,
                        stateEvent = stateEvent
                    )
                }
                else{
                    return DataState.data(
                        response = Response(
                            messageId = R.string.error,
                            message = INSERT_ENTITY_FAILED,
                            uiComponentType = UIComponentType.Toast(),
                            messageType = MessageType.Error()
                        ),
                        data = null,
                        stateEvent = stateEvent
                    )
                }
            }
        }.getResult()

        emit(cacheResponse)
    }

    companion object{
        val INSERT_ENTITY_SUCCESS = "Successfully inserted new entity."
        val INSERT_ENTITY_FAILED = "Failed to insert new entity."
    }
}