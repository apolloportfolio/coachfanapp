package com.aps.coachfanapp.feature01.framework.presentation.activity01.state

import com.aps.coachfanapp.common.business.domain.state.StateEvent
import com.aps.coachfanapp.common.business.domain.state.StateMessage
import com.aps.coachfanapp.core.business.domain.model.entities.ProjectUser

sealed class RegisterLoginActivityStateEvent : StateEvent {

    data object None : RegisterLoginActivityStateEvent() {
        override fun errorInfo() = eventName() + " error"
        override fun eventName() = "None"
        override fun shouldDisplayProgressBar() = false
    }

    data object CheckIfEmailIsTakenUserEvent : RegisterLoginActivityStateEvent() {
        override fun errorInfo() = eventName() + " error"
        override fun eventName() = "CheckIfEmailIsTakenUserEvent"
        override fun shouldDisplayProgressBar() = true
    }

    data object CheckIfPasswordMatchesEmailUserEvent : RegisterLoginActivityStateEvent() {
        override fun errorInfo() = eventName() + " error"
        override fun eventName() = "CheckIfPasswordMatchesEmailUserEvent"
        override fun shouldDisplayProgressBar() = true
    }

    data object RegisterUserEvent : RegisterLoginActivityStateEvent() {
        override fun errorInfo() = eventName() + " error"
        override fun eventName() = "RegisterUserEvent"
        override fun shouldDisplayProgressBar() = true
    }

    data object LoginUserEvent : RegisterLoginActivityStateEvent() {
        override fun errorInfo() = eventName() + " error"
        override fun eventName() = "LoginUserEvent"
        override fun shouldDisplayProgressBar() = true
    }

    data object GetAppUserEvent : RegisterLoginActivityStateEvent() {
        override fun errorInfo() = eventName() + " error"
        override fun eventName() = "GetAppUserEvent"
        override fun shouldDisplayProgressBar() = true
    }

    data object AddNewAppUserEvent : RegisterLoginActivityStateEvent() {
        override fun errorInfo() = eventName() + " error"
        override fun eventName() = "AddNewAppUserEvent"
        override fun shouldDisplayProgressBar() = true
    }

    data object InsertNewUserEvent : RegisterLoginActivityStateEvent() {
        override fun errorInfo() = eventName() + " error"
        override fun eventName() = "InsertNewUserEvent"
        override fun shouldDisplayProgressBar() = true
    }

    data object InsertMultipleNewUsersEvent : RegisterLoginActivityStateEvent() {
        override fun errorInfo() = eventName() + " error"
        override fun eventName() = "InsertMultipleNewUsersEvent"
        override fun shouldDisplayProgressBar() = true
    }

    data object GetAllCacheEntities : RegisterLoginActivityStateEvent() {
        override fun errorInfo() = eventName() + " error"
        override fun eventName() = "GetAllCacheEntities"
        override fun shouldDisplayProgressBar() = true
    }

    data object SearchUserEvent : RegisterLoginActivityStateEvent() {
        override fun errorInfo() = eventName() + " error"
        override fun eventName() = "SearchUserEvent"
        override fun shouldDisplayProgressBar() = true
    }

    data object LoginAppUserStateEvent : RegisterLoginActivityStateEvent() {
        override fun errorInfo() = eventName() + " error"
        override fun eventName() = "LoginAppUser"
        override fun shouldDisplayProgressBar() = true
    }

    data object CheckDeviceCompatibility : RegisterLoginActivityStateEvent() {
        override fun errorInfo() = eventName() + " error"
        override fun eventName() = "CheckDeviceCompatibility"
        override fun shouldDisplayProgressBar() = true
    }

    class UpdateAppUserEvent(val appProjectUser: ProjectUser) : RegisterLoginActivityStateEvent() {
        override fun errorInfo() = eventName() + " error"
        override fun eventName() = "UpdateAppUserEvent"
        override fun shouldDisplayProgressBar() = true
    }

    class DeleteAppUserEvent(val appProjectUser: ProjectUser) : RegisterLoginActivityStateEvent() {
        override fun errorInfo() = eventName() + " error"
        override fun eventName() = "DeleteAppUserEvent"
        override fun shouldDisplayProgressBar() = true
    }

    class DeleteMultipleAppUsersEvent(val appProjectUsers: ArrayList<ProjectUser>) : RegisterLoginActivityStateEvent() {
        override fun errorInfo() = eventName() + " error"
        override fun eventName() = "DeleteMultipleAppUsersEvent"
        override fun shouldDisplayProgressBar() = true
    }

    class CreateStateMessageEvent(val stateMessage: StateMessage) : RegisterLoginActivityStateEvent() {
        override fun errorInfo() = eventName() + " error"
        override fun eventName() = "CreateStateMessageEvent"
        override fun shouldDisplayProgressBar() = false
    }
}

