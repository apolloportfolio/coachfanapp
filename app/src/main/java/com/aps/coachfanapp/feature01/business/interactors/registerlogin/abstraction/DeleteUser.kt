package com.aps.coachfanapp.feature01.business.interactors.registerlogin.abstraction

import com.aps.coachfanapp.common.business.domain.state.DataState
import com.aps.coachfanapp.common.business.domain.state.StateEvent
import com.aps.coachfanapp.core.business.domain.model.entities.ProjectUser
import kotlinx.coroutines.flow.Flow

interface DeleteUser<ViewState> {
    fun deleteEntity(
        entity: ProjectUser,
        stateEvent: StateEvent
    ): Flow<DataState<ViewState>?>
}