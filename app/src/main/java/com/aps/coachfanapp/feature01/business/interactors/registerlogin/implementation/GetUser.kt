package com.aps.coachfanapp.feature01.business.interactors.registerlogin.implementation

import com.aps.coachfanapp.R
import com.aps.coachfanapp.common.business.data.cache.CacheResponseHandler
import com.aps.coachfanapp.common.business.data.util.safeApiCall
import com.aps.coachfanapp.common.business.data.util.safeCacheCall
import com.aps.coachfanapp.common.business.domain.state.*
import com.aps.coachfanapp.common.util.DateUtil
import com.aps.coachfanapp.common.util.UniqueID
import com.aps.coachfanapp.core.business.data.cache.abstraction.UserCacheDataSource
import com.aps.coachfanapp.core.business.data.network.abstraction.UserNetworkDataSource
import com.aps.coachfanapp.core.business.domain.model.entities.ProjectUser
import com.aps.coachfanapp.feature01.business.interactors.registerlogin.abstraction.GetUser
import com.aps.coachfanapp.feature01.framework.presentation.activity01.state.RegisterLoginActivityViewState
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject


class GetUserImpl @Inject constructor(
    private val cacheDataSource: UserCacheDataSource,
    private val networkDataSource: UserNetworkDataSource,
    private val dateUtil: DateUtil
): GetUser {

    override fun getEntity(id: UniqueID, stateEvent: StateEvent): Flow<DataState<RegisterLoginActivityViewState<ProjectUser>>?> = flow {
        val cacheResult = safeCacheCall(Dispatchers.IO){
            syncEntity(id)
        }

        val response = object: CacheResponseHandler<RegisterLoginActivityViewState<ProjectUser>, ProjectUser>(
            response = cacheResult,
            stateEvent = stateEvent
        ){
            override suspend fun handleSuccess(resultObj: ProjectUser): DataState<RegisterLoginActivityViewState<ProjectUser>>? {
                var message: String? =
                    SYNC_ENTITIES_SUCCESS
                var uiComponentType: UIComponentType? = UIComponentType.None()

                var data = RegisterLoginActivityViewState<ProjectUser>()
                if(resultObj == null){
                    message = SEARCH_ENTITIES_NO_MATCHING_RESULTS
                    uiComponentType = UIComponentType.Toast()
                } else {
                    data.appProjectUser = resultObj
                    data.mainEntity = resultObj
                }
                return DataState.data(
                    response = Response(
                        messageId = R.string.error,
                        message = message,
                        uiComponentType = uiComponentType as UIComponentType,
                        messageType = MessageType.Success()
                    ),
                    data = data,
                    stateEvent = stateEvent
                )
            }
        }.getResult()

        emit(response)
    }

    private suspend fun syncEntity(entityId: UniqueID): ProjectUser? {
        val cachedEntity = cacheDataSource.getEntityById(entityId)
        val networkEntity = networkDataSource.getEntityById(entityId)

        if(networkEntity == null)return null
        if(cachedEntity == null) {
            cacheDataSource.insertEntity(networkEntity)
            return networkEntity
        } else {
            if(dateUtil.convertStringDateToFirebaseTimestamp(networkEntity.updated_at!!) > dateUtil.convertStringDateToFirebaseTimestamp(cachedEntity.updated_at!!)) {
                safeCacheCall(Dispatchers.IO){
                    cacheDataSource.updateEntity(
                        networkEntity.id,
                        networkEntity.updated_at,
                        networkEntity.created_at,
                        networkEntity.emailAddress,
                        networkEntity.password,
                        networkEntity.profilePhotoImageURI,
                        networkEntity.name,
                        networkEntity.surname,
                        networkEntity.description,
                        networkEntity.city,
                        networkEntity.emailAddressVerified,
                        networkEntity.phoneNumberVerified,
                    )
                }// retain network timestamp
                return networkEntity
            } else {
                safeApiCall(Dispatchers.IO){
                    networkDataSource.insertOrUpdateEntity(cachedEntity)
                }
                return cachedEntity
            }
        }
    }

    companion object{
        val SYNC_ENTITIES_SUCCESS = "Successfully synced entities."
        val SEARCH_ENTITIES_NO_MATCHING_RESULTS = "There are no entities that match that query."
        val SEARCH_ENTITIES_FAILED = "Failed to retrieve the list of entities."

    }
}