package com.aps.coachfanapp.feature01.business.interactors.registerlogin.implementation

import com.aps.coachfanapp.R
import com.aps.coachfanapp.common.business.data.cache.CacheResponseHandler
import com.aps.coachfanapp.core.business.data.network.abstraction.UserNetworkDataSource
import com.aps.coachfanapp.feature01.business.interactors.registerlogin.abstraction.CheckIfPasswordMatchesEmail
import com.aps.coachfanapp.common.business.data.util.safeCacheCall
import com.aps.coachfanapp.common.business.domain.state.*
import com.aps.coachfanapp.core.business.domain.model.entities.ProjectUser
import com.aps.coachfanapp.feature01.framework.presentation.activity01.state.RegisterLoginActivityViewState
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject


class CheckIfPasswordMatchesEmailImpl @Inject constructor(
    private val networkDataSource: UserNetworkDataSource
): CheckIfPasswordMatchesEmail {
    override fun checkIfPasswordMatchesEmail(email: String?, password: String?, stateEvent: StateEvent): Flow<DataState<RegisterLoginActivityViewState<ProjectUser>>?> = flow {
        val networkResult = safeCacheCall(Dispatchers.IO){
            //networkDataSource.getIdOfUserWithTheseCredentials(email, password, null)
            //networkDataSource.getEntityById(networkDataSource.getIdOfUserWithTheseCredentials(email, password, null), null)
            networkDataSource.getEntityByCredentials(email, password)
        }

        val response = object: CacheResponseHandler<RegisterLoginActivityViewState<ProjectUser>, ProjectUser?>(
            response = networkResult,
            stateEvent = stateEvent
        ){
            override suspend fun handleSuccess(resultObj: ProjectUser?): DataState<RegisterLoginActivityViewState<ProjectUser>>? {
                if(resultObj != null){
                    val viewState =
                        RegisterLoginActivityViewState<ProjectUser>(
                            appProjectUser = resultObj
                        )
                    return DataState.data(
                        response = Response(
                            messageId = R.string.error,
                            message = PASSWORD_MATCHES_EMAIL,
                            uiComponentType = UIComponentType.Toast(),
                            messageType = MessageType.Success()
                        ),
                        data = viewState,
                        stateEvent = stateEvent
                    )
                } else {
                    return DataState.data(
                        response = Response(
                            messageId = R.string.error,
                            message = PASSWORD_DOESNT_MATCH_EMAIL,
                            uiComponentType = UIComponentType.Toast(),
                            messageType = MessageType.Error()
                        ),
                        data = null,
                        stateEvent = stateEvent
                    )
                }
            }
        }.getResult()

        emit(response)
    }

    companion object{
        val PASSWORD_MATCHES_EMAIL = "Password matches email."
        val WRONG_PASSWORD = "Wrong Password"
        val PASSWORD_DOESNT_MATCH_EMAIL = "Password doesn't match email."
        val RESPONSE_IS_NULL = "No response received from server. Try again later."
        val SEARCH_ENTITIES_SUCCESS = "Successfully found an entity in repository."
    }
}