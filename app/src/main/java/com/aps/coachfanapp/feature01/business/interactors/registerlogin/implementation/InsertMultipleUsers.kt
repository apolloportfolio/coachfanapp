package com.aps.coachfanapp.feature01.business.interactors.registerlogin.implementation

import com.aps.coachfanapp.R
import com.aps.coachfanapp.common.business.data.util.safeApiCall
import com.aps.coachfanapp.common.business.data.util.safeCacheCall
import com.aps.coachfanapp.common.business.domain.state.*
import com.aps.coachfanapp.common.util.DateUtil
import com.aps.coachfanapp.core.business.data.cache.abstraction.UserCacheDataSource
import com.aps.coachfanapp.core.business.data.network.abstraction.UserNetworkDataSource
import com.aps.coachfanapp.core.business.domain.model.entities.ProjectUser
import com.aps.coachfanapp.core.business.domain.model.factories.UserFactory
import com.aps.coachfanapp.feature01.business.interactors.registerlogin.abstraction.InsertMultipleUsers
import com.aps.coachfanapp.feature01.framework.presentation.activity01.state.RegisterLoginActivityViewState
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject


class InsertMultipleUsersImpl
@Inject
constructor(
    private val cacheDataSource: UserCacheDataSource,
    private val networkDataSource: UserNetworkDataSource,
): InsertMultipleUsers {

    override fun insertEntities(
        numEntities: Int,
        stateEvent: StateEvent
    ): Flow<DataState<RegisterLoginActivityViewState<ProjectUser>>?> = flow {

        val noteList = EntityListTester.generateEntityList(numEntities)
        safeCacheCall(Dispatchers.IO){
            cacheDataSource.insertEntities(noteList)
        }

        emit(
            DataState.data<RegisterLoginActivityViewState<ProjectUser>>(
                response = Response(
                    messageId = R.string.error,
                    message = "success",
                    uiComponentType = UIComponentType.None(),
                    messageType = MessageType.None()
                ),
                data = null,
                stateEvent = stateEvent
            )
        )

        updateNetwork(noteList)
    }

    private suspend fun updateNetwork(entityList: List<ProjectUser>){
        safeApiCall(Dispatchers.IO){
            networkDataSource.insertOrUpdateEntities(entityList)
        }
    }

}


private object EntityListTester {

    private val dateFormatForFirestore = SimpleDateFormat("yyyy-MM-dd hh:mm:ss a", Locale.ENGLISH)
    private val dateFormatForUser = SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH)
    private val dateFormatJustYear = SimpleDateFormat("yyyy", Locale.ENGLISH)
    private val dateUtil =
        DateUtil(dateFormatForFirestore, dateFormatForUser, dateFormatJustYear)
    private val userFactory: UserFactory = UserFactory(dateUtil)

    fun generateEntityList(numEntities: Int): List<ProjectUser>{
        val list: ArrayList<ProjectUser> = ArrayList()
        for(id in 0..numEntities){
            list.add(userFactory.generateEmpty())
        }
        return list
    }
}