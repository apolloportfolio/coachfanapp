package com.aps.coachfanapp.feature01.business.interactors.registerlogin.abstraction

interface SyncUsers {
    suspend fun syncEntities()
}