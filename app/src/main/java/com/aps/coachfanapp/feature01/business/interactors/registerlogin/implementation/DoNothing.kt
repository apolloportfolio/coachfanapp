package com.aps.coachfanapp.feature01.business.interactors.registerlogin.implementation


import com.aps.coachfanapp.feature01.business.interactors.registerlogin.abstraction.DoNothing
import com.aps.coachfanapp.common.business.domain.state.DataState
import com.aps.coachfanapp.core.business.data.cache.abstraction.UserCacheDataSource
import com.aps.coachfanapp.core.business.data.network.abstraction.UserNetworkDataSource
import com.aps.coachfanapp.core.business.domain.model.entities.ProjectUser
import com.aps.coachfanapp.feature01.framework.presentation.activity01.state.RegisterLoginActivityViewState
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject


class DoNothingImpl @Inject constructor(
    private val cacheDataSource: UserCacheDataSource,
    private val networkDataSource: UserNetworkDataSource
): DoNothing {

    override fun doNothing(): Flow<DataState<RegisterLoginActivityViewState<ProjectUser>>?> = flow {
       //Nothing to do
    }
}
