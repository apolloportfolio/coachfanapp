package com.aps.coachfanapp.feature01.framework.presentation.activity01.fragments

import android.content.Context
import android.os.Bundle
import android.os.Parcel
import android.os.Parcelable
import android.view.MotionEvent
import android.view.View
import android.widget.EditText
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.aps.coachfanapp.core.util.ApplicationContextProvider
import com.aps.coachfanapp.R
import com.aps.coachfanapp.common.business.domain.state.*
import com.aps.coachfanapp.common.framework.presentation.BaseMVIFragment
import com.aps.coachfanapp.common.framework.presentation.BaseViewModel
import com.aps.coachfanapp.common.util.ALog
import com.aps.coachfanapp.common.util.DateUtil
import com.aps.coachfanapp.common.util.extensions.hideKeyboard
import com.aps.coachfanapp.common.util.extensions.showKeyboard
import com.aps.coachfanapp.core.business.domain.model.entities.ProjectUser
import com.aps.coachfanapp.databinding.FragmentActivityRegisterloginRegisterBinding
import com.aps.coachfanapp.feature01.framework.presentation.activity01.RegisterLoginActivityNavigation
import com.aps.coachfanapp.feature01.framework.presentation.activity01.RegisterLoginActivityViewModel
import com.aps.coachfanapp.feature01.framework.presentation.activity01.state.RegisterLoginActivityInteractionState
import com.aps.coachfanapp.feature01.framework.presentation.activity01.state.RegisterLoginActivityStateEvent
import com.aps.coachfanapp.feature01.framework.presentation.activity01.state.RegisterLoginActivityViewState
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.InternalCoroutinesApi

const val REGISTERLOGIN_REGISTRATION_STATE_BUNDLE_KEY = "com.aps.cleanarchitecturetemplateapplication.feature01.framework.presentation.activity01.fragments.registration"

private const val TAG = "RegisterLoginActivityRegistrationFragment"
private const val LOG_ME = true

@FlowPreview
@ExperimentalCoroutinesApi
@AndroidEntryPoint
class RegisterLoginActivityRegistrationFragment
constructor(
        private val viewModelFactory: ViewModelProvider.Factory,
        private val dateUtil: DateUtil
): BaseMVIFragment<ProjectUser, RegisterLoginActivityViewState<ProjectUser>>(layoutRes = R.layout.fragment_activity_registerlogin_register),
    RegisterLoginActivityNavigation,
    View.OnTouchListener {

    private val viewModel: RegisterLoginActivityViewModel by viewModels {
        viewModelFactory
    }

    private lateinit var binding: FragmentActivityRegisterloginRegisterBinding

    constructor(parcel: Parcel) : this(
            TODO("viewModelFactory"),
            TODO("dateUtil")) {
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.setupChannel()
    }

    @InternalCoroutinesApi
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupOnBackPressDispatcher()
    }

    override fun onResume() {
        super.onResume()
    }

    override fun onPause() {
        super.onPause()
        //Save data from ui in ViewModel if necessary
        updateEmailInViewModel()
        updatePasswordInViewModel()
        updatePasswordConfirmationInViewModel()
    }

    override fun restoreInstanceState(savedInstanceState: Bundle?){
    }

    private fun setupOnBackPressDispatcher() {
        val callback = object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                onBackPressed()
            }
        }
        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner, callback)
    }

    override fun onBackPressed() {
        super.onBackPressed()
        view?.hideKeyboard()
        clearUIInViewModel()
        findNavController().popBackStack()
    }

    private fun clearUIInViewModel() {
        val methodName: String = "clearUIInViewModel()"
        if (LOG_ME) ALog.d(TAG, "Method start: $methodName")
        try {
            viewModel.clearEmail()
            viewModel.clearPassword()
            viewModel.clearPasswordConfirmation()
            viewModel.clearAppUser()
        } catch (e: Exception) {
            ALog.e(TAG, methodName, e)
        } finally {
            if (LOG_ME) ALog.d(TAG, "Method end: $methodName")
        }
    }

    override fun setupUI(){
        view?.hideKeyboard()
    }

    override fun onDestroyView() {
        super.onDestroyView()
    }

    override fun setupBinding(view: View) {
        binding = FragmentActivityRegisterloginRegisterBinding.bind(view)
    }

    companion object CREATOR : Parcelable.Creator<RegisterLoginActivityRegistrationFragment> {
        override fun createFromParcel(parcel: Parcel): RegisterLoginActivityRegistrationFragment {
            return RegisterLoginActivityRegistrationFragment(parcel)
        }

        override fun newArray(size: Int): Array<RegisterLoginActivityRegistrationFragment?> {
            return arrayOfNulls(size)
        }
    }

    override fun subscribeCustomObservers() {
        viewModel.emailInteractionState.observe(viewLifecycleOwner, Observer { state ->
            when(state){
                is RegisterLoginActivityInteractionState.EditState -> {
                    if(LOG_ME)ALog.d(TAG, ".subscribeCustomObservers(): " +
                            "emailInteractionState is in EditState")
                    view?.showKeyboard()
                    viewModel.setIsUpdatePending(true)
                }

                is RegisterLoginActivityInteractionState.DefaultState -> {
                }
            }
        })

        viewModel.passwordInteractionState.observe(viewLifecycleOwner, Observer { state ->
            when(state){
                is RegisterLoginActivityInteractionState.EditState -> {
                    if(LOG_ME)ALog.d(TAG, ".subscribeCustomObservers(): " +
                            "passwordInteractionState is in EditState")
                    view?.showKeyboard()
                    viewModel.setIsUpdatePending(true)
                }

                is RegisterLoginActivityInteractionState.DefaultState -> {
                }
            }
        })

        viewModel.passwordConfirmationInteractionState.observe(viewLifecycleOwner, Observer { state ->
            when(state){
                is RegisterLoginActivityInteractionState.EditState -> {
                    if(LOG_ME)ALog.d(TAG, ".subscribeCustomObservers(): " +
                                            "passwordConfirmationInteractionState is in EditState")
                    view?.showKeyboard()
                    viewModel.setIsUpdatePending(true)
                }

                is RegisterLoginActivityInteractionState.DefaultState -> {
                }
            }
        })
    }

    override fun getViewModel(): BaseViewModel<ProjectUser, RegisterLoginActivityViewState<ProjectUser>> {
        return viewModel
    }

    override fun getInitStateEvent(): StateEvent {
        return RegisterLoginActivityStateEvent.None
    }

    override fun changeUiOrNavigateDependingOnViewState(viewState: RegisterLoginActivityViewState<ProjectUser>) {
        if(viewState.firebaseUserIsLoggedIn != null) {
            if(viewState.firebaseUserIsLoggedIn!!) {
                if(viewState.appProjectUser!!.fullyVerified) {
                    this.navigateToHomeScreen(this, viewState.appProjectUser!!)
                } else {
                    this.navigateToProfileScreen(this, viewState.appProjectUser!!)
                }
            } else {
                this.navigateToOptionsFragment()
            }
        }

        viewState.isInternetAvailable?.let {
            if(!it) {
                hideUI()
            } else {
                showUI()
            }
            viewModel.setIsInternetAvailable(null)
        }

        viewState.killAppAndOpenNetworkSettings?.let{
            if(it) {
                viewModel.setKillAppAndOpenNetworkSettings(null)
                closeApplicationAndOpenInternetSettings(this)
            }
        }
    }

    private fun hideUI() {
        binding.buttonRegister.visibility = View.INVISIBLE
        binding.emailEdittext.visibility = View.INVISIBLE
        binding.passwordEdittext.visibility = View.INVISIBLE
        binding.passwordConfirmEdittext.visibility = View.INVISIBLE
    }

    private fun showUI() {
        binding.buttonRegister.visibility = View.VISIBLE
        binding.emailEdittext.visibility = View.VISIBLE
        binding.passwordEdittext.visibility = View.VISIBLE
        binding.passwordConfirmEdittext.visibility = View.VISIBLE
    }

    private fun navigateToMainFragment() {
        findNavController().navigate(
            R.id.action_RegisterLoginActivityRegistrationFragment_to_RegisterLoginActivityMainFragment
        )
    }

    private fun navigateToOptionsFragment() {
        findNavController().navigate(
            R.id.action_RegisterLoginActivityRegistrationFragment_to_RegisterLoginActivityOptionsFragment
        )
    }

    override fun onEntityUpdateSuccess() {

    }

    override fun onEntityDeleteSuccess() {
        this.navigateToMainFragment()
    }

    override fun getStateBundleKey(): String? {
        return REGISTERLOGIN_REGISTRATION_STATE_BUNDLE_KEY
    }

    override fun setAllOnClickAndOnTouchListeners() {
        binding.emailEdittext.setOnTouchListener(this)
        binding.passwordEdittext.setOnTouchListener(this)
        binding.passwordConfirmEdittext.setOnTouchListener(this)
        binding.buttonRegister.setOnTouchListener(this)
    }

    override fun onTouch(v: View?, event: MotionEvent?): Boolean {
        when(v) {
            binding.buttonRegister -> {
                when(event!!.action) {
                    MotionEvent.ACTION_DOWN -> {
                        if (LOG_ME) ALog.d(TAG, "onTouch(): binding.buttonRegister")
                        updateEmailInViewModel()
                        updatePasswordInViewModel()
                        updatePasswordConfirmationInViewModel()
                        if (allInputFieldsArentEmpty()) {
                            if ((getViewModel() as RegisterLoginActivityViewModel).emailIsValid()) {
                                if ((getViewModel() as RegisterLoginActivityViewModel).passwordsMatch()) this.registerInFirestore()
                                else {
                                    if (LOG_ME) ALog.d(TAG, ".onTouch(): Passwords do not match.")
                                    getViewModel().setStateEvent(
                                        RegisterLoginActivityStateEvent.CreateStateMessageEvent(
                                            stateMessage = StateMessage(
                                                response = Response(
                                                    messageId = R.string.text_ok,
                                                    message = context?.getString(
                                                        R.string.text_ok
                                                    ),
                                                    uiComponentType = UIComponentType.SnackBar(
                                                        null,
                                                        R.string.passwords_do_not_match,
                                                        context?.getString(
                                                            R.string.passwords_do_not_match
                                                        ),
                                                        View.OnClickListener { }
                                                    ),
                                                    messageType = MessageType.Error()
                                                )
                                            ),
                                        )
                                    )
                                }
                            } else {
                                if (LOG_ME) ALog.d(TAG, ".onTouch(): Invalid email.")
                                getViewModel().setStateEvent(
                                    RegisterLoginActivityStateEvent.CreateStateMessageEvent(
                                        stateMessage = StateMessage(
                                            response = Response(
                                                messageId = R.string.text_ok,
                                                message = context?.getString(
                                                    R.string.text_ok
                                                ),
                                                uiComponentType = UIComponentType.SnackBar(
                                                    null,
                                                    R.string.invalid_email,
                                                    context?.getString(
                                                        R.string.invalid_email
                                                    ),
                                                    View.OnClickListener { }
                                                ),
                                                messageType = MessageType.Error()
                                            )
                                        ),
                                    )
                                )
                            }

                        } else {
                            if (LOG_ME) ALog.d(
                                TAG,
                                "onTouch(): binding.buttonLogin: At least one input field is empty"
                            )
                        }
                    }
                }
            }

            binding.emailEdittext -> {
                when(event!!.action) {
                    MotionEvent.ACTION_DOWN -> {
                        if (LOG_ME) ALog.d(TAG, "onTouch(): binding.emailEdittext")
                        if (!viewModel.isEditingEmail()) {
                            if (LOG_ME) ALog.d(
                                TAG,
                                "onTouch(): Setting EditState for binding.emailEdittext"
                            )
                            viewModel.setEmailInteractionState(RegisterLoginActivityInteractionState.EditState())
                        }
                    }
                }                
            }

            binding.passwordEdittext -> {
                when(event!!.action) {
                    MotionEvent.ACTION_DOWN -> {
                        if (LOG_ME) ALog.d(TAG, "onTouch(): binding.passwordEdittext")
                        if (!viewModel.isEditingPassword()) {
                            if (LOG_ME) ALog.d(
                                TAG,
                                "onTouch(): Setting EditState for binding.passwordEdittext"
                            )
                            viewModel.setPasswordInteractionState(RegisterLoginActivityInteractionState.EditState())
                        }
                    }
                }                
            }

            binding.passwordConfirmEdittext -> {
                when(event!!.action) {
                    MotionEvent.ACTION_DOWN -> {
                        if (LOG_ME) ALog.d(TAG, "onTouch(): binding.passwordConfirmEdittext")
                        if (!viewModel.isEditingPasswordConfirmation()) {
                            if (LOG_ME) ALog.d(
                                TAG,
                                "onTouch(): Setting EditState for binding.passwordConfirmEdittext"
                            )
                            viewModel.setPasswordConfirmationInteractionState(
                                RegisterLoginActivityInteractionState.EditState()
                            )
                        }
                    }
                }                
            }
        }
        return false
    }

    override fun onClick(v: View?) {
        if (LOG_ME) ALog.d(TAG, "onClick(): Method start")
        super.onClick(v)
    }

    private fun getEmailBody(): String {
        return binding.emailEdittext?.text.toString()
    }

    private fun getPasswordBody(): String {
        return binding.passwordEdittext?.text.toString()
    }

    private fun getPasswordConfirmationBody(): String {
        return binding.passwordConfirmEdittext?.text.toString()
    }

    private fun allInputFieldsArentEmpty():Boolean {
        return editTextIsNotEmpty(binding.emailEdittext) && editTextIsNotEmpty(binding.passwordEdittext)
    }

    private fun editTextIsNotEmpty(editText: EditText): Boolean {
        return editText.text.toString().isNotEmpty()
    }

    private fun updateEmailInViewModel() {
        if (LOG_ME) ALog.d(TAG, "updateEmailInViewModel(): Updating email ViewModel with " + getEmailBody())
        viewModel.updateEmail(getEmailBody())
    }

    private fun updatePasswordInViewModel() {
        if (LOG_ME) ALog.d(TAG, "updatePasswordInViewModel(): Updating password ViewModel with " + getPasswordBody())
        viewModel.updatePassword(getPasswordBody())
    }

    private fun updatePasswordConfirmationInViewModel() {
        if (LOG_ME) ALog.d(TAG, "updatePasswordConfirmationInViewModel(): Updating password confirmation ViewModel with " + getPasswordBody())
        viewModel.updatePasswordConfirmation(getPasswordConfirmationBody())
    }

    override fun updateUIInViewModel() {
        if(viewModel.checkEditState()){
            view?.hideKeyboard()
            if (LOG_ME) {
                ALog.d(TAG, "updateUIInViewModel():  User interface is in EditState")
                ALog.d(TAG, "updateUIInViewModel():  updating email with: " + getEmailBody())
                ALog.d(TAG, "updateUIInViewModel():  updating password with: " + getPasswordBody())
                ALog.d(TAG, "updateUIInViewModel():  updating password confirmation with: " + getPasswordConfirmationBody())
                ALog.d(TAG, "updateUIInViewModel(): updated appUser: == " + viewModel.getCurrentViewStateOrNew().appProjectUser)
            }
            updateEmailInViewModel()
            updatePasswordInViewModel()
            updatePasswordConfirmationInViewModel()
            updateAppUser()
            if(LOG_ME)ALog.d(TAG, ".updateUIInViewModel(): After updateAppUser() appUser == " + viewModel.getCurrentViewStateOrNew().appProjectUser)
            viewModel.exitEditState()
        }
    }

    private fun updateAppUser() {
        if(viewModel.getIsUpdatePending()){
            viewModel.getCurrentViewStateOrNew().appProjectUser?.let {
                RegisterLoginActivityStateEvent.UpdateAppUserEvent(it)
            }?.let {
                if (LOG_ME) ALog.d(TAG, "updateAppUser(): Sending UpdateAppUserEvent")
                viewModel.setStateEvent(
                    it
                )
            }
        }
    }

    private fun registerInFirestore() {
        if (LOG_ME) ALog.d(TAG, "registerInFirestore(): Trying to register user in Firestore.")
        getViewModel().setStateEvent(RegisterLoginActivityStateEvent.RegisterUserEvent)
    }
}