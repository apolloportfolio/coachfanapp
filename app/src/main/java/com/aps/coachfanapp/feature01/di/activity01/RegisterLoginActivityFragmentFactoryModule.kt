package com.aps.coachfanapp.feature01.di.activity01

import androidx.lifecycle.ViewModelProvider
import com.aps.coachfanapp.common.util.DateUtil
import com.aps.coachfanapp.feature01.framework.presentation.activity01.RegisterLoginActivityFragmentFactory
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import javax.inject.Singleton

@FlowPreview
@ExperimentalCoroutinesApi
@Module
@InstallIn(SingletonComponent::class)
object RegisterLoginActivityFragmentFactoryModule {

    @JvmStatic
    @Singleton
    @Provides
    fun provideRegisterLoginActivityFragmentFactory(
        viewModelFactory: ViewModelProvider.Factory,
        dateUtil: DateUtil
    ): RegisterLoginActivityFragmentFactory {
        return RegisterLoginActivityFragmentFactory(viewModelFactory, dateUtil)
    }
}