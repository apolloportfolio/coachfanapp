package com.aps.coachfanapp.feature01.business.interactors.registerlogin.implementation

import com.aps.coachfanapp.R
import com.aps.coachfanapp.common.business.data.cache.CacheResponseHandler
import com.aps.coachfanapp.common.business.data.util.safeCacheCall
import com.aps.coachfanapp.common.business.domain.state.*
import com.aps.coachfanapp.core.business.data.cache.abstraction.UserCacheDataSource
import com.aps.coachfanapp.core.business.domain.model.entities.ProjectUser
import com.aps.coachfanapp.feature01.business.interactors.registerlogin.abstraction.GetAllCachedUsers
import com.aps.coachfanapp.feature01.framework.presentation.activity01.state.RegisterLoginActivityViewState
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject


class GetAllCachedUsersImpl @Inject constructor(
    private val cacheDataSource: UserCacheDataSource
): GetAllCachedUsers {

    override fun getAllCachedEntities(
        stateEvent: StateEvent
    ): Flow<DataState<RegisterLoginActivityViewState<ProjectUser>>?> = flow {
        val cacheResult = safeCacheCall(Dispatchers.IO){
            cacheDataSource.getAllEntities()
        }

        val response = object: CacheResponseHandler<RegisterLoginActivityViewState<ProjectUser>, List<ProjectUser>>(
            response = cacheResult,
            stateEvent = stateEvent
        ){
            override suspend fun handleSuccess(resultObj: List<ProjectUser>): DataState<RegisterLoginActivityViewState<ProjectUser>>? {
                if(resultObj.isNotEmpty()){
                    val viewState =
                        RegisterLoginActivityViewState<ProjectUser>(
                            appProjectUser = resultObj[0]
                        )
                    viewState.listOfEntities = ArrayList(resultObj)
                    return DataState.data(
                        response = Response(
                            messageId = R.string.error,
                            message = SEARCH_ENTITIES_SUCCESS,
                            uiComponentType = UIComponentType.None(),
                            messageType = MessageType.Success()
                        ),
                        data = viewState,
                        stateEvent = stateEvent
                    )
                } else {
                    val viewState = RegisterLoginActivityViewState<ProjectUser>()
                    viewState.noCachedEntity = true
                    return DataState.data(
                        response = Response(
                            messageId = R.string.error,
                            message = SEARCH_ENTITIES_NO_MATCHING_RESULTS,
                            uiComponentType = UIComponentType.None(),
                            messageType = MessageType.Success()
                        ),
                        data = viewState,
                        stateEvent = stateEvent
                    )
                }
            }
        }.getResult()

        emit(response)
    }

    companion object{
        val SEARCH_ENTITIES_SUCCESS = "Successfully retrieved list of entities."
        val SEARCH_ENTITIES_NO_MATCHING_RESULTS = "There are no entities that match that query."
        val SEARCH_ENTITIES_FAILED = "Failed to retrieve the list of entities."

    }
}