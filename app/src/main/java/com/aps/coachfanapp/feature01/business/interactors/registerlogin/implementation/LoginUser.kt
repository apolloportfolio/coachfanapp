package com.aps.coachfanapp.feature01.business.interactors.registerlogin.implementation

import com.aps.coachfanapp.R
import com.aps.coachfanapp.common.business.data.network.ApiResponseHandler
import com.aps.coachfanapp.common.business.data.util.safeApiCall
import com.aps.coachfanapp.common.business.domain.state.*
import com.aps.coachfanapp.core.business.data.cache.abstraction.UserCacheDataSource
import com.aps.coachfanapp.core.business.data.network.abstraction.UserNetworkDataSource
import com.aps.coachfanapp.core.business.domain.model.entities.ProjectUser
import com.aps.coachfanapp.core.business.domain.model.factories.UserFactory
import com.aps.coachfanapp.core.util.ApplicationContextProvider
import com.aps.coachfanapp.core.util.ConnectivityHelper
import com.aps.coachfanapp.feature01.business.interactors.registerlogin.abstraction.LoginUser
import com.aps.coachfanapp.feature01.framework.presentation.activity01.state.RegisterLoginActivityViewState
import com.google.firebase.auth.FirebaseUser
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

private const val TAG = "LoginUser"
private const val LOG_ME = true

class LoginUserImpl
@Inject
constructor(
    private val cacheDataSource: UserCacheDataSource,
    private val networkDataSource: UserNetworkDataSource,
    private val entityFactory: UserFactory,
    private val applicationContextProvider: ApplicationContextProvider,
    private val connectivityHelper: ConnectivityHelper,
): LoginUser {

    override fun loginUser(
        newEntity: ProjectUser,
        stateEvent: StateEvent,
        okButtonCallbackWhenNoInternetConnection: OkButtonCallback,
    ):Flow<DataState<RegisterLoginActivityViewState<ProjectUser>>?> = flow {
        val registerResult = safeApiCall(Dispatchers.IO){
            networkDataSource.loginUser(newEntity)
        }

        val apiResponse = object: ApiResponseHandler<RegisterLoginActivityViewState<ProjectUser>, FirebaseUser?>(
            response = registerResult,
            stateEvent = stateEvent,
            networkErrorMessage = applicationContextProvider.applicationContext(TAG).getString(
                R.string.NO_INTERNET_CONNECTION_ENABLE_AND_RESTART
            ),
            networkDataNullMessage = applicationContextProvider.applicationContext(TAG).getString(
                R.string.unsuccessful_login
            ),
            okButtonCallbackWhenNoInternetConnection = okButtonCallbackWhenNoInternetConnection,
            internetConnectionIsAvailable = connectivityHelper
                .internetConnectionIsAvailable(applicationContextProvider.applicationContext(TAG)),
        ){
            override suspend fun handleSuccess(resultObj: FirebaseUser?): DataState<RegisterLoginActivityViewState<ProjectUser>>? {
                return if(resultObj != null){
                    val viewState =
                        RegisterLoginActivityViewState<ProjectUser>(
                            firebaseUser = resultObj,
                            firebaseUserIsLoggedIn = true
                        )
                    DataState.data(
                        response = Response(
                            messageId = R.string.text_ok,
                            message = LOGIN_SUCCESS,
                            uiComponentType = UIComponentType.Toast(),
                            messageType = MessageType.Success()
                        ),
                        data = viewState,
                        stateEvent = stateEvent
                    )
                }
                else{
                    DataState.data(
                        response = Response(
                            messageId = R.string.error,
                            message = LOGIN_FAILED,
                            uiComponentType = UIComponentType.Toast(),
                            messageType = MessageType.Error()
                        ),
                        data = null,
                        stateEvent = stateEvent
                    )
                }
            }
        }.getResult()

        emit(apiResponse)
    }

    companion object{
        val LOGIN_SUCCESS = "Login successful."
        val LOGIN_FAILED = "Login failed."
    }
}