package com.aps.coachfanapp.feature01.business.interactors.registerlogin.abstraction


import com.aps.coachfanapp.common.business.domain.state.DataState
import com.aps.coachfanapp.common.business.domain.state.StateEvent
import com.aps.coachfanapp.core.business.domain.model.entities.ProjectUser
import com.aps.coachfanapp.feature01.framework.presentation.activity01.state.RegisterLoginActivityViewState
import kotlinx.coroutines.flow.Flow

interface SearchCachedUsers {
    fun searchEntities(
        query: String,
        filterAndOrder: String,
        page: Int,
        stateEvent: StateEvent
    ): Flow<DataState<RegisterLoginActivityViewState<ProjectUser>>?>
}