package com.aps.coachfanapp.feature01.framework.presentation.activity01.fragments

import android.os.Bundle
import android.os.Parcel
import android.os.Parcelable
import android.view.MotionEvent
import android.view.View
import android.widget.EditText
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.aps.coachfanapp.R
import com.aps.coachfanapp.common.business.domain.state.StateEvent
import com.aps.coachfanapp.common.framework.presentation.BaseMVIFragment
import com.aps.coachfanapp.common.framework.presentation.BaseViewModel
import com.aps.coachfanapp.common.util.ALog
import com.aps.coachfanapp.common.util.DateUtil
import com.aps.coachfanapp.common.util.extensions.hideKeyboard
import com.aps.coachfanapp.common.util.extensions.showKeyboard
import com.aps.coachfanapp.core.business.domain.model.entities.ProjectUser
import com.aps.coachfanapp.databinding.FragmentActivityRegisterloginLoginBinding
import com.aps.coachfanapp.feature01.framework.presentation.activity01.RegisterLoginActivityNavigation
import com.aps.coachfanapp.feature01.framework.presentation.activity01.RegisterLoginActivityViewModel
import com.aps.coachfanapp.feature01.framework.presentation.activity01.state.RegisterLoginActivityInteractionState
import com.aps.coachfanapp.feature01.framework.presentation.activity01.state.RegisterLoginActivityStateEvent
import com.aps.coachfanapp.feature01.framework.presentation.activity01.state.RegisterLoginActivityViewState
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.InternalCoroutinesApi

const val REGISTERLOGIN_LOGIN_STATE_BUNDLE_KEY = "com.aps.cleanarchitecturetemplateapplication.feature01.framework.presentation.activity01.fragments.login"

private const val TAG = "RegisterLoginActivityLoginFragment"
private const val LOG_ME = true

@FlowPreview
@ExperimentalCoroutinesApi
@AndroidEntryPoint
class RegisterLoginActivityLoginFragment
constructor(
        private val viewModelFactory: ViewModelProvider.Factory,
        private val dateUtil: DateUtil
): BaseMVIFragment<ProjectUser, RegisterLoginActivityViewState<ProjectUser>>(R.layout.fragment_activity_registerlogin_login),
    RegisterLoginActivityNavigation,
    View.OnTouchListener {

    private val viewModel: RegisterLoginActivityViewModel by viewModels {
        viewModelFactory
    }
    private lateinit var binding: FragmentActivityRegisterloginLoginBinding


    constructor(parcel: Parcel) : this(
            TODO("viewModelFactory"),
            TODO("dateUtil")) {
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.setupChannel()
    }

    @InternalCoroutinesApi
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupOnBackPressDispatcher()
    }

    override fun onResume() {
        super.onResume()
    }

    override fun onPause() {
        super.onPause()
        //Save data from ui in ViewModel if necessary
        updateEmailInViewModel()
        updatePasswordInViewModel()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
    }

    override fun restoreInstanceState(savedInstanceState: Bundle?){
    }

    private fun setupOnBackPressDispatcher() {
        val callback = object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                onBackPressed()
            }
        }
        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner, callback)
    }

    override fun onBackPressed() {
        super.onBackPressed()
        clearUIInViewModel()
        view?.hideKeyboard()
        findNavController().popBackStack()
    }
    override fun setupUI(){
        view?.hideKeyboard()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        updateUIInViewModel()
    }

    override fun setupBinding(view: View) {
        binding = FragmentActivityRegisterloginLoginBinding.bind(view)
    }

    companion object CREATOR : Parcelable.Creator<RegisterLoginActivityLoginFragment> {
        override fun createFromParcel(parcel: Parcel): RegisterLoginActivityLoginFragment {
            return RegisterLoginActivityLoginFragment(parcel)
        }

        override fun newArray(size: Int): Array<RegisterLoginActivityLoginFragment?> {
            return arrayOfNulls(size)
        }
    }

    override fun subscribeCustomObservers() {
        val methodName: String = "subscribeCustomObservers"
        viewModel.emailInteractionState.observe(viewLifecycleOwner, Observer { state ->
            when(state){
                is RegisterLoginActivityInteractionState.EditState -> {
                    if(LOG_ME) ALog.d(TAG, ".$methodName(): " +
                            "emailInteractionState is in EditState")
                    view?.showKeyboard()
                    viewModel.setIsUpdatePending(true)
                    if(LOG_ME)ALog.d(TAG, ".$methodName(): " +
                            "EditState: binding.emailEdittext.text == " +
                            "${binding.emailEdittext.text}")
                }

                is RegisterLoginActivityInteractionState.DefaultState -> {
                    if(LOG_ME)ALog.d(TAG, ".$methodName(): " +
                            "emailInteractionState is in DefaultState")
                    if(LOG_ME)ALog.d(TAG, ".$methodName(): " +
                            "DefaultState: binding.emailEdittext.text == " +
                            "${binding.emailEdittext.text}")
                }
            }
        })

        viewModel.passwordInteractionState.observe(viewLifecycleOwner, Observer { state ->
            when(state){
                is RegisterLoginActivityInteractionState.EditState -> {
                    if(LOG_ME)ALog.d(TAG, ".$methodName(): " +
                            "passwordInteractionState is in EditState")
                    view?.showKeyboard()
                    viewModel.setIsUpdatePending(true)
                    if(LOG_ME)ALog.d(TAG, ".$methodName(): " +
                            "EditState: binding.passwordEdittext.text == " +
                            "${binding.passwordEdittext.text}")
                }

                is RegisterLoginActivityInteractionState.DefaultState -> {
                    if(LOG_ME)ALog.d(TAG, ".$methodName(): " +
                            "passwordInteractionState is in DefaultState")
                    if(LOG_ME)ALog.d(TAG, ".$methodName(): " +
                            "DefaultState: binding.passwordEdittext.text == " +
                            "${binding.passwordEdittext.text}")
                }
            }
        })
    }

    override fun getViewModel(): BaseViewModel<ProjectUser, RegisterLoginActivityViewState<ProjectUser>> {
        return viewModel
    }

    override fun getInitStateEvent(): StateEvent {
        return RegisterLoginActivityStateEvent.None
    }

    override fun changeUiOrNavigateDependingOnViewState(
        viewState: RegisterLoginActivityViewState<ProjectUser>
    ) {
        if(viewState.firebaseUserIsLoggedIn != null) {
            if(viewState.firebaseUserIsLoggedIn!!) {
                if(viewState.appProjectUser!!.fullyVerified) {
                    this.navigateToHomeScreen(this, viewState.appProjectUser!!)
                } else {
                    this.navigateToProfileScreen(this, viewState.appProjectUser!!)
                }
            } else {
                //this.navigateToMainFragment()
                this.navigateToOptionsFragment()
            }
        }

        viewState.killAppAndOpenNetworkSettings?.let{
            if(it) {
                viewModel.setKillAppAndOpenNetworkSettings(null)
                closeApplicationAndOpenInternetSettings(this)
            }
        }

        viewState.isInternetAvailable?.let {
            if(!it) {
                hideUI()
            } else {
                showUI()
            }
            viewModel.setIsInternetAvailable(null)
        }
    }

    private fun hideUI() {
        binding.buttonLogin.visibility = View.INVISIBLE
        binding.emailEdittext.visibility = View.INVISIBLE
        binding.passwordEdittext.visibility = View.INVISIBLE
    }

    private fun showUI() {
        binding.buttonLogin.visibility = View.VISIBLE
        binding.emailEdittext.visibility = View.VISIBLE
        binding.passwordEdittext.visibility = View.VISIBLE
    }

    override fun onEntityUpdateSuccess() {

    }

    override fun onEntityDeleteSuccess() {
        this.navigateToMainFragment()
    }

    override fun getStateBundleKey(): String? {
        return REGISTERLOGIN_LOGIN_STATE_BUNDLE_KEY
    }

    override fun setAllOnClickAndOnTouchListeners() {
        binding.buttonLogin.setOnTouchListener(this)
        binding.emailEdittext.setOnTouchListener(this)
        binding.passwordEdittext.setOnTouchListener(this)
    }

    override fun updateUIInViewModel() {
        if(viewModel.checkEditState()){
            view?.hideKeyboard()
            if (LOG_ME) {
                ALog.d(TAG, "updateUIInViewModel():  User interface is in EditState")
                ALog.d(TAG, "updateUIInViewModel():  " +
                        "updating email with: " + getEmailBody())
                ALog.d(TAG, "updateUIInViewModel():  " +
                        "updating password with: " + getPasswordBody())
                ALog.d(TAG, "updateUIInViewModel(): " +
                        "updated appUser: == " + viewModel.getCurrentViewStateOrNew().appProjectUser)
            }
            updateEmailInViewModel()
            updatePasswordInViewModel()
            updateAppUser()
            if(LOG_ME)ALog.d(TAG, ".updateUIInViewModel(): " +
                    "After updateAppUser() appUser == " +
                    viewModel.getCurrentViewStateOrNew().appProjectUser)
            viewModel.exitEditState()
        }
    }

    private fun clearUIInViewModel() {
        val methodName: String = "clearUIInViewModel()"
        if (LOG_ME) ALog.d(TAG, "Method start: $methodName")
        try {
            viewModel.clearEmail()
            viewModel.clearPassword()
            viewModel.clearPasswordConfirmation()
            viewModel.clearAppUser()
        } catch (e: Exception) {
            ALog.e(TAG, methodName, e)
        } finally {
            if (LOG_ME) ALog.d(TAG, "Method end: $methodName")
        }
    }

    override fun onTouch(v: View?, event: MotionEvent?): Boolean {
        when(v) {
            binding.buttonLogin -> {
                when(event!!.action) {
                    MotionEvent.ACTION_DOWN -> {
                        if (LOG_ME) ALog.d(TAG, "onTouch(): binding.buttonLogin")
                        updateEmailInViewModel()
                        updatePasswordInViewModel()
                        if(allInputFieldsArentEmpty()) {
                            this.loginToFirestore()
                        } else {
                            if (LOG_ME) ALog.d(TAG, "onTouch(): " +
                                    "binding.buttonLogin: At least one input field is empty")
                        }
                    }
                }
            }

            binding.emailEdittext -> {
                when(event!!.action) {
                    MotionEvent.ACTION_DOWN -> {
                        if (LOG_ME) ALog.d(TAG, "onTouch(): binding.emailEdittext")
                        if(!viewModel.isEditingEmail()){
                            if (LOG_ME) ALog.d(TAG, "onTouch(): " +
                                    "Setting EditState for binding.emailEdittext")
                            viewModel.setEmailInteractionState(
                                RegisterLoginActivityInteractionState.EditState()
                            )
                        }
                    }
                }
            }

            binding.passwordEdittext -> {
                when(event!!.action) {
                    MotionEvent.ACTION_DOWN -> {
                        if (LOG_ME) ALog.d(TAG, "onTouch(): binding.passwordEdittext")
                        if(!viewModel.isEditingPassword()){
                            if (LOG_ME) ALog.d(TAG, "onTouch(): " +
                                    "Setting EditState for binding.passwordEdittext")
                            viewModel.setPasswordInteractionState(
                                RegisterLoginActivityInteractionState.EditState()
                            )
                        }
                    }
                }
            }
        }
        
        return false
    }

    override fun onClick(v: View?) {
        if (LOG_ME) ALog.d(TAG, "onClick(): Method start")
        super.onClick(v)
    }

    private fun updateAppUser() {
        if(viewModel.getIsUpdatePending()){
            viewModel.getCurrentViewStateOrNew().appProjectUser?.let {
                RegisterLoginActivityStateEvent.UpdateAppUserEvent(it)
            }?.let {
                if (LOG_ME) ALog.d(TAG, "updateAppUser(): Sending UpdateAppUserEvent")
                viewModel.setStateEvent(
                    it
                )
            }
        }
    }

    private fun loginToFirestore() {
        if (LOG_ME) ALog.d(TAG, "loginToFirestore(): Trying to log user into Firestore.")
        getViewModel().setStateEvent(RegisterLoginActivityStateEvent.LoginUserEvent)
    }

    private fun navigateToMainFragment() {
        findNavController().navigate(
            R.id.action_RegisterLoginActivityLoginFragment_to_RegisterLoginActivityMainFragment
        )
    }

    private fun navigateToOptionsFragment() {
        findNavController().navigate(
            R.id.action_RegisterLoginActivityLoginFragment_to_RegisterLoginActivityOptionsFragment
        )
    }

    private fun updateEmailInViewModel(){
        if (LOG_ME) ALog.d(TAG, "updateEmailInViewModel(): " +
                "Updating email ViewModel with " + getEmailBody())
        viewModel.updateEmail(getEmailBody())
    }

    private fun updatePasswordInViewModel(){
        if (LOG_ME) ALog.d(TAG, "updatePasswordInViewModel(): " +
                "Updating password ViewModel with " + getPasswordBody())
        viewModel.updatePassword(getPasswordBody())
    }

    private fun getEmailBody(): String {
        return binding.emailEdittext.text.toString()
    }

    private fun getPasswordBody(): String {
        return binding.passwordEdittext.text.toString()
    }

    private fun allInputFieldsArentEmpty(): Boolean {
            return editTextIsNotEmpty(binding.emailEdittext) &&
                    editTextIsNotEmpty(binding.passwordEdittext)
        }

    private fun editTextIsNotEmpty(editText: EditText): Boolean {
        return editText.text.toString().isNotEmpty()
    }
}