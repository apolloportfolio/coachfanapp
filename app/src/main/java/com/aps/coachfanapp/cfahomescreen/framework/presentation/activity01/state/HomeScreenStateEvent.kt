package com.aps.coachfanapp.cfahomescreen.framework.presentation.activity01.state

import android.content.Context
import com.aps.coachfanapp.R
import com.aps.coachfanapp.cfahomescreen.business.domain.model.entities.SportsFanMessage
import com.aps.coachfanapp.cfahomescreen.business.domain.model.entities.SportsGame
import com.aps.coachfanapp.cfahomescreen.business.domain.model.entities.SportsTeam
import com.aps.coachfanapp.common.business.domain.state.StateEvent
import com.aps.coachfanapp.common.business.domain.state.StateMessage
import com.aps.coachfanapp.core.business.domain.model.entities.ProjectUser
import com.aps.coachfanapp.core.util.ApplicationContextProvider
import java.io.File

sealed class HomeScreenStateEvent : StateEvent {

    class GetSportsGamesAroundUserStateEvent(
        val internalDirectory: File,
    ) : HomeScreenStateEvent() {
        override fun errorInfo() = eventName() + " error"
        override fun eventName() = "GetSportsGamesAroundUserStateEvent"
        override fun shouldDisplayProgressBar() = true
    }

    class GetSportsNewsAboutSportsGame(
        val sportsGame: SportsGame?
    ) : HomeScreenStateEvent() {
        override fun errorInfo() = eventName() + " error"
        override fun eventName() = "GetSportsNewsAboutSportsGame"
        override fun shouldDisplayProgressBar() = true
    }

    class SetCurrentlyShownSportsTeam(
        val sportsTeam: SportsTeam?
    ) : HomeScreenStateEvent() {
        override fun errorInfo() = eventName() + " error"
        override fun eventName() = "SetCurrentlyShownSportsTeam"
        override fun shouldDisplayProgressBar() = true
    }

    class SendSportsFanMessage(
        val sportsFanMessage: SportsFanMessage?
    ) : HomeScreenStateEvent() {
        override fun errorInfo() = eventName() + " error"
        override fun eventName() = "SendSportsFanMessage"
        override fun shouldDisplayProgressBar() = false
    }

    class ActOnEntity1(
        val sportsGame: SportsGame?,
        val callbackOnProfileIncomplete: () -> Unit
    ) : HomeScreenStateEvent() {
        override fun errorInfo() = eventName() + " error"
        override fun eventName() = "ActOnEntity1"
        override fun shouldDisplayProgressBar() = true
    }

    class UpdateMainEntityEvent(
        val mainEntity: ProjectUser
    ) : HomeScreenStateEvent() {
        override fun errorInfo() = eventName() + " error"
        override fun eventName() = "UpdateMainEntityEvent"
        override fun shouldDisplayProgressBar() = true
    }

    class CreateStateMessageEvent(
        val stateMessage: StateMessage,
    ): HomeScreenStateEvent(){
        override fun errorInfo() = eventName() + " error"
        override fun eventName() = "CreateStateMessageEvent"
        override fun shouldDisplayProgressBar() = false
    }

    data object None : HomeScreenStateEvent() {
        override fun errorInfo() = eventName() + " error"
        override fun eventName() = "None"
        override fun shouldDisplayProgressBar() = false
    }

    class SetDrawerOpen(
        val isDrawerOpen: Boolean?
    ) : HomeScreenStateEvent() {
        override fun errorInfo() = eventName() + " error"
        override fun eventName() = "SetDrawerOpen"
        override fun shouldDisplayProgressBar() = true
    }

    class SaveInFileAndClearSearchedEntities1ListStateEvent(
        val internalDirectory: File
    ) : HomeScreenStateEvent() {
        override fun errorInfo() = eventName() + " error"
        override fun eventName() = "SaveInFileAndClearSearchedEntities1ListStateEvent"
        override fun shouldDisplayProgressBar() = true
    }

    data object SearchEntities1StateEvent : HomeScreenStateEvent() {
        override fun errorInfo() = eventName() + " error"
        override fun eventName() = "SearchEntities1StateEvent"
        override fun shouldDisplayProgressBar() = true
    }

    class InitiateGooglePayPaymentProcess(
        val context: Context
    ) : HomeScreenStateEvent() {
        override fun errorInfo() = eventName() + " error"
        override fun eventName() = "InitiateGooglePayPaymentProcess"
        override fun shouldDisplayProgressBar() = true
    }

    class RateApplication(
        val rateAppFunction: () -> Unit
    ) : HomeScreenStateEvent() {
        override fun errorInfo() = eventName() + " error"
        override fun eventName() = "RateApplication"
        override fun shouldDisplayProgressBar() = false
    }

    class Navigate(
        val navigationFunction: () -> Unit
    ) : HomeScreenStateEvent() {
        override fun errorInfo() = eventName() + " error"
        override fun eventName() = "Navigate"
        override fun shouldDisplayProgressBar() = false
    }

    class RecommendApp(
        val recommendationFunction: () -> Unit
    ) : HomeScreenStateEvent() {
        override fun errorInfo() = eventName() + " error"
        override fun eventName() = "RecommendApp"
        override fun shouldDisplayProgressBar() = false
    }

    class ReportProblem(
        val reportProblemFunction: () -> Unit
    ) : HomeScreenStateEvent() {
        override fun errorInfo() = eventName() + " error"
        override fun eventName() = "ReportProblem"
        override fun shouldDisplayProgressBar() = false
    }



    data object ClearFiltersStateEvent : HomeScreenStateEvent() {
        override fun errorInfo() = eventName() + " error"
        override fun eventName() = "ClearFiltersStateEvent"
        override fun shouldDisplayProgressBar() = false
    }

    data object GetSportsTeamPlayersStateEvent : HomeScreenStateEvent() {
        override fun errorInfo() = eventName() + " error"
        override fun eventName() = "GetSportsTeamPlayersStateEvent"
        override fun shouldDisplayProgressBar() = true

    }

    data object ClearUIChangesInFragment5 : HomeScreenStateEvent() {
        override fun errorInfo() = eventName() + " error"
        override fun eventName() = "ClearUIChangesInFragment5"
        override fun shouldDisplayProgressBar() = true

    }

    data object AskUserAboutLogout : HomeScreenStateEvent() {
        override fun errorInfo() = eventName() + " error"
        override fun eventName() = "AskUserAboutLogout"
        override fun shouldDisplayProgressBar() = true
    }

    data object LogoutUser : HomeScreenStateEvent() {
        override fun errorInfo() = eventName() + " error"
        override fun eventName() = "LogoutUser"
        override fun shouldDisplayProgressBar() = true
    }

    class CheckGooglePayAvailability(
        val context: Context,
        val continueFlag: Boolean?,
    ) : HomeScreenStateEvent() {
        override fun errorInfo() = eventName() + " error"
        override fun eventName() = "CheckGooglePayAvailability"
        override fun shouldDisplayProgressBar() = true
    }

    class GetMerchantName(
        val continueFlag: Boolean?,
    ) : HomeScreenStateEvent() {
        override fun errorInfo() = eventName() + " error"
        override fun eventName() = "GetMerchantName"
        override fun shouldDisplayProgressBar() = true
    }

    class ShowDialogSayingThatGooglePayIsUnavailable(
        val context: Context,
    ): HomeScreenStateEvent() {
        override fun errorInfo() = eventName() + " error"
        override fun eventName() = "ShowDialogSayingThatGooglePayIsUnavailable"
        override fun shouldDisplayProgressBar() = true
    }

    class DownloadExchangeRates(
        val currencies: ArrayList<String>,
        val continueFlag: Boolean?,
        val applicationContextProvider: ApplicationContextProvider,
    ): HomeScreenStateEvent() {
        override fun errorInfo() = eventName() + " error"
        override fun eventName() = "DownloadExchangeRates"
        override fun shouldDisplayProgressBar() = true
    }

    class GetGatewayNameAndMerchantID(
        val continueFlag: Boolean?,
        val applicationContextProvider: ApplicationContextProvider,
    ): HomeScreenStateEvent() {
        override fun errorInfo() = eventName() + " error"
        override fun eventName() = "GetGatewayNameAndMerchantID"
        override fun shouldDisplayProgressBar() = true
    }

    data object GetEntities3 : HomeScreenStateEvent() {
        override fun errorInfo() = eventName() + " error"
        override fun eventName() = "GetEntities3"
        override fun shouldDisplayProgressBar() = true
    }

    data object GetEntities4 : HomeScreenStateEvent() {
        override fun errorInfo() = eventName() + " error"
        override fun eventName() = "GetEntities4"
        override fun shouldDisplayProgressBar() = true
    }

    data object NotifyUserAboutLackOfEmailAppInstalled : HomeScreenStateEvent() {
        override fun errorInfo() = eventName() + " error"
        override fun eventName() = "NotifyUserAboutLackOfEmailAppInstalled"
        override fun shouldDisplayProgressBar() = true
    }

    data object GetUsersRating : HomeScreenStateEvent() {
        override fun errorInfo() = eventName() + " error"
        override fun eventName() = "GetUsersRating"
        override fun shouldDisplayProgressBar() = true
    }
}