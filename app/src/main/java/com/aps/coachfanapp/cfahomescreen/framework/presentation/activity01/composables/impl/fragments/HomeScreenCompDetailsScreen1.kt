package com.aps.coachfanapp.cfahomescreen.framework.presentation.activity01.composables.impl.fragments

import android.app.Activity
import android.widget.Toast
import androidx.compose.animation.core.Spring
import androidx.compose.animation.core.spring
import androidx.compose.material.BottomSheetValue
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.SnackbarDuration
import androidx.compose.material.rememberBottomSheetState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import com.aps.coachfanapp.R
import com.aps.coachfanapp.cfahomescreen.business.domain.model.entities.SportsGame
import com.aps.coachfanapp.cfahomescreen.business.domain.model.entities.SportsNewsMessage
import com.aps.coachfanapp.cfahomescreen.business.domain.model.factories.SportsGameFactory
import com.aps.coachfanapp.cfahomescreen.business.domain.model.factories.SportsNewsMessageFactory
import com.aps.coachfanapp.cfahomescreen.framework.presentation.activity01.composables.impl.values.Dimens
import com.aps.coachfanapp.cfahomescreen.framework.presentation.activity01.composables.impl.values.HomeScreenTheme
import com.aps.coachfanapp.cfahomescreen.framework.presentation.activity01.state.HomeScreenStateEvent
import com.aps.coachfanapp.common.business.domain.state.StateEventTracker
import com.aps.coachfanapp.common.framework.presentation.PermissionHandlingData
import com.aps.coachfanapp.common.framework.presentation.views.DialogState
import com.aps.coachfanapp.common.framework.presentation.views.ProgressIndicatorState
import com.aps.coachfanapp.common.framework.presentation.views.ProgressIndicatorType
import com.aps.coachfanapp.common.framework.presentation.views.SnackBarState
import com.aps.coachfanapp.common.framework.presentation.views.SwipeableFragmentWithBottomSheetAndFAB
import com.aps.coachfanapp.common.framework.presentation.views.TipContentIsUnavailable
import com.aps.coachfanapp.common.framework.presentation.views.ToastState
import com.aps.coachfanapp.common.util.ALog

private const val TAG = "HomeScreenCompDetailsScreen1"
private const val LOG_ME = true
const val HomeScreenCompDetailsScreen1TestTag = TAG
const val HomeScreenCompDetailsScreen1CityTestTag = "HomeScreenCompDetailsScreen1CityTestTag"
const val HomeScreenCompDetailsScreen1ActionButtonTestTag = "HomeScreenCompDetailsScreen1ActionButtonTestTag"

@OptIn(ExperimentalMaterialApi::class)
@Composable
fun HomeScreenCompDetailsScreen1(
    onNewsClick: (SportsNewsMessage) -> Unit,

    launchStateEvent: (HomeScreenStateEvent) -> Unit = {},

    activity: Activity? = null,
    permissionHandlingData: PermissionHandlingData = PermissionHandlingData(),

    stateEventTracker: StateEventTracker,

    snackBarState: SnackBarState?,
    toastState: ToastState?,
    dialogState: DialogState?,
    progressIndicatorState: ProgressIndicatorState?,

    entity: SportsGame?,
    newsAboutSportsGame: List<SportsNewsMessage>?,
    actionOnEntity: (SportsGame?, ()-> Unit) -> Unit,
    navigateToProfileScreen: () -> Unit,
    isPreview: Boolean = false,
) {
    val permissionsRequiredInFragment = mutableSetOf<String>()

    val scope = rememberCoroutineScope()            // For bottom sheet if implemented.
    val sheetState = rememberBottomSheetState(
        initialValue = BottomSheetValue.Collapsed,
//        initialValue = BottomSheetValue.Expanded,
        animationSpec = spring(dampingRatio = Spring.DampingRatioHighBouncy)
    )
    val launchInitStateEvent: () -> Unit = {
        if(activity != null) {
            if(LOG_ME) ALog.d(TAG, "launchInitStateEvent(): " +
                    "activity != null, launching state event in view model.")
            launchStateEvent(
                HomeScreenStateEvent.GetSportsNewsAboutSportsGame(entity)
            )
        } else {
            if(LOG_ME) ALog.w(TAG, "launchInitStateEvent(): " +
                    "activity == null, couldn't set initial state event!")
        }
    }

    SwipeableFragmentWithBottomSheetAndFAB(
        launchInitStateEvent = launchInitStateEvent,
        activity = activity,
        permissionHandlingData = permissionHandlingData,
        permissionsRequiredInFragment = permissionsRequiredInFragment,
        backgroundDrawableId = R.drawable.default_background,
        onSwipeLeft = {},
        onSwipeRight = {},
        onSwipeUp = {},
        onSwipeDown = {},
        onSwipeRightFromComposableSide = {},
        onSwipeLeftFromComposableSide = {},
        onSwipeDownFromComposableSide = {},
        onSwipeUpFromComposableSide = {},
        leftSideThreshold = Dimens.fragmentsSwipingLeftSideThresholdFraction,
        floatingActionButtonDrawableId = null,
        floatingActionButtonOnClick = {},
        floatingActionButtonContentDescription = null,
        snackBarState = snackBarState,
        toastState = toastState,
        dialogState = dialogState,
        progressIndicatorState = progressIndicatorState,
        sheetState = sheetState,
        sheetContent = {},
        content = {
            if(entity == null) {
                TipContentIsUnavailable(tip = stringResource(id = R.string.no_content_to_show_due_to_an_error))
            } else {
                CoachFanAppGameDetailsScreenContent(
                    launchStateEvent = launchStateEvent,
                    activity = activity,
                    stateEventTracker = stateEventTracker,
                    refreshSportsNewsMessages = launchInitStateEvent,
                    sportsGame = entity,
                    sportsNewsList = newsAboutSportsGame,
                    isPreview = isPreview,
                    onNewsClick = onNewsClick,
                )
            }
        },
        isPreview = isPreview,
    )
}

//========================================================================================
@Preview
@Composable
fun HomeScreenComposableDetailsScreen1Preview() {
    val previewEntity = SportsGameFactory.createPreviewEntitiesList()[0]
    val navigateToProfileScreen = {}
    val actionOnEntity: (SportsGame?, ()-> Unit) -> Unit = { _, _ -> }
    HomeScreenTheme {
        // Start Interactive Mode to actually see SnackBar
        val snackBarState = remember{
            SnackBarState(
                show = true,
                message = "Snackbar",
                actionLabel = "action",
                iconDrawableRes = R.drawable.ic_launcher,
                onClickAction = {},
                onDismissAction = {},
                duration = SnackbarDuration.Indefinite,
                updateSnackBarInViewModel = {},
            )
        }
        snackBarState.onDismissAction = { snackBarState.show = false }
        snackBarState.onClickAction = { snackBarState.show = false }

        val toastState = ToastState(
            show = true,
            message = "This is a Toast from Preview!",
            duration = Toast.LENGTH_LONG,
            updateToastInViewModel = {},
        )

        val dialogState = DialogState(
            show = false,
            onDismissRequest = {},
            title = "Dialog's Title",
            text = "Dialog's text.",
            leftButtonText = "Dismiss",
            leftButtonOnClick = {},
            rightButtonText = "Confirm",
            rightButtonOnClick = {},
            updateDialogInViewModel = {},
        )

        val progressIndicatorState = ProgressIndicatorState(
            show = false,
            progress = 0.7f,
            type = ProgressIndicatorType.DeterminateCircularProgressIndicator,
            updateProgressIndicatorStateInViewModel = {},
        )

        HomeScreenCompDetailsScreen1(
            onNewsClick = {},
            stateEventTracker = StateEventTracker(),
            snackBarState = snackBarState,
            toastState = toastState,
            dialogState = dialogState,
            progressIndicatorState = progressIndicatorState,

            entity = previewEntity,
            newsAboutSportsGame = SportsNewsMessageFactory.createPreviewEntitiesList(),
            actionOnEntity = actionOnEntity,
            navigateToProfileScreen = navigateToProfileScreen,
        )
    }
}