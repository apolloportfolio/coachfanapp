package com.aps.coachfanapp.cfahomescreen.framework.datasources.cachedatasources.room.impl

import com.aps.coachfanapp.cfahomescreen.business.domain.model.entities.SportsTeamPlayer
import com.aps.coachfanapp.cfahomescreen.framework.datasources.cachedatasources.room.abs.SportsTeamPlayerDaoService
import com.aps.coachfanapp.cfahomescreen.framework.datasources.cachedatasources.room.daos.SportsTeamPlayerDao
import com.aps.coachfanapp.cfahomescreen.framework.datasources.cachedatasources.room.mappers.SportsTeamPlayerCacheMapper
import com.aps.coachfanapp.common.util.DateUtil
import com.aps.coachfanapp.common.util.UniqueID
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class SportsTeamPlayerDaoServiceImpl
@Inject
constructor(
    private val dao: SportsTeamPlayerDao,
    private val mapper: SportsTeamPlayerCacheMapper,
    private val dateUtil: DateUtil
): SportsTeamPlayerDaoService {

    override suspend fun insertOrUpdateEntity(entity: SportsTeamPlayer): SportsTeamPlayer {
        val databaseEntity = dao.getEntityById(entity.id)
        if(databaseEntity != null) {
            dao.insert(mapper.mapToEntity(entity))
            return mapper.mapFromEntity(databaseEntity)
        } else {
            this.updateEntity(
                entity.id,
                entity.updated_at,
                entity.created_at,

                entity.picture1URI,

                entity.description,

                entity.sportsTeamId,

                entity.playerName,
            )
            return mapper.mapFromEntity(dao.getEntityById(entity.id)!!)
        }
    }


    override suspend fun insertEntity(entity: SportsTeamPlayer): Long {
        return dao.insert(mapper.mapToEntity(entity))
    }

    override suspend fun insertEntities(entities: List<SportsTeamPlayer>): LongArray {
        return dao.insertEntities(
            mapper.mapToEntityList(entities)
        )
    }

    override suspend fun getEntityById(id: UniqueID?): SportsTeamPlayer? {
        return dao.getEntityById(id)?.let { entity ->
            mapper.mapFromEntity(entity)
        }
    }

    override suspend fun updateEntity(
        id : UniqueID?,
        updated_at: String?,
        created_at: String?,

        picture1URI: String?,

        description: String?,

        sportsTeamId: UniqueID?,

        playerName: String?,
    ): Int {
        return if(updated_at != null){
            dao.updateEntity(
                id,
                updated_at,
                created_at,

                picture1URI,

                description,

                sportsTeamId,

                playerName,
            )
        } else {
            dao.updateEntity(
                id,
                dateUtil.getCurrentTimestamp(),
                created_at,

                picture1URI,

                description,

                sportsTeamId,

                playerName,
            )
        }

    }

    override suspend fun deleteEntity(primaryKey: UniqueID?): Int {
        return dao.deleteEntity(primaryKey)
    }

    override suspend fun deleteEntities(entities: List<SportsTeamPlayer>): Int {
        val ids = entities.mapIndexed {index, value -> value.id}
        return dao.deleteEntities(ids)
    }

    override suspend fun searchEntities(): List<SportsTeamPlayer> {
        return mapper.mapFromEntityList(
            dao.searchEntities()
        )
    }

    override suspend fun getAllEntities(): List<SportsTeamPlayer> {
        return mapper.mapFromEntityList(dao.getAllEntities())
    }

    override suspend fun getNumEntities(): Int {
        return dao.getNumEntities()
    }
}