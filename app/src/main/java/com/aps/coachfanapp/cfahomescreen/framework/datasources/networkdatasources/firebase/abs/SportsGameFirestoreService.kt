package com.aps.coachfanapp.cfahomescreen.framework.datasources.networkdatasources.firebase.abs

import android.net.Uri
import com.aps.coachfanapp.common.util.UserUniqueID
import com.aps.coachfanapp.core.framework.datasources.networkdatasources.firebase.abstraction.BasicFirestoreService
import com.aps.coachfanapp.cfahomescreen.business.domain.model.entities.SportsGame
import com.aps.coachfanapp.cfahomescreen.business.interactors.impl.FirestoreSportsGameSearchParameters
import com.aps.coachfanapp.cfahomescreen.framework.datasources.networkdatasources.firebase.mappers.SportsGameFirestoreMapper
import com.aps.coachfanapp.cfahomescreen.framework.datasources.networkdatasources.firebase.model.SportsGameFirestoreEntity

interface SportsGameFirestoreService: BasicFirestoreService<
        SportsGame,
        SportsGameFirestoreEntity,
        SportsGameFirestoreMapper
        > {
    suspend fun searchEntities(
        searchParameters : FirestoreSportsGameSearchParameters
    ) : List<SportsGame>?

    suspend fun getUsersSportsGames(userID: UserUniqueID): List<SportsGame>?

    suspend fun uploadEntity1PhotoToFirestore(
        entity : SportsGame,
        entitysPhotoUri : Uri,
        entitysPhotoNumber: Int,
    ) : String?
}