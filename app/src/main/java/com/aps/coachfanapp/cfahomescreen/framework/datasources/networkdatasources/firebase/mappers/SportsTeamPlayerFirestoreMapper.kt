package com.aps.coachfanapp.cfahomescreen.framework.datasources.networkdatasources.firebase.mappers

import com.aps.coachfanapp.common.business.domain.model.EntityMapper
import com.aps.coachfanapp.cfahomescreen.business.domain.model.entities.SportsTeamPlayer
import com.aps.coachfanapp.cfahomescreen.framework.datasources.networkdatasources.firebase.model.SportsTeamPlayerFirestoreEntity
import javax.inject.Inject

class SportsTeamPlayerFirestoreMapper
@Inject
constructor() : EntityMapper<SportsTeamPlayerFirestoreEntity, SportsTeamPlayer> {
    override fun mapFromEntity(entity: SportsTeamPlayerFirestoreEntity): SportsTeamPlayer {
        return SportsTeamPlayer(
            entity.id,
            entity.created_at,
            entity.updated_at,

            entity.picture1URI,

            entity.description,

            entity.sportsTeamId,
            entity.playerName,
        )
    }

    override fun mapToEntity(domainModel: SportsTeamPlayer): SportsTeamPlayerFirestoreEntity {
        return SportsTeamPlayerFirestoreEntity(
            domainModel.id,
            domainModel.created_at,
            domainModel.updated_at,
            domainModel.picture1URI,
            domainModel.description,
            domainModel.sportsTeamId,
            domainModel.playerName,
        )
    }

    override fun mapFromEntityList(entities : List<SportsTeamPlayerFirestoreEntity>) : List<SportsTeamPlayer> {
        return entities.map{mapFromEntity(it)}
    }

    override fun mapToEntityList(entities: List<SportsTeamPlayer>): List<SportsTeamPlayerFirestoreEntity> {
        return entities.map{mapToEntity(it)}
    }
}