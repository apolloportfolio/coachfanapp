package com.aps.coachfanapp.cfahomescreen.di

import com.aps.coachfanapp.core.framework.datasources.networkdatasources.firebase.abstraction.*
import com.aps.coachfanapp.core.framework.datasources.networkdatasources.firebase.implementation.*
import com.aps.coachfanapp.cfahomescreen.framework.datasources.networkdatasources.firebase.abs.SportsGameFirestoreService
import com.aps.coachfanapp.cfahomescreen.framework.datasources.networkdatasources.firebase.abs.SportsTeamPlayerFirestoreService
import com.aps.coachfanapp.cfahomescreen.framework.datasources.networkdatasources.firebase.abs.SportsTeamFirestoreService
import com.aps.coachfanapp.cfahomescreen.framework.datasources.networkdatasources.firebase.abs.SportsNewsMessageFirestoreService
import com.aps.coachfanapp.cfahomescreen.framework.datasources.networkdatasources.firebase.impl.SportsGameFirestoreServiceImpl
import com.aps.coachfanapp.cfahomescreen.framework.datasources.networkdatasources.firebase.impl.SportsTeamPlayerFirestoreServiceImpl
import com.aps.coachfanapp.cfahomescreen.framework.datasources.networkdatasources.firebase.impl.SportsTeamFirestoreServiceImpl
import com.aps.coachfanapp.cfahomescreen.framework.datasources.networkdatasources.firebase.impl.SportsNewsMessageFirestoreServiceImpl
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
abstract class FirestoreDAOsModule {

    @Binds
    abstract fun bindSportsGameFirestoreService(implementation: SportsGameFirestoreServiceImpl): SportsGameFirestoreService

    @Binds
    abstract fun bindSportsTeamPlayerFirestoreService(implementation: SportsTeamPlayerFirestoreServiceImpl): SportsTeamPlayerFirestoreService

    @Binds
    abstract fun bindSportsTeamFirestoreService(implementation: SportsTeamFirestoreServiceImpl): SportsTeamFirestoreService

    @Binds
    abstract fun bindSportsNewsMessageFirestoreService(implementation: SportsNewsMessageFirestoreServiceImpl): SportsNewsMessageFirestoreService


}