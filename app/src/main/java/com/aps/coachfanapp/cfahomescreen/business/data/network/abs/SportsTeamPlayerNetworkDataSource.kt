package com.aps.coachfanapp.cfahomescreen.business.data.network.abs

import com.aps.coachfanapp.common.business.data.network.StandardNetworkDataSource
import com.aps.coachfanapp.common.util.UniqueID
import com.aps.coachfanapp.common.util.UserUniqueID
import com.aps.coachfanapp.cfahomescreen.business.domain.model.entities.SportsTeamPlayer

interface SportsTeamPlayerNetworkDataSource: StandardNetworkDataSource<SportsTeamPlayer> {
    override suspend fun insertOrUpdateEntity(entity: SportsTeamPlayer): SportsTeamPlayer?

    override suspend fun deleteEntity(primaryKey: UniqueID?)

    override suspend fun insertDeletedEntity(entity: SportsTeamPlayer)

    override suspend fun insertDeletedEntities(Entities: List<SportsTeamPlayer>)

    override suspend fun deleteDeletedEntity(entity: SportsTeamPlayer)

    override suspend fun getDeletedEntities(): List<SportsTeamPlayer>

    override suspend fun deleteAllEntities()

    override suspend fun searchEntity(entity: SportsTeamPlayer): SportsTeamPlayer?

    override suspend fun getAllEntities(): List<SportsTeamPlayer>

    override suspend fun insertOrUpdateEntities(Entities: List<SportsTeamPlayer>): List<SportsTeamPlayer>?

    override suspend fun getEntityById(id: UniqueID): SportsTeamPlayer?

    suspend fun getUsersEntities2(userId : UserUniqueID) : List<SportsTeamPlayer>?
}