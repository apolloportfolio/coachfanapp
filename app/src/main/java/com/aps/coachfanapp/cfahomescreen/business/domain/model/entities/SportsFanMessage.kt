package com.aps.coachfanapp.cfahomescreen.business.domain.model.entities

import android.os.Parcelable
import com.aps.coachfanapp.common.util.ALog
import com.aps.coachfanapp.common.util.UniqueID
import com.aps.coachfanapp.common.util.UserUniqueID
import com.aps.coachfanapp.core.util.ProjectConstants
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.StorageReference
import com.google.firebase.storage.ktx.storage
import kotlinx.android.parcel.Parcelize
import kotlinx.parcelize.IgnoredOnParcel

private const val TAG = "SportsFanMessage"
private const val LOG_ME = true

// Entity5
@Parcelize
data class SportsFanMessage(
    var id: UniqueID? = null,
    var created_at: String? = null,
    var updated_at: String? = null,
    var sender: UserUniqueID?,
    var senderName: String?,
    var senderPicture1URI: String? = null,
    var recipient: UserUniqueID? = null,
    var recipientName: String? = null,
    var message: String?,
    var gameId: UniqueID?,
    var seen: Boolean? = null,
) : Parcelable {

    var hasPictures:Boolean =
        senderPicture1URI != null

    @IgnoredOnParcel
    val senderPicture1FirebaseImageRef: StorageReference?
        get() {
            return if(id != null && senderPicture1URI != null) {
                if(LOG_ME) ALog.d(TAG, ".picture1ImageRef(): " +
                            "this.id!!.firestoreDocumentID == ${this.id!!.firestoreDocumentID}"
                )
                Firebase.storage.reference
                    .child(ProjectConstants.FIRESTORE_IMAGES_COLLECTION)
                    .child(ProjectConstants.FIRESTORE_PROFILE_PHOTOS_SUB_COLLECTION)
                    .child(this.id!!.firestoreDocumentID)
                    .child(this.senderPicture1URI!!)
            } else {
                null
            }
        }

    companion object {
        fun createPreviewEntitiesList(gameId: UniqueID): List<SportsFanMessage> {
            return listOf(
                SportsFanMessage(
                    senderName = "SportyFan123",
                    id = UniqueID(),
                    created_at = "2023-01-01",
                    updated_at = "2023-01-02",
                    sender = UserUniqueID("PreviewSender"),
                    recipient = UserUniqueID("PreviewRecipient"),
                    message = "Go Team A!",
                    gameId = gameId,
                    seen = true,
                ),
                SportsFanMessage(
                    senderName = "GameDayEnthusiast",
                    id = UniqueID(),
                    created_at = "2023-02-01",
                    updated_at = "2023-02-02",
                    sender = UserUniqueID("AnotherSender"),
                    recipient = UserUniqueID("AnotherRecipient"),
                    message = "Exciting match!",
                    gameId = gameId,
                    seen = false,
                ),
                SportsFanMessage(
                    senderName = "AthleteAdmirer",
                    id = UniqueID(),
                    created_at = "2023-03-01",
                    updated_at = "2023-03-02",
                    sender = UserUniqueID("Sender3"),
                    recipient = UserUniqueID("Recipient3"),
                    message = "Let's go Team B!",
                    gameId = gameId,
                    seen = true,
                ),
                SportsFanMessage(
                    senderName = "TeamSpiritSupporter",
                    id = UniqueID(),
                    created_at = "2023-04-01",
                    updated_at = "2023-04-02",
                    sender = UserUniqueID("Sender4"),
                    recipient = UserUniqueID("Recipient4"),
                    message = "Predicting a tie!",
                    gameId = gameId,
                    seen = false,
                ),
                SportsFanMessage(
                    senderName = "VictoryVoyager",
                    id = UniqueID(),
                    created_at = "2023-05-01",
                    updated_at = "2023-05-02",
                    sender = UserUniqueID("Sender5"),
                    recipient = UserUniqueID("Recipient5"),
                    message = "Can't wait for the game!",
                    gameId = gameId,
                    seen = true,
                ),
                SportsFanMessage(
                    senderName = "ScoreboardChamp",
                    id = UniqueID(),
                    created_at = "2023-06-01",
                    updated_at = "2023-06-02",
                    sender = UserUniqueID("Sender6"),
                    recipient = UserUniqueID("Recipient6"),
                    message = "Who's your favorite player?",
                    gameId = gameId,
                    seen = false,
                ),
                SportsFanMessage(
                    senderName = "FanaticFanfare",
                    id = UniqueID(),
                    created_at = "2023-07-01",
                    updated_at = "2023-07-02",
                    sender = UserUniqueID("Sender7"),
                    recipient = UserUniqueID("Recipient7"),
                    message = "Expecting a high-scoring match!",
                    gameId = gameId,
                    seen = true,
                ),
                SportsFanMessage(
                    senderName = "SportsSpectator",
                    id = UniqueID(),
                    created_at = "2023-08-01",
                    updated_at = "2023-08-02",
                    sender = UserUniqueID("Sender8"),
                    recipient = UserUniqueID("Recipient8"),
                    message = "Let the best team win!",
                    gameId = gameId,
                    seen = false,
                ),
                SportsFanMessage(
                    senderName = "CheerCaptain",
                    id = UniqueID(),
                    created_at = "2023-09-01",
                    updated_at = "2023-09-02",
                    sender = UserUniqueID("Sender9"),
                    recipient = UserUniqueID("Recipient9"),
                    message = "Predicting an upset!",
                    gameId = gameId,
                    seen = true,
                ),
                SportsFanMessage(
                    senderName = "GoalGetter99",
                    id = UniqueID(),
                    created_at = "2023-10-01",
                    updated_at = "2023-10-02",
                    sender = UserUniqueID("Sender10"),
                    recipient = UserUniqueID("Recipient10"),
                    message = "Cheering for both teams!",
                    gameId = gameId,
                    seen = true,
                )
            )
        }
    }
}