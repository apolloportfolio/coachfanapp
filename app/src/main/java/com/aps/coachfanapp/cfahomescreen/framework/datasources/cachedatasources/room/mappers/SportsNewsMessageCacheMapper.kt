package com.aps.coachfanapp.cfahomescreen.framework.datasources.cachedatasources.room.mappers

import com.aps.coachfanapp.common.business.domain.model.EntityMapper
import com.aps.coachfanapp.cfahomescreen.business.domain.model.entities.SportsNewsMessage
import com.aps.coachfanapp.cfahomescreen.framework.datasources.cachedatasources.room.model.SportsNewsMessageCacheEntity
import javax.inject.Inject

class SportsNewsMessageCacheMapper
@Inject
constructor() : EntityMapper<SportsNewsMessageCacheEntity, SportsNewsMessage> {
    override fun mapFromEntity(entity: SportsNewsMessageCacheEntity): SportsNewsMessage {
        return SportsNewsMessage(
            entity.id,
            entity.created_at,
            entity.updated_at,

            entity.latitude ,
            entity.longitude,
            entity.geoLocation,
            entity.firestoreGeoLocation,

            entity.picture1URI,
            entity.description,

            entity.title,
            entity.message,
            entity.sportType,
            entity.gameId,
            entity.teamId,
            entity.playerIds,
            )
    }

    override fun mapToEntity(domainModel: SportsNewsMessage): SportsNewsMessageCacheEntity {
        return SportsNewsMessageCacheEntity(
            domainModel.id!!,
            domainModel.created_at,
            domainModel.updated_at,

            domainModel.latitude ,
            domainModel.longitude,
            domainModel.geoLocation,
            domainModel.firestoreGeoLocation,

            domainModel.picture1URI,
            domainModel.description,

            domainModel.title,
            domainModel.message,
            domainModel.sportType,
            domainModel.gameId,
            domainModel.teamId,
            domainModel.playerIds,
        )
    }

    override fun mapFromEntityList(entities : List<SportsNewsMessageCacheEntity>) : List<SportsNewsMessage> {
        return entities.map{mapFromEntity(it)}
    }

    override fun mapToEntityList(entities: List<SportsNewsMessage>): List<SportsNewsMessageCacheEntity> {
        return entities.map{mapToEntity(it)}
    }
}