package com.aps.coachfanapp.cfahomescreen.framework.datasources.cachedatasources.room.model

import androidx.annotation.NonNull
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.aps.coachfanapp.common.util.UniqueID

@Entity(tableName = "sportsteamplayer")
data class SportsTeamPlayerCacheEntity(
    @PrimaryKey(autoGenerate = false)
    @ColumnInfo(name = "id")
    @NonNull
    var id : UniqueID,

    @ColumnInfo(name = "updated_at")
    val updated_at: String?,

    @ColumnInfo(name = "created_at")
    val created_at: String?,

    @ColumnInfo(name = "picture1URI")
    var picture1URI: String?,

    @ColumnInfo(name = "description")
    var description : String?,

    @ColumnInfo(name = "sportsTeamId")
    var sportsTeamId: UniqueID?,

    @ColumnInfo(name = "playerName")
    var playerName : String?,

    ) {
}