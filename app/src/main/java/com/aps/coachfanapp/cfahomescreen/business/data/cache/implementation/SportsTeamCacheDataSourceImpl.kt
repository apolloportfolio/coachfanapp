package com.aps.coachfanapp.cfahomescreen.business.data.cache.implementation


import com.aps.coachfanapp.common.util.UniqueID
import com.aps.coachfanapp.common.util.UserUniqueID
import com.aps.coachfanapp.cfahomescreen.business.data.cache.abstraction.SportsTeamCacheDataSource
import com.aps.coachfanapp.cfahomescreen.business.domain.model.entities.SportsTeam
import com.aps.coachfanapp.cfahomescreen.framework.datasources.cachedatasources.room.abs.SportsTeamDaoService
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class SportsTeamCacheDataSourceImpl
@Inject
constructor(
    private val entityDaoService: SportsTeamDaoService
): SportsTeamCacheDataSource {

    override suspend fun insertOrUpdateEntity(entity: SportsTeam): SportsTeam? {
        return entityDaoService.insertOrUpdateEntity(entity)
    }

    override suspend fun insertEntity(entity: SportsTeam): Long {
        return entityDaoService.insertEntity(entity)
    }

    override suspend fun deleteEntity(primaryKey: UniqueID?): Int {
        return entityDaoService.deleteEntity(primaryKey)
    }

    override suspend fun deleteEntities(entities: List<SportsTeam>): Int {
        return entityDaoService.deleteEntities(entities)
    }

    override suspend fun updateEntity(
        id : UniqueID?,
        updated_at: String?,
        created_at: String?,

        picture1URI: String?,

        description: String?,

        ownerID: UserUniqueID?,
    ): Int {
        return entityDaoService.updateEntity(
            id,
            updated_at,
            created_at,

            picture1URI,

            description,

            ownerID,
        )
    }

    override suspend fun searchEntities(
        query: String,
        filterAndOrder: String,
        page: Int
    ): List<SportsTeam> {
        return entityDaoService.searchEntities()
    }

    override suspend fun getAllEntities(): List<SportsTeam> {
        return entityDaoService.getAllEntities()
    }

    override suspend fun getEntityById(id: UniqueID?): SportsTeam? {
        return entityDaoService.getEntityById(id)
    }

    override suspend fun getNumEntities(): Int {
        return entityDaoService.getNumEntities()
    }

    override suspend fun insertEntities(entities: List<SportsTeam>): LongArray{
        return entityDaoService.insertEntities(entities)
    }
}