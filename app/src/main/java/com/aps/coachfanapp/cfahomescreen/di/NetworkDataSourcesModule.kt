package com.aps.coachfanapp.cfahomescreen.di

import com.aps.coachfanapp.cfahomescreen.business.data.network.abs.SportsGameNetworkDataSource
import com.aps.coachfanapp.cfahomescreen.business.data.network.abs.SportsTeamPlayerNetworkDataSource
import com.aps.coachfanapp.cfahomescreen.business.data.network.abs.SportsTeamNetworkDataSource
import com.aps.coachfanapp.cfahomescreen.business.data.network.abs.SportsNewsMessageNetworkDataSource
import com.aps.coachfanapp.cfahomescreen.business.data.network.impl.SportsGameNetworkDataSourceImpl
import com.aps.coachfanapp.cfahomescreen.business.data.network.impl.SportsTeamPlayerNetworkDataSourceImpl
import com.aps.coachfanapp.cfahomescreen.business.data.network.impl.SportsTeamNetworkDataSourceImpl
import com.aps.coachfanapp.cfahomescreen.business.data.network.impl.SportsNewsMessageNetworkDataSourceImpl
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
abstract class NetworkDataSourcesModule {

    @Binds
    abstract fun bindSportsGameNetworkDataSource(implementation: SportsGameNetworkDataSourceImpl): SportsGameNetworkDataSource

    @Binds
    abstract fun bindSportsTeamPlayerNetworkDataSource(implementation: SportsTeamPlayerNetworkDataSourceImpl): SportsTeamPlayerNetworkDataSource

    @Binds
    abstract fun bindSportsTeamNetworkDataSource(implementation: SportsTeamNetworkDataSourceImpl): SportsTeamNetworkDataSource

    @Binds
    abstract fun bindSportsNewsMessageNetworkDataSource(implementation: SportsNewsMessageNetworkDataSourceImpl): SportsNewsMessageNetworkDataSource

}