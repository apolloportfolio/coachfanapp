package com.aps.coachfanapp.cfahomescreen.di

import com.aps.coachfanapp.core.business.data.cache.abstraction.*
import com.aps.coachfanapp.core.business.data.cache.implementation.*
import com.aps.coachfanapp.cfahomescreen.business.data.cache.abstraction.SportsGameCacheDataSource
import com.aps.coachfanapp.cfahomescreen.business.data.cache.abstraction.SportsTeamPlayerCacheDataSource
import com.aps.coachfanapp.cfahomescreen.business.data.cache.abstraction.SportsTeamCacheDataSource
import com.aps.coachfanapp.cfahomescreen.business.data.cache.abstraction.SportsNewsMessageCacheDataSource
import com.aps.coachfanapp.cfahomescreen.business.data.cache.implementation.SportsGameCacheDataSourceImpl
import com.aps.coachfanapp.cfahomescreen.business.data.cache.implementation.SportsTeamPlayerCacheDataSourceImpl
import com.aps.coachfanapp.cfahomescreen.business.data.cache.implementation.SportsTeamCacheDataSourceImpl
import com.aps.coachfanapp.cfahomescreen.business.data.cache.implementation.SportsNewsMessageCacheDataSourceImpl
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
abstract class CacheDataSourcesModule {

    @Binds
    abstract fun bindSportsGameCacheDataSource(implementation: SportsGameCacheDataSourceImpl): SportsGameCacheDataSource

    @Binds
    abstract fun bindSportsTeamPlayerCacheDataSource(implementation: SportsTeamPlayerCacheDataSourceImpl): SportsTeamPlayerCacheDataSource

    @Binds
    abstract fun bindSportsTeamCacheDataSource(implementation: SportsTeamCacheDataSourceImpl): SportsTeamCacheDataSource

    @Binds
    abstract fun bindSportsNewsMessageCacheDataSource(implementation: SportsNewsMessageCacheDataSourceImpl): SportsNewsMessageCacheDataSource

    // Unavailable in demo project.
//    @Binds
//    abstract fun bindSportsFanMessageCacheDataSource(implementation: SportsFanMessageCacheDataSourceImpl): SportsFanMessageCacheDataSource
}