package com.aps.coachfanapp.cfahomescreen.business.data.cache.implementation

import com.aps.coachfanapp.common.util.ParcelableGeoPoint
import com.aps.coachfanapp.common.util.UniqueID
import com.aps.coachfanapp.cfahomescreen.business.data.cache.abstraction.SportsNewsMessageCacheDataSource
import com.aps.coachfanapp.cfahomescreen.business.domain.model.entities.SportsNewsMessage
import com.aps.coachfanapp.cfahomescreen.framework.datasources.cachedatasources.room.abs.SportsNewsMessageDaoService
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class SportsNewsMessageCacheDataSourceImpl
@Inject
constructor(
    private val entityDaoService: SportsNewsMessageDaoService
): SportsNewsMessageCacheDataSource {

    override suspend fun insertOrUpdateEntity(entity: SportsNewsMessage): SportsNewsMessage? {
        return entityDaoService.insertOrUpdateEntity(entity)
    }

    override suspend fun insertEntity(entity: SportsNewsMessage): Long {
        return entityDaoService.insertEntity(entity)
    }

    override suspend fun deleteEntity(primaryKey: UniqueID?): Int {
        return entityDaoService.deleteEntity(primaryKey)
    }

    override suspend fun deleteEntities(entities: List<SportsNewsMessage>): Int {
        return entityDaoService.deleteEntities(entities)
    }

    override suspend fun updateEntity(
        id: UniqueID?,
        created_at: String?,
        updated_at: String?,

        lattitude : Double?,
        longitude: Double?,
        geoLocation: ParcelableGeoPoint?,
        firestoreGeoLocation: Double?,

        picture1URI: String?,
        description: String?,

        title: String?,
        message: String?,
        sportType: String?,
        gameId: UniqueID?,
        teamId: UniqueID?,
        playerIds: String?,
    ): Int {
        return entityDaoService.updateEntity(
            id,
            created_at,
            updated_at,

            lattitude,
            longitude,
            geoLocation,
            firestoreGeoLocation,

            picture1URI,
            description,
            title,
            message,
            sportType,
            gameId,
            teamId,
            playerIds,
            )
    }

    override suspend fun searchEntities(
        query: String,
        filterAndOrder: String,
        page: Int
    ): List<SportsNewsMessage> {
        return entityDaoService.searchEntities()
    }

    override suspend fun getAllEntities(): List<SportsNewsMessage> {
        return entityDaoService.getAllEntities()
    }

    override suspend fun getEntityById(id: UniqueID?): SportsNewsMessage? {
        return entityDaoService.getEntityById(id)
    }

    override suspend fun getNumEntities(): Int {
        return entityDaoService.getNumEntities()
    }

    override suspend fun insertEntities(entities: List<SportsNewsMessage>): LongArray{
        return entityDaoService.insertEntities(entities)
    }
}