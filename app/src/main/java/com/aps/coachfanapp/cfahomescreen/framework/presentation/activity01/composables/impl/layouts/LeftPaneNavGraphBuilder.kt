package com.aps.coachfanapp.cfahomescreen.framework.presentation.activity01.composables.impl.layouts

import android.app.Activity
import androidx.compose.runtime.Composable
import androidx.compose.ui.res.stringResource
import androidx.navigation.NavGraphBuilder
import androidx.navigation.compose.composable
import com.aps.coachfanapp.cfahomescreen.business.domain.model.entities.SportsFanMessage
import com.aps.coachfanapp.common.business.domain.state.StateEventTracker
import com.aps.coachfanapp.common.framework.presentation.PermissionHandlingData
import com.aps.coachfanapp.common.framework.presentation.views.DialogState
import com.aps.coachfanapp.common.framework.presentation.views.ProgressIndicatorState
import com.aps.coachfanapp.common.framework.presentation.views.SnackBarState
import com.aps.coachfanapp.common.framework.presentation.views.ToastState
import com.aps.coachfanapp.common.util.DeviceLocation
import com.aps.coachfanapp.cfahomescreen.business.domain.model.entities.SportsGame
import com.aps.coachfanapp.cfahomescreen.business.domain.model.entities.SportsTeam
import com.aps.coachfanapp.cfahomescreen.business.domain.model.entities.SportsNewsMessage
import com.aps.coachfanapp.cfahomescreen.framework.presentation.activity01.composables.HomeScreenDestination
import com.aps.coachfanapp.cfahomescreen.framework.presentation.activity01.composables.impl.fragments.HomeScreenCompDetailsScreen1
import com.aps.coachfanapp.cfahomescreen.framework.presentation.activity01.composables.impl.fragments.HomeScreenCompDetailsScreen2
import com.aps.coachfanapp.cfahomescreen.framework.presentation.activity01.composables.impl.fragments.HomeScreenCompDetailsScreen3
import com.aps.coachfanapp.cfahomescreen.framework.presentation.activity01.composables.impl.fragments.HomeScreenCompDetailsScreen4
import com.aps.coachfanapp.cfahomescreen.framework.presentation.activity01.composables.impl.fragments.HomeScreenCompDetailsScreen5
import com.aps.coachfanapp.cfahomescreen.framework.presentation.activity01.composables.impl.fragments.HomeScreenCompDetailsScreenPlaceholder
import com.aps.coachfanapp.cfahomescreen.framework.presentation.activity01.composables.impl.fragments.HomeScreenCompFragment1
import com.aps.coachfanapp.cfahomescreen.framework.presentation.activity01.composables.impl.fragments.HomeScreenComposableFragment1BottomSheetActions
import com.aps.coachfanapp.cfahomescreen.framework.presentation.activity01.composables.impl.fragments.HomeScreenCompFragment2
import com.aps.coachfanapp.cfahomescreen.framework.presentation.activity01.composables.impl.fragments.HomeScreenComposableFragment2BottomSheetActions
import com.aps.coachfanapp.cfahomescreen.framework.presentation.activity01.composables.impl.fragments.HomeScreenCompFragment3
import com.aps.coachfanapp.cfahomescreen.framework.presentation.activity01.composables.impl.fragments.HomeScreenComposableFragment3BottomSheetActions
import com.aps.coachfanapp.cfahomescreen.framework.presentation.activity01.composables.impl.fragments.HomeScreenCompFragment4
import com.aps.coachfanapp.cfahomescreen.framework.presentation.activity01.composables.impl.fragments.HomeScreenComposableFragment4BottomSheetActions
import com.aps.coachfanapp.cfahomescreen.framework.presentation.activity01.composables.impl.fragments.HomeScreenCompFragment5
import com.aps.coachfanapp.cfahomescreen.framework.presentation.activity01.composables.impl.fragments.HomeScreenComposableFragment5BottomSheetActions
import com.aps.coachfanapp.cfahomescreen.framework.presentation.activity01.state.HomeScreenCard1SearchFilters
import com.aps.coachfanapp.cfahomescreen.framework.presentation.activity01.state.HomeScreenStateEvent
import com.aps.coachfanapp.common.framework.presentation.views.TipContentIsUnavailable
import com.aps.coachfanapp.common.util.UniqueID
import com.aps.coachfanapp.common.util.UserUniqueID
import com.aps.coachfanapp.core.business.domain.model.entities.ProjectUser
import com.aps.coachfanapp.R

private const val TAG = "LeftPaneNavGraphBuilder"
private const val LOG_ME = true
@Composable
internal fun LeftPaneNavGraphBuilder(

    launchStateEvent: (HomeScreenStateEvent) -> Unit,

    activity: Activity?,
    permissionHandlingData: PermissionHandlingData,

    stateEventTracker: StateEventTracker,

    deviceLocation: DeviceLocation?,
    snackBarState: SnackBarState?,
    toastState: ToastState?,
    dialogState: DialogState?,
    progressIndicatorState: ProgressIndicatorState?,

    navigateCardLeft: () -> Unit,
    navigateCardRight: () -> Unit,
    openDrawer: () -> Unit,
    navigateToProfileScreen: () -> Unit,

    floatingActionButtonDrawableIdCard1: Int? = null,
    floatingActionButtonOnClickCard1: (() -> Unit)? = null,
    floatingActionButtonContentDescriptionCard1: String? = null,
    card1EntitiesList: List<SportsGame>?,
    card1OnListItemClick: (SportsGame) -> Unit,
    showProfileStatusBar: Boolean,
    finishVerification: () -> Unit,
    card1SearchQuery: String,
    card1OnSearchQueryUpdate: (String) -> Unit,
    searchFilters: HomeScreenCard1SearchFilters,
    onSearchFiltersUpdated: (HomeScreenCard1SearchFilters) -> Unit,
    card1BottomSheetActions: HomeScreenComposableFragment1BottomSheetActions,
    card1CurrentlyShownEntity: SportsGame? = null,
    card1ActionOnEntity: (SportsGame?, ()-> Unit) -> Unit,

    floatingActionButtonDrawableIdCard2: Int? = null,
    floatingActionButtonOnClickCard2: (() -> Unit)? = null,
    floatingActionButtonContentDescriptionCard2: String? = null,
    card2EntitiesList: List<SportsFanMessage>?,
    card2OnListItemClick: (SportsFanMessage) -> Unit,
    card2BottomSheetActions: HomeScreenComposableFragment2BottomSheetActions,

    floatingActionButtonDrawableIdCard3: Int? = null,
    floatingActionButtonOnClickCard3: (() -> Unit)? = null,
    floatingActionButtonContentDescriptionCard3: String? = null,
    card3EntitiesList: List<SportsTeam>?,
    card3OnListItemClick: (SportsTeam) -> Unit,
    card3BottomSheetActions: HomeScreenComposableFragment3BottomSheetActions,

    floatingActionButtonDrawableIdCard4: Int? = null,
    floatingActionButtonOnClickCard4: (() -> Unit)? = null,
    floatingActionButtonContentDescriptionCard4: String? = null,
    card4EntitiesList: List<SportsNewsMessage>?,
    card4OnListItemClick: (SportsNewsMessage) -> Unit,
    card4BottomSheetActions: HomeScreenComposableFragment4BottomSheetActions,

    card5BottomSheetActions: HomeScreenComposableFragment5BottomSheetActions,

    currentlyShownSportsGame: SportsGame?,
    onSendSportsFanMessage: (String, UserUniqueID, UserUniqueID?, UniqueID?) -> Unit,
    user: ProjectUser?,
    currentSportsFanMessageRecipient: UserUniqueID?,
    sportsNewsMessages: List<SportsNewsMessage>?,
    onNewsClick: (SportsNewsMessage) -> Unit,
    onBackButtonPress: () -> Unit,
    favoriteTeams: List<SportsTeam>?,
    navigateToTeamDetails: (SportsTeam) -> Unit,

    isPreview: Boolean = false,
): NavGraphBuilder.() -> Unit {
    return {
        composable(HomeScreenDestination.Card1.route) {
            HomeScreenCompFragment1(
                launchStateEvent = launchStateEvent,
                activity = activity,
                permissionHandlingData = permissionHandlingData,
                stateEventTracker = stateEventTracker,
                deviceLocation = deviceLocation,
                showProfileStatusBar = showProfileStatusBar,
                finishVerification = finishVerification,
                initialSearchQuery = card1SearchQuery,
                entities = card1EntitiesList,
                sportsNewsMessages = sportsNewsMessages,
                onSearchQueryUpdate = card1OnSearchQueryUpdate,
                onListItemClick = card1OnListItemClick,
                onNewsClick = onNewsClick,
                searchFilters = searchFilters,
                onSearchFiltersUpdated = onSearchFiltersUpdated,
                onSwipeLeft = navigateCardRight,
                onSwipeRight = navigateCardLeft,
                onSwipeRightFromComposableSide = openDrawer,
                floatingActionButtonDrawableId = floatingActionButtonDrawableIdCard1,
                floatingActionButtonOnClick = floatingActionButtonOnClickCard1,
                floatingActionButtonContentDescription = floatingActionButtonContentDescriptionCard1,
                snackBarState = snackBarState,
                toastState = toastState,
                dialogState = dialogState,
                progressIndicatorState = progressIndicatorState,
                bottomSheetActions = card1BottomSheetActions,
                isPreview = isPreview,
            )
        }
        composable(HomeScreenDestination.Card2.route) {
            if(user == null) {
                TipContentIsUnavailable(tip = stringResource(id = R.string.no_content_to_show_due_to_an_error))
            } else {
                HomeScreenCompFragment2(
                    senderId = user.id!!,
                    recipientId = currentSportsFanMessageRecipient,
                    sportsGame = currentlyShownSportsGame,
                    onSendMessage = onSendSportsFanMessage,
                    launchStateEvent = launchStateEvent,
                    activity = activity,
                    permissionHandlingData = permissionHandlingData,
                    stateEventTracker = stateEventTracker,
                    deviceLocation = deviceLocation,
                    showProfileStatusBar = showProfileStatusBar,
                    finishVerification = finishVerification,
                    entities = card2EntitiesList,
                    onListItemClick = card2OnListItemClick,
                    onSwipeLeft = navigateCardRight,
                    onSwipeRight = navigateCardLeft,
                    onSwipeRightFromComposableSide = openDrawer,
                    floatingActionButtonDrawableId = floatingActionButtonDrawableIdCard2,
                    floatingActionButtonOnClick = floatingActionButtonOnClickCard2,
                    floatingActionButtonContentDescription = floatingActionButtonContentDescriptionCard2,
                    snackBarState = snackBarState,
                    toastState = toastState,
                    dialogState = dialogState,
                    progressIndicatorState = progressIndicatorState,
                    bottomSheetActions = card2BottomSheetActions,
                )
            }
        }
        composable(HomeScreenDestination.Card3.route) {
            HomeScreenCompFragment3(
                launchStateEvent = launchStateEvent,
                activity = activity,
                permissionHandlingData = permissionHandlingData,
                stateEventTracker = stateEventTracker,
                deviceLocation = deviceLocation,
                showProfileStatusBar = showProfileStatusBar,
                finishVerification = finishVerification,
                entities = card3EntitiesList,
                onListItemClick = card3OnListItemClick,
                onSwipeLeft = navigateCardRight,
                onSwipeRight = navigateCardLeft,
                onSwipeRightFromComposableSide = openDrawer,
                floatingActionButtonDrawableId = floatingActionButtonDrawableIdCard3,
                floatingActionButtonOnClick = floatingActionButtonOnClickCard3,
                floatingActionButtonContentDescription = floatingActionButtonContentDescriptionCard3,
                snackBarState = snackBarState,
                toastState = toastState,
                dialogState = dialogState,
                progressIndicatorState = progressIndicatorState,
                bottomSheetActions = card3BottomSheetActions,
            )
        }
        composable(HomeScreenDestination.Card4.route) {
            HomeScreenCompFragment4(
                launchStateEvent = launchStateEvent,
                activity = activity,
                permissionHandlingData = permissionHandlingData,
                stateEventTracker = stateEventTracker,
                deviceLocation = deviceLocation,
                showProfileStatusBar = showProfileStatusBar,
                finishVerification = finishVerification,
                entities = card4EntitiesList,
                onListItemClick = card4OnListItemClick,
                onSwipeLeft = navigateCardRight,
                onSwipeRight = navigateCardLeft,
                onSwipeRightFromComposableSide = openDrawer,
                floatingActionButtonDrawableId = floatingActionButtonDrawableIdCard4,
                floatingActionButtonOnClick = floatingActionButtonOnClickCard4,
                floatingActionButtonContentDescription = floatingActionButtonContentDescriptionCard4,
                snackBarState = snackBarState,
                toastState = toastState,
                dialogState = dialogState,
                progressIndicatorState = progressIndicatorState,
                bottomSheetActions = card4BottomSheetActions,
            )
        }
        composable(HomeScreenDestination.Card5.route) {
            HomeScreenCompFragment5(
                user = user,
                favoriteTeams = favoriteTeams,
                navigateToTeamDetails = navigateToTeamDetails,
                onBackClick = onBackButtonPress,
                launchStateEvent = launchStateEvent,
                activity = activity,
                permissionHandlingData = permissionHandlingData,
                stateEventTracker = stateEventTracker,
                onSwipeLeft = navigateCardRight,
                onSwipeRight = navigateCardLeft,
                onSwipeRightFromComposableSide = openDrawer,
                snackBarState = snackBarState,
                toastState = toastState,
                dialogState = dialogState,
                progressIndicatorState = progressIndicatorState,
                bottomSheetActions = card5BottomSheetActions,
                isPreview = isPreview,
            )
        }
        composable(HomeScreenDestination.DetailsScreen1.route) {
            HomeScreenCompDetailsScreen1(
                launchStateEvent = launchStateEvent,
                activity = activity,
                permissionHandlingData = permissionHandlingData,
                stateEventTracker = stateEventTracker,
                snackBarState = snackBarState,
                toastState = toastState,
                dialogState = dialogState,
                progressIndicatorState = progressIndicatorState,

                entity = card1CurrentlyShownEntity,
                newsAboutSportsGame = sportsNewsMessages ?: ArrayList<SportsNewsMessage>(),
                actionOnEntity = card1ActionOnEntity,
                navigateToProfileScreen = navigateToProfileScreen,
                onNewsClick = onNewsClick,
            )
        }
        composable(HomeScreenDestination.DetailsScreen2.route) {
            HomeScreenCompDetailsScreen2()
        }
        composable(HomeScreenDestination.DetailsScreen3.route) {
            HomeScreenCompDetailsScreen3()
        }
        composable(HomeScreenDestination.DetailsScreen4.route) {
            HomeScreenCompDetailsScreen4()
        }
        composable(HomeScreenDestination.DetailsScreen5.route) {
            HomeScreenCompDetailsScreen5()
        }
        composable(HomeScreenDestination.DetailsPlaceholderScreen.route) {
            HomeScreenCompDetailsScreenPlaceholder()
        }
    }
}