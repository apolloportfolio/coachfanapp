package com.aps.coachfanapp.cfahomescreen.business.data.network.abs

import com.aps.coachfanapp.common.business.data.network.StandardNetworkDataSource
import com.aps.coachfanapp.common.util.UniqueID
import com.aps.coachfanapp.common.util.UserUniqueID
import com.aps.coachfanapp.cfahomescreen.business.domain.model.entities.SportsTeam

interface SportsTeamNetworkDataSource: StandardNetworkDataSource<SportsTeam> {
    override suspend fun insertOrUpdateEntity(entity: SportsTeam): SportsTeam?

    override suspend fun deleteEntity(primaryKey: UniqueID?)

    override suspend fun insertDeletedEntity(entity: SportsTeam)

    override suspend fun insertDeletedEntities(Entities: List<SportsTeam>)

    override suspend fun deleteDeletedEntity(entity: SportsTeam)

    override suspend fun getDeletedEntities(): List<SportsTeam>

    override suspend fun deleteAllEntities()

    override suspend fun searchEntity(entity: SportsTeam): SportsTeam?

    override suspend fun getAllEntities(): List<SportsTeam>

    override suspend fun insertOrUpdateEntities(Entities: List<SportsTeam>): List<SportsTeam>?

    override suspend fun getEntityById(id: UniqueID): SportsTeam?

    suspend fun getUsersEntities3(userId: UserUniqueID): List<SportsTeam>?
}