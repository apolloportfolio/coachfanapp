package com.aps.coachfanapp.cfahomescreen.framework.presentation.activity01.composables.impl.fragments

import android.app.Activity
import android.location.Location
import android.widget.Toast
import androidx.compose.animation.core.Spring
import androidx.compose.animation.core.spring
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.tooling.preview.PreviewParameter
import androidx.compose.ui.tooling.preview.PreviewParameterProvider
import com.aps.coachfanapp.cfahomescreen.business.domain.model.entities.SportsFanMessage
import com.aps.coachfanapp.cfahomescreen.business.domain.model.entities.SportsGame
import com.aps.coachfanapp.common.business.domain.state.StateEventTracker
import com.aps.coachfanapp.common.framework.presentation.PermissionHandlingData
import com.aps.coachfanapp.common.framework.presentation.views.DialogState
import com.aps.coachfanapp.common.framework.presentation.views.ProgressIndicatorState
import com.aps.coachfanapp.common.framework.presentation.views.ProgressIndicatorType
import com.aps.coachfanapp.common.framework.presentation.views.SnackBarState
import com.aps.coachfanapp.common.framework.presentation.views.SwipeableFragmentWithBottomSheetAndFAB
import com.aps.coachfanapp.common.framework.presentation.views.ToastState
import com.aps.coachfanapp.common.util.ALog
import com.aps.coachfanapp.common.util.DeviceLocation
import com.aps.coachfanapp.cfahomescreen.framework.presentation.activity01.composables.impl.values.Dimens
import com.aps.coachfanapp.cfahomescreen.framework.presentation.activity01.composables.impl.values.HomeScreenTheme
import com.aps.coachfanapp.cfahomescreen.framework.presentation.activity01.state.HomeScreenStateEvent
import com.aps.coachfanapp.common.util.UniqueID
import com.aps.coachfanapp.common.util.UserUniqueID
import com.aps.coachfanapp.R

private const val TAG = "HomeScreenCompFragment2"
private const val LOG_ME = true

@OptIn(ExperimentalMaterialApi::class)
@Composable
fun HomeScreenCompFragment2(
    sportsGame: SportsGame?,
    onSendMessage: (String, UserUniqueID, UserUniqueID?, UniqueID?) -> Unit,
    senderId: UserUniqueID,
    recipientId: UserUniqueID?,

    launchStateEvent: (HomeScreenStateEvent) -> Unit = {},

    activity: Activity? = null,
    permissionHandlingData: PermissionHandlingData = PermissionHandlingData(),

    stateEventTracker: StateEventTracker,

    deviceLocation: DeviceLocation?,
    showProfileStatusBar: Boolean,
    finishVerification: () -> Unit,
    entities: List<SportsFanMessage>?,
    onListItemClick: (SportsFanMessage) -> Unit,
    onSwipeLeft: () -> Unit = {},
    onSwipeRight: () -> Unit = {},
    onSwipeUp: () -> Unit = {},
    onSwipeDown: () -> Unit = {},
    onSwipeRightFromComposableSide: () -> Unit = {},
    onSwipeLeftFromComposableSide: () -> Unit = {},
    onSwipeDownFromComposableSide: () -> Unit = {},
    onSwipeUpFromComposableSide: () -> Unit = {},
    floatingActionButtonDrawableId: Int? = null,
    floatingActionButtonOnClick: (() -> Unit)? = null,
    floatingActionButtonContentDescription: String? = null,
    snackBarState: SnackBarState?,
    toastState: ToastState?,
    dialogState: DialogState?,
    progressIndicatorState: ProgressIndicatorState?,
    bottomSheetActions: HomeScreenComposableFragment2BottomSheetActions,

    isPreview: Boolean = false,
) {
    val scope = rememberCoroutineScope()
    val sheetState = rememberBottomSheetState(
        initialValue = BottomSheetValue.Collapsed,
//        initialValue = BottomSheetValue.Expanded,
        animationSpec = spring(dampingRatio = Spring.DampingRatioHighBouncy)
    )
    val launchInitStateEvent: () -> Unit = {
        if(activity != null) {
            if(LOG_ME) ALog.w(TAG, "launchInitStateEvent(): " +
                    "activity != null, launching state event in view model.")
            launchStateEvent(
                HomeScreenStateEvent.GetSportsTeamPlayersStateEvent
            )
        } else {
            if(LOG_ME) ALog.w(TAG, "launchInitStateEvent(): " +
                    "activity == null, couldn't set initial state event!")
        }
    }
    SwipeableFragmentWithBottomSheetAndFAB(
        launchInitStateEvent = launchInitStateEvent,
        activity = activity,
        permissionHandlingData = permissionHandlingData,
        permissionsRequiredInFragment = mutableSetOf(),
        backgroundDrawableId = R.drawable.default_background,
        onSwipeLeft = onSwipeLeft,
        onSwipeRight = onSwipeRight,
        onSwipeUp = onSwipeUp,
        onSwipeDown = onSwipeDown,
        onSwipeRightFromComposableSide = onSwipeRightFromComposableSide,
        onSwipeLeftFromComposableSide = onSwipeLeftFromComposableSide,
        onSwipeDownFromComposableSide = onSwipeDownFromComposableSide,
        onSwipeUpFromComposableSide = onSwipeUpFromComposableSide,
        leftSideThreshold = Dimens.fragmentsSwipingLeftSideThresholdFraction,
        floatingActionButtonDrawableId = floatingActionButtonDrawableId,
        floatingActionButtonOnClick = floatingActionButtonOnClick,
        floatingActionButtonContentDescription = floatingActionButtonContentDescription,
        snackBarState = snackBarState,
        toastState = toastState,
        dialogState = dialogState,
        sheetState = sheetState,
        progressIndicatorState = progressIndicatorState,
        sheetContent = HomeScreenCompFragment2BottomSheet(
            scope = scope,
            sheetState = sheetState,
            leftButtonOnClick = bottomSheetActions.leftButtonOnClick,
            rightButtonOnClick = bottomSheetActions.rightButtonOnClick,
        ),
        content = {
            CoachFanAppChatScreenContent(
                senderId = senderId,
                recipientId = recipientId,
                stateEventTracker = stateEventTracker,
                launchInitStateEvent = launchInitStateEvent,
                sportsGame = sportsGame,
                messages = entities ?: ArrayList<SportsFanMessage>(),
                onSendMessage = onSendMessage,
                isPreview = isPreview,
            )
        }
    )
}

// Preview =========================================================================================
@Preview
@Composable
fun HomeScreenComposableFragment2Preview(
    @PreviewParameter(
        HomeScreenComposableFragment2ParamsProvider::class
    ) params: HomeScreenComposableFragment2Params
) {
    HomeScreenTheme {
        // Start Interactive Mode to actually see SnackBar
        val snackBarState = remember{
            SnackBarState(
                show = true,
                message = "Snackbar",
                actionLabel = "action",
                iconDrawableRes = R.drawable.ic_launcher,
                onClickAction = {},
                onDismissAction = {},
                duration = SnackbarDuration.Indefinite,
                updateSnackBarInViewModel = {},
            )
        }
        snackBarState.onDismissAction = { snackBarState.show = false }
        snackBarState.onClickAction = { snackBarState.show = false }

        val toastState = ToastState(
            show = true,
            message = "This is a Toast!",
            duration = Toast.LENGTH_LONG,
            updateToastInViewModel = {},
        )

        val dialogState = DialogState(
            show = true,
            onDismissRequest = {},
            title = "Dialog's Title",
            text = "Dialog's text.",
            leftButtonText = "Dismiss",
            leftButtonOnClick = {},
            rightButtonText = "Confirm",
            rightButtonOnClick = {},
            updateDialogInViewModel = {},
        )

        val bottomSheetActions = HomeScreenComposableFragment2BottomSheetActions(
            leftButtonOnClick = {},
            rightButtonOnClick = {},
        )

        val progressIndicatorState = ProgressIndicatorState(
            show = true,
            progress = 0.7f,
            type = ProgressIndicatorType.DeterminateCircularProgressIndicator,
            updateProgressIndicatorStateInViewModel = {},
        )

        HomeScreenCompFragment2(
            senderId = params.senderId,
            recipientId = params.recipientId,
            sportsGame = params.sportsGame,
            onSendMessage = { message, senderId, recipientId, gameId -> },
            stateEventTracker = StateEventTracker(),
            deviceLocation = params.deviceLocation,
            showProfileStatusBar = params.showProfileStatusBar,
            finishVerification = params.finishVerification,
            entities = params.entities,
            onListItemClick = params.onListItemClick,
            onSwipeLeft = params.onSwipeLeft,
            onSwipeRight = params.onSwipeRight,
            onSwipeUp = params.onSwipeUp,
            onSwipeDown = params.onSwipeDown,
            floatingActionButtonDrawableId = params.floatingActionButtonDrawableId,
            floatingActionButtonOnClick = params.floatingActionButtonOnClick,
            floatingActionButtonContentDescription = params.floatingActionButtonContentDescription,
            snackBarState = snackBarState,
            toastState = toastState,
            dialogState = dialogState,
            progressIndicatorState = progressIndicatorState,
            bottomSheetActions = bottomSheetActions,
        )
    }
}

class HomeScreenComposableFragment2ParamsProvider :
    PreviewParameterProvider<HomeScreenComposableFragment2Params> {
    val gameId = UniqueID("PreviewSportsGame")
    val sportsGame = SportsGame(
        id = gameId,
        created_at = "2023-01-01T12:00:00",
        updated_at = "2023-01-01T13:30:00",
        latitude = 37.7749,
        longitude = -122.4194,
        geoLocation = null,
        firestoreGeoLocation = 123.456,
        picture1URI = null,
        description = "Description of the sports game",
        city = "San Francisco",
        ownerID = null,
        name = "Football Game",
        switch1 = true,
        switch2 = false,
        switch3 = true,
        switch4 = false,
        switch5 = true,
        switch6 = false,
        switch7 = true,
        picture2URI = null,
        date = "2023-01-15",
        location = "Stadium XYZ",
        tvChannel = "ESPN",
        ticketPrices = "$50 - $200",
        currentScore = "Team A 2 - Team B 1",
        gameBreakdown = "Exciting match with unexpected turns",
        team1Id = UniqueID("TeamA"),
        team2Id = UniqueID("TeamB")
    )

    private val sportsFanMessages = SportsFanMessage.createPreviewEntitiesList(gameId)

    
    override val values: Sequence<HomeScreenComposableFragment2Params> = sequenceOf(
        HomeScreenComposableFragment2Params(
            senderId = UserUniqueID("PreviewSender"),
            recipientId = null,
            sportsGame = sportsGame,
            onSendMessage = {message, senderId, recipientId, gameId -> },
            deviceLocation = DeviceLocation(
                locationPermissionGranted = true,
                location = Location("Lublin").apply {
                    latitude = 51.2465
                    longitude = 22.5684
                }
            ),
            showProfileStatusBar = true,
            finishVerification = {},
            entities = sportsFanMessages,
            onListItemClick = {},
            onSwipeLeft = {},
            onSwipeRight = {},
            onSwipeUp = {},
            onSwipeDown = {},
            floatingActionButtonDrawableId = R.drawable.ic_search_yellow_24,
            floatingActionButtonOnClick = { },
            floatingActionButtonContentDescription = "FAB 2",
        )
    )
}

data class HomeScreenComposableFragment2Params(
    val senderId: UserUniqueID,
    val recipientId: UserUniqueID?,
    val sportsGame: SportsGame,
    val onSendMessage: (String, UserUniqueID, UserUniqueID?, UniqueID?) -> Unit,
    
    val deviceLocation: DeviceLocation?,
    val showProfileStatusBar: Boolean,
    val finishVerification: () -> Unit,
    val entities: List<SportsFanMessage>?,
    val onListItemClick: (SportsFanMessage) -> Unit,
    val onSwipeLeft: () -> Unit,
    val onSwipeRight: () -> Unit,
    val onSwipeUp: () -> Unit,
    val onSwipeDown: () -> Unit,
    val floatingActionButtonDrawableId: Int?,
    val floatingActionButtonOnClick: (() -> Unit)?,
    val floatingActionButtonContentDescription: String?,
)