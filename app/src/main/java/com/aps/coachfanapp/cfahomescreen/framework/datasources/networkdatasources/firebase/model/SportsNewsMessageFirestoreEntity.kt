package com.aps.coachfanapp.cfahomescreen.framework.datasources.networkdatasources.firebase.model

import androidx.room.Entity
import com.aps.coachfanapp.common.util.UniqueID
import com.google.firebase.firestore.GeoPoint
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.util.*

@Entity(tableName = "sportsnewsmessages")
data class SportsNewsMessageFirestoreEntity(
    @SerializedName("id")
    @Expose
    var id : UniqueID?,

    @SerializedName("created_at")
    @Expose
    val created_at: String?,

    @SerializedName("updated_at")
    @Expose
    var updated_at: String?,

    @SerializedName("lattitude")
    @Expose
    var latitude : Double?,

    @SerializedName("longitude")
    @Expose
    var longitude: Double?,

    @SerializedName("geoLocation")
    @Expose
    var geoLocation: GeoPoint?,

    @SerializedName("firestoreGeoLocation")
    @Expose
    var firestoreGeoLocation: Double?,

    @SerializedName("picture1URI")
    @Expose
    var picture1URI: String?,

    @SerializedName("description")
    @Expose
    var description : String?,


    @SerializedName("title")
    @Expose
    var title : String?,

    @SerializedName("message")
    @Expose
    var message : String?,

    @SerializedName("sportType")
    @Expose
    var sportType : String?,

    @SerializedName("gameId")
    @Expose
    var gameId : UniqueID?,

    @SerializedName("teamId")
    @Expose
    var teamId : UniqueID?,

    @SerializedName("playerIds")
    @Expose
    var playerIds : String?,

    ) {
    constructor() : this(
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
    )
}