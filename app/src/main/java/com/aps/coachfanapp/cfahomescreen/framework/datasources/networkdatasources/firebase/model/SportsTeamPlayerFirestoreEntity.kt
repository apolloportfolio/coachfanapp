package com.aps.coachfanapp.cfahomescreen.framework.datasources.networkdatasources.firebase.model

import androidx.room.Entity
import com.aps.coachfanapp.common.util.UniqueID
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.util.*

@Entity(tableName = "sportsteamplayers")
data class SportsTeamPlayerFirestoreEntity(
    @SerializedName("id")
    @Expose
    var id : UniqueID?,

    @SerializedName("created_at")
    @Expose
    val created_at: String?,

    @SerializedName("updated_at")
    @Expose
    var updated_at: String?,

    @SerializedName("picture1URI")
    @Expose
    var picture1URI: String?,

    @SerializedName("description")
    @Expose
    var description: String?,

    @SerializedName("ownerID")
    @Expose
    var sportsTeamId: UniqueID?,

    @SerializedName("playerName")
    @Expose
    var playerName: String?,

    ) {
    constructor() : this(
        null,
        null,
        null,
        null,
        null,
        null,
        null,
    )
}