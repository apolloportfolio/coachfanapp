package com.aps.coachfanapp.cfahomescreen.business.data.network.impl

import com.aps.coachfanapp.common.util.UniqueID
import com.aps.coachfanapp.common.util.UserUniqueID
import com.aps.coachfanapp.cfahomescreen.business.data.network.abs.SportsTeamPlayerNetworkDataSource
import com.aps.coachfanapp.cfahomescreen.business.domain.model.entities.SportsTeamPlayer
import com.aps.coachfanapp.cfahomescreen.framework.datasources.networkdatasources.firebase.abs.SportsTeamPlayerFirestoreService
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class SportsTeamPlayerNetworkDataSourceImpl
@Inject
constructor(
    private val firestoreService: SportsTeamPlayerFirestoreService
): SportsTeamPlayerNetworkDataSource {
    override suspend fun insertOrUpdateEntity(entity: SportsTeamPlayer): SportsTeamPlayer? {
        return firestoreService.insertOrUpdateEntity(entity)
    }

    override suspend fun deleteEntity(primaryKey: UniqueID?) {
        return firestoreService.deleteEntity(primaryKey)
    }

    override suspend fun insertDeletedEntity(entity: SportsTeamPlayer) {
        return firestoreService.insertDeletedEntity(entity)
    }

    override suspend fun insertDeletedEntities(entities: List<SportsTeamPlayer>) {
        return firestoreService.insertDeletedEntities(entities)
    }

    override suspend fun deleteDeletedEntity(entity: SportsTeamPlayer) {
        return firestoreService.deleteDeletedEntity(entity)
    }

    override suspend fun getDeletedEntities(): List<SportsTeamPlayer> {
        return firestoreService.getAllDeletedEntities()
    }

    override suspend fun deleteAllEntities() {
        firestoreService.deleteAllEntities()
    }

    override suspend fun searchEntity(entity: SportsTeamPlayer): SportsTeamPlayer? {
        return firestoreService.searchEntity(entity)
    }

    override suspend fun getAllEntities(): List<SportsTeamPlayer> {
        return firestoreService.getAllEntities()
    }

    override suspend fun insertOrUpdateEntities(entities: List<SportsTeamPlayer>): List<SportsTeamPlayer>? {
        return firestoreService.insertOrUpdateEntities(entities)
    }

    override suspend fun getEntityById(id: UniqueID): SportsTeamPlayer? {
        return firestoreService.getEntityById(id)
    }

    override suspend fun getUsersEntities2(userId : UserUniqueID) : List<SportsTeamPlayer>? {
        return firestoreService.getUsersSportsTeamPlayers(userId)
    }
}