package com.aps.coachfanapp.cfahomescreen.framework.datasources.networkdatasources.firebase.impl

import com.aps.coachfanapp.common.business.data.util.cLog
import com.aps.coachfanapp.common.util.ALog
import com.aps.coachfanapp.common.util.DateUtil
import com.aps.coachfanapp.common.util.UniqueID
import com.aps.coachfanapp.common.util.UserUniqueID
import com.aps.coachfanapp.common.util.extensions.getSizeString
import com.aps.coachfanapp.core.business.data.network.abstraction.UserNetworkDataSource
import com.aps.coachfanapp.core.framework.datasources.networkdatasources.firebase.implementation.BasicFirestoreServiceImpl
import com.aps.coachfanapp.cfahomescreen.business.domain.model.entities.SportsTeam
import com.aps.coachfanapp.cfahomescreen.framework.datasources.networkdatasources.firebase.abs.SportsTeamFirestoreService
import com.aps.coachfanapp.cfahomescreen.framework.datasources.networkdatasources.firebase.mappers.SportsTeamFirestoreMapper
import com.aps.coachfanapp.cfahomescreen.framework.datasources.networkdatasources.firebase.model.SportsTeamFirestoreEntity
import kotlinx.coroutines.tasks.await
import javax.inject.Inject
import javax.inject.Singleton


@Singleton
class SportsTeamFirestoreServiceImpl
@Inject
constructor(
//    private val firebaseAuth: FirebaseAuth,
//    private val firestore: FirebaseFirestore,
    private val networkMapper: SportsTeamFirestoreMapper,
    private val dateUtil: DateUtil,
    private val userNetworkDataSource: UserNetworkDataSource,
): SportsTeamFirestoreService,
    BasicFirestoreServiceImpl<SportsTeam, SportsTeamFirestoreEntity, SportsTeamFirestoreMapper>(
//        firebaseAuth,
//        firestore,
        networkMapper,
        dateUtil
    ) {

    override fun getCollectionName(): String {
        return COLLECTION_NAME
    }

    override fun getDeletesCollectionName(): String {
        return DELETES_COLLECTION_NAME
    }

    override fun setEntityID(entity: SportsTeam, id: UniqueID) {
        entity.id = id
    }

    override fun getEntityID(entity: SportsTeam): UniqueID? {
        return entity.id
    }

    override fun setFirestoreEntityID(entity: SportsTeamFirestoreEntity, id: UniqueID) {
        entity.id = id
    }

    override fun updateEntityTimestamp(entity: SportsTeam, timestamp: String){
        entity.updated_at = timestamp
    }

    override fun updateFirestoreEntityTimestamp(entity: SportsTeamFirestoreEntity, timestamp: String){
        entity.updated_at = timestamp
    }

    override fun getFirestoreEntityType(): Class<SportsTeamFirestoreEntity> {
        return SportsTeamFirestoreEntity::class.java
    }

    override fun getFirestoreEntityID(entity: SportsTeamFirestoreEntity): UniqueID? {
        return entity.id
    }

    override suspend fun getUsersSportsTeams(userId: UserUniqueID): List<SportsTeam>? {
        val methodName: String = "getUsersEntities3"
        var result: List<SportsTeam>
        ALog.d(TAG, "Method start: $methodName")
        try {
            val collectionName = getCollectionName()
            result = networkMapper.mapFromEntityList(
                firestore
                    .collection(getCollectionName())
                    .whereEqualTo(SportsTeam.ownerIDFieldName, userId)
                    .get()
                    .addOnFailureListener {
                        // send error reports to Firebase Crashlytics
                        cLog(it.message)
                        ALog.w(
                            TAG, "$methodName(): " +
                                "Failed to get user's entities3.")
                    }
                    .addOnSuccessListener {
                        if(LOG_ME) ALog.d(
                            TAG, "$methodName(): " +
                                "Successfully got all user's entities3.")
                    }
                    .await()
                    .toObjects(getFirestoreEntityType())
            )
            if(LOG_ME)ALog.d(
                TAG, ".$methodName(): " +
                    "Got ${result.getSizeString()} entities3 " +
                    "belonging to user: $userId stored in $collectionName")
            return result
        } catch (e: Exception) {
            ALog.e(TAG, methodName, e)
            return emptyList()
        } finally {
            ALog.d(TAG, "Method end: $methodName")
        }
    }

    companion object {
        const val TAG = "Entity3FirestoreServiceImpl"
        const val LOG_ME = true
        const val COLLECTION_NAME = "entity3"
        const val DELETES_COLLECTION_NAME = "entity3_d"
    }
}