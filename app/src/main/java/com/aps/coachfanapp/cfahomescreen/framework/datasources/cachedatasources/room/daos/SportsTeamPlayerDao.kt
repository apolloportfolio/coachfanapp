package com.aps.coachfanapp.cfahomescreen.framework.datasources.cachedatasources.room.daos

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.aps.coachfanapp.common.util.UniqueID
import com.aps.coachfanapp.cfahomescreen.framework.datasources.cachedatasources.room.model.SportsTeamPlayerCacheEntity
import java.util.*


@Dao
interface SportsTeamPlayerDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(entity : SportsTeamPlayerCacheEntity) : Long

    @Query("SELECT * FROM sportsteamplayer")
    suspend fun get() : List<SportsTeamPlayerCacheEntity>
    
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insertEntities(entities: List<SportsTeamPlayerCacheEntity>): LongArray

    @Query("SELECT * FROM sportsteamplayer WHERE id = :id")
    suspend fun getEntityById(id: UniqueID?): SportsTeamPlayerCacheEntity?

    @Query("DELETE FROM sportsteamplayer WHERE id IN (:ids)")
    suspend fun deleteEntities(ids: List<UniqueID?>): Int

    @Query("DELETE FROM sportsteamplayer")
    suspend fun deleteAllEntities()

    @Query("SELECT * FROM sportsteamplayer")
    suspend fun getAllEntities(): List<SportsTeamPlayerCacheEntity>

    @Query("""
        UPDATE sportsteamplayer 
        SET 
        created_at = :created_at,
        updated_at = :updated_at,
        picture1URI = :picture1URI,
        description = :description,
        sportsTeamId = :sportsTeamId,
        playerName = :playerName
        WHERE id = :id
        """)
    suspend fun updateEntity(
        id : UniqueID?,
        created_at: String?,
        updated_at: String?,

        picture1URI: String?,

        description: String?,

        sportsTeamId: UniqueID?,

        playerName: String?,
    ): Int

    @Query("DELETE FROM sportsteamplayer WHERE id = :primaryKey")
    suspend fun deleteEntity(primaryKey: UniqueID?): Int

    @Query("SELECT * FROM sportsteamplayer")
    suspend fun searchEntities(): List<SportsTeamPlayerCacheEntity>
    
    @Query("SELECT COUNT(*) FROM sportsteamplayer")
    suspend fun getNumEntities(): Int
}