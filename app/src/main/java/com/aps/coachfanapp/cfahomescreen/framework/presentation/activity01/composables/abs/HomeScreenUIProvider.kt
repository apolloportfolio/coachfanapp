package com.aps.coachfanapp.cfahomescreen.framework.presentation.activity01.composables.abs

import android.app.Activity
import androidx.compose.runtime.Composable
import com.aps.coachfanapp.cfahomescreen.framework.presentation.activity01.HomeScreenViewModelCompose
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview

fun interface HomeScreenUIProvider {
    @OptIn(ExperimentalCoroutinesApi::class, FlowPreview::class)
    fun getComposable(
        viewModel: HomeScreenViewModelCompose,
        activity: Activity,
    ): @Composable () -> Unit
}