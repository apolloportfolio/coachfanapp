package com.aps.coachfanapp.cfahomescreen.business.data.network.impl

import com.aps.coachfanapp.common.util.UniqueID
import com.aps.coachfanapp.cfahomescreen.business.data.network.abs.SportsNewsMessageNetworkDataSource
import com.aps.coachfanapp.cfahomescreen.business.domain.model.entities.SportsNewsMessage
import com.aps.coachfanapp.cfahomescreen.framework.datasources.networkdatasources.firebase.abs.SportsNewsMessageFirestoreService
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class SportsNewsMessageNetworkDataSourceImpl
@Inject
constructor(
    private val firestoreService: SportsNewsMessageFirestoreService
): SportsNewsMessageNetworkDataSource {
    override suspend fun insertOrUpdateEntity(entity: SportsNewsMessage): SportsNewsMessage? {
        return firestoreService.insertOrUpdateEntity(entity)
    }

    override suspend fun deleteEntity(primaryKey: UniqueID?) {
        return firestoreService.deleteEntity(primaryKey)
    }

    override suspend fun insertDeletedEntity(entity: SportsNewsMessage) {
        return firestoreService.insertDeletedEntity(entity)
    }

    override suspend fun insertDeletedEntities(entities: List<SportsNewsMessage>) {
        return firestoreService.insertDeletedEntities(entities)
    }

    override suspend fun deleteDeletedEntity(entity: SportsNewsMessage) {
        return firestoreService.deleteDeletedEntity(entity)
    }

    override suspend fun getDeletedEntities(): List<SportsNewsMessage> {
        return firestoreService.getAllDeletedEntities()
    }

    override suspend fun deleteAllEntities() {
        firestoreService.deleteAllEntities()
    }

    override suspend fun searchEntity(entity: SportsNewsMessage): SportsNewsMessage? {
        return firestoreService.searchEntity(entity)
    }

    override suspend fun getAllEntities(): List<SportsNewsMessage> {
        return firestoreService.getAllEntities()
    }

    override suspend fun insertOrUpdateEntities(entities: List<SportsNewsMessage>): List<SportsNewsMessage>? {
        return firestoreService.insertOrUpdateEntities(entities)
    }

    override suspend fun getEntityById(id: UniqueID): SportsNewsMessage? {
        return firestoreService.getEntityById(id)
    }
}