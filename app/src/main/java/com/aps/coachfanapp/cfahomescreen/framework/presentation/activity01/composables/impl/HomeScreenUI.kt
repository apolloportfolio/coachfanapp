package com.aps.coachfanapp.cfahomescreen.framework.presentation.activity01.composables.impl

import android.annotation.SuppressLint
import android.app.Activity
import androidx.activity.compose.BackHandler
import androidx.compose.runtime.*
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import androidx.navigation.NavHostController
import androidx.navigation.compose.rememberNavController
import com.aps.coachfanapp.cfahomescreen.business.domain.model.entities.SportsFanMessage
import com.aps.coachfanapp.common.business.domain.state.StateEvent
import com.aps.coachfanapp.common.business.domain.state.StateEventTracker
import com.aps.coachfanapp.common.framework.presentation.DrawerData
import com.aps.coachfanapp.common.framework.presentation.NavItem
import com.aps.coachfanapp.common.framework.presentation.PermissionHandlingData
import com.aps.coachfanapp.common.framework.presentation.layouts.DualPaneWithNavRailAndMovableNavHostLayout
import com.aps.coachfanapp.common.framework.presentation.layouts.SinglePaneWithBottomNavBarAndMovableNavHostLayout
import com.aps.coachfanapp.common.framework.presentation.views.DialogState
import com.aps.coachfanapp.common.framework.presentation.views.ProgressIndicatorState
import com.aps.coachfanapp.common.framework.presentation.views.SnackBarState
import com.aps.coachfanapp.common.framework.presentation.views.StandardNavigationDrawer
import com.aps.coachfanapp.common.framework.presentation.views.ToastState
import com.aps.coachfanapp.common.util.ALog
import com.aps.coachfanapp.common.util.DeviceLocation
import com.aps.coachfanapp.common.util.composableutils.isDualPane
import com.aps.coachfanapp.cfahomescreen.business.domain.model.entities.SportsGame
import com.aps.coachfanapp.cfahomescreen.business.domain.model.entities.SportsTeam
import com.aps.coachfanapp.cfahomescreen.business.domain.model.entities.SportsNewsMessage
import com.aps.coachfanapp.cfahomescreen.framework.presentation.activity01.composables.HomeScreenDestination
import com.aps.coachfanapp.cfahomescreen.framework.presentation.activity01.composables.impl.fragments.*
import com.aps.coachfanapp.cfahomescreen.framework.presentation.activity01.composables.impl.layouts.LeftNavHost
import com.aps.coachfanapp.cfahomescreen.framework.presentation.activity01.composables.impl.layouts.LeftPaneNavGraphBuilder
import com.aps.coachfanapp.cfahomescreen.framework.presentation.activity01.composables.impl.layouts.RightNavHost
import com.aps.coachfanapp.cfahomescreen.framework.presentation.activity01.composables.impl.layouts.RightPaneNavGraphBuilder
import com.aps.coachfanapp.cfahomescreen.framework.presentation.activity01.composables.impl.values.HomeScreenTheme
import com.aps.coachfanapp.cfahomescreen.framework.presentation.activity01.composables.impl.views.HomeScreenAppBar
import com.aps.coachfanapp.cfahomescreen.framework.presentation.activity01.state.HomeScreenCard1SearchFilters
import com.aps.coachfanapp.cfahomescreen.framework.presentation.activity01.state.HomeScreenStateEvent
import com.aps.coachfanapp.common.util.UniqueID
import com.aps.coachfanapp.common.util.UserUniqueID
import com.aps.coachfanapp.core.business.domain.model.entities.ProjectUser
import com.aps.coachfanapp.R

private const val TAG = "HomeScreenUI"
private const val LOG_ME = true

// This UI is composed of two panes or one pane depending on configuration.
// It uses two navigation controllers: one for the left pane and one for the right pane.
// Left pane is the one that is always shown whether two or one pane are visible.
// Left panes navigation graph has all possible destinations while right pane navigation graph has
// only destinations that show details screens or a placeholder.
// Details screens are shown on the right pane in dual pane mode.
// Other screens are shown on the left pane in dual pane mode.
// This UI will be automatically recomposed on configuration change.
@Composable
fun HomeScreenUI(
    currentlyShownSportsGame: SportsGame?,
    onSendSportsFanMessage: (String, UserUniqueID, UserUniqueID?, UniqueID?) -> Unit,
    user: ProjectUser?,
    currentSportsFanMessageRecipient: UserUniqueID?,
    sportsNewsMessages: List<SportsNewsMessage>?,
    onNewsClick: (SportsNewsMessage) -> Unit,
    favoriteTeams: ArrayList<SportsTeam>?,

    launchStateEvent: (StateEvent) -> Unit = {},

    activity: Activity? = null,
    permissionHandlingData: PermissionHandlingData,

    stateEventTracker: StateEventTracker,

    deviceLocation: DeviceLocation?,
    snackBarState: SnackBarState?,
    toastState: ToastState?,
    dialogState: DialogState?,
    progressIndicatorState: ProgressIndicatorState?,
    isDrawerOpen: Boolean?,

    updateOrientation: (Boolean) -> Unit,
    leftPaneStartDestination: String,
    rightPaneStartDestination: String,

    showProfileStatusBar: Boolean,
    card1SearchQuery: String,
    card1OnSearchQueryUpdate: (String) -> Unit,
    searchFilters: HomeScreenCard1SearchFilters,
    onSearchFiltersUpdated: (HomeScreenCard1SearchFilters) -> Unit,
    floatingActionButtonDrawableIdCard1: Int? = null,
    floatingActionButtonOnClickCard1: (() -> Unit)? = null,
    floatingActionButtonContentDescriptionCard1: String? = null,
    card1EntitiesList: List<SportsGame>?,
    card1OnListItemClick: (SportsGame) -> Unit,
    card1BottomSheetActions: HomeScreenComposableFragment1BottomSheetActions,
    card1CurrentlyShownEntity: SportsGame? = null,
    card1ActionOnEntity: (SportsGame?, ()-> Unit) -> Unit,

    floatingActionButtonDrawableIdCard2: Int? = null,
    floatingActionButtonOnClickCard2: (() -> Unit)? = null,
    floatingActionButtonContentDescriptionCard2: String? = null,
    card2EntitiesList: List<SportsFanMessage>?,
    card2OnListItemClick: (SportsFanMessage) -> Unit,
    card2BottomSheetActions: HomeScreenComposableFragment2BottomSheetActions,

    floatingActionButtonDrawableIdCard3: Int? = null,
    floatingActionButtonOnClickCard3: (() -> Unit)? = null,
    floatingActionButtonContentDescriptionCard3: String? = null,
    card3EntitiesList: List<SportsTeam>?,
    card3OnListItemClick: (SportsTeam) -> Unit,
    card3BottomSheetActions: HomeScreenComposableFragment3BottomSheetActions,

    floatingActionButtonDrawableIdCard4: Int? = null,
    floatingActionButtonOnClickCard4: (() -> Unit)? = null,
    floatingActionButtonContentDescriptionCard4: String? = null,
    card4EntitiesList: List<SportsNewsMessage>?,
    card4OnListItemClick: (SportsNewsMessage) -> Unit,
    card4BottomSheetActions: HomeScreenComposableFragment4BottomSheetActions,

    card5BottomSheetActions: HomeScreenComposableFragment5BottomSheetActions,

    isPreview: Boolean = false,
) {
    HomeScreenTheme {
        if(LOG_ME) ALog.d(TAG, "(): Recomposition start.")

        // NavController for the left pane in two pane mode.
        val leftNavController = rememberNavController()

        // NavController for the right pane in two pane mode and for single pane mode.
        val rightNavController = rememberNavController()
        val isDualPane = isDualPane()
        // rememberSaveable persists value over configuration changes and process death.
        var wasDualPane by rememberSaveable { mutableStateOf(isDualPane) }
        var navigateBack by remember { mutableStateOf(false) }

        // This is on click listener only for screens that either have a list of items or
        // otherwise show a details screen upon user's input.
        // Those screens are shown on left pane in dual pane mode.
        val onItemClickNavigationListScreens: (
            Boolean,
            NavItem,
            NavHostController?,
            NavHostController?,
        ) -> Unit = remember {
            { isDualPane, navItem, leftNavController, rightNavController ->
                if(isDualPane) {
                    // In dual pane mode if menu option gets clicked, then we navigate to it in left
                    // navigation controller.
                    // Then we navigate in right navigation controller to the details placeholder screen.
                    leftNavController?.navigate(navItem.route.route)
                    rightNavController?.navigate(HomeScreenDestination.DetailsPlaceholderScreen.route)
                } else {
                    // In single pane mode if menu option gets clicked, then we navigate to it in left navigation controller.
                    leftNavController?.navigate(navItem.route.route)
                }
            }
        }

        // This is on click listener for details screens.
        // Those screens are shown on right pane in dual pane mode.
        // It should be run after onItemClick lambda of the list view.
        val onItemClickNavigationDetailsScreens: (
            Boolean,
            NavItem,
            NavHostController?,
            NavHostController?,
        ) -> Unit = remember {
            { isDualPane, navItem, leftNavController, rightNavController ->
                if(isDualPane) {
                    // In dual pane mode if list item gets clicked, then we do not navigate in left
                    // navigation controller. However we navigate in right navigation controller
                    // to details screen of that item.
                    // Currently shown entity in details screen will be set in onItemClick of
                    // appropriate list view.
                    rightNavController?.navigate(navItem.route.route)
                } else {
                    // In single pane mode if list item gets clicked, then we navigate to it in left navigation controller.
                    leftNavController?.navigate(navItem.route.route)
                }
            }
        }
        val listScreensNavItems = remember { listScreensNavItems(onItemClickNavigationListScreens) }
        val detailsNavItems = remember { detailsNavItems(onItemClickNavigationDetailsScreens) }
        val leftPaneNavItems = remember { listScreensNavItems }

        val navigateToCard: (Int) -> Unit = remember {
            { cardNumber ->
                if(isDualPane) {
                    leftNavController.navigate(HomeScreenDestination.fromNumber(cardNumber).route)
                    rightNavController.navigate(HomeScreenDestination.DetailsPlaceholderScreen.route)
                } else {
                    leftNavController.navigate(HomeScreenDestination.fromNumber(cardNumber).route)
                }
            }
        }

        val navigateCardLeft: () -> Unit = remember {
            {
                if(LOG_ME)ALog.d(TAG, "(): " +
                                        "Navigating card left")
                val route = leftNavController.currentBackStackEntry?.destination?.route
                if(LOG_ME)ALog.d(TAG, "(): " +
                        "leftNavController.currentBackStackEntry == " +
                            "${leftNavController.currentBackStackEntry}\n" +
                        "leftNavController.currentBackStackEntry.destination == " +
                            "${leftNavController.currentBackStackEntry?.destination}\n" +
                        "route = $route")
                if(route != null) {
                    val currentCardInLeftPane = HomeScreenDestination.fromString(route)
                    when(val currentCardInLeftPaneNumber = currentCardInLeftPane.toNumber()) {
                        1 -> navigateToCard(5)
                        else -> navigateToCard(currentCardInLeftPaneNumber-1)
                    }
                } else {
                    if(LOG_ME)ALog.d(TAG, "(): " +
                                            "route == null Navigating to card 1.")
                    navigateToCard(1)
                }
            }
        }
        val navigateCardRight: () -> Unit = remember {
            {
                if(LOG_ME)ALog.d(TAG, "(): " +
                        "Navigating card right")
                val route = leftNavController.currentBackStackEntry?.destination?.route
                if(LOG_ME)ALog.d(TAG, "(): " +
                        "leftNavController.currentBackStackEntry == " +
                        "${leftNavController.currentBackStackEntry}\n" +
                        "leftNavController.currentBackStackEntry.destination == " +
                        "${leftNavController.currentBackStackEntry?.destination}\n" +
                        "route = $route")
                if(route != null) {
                    val currentCardInLeftPane = HomeScreenDestination.fromString(route)
                    when(val currentCardInLeftPaneNumber = currentCardInLeftPane.toNumber()) {
                        5 -> navigateToCard(1)
                        else -> navigateToCard(currentCardInLeftPaneNumber+1)
                    }
                } else {
                    if(LOG_ME)ALog.d(TAG, "(): " +
                            "route == null Navigating to card 1.")
                    navigateToCard(1)
                }
            }
        }
        val navigateToDetailsScreen: (HomeScreenDestination) -> Unit = remember {
            { detailsScreenDestination ->
                if(isDualPane) {
                    if(LOG_ME)ALog.d(TAG, "(): " +
                            "Navigating to Details Screen in right nav controller. " +
                            "detailsScreenDestination == $detailsScreenDestination")
                    rightNavController.navigate(detailsScreenDestination.route)
                } else {
                    if(LOG_ME)ALog.d(TAG, "(): " +
                            "Navigating to Details Screen in left nav controller. " +
                            "detailsScreenDestination == $detailsScreenDestination")
                    leftNavController.navigate(detailsScreenDestination.route)
                }
            }
        }
        val finishVerification: () -> Unit = remember { {navigateToCard(5)} }
        val navigateToTeamDetails: (SportsTeam) -> Unit = remember {
            {
                launchStateEvent(HomeScreenStateEvent.SetCurrentlyShownSportsTeam(
                    it,
                ))
                navigateToDetailsScreen(HomeScreenDestination.DetailsScreen2)
            }
        }
        val navigateToSportsNewsMessageDetailsScreen: () -> Unit = remember {
            {
                navigateToDetailsScreen(HomeScreenDestination.DetailsScreen3)
            }
        }

        val openDrawer = {
            if(LOG_ME)ALog.d(TAG, "(): " +
                    "Opening navigation drawer.")
            launchStateEvent(HomeScreenStateEvent.SetDrawerOpen(true))
        }

        val leftPaneNavGraphBuilder = LeftPaneNavGraphBuilder(
            launchStateEvent = launchStateEvent,
            activity = activity,
            permissionHandlingData = permissionHandlingData,
            stateEventTracker = stateEventTracker,

            deviceLocation,
            snackBarState = snackBarState,
            toastState = toastState,
            dialogState = dialogState,
            progressIndicatorState = progressIndicatorState,

            navigateCardLeft = navigateCardLeft,
            navigateCardRight = navigateCardRight,
            openDrawer = openDrawer,
            navigateToProfileScreen = finishVerification,

            floatingActionButtonDrawableIdCard1,
            floatingActionButtonOnClickCard1,
            floatingActionButtonContentDescriptionCard1,
            card1EntitiesList,
            card1OnListItemClick = {
                card1OnListItemClick(it)
                navigateToDetailsScreen(HomeScreenDestination.DetailsScreen1)
            },
            showProfileStatusBar,
            finishVerification,
            card1SearchQuery,
            card1OnSearchQueryUpdate,
            searchFilters,
            onSearchFiltersUpdated,
            card1BottomSheetActions,
            card1CurrentlyShownEntity = card1CurrentlyShownEntity,
            card1ActionOnEntity = card1ActionOnEntity,

            floatingActionButtonDrawableIdCard2,
            floatingActionButtonOnClickCard2,
            floatingActionButtonContentDescriptionCard2,
            card2EntitiesList,
            card2OnListItemClick = {
                card2OnListItemClick(it)
                navigateToDetailsScreen(HomeScreenDestination.DetailsScreen2)
            },
            card2BottomSheetActions,

            floatingActionButtonDrawableIdCard3,
            floatingActionButtonOnClickCard3,
            floatingActionButtonContentDescriptionCard3,
            card3EntitiesList,
            card3OnListItemClick = {
                card3OnListItemClick(it)
                navigateToDetailsScreen(HomeScreenDestination.DetailsScreen3)
            },
            card3BottomSheetActions,

            floatingActionButtonDrawableIdCard4,
            floatingActionButtonOnClickCard4,
            floatingActionButtonContentDescriptionCard4,
            card4EntitiesList,
            card4OnListItemClick = {
                card4OnListItemClick(it)
                navigateToDetailsScreen(HomeScreenDestination.DetailsScreen4)
            },
            card4BottomSheetActions,

            card5BottomSheetActions,


            currentlyShownSportsGame = currentlyShownSportsGame,
            onSendSportsFanMessage = onSendSportsFanMessage,
            user = user,
            currentSportsFanMessageRecipient = currentSportsFanMessageRecipient,
            sportsNewsMessages = sportsNewsMessages,
            onNewsClick = {
                navigateToSportsNewsMessageDetailsScreen()
                onNewsClick(it)
            },
            onBackButtonPress = { navigateBack = true},
            favoriteTeams = favoriteTeams,
            navigateToTeamDetails = navigateToTeamDetails,

            isPreview = isPreview,
        )
        val rightPaneNavGraphBuilder = RightPaneNavGraphBuilder(
            onNewsClick = onNewsClick,
            launchStateEvent = launchStateEvent,
            activity = activity,
            permissionHandlingData = permissionHandlingData,
            stateEventTracker = stateEventTracker,
            snackBarState = snackBarState,
            toastState = toastState,
            dialogState = dialogState,
            progressIndicatorState = progressIndicatorState,

            card1CurrentlyShownEntity = card1CurrentlyShownEntity,
            card1ActionOnEntity = card1ActionOnEntity,
            navigateToProfileScreen = finishVerification,
            sportsNewsMessages = sportsNewsMessages,
        )

        val onBackButtonPress: () -> Unit = remember {
            {
                if(isDualPane) {
                    val thereIsPlaceholderInRightNavController = thisIsNavControllersCurrentRouteName(
                        rightNavController,
                        HomeScreenDestination.DetailsPlaceholderScreen.route,
                    )

                    if(thereIsPlaceholderInRightNavController) {
                        // In dual pane mode if back button gets pressed while there is currently shown
                        // placeholder in right navigation controller, then we navigate back stack in
                        // left navigation controller.
                    } else {
                        // In dual pane mode if back button gets pressed while there is not currently
                        // shown placeholder in right navigation controller, then we don't navigate
                        // in left navigation controller.
                        // However we navigate to details placeholder screen in right navigation
                        // controller.
                        rightNavController.navigate(HomeScreenDestination.DetailsPlaceholderScreen.route)
                    }
                } else {
                    // In single pane mode if back button gets pressed then we navigate back the stack in left navigation graph.
                    leftNavController.popBackStack()
                }
            }
        }

        val drawerData = DrawerData(
            drawerContent = {
                HomeScreenTheme {
                    StandardNavigationDrawer(
                        isDualPane = isDualPane,
                        titleId = R.string.homescreen_navigation_drawer_title,
                        contentDescriptionId = R.string.homescreen_drawer,
                        titleBackgroundId = R.drawable.list_item_example_background_1,

                        navItems = leftPaneNavItems,
                        leftPaneNavController = leftNavController,
                        rightPaneNavController = rightNavController,

                        navItemsListBackgroundId = R.drawable.list_item_example_background_1,
                        navItemTextStyle = TextStyle(fontSize = 18.sp),
                        navItemBackgroundId = R.drawable.list_item_example_background_1,
                    )
                }

            },
        )

        val homeScreenTopBar:  @Composable () -> Unit = {
            HomeScreenAppBar(
                activity = activity,
                onBackButtonPress = onBackButtonPress,
                navigateToSettingsScreen = null,        // TODO: Unavailable in demo project.
            )
        }

        // Here we decide which layout to show
        if(isDualPane) {
            DualPaneWithNavRailAndMovableNavHostLayout (
                launchStateEvent = launchStateEvent,
                launchStateEventSetDrawerOpen = { isOpen ->
                    HomeScreenStateEvent.SetDrawerOpen(isOpen)
                },
                leftPaneNavController = leftNavController,
                rightPaneNavController = rightNavController,
                leftPaneNavItems = leftPaneNavItems,
                rightPaneNavItems = detailsNavItems,
                leftNavHost = {
                    LeftNavHost(
                        leftPaneStartDestination,
                        leftPaneNavGraphBuilder,
                        leftNavController,
                        modifier = null,
                    )
                },
                rightNavHost = {
                    RightNavHost(
                        rightPaneStartDestination,
                        rightPaneNavGraphBuilder,
                        rightNavController,
                        modifier = null,
                    )
                },
                topBar = homeScreenTopBar,
                drawerData = drawerData,
                isDrawerOpen = isDrawerOpen,
            )
        } else {
            SinglePaneWithBottomNavBarAndMovableNavHostLayout(
                launchStateEvent = launchStateEvent,
                launchStateEventSetDrawerOpen = { isOpen ->
                    HomeScreenStateEvent.SetDrawerOpen(isOpen)
                },
                leftPaneNavController = leftNavController,
                rightPaneNavController = rightNavController,
                navItems = leftPaneNavItems,
                navHost = {
                    LeftNavHost(
                        leftPaneStartDestination,
                        leftPaneNavGraphBuilder,
                        leftNavController,
                        modifier = null,
                    )
                },
                topBar = homeScreenTopBar,
                drawerData = drawerData,
                isDrawerOpen = isDrawerOpen,
            )
        }

        // Method navigates in leftNavController and rightNavController depending
        // on what kind of orientation change has occurred if it occurred.
        onOrientationChange(
            isDualPane,
            wasDualPane,
            leftNavController,
            rightNavController,
        )

        BackHandler(enabled = true, onBack = onBackButtonPress)

        LaunchedEffect(
            key1 = isDualPane,
            block = {
                // This will launch only when isDualPane changes as in on configuration change.
                // LaunchedEffect is launched after recomposition if key was changed.
                // LaunchedEffect will launch after assigning value to key for the first time.
                wasDualPane = isDualPane
                updateOrientation(isDualPane)
            }
        )

        LaunchedEffect(
            key1 = navigateBack,
            block = {
                if(navigateBack)onBackButtonPress()
                navigateBack = false
            },
        )
        if(LOG_ME) ALog.d(TAG, "(): Recomposition end.")

    }
}

// Method navigates in leftNavController and rightNavController depending
// on what kind of orientation change has occurred if it occurred.
@SuppressLint("ComposableNaming")
@Composable
internal fun onOrientationChange(
    isDualPane: Boolean,
    wasDualPane: Boolean,
    leftNavController: NavHostController,
    rightNavController: NavHostController,
) {
    val switchedFromDualPaneToSinglePane = wasDualPane && !isDualPane
    val switchedFromSinglePaneToDualPane = !wasDualPane && isDualPane
    if(switchedFromSinglePaneToDualPane) {
        val dummyItemClick: (Boolean, NavItem, NavHostController?, NavHostController?) -> Unit = remember {
            { _, _, _, _ ->
                if(LOG_ME)ALog.w(TAG, ".emptyItemClick(): " +
                        "This item click should never be called as it is just a dummy.")
            }
        }
        val currentLeftPaneScreenIsDetailsScreen = currentLeftPaneScreenIsDetailsScreen(
            leftNavController,
            detailsNavItems(dummyItemClick),
        )
        if(currentLeftPaneScreenIsDetailsScreen) {
            // If single pane mode switches to dual pane mode and current screen is details screen,
            // then in left nav controller we navigate back stack and in right nav controller we
            // navigate to details screen from left nav controller.
            leftNavController.popBackStack()
            rightNavController.navigate(HomeScreenDestination.DetailsPlaceholderScreen.route)
        } else {
            // If single pane mode switches to dual pane mode and current screen is not details
            // screen (it is list screen for example), then in right nav controller we navigate
            // to placeholder screen.
        }
    }
    if(switchedFromDualPaneToSinglePane) {
        val thereIsPlaceholderInRightNavController = thisIsNavControllersCurrentRouteName(
            rightNavController,
            HomeScreenDestination.DetailsPlaceholderScreen.route,
        )

        if(thereIsPlaceholderInRightNavController) {
            // If dual pane mode switches to single pane mode and there is currently shown
            // details screen placeholder in right navigation controller, then we do not navigate
            // in left and right navigation controller.
        } else {
            // If dual pane mode switches to single pane mode and there is not currently shown
            // details screen placeholder in right navigation controller, then in left
            // navigation controller we navigate to details screen shown in right navigation
            // controller.
            // Furthermore in right navigation controller we navigate to details screen placeholder.
            val navItemsNameShownInRightNavigationController =
                rightNavController.currentBackStackEntry?.destination?.route
            if(navItemsNameShownInRightNavigationController != null) {
                leftNavController.navigate(navItemsNameShownInRightNavigationController)
            } else {
                if(LOG_ME)ALog.w(TAG, ".HomeScreenUI(): " +
                        "navItemsNameShownInRightNavigationController == null")
            }
            rightNavController.navigate(HomeScreenDestination.DetailsPlaceholderScreen.route)
        }
    }
}
internal fun thisIsNavControllersCurrentRouteName(
    navController: NavController,
    routeName: String,
): Boolean {
    if(LOG_ME) ALog.d(TAG, ".thisIsNavControllersCurrentRouteName(): " +
            "routeName == $routeName\n" +
            "navController.currentBackStackEntry is null is " +
            "${navController.currentBackStackEntry == null}\n" +
            "navController.currentBackStackEntry?.destination? is null is " +
            "${navController.currentBackStackEntry?.destination == null}")
    return navController.currentBackStackEntry?.destination?.route == routeName
}

internal fun listScreensNavItems(
    onItemClickNavigation: (Boolean, NavItem, NavHostController?, NavHostController?) -> Unit,
): List<NavItem> {
    return listOf(
        NavItem(
            nameStringID = R.string.home_screen_bottom_nav_menu_1,
            route = HomeScreenDestination.Card1,
            iconDrawableID = R.drawable.ic_search_yellow_24,
            onItemClickNavigation = onItemClickNavigation,
            contentDescriptionID = R.string.home_screen_bottom_nav_menu_1_content_description,
        ),
        NavItem(
            nameStringID = R.string.home_screen_bottom_nav_menu_2,
            route = HomeScreenDestination.Card2,
            iconDrawableID = R.drawable.ic_calendar_yellow_24,
            onItemClickNavigation = onItemClickNavigation,
            contentDescriptionID = R.string.home_screen_bottom_nav_menu_2_content_description,
        ),
        NavItem(
            nameStringID = R.string.home_screen_bottom_nav_menu_3,
            route = HomeScreenDestination.Card3,
            iconDrawableID = R.drawable.ic_message_yellow_24,
            onItemClickNavigation = onItemClickNavigation,
            contentDescriptionID = R.string.home_screen_bottom_nav_menu_3_content_description,
        ),
        NavItem(
            nameStringID = R.string.home_screen_bottom_nav_menu_4,
            route = HomeScreenDestination.Card4,
            iconDrawableID = R.drawable.ic_car_yellow_24,
            onItemClickNavigation = onItemClickNavigation,
            contentDescriptionID = R.string.home_screen_bottom_nav_menu_4_content_description,
        ),
        NavItem(
            nameStringID = R.string.home_screen_bottom_nav_menu_5,
            route = HomeScreenDestination.Card5,
            iconDrawableID = R.drawable.ic_account_box_yellow_24,
            onItemClickNavigation = onItemClickNavigation,
            contentDescriptionID = R.string.home_screen_bottom_nav_menu_5_content_description,
        ),
    )
}

internal fun detailsNavItems(
    onItemClickNavigation: (Boolean, NavItem, NavHostController?, NavHostController?) -> Unit,
): List<NavItem> {
    return listOf(
        NavItem(
            nameStringID = R.string.home_screen_details_screen_placeholder,
            route = HomeScreenDestination.DetailsPlaceholderScreen,
            iconDrawableID = R.drawable.ic_launcher,
            onItemClickNavigation = onItemClickNavigation,
            contentDescriptionID = R.string.home_screen_details_screen_placeholder_content_description,
        ),
        NavItem(
            nameStringID = R.string.home_screen_details_screen_1,
            route = HomeScreenDestination.DetailsScreen1,
            iconDrawableID = R.drawable.ic_search_yellow_24,
            onItemClickNavigation = onItemClickNavigation,
            contentDescriptionID = R.string.home_screen_details_screen_1_content_description,
        ),
        NavItem(
            nameStringID = R.string.home_screen_details_screen_2,
            route = HomeScreenDestination.DetailsScreen2,
            iconDrawableID = R.drawable.ic_calendar_yellow_24,
            onItemClickNavigation = onItemClickNavigation,
            contentDescriptionID = R.string.home_screen_details_screen_2_content_description,
        ),
        NavItem(
            nameStringID = R.string.home_screen_details_screen_3,
            route = HomeScreenDestination.DetailsScreen3,
            iconDrawableID = R.drawable.ic_message_yellow_24,
            onItemClickNavigation = onItemClickNavigation,
            contentDescriptionID = R.string.home_screen_details_screen_3_content_description,
        ),
        NavItem(
            nameStringID = R.string.home_screen_details_screen_4,
            route = HomeScreenDestination.DetailsScreen4,
            iconDrawableID = R.drawable.ic_car_yellow_24,
            onItemClickNavigation = onItemClickNavigation,
            contentDescriptionID = R.string.home_screen_details_screen_4_content_description,
        ),
        NavItem(
            nameStringID = R.string.home_screen_details_screen_5,
            route = HomeScreenDestination.DetailsScreen5,
            iconDrawableID = R.drawable.ic_account_box_yellow_24,
            onItemClickNavigation = onItemClickNavigation,
            contentDescriptionID = R.string.home_screen_details_screen_5_content_description,
        ),
    )
}

internal fun currentLeftPaneScreenIsDetailsScreen(
    leftNavController: NavController,
    detailsNavItems: List<NavItem>,
): Boolean {
    val currentRouteInLeftNavController = leftNavController.currentBackStackEntry?.destination?.route
    if(currentRouteInLeftNavController != null) {
        for(detailsNavItem in detailsNavItems) {
            if(detailsNavItem.route.route == currentRouteInLeftNavController) {
                if(LOG_ME)ALog.d(TAG, ".currentLeftPaneScreenIsDetailsScreen(): " +
                        "currentRouteInLeftNavController is ${detailsNavItem.route.route}")
                return true
            }
        }
        if(LOG_ME)ALog.d(TAG, ".currentLeftPaneScreenIsDetailsScreen(): " +
                "currentRouteInLeftNavController is $currentRouteInLeftNavController " +
                "which is not a details screen")
    } else {
        if(LOG_ME)ALog.e(TAG, ".currentLeftPaneScreenIsDetailsScreen(): " +
                "currentRouteInLeftNavController == null")
    }
    return false
}