package com.aps.coachfanapp.cfahomescreen.framework.datasources.cachedatasources.room.abs

import com.aps.coachfanapp.common.util.ParcelableGeoPoint
import com.aps.coachfanapp.common.util.UniqueID
import com.aps.coachfanapp.common.util.UserUniqueID
import com.aps.coachfanapp.cfahomescreen.business.domain.model.entities.SportsGame

interface SportsGameDaoService {
    suspend fun insertOrUpdateEntity(entity: SportsGame): SportsGame

    suspend fun insertEntity(entity: SportsGame): Long

    suspend fun insertEntities(Entities: List<SportsGame>): LongArray

    suspend fun getEntityById(id: UniqueID?): SportsGame?

    suspend fun updateEntity(
        id: UniqueID?,
        created_at: String?,
        updated_at: String?,

        latitude : Double?,
        longitude: Double?,
        geoLocation: ParcelableGeoPoint?,
        firestoreGeoLocation: Double?,

        picture1URI: String?,
        description : String?,

        city: String?,

        ownerID: UserUniqueID?,

        name: String?,

        switch1: Boolean?,
        switch2: Boolean?,
        switch3: Boolean?,
        switch4: Boolean?,
        switch5: Boolean?,
        switch6: Boolean?,
        switch7: Boolean?,

        picture2URI: String?,
        date: String?,
        location: String?,
        tvChannel: String?,
        ticketPrices: String?,
        currentScore: String?,
        gameBreakdown: String?,
        team1Id: UniqueID?,
        team2Id: UniqueID?,
    ): Int

    suspend fun deleteEntity(primaryKey: UniqueID?): Int

    suspend fun deleteEntities(Entities: List<SportsGame>): Int

    suspend fun searchEntities(): List<SportsGame>

    suspend fun getAllEntities(): List<SportsGame>

    suspend fun getNumEntities(): Int
}