package com.aps.coachfanapp.cfahomescreen.framework.datasources.cachedatasources.room.abs

import com.aps.coachfanapp.common.util.ParcelableGeoPoint
import com.aps.coachfanapp.common.util.UniqueID
import com.aps.coachfanapp.cfahomescreen.business.domain.model.entities.SportsNewsMessage

interface SportsNewsMessageDaoService {
    suspend fun insertOrUpdateEntity(entity: SportsNewsMessage): SportsNewsMessage

    suspend fun insertEntity(entity: SportsNewsMessage): Long

    suspend fun insertEntities(Entities: List<SportsNewsMessage>): LongArray

    suspend fun getEntityById(id: UniqueID?): SportsNewsMessage?

    suspend fun updateEntity(
        id: UniqueID?,
        created_at: String?,
        updated_at: String?,

        latitude : Double?,
        longitude: Double?,
        geoLocation: ParcelableGeoPoint?,
        firestoreGeoLocation: Double?,

        picture1URI: String?,
        description : String?,

        title: String?,
        message: String?,
        sportType: String?,
        gameId: UniqueID?,
        teamId: UniqueID?,
        playerIds: String?,
    ): Int

    suspend fun deleteEntity(primaryKey: UniqueID?): Int

    suspend fun deleteEntities(Entities: List<SportsNewsMessage>): Int

    suspend fun searchEntities(): List<SportsNewsMessage>

    suspend fun getAllEntities(): List<SportsNewsMessage>

    suspend fun getNumEntities(): Int
}