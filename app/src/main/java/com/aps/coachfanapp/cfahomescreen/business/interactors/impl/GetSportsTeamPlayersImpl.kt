package com.aps.coachfanapp.cfahomescreen.business.interactors.impl

import com.aps.coachfanapp.R
import com.aps.coachfanapp.common.business.data.network.ApiResponseHandler
import com.aps.coachfanapp.common.business.data.util.safeApiCall
import com.aps.coachfanapp.common.business.domain.state.*
import com.aps.coachfanapp.common.util.ALog
import com.aps.coachfanapp.cfahomescreen.business.data.cache.abstraction.SportsTeamPlayerCacheDataSource
import com.aps.coachfanapp.cfahomescreen.business.data.network.abs.SportsTeamPlayerNetworkDataSource
import com.aps.coachfanapp.cfahomescreen.business.domain.model.entities.SportsTeamPlayer
import com.aps.coachfanapp.core.business.domain.model.entities.ProjectUser
import com.aps.coachfanapp.cfahomescreen.business.domain.model.factories.SportsTeamPlayerFactory
import com.aps.coachfanapp.cfahomescreen.business.interactors.abs.GetSportsTeamPlayers
import com.aps.coachfanapp.cfahomescreen.framework.presentation.activity01.state.HomeScreenViewState
import com.aps.coachfanapp.core.util.ApplicationContextProvider
import com.aps.coachfanapp.core.util.ConnectivityHelper
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

private const val TAG = "GetSportsTeamPlayersImpl"
private const val LOG_ME = true

class GetSportsTeamPlayersImpl
@Inject
constructor(
    private val cacheDataSource: SportsTeamPlayerCacheDataSource,
    private val networkDataSource: SportsTeamPlayerNetworkDataSource,
    private val entityFactory: SportsTeamPlayerFactory,
    private val applicationContextProvider: ApplicationContextProvider,
    private val connectivityHelper: ConnectivityHelper,
): GetSportsTeamPlayers {
    override fun getSportsTeamPlayers(
        stateEvent: StateEvent,
        returnViewState : HomeScreenViewState<ProjectUser>,
        updateReturnViewState : (HomeScreenViewState<ProjectUser>, List<SportsTeamPlayer>?) -> HomeScreenViewState<ProjectUser>,
        onErrorAction: () -> Unit,
    ): Flow<DataState<HomeScreenViewState<ProjectUser>>?> = flow {
        val userTrips = safeApiCall(
            dispatcher = Dispatchers.IO,
            onErrorAction = onErrorAction,
            apiCall = {
                returnViewState.mainEntity?.id?.let { networkDataSource.getUsersEntities2(it) }
            }
        )

        val response = object : ApiResponseHandler<HomeScreenViewState<ProjectUser>, List<SportsTeamPlayer>>(
            response = userTrips,
            stateEvent = stateEvent,
            internetConnectionIsAvailable = connectivityHelper
                .internetConnectionIsAvailable(applicationContextProvider.applicationContext(TAG)),
        ) {
            override suspend fun handleSuccess(resultObj: List<SportsTeamPlayer>): DataState<HomeScreenViewState<ProjectUser>>? {
                var message: String? = GetUsersTripsImplConstants.GET_ENTITY_SUCCESS
                var uiComponentType: UIComponentType? = UIComponentType.None()
                ALog.d(TAG, "getEntities2().handleSuccess(): ")


                if(resultObj == null){
                    ALog.d(TAG, "getEntities2(): resultObj == null")
                    message = GetUsersTripsImplConstants.GET_ENTITY_NO_MATCHING_RESULTS
                    uiComponentType = UIComponentType.Toast()
                } else {
                    ALog.d(TAG, "getEntities2(): resultObj != null")
                    returnViewState.entities2List = ArrayList(resultObj)
                    updateReturnViewState(returnViewState, resultObj)
                }
                return DataState.data(
                    response = Response(
                        messageId = R.string.error,
                        message = message,
                        uiComponentType = uiComponentType as UIComponentType,
                        messageType = MessageType.Success()
                    ),
                    data = returnViewState,
                    stateEvent = stateEvent
                )
            }
        }.getResult()

        emit(response)
    }

    object GetUsersTripsImplConstants{
        const val GET_ENTITY_SUCCESS = "Successfully entities 2."
        const val GET_ENTITY_NO_MATCHING_RESULTS = "There are no entities 2 in database."
        const val SEARCH_ENTITIES_FAILED = "Failed to retrieve the list of entities 2."
    }
}
