package com.aps.coachfanapp.cfahomescreen.business.data.network.abs

import com.aps.coachfanapp.common.business.data.network.StandardNetworkDataSource
import com.aps.coachfanapp.common.util.UniqueID
import com.aps.coachfanapp.cfahomescreen.business.domain.model.entities.SportsNewsMessage

interface SportsNewsMessageNetworkDataSource: StandardNetworkDataSource<SportsNewsMessage> {
    override suspend fun insertOrUpdateEntity(entity: SportsNewsMessage): SportsNewsMessage?

    override suspend fun deleteEntity(primaryKey: UniqueID?)

    override suspend fun insertDeletedEntity(entity: SportsNewsMessage)

    override suspend fun insertDeletedEntities(Entities: List<SportsNewsMessage>)

    override suspend fun deleteDeletedEntity(entity: SportsNewsMessage)

    override suspend fun getDeletedEntities(): List<SportsNewsMessage>

    override suspend fun deleteAllEntities()

    override suspend fun searchEntity(entity: SportsNewsMessage): SportsNewsMessage?

    override suspend fun getAllEntities(): List<SportsNewsMessage>

    override suspend fun insertOrUpdateEntities(Entities: List<SportsNewsMessage>): List<SportsNewsMessage>?

    override suspend fun getEntityById(id: UniqueID): SportsNewsMessage?
}