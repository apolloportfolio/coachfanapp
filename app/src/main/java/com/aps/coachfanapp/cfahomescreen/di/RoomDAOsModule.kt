package com.aps.coachfanapp.cfahomescreen.di

import com.aps.coachfanapp.core.framework.datasources.cachedatasources.room.abstraction.*
import com.aps.coachfanapp.core.framework.datasources.cachedatasources.room.implementation.*
import com.aps.coachfanapp.cfahomescreen.framework.datasources.cachedatasources.room.abs.SportsGameDaoService
import com.aps.coachfanapp.cfahomescreen.framework.datasources.cachedatasources.room.abs.SportsTeamPlayerDaoService
import com.aps.coachfanapp.cfahomescreen.framework.datasources.cachedatasources.room.abs.SportsTeamDaoService
import com.aps.coachfanapp.cfahomescreen.framework.datasources.cachedatasources.room.abs.SportsNewsMessageDaoService
import com.aps.coachfanapp.cfahomescreen.framework.datasources.cachedatasources.room.impl.SportsGameDaoServiceImpl
import com.aps.coachfanapp.cfahomescreen.framework.datasources.cachedatasources.room.impl.SportsTeamPlayerDaoServiceImpl
import com.aps.coachfanapp.cfahomescreen.framework.datasources.cachedatasources.room.impl.SportsTeamDaoServiceImpl
import com.aps.coachfanapp.cfahomescreen.framework.datasources.cachedatasources.room.impl.SportsNewsMessageDaoServiceImpl
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
abstract class RoomDAOsModule {
    @Binds
    abstract fun bindSportsGameDaoService(implementation: SportsGameDaoServiceImpl): SportsGameDaoService

    @Binds
    abstract fun bindSportsTeamPlayerDaoService(implementation: SportsTeamPlayerDaoServiceImpl): SportsTeamPlayerDaoService

    @Binds
    abstract fun bindSportsTeamDaoService(implementation: SportsTeamDaoServiceImpl): SportsTeamDaoService

    @Binds
    abstract fun bindSportsNewsMessageDaoService(implementation: SportsNewsMessageDaoServiceImpl): SportsNewsMessageDaoService

}