package com.aps.coachfanapp.cfahomescreen.business.data.network.impl

import android.net.Uri
import com.aps.coachfanapp.common.util.ALog
import com.aps.coachfanapp.common.util.UniqueID
import com.aps.coachfanapp.common.util.UserUniqueID
import com.aps.coachfanapp.cfahomescreen.business.data.network.abs.SportsGameNetworkDataSource
import com.aps.coachfanapp.cfahomescreen.business.domain.model.entities.SportsGame
import com.aps.coachfanapp.cfahomescreen.business.interactors.impl.FirestoreSportsGameSearchParameters
import com.aps.coachfanapp.cfahomescreen.framework.datasources.networkdatasources.firebase.abs.SportsGameFirestoreService
import javax.inject.Inject
import javax.inject.Singleton


@Singleton
class SportsGameNetworkDataSourceImpl
@Inject
constructor(
    private val firestoreService: SportsGameFirestoreService
): SportsGameNetworkDataSource {
    override suspend fun insertOrUpdateEntity(entity: SportsGame): SportsGame? {
        return firestoreService.insertOrUpdateEntity(entity)
    }

    override suspend fun deleteEntity(primaryKey: UniqueID?) {
        return firestoreService.deleteEntity(primaryKey)
    }

    override suspend fun insertDeletedEntity(entity: SportsGame) {
        return firestoreService.insertDeletedEntity(entity)
    }

    override suspend fun insertDeletedEntities(entities: List<SportsGame>) {
        return firestoreService.insertDeletedEntities(entities)
    }

    override suspend fun deleteDeletedEntity(entity: SportsGame) {
        return firestoreService.deleteDeletedEntity(entity)
    }

    override suspend fun getDeletedEntities(): List<SportsGame> {
        return firestoreService.getAllDeletedEntities()
    }

    override suspend fun deleteAllEntities() {
        firestoreService.deleteAllEntities()
    }

    override suspend fun searchEntity(entity: SportsGame): SportsGame? {
        return firestoreService.searchEntity(entity)
    }

    override suspend fun getAllEntities(): List<SportsGame> {
        return firestoreService.getAllEntities()
    }

    override suspend fun insertOrUpdateEntities(entities: List<SportsGame>): List<SportsGame>? {
        return firestoreService.insertOrUpdateEntities(entities)
    }

    override suspend fun getEntityById(id: UniqueID): SportsGame? {
        return firestoreService.getEntityById(id)
    }

    override suspend fun searchEntities(
        searchParameters : FirestoreSportsGameSearchParameters
    ) : List<SportsGame>? {
        if(LOG_ME) ALog.d(TAG, ".searchEntities(): searchParameters:\n$searchParameters")
        return firestoreService.searchEntities(searchParameters)
    }

    override suspend fun getUsersEntites1(userID: UserUniqueID) : List<SportsGame>? {
        return firestoreService.getUsersSportsGames(userID)
    }

    override suspend fun uploadSportsGamePhotoToFirestore(
        entity : SportsGame,
        entitysPhotoUri : Uri,
        entitysPhotoNumber: Int,
    ) : String? {
        return firestoreService.uploadEntity1PhotoToFirestore(
            entity,
            entitysPhotoUri,
            entitysPhotoNumber
        )
    }

    companion object {
        const val TAG = "Entity1NetworkDataSourceImpl"
        const val LOG_ME = true
        const val COLLECTION_NAME = "entity1"
        const val DELETES_COLLECTION_NAME = "entity1_d"
    }
}
