package com.aps.coachfanapp.cfahomescreen.business.data.network.impl

import com.aps.coachfanapp.common.util.UniqueID
import com.aps.coachfanapp.common.util.UserUniqueID
import com.aps.coachfanapp.cfahomescreen.business.data.network.abs.SportsTeamNetworkDataSource
import com.aps.coachfanapp.cfahomescreen.business.domain.model.entities.SportsTeam
import com.aps.coachfanapp.cfahomescreen.framework.datasources.networkdatasources.firebase.abs.SportsTeamFirestoreService
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class SportsTeamNetworkDataSourceImpl
@Inject
constructor(
    private val firestoreService: SportsTeamFirestoreService
): SportsTeamNetworkDataSource {
    override suspend fun insertOrUpdateEntity(entity: SportsTeam): SportsTeam? {
        return firestoreService.insertOrUpdateEntity(entity)
    }

    override suspend fun deleteEntity(primaryKey: UniqueID?) {
        return firestoreService.deleteEntity(primaryKey)
    }

    override suspend fun insertDeletedEntity(entity: SportsTeam) {
        return firestoreService.insertDeletedEntity(entity)
    }

    override suspend fun insertDeletedEntities(entities: List<SportsTeam>) {
        return firestoreService.insertDeletedEntities(entities)
    }

    override suspend fun deleteDeletedEntity(entity: SportsTeam) {
        return firestoreService.deleteDeletedEntity(entity)
    }

    override suspend fun getDeletedEntities(): List<SportsTeam> {
        return firestoreService.getAllDeletedEntities()
    }

    override suspend fun deleteAllEntities() {
        firestoreService.deleteAllEntities()
    }

    override suspend fun searchEntity(entity: SportsTeam): SportsTeam? {
        return firestoreService.searchEntity(entity)
    }

    override suspend fun getAllEntities(): List<SportsTeam> {
        return firestoreService.getAllEntities()
    }

    override suspend fun insertOrUpdateEntities(entities: List<SportsTeam>): List<SportsTeam>? {
        return firestoreService.insertOrUpdateEntities(entities)
    }

    override suspend fun getEntityById(id: UniqueID): SportsTeam? {
        return firestoreService.getEntityById(id)
    }

    override suspend fun getUsersEntities3(userId: UserUniqueID): List<SportsTeam>? {
        return firestoreService.getUsersSportsTeams(userId)
    }
}