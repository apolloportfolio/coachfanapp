package com.aps.coachfanapp.cfahomescreen.framework.datasources.cachedatasources.room.mappers

import com.aps.coachfanapp.common.business.domain.model.EntityMapper
import com.aps.coachfanapp.cfahomescreen.business.domain.model.entities.SportsTeamPlayer
import com.aps.coachfanapp.cfahomescreen.framework.datasources.cachedatasources.room.model.SportsTeamPlayerCacheEntity
import javax.inject.Inject

class SportsTeamPlayerCacheMapper
@Inject
constructor() : EntityMapper<SportsTeamPlayerCacheEntity, SportsTeamPlayer> {
    override fun mapFromEntity(entity: SportsTeamPlayerCacheEntity): SportsTeamPlayer {
        return SportsTeamPlayer(
            entity.id,
            entity.created_at,
            entity.updated_at,

            entity.picture1URI,

            entity.description,

            entity.sportsTeamId,

            entity.playerName,
        )
    }

    override fun mapToEntity(domainModel: SportsTeamPlayer): SportsTeamPlayerCacheEntity {
        return SportsTeamPlayerCacheEntity(
            domainModel.id!!,
            domainModel.created_at,
            domainModel.updated_at,

            domainModel.picture1URI,

            domainModel.description,

            domainModel.sportsTeamId,

            domainModel.playerName,
        )
    }

    override fun mapFromEntityList(entities : List<SportsTeamPlayerCacheEntity>) : List<SportsTeamPlayer> {
        return entities.map{mapFromEntity(it)}
    }

    override fun mapToEntityList(entities: List<SportsTeamPlayer>): List<SportsTeamPlayerCacheEntity> {
        return entities.map{mapToEntity(it)}
    }
}