package com.aps.coachfanapp.cfahomescreen.framework.datasources.networkdatasources.firebase.mappers

import com.aps.coachfanapp.common.business.domain.model.EntityMapper
import com.aps.coachfanapp.common.util.ParcelableGeoPoint
import com.aps.coachfanapp.cfahomescreen.business.domain.model.entities.SportsNewsMessage
import com.aps.coachfanapp.cfahomescreen.framework.datasources.networkdatasources.firebase.model.SportsNewsMessageFirestoreEntity
import javax.inject.Inject

class SportsNewsMessageFirestoreMapper
@Inject
constructor() : EntityMapper<SportsNewsMessageFirestoreEntity, SportsNewsMessage> {
    override fun mapFromEntity(entity: SportsNewsMessageFirestoreEntity): SportsNewsMessage {
        val sportsNewsMessage = SportsNewsMessage(
            entity.id,
            entity.created_at,
            entity.updated_at,

            entity.latitude,
            entity.longitude,
            null,
            entity.firestoreGeoLocation,

            entity.picture1URI,
            entity.description,

            entity.title,
            entity.message,
            entity.sportType,
            entity.gameId,
            entity.teamId,
            entity.playerIds,
        )
        if(entity.geoLocation != null){
            sportsNewsMessage.geoLocation = ParcelableGeoPoint(
                entity.geoLocation!!.latitude,
                entity.geoLocation!!.longitude
            )
        }
        return sportsNewsMessage
    }

    override fun mapToEntity(domainModel: SportsNewsMessage): SportsNewsMessageFirestoreEntity {
        return SportsNewsMessageFirestoreEntity(
            domainModel.id,
            domainModel.created_at,
            domainModel.updated_at,

            domainModel.latitude,
            domainModel.longitude,
            domainModel.geoLocation,
            domainModel.firestoreGeoLocation,

            domainModel.picture1URI,
            domainModel.description,

            domainModel.title,
            domainModel.message,
            domainModel.sportType,
            domainModel.gameId,
            domainModel.teamId,
            domainModel.playerIds,
        )
    }

    override fun mapFromEntityList(entities : List<SportsNewsMessageFirestoreEntity>) : List<SportsNewsMessage> {
        return entities.map{mapFromEntity(it)}
    }

    override fun mapToEntityList(entities: List<SportsNewsMessage>): List<SportsNewsMessageFirestoreEntity> {
        return entities.map{mapToEntity(it)}
    }
}