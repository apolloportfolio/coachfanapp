package com.aps.coachfanapp.cfahomescreen.framework.presentation.activity01.composables.impl.fragments

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.material.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.aps.coachfanapp.R
import com.aps.coachfanapp.cfahomescreen.business.domain.model.entities.SportsFanMessage
import com.aps.coachfanapp.cfahomescreen.business.domain.model.entities.SportsGame
import com.aps.coachfanapp.cfahomescreen.framework.presentation.activity01.composables.impl.values.BackgroundsOfLayoutsCoachFanApp
import com.aps.coachfanapp.cfahomescreen.framework.presentation.activity01.composables.impl.values.Dimens
import com.aps.coachfanapp.cfahomescreen.framework.presentation.activity01.composables.impl.values.HomeScreenTheme
import com.aps.coachfanapp.common.business.domain.state.StateEventTracker
import com.aps.coachfanapp.common.framework.presentation.layouts.LazyColumnWithPullToRefresh
import com.aps.coachfanapp.common.framework.presentation.views.BoxWithBackground
import com.aps.coachfanapp.common.framework.presentation.views.InputFieldType01
import com.aps.coachfanapp.common.framework.presentation.views.ListItemType06
import com.aps.coachfanapp.common.framework.presentation.views.TitleRowType01
import com.aps.coachfanapp.common.util.DateUtil
import com.aps.coachfanapp.common.util.UniqueID
import com.aps.coachfanapp.common.util.UserUniqueID
import com.aps.coachfanapp.common.util.composableutils.getDrawableIdInPreview

@Composable
fun CoachFanAppChatScreenContent(
    senderId: UserUniqueID,
    recipientId: UserUniqueID?,
    stateEventTracker: StateEventTracker,
    launchInitStateEvent: () -> Unit,
    sportsGame: SportsGame?,
    messages: List<SportsFanMessage>,
    onSendMessage: (String, UserUniqueID, UserUniqueID?, UniqueID?) -> Unit,
    dateUtil: DateUtil = DateUtil.getDateUtilForPreview(),

    isPreview: Boolean = false,
) {
    BoxWithBackground(
        backgroundDrawableId = R.drawable.example_background_1,
        composableBackground = BackgroundsOfLayoutsCoachFanApp.backgroundScreen01(
            colors = null,
            brush = null,
            shape = null,
            alpha = 0.6f,
        )
    ) {
        Column(
            modifier = Modifier
                .fillMaxSize()
        ) {
            // Title
            TitleRowType01(
                titleString = sportsGame?.name ?: "",
                subtitleString = sportsGame?.getLocationAndDate(dateUtil),
                textAlign = TextAlign.Start,
                backgroundDrawableId = R.drawable.example_background_1,
                composableBackground = BackgroundsOfLayoutsCoachFanApp.backgroundTitleType01(
                    colors = null,
                    brush = null,
                    shape = null,
                    alpha = 0.7f,
                ),
                leftPictureDrawableId = R.drawable.baseline_sports_score_24,
                titleFontSize = Dimens.titleFontSize,
                leftImageTint = MaterialTheme.colors.secondary,
                titleColor = MaterialTheme.colors.secondary,
            )

//             Messages
            LazyColumnWithPullToRefresh(
                onRefresh = launchInitStateEvent,
                isRefreshing = stateEventTracker.isRefreshing,
                modifier = Modifier.weight(1f),
                contentPadding = Dimens.columnPadding,
                verticalArrangement = Arrangement.spacedBy(4.dp)
            ) {
                itemsIndexed(messages) { index, message ->
                    ListItemType06(
                        index = index,
                        itemTitleString = message.senderName,
                        itemDescription = message.message,
                        onListItemClick = {},
                        itemRef = if (!isPreview) {
                            message.senderPicture1FirebaseImageRef
                        } else {
                            null
                        },
                        backgroundDrawableId = R.drawable.example_background_1,
                        composableBackground = BackgroundsOfLayoutsCoachFanApp.backgroundListItemType01(
                            colors = null,
                            brush = null,
                            shape = null,
                            alpha = 0.6f,
                        ),
                        itemDrawableId = if(isPreview) {
                            getDrawableIdInPreview("coach_fan_app_example_user_profile_picture_", index)
                        } else { null },
                        imageTint = null,
                    )
                }
            }

            // Input field and send button
            InputFieldType01(
                onButtonClick = { messageText ->
                    onSendMessage(
                        messageText,
                        senderId,
                        recipientId,
                        sportsGame?.id,
                    )
                },
            )

        }
    }
}

@Preview
@Composable
private fun CoachFanAppChatScreenContentPreview() {
    HomeScreenTheme {
        val gameId = UniqueID("PreviewSportsGame")
        val sportsGame = SportsGame(
            id = gameId,
            created_at = "2023-01-01T12:00:00",
            updated_at = "2023-01-01T13:30:00",
            latitude = 37.7749,
            longitude = -122.4194,
            geoLocation = null,
            firestoreGeoLocation = 123.456,
            picture1URI = null,
            description = "Description of the sports game",
            city = "San Francisco",
            ownerID = null,
            name = "Football Game",
            switch1 = true,
            switch2 = false,
            switch3 = true,
            switch4 = false,
            switch5 = true,
            switch6 = false,
            switch7 = true,
            picture2URI = null,
            date = "2023-01-02 13:30:00",
            location = "Stadium San Francisc",
            tvChannel = "ESPN",
            ticketPrices = "$50 - $200",
            currentScore = "Team A 2 - Team B 1",
            gameBreakdown = "Exciting match with unexpected turns",
            team1Id = UniqueID("TeamA"),
            team2Id = UniqueID("TeamB")
        )
        val sportsFanMessages = SportsFanMessage.createPreviewEntitiesList(gameId)

        CoachFanAppChatScreenContent(
            senderId = UserUniqueID("PreviewSender"),
            recipientId = null,
            stateEventTracker = StateEventTracker(),
            launchInitStateEvent = {},

            sportsGame = sportsGame,
            messages = sportsFanMessages,
            onSendMessage = { message, senderId, recipientId, gameId -> },

            isPreview = true,
        )
    }
}
