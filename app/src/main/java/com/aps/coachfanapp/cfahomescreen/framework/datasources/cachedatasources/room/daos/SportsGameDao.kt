package com.aps.coachfanapp.cfahomescreen.framework.datasources.cachedatasources.room.daos

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.aps.coachfanapp.common.util.UniqueID
import com.aps.coachfanapp.common.util.UserUniqueID
import com.aps.coachfanapp.cfahomescreen.framework.datasources.cachedatasources.room.model.SportsGameCacheEntity
import com.google.firebase.firestore.GeoPoint
import java.util.*


@Dao
interface SportsGameDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(entity : SportsGameCacheEntity) : Long

    @Query("SELECT * FROM sportsgame")
    suspend fun get() : List<SportsGameCacheEntity>
    
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insertEntities(entities: List<SportsGameCacheEntity>): LongArray

    @Query("SELECT * FROM sportsgame WHERE id = :id")
    suspend fun getEntityById(id: UniqueID?): SportsGameCacheEntity?

    @Query("DELETE FROM sportsgame WHERE id IN (:ids)")
    suspend fun deleteEntities(ids: List<UniqueID?>): Int

    @Query("DELETE FROM sportsgame")
    suspend fun deleteAllEntities()

    @Query("SELECT * FROM sportsgame")
    suspend fun getAllEntities(): List<SportsGameCacheEntity>

    @Query(
        """
        UPDATE sportsgame 
        SET 
        created_at = :created_at,
        updated_at = :updated_at,
        latitude = :latitude,
        longitude = :longitude,
        geoLocation = :geoLocation,
        firestoreGeoLocation = :firestoreGeoLocation,
        picture1URI = :picture1URI,
        description = :description,
        city = :city,
        ownerID = :ownerID,
        name = :name,
        switch1 = :switch1,
        switch2 = :switch2,
        switch3 = :switch3,
        switch4 = :switch4,
        switch5 = :switch5,
        switch6 = :switch6,
        switch7 = :switch7,
        picture2URI = :picture2URI,
        date = :date,
        location = :location,
        tvChannel = :tvChannel,
        ticketPrices = :ticketPrices,
        currentScore = :currentScore,
        gameBreakdown = :gameBreakdown,
        team1Id = :team1Id,
        team2Id = :team2Id
        WHERE id = :id
        """
    )
    suspend fun updateEntity(
        id: UniqueID?,
        created_at: String?,
        updated_at: String?,

        latitude : Double?,
        longitude: Double?,
        geoLocation: GeoPoint?,
        firestoreGeoLocation: Double?,

        picture1URI: String?,

        description : String?,

        city : String?,

        ownerID: UserUniqueID?,

        name: String?,

        switch1: Boolean? = null,
        switch2: Boolean? = null,
        switch3: Boolean? = null,
        switch4: Boolean? = null,
        switch5: Boolean? = null,
        switch6: Boolean? = null,
        switch7: Boolean? = null,

        picture2URI: String?,
        date: String?,
        location: String?,
        tvChannel: String?,
        ticketPrices: String?,
        currentScore: String?,
        gameBreakdown: String?,
        team1Id: UniqueID?,
        team2Id: UniqueID?,
    ): Int

    @Query("DELETE FROM sportsgame WHERE id = :primaryKey")
    suspend fun deleteEntity(primaryKey: UniqueID?): Int

    @Query("SELECT * FROM sportsgame")
    suspend fun searchEntities(): List<SportsGameCacheEntity>
    
    @Query("SELECT COUNT(*) FROM sportsgame")
    suspend fun getNumEntities(): Int
}