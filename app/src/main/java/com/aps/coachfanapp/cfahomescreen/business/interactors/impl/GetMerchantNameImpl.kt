package com.aps.coachfanapp.cfahomescreen.business.interactors.impl

import com.aps.coachfanapp.R
import com.aps.coachfanapp.cfahomescreen.business.interactors.abs.GetMerchantName
import com.aps.coachfanapp.cfahomescreen.framework.presentation.activity01.state.HomeScreenViewState
import com.aps.coachfanapp.common.business.data.network.ApiResponseHandler
import com.aps.coachfanapp.common.business.data.util.safeApiCall
import com.aps.coachfanapp.common.business.domain.state.DataState
import com.aps.coachfanapp.common.business.domain.state.MessageType
import com.aps.coachfanapp.common.business.domain.state.Response
import com.aps.coachfanapp.common.business.domain.state.StateEvent
import com.aps.coachfanapp.common.business.domain.state.UIComponentType
import com.aps.coachfanapp.common.util.ALog
import com.aps.coachfanapp.common.util.DateUtil
import com.aps.coachfanapp.core.business.domain.model.entities.ProjectUser
import com.aps.coachfanapp.core.util.ApplicationContextProvider
import com.aps.coachfanapp.core.util.ConnectivityHelper
import com.aps.coachfanapp.core.util.SecureKeyStorage
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

private const val TAG = "GetMerchantNameImpl"
private const val LOG_ME = true

class GetMerchantNameImpl
@Inject
constructor(
    private val secureKeyStorage: SecureKeyStorage,
    private val dateUtil: DateUtil,
    private val applicationContextProvider: ApplicationContextProvider,
    private val connectivityHelper: ConnectivityHelper,
) : GetMerchantName {
    override fun getMerchantName(
        continueFlag: Boolean?,
        stateEvent : StateEvent,
        onErrorAction: () -> Unit,
        returnViewState : HomeScreenViewState<ProjectUser>,
        updateReturnViewState : (
            HomeScreenViewState<ProjectUser>,
            String?,
            Boolean?,
        ) -> (HomeScreenViewState<ProjectUser>),
    ) : Flow<DataState<HomeScreenViewState<ProjectUser>>?> = flow {
        val methodName: String = "getMerchantName"
        if (LOG_ME) ALog.d(TAG, "Method start: $methodName")
        try {
            val safeApiCall = safeApiCall(
                dispatcher = Dispatchers.IO,
                onErrorAction = onErrorAction,
                apiCall = {
                    secureKeyStorage.getMerchantName()
                }
            )

            val networkDataNullMessage = applicationContextProvider
                .applicationContext(TAG).getString(R.string.failed_to_get_merchant_name)

            val response = object : ApiResponseHandler<HomeScreenViewState<ProjectUser>, String?>(
                response = safeApiCall,
                stateEvent = stateEvent,
                networkDataNullMessage = networkDataNullMessage,
                internetConnectionIsAvailable = connectivityHelper
                    .internetConnectionIsAvailable(applicationContextProvider.applicationContext(TAG)),
            ) {
                override suspend fun handleSuccess(resultObj: String?): DataState<HomeScreenViewState<ProjectUser>>? {
                    val methodName: String = "handleSuccess"
                    if (LOG_ME) ALog.d(TAG, "Method start: $methodName")
                    var messageId : Int
                    var message : String
                    var uiComponentType : UIComponentType
                    var messageType : MessageType

                    if(resultObj != null) {
                        if(LOG_ME)ALog.d(TAG, ".$methodName(): resultObj != null")
                        messageId = R.string.successfully_got_merchant_name
                        message = applicationContextProvider.applicationContext(TAG).getString(messageId)
                        uiComponentType = UIComponentType.None()
                        messageType = MessageType.Success()

                        updateReturnViewState(
                            returnViewState,
                            resultObj,
                            continueFlag,
                        )
                    } else {
                        if(LOG_ME)ALog.d(TAG, ".$methodName(): resultObj == null")
                        messageId = R.string.failed_to_get_merchant_name
                        message = applicationContextProvider.applicationContext(TAG).getString(messageId)
                        uiComponentType = UIComponentType.Toast()
                        messageType = MessageType.Error()
                    }

                    if (LOG_ME) ALog.d(TAG, "Method end: $methodName")
                    return DataState.data(
                        response = Response(
                            messageId, message, uiComponentType, messageType,
                        ),
                        data = returnViewState,
                        stateEvent,
                    )
                }
            }.getResult()

            emit(response)
        } catch (e: Exception) {
            ALog.e(TAG, methodName, e)
        } finally {
            if (LOG_ME) ALog.d(TAG, "Method end: $methodName")
        }
    }
}