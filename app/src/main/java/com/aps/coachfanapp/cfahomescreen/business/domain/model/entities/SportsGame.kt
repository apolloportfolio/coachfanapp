package com.aps.coachfanapp.cfahomescreen.business.domain.model.entities

import android.os.Parcelable
import com.aps.coachfanapp.common.util.ALog
import com.aps.coachfanapp.common.util.DateUtil
import com.aps.coachfanapp.common.util.ParcelableGeoPoint
import com.aps.coachfanapp.common.util.UniqueID
import com.aps.coachfanapp.common.util.UserUniqueID
import com.aps.coachfanapp.core.util.ProjectConstants
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.StorageReference
import com.google.firebase.storage.ktx.storage
import kotlinx.parcelize.IgnoredOnParcel
import kotlinx.parcelize.Parcelize
import java.io.Serializable

private const val TAG = "SportsGame"
private const val LOG_ME = true

// Entity1
@Parcelize
data class SportsGame(
    var id: UniqueID?,
    var created_at: String?,
    var updated_at: String?,

    var latitude: Double?,
    var longitude: Double?,
    var geoLocation: ParcelableGeoPoint?,
    var firestoreGeoLocation: Double?,

    var picture1URI: String?,
    var description: String?,

    var city: String?,

    var ownerID: UserUniqueID?,

    var name: String?,

    var switch1: Boolean? = null,
    var switch2: Boolean? = null,
    var switch3: Boolean? = null,
    var switch4: Boolean? = null,
    var switch5: Boolean? = null,
    var switch6: Boolean? = null,
    var switch7: Boolean? = null,

    var picture2URI: String?,
    var date: String?,
    var location: String?,
    var tvChannel: String?,
    var ticketPrices: String?,
    var currentScore: String?,
    var gameBreakdown: String?,
    var team1Id: UniqueID?,
    var team2Id: UniqueID?,
): Parcelable, Serializable {

    fun getLocationAndDate(dateUtil: DateUtil): String? {
        val formattedDate = date?.let { dateUtil.convertStringDateToFirebaseTimestamp(it) }?.let {
            dateUtil.convertFirebaseTimestampToStringDateForUser(
                it
            )
        }
        return if(formattedDate != null && location != null) {
            "$location, \n$formattedDate"
        } else if(formattedDate == null && location != null) {
            "$location"
        }else if(formattedDate != null && location == null) {
            "$formattedDate"
        } else { null }
    }

    var hasPictures:Boolean =
        picture1URI != null || picture2URI != null

    @IgnoredOnParcel
    val picture1FirebaseImageRef: StorageReference?
        get() {
            return if(id != null && picture1URI != null) {
                if(LOG_ME) ALog.d(
                    TAG, ".picture1ImageRef(): " +
                            "this.id!!.firestoreDocumentID == ${this.id!!.firestoreDocumentID}"
                )
                Firebase.storage.reference
                    .child(ProjectConstants.FIRESTORE_IMAGES_COLLECTION)
                    .child(ProjectConstants.FIRESTORE_ENTITY_1_IMAGES_SUB_COLLECTION)
                    .child(this.id!!.firestoreDocumentID)
                    .child(this.picture1URI!!)
            } else {
                null
            }
        }

    @IgnoredOnParcel
    val picture2FirebaseImageRef: StorageReference?
        get() {
            return if(id != null && picture2URI != null) {
                if(LOG_ME) ALog.d(
                    TAG, ".picture1ImageRef(): " +
                            "this.id!!.firestoreDocumentID == ${this.id!!.firestoreDocumentID}"
                )
                Firebase.storage.reference
                    .child(ProjectConstants.FIRESTORE_IMAGES_COLLECTION)
                    .child(ProjectConstants.FIRESTORE_ENTITY_1_IMAGES_SUB_COLLECTION)
                    .child(this.id!!.firestoreDocumentID)
                    .child(this.picture2URI!!)
            } else {
                null
            }
        }


    object Entity1Constants {
        const val LOCATION_FIELD_NAME = "firestoreGeoLocation"
    }

    override fun toString(): String {
        return "${id.toString()}: geoLocation == $geoLocation \n"
    }

    fun toIdString() : String {
        return "${this.javaClass.simpleName}($id)"
    }

    companion object {
        const val ownerIDFieldName = "ownerID"
    }
}