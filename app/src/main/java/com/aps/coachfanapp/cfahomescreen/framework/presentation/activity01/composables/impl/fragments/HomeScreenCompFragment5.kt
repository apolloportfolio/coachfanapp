package com.aps.coachfanapp.cfahomescreen.framework.presentation.activity01.composables.impl.fragments

import android.app.Activity
import android.widget.Toast
import androidx.compose.animation.core.Spring
import androidx.compose.animation.core.spring
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.tooling.preview.PreviewParameter
import androidx.compose.ui.tooling.preview.PreviewParameterProvider
import com.aps.coachfanapp.cfahomescreen.business.domain.model.entities.SportsTeam
import com.aps.coachfanapp.cfahomescreen.business.domain.model.factories.SportsTeamFactory
import com.aps.coachfanapp.cfahomescreen.framework.presentation.activity01.composables.impl.values.Dimens
import com.aps.coachfanapp.cfahomescreen.framework.presentation.activity01.composables.impl.values.HomeScreenTheme
import com.aps.coachfanapp.cfahomescreen.framework.presentation.activity01.state.HomeScreenStateEvent
import com.aps.coachfanapp.common.business.domain.state.StateEventTracker
import com.aps.coachfanapp.common.framework.presentation.PermissionHandlingData
import com.aps.coachfanapp.common.framework.presentation.views.DialogState
import com.aps.coachfanapp.common.framework.presentation.views.ProgressIndicatorState
import com.aps.coachfanapp.common.framework.presentation.views.ProgressIndicatorType
import com.aps.coachfanapp.common.framework.presentation.views.SnackBarState
import com.aps.coachfanapp.common.framework.presentation.views.SwipeableFragmentWithBottomSheetAndFAB
import com.aps.coachfanapp.common.framework.presentation.views.TipContentIsUnavailable
import com.aps.coachfanapp.common.framework.presentation.views.ToastState
import com.aps.coachfanapp.common.util.ALog
import com.aps.coachfanapp.common.util.UserUniqueID
import com.aps.coachfanapp.core.business.domain.model.entities.ProjectUser
import com.aps.coachfanapp.R

private const val TAG = "HomeScreenCompFragment5"
private const val LOG_ME = true

@OptIn(ExperimentalMaterialApi::class)
@Composable
fun HomeScreenCompFragment5(
    user: ProjectUser?,
    favoriteTeams: List<SportsTeam>?,
    navigateToTeamDetails: (SportsTeam) -> Unit,
    onBackClick: () -> Unit,

    launchStateEvent: (HomeScreenStateEvent) -> Unit = {},

    activity: Activity? = null,
    permissionHandlingData: PermissionHandlingData = PermissionHandlingData(),

    stateEventTracker: StateEventTracker,

    onSwipeLeft: () -> Unit = {},
    onSwipeRight: () -> Unit = {},
    onSwipeUp: () -> Unit = {},
    onSwipeDown: () -> Unit = {},
    onSwipeRightFromComposableSide: () -> Unit = {},
    onSwipeLeftFromComposableSide: () -> Unit = {},
    onSwipeDownFromComposableSide: () -> Unit = {},
    onSwipeUpFromComposableSide: () -> Unit = {},
    floatingActionButtonDrawableId: Int? = null,
    floatingActionButtonOnClick: (() -> Unit)? = null,
    floatingActionButtonContentDescription: String? = null,
    snackBarState: SnackBarState?,
    toastState: ToastState?,
    dialogState: DialogState?,
    progressIndicatorState: ProgressIndicatorState?,
    bottomSheetActions: HomeScreenComposableFragment5BottomSheetActions,

    isPreview: Boolean = false,
) {
    val scope = rememberCoroutineScope()
    val sheetState = rememberBottomSheetState(
        initialValue = BottomSheetValue.Collapsed,
        animationSpec = spring(dampingRatio = Spring.DampingRatioHighBouncy)
    )
    val launchInitStateEvent: () -> Unit = {
        if(activity != null) {
            if(LOG_ME) ALog.w(TAG, "launchInitStateEvent(): " +
                    "activity != null, launching state event in view model.")
            launchStateEvent(
                HomeScreenStateEvent.GetUsersRating
            )
        } else {
            if(LOG_ME) ALog.w(TAG, "launchInitStateEvent(): " +
                    "activity == null, couldn't set initial state event!")
        }
    }
    SwipeableFragmentWithBottomSheetAndFAB(
        launchInitStateEvent = launchInitStateEvent,
        activity = activity,
        permissionHandlingData = permissionHandlingData,
        permissionsRequiredInFragment = mutableSetOf(),
        backgroundDrawableId = R.drawable.default_background,
        onSwipeLeft = onSwipeLeft,
        onSwipeRight = onSwipeRight,
        onSwipeUp = onSwipeUp,
        onSwipeDown = onSwipeDown,
        onSwipeRightFromComposableSide = onSwipeRightFromComposableSide,
        onSwipeLeftFromComposableSide = onSwipeLeftFromComposableSide,
        onSwipeDownFromComposableSide = onSwipeDownFromComposableSide,
        onSwipeUpFromComposableSide = onSwipeUpFromComposableSide,
        leftSideThreshold = Dimens.fragmentsSwipingLeftSideThresholdFraction,
        floatingActionButtonDrawableId = floatingActionButtonDrawableId,
        floatingActionButtonOnClick = floatingActionButtonOnClick,
        floatingActionButtonContentDescription = floatingActionButtonContentDescription,
        snackBarState = snackBarState,
        toastState = toastState,
        dialogState = dialogState,
        progressIndicatorState = progressIndicatorState,
        sheetState = sheetState,
        sheetContent = HomeScreenCompFragment5BottomSheet(
            scope = scope,
            sheetState = sheetState,
            leftButtonOnClick = bottomSheetActions.leftButtonOnClick,
            rightButtonOnClick = bottomSheetActions.rightButtonOnClick,
        ),
        content = {
            if(user == null) {
                TipContentIsUnavailable(tip = stringResource(id = R.string.no_content_to_show_due_to_an_error))
            } else {
                CoachFanAppProfileScreenContent(
                    launchStateEvent = launchStateEvent,
                    activity = activity,
                    stateEventTracker = stateEventTracker,
                    user = user,
                    favoriteTeams = favoriteTeams,
                    navigateToTeamDetails = navigateToTeamDetails,
                    onBackClick = onBackClick,
                    isPreview = isPreview,
                )
            }
        }
    )
}

data class HomeScreenComposableFragment5BottomSheetActions(
    val leftButtonOnClick: () -> Unit,
    val rightButtonOnClick: () -> Unit,
)

// Preview =========================================================================================
class HomeScreenComposableFragment5ParamsProvider :
    PreviewParameterProvider<HomeScreenComposableFragment5Params> {
    override val values: Sequence<HomeScreenComposableFragment5Params> = sequenceOf(
        HomeScreenComposableFragment5Params(
            onSwipeLeft = {},
            onSwipeRight = {},
            onSwipeUp = {},
            onSwipeDown = {},
            floatingActionButtonDrawableId = null,
            floatingActionButtonOnClick = null,
            floatingActionButtonContentDescription = null,
        )
    )
}

data class HomeScreenComposableFragment5Params(
    val onSwipeLeft: () -> Unit = {},
    val onSwipeRight: () -> Unit = {},
    val onSwipeUp: () -> Unit = {},
    val onSwipeDown: () -> Unit = {},
    val floatingActionButtonDrawableId: Int? = null,
    val floatingActionButtonOnClick: (() -> Unit)? = null,
    val floatingActionButtonContentDescription: String? = null,
)

@Preview
@Composable
fun HomeScreenComposableFragment5Preview(
    @PreviewParameter(HomeScreenComposableFragment5ParamsProvider::class) params: HomeScreenComposableFragment5Params
) {
    HomeScreenTheme {
        // Start Interactive Mode to actually see SnackBar
        val snackBarState = remember{
            SnackBarState(
                show = true,
                message = "Snackbar",
                actionLabel = "action",
                iconDrawableRes = R.drawable.ic_launcher,
                onClickAction = {},
                onDismissAction = {},
                duration = SnackbarDuration.Indefinite,
                updateSnackBarInViewModel = {},
            )
        }
        snackBarState.onDismissAction = { snackBarState.show = false }
        snackBarState.onClickAction = { snackBarState.show = false }

        val toastState = ToastState(
            show = true,
            message = "This is a Toast!",
            duration = Toast.LENGTH_LONG,
            updateToastInViewModel = {},
        )

        val dialogState = DialogState(
            show = true,
            onDismissRequest = {},
            title = "Dialog's Title",
            text = "Dialog's text.",
            leftButtonText = "Dismiss",
            leftButtonOnClick = {},
            rightButtonText = "Confirm",
            rightButtonOnClick = {},
            updateDialogInViewModel = {},
        )

        val progressIndicatorState = ProgressIndicatorState(
            show = true,
            progress = 0.7f,
            type = ProgressIndicatorType.DeterminateCircularProgressIndicator,
            updateProgressIndicatorStateInViewModel = {},
        )

        val bottomSheetActions = HomeScreenComposableFragment5BottomSheetActions(
            leftButtonOnClick = {},
            rightButtonOnClick = {},
        )

        val previewUser = ProjectUser(
            id = UserUniqueID("123"),
            updated_at = "2023-01-15T08:30:00",
            created_at = "2023-01-15T08:00:00",
            emailAddress = "user@example.com",
            password = "securepassword",
            profilePhotoImageURI = null,
            name = "John",
            surname = "Doe",
            description = "Passionate sports enthusiast",
            city = "New York",
            emailAddressVerified = true,
            phoneNumberVerified = false
        )

        HomeScreenCompFragment5(
            user = previewUser,
            favoriteTeams = SportsTeamFactory.createPreviewEntitiesList(),
            navigateToTeamDetails = { sportsTeam ->  },
            onBackClick = {},
            stateEventTracker = StateEventTracker(),
            onSwipeLeft = params.onSwipeLeft,
            onSwipeRight = params.onSwipeRight,
            onSwipeUp = params.onSwipeUp,
            onSwipeDown = params.onSwipeDown,
            floatingActionButtonDrawableId = params.floatingActionButtonDrawableId,
            floatingActionButtonOnClick = params.floatingActionButtonOnClick,
            floatingActionButtonContentDescription = params.floatingActionButtonContentDescription,
            snackBarState = snackBarState,
            toastState = toastState,
            dialogState = dialogState,
            progressIndicatorState = progressIndicatorState,
            bottomSheetActions = bottomSheetActions,
        )
    }
}