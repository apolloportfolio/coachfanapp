package com.aps.coachfanapp.cfahomescreen.framework.datasources.cachedatasources.room.impl

import com.aps.coachfanapp.common.util.ALog
import com.aps.coachfanapp.common.util.DateUtil
import com.aps.coachfanapp.common.util.ParcelableGeoPoint
import com.aps.coachfanapp.common.util.UniqueID
import com.aps.coachfanapp.cfahomescreen.business.domain.model.entities.SportsNewsMessage
import com.aps.coachfanapp.cfahomescreen.framework.datasources.cachedatasources.room.abs.SportsNewsMessageDaoService
import com.aps.coachfanapp.cfahomescreen.framework.datasources.cachedatasources.room.daos.SportsNewsMessageDao
import com.aps.coachfanapp.cfahomescreen.framework.datasources.cachedatasources.room.mappers.SportsNewsMessageCacheMapper
import javax.inject.Inject
import javax.inject.Singleton

private const val TAG = "SportsNewsMessageDaoServiceImpl"
private const val LOG_ME = true

@Singleton
class SportsNewsMessageDaoServiceImpl
@Inject
constructor(
    private val dao: SportsNewsMessageDao,
    private val mapper: SportsNewsMessageCacheMapper,
    private val dateUtil: DateUtil
): SportsNewsMessageDaoService {

    override suspend fun insertOrUpdateEntity(entity: SportsNewsMessage): SportsNewsMessage {
        val databaseEntity = dao.getEntityById(entity.id)
        if(LOG_ME) ALog.d(TAG, ".insertOrUpdateEntity(): databaseEntity == $databaseEntity")
        if(databaseEntity == null) {
            if(LOG_ME)ALog.d(TAG, ".insertOrUpdateEntity(): entity before inserting:\n$entity")
            dao.insert(mapper.mapToEntity(entity))
            val entityFromDao = dao.getEntityById(entity.id)
            if(LOG_ME) ALog.d(TAG, ".insertOrUpdateEntity(): entity after inserting == ${entityFromDao}\n")
            return mapper.mapFromEntity(entityFromDao!!)
        } else {
            this.updateEntity(
                entity.id,
                entity.created_at,
                entity.updated_at,

                entity.latitude ,
                entity.longitude,
                entity.geoLocation,
                entity.firestoreGeoLocation,

                entity.picture1URI,

                entity.description,

                entity.title,
                entity.message,
                entity.sportType,
                entity.gameId,
                entity.teamId,
                entity.playerIds,
            )
                val entityFromDao = dao.getEntityById(entity.id)
            if(LOG_ME) ALog.d(TAG, ".insertOrUpdateEntity(): entityFromDao == $entityFromDao")
            return mapper.mapFromEntity(entityFromDao!!)
        }
    }

    override suspend fun insertEntity(entity: SportsNewsMessage): Long {
        return dao.insert(mapper.mapToEntity(entity))
    }

    override suspend fun insertEntities(entities: List<SportsNewsMessage>): LongArray {
        return dao.insertEntities(
            mapper.mapToEntityList(entities)
        )
    }

    override suspend fun getEntityById(id: UniqueID?): SportsNewsMessage? {
        return dao.getEntityById(id)?.let { entity ->
            mapper.mapFromEntity(entity)
        }
    }

    override suspend fun updateEntity(
        id: UniqueID?,
        created_at: String?,
        updated_at: String?,

        latitude : Double?,
        longitude: Double?,
        geoLocation: ParcelableGeoPoint?,
        firestoreGeoLocation: Double?,

        picture1URI: String?,
        description : String?,

        title: String?,
        message: String?,
        sportType: String?,
        gameId: UniqueID?,
        teamId: UniqueID?,
        playerIds: String?,
    ): Int {
        return if(updated_at != null){
            dao.updateEntity(
                id,
                created_at,
                updated_at,

                latitude,
                longitude,
                geoLocation,
                firestoreGeoLocation,

                picture1URI,
                description,

                title,
                message,
                sportType,
                gameId,
                teamId,
                playerIds,
            )
        }else{
            dao.updateEntity(
                id,
                created_at,
                dateUtil.getCurrentTimestamp(),

                latitude,
                longitude,
                geoLocation,
                firestoreGeoLocation,

                picture1URI,

                description,

                title,
                message,
                sportType,
                gameId,
                teamId,
                playerIds,
            )
        }

    }

    override suspend fun deleteEntity(primaryKey: UniqueID?): Int {
        return dao.deleteEntity(primaryKey)
    }

    override suspend fun deleteEntities(entities: List<SportsNewsMessage>): Int {
        val ids = entities.mapIndexed {index, value -> value.id}
        return dao.deleteEntities(ids)
    }

    override suspend fun searchEntities(): List<SportsNewsMessage> {
        return mapper.mapFromEntityList(
            dao.searchEntities()
        )
    }

    override suspend fun getAllEntities(): List<SportsNewsMessage> {
        return mapper.mapFromEntityList(dao.getAllEntities())
    }

    override suspend fun getNumEntities(): Int {
        return dao.getNumEntities()
    }
}