package com.aps.coachfanapp.cfahomescreen.framework.presentation.activity01.composables.impl.fragments

import android.app.Activity
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.heightIn
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.material.Button
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material.icons.filled.Check
import androidx.compose.material.icons.filled.Description
import androidx.compose.material.icons.filled.LocationCity
import androidx.compose.material.icons.filled.MailOutline
import androidx.compose.material.icons.filled.Person
import androidx.compose.material.icons.filled.Update
import androidx.compose.material.icons.filled.Warning
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.constraintlayout.compose.ConstraintLayout
import com.aps.coachfanapp.R
import com.aps.coachfanapp.cfahomescreen.business.domain.model.entities.SportsTeam
import com.aps.coachfanapp.cfahomescreen.business.domain.model.factories.SportsTeamFactory
import com.aps.coachfanapp.cfahomescreen.framework.presentation.activity01.composables.impl.values.BackgroundsOfLayoutsCoachFanApp
import com.aps.coachfanapp.cfahomescreen.framework.presentation.activity01.composables.impl.values.Dimens
import com.aps.coachfanapp.cfahomescreen.framework.presentation.activity01.composables.impl.values.HomeScreenTheme
import com.aps.coachfanapp.cfahomescreen.framework.presentation.activity01.state.HomeScreenStateEvent
import com.aps.coachfanapp.common.business.domain.state.StateEventTracker
import com.aps.coachfanapp.common.framework.presentation.layouts.LazyColumnWithPullToRefresh
import com.aps.coachfanapp.common.framework.presentation.views.BoxWithBackground
import com.aps.coachfanapp.common.framework.presentation.views.ImageFromFirebase
import com.aps.coachfanapp.common.framework.presentation.views.ListItemType02
import com.aps.coachfanapp.common.framework.presentation.views.TipContentIsUnavailable
import com.aps.coachfanapp.common.framework.presentation.views.TitleRowType01
import com.aps.coachfanapp.common.framework.presentation.views.UserInfoItem
import com.aps.coachfanapp.common.util.composableutils.fallbackPainterResource
import com.aps.coachfanapp.common.util.ALog
import com.aps.coachfanapp.common.util.UserUniqueID
import com.aps.coachfanapp.common.util.composableutils.getDrawableIdInPreview
import com.aps.coachfanapp.core.business.domain.model.entities.ProjectUser

private const val TAG = "CoachFanAppProfileScreenContent"
private const val LOG_ME = true

@Composable
fun CoachFanAppProfileScreenContent(
    launchStateEvent: (HomeScreenStateEvent) -> Unit = {},
    activity: Activity? = null,
    stateEventTracker: StateEventTracker,

    user: ProjectUser,
    favoriteTeams: List<SportsTeam>?,
    navigateToTeamDetails: (SportsTeam) -> Unit,
    onBackClick: () -> Unit,

    isPreview: Boolean = false,
) {
    BoxWithBackground(
        backgroundDrawableId = R.drawable.example_background_1,
        composableBackground = BackgroundsOfLayoutsCoachFanApp.backgroundScreen01(
            colors = null,
            brush = null,
            shape = null,
            alpha = 0.6f,
        )
    ) {
        var isEditing by remember { mutableStateOf(false) }
        val launchInitStateEvent: () -> Unit = {
            if(activity != null) {
                if(LOG_ME) ALog.d(
                    TAG, "launchInitStateEvent(): " +
                            "activity != null, launching state event in view model.")
                val path = activity.filesDir
                launchStateEvent(
                    HomeScreenStateEvent.GetSportsGamesAroundUserStateEvent(path)
                )
            } else {
                if(LOG_ME)ALog.w(
                    TAG, "launchInitStateEvent(): " +
                            "activity == null, couldn't set initial state event!")
            }
        }

        Column(
            modifier = Modifier
                .fillMaxSize()
        ) {
            BoxWithBackground(
                backgroundDrawableId = R.drawable.example_background_1,
                composableBackground = BackgroundsOfLayoutsCoachFanApp.backgroundScreen01(
                    colors = null,
                    brush = null,
                    shape = null,
                    alpha = 0.6f,
                )
            ) {
                if(user == null) {
                    TipContentIsUnavailable(tip = stringResource(R.string.no_content_to_show_due_to_an_error))
                } else {
                    ConstraintLayout(
                        modifier = Modifier
                            .fillMaxSize()
                            .padding(
                                start = Dimens.spacingMedium,
                                end = Dimens.spacingMedium,
                                top = Dimens.spacingMedium,
                                bottom = Dimens.spacingMedium,
                            )
                    ) {
                        val (userInfo, favoriteTeamsSection, profileScreenButtonRow,) = createRefs()

                        UserInfo(
                            modifier = Modifier
                                .constrainAs(userInfo) {
                                    top.linkTo(parent.top)
                                    start.linkTo(parent.start)
                                    end.linkTo(parent.end)
                                },
                            user = user,
                            isEditing = isEditing,
                            onDoneClick = { isEditing = false },
                            isPreview = isPreview,
                        )

                        FavoriteTeamsSection(
                            modifier = Modifier
                                .constrainAs(favoriteTeamsSection) {
                                    top.linkTo(userInfo.bottom)
                                    start.linkTo(parent.start)
                                    end.linkTo(parent.end)
                                },
                            launchInitStateEvent = launchInitStateEvent,
                            stateEventTracker = stateEventTracker,
                            favoriteTeams = favoriteTeams,
                            navigateToTeamDetails = navigateToTeamDetails,
                            isPreview = isPreview,
                        )

                        ProfileScreenButtonRow(
                            modifier = Modifier
                                .constrainAs(profileScreenButtonRow) {
                                    bottom.linkTo(parent.bottom)
                                    start.linkTo(parent.start)
                                    end.linkTo(parent.end)
                                },
                            isEditing,
                            onBackClick,
                            onEditClick = {
                                isEditing = !isEditing
                            },
                        )
                    }
                }
            }
        }
    }
}

@Composable
fun FavoriteTeamsSection(
    modifier: Modifier,
    launchInitStateEvent: () -> Unit = {},
    stateEventTracker: StateEventTracker,
    favoriteTeams: List<SportsTeam>?,
    navigateToTeamDetails: (SportsTeam) -> Unit,
    isPreview: Boolean = false,
) {
    if(!favoriteTeams.isNullOrEmpty()) {
        Column(
            modifier = modifier,
        ) {
            TitleRowType01(
                titleString = stringResource(id = R.string.favourite_teams),
                textAlign = TextAlign.Start,
                backgroundDrawableId = R.drawable.example_background_1,
                composableBackground = BackgroundsOfLayoutsCoachFanApp.backgroundTitleType01(
                    colors = null,
                    brush = null,
                    shape = null,
                    alpha = 0.7f,
                ),
                leftPictureDrawableId = null,
                titleFontSize = Dimens.titleFontSizeMedium,
                leftImageTint = MaterialTheme.colors.secondary,
                titleColor = MaterialTheme.colors.secondary,
            )
//            LazyColumn(
//                Modifier.padding(Dimens.columnPadding),
//            ) {

//            val sportsTeam = favoriteTeams[0]
//            ListItemType01(
//                index = 0,
//                itemTitleString = "team name",
//                itemDescription = "team desc",
//                onListItemClick = { navigateToTeamDetails(sportsTeam) },
//                itemRef = if(isPreview) {
//                    null
//                } else {
//                    sportsTeam.picture1FirebaseImageRef
//                },
//            )

            LazyColumnWithPullToRefresh(
                onRefresh = launchInitStateEvent,
                isRefreshing = stateEventTracker.isRefreshing,
                modifier = Modifier.weight(1f),
                verticalArrangement = Arrangement.spacedBy(4.dp)
            ) {
                itemsIndexed(favoriteTeams) { index, sportsTeam ->
                    ListItemType02(
                        index = index,
                        itemTitleString = sportsTeam.name,
                        itemDescription = sportsTeam.description,
                        onListItemClick = { navigateToTeamDetails(sportsTeam) },
                        itemRef = if(isPreview) {
                            null
                        } else {
                            sportsTeam.picture1FirebaseImageRef
                        },
                        backgroundDrawableId = R.drawable.example_background_1,
                        composableBackground = BackgroundsOfLayoutsCoachFanApp.backgroundListItemType01(
                            colors = null,
                            brush = null,
                            shape = null,
                            alpha = 0.6f,
                        ),
                        itemDrawableId = if(isPreview) {
                            getDrawableIdInPreview("coach_fan_app_example_team_", index)
                        } else { null },
                        imageTint = null,
                        buttonTint = MaterialTheme.colors.secondary,
                        itemDescriptionFontWeight = FontWeight.Normal,
                        buttonImageVector = null,
                    )
                }
            }
        }
    }
}

@Composable
fun ProfileScreenButtonRow(
    modifier: Modifier,
    isEditing: Boolean = false,
    onBackClick: () -> Unit,
    onEditClick: () -> Unit,
) {
    Row(
        modifier = modifier
            .fillMaxWidth()
            .heightIn(min = 56.dp),
        horizontalArrangement = Arrangement.SpaceBetween,
        verticalAlignment = Alignment.CenterVertically
    ) {
        IconButton(
            onClick = onBackClick,
            modifier = Modifier
                .size(48.dp)
                .clip(MaterialTheme.shapes.small)
                .background(MaterialTheme.colors.primary)
        ) {
            // Back button
            Icon(
                imageVector = Icons.Default.ArrowBack,
                contentDescription = stringResource(R.string.back),
                tint = MaterialTheme.colors.onPrimary
            )
        }

        // Edit profile button
        if (!isEditing) {
            IconButton(
                onClick = onEditClick,
                modifier = Modifier
                    .size(48.dp)
                    .clip(MaterialTheme.shapes.small)
                    .background(MaterialTheme.colors.primary)
            ) {
                Icon(
                    imageVector = Icons.Default.Update,
                    contentDescription = stringResource(R.string.edit),
                    tint = MaterialTheme.colors.onPrimary
                )
            }
        }
    }
}

@Composable
private fun UserInfo(
    modifier: Modifier,
    launchStateEvent: (HomeScreenStateEvent) -> Unit = {},

    user: ProjectUser,
    isEditing: Boolean,
    onDoneClick: () -> Unit,

    isPreview: Boolean,
) {
    Column(
        modifier = modifier
            .fillMaxWidth()
            .padding(4.dp)
    ) {
        // Profile picture
        Box(
            modifier = Modifier
                .fillMaxWidth()
                .height(240.dp)
                .clip(MaterialTheme.shapes.medium)
                .background(MaterialTheme.colors.primary)
        ) {
            if(isPreview) {
                Image(
                    modifier = modifier,
                    painter = fallbackPainterResource(R.drawable.coach_fan_app_example_user_profile_picture_0),
                    contentDescription = stringResource(R.string.user_profile_image),
                    contentScale = ContentScale.Crop
                )
            } else {
                ImageFromFirebase(
                    firebaseImageRef = user.profilePictureFirebaseImageRef,
                    modifier = Modifier
                        .fillMaxSize()
                        .clip(MaterialTheme.shapes.medium),
                    contentDescription = stringResource(R.string.user_profile_image),
                    contentScale = ContentScale.Crop
                )
            }

            val changeProfilePicture: ()->Unit = {
                // Functionality unavailable in demo project.
            }

            // Edit profile picture button
            if (isEditing) {
                Box(
                    modifier = Modifier
                        .fillMaxSize()
                        .background(
                            MaterialTheme.colors.background.copy(alpha = 0.8f)
                        )
                ) {
                    IconButton(
                        onClick = changeProfilePicture,
                        modifier = Modifier
                            .size(48.dp)
                            .clip(MaterialTheme.shapes.small)
                            .align(Alignment.BottomEnd)
                            .padding(16.dp)
                            .background(MaterialTheme.colors.primary)
                    ) {
                        Icon(
                            imageVector = Icons.Default.Warning,
                            contentDescription = stringResource(R.string.edit_image),
                            tint = MaterialTheme.colors.onPrimary
                        )
                    }
                }
            }
        }

//        Spacer(modifier = Modifier.height(16.dp))

        UserInfoItem(
            icon = Icons.Default.Person,
            label = stringResource(R.string.name),
            value = user.name ?: "",
            isEditing = isEditing,
            onValueChange = {
                user.name = it
                launchStateEvent(
                    HomeScreenStateEvent.UpdateMainEntityEvent(user)
                )
            }
        )

//        Spacer(modifier = Modifier.height(16.dp))

        UserInfoItem(
            icon = Icons.Default.Person,
            label = stringResource(R.string.surname),
            value = user.surname ?: "",
            isEditing = isEditing,
            onValueChange = {
                user.surname = it
                launchStateEvent(
                    HomeScreenStateEvent.UpdateMainEntityEvent(user)
                )
            }
        )

//        Spacer(modifier = Modifier.height(8.dp))

        UserInfoItem(
            icon = Icons.Default.MailOutline,
            label = stringResource(R.string.email),
            value = user.emailAddress ?: "",
            isEditing = isEditing,
            onValueChange = {
                user.emailAddress = it
                launchStateEvent(
                    HomeScreenStateEvent.UpdateMainEntityEvent(user)
                )
            }
        )

//        Spacer(modifier = Modifier.height(8.dp))

        UserInfoItem(
            icon = Icons.Default.LocationCity,
            label = stringResource(R.string.city),
            value = user.city ?: "",
            isEditing = isEditing,
            onValueChange = {
                user.city = it
                launchStateEvent(
                    HomeScreenStateEvent.UpdateMainEntityEvent(user)
                )
            }
        )

//        Spacer(modifier = Modifier.height(8.dp))

        UserInfoItem(
            icon = Icons.Default.Description,
            label = stringResource(R.string.about_me),
            value = user.description ?: "",
            isEditing = isEditing,
            onValueChange = {
                user.description = it
                launchStateEvent(
                    HomeScreenStateEvent.UpdateMainEntityEvent(user)
                )
            }
        )

//        Spacer(modifier = Modifier.height(32.dp))

        if (isEditing) {
            Button(
                onClick = { onDoneClick() },
                modifier = Modifier
                    .fillMaxWidth()
                    .height(48.dp)
            ) {
                Icon(
                    imageVector = Icons.Default.Check,
                    contentDescription = stringResource(R.string.done),
                    modifier = Modifier.size(24.dp),
                    tint = MaterialTheme.colors.onPrimary
                )
                Spacer(modifier = Modifier.width(8.dp))
                Text(
                    text = stringResource(R.string.done),
                    style = MaterialTheme.typography.button,
                    color = MaterialTheme.colors.onPrimary
                )
            }
        }

    }
}

// Preview
@Preview
@Composable
fun CoachFanAppProfileScreenContentPreview() {
    val sampleUser = ProjectUser(
        id = UserUniqueID("123"),
        updated_at = "2023-01-01",
        created_at = "2023-01-01",
        emailAddress = "JohnnySalazzar@example.com",
        password = "securePassword",
        profilePhotoImageURI = null,
        name = "Johnny",
        surname = "Salazzar",
        description = "A passionate baseball fan.",
        city = "Toronto",
        emailAddressVerified = true,
        phoneNumberVerified = true
    )

    HomeScreenTheme {
        CoachFanAppProfileScreenContent(
            launchStateEvent = {},
            activity = null,
            stateEventTracker = StateEventTracker(),
            user = sampleUser,
            favoriteTeams = SportsTeamFactory.createPreviewEntitiesList().take(2),
            navigateToTeamDetails = { sportsTeam -> },
            onBackClick = {},
            isPreview = true,
        )
    }
}
