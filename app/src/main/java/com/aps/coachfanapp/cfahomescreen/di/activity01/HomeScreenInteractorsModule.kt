package com.aps.coachfanapp.cfahomescreen.di.activity01

import com.aps.coachfanapp.common.business.interactors.abstraction.DoNothingAtAll
import com.aps.coachfanapp.common.business.interactors.implementation.DoNothingAtAllImpl
import com.aps.coachfanapp.common.util.DateUtil
import com.aps.coachfanapp.cfahomescreen.business.data.cache.abstraction.SportsGameCacheDataSource
import com.aps.coachfanapp.cfahomescreen.business.data.cache.abstraction.SportsTeamPlayerCacheDataSource
import com.aps.coachfanapp.cfahomescreen.business.data.cache.abstraction.SportsTeamCacheDataSource
import com.aps.coachfanapp.core.business.data.cache.abstraction.UserCacheDataSource
import com.aps.coachfanapp.core.business.data.cache.implementation.UserCacheDataSourceImpl
import com.aps.coachfanapp.cfahomescreen.business.data.network.abs.SportsGameNetworkDataSource
import com.aps.coachfanapp.cfahomescreen.business.data.network.abs.SportsTeamPlayerNetworkDataSource
import com.aps.coachfanapp.cfahomescreen.business.data.network.abs.SportsTeamNetworkDataSource
import com.aps.coachfanapp.core.business.data.network.abstraction.UserNetworkDataSource
import com.aps.coachfanapp.core.business.domain.model.entities.ProjectUser
import com.aps.coachfanapp.cfahomescreen.business.domain.model.factories.SportsGameFactory
import com.aps.coachfanapp.cfahomescreen.business.domain.model.factories.SportsTeamPlayerFactory
import com.aps.coachfanapp.cfahomescreen.business.domain.model.factories.SportsTeamFactory
import com.aps.coachfanapp.core.util.SecureKeyStorage
import com.aps.coachfanapp.cfahomescreen.business.interactors.abs.*
import com.aps.coachfanapp.cfahomescreen.business.interactors.impl.*
import com.aps.coachfanapp.cfahomescreen.framework.presentation.activity01.state.HomeScreenViewState
import com.aps.coachfanapp.core.util.ApplicationContextProvider
import com.aps.coachfanapp.core.util.ConnectivityHelper
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview

@ExperimentalCoroutinesApi
@FlowPreview
@Module
@InstallIn(SingletonComponent::class)
object HomeScreenInteractorsModule {

    @Provides
    fun provideGetUsersRating(
        cacheDataSource: UserCacheDataSource,
        networkDataSource: UserNetworkDataSource,
        applicationContextProvider: ApplicationContextProvider,
        connectivityHelper: ConnectivityHelper,
    ): GetUsersRating {
        return GetUsersRatingImpl(
            cacheDataSource,
            networkDataSource,
            applicationContextProvider,
            connectivityHelper,
        )
    }

    @Provides
    fun InitiateGooglePayPaymentProcess(
        secureKeyStorage: SecureKeyStorage,
        dateUtil: DateUtil,
        applicationContextProvider: ApplicationContextProvider,
        connectivityHelper: ConnectivityHelper,
    ): InitiateGooglePayPaymentProcess {
        return InitiateGooglePayPaymentProcessImpl(
            secureKeyStorage,
            dateUtil,
            applicationContextProvider,
            connectivityHelper,
        )
    }

    @Provides
    fun provideGetMerchantName(
        secureKeyStorage: SecureKeyStorage,
        dateUtil: DateUtil,
        applicationContextProvider: ApplicationContextProvider,
        connectivityHelper: ConnectivityHelper,
    ): GetMerchantName {
        return GetMerchantNameImpl(
            secureKeyStorage,
            dateUtil,
            applicationContextProvider,
            connectivityHelper,
        )
    }

    @Provides
    fun provideGetGatewayNameAndMerchantID(
        secureKeyStorage: SecureKeyStorage,
        dateUtil: DateUtil,
        applicationContextProvider: ApplicationContextProvider,
        connectivityHelper: ConnectivityHelper,
    ): GetGatewayNameAndMerchantID {
        return GetGatewayNameAndMerchantIDImpl(
            secureKeyStorage,
            dateUtil,
            applicationContextProvider,
            connectivityHelper,
        )
    }

    @Provides
    fun provideDownloadExchangeRates(
        applicationContextProvider: ApplicationContextProvider,
        connectivityHelper: ConnectivityHelper,
    ): DownloadExchangeRates {
        return DownloadExchangeRatesImpl(
            applicationContextProvider,
            connectivityHelper,
        )
    }

    @Provides
    fun provideCheckGooglePayAvailability(
        applicationContextProvider: ApplicationContextProvider,
        connectivityHelper: ConnectivityHelper,
    ): CheckGooglePayAvailability {
        return CheckGooglePayAvailabilityImpl(
            applicationContextProvider,
            connectivityHelper,
        )
    }

    @Provides
    fun provideLogoutUser(
        userCacheDataSource: UserCacheDataSource,
        userNetworkDataSource: UserNetworkDataSource,
    ): LogoutUser {
        return LogoutUserImpl(
            userCacheDataSource,
            userNetworkDataSource,
        )
    }

    @Provides
    fun provideGetEntities1AroundUser(
        cacheDataSource: SportsGameCacheDataSource,
        networkDataSource: SportsGameNetworkDataSource,
        entityFactory: SportsGameFactory,
        applicationContextProvider: ApplicationContextProvider,
        connectivityHelper: ConnectivityHelper,
    ): GetSportsGamesAroundUser {
        return GetSportsGamesAroundUserImpl(
            cacheDataSource,
            networkDataSource,
            entityFactory,
            applicationContextProvider,
            connectivityHelper,
        )
    }


    @Provides
    fun provideGetEntities2(
        cacheDataSource: SportsTeamPlayerCacheDataSource,
        networkDataSource: SportsTeamPlayerNetworkDataSource,
        entityFactory: SportsTeamPlayerFactory,
        applicationContextProvider: ApplicationContextProvider,
        connectivityHelper: ConnectivityHelper,
    ): GetSportsTeamPlayers {
        return GetSportsTeamPlayersImpl(
            cacheDataSource,
            networkDataSource,
            entityFactory,
            applicationContextProvider,
            connectivityHelper,
        )
    }

    @Provides
    fun provideGetEntities3(
        cacheDataSource: SportsTeamCacheDataSource,
        networkDataSource: SportsTeamNetworkDataSource,
        entityFactory: SportsTeamFactory,
        applicationContextProvider: ApplicationContextProvider,
        connectivityHelper: ConnectivityHelper,
    ): GetSportsTeams {
        return GetSportsTeamsImpl(
            cacheDataSource,
            networkDataSource,
            entityFactory,
            applicationContextProvider,
            connectivityHelper,
        )
    }

    @Provides
    fun provideSearchEntities1(
        cacheDataSource: SportsGameCacheDataSource,
        networkDataSource: SportsGameNetworkDataSource,
        entityFactory: SportsGameFactory,
        applicationContextProvider: ApplicationContextProvider,
        connectivityHelper: ConnectivityHelper,
    ): SearchSportsGames {
        return SearchSportsGamesImpl(
            cacheDataSource,
            networkDataSource,
            applicationContextProvider,
            connectivityHelper,
        )
    }

    @Provides
    fun provideDoNothingAtAll(
        cacheDataSource: UserCacheDataSourceImpl,
        networkDataSource: UserNetworkDataSource
    ): DoNothingAtAll<
            ProjectUser,
            UserCacheDataSourceImpl,
            UserNetworkDataSource,
            HomeScreenViewState<ProjectUser>> {
        return DoNothingAtAllImpl<
                ProjectUser,
                UserCacheDataSourceImpl,
                UserNetworkDataSource,
                HomeScreenViewState<ProjectUser>>()
    }
}