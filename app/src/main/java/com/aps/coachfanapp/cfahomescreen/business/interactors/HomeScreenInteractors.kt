package com.aps.coachfanapp.cfahomescreen.business.interactors

import com.aps.coachfanapp.common.business.data.cache.StandardCacheDataSource
import com.aps.coachfanapp.common.business.data.network.StandardNetworkDataSource
import com.aps.coachfanapp.common.business.interactors.abstraction.DoNothingAtAll
import com.aps.coachfanapp.cfahomescreen.business.interactors.abs.*
import javax.inject.Inject

// Use cases
class HomeScreenInteractors<
        Entity,
        CacheDataSource: StandardCacheDataSource<Entity>,
        NetworkDataSource: StandardNetworkDataSource<Entity>,
        ViewState: com.aps.coachfanapp.common.business.domain.state.ViewState<Entity>>
@Inject
constructor(
    val getSportsGamesAroundUser: GetSportsGamesAroundUser,
    val searchSportsGames: SearchSportsGames,
    val getSportsTeamPlayers: GetSportsTeamPlayers,
    val getSportsTeams: GetSportsTeams,
    val getUsersRating: GetUsersRating,
    val logout: LogoutUser,
    val checkGooglePayAvailability: CheckGooglePayAvailability,
    val downloadExchangeRates: DownloadExchangeRates,
    val getMerchantName: GetMerchantName,
    val getGatewayNameAndMerchantID: GetGatewayNameAndMerchantID,

    val doNothingAtAll: DoNothingAtAll<
            Entity,
            CacheDataSource,
            NetworkDataSource,
            ViewState
            >,
)