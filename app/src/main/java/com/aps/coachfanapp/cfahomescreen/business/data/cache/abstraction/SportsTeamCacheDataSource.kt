package com.aps.coachfanapp.cfahomescreen.business.data.cache.abstraction

import com.aps.coachfanapp.common.business.data.cache.StandardCacheDataSource
import com.aps.coachfanapp.common.util.UniqueID
import com.aps.coachfanapp.common.util.UserUniqueID
import com.aps.coachfanapp.cfahomescreen.business.domain.model.entities.SportsTeam

interface SportsTeamCacheDataSource: StandardCacheDataSource<SportsTeam> {
    override suspend fun insertOrUpdateEntity(entity: SportsTeam): SportsTeam?

    override suspend fun insertEntity(entity: SportsTeam): Long

    override suspend fun deleteEntity(primaryKey: UniqueID?): Int

    override suspend fun deleteEntities(entities: List<SportsTeam>): Int

    suspend fun updateEntity(
        id : UniqueID?,
        updated_at: String?,
        created_at: String?,

        picture1URI: String?,

        description: String?,

        ownerID: UserUniqueID?,
    ): Int

    override suspend fun searchEntities(
        query: String,
        filterAndOrder: String,
        page: Int
    ): List<SportsTeam>

    override suspend fun getAllEntities(): List<SportsTeam>

    override suspend fun getEntityById(id: UniqueID?): SportsTeam?

    override suspend fun getNumEntities(): Int

    override suspend fun insertEntities(entities: List<SportsTeam>): LongArray
}