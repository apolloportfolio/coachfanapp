package com.aps.coachfanapp.cfahomescreen.business.interactors.impl

import com.aps.coachfanapp.R
import com.aps.coachfanapp.common.business.data.network.ApiResponseHandler
import com.aps.coachfanapp.common.business.data.util.safeApiCall
import com.aps.coachfanapp.common.business.domain.state.*
import com.aps.coachfanapp.common.util.ALog
import com.aps.coachfanapp.cfahomescreen.business.data.cache.abstraction.SportsTeamCacheDataSource
import com.aps.coachfanapp.cfahomescreen.business.data.network.abs.SportsTeamNetworkDataSource
import com.aps.coachfanapp.cfahomescreen.business.domain.model.entities.SportsTeam
import com.aps.coachfanapp.core.business.domain.model.entities.ProjectUser
import com.aps.coachfanapp.cfahomescreen.business.domain.model.factories.SportsTeamFactory
import com.aps.coachfanapp.cfahomescreen.business.interactors.abs.GetSportsTeams
import com.aps.coachfanapp.cfahomescreen.framework.presentation.activity01.state.HomeScreenViewState
import com.aps.coachfanapp.core.util.ApplicationContextProvider
import com.aps.coachfanapp.core.util.ConnectivityHelper
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

private const val TAG = "GetSportsTeamsImpl"
private const val LOG_ME = true

class GetSportsTeamsImpl
@Inject
constructor(
    private val cacheDataSource: SportsTeamCacheDataSource,
    private val networkDataSource: SportsTeamNetworkDataSource,
    private val entityFactory: SportsTeamFactory,
    private val applicationContextProvider: ApplicationContextProvider,
    private val connectivityHelper: ConnectivityHelper,
): GetSportsTeams {
    override fun getSportsTeams(
        stateEvent: StateEvent,
        returnViewState : HomeScreenViewState<ProjectUser>,
        updateReturnViewState : (HomeScreenViewState<ProjectUser>, List<SportsTeam>?) -> HomeScreenViewState<ProjectUser>,
    ): Flow<DataState<HomeScreenViewState<ProjectUser>>?> = flow {
        val userEntities3 = safeApiCall(
            dispatcher = Dispatchers.IO,
            apiCall = {
                returnViewState.mainEntity?.id?.let { networkDataSource.getUsersEntities3(it) }
            }
        )

        val response = object : ApiResponseHandler<HomeScreenViewState<ProjectUser>, List<SportsTeam>>(
            response = userEntities3,
            stateEvent = stateEvent,
            internetConnectionIsAvailable = connectivityHelper
                .internetConnectionIsAvailable(applicationContextProvider.applicationContext(TAG)),
        ) {
            override suspend fun handleSuccess(resultObj: List<SportsTeam>): DataState<HomeScreenViewState<ProjectUser>>? {
                var message: String? = GetEntities3Constants.GET_ENTITY_SUCCESS
                var uiComponentType: UIComponentType? = UIComponentType.None()
                ALog.d(TAG, "getEntities3().handleSuccess(): ")


                if (resultObj == null) {
                    ALog.d(TAG, "getEntities3(): resultObj == null")
                    message = GetEntities3Constants.GET_ENTITY_NO_MATCHING_RESULTS
                    uiComponentType = UIComponentType.Toast()
                } else {
                    ALog.d(TAG, "getEntities3(): resultObj != null")
                    returnViewState.entities3List = ArrayList(resultObj)
                    updateReturnViewState(returnViewState, resultObj)
                }
                return DataState.data(
                    response = Response(
                        messageId = R.string.error,
                        message = message,
                        uiComponentType = uiComponentType as UIComponentType,
                        messageType = MessageType.Success()
                    ),
                    data = returnViewState,
                    stateEvent = stateEvent
                )
            }
        }.getResult()

        emit(response)
    }

    object GetEntities3Constants{
        const val GET_ENTITY_SUCCESS = "Successfully got entities 3."
        const val GET_ENTITY_NO_MATCHING_RESULTS = "There are no entities 3 in database."
        const val SEARCH_ENTITIES_FAILED = "Failed to retrieve the list of entities 3."
    }
}