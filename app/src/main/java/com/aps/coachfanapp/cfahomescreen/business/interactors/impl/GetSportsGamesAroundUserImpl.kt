package com.aps.coachfanapp.cfahomescreen.business.interactors.impl

import android.content.Context
import android.location.Location
import com.aps.coachfanapp.core.util.ApplicationContextProvider
import com.aps.coachfanapp.R
import com.aps.coachfanapp.common.business.data.network.ApiResponseHandler
import com.aps.coachfanapp.common.business.data.network.NetworkConstants
import com.aps.coachfanapp.common.business.data.util.safeApiCall
import com.aps.coachfanapp.common.business.domain.state.*
import com.aps.coachfanapp.common.util.ALog
import com.aps.coachfanapp.cfahomescreen.business.data.cache.abstraction.SportsGameCacheDataSource
import com.aps.coachfanapp.cfahomescreen.business.data.network.abs.SportsGameNetworkDataSource
import com.aps.coachfanapp.cfahomescreen.business.domain.model.entities.SportsGame
import com.aps.coachfanapp.core.business.domain.model.entities.ProjectUser
import com.aps.coachfanapp.cfahomescreen.business.domain.model.factories.SportsGameFactory
import com.aps.coachfanapp.cfahomescreen.business.interactors.abs.GetSportsGamesAroundUser
import com.aps.coachfanapp.cfahomescreen.framework.presentation.activity01.state.HomeScreenViewState
import com.aps.coachfanapp.core.util.ConnectivityHelper
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

private const val TAG = "GetSportsGamesAroundUserImpl"
private const val LOG_ME = true

class GetSportsGamesAroundUserImpl
@Inject
constructor(
    private val sportsGameCacheDataSource: SportsGameCacheDataSource,
    private val sportsGameNetworkDataSource: SportsGameNetworkDataSource,
    private val entityFactory: SportsGameFactory,
    private val applicationContextProvider: ApplicationContextProvider,
    private val connectivityHelper: ConnectivityHelper,
): GetSportsGamesAroundUser {

    override fun getSportsGamesAroundUser(
        location : Location?,
        stateEvent: StateEvent,
        returnViewState: HomeScreenViewState<ProjectUser>,
        updateReturnViewState: (HomeScreenViewState<ProjectUser>, List<SportsGame>?)-> HomeScreenViewState<ProjectUser>,
        onErrorAction: () -> Unit,
    ): Flow<DataState<HomeScreenViewState<ProjectUser>>?> = flow {
        val syncedEntities = safeApiCall(
            Dispatchers.IO,
            networkTimeout = NetworkConstants.NETWORK_TIMEOUT,
            onErrorAction = onErrorAction,
        ){
            syncEntities(location, applicationContextProvider.applicationContext(TAG))
        }

        val response = object: ApiResponseHandler<HomeScreenViewState<ProjectUser>, List<SportsGame>>(
            response = syncedEntities,
            stateEvent = stateEvent,
            internetConnectionIsAvailable = connectivityHelper.internetConnectionIsAvailable(
                applicationContextProvider.applicationContext(TAG)
            ),
        ){
            override suspend fun handleSuccess(resultObj: List<SportsGame>): DataState<HomeScreenViewState<ProjectUser>>? {
                var message: String? = GET_ENTITIES_SUCCESS
                var uiComponentType: UIComponentType? = UIComponentType.None()
                if(LOG_ME) ALog.d(TAG, "getEntities1AroundUser().handleSuccess(): ")


                if(resultObj == null){
                    if(LOG_ME) ALog.d(TAG, "getEntities1AroundUser(): resultObj == null")
                    message = GET_ENTITY_NO_MATCHING_RESULTS
                    uiComponentType = UIComponentType.Toast()
                } else {
                    returnViewState.searchedEntities1List = ArrayList(resultObj)
                    updateReturnViewState(returnViewState, resultObj)
                }
                return DataState.data(
                    response = Response(
                        messageId = R.string.error,
                        message = message,
                        uiComponentType = uiComponentType as UIComponentType,
                        messageType = MessageType.Success()
                    ),
                    data = returnViewState,
                    stateEvent = stateEvent
                )
            }
        }.getResult()

        emit(response)
    }

    // Method downloads entities from network and saves them in cache
    private suspend fun syncEntities(
        location : Location?,
        context: Context,
    ) : List<SportsGame> {
        val firestoreSearchParameters =
            FirestoreSportsGameSearchParameters(null, location)
        val networkEntitiesList = getNetworkEntities(firestoreSearchParameters, context)
        for((index, sportsGame: SportsGame) in networkEntitiesList.withIndex()) {
            if(LOG_ME)ALog.d(TAG, ".syncEntities(): $index. $sportsGame")
            try {
                sportsGameCacheDataSource.insertOrUpdateEntity(sportsGame)
            } catch (e: Exception) {
                ALog.e(TAG, "syncEntities", e)
            }
        }
        return networkEntitiesList
    }

    private suspend fun getNetworkEntities(
        searchParameters : FirestoreSportsGameSearchParameters,
        context: Context,
        ): List<SportsGame>{
        val networkResult = safeApiCall(Dispatchers.IO){
            sportsGameNetworkDataSource.searchEntities(searchParameters)
        }

        val response = object: ApiResponseHandler<List<SportsGame>, List<SportsGame>>(
            response = networkResult,
            stateEvent = null,
            internetConnectionIsAvailable = connectivityHelper.internetConnectionIsAvailable(
                applicationContextProvider.applicationContext(TAG)
            ),
        ){
            override suspend fun handleSuccess(resultObj: List<SportsGame>): DataState<List<SportsGame>>? {
                return DataState.data(
                    response = null,
                    data = resultObj,
                    stateEvent = null
                )
            }
        }.getResult()

        return response?.data ?: ArrayList()
    }

    companion object {
        const val GET_ENTITIES_SUCCESS = "Successfully got sports games around the user."
        const val GET_ENTITY_NO_MATCHING_RESULTS = "There are no sports games that match that query."
        const val SEARCH_ENTITIES_FAILED = "Failed to retrieve the list of sports games."
    }
}
