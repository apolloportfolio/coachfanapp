package com.aps.coachfanapp.cfahomescreen.business.data.cache.abstraction

import com.aps.coachfanapp.common.business.data.cache.StandardCacheDataSource
import com.aps.coachfanapp.common.util.UniqueID
import com.aps.coachfanapp.common.util.UserUniqueID
import com.aps.coachfanapp.cfahomescreen.business.domain.model.entities.SportsTeamPlayer

interface SportsTeamPlayerCacheDataSource: StandardCacheDataSource<SportsTeamPlayer> {
    override suspend fun insertOrUpdateEntity(entity: SportsTeamPlayer): SportsTeamPlayer?

    override suspend fun insertEntity(entity: SportsTeamPlayer): Long

    override suspend fun deleteEntity(primaryKey: UniqueID?): Int

    override suspend fun deleteEntities(entities: List<SportsTeamPlayer>): Int

    suspend fun updateEntity(
        id : UniqueID?,
        updated_at: String?,
        created_at: String?,

        picture1URI: String?,

        description: String?,

        ownerID: UserUniqueID?,

        playerName: String?,
    ): Int

    override suspend fun searchEntities(
        query: String,
        filterAndOrder: String,
        page: Int
    ): List<SportsTeamPlayer>

    override suspend fun getAllEntities(): List<SportsTeamPlayer>

    override suspend fun getEntityById(id: UniqueID?): SportsTeamPlayer?

    override suspend fun getNumEntities(): Int

    override suspend fun insertEntities(entities: List<SportsTeamPlayer>): LongArray
}