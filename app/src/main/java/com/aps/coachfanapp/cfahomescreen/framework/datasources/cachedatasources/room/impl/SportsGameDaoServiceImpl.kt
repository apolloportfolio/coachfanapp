package com.aps.coachfanapp.cfahomescreen.framework.datasources.cachedatasources.room.impl

import com.aps.coachfanapp.common.util.*
import com.aps.coachfanapp.cfahomescreen.business.domain.model.entities.SportsGame
import com.aps.coachfanapp.cfahomescreen.framework.datasources.cachedatasources.room.abs.SportsGameDaoService
import com.aps.coachfanapp.cfahomescreen.framework.datasources.cachedatasources.room.daos.SportsGameDao
import com.aps.coachfanapp.cfahomescreen.framework.datasources.cachedatasources.room.mappers.SportsGameCacheMapper
import javax.inject.Inject
import javax.inject.Singleton

private const val TAG = "SportsGameDaoServiceImpl"
private const val LOG_ME = true

@Singleton
class SportsGameDaoServiceImpl
@Inject
constructor(
    private val dao: SportsGameDao,
    private val mapper: SportsGameCacheMapper,
    private val dateUtil: DateUtil
): SportsGameDaoService {

    override suspend fun insertOrUpdateEntity(entity: SportsGame): SportsGame {
        val databaseEntity = dao.getEntityById(entity.id)
        if(LOG_ME) ALog.d(TAG, ".insertOrUpdateEntity(): databaseEntity == $databaseEntity")
        if(databaseEntity == null) {
            if(LOG_ME)ALog.d(TAG, ".insertOrUpdateEntity(): entity before inserting:\n$entity")
            dao.insert(mapper.mapToEntity(entity))
            val entityFromDao = dao.getEntityById(entity.id)
            if(LOG_ME) ALog.d(TAG, ".insertOrUpdateEntity(): entity after inserting == ${entityFromDao}\n")
            return mapper.mapFromEntity(entityFromDao!!)
        } else {
            this.updateEntity(
                entity.id,
                entity.created_at,
                entity.updated_at,

                entity.latitude ,
                entity.longitude,
                entity.geoLocation,
                entity.firestoreGeoLocation,

                entity.picture1URI,

                entity.description,

                entity.city,

                entity.ownerID,

                entity.name,

                entity.switch1,
                entity.switch2,
                entity.switch3,
                entity.switch4,
                entity.switch5,
                entity.switch6,
                entity.switch7,
                
                entity.picture2URI,
                entity.date,
                entity.location,
                entity.tvChannel,
                entity.ticketPrices,
                entity.currentScore,
                entity.gameBreakdown,
                entity.team1Id,
                entity.team2Id,
                )
            val entityFromDao = dao.getEntityById(entity.id)
            if(LOG_ME) ALog.d(TAG, ".insertOrUpdateEntity(): entityFromDao == $entityFromDao")
            return mapper.mapFromEntity(entityFromDao!!)
        }
    }

    override suspend fun insertEntity(entity: SportsGame): Long {
        return dao.insert(mapper.mapToEntity(entity))
    }

    override suspend fun insertEntities(entities: List<SportsGame>): LongArray {
        return dao.insertEntities(
            mapper.mapToEntityList(entities)
        )
    }

    override suspend fun getEntityById(id: UniqueID?): SportsGame? {
        return dao.getEntityById(id)?.let { entity ->
            mapper.mapFromEntity(entity)
        }
    }

    override suspend fun updateEntity(
        id: UniqueID?,
        created_at: String?,
        updated_at: String?,

        latitude : Double?,
        longitude: Double?,
        geoLocation: ParcelableGeoPoint?,
        firestoreGeoLocation: Double?,

        picture1URI: String?,
        description : String?,

        city : String?,

        ownerID: UserUniqueID?,

        name: String?,

        switch1: Boolean?,
        switch2: Boolean?,
        switch3: Boolean?,
        switch4: Boolean?,
        switch5: Boolean?,
        switch6: Boolean?,
        switch7: Boolean?,

        picture2URI: String?,
        date: String?,
        location: String?,
        tvChannel: String?,
        ticketPrices: String?,
        currentScore: String?,
        gameBreakdown: String?,
        team1Id: UniqueID?,
        team2Id: UniqueID?,
    ): Int {
        return if(updated_at != null){
            dao.updateEntity(
                id,
                created_at,
                updated_at,

                latitude,
                longitude,
                geoLocation,
                firestoreGeoLocation,

                picture1URI,
                description,

                city,

                ownerID,

                name,

                switch1,
                switch2,
                switch3,
                switch4,
                switch5,
                switch6,
                switch7,

                picture2URI,
                date,
                location,
                tvChannel,
                ticketPrices,
                currentScore,
                gameBreakdown,
                team1Id,
                team2Id,
            )
        }else{
            dao.updateEntity(
                id,
                created_at,
                dateUtil.getCurrentTimestamp(),

                latitude,
                longitude,
                geoLocation,
                firestoreGeoLocation,

                picture1URI,

                description,

                city,

                ownerID,

                name,

                switch1,
                switch2,
                switch3,
                switch4,
                switch5,
                switch6,
                switch7,

                picture2URI,
                date,
                location,
                tvChannel,
                ticketPrices,
                currentScore,
                gameBreakdown,
                team1Id,
                team2Id,
            )
        }

    }

    override suspend fun deleteEntity(primaryKey: UniqueID?): Int {
        return dao.deleteEntity(primaryKey)
    }

    override suspend fun deleteEntities(entities: List<SportsGame>): Int {
        val ids = entities.mapIndexed {index, value -> value.id}
        return dao.deleteEntities(ids)
    }

    override suspend fun searchEntities(): List<SportsGame> {
        return mapper.mapFromEntityList(
            dao.searchEntities()
        )
    }

    override suspend fun getAllEntities(): List<SportsGame> {
        return mapper.mapFromEntityList(dao.getAllEntities())
    }

    override suspend fun getNumEntities(): Int {
        return dao.getNumEntities()
    }
}