package com.aps.coachfanapp.cfahomescreen.framework.datasources.networkdatasources.firebase.abs

import com.aps.coachfanapp.core.framework.datasources.networkdatasources.firebase.abstraction.BasicFirestoreService
import com.aps.coachfanapp.cfahomescreen.business.domain.model.entities.SportsNewsMessage
import com.aps.coachfanapp.cfahomescreen.framework.datasources.networkdatasources.firebase.mappers.SportsNewsMessageFirestoreMapper
import com.aps.coachfanapp.cfahomescreen.framework.datasources.networkdatasources.firebase.model.SportsNewsMessageFirestoreEntity

interface SportsNewsMessageFirestoreService: BasicFirestoreService<
        SportsNewsMessage,
        SportsNewsMessageFirestoreEntity,
        SportsNewsMessageFirestoreMapper
        > {

}