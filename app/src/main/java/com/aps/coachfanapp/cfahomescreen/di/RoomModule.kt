package com.aps.coachfanapp.cfahomescreen.di

import com.aps.coachfanapp.cfahomescreen.framework.datasources.cachedatasources.room.daos.SportsGameDao
import com.aps.coachfanapp.cfahomescreen.framework.datasources.cachedatasources.room.daos.SportsNewsMessageDao
import com.aps.coachfanapp.cfahomescreen.framework.datasources.cachedatasources.room.daos.SportsTeamDao
import com.aps.coachfanapp.cfahomescreen.framework.datasources.cachedatasources.room.daos.SportsTeamPlayerDao
import com.aps.coachfanapp.core.framework.datasources.cachedatasources.room.ProjectRoomDatabase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
class RoomModule {
    @Singleton
    @Provides
    fun provideEntity1Dao(database : ProjectRoomDatabase) : SportsGameDao {
        return database.sportsGameDao()
    }

    @Singleton
    @Provides
    fun provideEntity2Dao(database : ProjectRoomDatabase) : SportsTeamPlayerDao {
        return database.sportsTeamPlayerDao()
    }

    @Singleton
    @Provides
    fun provideEntity3Dao(database : ProjectRoomDatabase) : SportsTeamDao {
        return database.sportsTeamDao()
    }

    @Singleton
    @Provides
    fun provideEntity4Dao(database : ProjectRoomDatabase) : SportsNewsMessageDao {
        return database.sportsNewsMessageDao()
    }

//    @Singleton
//    @Provides
//    fun provideSportsFanMessageDao(database : ProjectRoomDatabase) : SportsFanMessageDao {
//        return database.sportsFanMessageDao()
//    }
}