package com.aps.coachfanapp.cfahomescreen.business.domain.model.factories

import com.aps.coachfanapp.common.util.DateUtil
import com.aps.coachfanapp.common.util.ParcelableGeoPoint
import com.aps.coachfanapp.common.util.UniqueID
import com.aps.coachfanapp.common.util.UserUniqueID
import com.aps.coachfanapp.cfahomescreen.business.domain.model.entities.SportsGame
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class SportsGameFactory
@Inject
constructor(
    private val dateUtil: DateUtil
) {
    fun createSingleEntity(
        id: UniqueID?,

        latitude : Double?,
        longitude: Double?,
        geoLocation: ParcelableGeoPoint?,
        firestoreGeoLocation: Double?,

        picture1URI: String?,
        description : String?,

        city: String?,

        ownerID: UserUniqueID?,

        name: String?,

        switch1: Boolean? = null,
        switch2: Boolean? = null,
        switch3: Boolean? = null,
        switch4: Boolean? = null,
        switch5: Boolean? = null,
        switch6: Boolean? = null,
        switch7: Boolean? = null,

        picture2URI: String?,
        date: String?,
        location: String?,
        tvChannel: String?,
        ticketPrices: String?,
        currentScore: String?,
        gameBreakdown: String?,
        team1Id: UniqueID?,
        team2Id: UniqueID?,
    ): SportsGame {
        return SportsGame(
            id,
            dateUtil.getCurrentTimestamp(),
            dateUtil.getCurrentTimestamp(),

            latitude,
            longitude,
            geoLocation,
            firestoreGeoLocation,

            picture1URI,
            description,

            city,

            ownerID,

            name,

            switch1,
            switch2,
            switch3,
            switch4,
            switch5,
            switch6,
            switch7,

            picture2URI,
            date,
            location,
            tvChannel,
            ticketPrices,
            currentScore,
            gameBreakdown,
            team1Id,
            team2Id,
            )
    }

    fun createEntitiesList(numEntities: Int): List<SportsGame> {
        val list: ArrayList<SportsGame> = ArrayList()
        for(i in 0 until numEntities){ // exclusive on upper bound
            list.add(
                createSingleEntity(
                    UniqueID.generateFakeRandomID(),
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                )
            )
        }
        return list
    }


    fun generateEmpty(): SportsGame {
        return createSingleEntity(
            UniqueID.generateFakeRandomID(),
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
        )
    }
    companion object {
        fun createPreviewEntitiesList(): List<SportsGame> {
            return arrayListOf(
                SportsGame(
                    id = UniqueID("test01"),
                    created_at = "2019-04-14 07:05:11 AM",
                    updated_at = "2019-04-14 08:41:22 AM",
                    latitude = 54.23154,
                    longitude = 21.614117,
                    geoLocation = null,
                    firestoreGeoLocation = null,
                    picture1URI = "1",
                    city = "Warsaw",
                    ownerID = UserUniqueID("666", "666"),
                    name = "Turbo Thrust: High-Speed Racing Challenge",
                    description = "Strap into your favorite racing car and experience the adrenaline rush of high-speed competition. Navigate treacherous tracks, outmaneuver rivals, and push your limits to emerge victorious in Turbo Thrust!",
                    picture2URI = "2",
                    date = "2023-01-01",
                    location = "Stadium 1",
                    tvChannel = "Sports Channel 1",
                    ticketPrices = "$20 - $50",
                    currentScore = "1st Place: Collin McRae\n2nd Place: Samantha Quirky\n3rd Place: Anthony Hasty",
                    gameBreakdown = "Exciting race with lots of action!",
                    team1Id = UniqueID("teamA"),
                    team2Id = UniqueID("teamB")
                ),
                SportsGame(
                    id = UniqueID("test02"),
                    name = "Slam Dunk Mania: Basketball Showdown",
                    description = "Get ready to hit the courts in Slam Dunk Mania! Showcase your skills in electrifying basketball matches, perform jaw-dropping dunks, and lead your team to victory in this action-packed showdown!",
                    created_at = "2019-04-14 07:05:11 AM",
                    updated_at = "2019-04-14 08:41:22 AM",
                    latitude = 54.23154,
                    longitude = 21.614117,
                    geoLocation = null,
                    firestoreGeoLocation = null,
                    picture1URI = "1",
                    city = "Warsaw",
                    ownerID = UserUniqueID("666", "666"),
                    picture2URI = "2",
                    date = "2023-01-02",
                    location = "Stadium 2",
                    tvChannel = "Sports Channel 2",
                    ticketPrices = "$30 - $60",
                    currentScore = "Team C 1 - Team D 0",
                    gameBreakdown = "Tense match with a late goal!",
                    team1Id = UniqueID("teamC"),
                    team2Id = UniqueID("teamD")
                ),
                SportsGame(
                    id = UniqueID("test03"),
                    name = "Gridiron Glory: American Football Championship",
                    description = "Take to the gridiron and experience the intensity of American football in Gridiron Glory! Strategize, tackle, and execute game-winning plays as you compete for the ultimate championship title!",
                    created_at = "2019-04-14 07:05:11 AM",
                    updated_at = "2019-04-14 08:41:22 AM",
                    latitude = 54.23154,
                    longitude = 21.614117,
                    geoLocation = null,
                    firestoreGeoLocation = null,
                    picture1URI = "1",
                    city = "Warsaw",
                    ownerID = UserUniqueID("666", "666"),
                    picture2URI = "3",
                    date = "2023-01-03",
                    location = "Stadium 3",
                    tvChannel = "Sports Channel 3",
                    ticketPrices = "$40 - $70",
                    currentScore = "Team E 3 - Team F 2",
                    gameBreakdown = "Back-and-forth battle until the end!",
                    team1Id = UniqueID("teamE"),
                    team2Id = UniqueID("teamF")
                ),
                SportsGame(
                    id = UniqueID("test04"),
                    name = "Skateboard Showdown: Extreme Tricks Challenge",
                    description = "Grab your board and hit the ramps in Skateboard Showdown! Show off your mastery of extreme tricks, navigate obstacle-filled courses, and compete against the best skaters in the world!",
                    created_at = "2019-04-14 07:05:11 AM",
                    updated_at = "2019-04-14 08:41:22 AM",
                    latitude = 51.23164,
                    longitude = 22.614167,
                    geoLocation = null,
                    firestoreGeoLocation = null,
                    picture1URI = "1",
                    city = "Warsaw",
                    ownerID = UserUniqueID("666", "666"),
                    picture2URI = "4",
                    date = "2023-01-04",
                    location = "Stadium 4",
                    tvChannel = "Sports Channel 4",
                    ticketPrices = "$50 - $80",
                    currentScore = "Team G 4 - Team H 3",
                    gameBreakdown = "High-scoring thriller with a last-minute goal!",
                    team1Id = UniqueID("teamG"),
                    team2Id = UniqueID("teamH")
                ),
                SportsGame(
                    id = UniqueID("test05"),
                    name = "Goalkeeper Gauntlet: Soccer Penalty Shootout",
                    description = "Step into the goalkeeper's shoes and face off against the most formidable strikers in Goalkeeper Gauntlet! Dive, block, and anticipate shots to become the hero of the penalty shootout!",
                    created_at = "2019-04-14 07:05:11 AM",
                    updated_at = "2019-04-14 08:41:22 AM",
                    latitude = 51.23254,
                    longitude = 22.615117,
                    geoLocation = null,
                    firestoreGeoLocation = null,
                    picture1URI = "1",
                    city = "Warsaw",
                    ownerID = UserUniqueID("666", "666"),
                    picture2URI = "5",
                    date = "2023-01-05",
                    location = "Stadium 5",
                    tvChannel = "Sports Channel 5",
                    ticketPrices = "$60 - $90",
                    currentScore = "Team I 2 - Team J 1",
                    gameBreakdown = "Close match with a late winner!",
                    team1Id = UniqueID("teamI"),
                    team2Id = UniqueID("teamJ")
                ),
                SportsGame(
                    id = UniqueID("test06"),
                    name = "Alpine Ascent: Skiing Adventure Challenge",
                    description = "Embark on an exhilarating skiing adventure in Alpine Ascent! Race down snowy slopes, navigate challenging terrain, and conquer the mountains as you strive for victory in this thrilling challenge!",
                    created_at = "2019-04-14 07:05:11 AM",
                    updated_at = "2019-04-14 08:41:22 AM",
                    latitude = 51.23354,
                    longitude = 22.614717,
                    geoLocation = null,
                    firestoreGeoLocation = null,
                    picture1URI = "1",
                    city = "Warsaw",
                    ownerID = UserUniqueID("666", "666"),
                    picture2URI = "6",
                    date = "2023-01-06",
                    location = "Stadium 6",
                    tvChannel = "Sports Channel 6",
                    ticketPrices = "$70 - $100",
                    currentScore = "Team K 1 - Team L 0",
                    gameBreakdown = "Defensive battle with a single goal!",
                    team1Id = UniqueID("teamK"),
                    team2Id = UniqueID("teamL")
                ),
                SportsGame(
                    id = UniqueID("test07"),
                    name = "Smash Serve: Tennis Tournament Edition",
                    description = "Serve up aces and dominate the court in Smash Serve! Compete against skilled opponents, master your shots, and rise through the ranks to claim the title of tennis champion!",
                    created_at = "2019-04-14 07:05:11 AM",
                    updated_at = "2019-04-14 08:41:22 AM",
                    latitude = 51.23154,
                    longitude = 22.624117,
                    geoLocation = null,
                    firestoreGeoLocation = null,
                    picture1URI = "1",
                    city = "Warsaw",
                    ownerID = UserUniqueID("666", "666"),
                    picture2URI = "7",
                    date = "2023-01-07",
                    location = "Stadium 7",
                    tvChannel = "Sports Channel 7",
                    ticketPrices = "$80 - $110",
                    currentScore = "Team M 3 - Team N 2",
                    gameBreakdown = "Dramatic match with a comeback!",
                    team1Id = UniqueID("teamM"),
                    team2Id = UniqueID("teamN")
                ),
                SportsGame(
                    id = UniqueID("test08"),
                    name = "Hoop Heroes: Street Basketball Clash",
                    description = "Hit the streets and show off your basketball skills in Hoop Heroes! Dribble, shoot, and dunk your way to victory in fast-paced street basketball clashes against fierce competitors!",
                    created_at = "2019-04-14 07:05:11 AM",
                    updated_at = "2019-04-14 08:41:22 AM",
                    latitude = 51.23154,
                    longitude = 22.664117,
                    geoLocation = null,
                    firestoreGeoLocation = null,
                    picture1URI = "1",
                    city = "Warsaw",
                    ownerID = UserUniqueID("666", "666"),
                    picture2URI = "8",
                    date = "2023-01-08",
                    location = "Stadium 8",
                    tvChannel = "Sports Channel 8",
                    ticketPrices = "$90 - $120",
                    currentScore = "Team O 2 - Team P 1",
                    gameBreakdown = "Nail-biting match with a late winner!",
                    team1Id = UniqueID("teamO"),
                    team2Id = UniqueID("teamP")
                ),
                SportsGame(
                    id = UniqueID("test09"),
                    name = "Touchdown Triumph: Rugby Championship",
                    description = "Experience the brute force and tactical prowess of rugby in Touchdown Triumph! Break through defensive lines, execute precision passes, and score unforgettable tries on your journey to championship glory!",
                    created_at = "2019-04-14 07:05:11 AM",
                    updated_at = "2019-04-14 08:41:22 AM",
                    latitude = 51.23154,
                    longitude = 22.6147,
                    geoLocation = null,
                    firestoreGeoLocation = null,
                    picture1URI = "1",
                    city = "Warsaw",
                    ownerID = UserUniqueID("666", "666"),
                    picture2URI = "9",
                    date = "2023-01-09",
                    location = "Stadium 9",
                    tvChannel = "Sports Channel 9",
                    ticketPrices = "$100 - $130",
                    currentScore = "Team Q 3 - Team R 0",
                    gameBreakdown = "Dominant performance with multiple goals!",
                    team1Id = UniqueID("teamQ"),
                    team2Id = UniqueID("teamR")
                ),
                SportsGame(
                    id = UniqueID("test10"),
                    name = "Synchronized Splash: Swimming Competition",
                    description = "Dive into the pool and compete in the ultimate swimming showdown in Synchronized Splash! Perfect your strokes, synchronize your movements, and aim for gold as you compete against elite swimmers from around the world!",
                    created_at = "2019-04-14 07:05:11 AM",
                    updated_at = "2019-04-14 08:41:22 AM",
                    latitude = 51.23154,
                    longitude = 22.714117,
                    geoLocation = null,
                    firestoreGeoLocation = null,
                    picture1URI = "1",
                    city = "Warsaw",
                    ownerID = UserUniqueID("666", "666"),
                    picture2URI = "10",
                    date = "2023-01-10",
                    location = "Stadium 10",
                    tvChannel = "Sports Channel 10",
                    ticketPrices = "$110 - $140",
                    currentScore = "Team S 1 - Team T 0",
                    gameBreakdown = "Tight match with a single goal!",
                    team1Id = UniqueID("teamS"),
                    team2Id = UniqueID("teamT")
                )
            )
        }
    }
}