package com.aps.coachfanapp.cfahomescreen.di

import android.content.SharedPreferences
import androidx.lifecycle.ViewModelProvider
import com.aps.coachfanapp.common.util.DateUtil
import com.aps.coachfanapp.cfahomescreen.CoachFanAppViewModelFactory
import com.aps.coachfanapp.core.business.data.cache.implementation.UserCacheDataSourceImpl
import com.aps.coachfanapp.core.business.data.network.abstraction.UserNetworkDataSource
import com.aps.coachfanapp.core.business.domain.model.entities.ProjectUser
import com.aps.coachfanapp.cfahomescreen.business.domain.model.factories.SportsGameFactory
import com.aps.coachfanapp.core.business.domain.model.factories.UserFactory
import com.aps.coachfanapp.cfahomescreen.business.interactors.HomeScreenInteractors
import com.aps.coachfanapp.cfahomescreen.framework.presentation.activity01.state.HomeScreenViewState
import com.aps.coachfanapp.core.util.ApplicationContextProvider
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import javax.inject.Named
import javax.inject.Singleton

@ExperimentalCoroutinesApi
@FlowPreview
@Module
@InstallIn(SingletonComponent::class)
object CoachFanAppViewModelsModule {
    @Singleton
    @JvmStatic
    @Provides
    @Named("CoachFanAppViewModelFactory")
    fun provideCoachFanAppViewModelFactory(
        homeScreenInteractors: HomeScreenInteractors<
                ProjectUser,
                UserCacheDataSourceImpl,
                UserNetworkDataSource,
                HomeScreenViewState<ProjectUser>
                >,
        dateUtil: DateUtil,
        entityFactory: UserFactory,
        sportsGameFactory: SportsGameFactory,
        editor: SharedPreferences.Editor,
        applicationContextProvider: ApplicationContextProvider,
        //sharedPreferences: SharedPreferences
    ): ViewModelProvider.Factory {
        return CoachFanAppViewModelFactory(
            homeScreenInteractors = homeScreenInteractors,
            dateUtil = dateUtil,
            userFactory = entityFactory,
            sportsGameFactory = sportsGameFactory,
            editor = editor,
            applicationContextProvider = applicationContextProvider
        )
    }
}