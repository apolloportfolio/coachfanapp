package com.aps.coachfanapp.cfahomescreen.business.domain.model.factories

import com.aps.coachfanapp.common.util.DateUtil
import com.aps.coachfanapp.common.util.ParcelableGeoPoint
import com.aps.coachfanapp.common.util.UniqueID
import com.aps.coachfanapp.cfahomescreen.business.domain.model.entities.SportsNewsMessage
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class SportsNewsMessageFactory
@Inject
constructor(
    private val dateUtil: DateUtil
) {
    fun createSingleEntity(
        id: UniqueID?,

        latitude : Double?,
        longitude: Double?,
        geoLocation: ParcelableGeoPoint?,
        firestoreGeoLocation: Double?,

        picture1URI: String?,
        description : String?,

        title: String?,
        message: String?,
        sportType: String?,
        gameId: UniqueID?,
        teamId: UniqueID?,
        playerIds: String?,
    ): SportsNewsMessage {
        return SportsNewsMessage(
            id,
            dateUtil.getCurrentTimestamp(),
            dateUtil.getCurrentTimestamp(),

            latitude,
            longitude,
            geoLocation,
            firestoreGeoLocation,

            picture1URI,
            description,
            
            title,
            message,
            sportType,
            gameId,
            teamId,
            playerIds,
            )
    }

    fun createEntitiesList(numEntities: Int): List<SportsNewsMessage> {
        val list: ArrayList<SportsNewsMessage> = ArrayList()
        for(i in 0 until numEntities){ // exclusive on upper bound
            list.add(
                createSingleEntity(
                    UniqueID.generateFakeRandomID(),
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                )
            )
        }
        return list
    }


    fun generateEmpty(): SportsNewsMessage {
        return createSingleEntity(
            UniqueID.generateFakeRandomID(),
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
        )
    }

    companion object {
        fun createPreviewEntitiesList(): List<SportsNewsMessage> {
            return arrayListOf(
                SportsNewsMessage(
                    id = UniqueID("test01"),
                    created_at = "2019-04-14 07:05:11 AM",
                    updated_at = "2019-04-14 08:41:22 AM",
                    latitude = 54.23154,
                    longitude = 21.614117,
                    geoLocation = null,
                    firestoreGeoLocation = null,
                    picture1URI = "1",
                    description = "Exciting Match Ends in Dramatic Penalty Shootout. Fans Ecstatic!",
                    title = "Football Fever Grips the Nation",
                    message = "Manchester United vs. Liverpool ends in a nail-biting penalty shootout. Don't miss the highlights!",
                    sportType = "Football",
                    gameId = UniqueID("game01"),
                    teamId = UniqueID("team01"),
                    playerIds = "player01, player02",
                ),
                SportsNewsMessage(
                    id = UniqueID("test02"),
                    created_at = "2019-04-14 07:05:11 AM",
                    updated_at = "2019-04-14 08:41:22 AM",
                    latitude = 54.23154,
                    longitude = 21.614117,
                    geoLocation = null,
                    firestoreGeoLocation = null,
                    picture1URI = "1",
                    description = "Intense Battle on the Court: Lakers vs. Celtics",
                    title = "NBA Showdown: Lakers vs. Celtics",
                    message = "Catch the highlights of the thrilling Lakers vs. Celtics game. Who emerged victorious?",
                    sportType = "Basketball",
                    gameId = UniqueID("game02"),
                    teamId = UniqueID("team02"),
                    playerIds = "player03, player04",
                ),
                SportsNewsMessage(
                    id = UniqueID("test03"),
                    created_at = "2019-04-14 07:05:11 AM",
                    updated_at = "2019-04-14 08:41:22 AM",
                    latitude = 54.23154,
                    longitude = 21.614117,
                    geoLocation = null,
                    firestoreGeoLocation = null,
                    picture1URI = "1",
                    description = "Epic Showdown: Federer vs. Nadal in French Open Final",
                    title = "Tennis Legends Clash in French Open Final",
                    message = "Witness the historic match between Federer and Nadal at the French Open. Who will lift the trophy?",
                    sportType = "Tennis",
                    gameId = UniqueID("game03"),
                    teamId = UniqueID("team03"),
                    playerIds = "player05, player06",
                ),
                SportsNewsMessage(
                    id = UniqueID("test04"),
                    created_at = "2019-04-14 07:05:11 AM",
                    updated_at = "2019-04-14 08:41:22 AM",
                    latitude = 51.23164,
                    longitude = 22.614167,
                    geoLocation = null,
                    firestoreGeoLocation = null,
                    picture1URI = "1",
                    description = "Dramatic Finish: Last-Minute Goal Decides the Match",
                    title = "Soccer Thriller: Real Madrid vs. Barcelona",
                    message = "Real Madrid and Barcelona face off in a high-stakes match. Watch the thrilling highlights!",
                    sportType = "Soccer",
                    gameId = UniqueID("game04"),
                    teamId = UniqueID("team04"),
                    playerIds = "player7, player08",
                ),
                SportsNewsMessage(
                    id = UniqueID("test05"),
                    created_at = "2019-04-14 07:05:11 AM",
                    updated_at = "2019-04-14 08:41:22 AM",
                    latitude = 51.23254,
                    longitude = 22.615117,
                    geoLocation = null,
                    firestoreGeoLocation = null,
                    picture1URI = "1",
                    description = "Exciting Golf Tournament: Woods Leads the Pack",
                    title = "Golf Championship: Woods Dominates",
                    message = "Tiger Woods showcases his brilliance in the latest golf championship. Watch his remarkable shots!",
                    sportType = "Golf",
                    gameId = UniqueID("game05"),
                    teamId = UniqueID("team05"),
                    playerIds = "player9, player10",
                ),
                SportsNewsMessage(
                    id = UniqueID("test06"),
                    created_at = "2019-04-14 07:05:11 AM",
                    updated_at = "2019-04-14 08:41:22 AM",
                    latitude = 51.23354,
                    longitude = 22.614717,
                    geoLocation = null,
                    firestoreGeoLocation = null,
                    picture1URI = "1",
                    description = "Intensity Soars: Volleyball Finals Reach Climax",
                    title = "Volleyball Showdown: Finals Heat Up",
                    message = "Don't miss the electrifying moments from the volleyball finals. Who will emerge as the champions?",
                    sportType = "Volleyball",
                    gameId = UniqueID("game06"),
                    teamId = UniqueID("team06"),
                    playerIds = "player11, player12",
                ),
                SportsNewsMessage(
                    id = UniqueID("test07"),
                    created_at = "2019-04-14 07:05:11 AM",
                    updated_at = "2019-04-14 08:41:22 AM",
                    latitude = 51.23154,
                    longitude = 22.624117,
                    geoLocation = null,
                    firestoreGeoLocation = null,
                    picture1URI = "1",
                    description = "Exciting Cricket Match: T20 World Cup Semifinal",
                    title = "T20 World Cup: Semifinal Drama",
                    message = "Witness the excitement of the T20 World Cup semifinal. Which team secured a spot in the final?",
                    sportType = "Cricket",
                    gameId = UniqueID("game07"),
                    teamId = UniqueID("team07"),
                    playerIds = "player13, player14",
                ),
                SportsNewsMessage(
                    id = UniqueID("test08"),
                    created_at = "2019-04-14 07:05:11 AM",
                    updated_at = "2019-04-14 08:41:22 AM",
                    latitude = 51.23154,
                    longitude = 22.664117,
                    geoLocation = null,
                    firestoreGeoLocation = null,
                    picture1URI = "1",
                    description = "Swimming Spectacle: World Championships Conclude",
                    title = "World Swimming Championships: Highlights",
                    message = "Catch the thrilling moments from the world swimming championships. Who set new records?",
                    sportType = "Swimming",
                    gameId = UniqueID("game08"),
                    teamId = UniqueID("team08"),
                    playerIds = "player15, player16",
                ),
                SportsNewsMessage(
                    id = UniqueID("test09"),
                    created_at = "2019-04-14 07:05:11 AM",
                    updated_at = "2019-04-14 08:41:22 AM",
                    latitude = 51.23154,
                    longitude = 22.6147,
                    geoLocation = null,
                    firestoreGeoLocation = null,
                    picture1URI = "1",
                    description = "Baseball Excitement: League Finals Reach Climax",
                    title = "Baseball League Finals: Intense Battles",
                    message = "Experience the intensity of the baseball league finals. Who will emerge victorious?",
                    sportType = "Baseball",
                    gameId = UniqueID("game09"),
                    teamId = UniqueID("team09"),
                    playerIds = "player17, player18",
                ),
                SportsNewsMessage(
                    id = UniqueID("test10"),
                    created_at = "2019-04-14 07:05:11 AM",
                    updated_at = "2019-04-14 08:41:22 AM",
                    latitude = 51.23154,
                    longitude = 22.714117,
                    geoLocation = null,
                    firestoreGeoLocation = null,
                    picture1URI = "1",
                    description = "Gymnastics Extravaganza: International Championships",
                    title = "International Gymnastics Championships: Highlights",
                    message = "Witness the incredible performances at the international gymnastics championships. Who claimed gold?",
                    sportType = "Gymnastics",
                    gameId = UniqueID("game10"),
                    teamId = UniqueID("team10"),
                    playerIds = "player19, player20",
                )
            )
        }
    }
}