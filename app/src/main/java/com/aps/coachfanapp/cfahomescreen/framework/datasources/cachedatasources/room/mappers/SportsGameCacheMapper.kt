package com.aps.coachfanapp.cfahomescreen.framework.datasources.cachedatasources.room.mappers

import com.aps.coachfanapp.common.business.domain.model.EntityMapper
import com.aps.coachfanapp.cfahomescreen.business.domain.model.entities.SportsGame
import com.aps.coachfanapp.cfahomescreen.framework.datasources.cachedatasources.room.model.SportsGameCacheEntity
import javax.inject.Inject

class SportsGameCacheMapper
@Inject
constructor() : EntityMapper<SportsGameCacheEntity, SportsGame> {
    override fun mapFromEntity(entity: SportsGameCacheEntity): SportsGame {
        return SportsGame(
            entity.id,
            entity.created_at,
            entity.updated_at,

            entity.latitude ,
            entity.longitude,
            entity.geoLocation,
            entity.firestoreGeoLocation,

            entity.picture1URI,
            entity.description,

            entity.city,

            entity.ownerID,

            entity.name,

            entity.switch1,
            entity.switch2,
            entity.switch3,
            entity.switch4,
            entity.switch5,
            entity.switch6,
            entity.switch7,
            
            entity.picture2URI,
            entity.date,
            entity.location,
            entity.tvChannel,
            entity.ticketPrices,
            entity.currentScore,
            entity.gameBreakdown,
            entity.team1Id,
            entity.team2Id,
            )
    }

    override fun mapToEntity(domainModel: SportsGame): SportsGameCacheEntity {
        return SportsGameCacheEntity(
            domainModel.id!!,
            domainModel.created_at,
            domainModel.updated_at,

            domainModel.latitude ,
            domainModel.longitude,
            domainModel.geoLocation,
            domainModel.firestoreGeoLocation,

            domainModel.picture1URI,
            domainModel.description,

            domainModel.city,

            domainModel.ownerID,

            domainModel.name,

            domainModel.switch1,
            domainModel.switch2,
            domainModel.switch3,
            domainModel.switch4,
            domainModel.switch5,
            domainModel.switch6,
            domainModel.switch7,

            domainModel.picture2URI,
            domainModel.date,
            domainModel.location,
            domainModel.tvChannel,
            domainModel.ticketPrices,
            domainModel.currentScore,
            domainModel.gameBreakdown,
            domainModel.team1Id,
            domainModel.team2Id,
            )
    }

    override fun mapFromEntityList(entities : List<SportsGameCacheEntity>) : List<SportsGame> {
        return entities.map{mapFromEntity(it)}
    }

    override fun mapToEntityList(entities: List<SportsGame>): List<SportsGameCacheEntity> {
        return entities.map{mapToEntity(it)}
    }
}