package com.aps.coachfanapp.cfahomescreen.framework.datasources.networkdatasources.firebase.impl

import com.aps.coachfanapp.common.business.data.util.cLog
import com.aps.coachfanapp.common.util.ALog
import com.aps.coachfanapp.common.util.DateUtil
import com.aps.coachfanapp.common.util.UniqueID
import com.aps.coachfanapp.common.util.UserUniqueID
import com.aps.coachfanapp.common.util.extensions.getSizeString
import com.aps.coachfanapp.core.business.data.network.abstraction.UserNetworkDataSource
import com.aps.coachfanapp.core.framework.datasources.networkdatasources.firebase.implementation.BasicFirestoreServiceImpl
import com.aps.coachfanapp.cfahomescreen.business.domain.model.entities.SportsTeamPlayer
import com.aps.coachfanapp.cfahomescreen.framework.datasources.networkdatasources.firebase.abs.SportsTeamPlayerFirestoreService
import com.aps.coachfanapp.cfahomescreen.framework.datasources.networkdatasources.firebase.mappers.SportsTeamPlayerFirestoreMapper
import com.aps.coachfanapp.cfahomescreen.framework.datasources.networkdatasources.firebase.model.SportsTeamPlayerFirestoreEntity
import kotlinx.coroutines.tasks.await
import javax.inject.Inject
import javax.inject.Singleton


@Singleton
class SportsTeamPlayerFirestoreServiceImpl
@Inject
constructor(
//    private val firebaseAuth: FirebaseAuth,
//    private val firestore: FirebaseFirestore,
    private val networkMapper: SportsTeamPlayerFirestoreMapper,
    private val dateUtil: DateUtil,
    private val userNetworkDataSource: UserNetworkDataSource,
): SportsTeamPlayerFirestoreService,
    BasicFirestoreServiceImpl<SportsTeamPlayer, SportsTeamPlayerFirestoreEntity, SportsTeamPlayerFirestoreMapper>(
//        firebaseAuth,
//        firestore,
        networkMapper,
        dateUtil
    ) {

    override fun getCollectionName(): String {
        return COLLECTION_NAME
    }

    override fun getDeletesCollectionName(): String {
        return DELETES_COLLECTION_NAME
    }

    override fun setEntityID(entity: SportsTeamPlayer, id: UniqueID) {
        entity.id = id
    }

    override fun getEntityID(entity: SportsTeamPlayer): UniqueID? {
        return entity.id
    }

    override fun setFirestoreEntityID(entity: SportsTeamPlayerFirestoreEntity, id: UniqueID) {
        entity.id = id
    }

    override fun updateEntityTimestamp(entity: SportsTeamPlayer, timestamp: String){
        entity.updated_at = timestamp
    }

    override fun updateFirestoreEntityTimestamp(entity: SportsTeamPlayerFirestoreEntity, timestamp: String){
        entity.updated_at = timestamp
    }

    override fun getFirestoreEntityType(): Class<SportsTeamPlayerFirestoreEntity> {
        return SportsTeamPlayerFirestoreEntity::class.java
    }

    override fun getFirestoreEntityID(entity: SportsTeamPlayerFirestoreEntity): UniqueID? {
        return entity.id
    }

    override suspend fun getUsersSportsTeamPlayers(userId: UserUniqueID): List<SportsTeamPlayer>? {
        val methodName: String = "getUsersEntities2"
        var result: List<SportsTeamPlayer>
        ALog.d(TAG, "Method start: $methodName")
        try {
            val collectionName = getCollectionName()
            result = networkMapper.mapFromEntityList(
                firestore
                    .collection(collectionName)
                    .whereEqualTo(SportsTeamPlayer.ownerIDFieldName, userId)
                    .get()
                    .addOnFailureListener {
                        // send error reports to Firebase Crashlytics
                        cLog(it.message)
                        ALog.w(TAG, "$methodName(): Failed to get user's entities2.")
                    }
                    .addOnSuccessListener {
                        if(LOG_ME) ALog.d(
                            TAG, "$methodName(): " +
                                "Successfully got all user's entities2.")
                    }
                    .await()
                    .toObjects(getFirestoreEntityType())
            )
            if(LOG_ME)ALog.d(
                TAG, ".$methodName(): " +
                    "Got ${result.getSizeString()} entities2 " +
                    "belonging to user: $userId stored in $collectionName")
            return result
        } catch (e: Exception) {
            ALog.e(TAG, methodName, e)
            return emptyList()
        } finally {
            ALog.d(TAG, "Method end: $methodName")
        }
    }

    companion object {
        const val TAG = "Entity2FirestoreServiceImpl"
        const val LOG_ME = true
        const val COLLECTION_NAME = "entity2"
        const val DELETES_COLLECTION_NAME = "entity2_d"
    }
}