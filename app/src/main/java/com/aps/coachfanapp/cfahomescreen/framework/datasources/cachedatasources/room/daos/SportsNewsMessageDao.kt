package com.aps.coachfanapp.cfahomescreen.framework.datasources.cachedatasources.room.daos

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.aps.coachfanapp.common.util.UniqueID
import com.aps.coachfanapp.cfahomescreen.framework.datasources.cachedatasources.room.model.SportsNewsMessageCacheEntity
import com.google.firebase.firestore.GeoPoint
import java.util.*


@Dao
interface SportsNewsMessageDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(entity : SportsNewsMessageCacheEntity) : Long

    @Query("SELECT * FROM sportsnewsmessage")
    suspend fun get() : List<SportsNewsMessageCacheEntity>
    
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insertEntities(entities: List<SportsNewsMessageCacheEntity>): LongArray

    @Query("SELECT * FROM sportsnewsmessage WHERE id = :id")
    suspend fun getEntityById(id: UniqueID?): SportsNewsMessageCacheEntity?

    @Query("DELETE FROM sportsnewsmessage WHERE id IN (:ids)")
    suspend fun deleteEntities(ids: List<UniqueID?>): Int

    @Query("DELETE FROM sportsnewsmessage")
    suspend fun deleteAllEntities()

    @Query("SELECT * FROM sportsnewsmessage")
    suspend fun getAllEntities(): List<SportsNewsMessageCacheEntity>

    @Query(
        """
        UPDATE sportsnewsmessage 
        SET 
        created_at = :created_at,
        updated_at = :updated_at,
        latitude = :latitude,
        longitude = :longitude,
        geoLocation = :geoLocation,
        firestoreGeoLocation = :firestoreGeoLocation,
        picture1URI = :picture1URI,
        description = :description,
        title = :title,
        message = :message,
        sportType = :sportType,
        gameId = :gameId,
        teamId = :teamId,
        playerIds = :playerIds
        WHERE id = :id
        """
    )
    suspend fun updateEntity(
        id: UniqueID?,
        created_at: String?,
        updated_at: String?,

        latitude : Double?,
        longitude: Double?,
        geoLocation: GeoPoint?,
        firestoreGeoLocation: Double?,

        picture1URI: String?,

        description : String?,
        title: String?,
        message: String?,
        sportType: String?,
        gameId: UniqueID?,
        teamId: UniqueID?,
        playerIds: String?,
    ): Int

    @Query("DELETE FROM sportsnewsmessage WHERE id = :primaryKey")
    suspend fun deleteEntity(primaryKey: UniqueID?): Int

    @Query("SELECT * FROM sportsnewsmessage")
    suspend fun searchEntities(): List<SportsNewsMessageCacheEntity>
    
    @Query("SELECT COUNT(*) FROM sportsnewsmessage")
    suspend fun getNumEntities(): Int
}