package com.aps.coachfanapp.cfahomescreen.framework.presentation.activity01.composables.impl.fragments

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.material.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import com.aps.coachfanapp.R
import com.aps.coachfanapp.cfahomescreen.business.domain.model.entities.SportsGame
import com.aps.coachfanapp.cfahomescreen.business.domain.model.entities.SportsNewsMessage
import com.aps.coachfanapp.cfahomescreen.business.domain.model.factories.SportsGameFactory
import com.aps.coachfanapp.cfahomescreen.business.domain.model.factories.SportsNewsMessageFactory
import com.aps.coachfanapp.cfahomescreen.framework.presentation.activity01.composables.impl.values.BackgroundsOfLayoutsCoachFanApp
import com.aps.coachfanapp.cfahomescreen.framework.presentation.activity01.composables.impl.values.Dimens
import com.aps.coachfanapp.cfahomescreen.framework.presentation.activity01.composables.impl.values.HomeScreenTheme
import com.aps.coachfanapp.common.framework.presentation.views.BoxWithBackground
import com.aps.coachfanapp.common.framework.presentation.views.ListItemType06
import com.aps.coachfanapp.common.framework.presentation.views.TitleRowType01
import com.aps.coachfanapp.common.util.composableutils.getDrawableIdInPreview

@Composable
fun CoachFanAppHomeScreenContent(
    sportsGames: List<SportsGame>,
    sportsNewsMessages: List<SportsNewsMessage>,
    onGameClick: (SportsGame) -> Unit,
    onNewsClick: (SportsNewsMessage) -> Unit,

    isPreview: Boolean = false,
) {
    BoxWithBackground(
        backgroundDrawableId = R.drawable.example_background_1,
        composableBackground = BackgroundsOfLayoutsCoachFanApp.backgroundScreen01(
            colors = null,
            brush = null,
            shape = null,
            alpha = 0.6f,
        ),
    ) {

        Column(
            modifier = Modifier
                .fillMaxSize()
                .padding(Dimens.columnPadding)
        ) {
            // Live Scores Section
            TitleRowType01(
                titleString = stringResource(id = R.string.live_scores),
                textAlign = TextAlign.Start,
                backgroundDrawableId = R.drawable.example_background_1,
                composableBackground = BackgroundsOfLayoutsCoachFanApp.backgroundTitleType01(
                    colors = null,
                    brush = null,
                    shape = null,
                    alpha = 0.7f,
                ),
                leftPictureDrawableId = R.drawable.baseline_sports_score_24,
                titleFontSize = Dimens.titleFontSize,
                leftImageTint = MaterialTheme.colors.secondary,
                titleColor = MaterialTheme.colors.secondary,
            )

            LazyColumn {
                itemsIndexed(sportsGames) { index, sportsGame ->
                    ListItemType06(
                        index = index,
                        itemTitleString = sportsGame.name,
                        itemDescription = sportsGame.description,
                        onListItemClick = { onGameClick(sportsGame) },
                        itemRef = if(!isPreview) {
                            sportsGame.picture1FirebaseImageRef
                        } else {
                            null
                        },
                        backgroundDrawableId = R.drawable.example_background_1,
                        composableBackground = BackgroundsOfLayoutsCoachFanApp.backgroundListItemType01(
                            colors = null,
                            brush = null,
                            shape = null,
                            alpha = 0.6f,
                        ),
                        itemDrawableId = if(isPreview) {
                            getDrawableIdInPreview("coach_fan_app_example_thumbnail_", index+10)
                        } else { null },
                        imageTint = null,
                    )
                }
            }

            // News & Updates Section
            TitleRowType01(
                titleString = stringResource(id = R.string.news_updates),
                textAlign = TextAlign.Start,
                backgroundDrawableId = R.drawable.example_background_1,
                composableBackground = BackgroundsOfLayoutsCoachFanApp.backgroundTitleType01(
                    null, null, null, 0.7f,
                ),
                leftPictureDrawableId = R.drawable.baseline_newspaper_24,
                titleFontSize = Dimens.titleFontSize,
                leftImageTint = MaterialTheme.colors.secondary,
                titleColor = MaterialTheme.colors.secondary,
            )

            LazyColumn {
                itemsIndexed(sportsNewsMessages) { index, sportsNewsMessage ->
                    ListItemType06(
                        index = index,
                        itemTitleString = sportsNewsMessage.title,
                        itemDescription = sportsNewsMessage.description,
                        onListItemClick = { onNewsClick(sportsNewsMessage) },
                        itemRef = if(!isPreview) {
                            sportsNewsMessage.picture1FirebaseImageRef
                        } else {
                            null
                        },
                        backgroundDrawableId = R.drawable.example_background_1,
                        composableBackground = BackgroundsOfLayoutsCoachFanApp.backgroundListItemType01(
                            colors = null,
                            brush = null,
                            shape = null,
                            alpha = 0.6f,
                        ),
                        itemDrawableId = if(isPreview) {
                            getDrawableIdInPreview("coach_fan_app_example_thumbnail_", index)
                        } else { null },
                        imageTint = null,
                    )
                }
            }
        }
    }
}


@Preview
@Composable
fun CoachFanAppHomeScreenContentPreview(

) {
    HomeScreenTheme {
        CoachFanAppHomeScreenContent(
            sportsGames = SportsGameFactory.createPreviewEntitiesList().take(4),
            sportsNewsMessages = SportsNewsMessageFactory.createPreviewEntitiesList().take(4),
            onGameClick = {},
            onNewsClick = {},
            isPreview = true,
        )
    }
}