package com.aps.coachfanapp.cfahomescreen.business.data.network.abs

import android.net.Uri
import com.aps.coachfanapp.common.business.data.network.StandardNetworkDataSource
import com.aps.coachfanapp.common.util.UniqueID
import com.aps.coachfanapp.common.util.UserUniqueID
import com.aps.coachfanapp.cfahomescreen.business.domain.model.entities.SportsGame
import com.aps.coachfanapp.cfahomescreen.business.interactors.impl.FirestoreSportsGameSearchParameters

interface SportsGameNetworkDataSource: StandardNetworkDataSource<SportsGame> {
    override suspend fun insertOrUpdateEntity(entity: SportsGame): SportsGame?

    override suspend fun deleteEntity(primaryKey: UniqueID?)

    override suspend fun insertDeletedEntity(entity: SportsGame)

    override suspend fun insertDeletedEntities(Entities: List<SportsGame>)

    override suspend fun deleteDeletedEntity(entity: SportsGame)

    override suspend fun getDeletedEntities(): List<SportsGame>

    override suspend fun deleteAllEntities()

    override suspend fun searchEntity(entity: SportsGame): SportsGame?

    override suspend fun getAllEntities(): List<SportsGame>

    override suspend fun insertOrUpdateEntities(Entities: List<SportsGame>): List<SportsGame>?

    override suspend fun getEntityById(id: UniqueID): SportsGame?

    suspend fun searchEntities(
        searchParameters : FirestoreSportsGameSearchParameters
    ) : List<SportsGame>?

    suspend fun getUsersEntites1(userID: UserUniqueID) : List<SportsGame>?

    suspend fun uploadSportsGamePhotoToFirestore(
        entity : SportsGame,
        entitysPhotoUri : Uri,
        entitysPhotoNumber: Int,
    ) : String?
}