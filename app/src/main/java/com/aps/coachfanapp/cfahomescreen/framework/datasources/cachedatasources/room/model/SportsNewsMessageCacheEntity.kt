package com.aps.coachfanapp.cfahomescreen.framework.datasources.cachedatasources.room.model

import androidx.annotation.NonNull
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.aps.coachfanapp.common.util.ParcelableGeoPoint
import com.aps.coachfanapp.common.util.UniqueID

@Entity(tableName = "sportsnewsmessage")
data class SportsNewsMessageCacheEntity(
    @PrimaryKey(autoGenerate = false)
    @ColumnInfo(name = "id")
    @NonNull
    var id : UniqueID,

    @ColumnInfo(name = "created_at")
    val created_at: String?,

    @ColumnInfo(name = "updated_at")
    val updated_at: String?,

    @ColumnInfo(name = "latitude")
    var latitude : Double?,

    @ColumnInfo(name = "longitude")
    var longitude: Double?,

    @ColumnInfo(name = "geoLocation")
    var geoLocation: ParcelableGeoPoint?,

    @ColumnInfo(name = "firestoreGeoLocation")
    var firestoreGeoLocation: Double?,

    @ColumnInfo(name = "picture1URI")
    var picture1URI: String?,

    @ColumnInfo(name = "description")
    var description : String?,


    @ColumnInfo(name = "title")
    var title : String?,

    @ColumnInfo(name = "message")
    var message : String?,

    @ColumnInfo(name = "sportType")
    var sportType : String?,

    @ColumnInfo(name = "gameId")
    var gameId : UniqueID?,

    @ColumnInfo(name = "teamId")
    var teamId : UniqueID?,

    @ColumnInfo(name = "playerIds")
    var playerIds : String?,

    ) {
}