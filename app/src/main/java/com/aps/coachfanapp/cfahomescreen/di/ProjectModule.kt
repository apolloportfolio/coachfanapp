package com.aps.coachfanapp.cfahomescreen.di

import com.aps.coachfanapp.core.util.ApplicationContextProvider
import com.aps.coachfanapp.core.util.ApplicationContextProviderImpl
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
class ProjectModule {
    @Singleton
    @Provides
    fun provideApplicationContextProvider() : ApplicationContextProvider {
        return ApplicationContextProviderImpl()
    }
}