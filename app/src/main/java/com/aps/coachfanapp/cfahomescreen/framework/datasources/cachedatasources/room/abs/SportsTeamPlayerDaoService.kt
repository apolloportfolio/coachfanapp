package com.aps.coachfanapp.cfahomescreen.framework.datasources.cachedatasources.room.abs


import com.aps.coachfanapp.common.util.UniqueID
import com.aps.coachfanapp.cfahomescreen.business.domain.model.entities.SportsTeamPlayer

interface SportsTeamPlayerDaoService {
    suspend fun insertOrUpdateEntity(entity: SportsTeamPlayer): SportsTeamPlayer

    suspend fun insertEntity(entity: SportsTeamPlayer): Long

    suspend fun insertEntities(Entities: List<SportsTeamPlayer>): LongArray

    suspend fun getEntityById(id: UniqueID?): SportsTeamPlayer?

    suspend fun updateEntity(
        id : UniqueID?,
        updated_at: String?,
        created_at: String?,

        picture1URI: String?,

        description: String?,

        sportsTeamId: UniqueID?,

        playerName: String?,
    ): Int

    suspend fun deleteEntity(primaryKey: UniqueID?): Int

    suspend fun deleteEntities(Entities: List<SportsTeamPlayer>): Int

    suspend fun searchEntities(): List<SportsTeamPlayer>

    suspend fun getAllEntities(): List<SportsTeamPlayer>

    suspend fun getNumEntities(): Int
}