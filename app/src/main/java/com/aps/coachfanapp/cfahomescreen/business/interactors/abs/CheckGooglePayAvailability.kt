package com.aps.coachfanapp.cfahomescreen.business.interactors.abs

import android.content.Context
import com.aps.coachfanapp.common.business.domain.state.DataState
import com.aps.coachfanapp.common.business.domain.state.StateEvent
import com.aps.coachfanapp.core.business.domain.model.entities.ProjectUser
import com.aps.coachfanapp.cfahomescreen.framework.presentation.activity01.state.HomeScreenViewState
import com.google.android.gms.wallet.PaymentsClient
import kotlinx.coroutines.flow.Flow

interface CheckGooglePayAvailability {
    fun checkGooglePayAvailability(
        context: Context,
        continueFlag: Boolean?,
        stateEvent: StateEvent,
        onErrorAction: () -> Unit = {},
        returnViewState: HomeScreenViewState<ProjectUser>,
        updateReturnViewState: (
            HomeScreenViewState<ProjectUser>,
            PaymentsClient?,
            Boolean?,
            Boolean?,
        )-> HomeScreenViewState<ProjectUser>,
    ): Flow<DataState<HomeScreenViewState<ProjectUser>>?>
}