package com.aps.coachfanapp.cfahomescreen.framework.datasources.cachedatasources.room.model

import androidx.annotation.NonNull
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.aps.coachfanapp.common.util.UniqueID
import com.aps.coachfanapp.common.util.UserUniqueID

@Entity(tableName = "sportsfanmessage")
data class SportsFanMessageCacheEntity(
    @PrimaryKey(autoGenerate = false)
    @ColumnInfo(name = "id")
    @NonNull
    var id: UniqueID,

    @ColumnInfo(name = "created_at")
    var created_at: String? = null,

    @ColumnInfo(name = "updated_at")
    var updated_at: String? = null,

    @ColumnInfo(name = "sender")
    var sender: UserUniqueID?,

    @ColumnInfo(name = "recipient")
    var recipient: UserUniqueID?,

    @ColumnInfo(name = "message")
    var message: String?,

    @ColumnInfo(name = "gameId")
    var gameId: UniqueID?,

    @ColumnInfo(name = "seen")
    var seen: Boolean? = null,
    ) {
}