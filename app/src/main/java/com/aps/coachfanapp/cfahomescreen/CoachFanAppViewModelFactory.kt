package com.aps.coachfanapp.cfahomescreen

import android.app.Application
import android.content.SharedPreferences
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.aps.coachfanapp.common.framework.presentation.ApplicationViewModel
import com.aps.coachfanapp.common.util.DateUtil
import com.aps.coachfanapp.core.business.data.cache.implementation.UserCacheDataSourceImpl
import com.aps.coachfanapp.core.business.data.network.abstraction.UserNetworkDataSource
import com.aps.coachfanapp.core.business.domain.model.entities.ProjectUser
import com.aps.coachfanapp.cfahomescreen.business.domain.model.factories.SportsGameFactory
import com.aps.coachfanapp.core.business.domain.model.factories.UserFactory
import com.aps.coachfanapp.cfahomescreen.business.interactors.HomeScreenInteractors
import com.aps.coachfanapp.cfahomescreen.framework.presentation.activity01.HomeScreenViewModel
import com.aps.coachfanapp.cfahomescreen.framework.presentation.activity01.state.HomeScreenViewState
import com.aps.coachfanapp.core.util.ApplicationContextProvider
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview


@FlowPreview
@ExperimentalCoroutinesApi
class CoachFanAppViewModelFactory
constructor(
    private val homeScreenInteractors: HomeScreenInteractors<
            ProjectUser,
            UserCacheDataSourceImpl,
            UserNetworkDataSource,
            HomeScreenViewState<ProjectUser>
            >,
    private val dateUtil: DateUtil,
    private val userFactory: UserFactory,
    private val sportsGameFactory: SportsGameFactory,
    private val editor: SharedPreferences.Editor,
    private val applicationContextProvider: ApplicationContextProvider,
    //private val sharedPreferences: SharedPreferences
) : ViewModelProvider.Factory
{

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return when(modelClass){

            HomeScreenViewModel::class.java -> {
                HomeScreenViewModel(
                    interactors = homeScreenInteractors,
                    dateUtil = dateUtil,
                    entityFactory = userFactory,
                    applicationContextProvider = applicationContextProvider
                ) as T
            }




            ApplicationViewModel::class.java -> {
                ApplicationViewModel(
                application = applicationContextProvider
                    .applicationContext("CoachFanAppViewModelFactory") as Application
                ) as T
            }

            else -> {
                throw IllegalArgumentException("unknown model class $modelClass")
            }
        }
    }
}