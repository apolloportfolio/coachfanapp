package com.aps.coachfanapp.cfahomescreen.framework.presentation.activity01.composables.impl.fragments

import android.app.Activity
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.res.dimensionResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.aps.coachfanapp.R
import com.aps.coachfanapp.cfahomescreen.business.domain.model.entities.SportsGame
import com.aps.coachfanapp.cfahomescreen.business.domain.model.entities.SportsNewsMessage
import com.aps.coachfanapp.cfahomescreen.business.domain.model.factories.SportsGameFactory
import com.aps.coachfanapp.cfahomescreen.business.domain.model.factories.SportsNewsMessageFactory
import com.aps.coachfanapp.cfahomescreen.framework.presentation.activity01.composables.impl.values.BackgroundsOfLayoutsCoachFanApp
import com.aps.coachfanapp.cfahomescreen.framework.presentation.activity01.composables.impl.values.Dimens
import com.aps.coachfanapp.cfahomescreen.framework.presentation.activity01.composables.impl.values.HomeScreenTheme
import com.aps.coachfanapp.cfahomescreen.framework.presentation.activity01.composables.impl.values.Shapes
import com.aps.coachfanapp.cfahomescreen.framework.presentation.activity01.composables.impl.values.Type
import com.aps.coachfanapp.cfahomescreen.framework.presentation.activity01.state.HomeScreenStateEvent
import com.aps.coachfanapp.common.business.domain.state.StateEventTracker
import com.aps.coachfanapp.common.framework.presentation.layouts.LazyColumnWithPullToRefresh
import com.aps.coachfanapp.common.framework.presentation.views.BoxWithBackground
import com.aps.coachfanapp.common.framework.presentation.views.ListItemType06
import com.aps.coachfanapp.common.framework.presentation.views.TitleRowType01
import com.aps.coachfanapp.common.util.composableutils.getDrawableIdInPreview

private const val TAG = "CoachFanAppGameDetailsScreenContent"
private const val LOG_ME = true

@Composable
fun CoachFanAppGameDetailsScreenContent(
    launchStateEvent: (HomeScreenStateEvent) -> Unit = {},
    activity: Activity? = null,
    stateEventTracker: StateEventTracker,
    refreshSportsNewsMessages: () -> Unit,

    sportsGame: SportsGame,
    sportsNewsList: List<SportsNewsMessage>?,
    onNewsClick: (SportsNewsMessage) -> Unit,

    isPreview: Boolean = false,
) {
    BoxWithBackground(
        backgroundDrawableId = R.drawable.example_background_1,
        composableBackground = BackgroundsOfLayoutsCoachFanApp.backgroundScreen01(
            colors = null,
            brush = null,
            shape = null,
            alpha = 0.6f,
        )
    ) {
        Column(
            modifier = Modifier
                .fillMaxSize()
                .padding(Dimens.defaultPadding),
        ) {
            Column(
                modifier = Modifier.weight(1f),
            ) {
                // Title and Date
                TitleRowType01(
                    titleString = sportsGame.name ?: "",
                    subtitleString = sportsGame.date ?: "",
                    textAlign = TextAlign.Center,
                    backgroundDrawableId = R.drawable.example_background_1,
                    composableBackground = BackgroundsOfLayoutsCoachFanApp.backgroundTitleType01(
                        colors = null,
                        brush = null,
                        shape = null,
                        alpha = 0.7f,
                    ),
                    titleFontSize = Dimens.titleFontSize,
                    subtitleFontSize = Dimens.titleFontSizeMedium,
                    leftImageTint = MaterialTheme.colors.secondary,
                    titleColor = MaterialTheme.colors.secondaryVariant,
                    subtitleColor = MaterialTheme.colors.secondaryVariant,
                )

                // Ticket Prices
                TitleRowType01(
                    titleString = stringResource(R.string.ticket_prices),
                    subtitleString = sportsGame.ticketPrices ?: "",
                    textAlign = TextAlign.Start,
                    backgroundDrawableId = R.drawable.example_background_1,
                    composableBackground = BackgroundsOfLayoutsCoachFanApp.backgroundListItemType01(
                        colors = null,
                        brush = null,
                        shape = null,
                        alpha = 0.7f,
                    ),
                    leftPictureDrawableId = R.drawable.baseline_price_check_24,
                    titleFontSize = Dimens.titleFontSizeMedium,
                    subtitleFontSize = Dimens.titleFontSizeSmall,
                    leftImageTint = MaterialTheme.colors.secondary,
                    titleColor = MaterialTheme.colors.secondary,
                )

                // Current Score
                TitleRowType01(
                    titleString = stringResource(R.string.current_score) ,
                    subtitleString = sportsGame.currentScore ?: "",
                    textAlign = TextAlign.Start,
                    backgroundDrawableId = R.drawable.example_background_1,
                    composableBackground = BackgroundsOfLayoutsCoachFanApp.backgroundListItemType01(
                        colors = null,
                        brush = null,
                        shape = null,
                        alpha = 0.7f,
                    ),
                    leftPictureDrawableId = R.drawable.baseline_scoreboard_24,
                    titleFontSize = Dimens.titleFontSizeMedium,
                    subtitleFontSize = Dimens.titleFontSizeSmall,
                    leftImageTint = MaterialTheme.colors.secondary,
                    titleColor = MaterialTheme.colors.secondary,
                )

                // Game Breakdown
                TitleRowType01(
                    titleString = stringResource(id = R.string.game_breakdown),
                    subtitleString = sportsGame.gameBreakdown ?: "",
                    textAlign = TextAlign.Start,
                    backgroundDrawableId = R.drawable.example_background_1,
                    composableBackground = BackgroundsOfLayoutsCoachFanApp.backgroundTitleType01(
                        colors = null,
                        brush = null,
                        shape = null,
                        alpha = 0.7f,
                    ),
                    leftPictureDrawableId = R.drawable.baseline_sports_score_24,
                    titleFontSize = Dimens.titleFontSize,
                    leftImageTint = MaterialTheme.colors.secondary,
                    titleColor = MaterialTheme.colors.secondary,
                )
            }

            // LazyColumn with SportsNewsMessage items
            if(sportsNewsList != null) {
                Spacer(modifier = Modifier
                    .fillMaxWidth()
                    .height(dimensionResource(id = R.dimen.margin_between_text_views)))
                // News & Updates Section
                TitleRowType01(
                    titleString = stringResource(id = R.string.news_updates),
                    textAlign = TextAlign.Start,
                    backgroundDrawableId = R.drawable.example_background_1,
                    composableBackground = BackgroundsOfLayoutsCoachFanApp.backgroundTitleType01(
                        null, null, null, 0.7f,
                    ),
                    leftPictureDrawableId = R.drawable.baseline_newspaper_24,
                    titleFontSize = Dimens.titleFontSize,
                    leftImageTint = MaterialTheme.colors.secondary,
                    titleColor = MaterialTheme.colors.secondary,
                )
                LazyColumnWithPullToRefresh(
                    onRefresh = refreshSportsNewsMessages,
                    isRefreshing = stateEventTracker.isRefreshing,
                    modifier = Modifier.weight(1f),
                ) {
                    itemsIndexed(sportsNewsList) { index, sportsNewsMessage ->
                        ListItemType06(
                            index = index,
                            itemTitleString = sportsNewsMessage.title,
                            itemDescription = sportsNewsMessage.description,
                            onListItemClick = { onNewsClick(sportsNewsMessage) },
                            itemRef = if(!isPreview) {
                                sportsNewsMessage.picture1FirebaseImageRef
                            } else {
                                null
                            },
                            backgroundDrawableId = R.drawable.example_background_1,
                            composableBackground = BackgroundsOfLayoutsCoachFanApp.backgroundListItemType01(
                                colors = null,
                                brush = null,
                                shape = null,
                                alpha = 0.6f,
                            ),
                            itemDrawableId = if(isPreview) {
                                getDrawableIdInPreview("coach_fan_app_example_thumbnail_", index)
                            } else { null },
                            imageTint = null,
                        )
                    }
                }
            }
        }
    }
}

@Composable
private fun SportsNewsItem(sportsNews: SportsNewsMessage, index: Int) {
    Box(
        modifier = Modifier
            .fillMaxWidth()
            .height(100.dp)
            .background(MaterialTheme.colors.surface, Shapes.medium)
            .padding(Dimens.paddingMedium),
        contentAlignment = Alignment.CenterStart,
    ) {
        Text(
            text = sportsNews.title ?: "",
            style = Type.body1,
            modifier = Modifier.testTag("sports_news_title_$index"),
        )
    }
}

@Preview
@Composable
private fun CoachFanAppGameDetailsScreenContentPreview() {
    HomeScreenTheme {
        CoachFanAppGameDetailsScreenContent(
            onNewsClick = {},
            launchStateEvent = {},
            activity = null,
            stateEventTracker = StateEventTracker.PreviewStateEventTracker,
            refreshSportsNewsMessages = {},

            sportsGame = SportsGameFactory.createPreviewEntitiesList()[0],
            sportsNewsList = SportsNewsMessageFactory.createPreviewEntitiesList(),

            isPreview = true,
        )

    }
}