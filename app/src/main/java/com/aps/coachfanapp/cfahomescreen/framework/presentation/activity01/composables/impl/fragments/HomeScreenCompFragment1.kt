package com.aps.coachfanapp.cfahomescreen.framework.presentation.activity01.composables.impl.fragments

import android.Manifest
import android.app.Activity
import android.location.Location
import android.widget.Toast
import androidx.compose.animation.core.Spring
import androidx.compose.animation.core.spring
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.tooling.preview.PreviewParameter
import androidx.compose.ui.tooling.preview.PreviewParameterProvider
import com.aps.coachfanapp.R
import com.aps.coachfanapp.common.business.domain.state.StateEventTracker
import com.aps.coachfanapp.common.framework.presentation.PermissionHandlingData
import com.aps.coachfanapp.common.framework.presentation.TAG_PERMISSION_ASKING
import com.aps.coachfanapp.common.framework.presentation.views.*
import com.aps.coachfanapp.common.util.ALog
import com.aps.coachfanapp.common.util.DeviceLocation
import com.aps.coachfanapp.cfahomescreen.business.domain.model.entities.SportsGame
import com.aps.coachfanapp.cfahomescreen.business.domain.model.entities.SportsNewsMessage
import com.aps.coachfanapp.cfahomescreen.business.domain.model.factories.SportsGameFactory
import com.aps.coachfanapp.cfahomescreen.business.domain.model.factories.SportsNewsMessageFactory
import com.aps.coachfanapp.cfahomescreen.framework.presentation.activity01.composables.impl.values.Dimens
import com.aps.coachfanapp.cfahomescreen.framework.presentation.activity01.composables.impl.values.HomeScreenTheme
import com.aps.coachfanapp.cfahomescreen.framework.presentation.activity01.state.HomeScreenCard1SearchFilters
import com.aps.coachfanapp.cfahomescreen.framework.presentation.activity01.state.HomeScreenStateEvent

private const val TAG = "HomeScreenCompFragment1"
private const val LOG_ME = true
const val HomeScreenComposableFragment1TestTag = TAG

@OptIn(ExperimentalMaterialApi::class)
@Composable
fun HomeScreenCompFragment1(
    launchStateEvent: (HomeScreenStateEvent) -> Unit = {},

    activity: Activity? = null,
    permissionHandlingData: PermissionHandlingData = PermissionHandlingData(),

    stateEventTracker: StateEventTracker,

    deviceLocation: DeviceLocation?,
    showProfileStatusBar: Boolean,
    finishVerification: () -> Unit,
    initialSearchQuery: String = "",
    entities: List<SportsGame>?,
    sportsNewsMessages: List<SportsNewsMessage>?,
    onSearchQueryUpdate: (String) -> Unit,
    onListItemClick: (SportsGame) -> Unit,
    onNewsClick: (SportsNewsMessage) -> Unit,
    searchFilters: HomeScreenCard1SearchFilters,
    onSearchFiltersUpdated: (HomeScreenCard1SearchFilters) -> Unit,

    onSwipeLeft: () -> Unit = {},
    onSwipeRight: () -> Unit = {},
    onSwipeUp: () -> Unit = {},
    onSwipeDown: () -> Unit = {},
    onSwipeRightFromComposableSide: () -> Unit = {},
    onSwipeLeftFromComposableSide: () -> Unit = {},
    onSwipeDownFromComposableSide: () -> Unit = {},
    onSwipeUpFromComposableSide: () -> Unit = {},

    floatingActionButtonDrawableId: Int? = null,
    floatingActionButtonOnClick: (() -> Unit)? = null,
    floatingActionButtonContentDescription: String? = null,
    snackBarState: SnackBarState?,
    toastState: ToastState?,
    dialogState: DialogState?,
    progressIndicatorState: ProgressIndicatorState?,
    bottomSheetActions: HomeScreenComposableFragment1BottomSheetActions,

    isPreview: Boolean = false,
) {
    if(LOG_ME) ALog.d(TAG, "(): Recomposition start.")

    val scope = rememberCoroutineScope()
    val sheetState = rememberBottomSheetState(
        initialValue = BottomSheetValue.Collapsed,
//        initialValue = BottomSheetValue.Expanded,
        animationSpec = spring(dampingRatio = Spring.DampingRatioHighBouncy)
    )
    val launchInitStateEvent: () -> Unit = {
        if(activity != null) {
            if(LOG_ME)ALog.d(TAG, "launchInitStateEvent(): " +
                    "activity != null, launching state event in view model.")
            val path = activity.filesDir
            launchStateEvent(
                HomeScreenStateEvent.GetSportsGamesAroundUserStateEvent(path)
            )
        } else {
            if(LOG_ME)ALog.w(TAG, "launchInitStateEvent(): " +
                    "activity == null, couldn't set initial state event!")
        }
    }

    val permissionsRequiredInFragment = mutableSetOf(
        Manifest.permission.ACCESS_COARSE_LOCATION,
        Manifest.permission.ACCESS_FINE_LOCATION,
    )

    if(LOG_ME)ALog.d(
        TAG_PERMISSION_ASKING+TAG, "(): " +
                            "permissionsRequiredInFragment = ${permissionsRequiredInFragment.joinToString()}")
    SwipeableFragmentWithBottomSheetAndFAB(
        launchInitStateEvent = launchInitStateEvent,
        activity = activity,
        permissionHandlingData = permissionHandlingData,
        permissionsRequiredInFragment = permissionsRequiredInFragment,
        backgroundDrawableId = R.drawable.default_background,
        onSwipeLeft = onSwipeLeft,
        onSwipeRight = onSwipeRight,
        onSwipeUp = onSwipeUp,
        onSwipeDown = onSwipeDown,
        onSwipeRightFromComposableSide = onSwipeRightFromComposableSide,
        onSwipeLeftFromComposableSide = onSwipeLeftFromComposableSide,
        onSwipeDownFromComposableSide = onSwipeDownFromComposableSide,
        onSwipeUpFromComposableSide = onSwipeUpFromComposableSide,
        leftSideThreshold = Dimens.fragmentsSwipingLeftSideThresholdFraction,
        floatingActionButtonDrawableId = floatingActionButtonDrawableId,
        floatingActionButtonOnClick = floatingActionButtonOnClick,
        floatingActionButtonContentDescription = floatingActionButtonContentDescription,
        snackBarState = snackBarState,
        toastState = toastState,
        dialogState = dialogState,
        progressIndicatorState = progressIndicatorState,
        sheetState = sheetState,
        sheetContent = HomeScreenCompFragment1BottomSheet(
            scope = scope,
            sheetState = sheetState,
            searchFilters = searchFilters,
            onSearchFiltersUpdated = onSearchFiltersUpdated,
            leftButtonOnClick = bottomSheetActions.leftButtonOnClick,
            rightButtonOnClick = bottomSheetActions.rightButtonOnClick,
            bottomPadding = BottomNavigationHeightPublic,
        ),
        content = {
            CoachFanAppHomeScreenContent(
                sportsGames = entities ?: ArrayList<SportsGame>(),
                sportsNewsMessages = sportsNewsMessages ?: ArrayList<SportsNewsMessage>(),
                onGameClick = onListItemClick,
                onNewsClick = onNewsClick,
                isPreview = isPreview,
            )
        },
        isPreview = isPreview,
    )
    if(LOG_ME) ALog.d(TAG, "(): Recomposition end.")
}


//========================================================================================
@Preview
@Composable
fun HomeScreenComposableFragment1Preview(
    @PreviewParameter(
        HomeScreenComposableFragment1ParamsProvider::class
    ) params: HomeScreenComposableFragment1Params
) {
    HomeScreenTheme {
        // Start Interactive Mode to actually see SnackBar
        val snackBarState = remember{
            SnackBarState(
                show = true,
                message = "Your favourite team has won!",
                actionLabel = "Show more",
                iconDrawableRes = R.drawable.ic_launcher,
                onClickAction = {},
                onDismissAction = {},
                duration = SnackbarDuration.Indefinite,
                updateSnackBarInViewModel = {},
            )
        }
        snackBarState.onDismissAction = { snackBarState.show = false }
        snackBarState.onClickAction = { snackBarState.show = false }

        val toastState = ToastState(
            show = true,
            message = "Your favourite team has won!",
            duration = Toast.LENGTH_LONG,
            updateToastInViewModel = {},
        )

        val dialogState = DialogState(
            show = false,
            onDismissRequest = {},
            title = "Your favourite team has won",
            text = "One of the teams you follow has scored a major victory. Click for more info!",
            leftButtonText = "Dismiss",
            leftButtonOnClick = {},
            rightButtonText = "Confirm",
            rightButtonOnClick = {},
            updateDialogInViewModel = {},
        )

        val progressIndicatorState = ProgressIndicatorState(
            show = false,
            progress = 0.7f,
            type = ProgressIndicatorType.DeterminateCircularProgressIndicator,
            updateProgressIndicatorStateInViewModel = {},
        )

        val bottomSheetActions = HomeScreenComposableFragment1BottomSheetActions(
            leftButtonOnClick = {},
            rightButtonOnClick = {},
        )

        HomeScreenCompFragment1(
            stateEventTracker = StateEventTracker(),
            deviceLocation = params.deviceLocation,
            showProfileStatusBar = params.showProfileStatusBar,
            finishVerification = params.finishVerification,
            initialSearchQuery = params.initialSearchQuery,
            entities = params.entities,
            sportsNewsMessages = params.sportsNewsMessages,
            onSearchQueryUpdate = params.onSearchQueryUpdate,
            onListItemClick = params.onListItemClick,
            onNewsClick = params.onNewsClick,
            searchFilters = params.searchFilters,
            onSearchFiltersUpdated = params.onSearchFiltersUpdated,
            onSwipeLeft = params.onSwipeLeft,
            onSwipeRight = params.onSwipeRight,
            onSwipeUp = params.onSwipeUp,
            onSwipeDown = params.onSwipeDown,
            floatingActionButtonDrawableId = params.floatingActionButtonDrawableId,
            floatingActionButtonOnClick = params.floatingActionButtonOnClick,
            floatingActionButtonContentDescription = params.floatingActionButtonContentDescription,
            snackBarState = snackBarState,
            toastState = toastState,
            dialogState = dialogState,
            bottomSheetActions = bottomSheetActions,
            progressIndicatorState = progressIndicatorState,
            isPreview = true,
        )
    }
}


class HomeScreenComposableFragment1ParamsProvider :
    PreviewParameterProvider<HomeScreenComposableFragment1Params> {

    override val values: Sequence<HomeScreenComposableFragment1Params>
        get() = sequenceOf(
            HomeScreenComposableFragment1Params(
                deviceLocation = DeviceLocation(
                    locationPermissionGranted = true,
                    location = Location("Lublin").apply {
                        latitude = 51.2465
                        longitude = 22.5684
                    }
                ),
                showProfileStatusBar = true,
                finishVerification = {},
                initialSearchQuery = "",
                entities = SportsGameFactory.createPreviewEntitiesList().take(4),
                sportsNewsMessages = SportsNewsMessageFactory.createPreviewEntitiesList().take(4),
                onSearchQueryUpdate = {},
                onListItemClick = {},
                onNewsClick = {},
                searchFilters = HomeScreenCard1SearchFilters(
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                ),
                onSearchFiltersUpdated = {},
                onSwipeLeft = {},
                onSwipeRight = {},
                onSwipeUp = {},
                onSwipeDown = {},
                floatingActionButtonDrawableId = null,
                floatingActionButtonOnClick = {},
                floatingActionButtonContentDescription = null,
            )
        )
}

data class HomeScreenComposableFragment1Params(
    val deviceLocation: DeviceLocation?,
    val showProfileStatusBar: Boolean,
    val finishVerification: () -> Unit,
    val initialSearchQuery: String = "",
    val entities: List<SportsGame>?,
    val sportsNewsMessages: List<SportsNewsMessage>?,
    val onSearchQueryUpdate: (String) -> Unit,
    val onListItemClick: (SportsGame) -> Unit,
    val onNewsClick: (SportsNewsMessage) -> Unit,
    val searchFilters: HomeScreenCard1SearchFilters,
    val onSearchFiltersUpdated: (HomeScreenCard1SearchFilters) -> Unit,
    val onSwipeLeft: () -> Unit = {},
    val onSwipeRight: () -> Unit = {},
    val onSwipeUp: () -> Unit = {},
    val onSwipeDown: () -> Unit = {},
    val floatingActionButtonDrawableId: Int? = null,
    val floatingActionButtonOnClick: (() -> Unit)? = null,
    val floatingActionButtonContentDescription: String? = null,
)