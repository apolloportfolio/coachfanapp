package com.aps.coachfanapp.cfahomescreen.business.data.cache.implementation

import com.aps.coachfanapp.common.util.ParcelableGeoPoint
import com.aps.coachfanapp.common.util.UniqueID
import com.aps.coachfanapp.common.util.UserUniqueID
import com.aps.coachfanapp.cfahomescreen.business.data.cache.abstraction.SportsGameCacheDataSource
import com.aps.coachfanapp.cfahomescreen.business.domain.model.entities.SportsGame
import com.aps.coachfanapp.cfahomescreen.framework.datasources.cachedatasources.room.abs.SportsGameDaoService
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class SportsGameCacheDataSourceImpl
@Inject
constructor(
    private val entityDaoService: SportsGameDaoService
): SportsGameCacheDataSource {

    override suspend fun insertOrUpdateEntity(entity: SportsGame): SportsGame? {
        return entityDaoService.insertOrUpdateEntity(entity)
    }

    override suspend fun insertEntity(entity: SportsGame): Long {
        return entityDaoService.insertEntity(entity)
    }

    override suspend fun deleteEntity(primaryKey: UniqueID?): Int {
        return entityDaoService.deleteEntity(primaryKey)
    }

    override suspend fun deleteEntities(entities: List<SportsGame>): Int {
        return entityDaoService.deleteEntities(entities)
    }

    override suspend fun updateEntity(
        id: UniqueID?,
        created_at: String?,
        updated_at: String?,

        lattitude : Double?,
        longitude: Double?,
        geoLocation: ParcelableGeoPoint?,
        firestoreGeoLocation: Double?,

        picture1URI: String?,
        description: String?,

        city: String?,

        ownerID: UserUniqueID?,

        name: String?,

        switch1: Boolean?,
        switch2: Boolean?,
        switch3: Boolean?,
        switch4: Boolean?,
        switch5: Boolean?,
        switch6: Boolean?,
        switch7: Boolean?,

        picture2URI: String?,
        date: String?,
        location: String?,
        tvChannel: String?,
        ticketPrices: String?,
        currentScore: String?,
        gameBreakdown: String?,
        team1Id: UniqueID?,
        team2Id: UniqueID?,
    ): Int {
        return entityDaoService.updateEntity(
            id,
            created_at,
            updated_at,

            lattitude,
            longitude,
            geoLocation,
            firestoreGeoLocation,

            picture1URI,
            description,
            city,
            ownerID,
            name,
            
            switch1,
            switch2,
            switch3,
            switch4,
            switch5,
            switch6,
            switch7,

            picture2URI,
            date,
            location,
            tvChannel,
            ticketPrices,
            currentScore,
            gameBreakdown,
            team1Id,
            team2Id,

            )
    }

    override suspend fun searchEntities(
        query: String,
        filterAndOrder: String,
        page: Int
    ): List<SportsGame> {
        return entityDaoService.searchEntities()
    }

    override suspend fun getAllEntities(): List<SportsGame> {
        return entityDaoService.getAllEntities()
    }

    override suspend fun getEntityById(id: UniqueID?): SportsGame? {
        return entityDaoService.getEntityById(id)
    }

    override suspend fun getNumEntities(): Int {
        return entityDaoService.getNumEntities()
    }

    override suspend fun insertEntities(entities: List<SportsGame>): LongArray{
        return entityDaoService.insertEntities(entities)
    }
}