package com.aps.coachfanapp.cfahomescreen.framework.datasources.cachedatasources.room.daos

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.aps.coachfanapp.common.util.UniqueID
import com.aps.coachfanapp.common.util.UserUniqueID
import com.aps.coachfanapp.cfahomescreen.framework.datasources.cachedatasources.room.model.SportsTeamCacheEntity
import java.util.*


@Dao
interface SportsTeamDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(entity : SportsTeamCacheEntity) : Long

    @Query("SELECT * FROM sportsteam")
    suspend fun get() : List<SportsTeamCacheEntity>
    
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insertEntities(entities: List<SportsTeamCacheEntity>): LongArray

    @Query("SELECT * FROM sportsteam WHERE id = :id")
    suspend fun getEntityById(id: UniqueID?): SportsTeamCacheEntity?

    @Query("DELETE FROM sportsteam WHERE id IN (:ids)")
    suspend fun deleteEntities(ids: List<UniqueID?>): Int

    @Query("DELETE FROM sportsteam")
    suspend fun deleteAllEntities()

    @Query("SELECT * FROM sportsteam")
    suspend fun getAllEntities(): List<SportsTeamCacheEntity>

    @Query("""
        UPDATE sportsteam 
        SET 
        created_at = :created_at,
        updated_at = :updated_at,
        picture1URI = :picture1URI,
        description = :description,
        ownerId = :ownerID
        WHERE id = :id
        """)
    suspend fun updateEntity(
        id : UniqueID?,
        created_at: String?,
        updated_at: String?,

        picture1URI: String?,

        description: String?,

        ownerID: UserUniqueID?,
    ): Int

    @Query("DELETE FROM sportsteam WHERE id = :primaryKey")
    suspend fun deleteEntity(primaryKey: UniqueID?): Int

    @Query("SELECT * FROM sportsteam")
    suspend fun searchEntities(): List<SportsTeamCacheEntity>
    
    @Query("SELECT COUNT(*) FROM sportsteam")
    suspend fun getNumEntities(): Int
}