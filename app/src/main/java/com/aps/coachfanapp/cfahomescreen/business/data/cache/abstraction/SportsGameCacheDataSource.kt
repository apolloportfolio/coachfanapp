package com.aps.coachfanapp.cfahomescreen.business.data.cache.abstraction


import com.aps.coachfanapp.common.business.data.cache.StandardCacheDataSource
import com.aps.coachfanapp.common.util.ParcelableGeoPoint
import com.aps.coachfanapp.common.util.UniqueID
import com.aps.coachfanapp.common.util.UserUniqueID
import com.aps.coachfanapp.cfahomescreen.business.domain.model.entities.SportsGame

interface SportsGameCacheDataSource: StandardCacheDataSource<SportsGame> {
    override suspend fun insertEntity(entity: SportsGame): Long

    override suspend fun deleteEntity(primaryKey: UniqueID?): Int

    override suspend fun deleteEntities(entities: List<SportsGame>): Int

    suspend fun updateEntity(
        id: UniqueID?,
        created_at: String?,
        updated_at: String?,

        lattitude : Double?,
        longitude: Double?,
        geoLocation: ParcelableGeoPoint?,
        firestoreGeoLocation: Double?,

        picture1URI: String?,
        description: String?,

        city: String?,

        ownerID: UserUniqueID?,

        name: String?,

        switch1: Boolean?,
        switch2: Boolean?,
        switch3: Boolean?,
        switch4: Boolean?,
        switch5: Boolean?,
        switch6: Boolean?,
        switch7: Boolean?,

        picture2URI: String?,
        date: String?,
        location: String?,
        tvChannel: String?,
        ticketPrices: String?,
        currentScore: String?,
        gameBreakdown: String?,
        team1Id: UniqueID?,
        team2Id: UniqueID?,
    ): Int

    override suspend fun searchEntities(
        query: String,
        filterAndOrder: String,
        page: Int
    ): List<SportsGame>

    override suspend fun getAllEntities(): List<SportsGame>

    override suspend fun getEntityById(id: UniqueID?): SportsGame?

    override suspend fun getNumEntities(): Int

    override suspend fun insertEntities(entities: List<SportsGame>): LongArray
}