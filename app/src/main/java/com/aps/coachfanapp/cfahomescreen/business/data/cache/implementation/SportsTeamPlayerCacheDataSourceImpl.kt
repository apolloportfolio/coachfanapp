package com.aps.coachfanapp.cfahomescreen.business.data.cache.implementation


import com.aps.coachfanapp.common.util.UniqueID
import com.aps.coachfanapp.common.util.UserUniqueID
import com.aps.coachfanapp.cfahomescreen.business.data.cache.abstraction.SportsTeamPlayerCacheDataSource
import com.aps.coachfanapp.cfahomescreen.business.domain.model.entities.SportsTeamPlayer
import com.aps.coachfanapp.cfahomescreen.framework.datasources.cachedatasources.room.abs.SportsTeamPlayerDaoService
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class SportsTeamPlayerCacheDataSourceImpl
@Inject
constructor(
    private val entityDaoService: SportsTeamPlayerDaoService
): SportsTeamPlayerCacheDataSource {

    override suspend fun insertOrUpdateEntity(entity: SportsTeamPlayer): SportsTeamPlayer? {
        return entityDaoService.insertOrUpdateEntity(entity)
    }

    override suspend fun insertEntity(entity: SportsTeamPlayer): Long {
        return entityDaoService.insertEntity(entity)
    }

    override suspend fun deleteEntity(primaryKey: UniqueID?): Int {
        return entityDaoService.deleteEntity(primaryKey)
    }

    override suspend fun deleteEntities(entities: List<SportsTeamPlayer>): Int {
        return entityDaoService.deleteEntities(entities)
    }

    override suspend fun updateEntity(
        id : UniqueID?,
        updated_at: String?,
        created_at: String?,

        picture1URI: String?,

        description: String?,

        ownerID: UserUniqueID?,

        playerName: String?,
    ): Int {
        return entityDaoService.updateEntity(
            id,
            updated_at,
            created_at,

            picture1URI,

            description,

            ownerID,

            playerName,
        )
    }

    override suspend fun searchEntities(
        query: String,
        filterAndOrder: String,
        page: Int
    ): List<SportsTeamPlayer> {
        return entityDaoService.searchEntities()
    }

    override suspend fun getAllEntities(): List<SportsTeamPlayer> {
        return entityDaoService.getAllEntities()
    }

    override suspend fun getEntityById(id: UniqueID?): SportsTeamPlayer? {
        return entityDaoService.getEntityById(id)
    }

    override suspend fun getNumEntities(): Int {
        return entityDaoService.getNumEntities()
    }

    override suspend fun insertEntities(entities: List<SportsTeamPlayer>): LongArray{
        return entityDaoService.insertEntities(entities)
    }
}