package com.aps.coachfanapp.cfahomescreen.framework.presentation.activity01.composables.impl.values

import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.material.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.Shape
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.aps.coachfanapp.common.framework.presentation.values.CommonTheme
import com.aps.coachfanapp.common.framework.presentation.views.BackgroundsOfLayouts
import com.aps.coachfanapp.common.framework.presentation.views.BackgroundsOfLayoutsImpl
import com.aps.coachfanapp.common.framework.presentation.views.BackgroundsWithGradientsImpl
import com.aps.coachfanapp.common.framework.presentation.views.BoxWithBackground
import com.aps.coachfanapp.common.framework.presentation.views.DrawnBackground

object BackgroundsOfLayoutsCoachFanApp: BackgroundsOfLayouts {
    @Composable
    override fun backgroundTitleType01(
        colors: List<Color>?,
        brush: Brush?,
        shape: Shape?,
        /*@FloatRange(from = 0.0, to = 1.0)*/
        alpha: Float?,
    ): DrawnBackground {
        val defaultColors = listOf(
            MaterialTheme.colors.surface,
            MaterialTheme.colors.primary,
            MaterialTheme.colors.primaryVariant,
        )
        return BackgroundsWithGradientsImpl.backgroundRadiantGradientFromCenterToOutside(
            colors = colors ?: defaultColors,
            radius = null,
            shape = shape,
            alpha = alpha,
        )
    }
    @Composable
    override fun backgroundListItemType01(
        colors: List<Color>?,
        brush: Brush?,
        shape: Shape?,
        /*@FloatRange(from = 0.0, to = 1.0)*/
        alpha: Float?,
    ): DrawnBackground {
        val defaultColors = listOf(
            MaterialTheme.colors.primary,
            MaterialTheme.colors.surface,
        )
        return BackgroundsWithGradientsImpl.backgroundVerticalGradient(
            colors = colors ?: defaultColors,
            radius = null,
            shape = shape,
            alpha = alpha,
        )
    }

    @Composable
    override fun backgroundScreen01(
        colors: List<Color>?,
        brush: Brush?,
        shape: Shape?,
        /*@FloatRange(from = 0.0, to = 1.0)*/
        alpha: Float?,
    ): DrawnBackground {
        val defaultColors = listOf(
            MaterialTheme.colors.primary,
            MaterialTheme.colors.surface,
        )
        return BackgroundsWithGradientsImpl.backgroundRadiantGradientTopLeftToBottomRight(
            colors = colors ?: defaultColors,
            radius = null,
            shape = shape,
            alpha = alpha,
        )
    }
}
@Preview
@Composable
private fun BackgroundTitle01Preview() {
    CommonTheme {
        val backgrounds: BackgroundsOfLayouts = BackgroundsOfLayoutsImpl
        BoxWithBackground(
            modifier = Modifier
                .fillMaxWidth()
                .height(56.dp),
            composableBackground = backgrounds.backgroundTitleType01(null, null, null, null)
        ) {}
    }
}
@Preview
@Composable
private fun BackgroundListItemType01Preview() {
    CommonTheme {
        val backgrounds: BackgroundsOfLayouts = BackgroundsOfLayoutsImpl
        BoxWithBackground(
            modifier = Modifier
                .fillMaxWidth()
                .height(56.dp),
            composableBackground = backgrounds.backgroundListItemType01(null, null, null, null)
        ) {}
    }
}
@Preview
@Composable
private fun BackgroundScreen01Preview() {
    CommonTheme {
        val backgrounds: BackgroundsOfLayouts = BackgroundsOfLayoutsImpl
        BoxWithBackground(
            modifier = Modifier
                .fillMaxWidth()
                .height(56.dp),
            composableBackground = backgrounds.backgroundScreen01(null, null, null, null)
        ) {}
    }
}