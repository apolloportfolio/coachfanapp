package com.aps.coachfanapp.cfahomescreen.framework.datasources.networkdatasources.firebase.impl

import com.aps.coachfanapp.common.util.DateUtil
import com.aps.coachfanapp.common.util.UniqueID
import com.aps.coachfanapp.core.business.data.network.abstraction.UserNetworkDataSource
import com.aps.coachfanapp.core.framework.datasources.networkdatasources.firebase.implementation.BasicFirestoreServiceImpl
import com.aps.coachfanapp.cfahomescreen.business.domain.model.entities.SportsNewsMessage
import com.aps.coachfanapp.cfahomescreen.framework.datasources.networkdatasources.firebase.abs.SportsNewsMessageFirestoreService
import com.aps.coachfanapp.cfahomescreen.framework.datasources.networkdatasources.firebase.mappers.SportsNewsMessageFirestoreMapper
import com.aps.coachfanapp.cfahomescreen.framework.datasources.networkdatasources.firebase.model.SportsNewsMessageFirestoreEntity
import javax.inject.Inject
import javax.inject.Singleton


@Singleton
class SportsNewsMessageFirestoreServiceImpl
@Inject
constructor(
//    private val firebaseAuth: FirebaseAuth,
//    private val firestore: FirebaseFirestore,
    private val networkMapper: SportsNewsMessageFirestoreMapper,
    private val dateUtil: DateUtil,
    private val userNetworkDataSource: UserNetworkDataSource,
): SportsNewsMessageFirestoreService,
    BasicFirestoreServiceImpl<SportsNewsMessage, SportsNewsMessageFirestoreEntity, SportsNewsMessageFirestoreMapper>(
//        firebaseAuth,
//        firestore,
        networkMapper,
        dateUtil
    ) {

    override fun getCollectionName(): String {
        return COLLECTION_NAME
    }

    override fun getDeletesCollectionName(): String {
        return DELETES_COLLECTION_NAME
    }

    override fun setEntityID(entity: SportsNewsMessage, id: UniqueID) {
        entity.id = id
    }

    override fun getEntityID(entity: SportsNewsMessage): UniqueID? {
        return entity.id
    }

    override fun setFirestoreEntityID(entity: SportsNewsMessageFirestoreEntity, id: UniqueID) {
        entity.id = id
    }

    override fun updateEntityTimestamp(entity: SportsNewsMessage, timestamp: String){
        entity.updated_at = timestamp
    }

    override fun updateFirestoreEntityTimestamp(entity: SportsNewsMessageFirestoreEntity, timestamp: String){
        entity.updated_at = timestamp
    }

    override fun getFirestoreEntityType(): Class<SportsNewsMessageFirestoreEntity> {
        return SportsNewsMessageFirestoreEntity::class.java
    }

    override fun getFirestoreEntityID(entity: SportsNewsMessageFirestoreEntity): UniqueID? {
        return entity.id
    }

    companion object {
        const val TAG = "Entity4FirestoreServiceImpl"
        const val LOG_ME = true
        const val COLLECTION_NAME = "entity4"
        const val DELETES_COLLECTION_NAME = "entity4_d"
    }
}