package com.aps.coachfanapp.cfahomescreen.framework.datasources.networkdatasources.firebase.abs

import com.aps.coachfanapp.common.util.UserUniqueID
import com.aps.coachfanapp.core.framework.datasources.networkdatasources.firebase.abstraction.BasicFirestoreService
import com.aps.coachfanapp.cfahomescreen.business.domain.model.entities.SportsTeamPlayer
import com.aps.coachfanapp.cfahomescreen.framework.datasources.networkdatasources.firebase.mappers.SportsTeamPlayerFirestoreMapper
import com.aps.coachfanapp.cfahomescreen.framework.datasources.networkdatasources.firebase.model.SportsTeamPlayerFirestoreEntity

interface SportsTeamPlayerFirestoreService: BasicFirestoreService<
        SportsTeamPlayer,
        SportsTeamPlayerFirestoreEntity,
        SportsTeamPlayerFirestoreMapper
        > {
    suspend fun getUsersSportsTeamPlayers(userId: UserUniqueID): List<SportsTeamPlayer>?

}