package com.aps.coachfanapp.cfahomescreen.business.interactors.abs

import com.aps.coachfanapp.common.business.domain.state.DataState
import com.aps.coachfanapp.common.business.domain.state.StateEvent
import com.aps.coachfanapp.core.business.domain.model.currencies.Currency
import com.aps.coachfanapp.core.business.domain.model.currencies.ExchangeRates
import com.aps.coachfanapp.core.business.domain.model.entities.ProjectUser
import com.aps.coachfanapp.cfahomescreen.framework.presentation.activity01.state.HomeScreenViewState
import kotlinx.coroutines.flow.Flow

interface DownloadExchangeRates {
    fun downloadExchangeRates(
        baseCurrency: Currency,
        stateEvent : StateEvent,
        onErrorAction: () -> Unit = {},
        returnViewState : HomeScreenViewState<ProjectUser>,
        updateReturnViewState : (
            HomeScreenViewState<ProjectUser>,
            ExchangeRates?,
        ) -> (HomeScreenViewState<ProjectUser>),
    ): Flow<DataState<HomeScreenViewState<ProjectUser>>?>
}