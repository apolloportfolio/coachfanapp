package com.aps.coachfanapp.cfahomescreen.business.interactors.abs

import android.location.Location
import com.aps.coachfanapp.cfahomescreen.business.domain.model.entities.SportsGame
import com.aps.coachfanapp.cfahomescreen.framework.presentation.activity01.state.HomeScreenViewState
import com.aps.coachfanapp.common.business.domain.state.DataState
import com.aps.coachfanapp.common.business.domain.state.StateEvent
import com.aps.coachfanapp.core.business.domain.model.entities.ProjectUser
import kotlinx.coroutines.flow.Flow

interface GetSportsGamesAroundUser {
    fun getSportsGamesAroundUser(
        location : Location?,
        stateEvent: StateEvent,
        returnViewState: HomeScreenViewState<ProjectUser>,
        updateReturnViewState: (HomeScreenViewState<ProjectUser>, List<SportsGame>?)-> HomeScreenViewState<ProjectUser>,
        onErrorAction: () -> Unit = {},
    ): Flow<DataState<HomeScreenViewState<ProjectUser>>?>
}