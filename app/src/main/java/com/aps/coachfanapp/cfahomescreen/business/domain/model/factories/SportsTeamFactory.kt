package com.aps.coachfanapp.cfahomescreen.business.domain.model.factories

import com.aps.coachfanapp.common.util.DateUtil
import com.aps.coachfanapp.common.util.UniqueID
import com.aps.coachfanapp.common.util.UserUniqueID
import com.aps.coachfanapp.cfahomescreen.business.domain.model.entities.SportsTeam
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class SportsTeamFactory
@Inject
constructor(
    private val dateUtil: DateUtil
) {
    fun createSingleEntity(
        id: UniqueID?,

        picture1URI: String?,
        description : String?,

        ownerID: UserUniqueID?,

        name: String?,
    ): SportsTeam {
        return SportsTeam(
            id,
            dateUtil.getCurrentTimestamp(),
            dateUtil.getCurrentTimestamp(),

            picture1URI,
            description,

            ownerID,

            name = name,
        )
    }

    fun createEntitiesList(numEntities: Int): List<SportsTeam> {
        val list: ArrayList<SportsTeam> = ArrayList()
        for(i in 0 until numEntities){ // exclusive on upper bound
            list.add(
                createSingleEntity(
                    UniqueID.generateFakeRandomID(),
                    null,
                    null,
                    null,
                    null,
                )
            )
        }
        return list
    }


    fun generateEmpty(): SportsTeam {
        return createSingleEntity(
            UniqueID.generateFakeRandomID(),
            null,
            null,
            null,
            null,
        )
    }

    companion object {
        fun createPreviewEntitiesList(): List<SportsTeam> {
            return arrayListOf(
                SportsTeam(
                    id = UniqueID("test01"),
                    created_at = "2019-04-14 07:05:11 AM",
                    updated_at = "2019-04-14 08:41:22 AM",
                    picture1URI = "1",
                    description = "Titans FC, known for their indomitable spirit and fierce determination, are a force to be reckoned with on the field. With a history of triumphs and legendary battles, they inspire awe and respect in equal measure.",
                    ownerID = UserUniqueID("666", "666"),
                    name = "Titans FC",
                ),
                SportsTeam(
                    id = UniqueID("test02"),
                    created_at = "2019-04-14 07:05:11 AM",
                    updated_at = "2019-04-14 08:41:22 AM",
                    picture1URI = "1",
                    description = "Dynamo United is a team fueled by passion and relentless energy. Their dynamic style of play electrifies the crowd and leaves opponents scrambling to keep up. With a roster of top talent, they are a formidable presence on the field.",
                    ownerID = UserUniqueID("666", "666"),
                    name = "Dynamo United",
                ),
                SportsTeam(
                    id = UniqueID("test03"),
                    created_at = "2019-04-14 07:05:11 AM",
                    updated_at = "2019-04-14 08:41:22 AM",
                    picture1URI = "1",
                    description = "Phoenix FC rises from the ashes of every defeat, stronger and more determined than before. With a burning desire for victory and a never-say-die attitude, they light up the field with their fiery performances.",
                    ownerID = UserUniqueID("666", "666"),
                    name = "Phoenix FC",
                ),
                SportsTeam(
                    id = UniqueID("test04"),
                    created_at = "2019-04-14 07:05:11 AM",
                    updated_at = "2019-04-14 08:41:22 AM",
                    picture1URI = "1",
                    description = "Thunder Strikers unleash a relentless storm of goals, thundering across the field with lightning-fast attacks. Their explosive style of play leaves opponents shaken and spectators in awe.",
                    ownerID = UserUniqueID("666", "666"),
                    name = "Thunder Strikers",
                ),
                SportsTeam(
                    id = UniqueID("test05"),
                    created_at = "2019-04-14 07:05:11 AM",
                    updated_at = "2019-04-14 08:41:22 AM",
                    picture1URI = "1",
                    description = "Avalanche FC overwhelms their opponents with an unstoppable force, sweeping aside all obstacles in their path. With a blend of power and precision, they bury the competition under an avalanche of goals.",
                    ownerID = UserUniqueID("666", "666"),
                    name = "Avalanche FC",
                ),
                SportsTeam(
                    id = UniqueID("test06"),
                    created_at = "2019-04-14 07:05:11 AM",
                    updated_at = "2019-04-14 08:41:22 AM",
                    picture1URI = "1",
                    description = "Blaze FC ignites the pitch with their scorching pace and fiery determination. Fueled by passion and driven by ambition, they blaze a trail of glory wherever they go.",
                    ownerID = UserUniqueID("666", "666"),
                    name = "Blaze FC",
                ),
                SportsTeam(
                    id = UniqueID("test07"),
                    created_at = "2019-04-14 07:05:11 AM",
                    updated_at = "2019-04-14 08:41:22 AM",
                    picture1URI = "1",
                    description = "Galaxy United showcases a stellar performance every time they step onto the field. With their out-of-this-world talent and cosmic teamwork, they dominate the galaxy of football.",
                    ownerID = UserUniqueID("666", "666"),
                    name = "Galaxy United",
                ),
                SportsTeam(
                    id = UniqueID("test08"),
                    created_at = "2019-04-14 07:05:11 AM",
                    updated_at = "2019-04-14 08:41:22 AM",
                    picture1URI = "1",
                    description = "Rampage Rovers rampage through their opponents' defenses with unbridled ferocity. Their aggressive style of play leaves a trail of destruction in its wake, earning them a fearsome reputation.",
                    ownerID = UserUniqueID("666", "666"),
                    name = "Rampage Rovers",
                ),
                SportsTeam(
                    id = UniqueID("test09"),
                    created_at = "2019-04-14 07:05:11 AM",
                    updated_at = "2019-04-14 08:41:22 AM",
                    picture1URI = "1",
                    description = "Hurricane FC sweeps across the field with unstoppable force, leaving opponents in their wake. With their whirlwind attack and relentless pressure, they leave a path of destruction on their way to victory.",
                    ownerID = UserUniqueID("666", "666"),
                    name = "Hurricane FC",
                ),
                SportsTeam(
                    id = UniqueID("test10"),
                    created_at = "2019-04-14 07:05:11 AM",
                    updated_at = "2019-04-14 08:41:22 AM",
                    picture1URI = "1",
                    description = "Wildfire United blazes a trail of glory with their explosive style of play. With their fiery passion and relentless determination, they ignite the pitch and leave opponents in ashes.",
                    ownerID = UserUniqueID("666", "666"),
                    name = "Wildfire United",
                )
            )
        }
    }
}