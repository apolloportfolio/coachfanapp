package com.aps.coachfanapp.cfahomescreen.business.interactors.impl

import com.aps.coachfanapp.R
import com.aps.coachfanapp.common.business.data.network.ApiResponseHandler
import com.aps.coachfanapp.common.business.data.util.safeApiCall
import com.aps.coachfanapp.common.business.domain.state.*
import com.aps.coachfanapp.common.util.ALog
import com.aps.coachfanapp.core.business.data.cache.abstraction.UserCacheDataSource
import com.aps.coachfanapp.core.business.data.network.abstraction.UserNetworkDataSource
import com.aps.coachfanapp.core.business.domain.model.entities.ProjectUser
import com.aps.coachfanapp.core.business.domain.model.entities.ProjectUsersRating
import com.aps.coachfanapp.cfahomescreen.business.interactors.abs.GetUsersRating
import com.aps.coachfanapp.cfahomescreen.framework.presentation.activity01.state.HomeScreenViewState
import com.aps.coachfanapp.core.util.ApplicationContextProvider
import com.aps.coachfanapp.core.util.ConnectivityHelper
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

private const val TAG = "GetUsersRatingImpl"
private const val LOG_ME = true

class GetUsersRatingImpl
@Inject
constructor(
    private val cacheDataSource: UserCacheDataSource,
    private val networkDataSource: UserNetworkDataSource,
    private val applicationContextProvider: ApplicationContextProvider,
    private val connectivityHelper: ConnectivityHelper,
): GetUsersRating {
    override fun getUsersRating(
        stateEvent: StateEvent,
        returnViewState : HomeScreenViewState<ProjectUser>,
        updateReturnViewState :
            (HomeScreenViewState<ProjectUser>, ProjectUsersRating?) -> HomeScreenViewState<ProjectUser>,
        onErrorAction: () -> Unit,
    ): Flow<DataState<HomeScreenViewState<ProjectUser>>?> = flow {
        val methodName: String = "getUsersRating"
        if (LOG_ME) ALog.d(TAG, "Method start: $methodName")
        try {
            val usersRating = safeApiCall(
                dispatcher = Dispatchers.IO,
                onErrorAction = onErrorAction,
                apiCall = {
                    returnViewState.mainEntity?.id?.let {
                        networkDataSource.getUsersRating(it)
                    }
                }
            )

            val response = object : ApiResponseHandler<HomeScreenViewState<ProjectUser>, ProjectUsersRating?>(
                response = usersRating,
                stateEvent = stateEvent,
                internetConnectionIsAvailable = connectivityHelper
                    .internetConnectionIsAvailable(applicationContextProvider.applicationContext(TAG)),
            ) {
                override suspend fun handleSuccess(resultObj: ProjectUsersRating?): DataState<HomeScreenViewState<ProjectUser>>? {
                    var message: String? = GetUsersRatingsImplConstants.GET_ENTITY_SUCCESS
                    var uiComponentType: UIComponentType? = UIComponentType.None()
                    ALog.d(TAG, "$methodName.handleSuccess(): ")


                    if (resultObj == null) {
                        ALog.d(TAG, "$methodName: resultObj == null")
                        message = GetUsersRatingsImplConstants.GET_ENTITY_NO_MATCHING_RESULTS
                        uiComponentType = UIComponentType.Toast()
                    } else {
                        ALog.d(TAG, "$methodName: resultObj != null")
                        updateReturnViewState(returnViewState, resultObj)
                    }
                    return DataState.data(
                        response = Response(
                            messageId = R.string.error,
                            message = message,
                            uiComponentType = uiComponentType as UIComponentType,
                            messageType = MessageType.Success()
                        ),
                        data = returnViewState,
                        stateEvent = stateEvent
                    )
                }
            }.getResult()

            emit(response)
        } catch (e: Exception) {
            ALog.e(TAG, methodName, e)
        } finally {
            if (LOG_ME) ALog.d(TAG, "Method end: $methodName")
        }
    }

    object GetUsersRatingsImplConstants{
        const val GET_ENTITY_SUCCESS = "Successfully got user's rating."
        const val GET_ENTITY_NO_MATCHING_RESULTS = "There is no user's rating in database."
    }
}