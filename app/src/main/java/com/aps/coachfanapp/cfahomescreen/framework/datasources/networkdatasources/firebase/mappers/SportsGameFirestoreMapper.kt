package com.aps.coachfanapp.cfahomescreen.framework.datasources.networkdatasources.firebase.mappers

import com.aps.coachfanapp.cfahomescreen.business.domain.model.entities.SportsGame
import com.aps.coachfanapp.cfahomescreen.framework.datasources.networkdatasources.firebase.model.SportsGameFirestoreEntity
import com.aps.coachfanapp.common.business.domain.model.EntityMapper
import com.aps.coachfanapp.common.util.ParcelableGeoPoint
import javax.inject.Inject

class SportsGameFirestoreMapper
@Inject
constructor() : EntityMapper<SportsGameFirestoreEntity, SportsGame> {
    override fun mapFromEntity(entity: SportsGameFirestoreEntity): SportsGame {
        val sportsGame = SportsGame(
            entity.id,
            entity.created_at,
            entity.updated_at,

            entity.lattitude,
            entity.longitude,
            null,
            entity.firestoreGeoLocation,

            entity.picture1URI,
            entity.description,

            entity.city,

            entity.ownerID,

            entity.name,

            entity.switch1,
            entity.switch2,
            entity.switch3,
            entity.switch4,
            entity.switch5,
            entity.switch6,
            entity.switch7,
            
            entity.picture2URI,
            entity.date,
            entity.location,
            entity.tvChannel,
            entity.ticketPrices,
            entity.currentScore,
            entity.gameBreakdown,
            entity.team1Id,
            entity.team2Id,
        )
        if(entity.geoLocation != null){
            sportsGame.geoLocation = ParcelableGeoPoint(
                entity.geoLocation!!.latitude,
                entity.geoLocation!!.longitude
            )
        }
        return sportsGame
    }

    override fun mapToEntity(domainModel: SportsGame): SportsGameFirestoreEntity {
        val entity1NetworkEntity =
        SportsGameFirestoreEntity(
            domainModel.id,
            domainModel.created_at,
            domainModel.updated_at,

            domainModel.latitude,
            domainModel.longitude,
            domainModel.geoLocation,
            domainModel.firestoreGeoLocation,

            domainModel.picture1URI,
            domainModel.description,
            
            domainModel.city,

            null,           //keywords
            
            domainModel.ownerID,

            domainModel.name,
            
            domainModel.switch1,
            domainModel.switch2,
            domainModel.switch3,
            domainModel.switch4,
            domainModel.switch5,
            domainModel.switch6,
            domainModel.switch7,

            domainModel.picture2URI,
            domainModel.date,
            domainModel.location,
            domainModel.tvChannel,
            domainModel.ticketPrices,
            domainModel.currentScore,
            domainModel.gameBreakdown,
            domainModel.team1Id,
            domainModel.team2Id,
        )
        return entity1NetworkEntity.generateKeywords()
    }

    override fun mapFromEntityList(entities : List<SportsGameFirestoreEntity>) : List<SportsGame> {
        return entities.map{mapFromEntity(it)}
    }

    override fun mapToEntityList(entities: List<SportsGame>): List<SportsGameFirestoreEntity> {
        return entities.map{mapToEntity(it)}
    }
}