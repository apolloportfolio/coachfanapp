package com.aps.coachfanapp.cfahomescreen.framework.datasources.networkdatasources.firebase.abs

import com.aps.coachfanapp.common.util.UserUniqueID
import com.aps.coachfanapp.core.framework.datasources.networkdatasources.firebase.abstraction.BasicFirestoreService
import com.aps.coachfanapp.cfahomescreen.business.domain.model.entities.SportsTeam
import com.aps.coachfanapp.cfahomescreen.framework.datasources.networkdatasources.firebase.mappers.SportsTeamFirestoreMapper
import com.aps.coachfanapp.cfahomescreen.framework.datasources.networkdatasources.firebase.model.SportsTeamFirestoreEntity

interface SportsTeamFirestoreService: BasicFirestoreService<
        SportsTeam,
        SportsTeamFirestoreEntity,
        SportsTeamFirestoreMapper
        > {
    suspend fun getUsersSportsTeams(userId: UserUniqueID): List<SportsTeam>?

}