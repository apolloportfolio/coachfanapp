package com.aps.coachfanapp.cfahomescreen.business.data.cache.abstraction


import com.aps.coachfanapp.common.business.data.cache.StandardCacheDataSource
import com.aps.coachfanapp.common.util.ParcelableGeoPoint
import com.aps.coachfanapp.common.util.UniqueID
import com.aps.coachfanapp.cfahomescreen.business.domain.model.entities.SportsNewsMessage

interface SportsNewsMessageCacheDataSource: StandardCacheDataSource<SportsNewsMessage> {
    override suspend fun insertEntity(entity: SportsNewsMessage): Long

    override suspend fun deleteEntity(primaryKey: UniqueID?): Int

    override suspend fun deleteEntities(entities: List<SportsNewsMessage>): Int

    suspend fun updateEntity(
        id: UniqueID?,
        created_at: String?,
        updated_at: String?,

        lattitude : Double?,
        longitude: Double?,
        geoLocation: ParcelableGeoPoint?,
        firestoreGeoLocation: Double?,

        picture1URI: String?,
        description: String?,

        title: String?,
        message: String?,
        sportType: String?,
        gameId: UniqueID?,
        teamId: UniqueID?,
        playerIds: String?,
    ): Int

    override suspend fun searchEntities(
        query: String,
        filterAndOrder: String,
        page: Int
    ): List<SportsNewsMessage>

    override suspend fun getAllEntities(): List<SportsNewsMessage>

    override suspend fun getEntityById(id: UniqueID?): SportsNewsMessage?

    override suspend fun getNumEntities(): Int

    override suspend fun insertEntities(entities: List<SportsNewsMessage>): LongArray
}