package com.aps.coachfanapp.cfahomescreen.framework.datasources.networkdatasources.firebase.mappers

import com.aps.coachfanapp.common.business.domain.model.EntityMapper
import com.aps.coachfanapp.cfahomescreen.business.domain.model.entities.SportsTeam
import com.aps.coachfanapp.cfahomescreen.framework.datasources.networkdatasources.firebase.model.SportsTeamFirestoreEntity
import javax.inject.Inject

class SportsTeamFirestoreMapper
@Inject
constructor() : EntityMapper<SportsTeamFirestoreEntity, SportsTeam> {
    override fun mapFromEntity(entity: SportsTeamFirestoreEntity): SportsTeam {
        return SportsTeam(
            entity.id,
            entity.created_at,
            entity.updated_at,

            entity.picture1URI,

            entity.description,

            entity.ownerID,

            entity.name,
        )
    }

    override fun mapToEntity(domainModel: SportsTeam): SportsTeamFirestoreEntity {
        return SportsTeamFirestoreEntity(
            domainModel.id,
            domainModel.created_at,
            domainModel.updated_at,
            domainModel.picture1URI,
            domainModel.description,
            domainModel.ownerID,
            domainModel.name,
        )
    }

    override fun mapFromEntityList(entities : List<SportsTeamFirestoreEntity>) : List<SportsTeam> {
        return entities.map{mapFromEntity(it)}
    }

    override fun mapToEntityList(entities: List<SportsTeam>): List<SportsTeamFirestoreEntity> {
        return entities.map{mapToEntity(it)}
    }
}