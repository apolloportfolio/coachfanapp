package com.aps.coachfanapp.cfahomescreen.framework.datasources.cachedatasources.room.mappers

import com.aps.coachfanapp.common.business.domain.model.EntityMapper
import com.aps.coachfanapp.cfahomescreen.business.domain.model.entities.SportsTeam
import com.aps.coachfanapp.cfahomescreen.framework.datasources.cachedatasources.room.model.SportsTeamCacheEntity
import javax.inject.Inject

class SportsTeamCacheMapper
@Inject
constructor() : EntityMapper<SportsTeamCacheEntity, SportsTeam> {
    override fun mapFromEntity(entity: SportsTeamCacheEntity): SportsTeam {
        return SportsTeam(
            entity.id,
            entity.created_at,
            entity.updated_at,

            entity.picture1URI,

            entity.description,

            entity.ownerID,

            entity.name,
        )
    }

    override fun mapToEntity(domainModel: SportsTeam): SportsTeamCacheEntity {
        return SportsTeamCacheEntity(
            domainModel.id!!,
            domainModel.created_at,
            domainModel.updated_at,

            domainModel.picture1URI,

            domainModel.description,

            domainModel.ownerID,

            domainModel.name,
        )
    }

    override fun mapFromEntityList(entities : List<SportsTeamCacheEntity>) : List<SportsTeam> {
        return entities.map{mapFromEntity(it)}
    }

    override fun mapToEntityList(entities: List<SportsTeam>): List<SportsTeamCacheEntity> {
        return entities.map{mapToEntity(it)}
    }
}