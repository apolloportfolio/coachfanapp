package com.aps.coachfanapp.cfahomescreen.framework.datasources.cachedatasources.room.abs


import com.aps.coachfanapp.common.util.UniqueID
import com.aps.coachfanapp.common.util.UserUniqueID
import com.aps.coachfanapp.cfahomescreen.business.domain.model.entities.SportsTeam

interface SportsTeamDaoService {
    suspend fun insertOrUpdateEntity(entity: SportsTeam): SportsTeam

    suspend fun insertEntity(entity: SportsTeam): Long

    suspend fun insertEntities(Entities: List<SportsTeam>): LongArray

    suspend fun getEntityById(id: UniqueID?): SportsTeam?

    suspend fun updateEntity(
        id : UniqueID?,
        updated_at: String?,
        created_at: String?,

        picture1URI: String?,

        description: String?,

        ownerID: UserUniqueID?,
    ): Int

    suspend fun deleteEntity(primaryKey: UniqueID?): Int

    suspend fun deleteEntities(Entities: List<SportsTeam>): Int

    suspend fun searchEntities(): List<SportsTeam>

    suspend fun getAllEntities(): List<SportsTeam>

    suspend fun getNumEntities(): Int
}