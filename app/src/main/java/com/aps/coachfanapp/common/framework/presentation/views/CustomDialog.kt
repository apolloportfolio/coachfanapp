package com.aps.coachfanapp.common.framework.presentation.views

import android.os.Parcelable
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.material.AlertDialog
import androidx.compose.material.Button
import androidx.compose.material.Card
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.dimensionResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.tooling.preview.PreviewParameter
import androidx.compose.ui.unit.dp
import androidx.constraintlayout.compose.ConstraintLayout
import com.aps.coachfanapp.R
import com.aps.coachfanapp.common.framework.presentation.values.CommonTheme
import com.aps.coachfanapp.common.util.ALog
import kotlinx.android.parcel.Parcelize
import java.io.Serializable
import com.aps.coachfanapp.common.framework.presentation.values.*

private const val TAG = "CustomDialog"
private const val LOG_ME = true

// References:
// https://stackoverflow.com/questions/68852110/show-custom-alert-dialog-in-jetpack-compose

@Composable
fun CustomDialog(
    dialogState: DialogState?,
    onDialogDismiss: () -> Unit,
    composableBackground: DrawnBackground? = null,
    backgroundDrawableId: Int = R.drawable.list_item_example_background_1,
    topBannerBackgroundImageID: Int? = null,
    topBannerBackgroundImageContentDescription: String = "",
    leftButtonTextID: Int? = R.string.dismiss,
    rightButtonTextID: Int = R.string.accept,
) {
    if(dialogState == null) {
        if(LOG_ME)ALog.d(TAG, ": " +
                                "dialogState == null")
    } else {
        if(LOG_ME)ALog.d(TAG, ": " +
                                "Showing Dialog")
        val startMargin = remember { 4 }.dp
        val endMargin = remember { 4 }.dp
        val topMargin = dimensionResource(id = R.dimen.list_item_vertical_margin)
        val bottomMargin = dimensionResource(id = R.dimen.list_item_vertical_margin)

        Card(
            modifier = Modifier
                .fillMaxWidth()
                .padding(
                    start = dimensionResource(id = R.dimen.list_tile_horizontal_margin),
                    end = dimensionResource(id = R.dimen.list_tile_horizontal_margin),
                    top = dimensionResource(id = R.dimen.list_tile_vertical_margin),
                    bottom = dimensionResource(id = R.dimen.list_tile_vertical_margin)
                )
                .background(ColorTransparent),
        ) {
            BoxWithBackground(
                backgroundDrawableId = backgroundDrawableId,
                composableBackground = composableBackground,
            ) {

                ConstraintLayout(
                    modifier = Modifier
                        .fillMaxWidth()
                        .wrapContentHeight()
                        .padding(
                            start = startMargin,
                            end = endMargin,
                            top = topMargin,
                            bottom = bottomMargin,
                        )
                ) {
                    val (topBanner, title, message, leftButton, rightButton,
                    ) = createRefs()

                    if(topBannerBackgroundImageID != null) {
                        Image(
                            modifier = Modifier
                                .constrainAs(topBanner) {
                                    start.linkTo(parent.start)
                                    end.linkTo(parent.end)
                                    top.linkTo(parent.top)
                                }
                                .height(200.dp)
                                .background(ColorTransparent)
                                .padding(
                                    start = startMargin,
                                    end = endMargin,
                                    top = topMargin,
                                    bottom = bottomMargin
                                )
                                .fillMaxWidth(),
                            contentDescription = topBannerBackgroundImageContentDescription,
                            painter = painterResource(id = topBannerBackgroundImageID),
                            contentScale = ContentScale.Fit
                        )
                    }


                    Text(
                        modifier = Modifier
                            .constrainAs(title) {
                                top.linkTo(topBanner.bottom)
                                start.linkTo(parent.start)
                                end.linkTo(parent.end)
                            }
                            .fillMaxWidth()
                            .padding(
                                start = startMargin,
                                end = endMargin,
                                top = topMargin,
                                bottom = bottomMargin,
                            ),
                        text = dialogState.title,
                        maxLines = 1,
                        textAlign = TextAlign.Center,
                        style = TextStyle(
                            color = MaterialTheme.colors.onSurface,
                            fontWeight = FontWeight.Bold
                        ),
                        overflow = TextOverflow.Ellipsis,
                    )

                    Text(
                        modifier = Modifier
                            .constrainAs(message) {
                                top.linkTo(title.bottom)
                                start.linkTo(parent.start)
                                end.linkTo(parent.end)
                            }
                            .fillMaxWidth()
                            .padding(
                                start = startMargin,
                                end = endMargin,
                                top = topMargin,
                                bottom = bottomMargin,
                            ),
                        text = dialogState.text,
                        textAlign = TextAlign.Center,
                        style = TextStyle(
                            color = MaterialTheme.colors.onSurface,
                            fontWeight = FontWeight.Normal
                        ),
                        overflow = TextOverflow.Ellipsis,
                    )

                    if(leftButtonTextID != null) {
                        Button(
                            modifier = Modifier
                                .constrainAs(leftButton) {
                                    top.linkTo(message.bottom)
                                    start.linkTo(parent.start)
                                    end.linkTo(rightButton.start)
                                }
                                .fillMaxWidth(0.4f)
                                .padding(
                                    start = startMargin,
                                    end = endMargin,
                                    top = topMargin,
                                    bottom = bottomMargin,
                                ),
                            onClick = {
                                dialogState.leftButtonOnClick()
                                onDialogDismiss()
                            },
                        ) {
                            Text(stringResource(id = leftButtonTextID))
                        }
                    }                    

                    Button(
                        modifier = Modifier
                            .constrainAs(rightButton) {
                                top.linkTo(message.bottom)
                                start.linkTo(leftButton.end)
                                end.linkTo(parent.end)
                            }
                            .fillMaxWidth(0.4f)
                            .padding(
                                start = startMargin,
                                end = endMargin,
                                top = topMargin,
                                bottom = bottomMargin,
                            ),
                        onClick = {
                            dialogState.rightButtonOnClick()
                            onDialogDismiss()
                        },
                    ) {
                        Text(stringResource(id = rightButtonTextID))
                    }
                }
            }
        }
    }
}

@Composable
fun ShowStandardAlertDialog(
    dialogState: DialogState?,
    onDialogDismiss: () -> Unit,
) {
    if (dialogState?.show == true) {
        var leftButton:@Composable (() -> Unit)? = {}
        if(dialogState.leftButtonText != null) {
            leftButton = {
                Button(
                    onClick = {
                        dialogState.leftButtonOnClick.invoke()
                        dialogState.onDismissRequest.invoke()
                        onDialogDismiss()
                    }
                ) {
                    Text(text = dialogState.leftButtonText ?: stringResource(id = R.string.dismiss))
                }
            }
        }
        val rightButton:@Composable (() -> Unit) = {
                Button(
                    onClick = {
                        dialogState.rightButtonOnClick.invoke()
                        dialogState.onDismissRequest.invoke()
                        onDialogDismiss()
                    }
                ) {
                    Text(text = dialogState.rightButtonText)
                }
            }

        AlertDialog(
            onDismissRequest = {
                dialogState.onDismissRequest.invoke()
                onDialogDismiss()
            },
            title = {
                Text(text = dialogState.title)
            },
            text = {
                Text(text = dialogState.text)
            },
            confirmButton = rightButton,
            dismissButton = leftButton
        )
    }
}

@Parcelize
data class DialogState(
    var show: Boolean = false,
    var onDismissRequest: () -> Unit = {},
    var title: String = "",
    var text: String = "",
    var leftButtonText: String? = "",         //dismissButton
    var leftButtonOnClick: () -> Unit = {},
    var rightButtonText: String = "",         //confirmButton
    var rightButtonOnClick: () -> Unit = {},
    var updateDialogInViewModel: (DialogState?) -> Unit = {},
) : Parcelable, Serializable {

    override fun toString(): String {
        return buildString {
            appendLine("DialogState:")
            appendLine("show: $show")
            appendLine("onDismissRequest: Function<Unit>")
            appendLine("title: $title")
            appendLine("text: $text")
            appendLine("leftButtonText: $leftButtonText")
            appendLine("leftButtonOnClick: Function<Unit>")
            appendLine("rightButtonText: $rightButtonText")
            appendLine("rightButtonOnClick: Function<Unit>")
            appendLine("updateDialogInViewModel: Function<DialogState?>")
        }
    }
}

// Preview =========================================================================================

@Preview(
    showSystemUi = true,
)
@Composable
fun CustomDialogPreview(
    @PreviewParameter(SwipeableFragmentWithBottomSheetAndFABParamsProvider::class) params: SwipeableFragmentWithBottomSheetAndFABParams
) {
    CommonTheme {
        val dialogState = DialogState(
            show = true,
            onDismissRequest = {
                if(LOG_ME)ALog.d(TAG, "CustomDialogPreview(): " +
                        "onDismissRequest launched")
            },
            title = "Preview Dialog Title",
            text = "This is a preview of a CustomDialog.",
            leftButtonText = "Dismiss",
            leftButtonOnClick = {
                if(LOG_ME) ALog.d(TAG, "CustomDialogPreview(): " +
                                        "Left button clicked")
            },
            rightButtonText = "Accept",
            rightButtonOnClick = {
                if(LOG_ME)ALog.d(TAG, "CustomDialogPreview(): " +
                                        "Right button clicked.")
            },
            updateDialogInViewModel = {
                if(LOG_ME)ALog.d(TAG, "CustomDialogPreview(): " +
                                        "updateDialogInViewModel launched")
            }
        )

        val onDialogDismiss = {
            if(LOG_ME)ALog.d(TAG, "CustomDialogPreview(): " +
                    "onDialogDismiss launched")
        }

        CustomDialog(
            dialogState = dialogState,
            onDialogDismiss = onDialogDismiss,
            backgroundDrawableId = R.drawable.list_item_example_background_1,
            topBannerBackgroundImageID = R.drawable.ic_launcher,
        )
    }
}