package com.aps.coachfanapp.common.framework.presentation.views

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.material.Card
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.graphics.DefaultAlpha
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalLayoutDirection
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.res.*
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.tooling.preview.PreviewParameter
import androidx.compose.ui.tooling.preview.PreviewParameterProvider
import androidx.compose.ui.unit.dp
import androidx.constraintlayout.compose.ConstraintLayout
import androidx.constraintlayout.compose.Dimension
import com.aps.coachfanapp.common.framework.presentation.values.CommonTheme
import com.aps.coachfanapp.common.util.ALog
import com.aps.coachfanapp.common.util.composableutils.fallbackPainterResource
import com.google.firebase.storage.StorageReference
import com.aps.coachfanapp.R

private const val TAG = "ListItemType06"
private const val LOG_ME = true

@Composable
fun ListItemType06(
    index: Int,
    itemTitleString: String?,
    itemDescription: String?,
    onListItemClick: () -> Unit,
    itemRef: StorageReference?,
    itemDrawableId: Int? = null,
    paddingValues: PaddingValues = PaddingValues(4.dp, 3.dp, 4.dp, 3.dp),
    composableBackground: DrawnBackground? = null,
    backgroundDrawableId: Int = R.drawable.list_item_example_background_1,
    imageContentDescription: String = "",
    imageTint: Color? = MaterialTheme.colors.primary,
) {
    if(LOG_ME)ALog.d(TAG, "ListItemType01(): " +
            "index == $index, itemTitleString == $itemTitleString\n")
    val transparentColor = remember {
        Color(0x00000000)
    }
    Card(
        modifier = Modifier
            .testTag(index.toString())
            .fillMaxWidth()
            .padding(paddingValues)
            .background(transparentColor)
            .clickable { onListItemClick() },
    ) {
        BoxWithBackground(
            backgroundDrawableId = backgroundDrawableId,
            composableBackground = composableBackground,
        ) {
            ConstraintLayout(
                modifier = Modifier
                    .fillMaxWidth()
                    .wrapContentHeight()
                    .padding(paddingValues)
            ) {

                val (itemThumbnail, itemTitle, lblUnderTitle,
                ) = createRefs()
                val pictureIsPresent = itemRef != null || itemDrawableId != null

                // Image to the start.
                if(itemRef != null) {
                    ImageFromFirebase(
                        itemRef,
                        modifier = Modifier
                            .constrainAs(itemThumbnail) {
                                top.linkTo(parent.top)
                                bottom.linkTo(parent.bottom)
                                start.linkTo(parent.start)
                                end.linkTo(itemTitle.start)
                            }
                            .height(56.dp)
                            .width(56.dp)
                            .background(transparentColor)
                            .padding(paddingValues),
                        contentDescription = null,
                        contentScale = ContentScale.Fit
                    )
                } else if(itemDrawableId != null) {
                    Image(
                        painter = fallbackPainterResource(id = itemDrawableId),
                        contentDescription = imageContentDescription,
                        modifier = Modifier
                            .constrainAs(itemThumbnail) {
                                top.linkTo(parent.top)
                                bottom.linkTo(parent.bottom)
                                start.linkTo(parent.start)
                                end.linkTo(itemTitle.start)
                            }
                            .height(56.dp)
                            .width(56.dp)
                            .background(transparentColor)
                            .padding(paddingValues),
                        alignment = Alignment.Center,
                        contentScale = ContentScale.Fit,
                        alpha = DefaultAlpha,
                        colorFilter = if(imageTint != null) { ColorFilter.tint(imageTint) } else {null},
                    )
                }


                // Item's title
                Text(
                    modifier = Modifier
                        .constrainAs(itemTitle) {
                            top.linkTo(parent.top)
                            bottom.linkTo(lblUnderTitle.top)
                            start.linkTo(
                                if (pictureIsPresent) {
                                    itemThumbnail.end
                                } else {
                                    parent.start
                                }
                            )
                            end.linkTo(parent.end)
                            width = Dimension.fillToConstraints
                        }
                        .padding(
                            start = paddingValues.calculateStartPadding(LocalLayoutDirection.current),
                            end = dimensionResource(id = R.dimen.margin_between_text_views),
                            top = paddingValues.calculateTopPadding(),
                            bottom = paddingValues.calculateBottomPadding()
                        ),
                    text = itemTitleString ?: "",
                    maxLines = 1,
                    textAlign = TextAlign.Start,
                    style = TextStyle(
                        color = MaterialTheme.colors.onSurface,
                        fontWeight = FontWeight.ExtraBold,
                    ),
                    overflow = TextOverflow.Ellipsis,
                )

                // Item's description
                Text(
                    modifier = Modifier
                        .constrainAs(lblUnderTitle) {
                            top.linkTo(itemTitle.bottom)
                            bottom.linkTo(parent.bottom)
                            start.linkTo(
                                if (pictureIsPresent) {
                                    itemThumbnail.end
                                } else {
                                    parent.start
                                }
                            )
                            end.linkTo(parent.end)
                            width = Dimension.fillToConstraints
                        }
                        .padding(
                            start = paddingValues.calculateStartPadding(LocalLayoutDirection.current),
                            end = dimensionResource(id = R.dimen.margin_between_text_views),
                            top = paddingValues.calculateTopPadding(),
                            bottom = paddingValues.calculateBottomPadding()
                        )
                        .background(transparentColor),
                    text = itemDescription ?: "",
                    textAlign = TextAlign.Start,
                    style = TextStyle(
                        color = MaterialTheme.colors.onSurface,
                        fontWeight = FontWeight.Normal,
                    ),
                    maxLines = 2,
                    overflow = TextOverflow.Ellipsis,
                )

            }
        }
    }
}


// Preview =========================================================================================
data class ListItemType06Params(
    val itemTitleString: String?,
    val itemDescription: String?,
    val onListItemClick: () -> Unit,
    val onItemsButtonClick: () -> Unit,
    val getItemsRating: () -> Float?,
    val ratingBarColor: Color,
)

class ListItemType06ParamsProvider : PreviewParameterProvider<ListItemType06Params> {
    override val values: Sequence<ListItemType06Params> = sequenceOf(
        ListItemType06Params(
            itemTitleString = "Lorem ipsum",
            itemDescription = "Lorem ipsum dolor sit amet, consectetur adipiscing elit.\n" +
                    "Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
            onListItemClick = {},
            onItemsButtonClick = {},
            getItemsRating = { 5f },
            ratingBarColor = Color(0xFFFFCC01)
        ),
    )
}

@Preview
@Composable
fun ListItemType06Preview1(
    @PreviewParameter(
        ListItemType06ParamsProvider::class
    ) params: ListItemType06Params
) {
    CommonTheme {
        ListItemType06(
            index = 0,
            itemTitleString = params.itemTitleString,
            itemDescription = params.itemDescription,
            onListItemClick = params.onListItemClick,
            itemRef = null,
            itemDrawableId = R.drawable.ic_launcher,
            imageTint = MaterialTheme.colors.primary,
        )
    }
}

@Preview
@Composable
fun ListItemType06Preview2(
    @PreviewParameter(
        ListItemType06ParamsProvider::class
    ) params: ListItemType06Params
) {
    CommonTheme {
        ListItemType06(
            index = 0,
            itemTitleString = params.itemTitleString,
            itemDescription = params.itemDescription,
            onListItemClick = params.onListItemClick,
            itemRef = null,
            itemDrawableId = R.drawable.example_user_profile_picture_5,
            imageTint = null,
        )
    }
}