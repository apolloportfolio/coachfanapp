package com.aps.coachfanapp.common.framework.presentation.views

import androidx.compose.runtime.Composable
import androidx.compose.runtime.DisposableEffect
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import com.aps.coachfanapp.common.util.ALog

private const val TAG = "LaunchEffectsInComposableLifecycle"
private const val LOG_ME = true

@Composable
fun LaunchEffectsInComposableLifecycle(
    onFirstComposition: () -> Unit = {},
    onBroughtBackIntoView: () -> Unit = {},
    onDisposeComposable: () -> Unit = {},
) {
    var firstComposition by rememberSaveable { mutableStateOf(true) }
    var isVisible by remember { mutableStateOf(true) }

    LaunchedEffect(isVisible) {
        if(isVisible) {
            // Composable is rendered for the first time or became visible
            // after being out of view.
            if(firstComposition) {
                if(LOG_ME) ALog.d(TAG, "LaunchedEffect(isVisible): " +
                        "This is the first composition.")
                onFirstComposition()
            } else {
                if(LOG_ME)ALog.d(TAG, "LaunchedEffect(isVisible): " +
                        "Composable was brought back into view.")
                onBroughtBackIntoView()
            }
            firstComposition = false
        } else {
            if(LOG_ME)ALog.d(TAG, "LaunchedEffect(isVisible): " +
                    "Composable was disposed of.")
        }
    }

    DisposableEffect(Unit) {
        if(LOG_ME)ALog.d(TAG, "DisposableEffect(Unit) Launched.")
        onDispose {
            if(LOG_ME)ALog.d(TAG, "DisposableEffect(Unit).onDispose() Launched.")
            isVisible = false
            onDisposeComposable()
        }
    }
}