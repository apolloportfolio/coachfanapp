package com.aps.coachfanapp.common.util.calendarviewhighlight

import android.content.Context
import android.os.Build
import android.provider.Settings
import androidx.annotation.RequiresApi
import com.aps.coachfanapp.R
import com.aps.coachfanapp.common.util.DateUtil
import com.google.firebase.Timestamp
import com.prolificinteractive.materialcalendarview.CalendarDay
import org.threeten.bp.LocalDate
import java.lang.NullPointerException
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

fun LOG_ME() = true
fun TAG() = "MaterialCalendarViewExtensions"

@RequiresApi(Build.VERSION_CODES.O)
fun  com.prolificinteractive.materialcalendarview.MaterialCalendarView.getLocalDate(date: String?): LocalDate? {
    val sdf = SimpleDateFormat(Settings.System.DATE_FORMAT, Locale.ENGLISH)
    return try {
        val input: Date = sdf.parse(date)
        val cal: Calendar = Calendar.getInstance()
        cal.time = input
        LocalDate.of(
            cal.get(Calendar.YEAR),
            cal.get(Calendar.MONTH) + 1,
            cal.get(Calendar.DAY_OF_MONTH)
        )
    } catch (e: NullPointerException) {
        null
    } catch (e: ParseException) {
        null
    }
}

fun com.prolificinteractive.materialcalendarview.MaterialCalendarView.setEventFromStrings(
    context : Context,
    dateList: List<String?>
) {
    val localDateList: MutableList<LocalDate> = ArrayList()
    for (string in dateList) {
        val calendar: LocalDate? = getLocalDate(string)
        if (calendar != null) {
            localDateList.add(calendar)
        }
    }
    setEvent(context, localDateList)
}

fun com.prolificinteractive.materialcalendarview.MaterialCalendarView.setEventFromDates(
    context : Context,
    dateList: List<GregorianCalendar?>
) {
    val localDateList: MutableList<LocalDate> = ArrayList()
    for (date in dateList) {
        val calendar: org.threeten.bp.LocalDate = date!!.toThreetenLocalDate()
        localDateList.add(calendar)
    }
    setEvent(context, localDateList)
}

fun java.time.LocalDate.toThreetenLocalDate() : org.threeten.bp.LocalDate {
//    return LocalDate.of(year, month, dayOfMonth)
    return org.threeten.bp.LocalDate.of(year, month.value, dayOfMonth)
}

fun GregorianCalendar.toThreetenLocalDate() : org.threeten.bp.LocalDate {
    return org.threeten.bp.LocalDate.of(
        get(GregorianCalendar.YEAR),
        get(GregorianCalendar.MONTH)+1,
        get(GregorianCalendar.DAY_OF_MONTH),
    )
}

fun Timestamp.toThreetenLocalDate() : org.threeten.bp.LocalDate {
    val gregorianCalendar : GregorianCalendar? = DateUtil.convertTimestampToGregorianCalendar(this)
    return org.threeten.bp.LocalDate.of(
        gregorianCalendar!!.get(GregorianCalendar.YEAR),
        gregorianCalendar.get(GregorianCalendar.MONTH)+1,
        gregorianCalendar.get(GregorianCalendar.DAY_OF_MONTH),
    )
}


fun com.prolificinteractive.materialcalendarview.MaterialCalendarView.setEvent(
    context : Context,
    startDate : LocalDate,
    endDate : LocalDate,
) {
    val dateList: MutableList<LocalDate> = ArrayList()
    var currentDate : LocalDate = startDate
    while(currentDate.isBefore(endDate)) {
        dateList.add(currentDate.plusDays(0))       // plusDays() returns a copy
        currentDate.plusDays(1)
    }
    dateList.add(endDate)
    setEvent(context, dateList)
}


fun com.prolificinteractive.materialcalendarview.MaterialCalendarView.setEvent(
    context : Context,
    localDateList: List<LocalDate>
) {
    val datesLeft: MutableList<CalendarDay> = ArrayList()
    val datesCenter: MutableList<CalendarDay> = ArrayList()
    val datesRight: MutableList<CalendarDay> = ArrayList()
    val datesIndependent: MutableList<CalendarDay> = ArrayList()
    for (localDate in localDateList) {
        var right = false
        var left = false
        for (day1 in localDateList) {
            if (localDate.isEqual(day1.plusDays(1))) {
                left = true
            }
            if (day1.isEqual(localDate.plusDays(1))) {
                right = true
            }
        }
        if (left && right) {
            datesCenter.add(CalendarDay.from(localDate))
        } else if (left) {
            datesLeft.add(CalendarDay.from(localDate))
        } else if (right) {
            datesRight.add(CalendarDay.from(localDate))
        } else {
            datesIndependent.add(CalendarDay.from(localDate))
        }
    }
    setDecor(context, datesCenter, R.drawable.calendarview_decor_center)
    setDecor(context, datesLeft, R.drawable.calendarview_decor_left)
    setDecor(context, datesRight, R.drawable.calendarview_decor_right)
    setDecor(context, datesIndependent, R.drawable.calendarview_decor_independent)
}

fun com.prolificinteractive.materialcalendarview.MaterialCalendarView.setDecor(context : Context, calendarDayList: List<CalendarDay>, drawable: Int) {
    this.addDecorators(
        EventDecorator(
            context, drawable, calendarDayList
        )
    )
}