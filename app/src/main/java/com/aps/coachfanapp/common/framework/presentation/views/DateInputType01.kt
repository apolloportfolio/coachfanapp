package com.aps.coachfanapp.common.framework.presentation.views

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.DateRange
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.MaterialTheme
import androidx.compose.material.OutlinedTextField
import androidx.compose.material.TextFieldDefaults
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.input.KeyboardCapitalization
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.aps.coachfanapp.R
import com.aps.coachfanapp.common.framework.presentation.values.CommonTheme

@Composable
fun DateInputType01(
    selectedDate: String,
    onDateSelected: (String) -> Unit,
    showDatePicker: ()->Unit,
    backgroundDrawableId: Int? = null,
    composableBackground: DrawnBackground? = null,
    modifier: Modifier = Modifier,
) {
    BoxWithBackground(
        modifier = modifier,
        backgroundDrawableId = backgroundDrawableId,
        composableBackground = composableBackground,
    ) {
        var outlinedTextFieldModifier: Modifier = Modifier
            .fillMaxWidth()
            .height(56.dp)
            .clip(MaterialTheme.shapes.medium)
            .padding(PaddingValues(4.dp, 4.dp, 4.dp, 4.dp))
        if(backgroundDrawableId == null && composableBackground == null) {
            outlinedTextFieldModifier = outlinedTextFieldModifier
                .background(MaterialTheme.colors.surface)
        }

        OutlinedTextField(
            value = selectedDate,
            onValueChange = { onDateSelected(it) },
            label = { Text(stringResource(id = R.string.select_date)) },
            leadingIcon = {
                Icon(
                    imageVector = Icons.Default.DateRange,
                    contentDescription = stringResource(id = R.string.date_icon)
                )
            },
            keyboardOptions = KeyboardOptions(
                capitalization = KeyboardCapitalization.None,
                keyboardType = KeyboardType.Text
            ),
            modifier = outlinedTextFieldModifier,
            colors = TextFieldDefaults.outlinedTextFieldColors(
                cursorColor = MaterialTheme.colors.primary
            ),
            singleLine = true,
            trailingIcon = {
                IconButton(onClick = showDatePicker) {
                    Icon(
                        imageVector = Icons.Default.DateRange,
                        contentDescription = stringResource(id = R.string.date_icon)
                    )
                }
            },
            maxLines = 1
        )
    }
}

@Preview
@Composable
private fun DateInputType01Preview01() {
    CommonTheme {
        DateInputType01(
            selectedDate = "",
            onDateSelected = {},
            showDatePicker = {},
            backgroundDrawableId = null,
            composableBackground = null,
            modifier = Modifier,
        )
    }
}

@Preview
@Composable
private fun DateInputType01Preview02() {
    CommonTheme {
        DateInputType01(
            selectedDate = "",
            onDateSelected = {},
            showDatePicker = {},
            backgroundDrawableId = R.drawable.example_background_1,
            composableBackground = BackgroundsOfLayoutsImpl.backgroundTitleType01(
                colors = null,
                brush = null,
                shape = null,
                alpha = 0.6f,
            ),
            modifier = Modifier,
        )
    }
}