package com.aps.coachfanapp.common.business.interactors.implementation

import com.aps.coachfanapp.R
import com.aps.coachfanapp.common.business.data.cache.StandardCacheDataSource
import com.aps.coachfanapp.common.business.data.network.ApiResponseHandler
import com.aps.coachfanapp.common.business.data.network.StandardNetworkDataSource
import com.aps.coachfanapp.common.business.data.util.safeApiCall
import com.aps.coachfanapp.common.business.domain.state.*
import com.aps.coachfanapp.common.business.interactors.abstraction.InsertEntity
import com.aps.coachfanapp.core.util.ApplicationContextProvider
import com.aps.coachfanapp.core.util.ConnectivityHelper
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

private const val TAG = "InsertEntityImpl"
private const val LOG_ME = true

class InsertEntityImpl<
        Entity,
        CacheDataSource: StandardCacheDataSource<Entity>,
        NetworkDataSource: StandardNetworkDataSource<Entity>,
        ViewState: com.aps.coachfanapp.common.business.domain.state.ViewState<Entity>>
@Inject constructor(
    private val cacheDataSource: CacheDataSource,
    private val networkDataSource: NetworkDataSource,
    private val applicationContextProvider: ApplicationContextProvider,
    private val connectivityHelper: ConnectivityHelper,
): InsertEntity<
        Entity,
        CacheDataSource,
        NetworkDataSource,
        ViewState> 
{

    override fun insertNewEntity(
        newEntity: Entity,
        stateEvent: StateEvent,
        returnViewState: ViewState,
        updateReturnViewState: (ViewState, Entity?)->ViewState
    ):Flow<DataState<ViewState>?> = flow {
        val networkResult = safeApiCall(Dispatchers.IO){
            networkDataSource.insertOrUpdateEntity(newEntity)
        }

        val cacheResponse = object: ApiResponseHandler<ViewState, Entity?>(
            response = networkResult,
            stateEvent = stateEvent,
            internetConnectionIsAvailable = connectivityHelper
                .internetConnectionIsAvailable(applicationContextProvider.applicationContext(TAG)),
        ){
            override suspend fun handleSuccess(resultObj: Entity?): DataState<ViewState>? {
                return if(resultObj != null){
                    returnViewState.mainEntity = resultObj
                    updateReturnViewState(returnViewState, resultObj)
                    return DataState.data(
                        response = Response(
                            messageId = R.string.text_ok,
                            message = INSERT_ENTITY_SUCCESS,
                            uiComponentType = UIComponentType.Toast(),
                            messageType = MessageType.Success()
                        ),
                        data = returnViewState,
                        stateEvent = stateEvent
                    )
                }
                else{
                    return DataState.data(
                        response = Response(
                            messageId = R.string.error,
                            message = INSERT_ENTITY_FAILED,
                            uiComponentType = UIComponentType.Toast(),
                            messageType = MessageType.Error()
                        ),
                        data = null,
                        stateEvent = stateEvent
                    )
                }
            }
        }.getResult()

        emit(cacheResponse)
    }

    companion object{
        const val INSERT_ENTITY_SUCCESS = "Successfully inserted new entity."
        const val INSERT_ENTITY_FAILED = "Failed to insert new entity."
    }
}