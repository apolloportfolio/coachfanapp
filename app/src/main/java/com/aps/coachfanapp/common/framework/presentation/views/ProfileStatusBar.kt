package com.aps.coachfanapp.common.framework.presentation.views

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.dimensionResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.semantics.contentDescription
import androidx.compose.ui.semantics.semantics
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.tooling.preview.PreviewParameter
import androidx.compose.ui.tooling.preview.PreviewParameterProvider
import androidx.constraintlayout.compose.ConstraintLayout
import com.aps.coachfanapp.R
import com.aps.coachfanapp.common.framework.presentation.values.ColorTransparent
import com.aps.coachfanapp.common.framework.presentation.values.CommonTheme

@Composable
fun ProfileStatusBar(
    finishVerification: () -> Unit,
    composableBackground: DrawnBackground? = null,
    backgroundDrawableId: Int = R.drawable.list_item_example_background_1,
    titleTint: Color  = MaterialTheme.colors.secondary,
    subtitleTint: Color  = MaterialTheme.colors.onPrimary,
) {
    // Content descriptions
    val buttonContentDescription = stringResource(
            R.string.profile_status_complete_profile_button_content_description
    )

    BoxWithBackground(
        backgroundDrawableId = backgroundDrawableId,
        composableBackground = composableBackground,
    ) {

        ConstraintLayout(
            modifier = Modifier
                .fillMaxWidth()
                .wrapContentHeight()
//            .height(IntrinsicSize.Min)
//                .aspectRatio(1f)
                .background(ColorTransparent)
                .padding(
                    horizontal = dimensionResource(R.dimen.fragment_horizontal_margin),
                    vertical = dimensionResource(R.dimen.fragment_vertical_margin)
                )
        ) {
            val (
                profileStatusInfoLbl,
                profileStatusInfoMessage,
                profileStatusCompleteProfileButton,
            ) = createRefs()

            Text(
                modifier = Modifier
                    .constrainAs(profileStatusInfoLbl) {
                        top.linkTo(parent.top)
                        start.linkTo(parent.start)
                        end.linkTo(parent.end)
                    }
                    .padding(
                        horizontal = dimensionResource(R.dimen.button_horizontal_margin),
                        vertical = dimensionResource(R.dimen.button_vertical_margin)
                    ),
                text = stringResource(R.string.welcome_title),
                style = MaterialTheme.typography.h4,
                fontWeight = FontWeight.Bold,
                maxLines = 1,
                textAlign = TextAlign.Center,
                color = titleTint,
            )


            Text(
                modifier = Modifier
                    .constrainAs(profileStatusInfoMessage) {
                        top.linkTo(profileStatusInfoLbl.bottom)
                        start.linkTo(parent.start)
                        end.linkTo(parent.end)
                    }
                    .padding(
                        horizontal = dimensionResource(R.dimen.paragraph_vertical_margin_small),
                        vertical = dimensionResource(R.dimen.button_vertical_margin)
                    ),
                text = stringResource(R.string.profile_incomplete_message),
                style = MaterialTheme.typography.body1.copy(fontWeight = FontWeight.Bold),
                textAlign = TextAlign.Center,
                color = subtitleTint,
            )

            Button(
                modifier = Modifier
                    .semantics {
                        contentDescription = buttonContentDescription
                    }
                    .constrainAs(profileStatusCompleteProfileButton) {
                        top.linkTo(profileStatusInfoMessage.bottom)
                        start.linkTo(parent.start)
                        end.linkTo(parent.end)
                    }
                    .padding(
                        horizontal = dimensionResource(R.dimen.button_horizontal_margin),
                        vertical = dimensionResource(R.dimen.button_vertical_margin)
                    )
                    .fillMaxWidth(),
                onClick = finishVerification,
                colors = ButtonDefaults.buttonColors(
                    backgroundColor = MaterialTheme.colors.primaryVariant,
                    contentColor = MaterialTheme.colors.onPrimary
                ),
            ) {
                Text(text = stringResource(R.string.complete_your_profile))
            }
        }
    }
}

data class ProfileStatusBarParams(
    val finishVerification: () -> Unit
)

class ProfileStatusBarParamsProvider : PreviewParameterProvider<ProfileStatusBarParams> {
    override val values: Sequence<ProfileStatusBarParams> = sequenceOf(
        ProfileStatusBarParams(
            finishVerification = {}
        )
    )
}

@Preview
@Composable
fun ProfileStatusBarPreview1(
    @PreviewParameter(ProfileStatusBarParamsProvider::class) params: ProfileStatusBarParams
) {
    CommonTheme {
        ProfileStatusBar(
            finishVerification = params.finishVerification
        )
    }
}

@Preview
@Composable
fun ProfileStatusBarPreview2(
    @PreviewParameter(ProfileStatusBarParamsProvider::class) params: ProfileStatusBarParams
) {
    CommonTheme {
        ProfileStatusBar(
            finishVerification = params.finishVerification,
            composableBackground = BackgroundsOfLayoutsImpl.backgroundScreen01(
                colors = null,
                brush = null,
                shape = null,
                alpha = 0.6f,
            ),
        )
    }
}