package com.aps.coachfanapp.common.framework.presentation.views

import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.heightIn
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.Icon
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.material.TextField
import androidx.compose.material.TextFieldDefaults
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Person
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.VisualTransformation
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.aps.coachfanapp.R
import com.aps.coachfanapp.common.framework.presentation.values.CommonTheme

@Composable
fun UserInfoItem(
    icon: ImageVector,
    label: String,
    value: String,
    isEditing: Boolean,
    onValueChange: (String) -> Unit
) {
    Row(
        modifier = Modifier
            .fillMaxWidth()
            .heightIn(min = 56.dp),
        verticalAlignment = Alignment.CenterVertically
    ) {
        Icon(
            imageVector = icon,
            contentDescription = label,
            tint = MaterialTheme.colors.primary
        )

        Spacer(modifier = Modifier.width(16.dp))

        if (isEditing) {
            var editedValue by remember { mutableStateOf(value) }
            TextField(
                value = editedValue,
                onValueChange = {
                    editedValue = it
                    onValueChange(it)
                },
                keyboardOptions = KeyboardOptions(
                    keyboardType = KeyboardType.Text
                ),
                textStyle = MaterialTheme.typography.body2.copy(fontWeight = FontWeight.Medium),
                colors = TextFieldDefaults.textFieldColors(
                    focusedIndicatorColor = Color.Transparent,
                    unfocusedIndicatorColor = Color.Transparent,
                ),
                visualTransformation = VisualTransformation.None,
                maxLines = 6,
                modifier = Modifier
                    .weight(1f)
            )
        } else {
            Text(
                text = value,
                style = MaterialTheme.typography.body2.copy(fontWeight = FontWeight.Medium),
                color = MaterialTheme.colors.primary,
                maxLines = 6,
                modifier = Modifier.weight(1f)
            )
        }
    }
}

@Preview
@Composable
private fun UserInfoItemPreviewNormal() {
    CommonTheme {
        UserInfoItem(
            icon = Icons.Default.Person,
            label = stringResource(R.string.name),
            value = "John",
            isEditing = false,
            onValueChange = {}
        )
    }
}

@Preview
@Composable
private fun UserInfoItemPreviewEdit() {
    CommonTheme {
        UserInfoItem(
            icon = Icons.Default.Person,
            label = stringResource(R.string.name),
            value = "John",
            isEditing = true,
            onValueChange = {}
        )
    }
}