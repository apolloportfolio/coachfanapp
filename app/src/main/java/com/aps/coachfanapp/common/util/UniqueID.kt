package com.aps.coachfanapp.common.util

import java.io.Serializable
import java.util.*


open class UniqueID(
        open var firestoreDocumentID : String
): Serializable {
    constructor() : this("-666")

    fun getFirestoreId():String {
        return firestoreDocumentID;
    }

    open fun <T: UniqueID> equalByValueTo(other: T): Boolean {
        return firestoreDocumentID.compareTo(other.firestoreDocumentID) == 0
    }

    companion object {
        val INVALID_ID: UniqueID = UniqueID("-666")

        fun generateFakeRandomID(
            randomString: String = UUID.randomUUID().toString()
        ): UniqueID? {
            return UniqueID(INVALID_ID.firestoreDocumentID + "_" + randomString)
        }
    }

    override fun toString(): String {
        return firestoreDocumentID
    }
}

