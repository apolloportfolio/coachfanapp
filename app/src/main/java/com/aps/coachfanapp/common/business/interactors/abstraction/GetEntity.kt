package com.aps.coachfanapp.common.business.interactors.abstraction

import com.aps.coachfanapp.common.business.data.cache.StandardCacheDataSource
import com.aps.coachfanapp.common.business.data.network.StandardNetworkDataSource
import com.aps.coachfanapp.common.business.domain.state.DataState
import com.aps.coachfanapp.common.business.domain.state.StateEvent
import com.aps.coachfanapp.common.util.UniqueID
import kotlinx.coroutines.flow.Flow

interface GetEntity<
        Entity,
        CacheDataSource: StandardCacheDataSource<Entity>,
        NetworkDataSource: StandardNetworkDataSource<Entity>,
        ViewState: com.aps.coachfanapp.common.business.domain.state.ViewState<Entity>> {
    fun getEntity(
        id: UniqueID,
        stateEvent: StateEvent,
        returnViewState: ViewState,
        updateReturnViewState: (ViewState, Entity?)->ViewState,
        getUpdateDate: (Entity)->String?,
        setUpdateDate: (Entity, String)->Entity
    ): Flow<DataState<ViewState>?>

}