package com.aps.coachfanapp.common.framework.presentation.views

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.material.TextField
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Send
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.aps.coachfanapp.R
import com.aps.coachfanapp.common.framework.presentation.values.*

@Composable
fun InputFieldType01(
    onButtonClick: (String) -> Unit = {},
    buttonImageVector: ImageVector = Icons.Default.Send,
    buttonContentDescription: String? = null,
    backgroundTint: Color = MaterialTheme.colors.primaryVariant,
    buttonImageTint: Color = MaterialTheme.colors.onPrimary,
    textTint: Color = MaterialTheme.colors.onPrimary,
    textStyle: TextStyle = TextStyle(
        fontFamily = FontFamily.Default,
        fontWeight = FontWeight.Normal,
        fontSize = 12.sp,
        color = Color.Gray
    ),
) {
    Row(
        modifier = Modifier
            .fillMaxWidth()
            .background(backgroundTint)
            .padding(top = 8.dp),
        verticalAlignment = Alignment.CenterVertically
    ) {
        var textValue by remember { mutableStateOf("") }
        TextField(
            value = textValue,
            onValueChange = { textValue = it },
            placeholder = {
                Text(
                    text = stringResource(id = R.string.send_message),
                    style = textStyle,
                    color = textTint,
                )
            },
            keyboardOptions = KeyboardOptions.Default.copy(
                imeAction = ImeAction.Send
            ),
            trailingIcon = {
                IconButton(
                    onClick = { onButtonClick(textValue) },
                    modifier = Modifier.size(48.dp)
                ) {
                    Icon(
                        imageVector = buttonImageVector,
                        contentDescription = buttonContentDescription,
                        tint = buttonImageTint,
                    )
                }
            },
            modifier = Modifier
                .weight(1f)
                .background(backgroundTint)
                .padding(end = 8.dp)
        )
    }
}

@Preview
@Composable
private fun InputFieldType01Preview() {
    CommonTheme {
        InputFieldType01(
            onButtonClick = {},
            buttonImageVector = Icons.Default.Send,
            buttonContentDescription = null,
            buttonImageTint = MaterialTheme.colors.onPrimary,
        )
    }
}