package com.aps.coachfanapp.common.framework.presentation.views


import androidx.compose.foundation.gestures.detectDragGestures
import androidx.compose.foundation.layout.Box
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.input.pointer.pointerInput
import androidx.compose.ui.layout.onSizeChanged
import androidx.compose.ui.unit.IntSize
import com.aps.coachfanapp.common.util.ALog
import kotlin.math.abs

private const val TAG = "SwipeableBox"
private const val LOG_ME = true

// Reference: https://developer.android.com/jetpack/compose/touch-input/pointer-input/drag-swipe-fling
@Composable
fun SwipeableBox(
    onDragLeft: () -> Unit,
    onDragRight: () -> Unit,
    onDragUp: () -> Unit,
    onDragDown: () -> Unit,
    onDragLeftFromComposableRightSide: () -> Unit = {},
    onDragRightFromComposableLeftSide: () -> Unit = {},
    onDragUpFromComposableBottomSide: () -> Unit = {},
    onDragDownFromComposableTopSide: () -> Unit = {},
    leftSideThreshold: Float = 0.0f,
    rightSideThreshold: Float = 0.0f,
    topSideThreshold: Float = 0.0f,
    bottomSideThreshold: Float = 0.0f,
    content: @Composable () -> Unit
) {
    var size by remember { mutableStateOf(IntSize.Zero) }

    var dragStartCoordinates by remember { mutableStateOf(Offset(0f, 0f)) }
    var dragEndCoordinates by remember { mutableStateOf(Offset(0f, 0f)) }
    var dragActionRequired by remember { mutableStateOf(false) }
    var dragActionType: DragActionType? by remember { mutableStateOf(null) }

    Box(
        modifier = Modifier
            .onSizeChanged { newSize ->
                size = newSize
            }
            .pointerInput(Unit) {
                detectDragGestures { change, dragAmount ->
                    if (!dragActionRequired) {
                        dragActionRequired = true
                        change.consume()
//                    if(LOG_ME)ALog.d(TAG, "detectDragGestures(): change.type == ${change.type}\n" +
//                            "dragAmount == ${dragAmount}\n" +
//                            "change.position == ${change.position}\n" +
//                            "change.previousPosition == ${change.previousPosition}\n" +
//                            "change.pressed == ${change.pressed}\n" +
//                            "change.previousPressed == ${change.previousPressed}\n"
//                    )

                        dragStartCoordinates = change.previousPosition
                        dragEndCoordinates = change.position

                        dragActionType = identifyDragActionType(
                            dragStartCoordinates,
                            dragEndCoordinates,
                            size,
                            leftSideThreshold,
                            rightSideThreshold,
                            topSideThreshold,
                            bottomSideThreshold,
                        )

                        if (LOG_ME) ALog.d(
                            TAG, "detectDragGestures(): " +
                                    "dragStartCoordinates: ${dragStartCoordinates.x}, ${dragStartCoordinates.y}\n" +
                                    "dragEndCoordinates: ${dragEndCoordinates.x}, ${dragEndCoordinates.y}\n" +
                                    "change.uptimeMillis == ${change.uptimeMillis}\n" +
                                    "size: ${size.width}, ${size.height}\n" +
                                    "leftSideThreshold: $leftSideThreshold of ${size.width} == ${leftSideThreshold * size.width}\n" +
                                    "dragActionType == ${dragActionType?.name}"
                        )

                    }
                }
            }

    ) {
        content()
    }
    
    LaunchedEffect(key1 = dragActionRequired) {
        if(dragActionRequired && dragActionType != null) {
            dragActionType?.let {
                if(LOG_ME)ALog.d(TAG, "LaunchedEffect(dragActionRequired): " +
                        "Committing drag action.")
                commitOnDragAction(
                    dragActionType = it,
                    onDragLeft = onDragLeft,
                    onDragRight = onDragRight,
                    onDragUp = onDragUp,
                    onDragDown = onDragDown,
                    onDragLeftFromComposableRightSide = onDragLeftFromComposableRightSide,
                    onDragRightFromComposableLeftSide = onDragRightFromComposableLeftSide,
                    onDragUpFromComposableBottomSide = onDragUpFromComposableBottomSide,
                    onDragDownFromComposableTopSide = onDragDownFromComposableTopSide,
                )
                dragActionRequired = false
            }
        } else {
//            if(LOG_ME)ALog.d(TAG, "LaunchedEffect(dragActionRequired): " +
//                    "Resetting drag action.")
        }
    }
}

internal fun identifyDragActionType(
    dragStartCoordinates: Offset,
    dragEndCoordinates: Offset,
    size: IntSize,
    leftSideThreshold: Float,
    rightSideThreshold: Float,
    topSideThreshold: Float,
    bottomSideThreshold: Float,
): DragActionType {
    val dx = dragEndCoordinates.x - dragStartCoordinates.x
    val dy = dragEndCoordinates.y - dragStartCoordinates.y

//    if(LOG_ME)ALog.d(TAG, "identifyDragActionType(): \n" +
//            "dragStartCoordinates: ${dragStartCoordinates.x}, ${dragStartCoordinates.y}\n" +
//            "dragEndCoordinates: ${dragEndCoordinates.x}, ${dragEndCoordinates.y}\n" +
//            "lambda: $dx, $dy\n" +
//            "size: ${size.width}, ${size.height}\n" +
//            "leftSideThreshold: $leftSideThreshold of ${size.width} == ${leftSideThreshold*size.width}\n" +
//            "rightSideThreshold: $rightSideThreshold of ${size.width} == ${rightSideThreshold*size.width}\n" +
//            "topSideThreshold: $topSideThreshold of ${size.height} == ${topSideThreshold*size.height}\n" +
//            "bottomSideThreshold: $bottomSideThreshold of ${size.height} == ${bottomSideThreshold*size.height}\n"
//    )
    if(dragEndCoordinates.x == 0.0f && dragEndCoordinates.y == 0.0f) {
        return DragActionType.NONE
    }
    if (abs(dx) > abs(dy)) {
        // Horizontal drag
        return if (dx > 0) {
            // Drag right
            if(dragStartCoordinates.x < leftSideThreshold*size.width) {
                DragActionType.DRAG_RIGHT_FROM_LEFT_SIDE
            } else {
                DragActionType.DRAG_RIGHT
            }
        } else {
            // Drag left
            if(dragStartCoordinates.x > (1-rightSideThreshold)*size.width) {
                DragActionType.DRAG_LEFT_FROM_RIGHT_SIDE
            } else {
                DragActionType.DRAG_LEFT
            }
        }
    } else {
        // Vertical drag
        return if (dy > 0) {
            // Drag down
            if(dragStartCoordinates.y < topSideThreshold*size.height) {
                DragActionType.DRAG_DOWN_FROM_TOP_SIDE
            } else {
                DragActionType.DRAG_DOWN
            }
        } else {
            // Drag up
            if(dragStartCoordinates.y > (1-bottomSideThreshold)*size.height) {
                DragActionType.DRAG_UP_FROM_BOTTOM_SIDE
            } else {
                DragActionType.DRAG_UP
            }
        }
    }

}

enum class DragActionType {
    NONE, DRAG_LEFT, DRAG_RIGHT, DRAG_UP, DRAG_DOWN,
    DRAG_LEFT_FROM_RIGHT_SIDE, DRAG_RIGHT_FROM_LEFT_SIDE,
    DRAG_UP_FROM_BOTTOM_SIDE, DRAG_DOWN_FROM_TOP_SIDE
}

internal fun commitOnDragAction(
    dragActionType: DragActionType,
    onDragLeft: () -> Unit,
    onDragRight: () -> Unit,
    onDragUp: () -> Unit,
    onDragDown: () -> Unit,
    onDragLeftFromComposableRightSide: () -> Unit,
    onDragRightFromComposableLeftSide: () -> Unit,
    onDragUpFromComposableBottomSide: () -> Unit,
    onDragDownFromComposableTopSide: () -> Unit,
) {
    when(dragActionType) {
        DragActionType.DRAG_LEFT -> {
            if(LOG_ME)ALog.d(TAG, "commitOnDragAction(): DRAG_LEFT")
            onDragLeft()
        }
        DragActionType.DRAG_RIGHT -> {
            if(LOG_ME)ALog.d(TAG, "commitOnDragAction(): DRAG_RIGHT")
            onDragRight()
        }
        DragActionType.DRAG_UP -> {
            if(LOG_ME)ALog.d(TAG, "commitOnDragAction(): DRAG_UP")
            onDragUp()
        }
        DragActionType.DRAG_DOWN -> {
            if(LOG_ME)ALog.d(TAG, "commitOnDragAction(): DRAG_DOWN")
            onDragDown()
        }
        DragActionType.DRAG_LEFT_FROM_RIGHT_SIDE -> {
            if(LOG_ME)ALog.d(TAG, "commitOnDragAction(): DRAG_LEFT_FROM_RIGHT_SIDE")
            onDragLeftFromComposableRightSide()
        }
        DragActionType.DRAG_RIGHT_FROM_LEFT_SIDE -> {
            if(LOG_ME)ALog.d(TAG, "commitOnDragAction(): DRAG_RIGHT_FROM_LEFT_SIDE")
            onDragRightFromComposableLeftSide()
        }
        DragActionType.DRAG_UP_FROM_BOTTOM_SIDE -> {
            if(LOG_ME)ALog.d(TAG, "commitOnDragAction(): DRAG_UP_FROM_BOTTOM_SIDE")
            onDragUpFromComposableBottomSide()
        }
        DragActionType.DRAG_DOWN_FROM_TOP_SIDE -> {
            if(LOG_ME)ALog.d(TAG, "commitOnDragAction(): DRAG_DOWN_FROM_TOP_SIDE")
            onDragDownFromComposableTopSide()
        }
        DragActionType.NONE -> {
            if(LOG_ME)ALog.d(TAG, "commitOnDragAction(): NONE")
            // Do nothing
        }
    }
}