package com.aps.coachfanapp.common.framework.presentation

object IntentExtras {
    const val APP_USER = "AppUser"
    const val SEARCHED_ITEMS_LIST = "searchedItemsList"
    const val DESTINATION = "Destination"
    const val ITEM = "Item"
    const val ENTITY1 = "entity1"
    const val ENTITY2 = "entity2"
}