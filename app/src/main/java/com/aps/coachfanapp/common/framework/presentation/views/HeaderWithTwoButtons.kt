package com.aps.coachfanapp.common.framework.presentation.views

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Icon
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Info
import androidx.compose.material.icons.filled.ThumbUp
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.aps.coachfanapp.R
import com.aps.coachfanapp.common.framework.presentation.values.CommonTheme

@Composable
fun HeaderWithTwoButtons(
    headerString: String?,
    iconStart: ImageVector?,
    iconStartOnClick: () -> Unit,
    iconStartContentDescription: String,
    iconEnd: ImageVector?,
    iconEndOnClick: () -> Unit,
    iconEndContentDescription: String,
    tint: Color = MaterialTheme.colors.onBackground,
    composableBackground: DrawnBackground? = null,
    backgroundDrawableId: Int = R.drawable.list_item_example_background_1,
    headerTitleTextStyle: TextStyle = TextStyle(
        fontFamily = FontFamily.Default,
        fontWeight = FontWeight.Bold,
        fontSize = 24.sp
    ),
    headerTitleTextAlignment: TextAlign = TextAlign.Center,
) {
    BoxWithBackground(
        backgroundDrawableId = backgroundDrawableId,
        composableBackground = composableBackground,
    ) {
        Row(
            verticalAlignment = Alignment.CenterVertically,
            horizontalArrangement = Arrangement.SpaceBetween,
            modifier = Modifier.fillMaxWidth()
        ) {
            // Button to the start
            if(iconStart != null) {
                Icon(
                    imageVector = iconStart,
                    contentDescription = iconStartContentDescription,
                    tint = tint,
                    modifier = Modifier
                        .clickable {
                            iconStartOnClick()
                        }
                        .testTag(iconStartContentDescription)
                        .padding(PaddingValues(8.dp, 8.dp, 8.dp, 8.dp)),
                )
            }


            // Header title
            Text(
                text = headerString ?: "",
                style = headerTitleTextStyle,
                color = tint,
                modifier = Modifier
                    .weight(1f)
                    .padding(PaddingValues(8.dp, 8.dp, 8.dp, 8.dp))
                    .align(Alignment.CenterVertically),
                textAlign = headerTitleTextAlignment,
            )

            // Button to the end
            if(iconEnd != null) {
                Icon(
                    imageVector = iconEnd,
                    contentDescription = iconEndContentDescription,
                    tint = tint,
                    modifier = Modifier
                        .clickable {
                            iconEndOnClick()
                        }
                        .testTag(iconEndContentDescription)
                        .padding(PaddingValues(8.dp, 8.dp, 8.dp, 8.dp)),
                )
            }
        }
    }
}

// Preview =========================================================================================
@Preview
@Composable
internal fun HeaderWithTwoButtonsPreview() {
    CommonTheme {
        HeaderWithTwoButtons(
            headerString = "Preview Header",
            iconStart = Icons.Default.Info,
            iconStartOnClick = {},
            iconStartContentDescription = "iconStartContentDescription",
            iconEnd = Icons.Default.ThumbUp,
            iconEndOnClick = {},
            iconEndContentDescription = "iconEndContentDescription",
            tint = MaterialTheme.colors.onBackground,
            backgroundDrawableId = R.drawable.list_item_example_background_1,
            composableBackground = BackgroundsOfLayoutsImpl.backgroundTitleType01(
                colors = null,
                brush = null,
                shape = null,
                alpha = 0.6f,
            ),
        )
    }
}