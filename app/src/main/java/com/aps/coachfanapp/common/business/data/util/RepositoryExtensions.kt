package com.aps.coachfanapp.common.business.data.util

import android.content.Context
import com.aps.coachfanapp.core.util.ApplicationContextProvider
import com.aps.coachfanapp.common.business.data.cache.CacheErrors.CACHE_ERROR_TIMEOUT
import com.aps.coachfanapp.common.business.data.cache.CacheErrors.CACHE_ERROR_UNKNOWN
import com.aps.coachfanapp.common.business.data.cache.CacheResult
import com.aps.coachfanapp.common.business.data.network.ApiResult
import com.aps.coachfanapp.common.business.data.network.NetworkConstants.NETWORK_TIMEOUT
import com.aps.coachfanapp.common.business.data.network.NetworkErrors
import com.aps.coachfanapp.common.business.data.cache.CacheConstants.CACHE_TIMEOUT
import com.aps.coachfanapp.common.business.data.util.GenericErrors.ERROR_UNKNOWN
import com.aps.coachfanapp.core.util.Config
import com.google.firebase.crashlytics.FirebaseCrashlytics
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.TimeoutCancellationException
import kotlinx.coroutines.withContext
import kotlinx.coroutines.*
import retrofit2.HttpException
import java.io.IOException

/**
 * Reference: https://medium.com/@douglas.iacovelli/how-to-handle-errors-with-retrofit-and-coroutines-33e7492a912
 */

fun cLog(msg: String?){
    msg?.let {
        if(!Config.LOGGING){
            FirebaseCrashlytics.getInstance().log(it)
        }
    }

}

suspend fun <T> safeApiCall(
    dispatcher: CoroutineDispatcher,
    networkErrorUnknownMessage: String = "Unknown network error",
    networkErrorTimeoutMessage: String = "Network timeout",
    networkTimeout: Long = NETWORK_TIMEOUT,
    onErrorAction: () -> Unit = {},
    apiCall: suspend () -> T?,
): ApiResult<T?> {
    return withContext(dispatcher) {
        try {
            // throws TimeoutCancellationException
            withTimeout(networkTimeout){
                ApiResult.Success(apiCall.invoke())
            }
        } catch (throwable: Throwable) {
            throwable.printStackTrace()
            onErrorAction()
            when (throwable) {
                is TimeoutCancellationException -> {
                    val code = 408 // timeout error code
                    ApiResult.GenericError(code, networkErrorTimeoutMessage)
                }
                is IOException -> {
                    ApiResult.NetworkError
                }
                is HttpException -> {
                    val code = throwable.code()
                    val errorResponse = convertErrorBody(throwable)
                    cLog(errorResponse)
                    ApiResult.GenericError(
                        code,
                        errorResponse
                    )
                }
                else -> {
                    cLog(networkErrorUnknownMessage)
                    ApiResult.GenericError(
                        null,
                        networkErrorUnknownMessage
                    )
                }
            }
        }
    }
}

suspend fun <T> safeCacheCall(
    dispatcher: CoroutineDispatcher,
    onErrorAction: () -> Unit = {},
    cacheCall: suspend () -> T?
): CacheResult<T?> {
    return withContext(dispatcher) {
        try {
            // throws TimeoutCancellationException
            withTimeout(CACHE_TIMEOUT){
                CacheResult.Success(cacheCall.invoke())
            }
        } catch (throwable: Throwable) {
            throwable.printStackTrace()
            onErrorAction()
            when (throwable) {
                is TimeoutCancellationException -> {
                    CacheResult.GenericError(CACHE_ERROR_TIMEOUT)
                }
                else -> {
                    cLog(CACHE_ERROR_UNKNOWN)
                    CacheResult.GenericError(CACHE_ERROR_UNKNOWN)
                }
            }
        }
    }
}


private fun convertErrorBody(throwable: HttpException): String? {
    return try {
        throwable.response()?.errorBody()?.string()
    } catch (exception: Exception) {
        ERROR_UNKNOWN
    }
}