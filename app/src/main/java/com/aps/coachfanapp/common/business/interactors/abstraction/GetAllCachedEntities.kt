package com.aps.coachfanapp.common.business.interactors.abstraction

import com.aps.coachfanapp.common.business.data.cache.StandardCacheDataSource
import com.aps.coachfanapp.common.business.data.network.StandardNetworkDataSource
import com.aps.coachfanapp.common.business.domain.state.DataState
import com.aps.coachfanapp.common.business.domain.state.StateEvent
import kotlinx.coroutines.flow.Flow

interface GetAllCachedEntities<
        Entity,
        CacheDataSource: StandardCacheDataSource<Entity>,
        NetworkDataSource: StandardNetworkDataSource<Entity>,
        ViewState: com.aps.coachfanapp.common.business.domain.state.ViewState<Entity>> {
    fun getAllCachedEntities(
        stateEvent: StateEvent,
        returnViewState: ViewState
    ): Flow<DataState<ViewState>?>
}