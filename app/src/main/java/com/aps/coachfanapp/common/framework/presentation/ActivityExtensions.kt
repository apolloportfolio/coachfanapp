package com.aps.coachfanapp.common.framework.presentation

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.provider.Settings

fun Activity.openAppSettings(fragment: String?) {
    Intent(
        Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
        Uri.fromParts("package", packageName, fragment)
    ).also(::startActivity)
}