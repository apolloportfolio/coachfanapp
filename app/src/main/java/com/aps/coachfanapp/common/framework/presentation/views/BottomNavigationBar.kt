package com.aps.coachfanapp.common.framework.presentation.views

import androidx.compose.foundation.layout.Column
import androidx.compose.material.BadgedBox
import androidx.compose.material.BottomNavigation
import androidx.compose.material.BottomNavigationItem
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.Icon
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment.Companion.CenterHorizontally
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import androidx.navigation.compose.currentBackStackEntryAsState
import com.aps.coachfanapp.common.framework.presentation.NavItem

val BottomNavigationHeightPublic = 56.dp        // Equivalent of androidx.compose.material.BottomNavigation.BottomNavigationHeight

// https://developer.android.com/reference/kotlin/androidx/compose/material3/package-summary#BadgedBox(kotlin.Function1,androidx.compose.ui.Modifier,kotlin.Function1)
@ExperimentalMaterialApi
@Composable
fun BottomNavigationBar(
    items: List<NavItem>,
    navController: NavController,
    modifier: Modifier = Modifier,
    onItemClick: (NavItem) -> Unit
) {
    val backStackEntry = navController.currentBackStackEntryAsState()
    BottomNavigation(
        modifier = modifier,
        backgroundColor = MaterialTheme.colors.primary,
        elevation = 5.dp
    ) {
        items.forEach { item ->
            val selected = item.route.route == backStackEntry.value?.destination?.route
            BottomNavigationItem(
                selected = selected,
                onClick = { onItemClick(item) },
                selectedContentColor = MaterialTheme.colors.secondaryVariant,
                unselectedContentColor = MaterialTheme.colors.secondary,
                icon = {
                    Column(horizontalAlignment = CenterHorizontally) {
                        if(item.badgeCount > 0) {
                            BadgedBox(
                                badge = {
                                    Text(text = item.badgeCount.toString())
                                }
                            ) {
                                Icon(
                                    painter = painterResource(item.iconDrawableID),
                                    contentDescription = stringResource(item.contentDescriptionID)
                                )
                            }
                        } else {
                            Icon(
                                painter = painterResource(item.iconDrawableID),
                                contentDescription = stringResource(item.contentDescriptionID)
                            )
                        }
                        if(selected) {
                            Text(
                                text = stringResource(item.nameStringID),
                                textAlign = TextAlign.Center,
                                fontSize = 10.sp
                            )
                        }
                    }
                }
            )
        }
    }
}