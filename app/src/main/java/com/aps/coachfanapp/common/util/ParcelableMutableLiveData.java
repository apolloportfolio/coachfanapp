package com.aps.coachfanapp.common.util;

import android.os.Parcel;
import android.os.Parcelable;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

public class ParcelableMutableLiveData<T extends Parcelable> extends MutableLiveData<T> implements Parcelable {

    public ParcelableMutableLiveData(T value) {
        super(value);
    }

    public ParcelableMutableLiveData() {
        super();
    }

    @Override
    public void postValue(T value) {
        super.postValue(value);
    }

    @Override
    public void setValue(T value) {
        super.setValue(value);
    }

    protected ParcelableMutableLiveData(Parcel in) {
        super(in.readParcelable(LiveData.class.getClassLoader()));
    }

    public static final Creator<ParcelableMutableLiveData> CREATOR = new Creator<ParcelableMutableLiveData>() {
        @Override
        public ParcelableMutableLiveData createFromParcel(Parcel in) {
            return new ParcelableMutableLiveData(in);
        }

        @Override
        public ParcelableMutableLiveData[] newArray(int size) {
            return new ParcelableMutableLiveData[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable((Parcelable)(getValue()), flags);
    }
}

