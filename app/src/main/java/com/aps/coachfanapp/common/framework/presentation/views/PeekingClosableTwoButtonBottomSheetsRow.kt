package com.aps.coachfanapp.common.framework.presentation.views

import androidx.compose.animation.core.Spring
import androidx.compose.animation.core.spring
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.BottomSheetState
import androidx.compose.material.BottomSheetValue
import androidx.compose.material.Button
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.material.rememberBottomSheetState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.res.dimensionResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.constraintlayout.compose.ConstraintLayout
import com.aps.coachfanapp.R
import com.aps.coachfanapp.common.framework.presentation.values.ColorTransparent
import com.aps.coachfanapp.common.framework.presentation.values.CommonTheme
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch

const val PeekingClosableTwoButtonBottomSheetsRowLeftButtonTestTag = "PeekingClosableTwoButtonBottomSheetsRowLeftButtonTestTag"
const val PeekingClosableTwoButtonBottomSheetsRowRightButtonTestTag = "PeekingClosableTwoButtonBottomSheetsRowRightButtonTestTag"
const val PeekingClosableTwoButtonBottomSheetsRowCloseButtonTestTag = "PeekingClosableTwoButtonBottomSheetsRowCloseButtonTestTag"

@OptIn(ExperimentalMaterialApi::class)
@Composable
fun PeekingClosableTwoButtonBottomSheetsRow(
    scope: CoroutineScope,
    sheetState: BottomSheetState,
    toggleBottomSheetState: () -> Unit =
        remember { standardToggleBottomSheet(scope, sheetState) },
    leftButtonsIconID: Int = R.drawable.ic_filters,
    leftButtonOnClick: () -> Unit,
    rightButtonOnClick: () -> Unit,
    leftButtonTextID: Int = R.string.left_button,
    rightButtonTextID: Int = R.string.right_button,
    modifier: Modifier,
) {
    ConstraintLayout(
        modifier = modifier.then(
            Modifier
                .fillMaxWidth()
                .background(ColorTransparent)
        )
    ) {
        val (
            bottomSheetCloseButton,
            leftButton,
            rightButton,
        ) = createRefs()

        IconButton(
            onClick = toggleBottomSheetState,
            modifier = Modifier
                .constrainAs(bottomSheetCloseButton) {
                    top.linkTo(parent.top)
                    end.linkTo(parent.end)
                }
                .fillMaxWidth(.15f)
                .aspectRatio(1f)
                .padding(
                    start = dimensionResource(id = R.dimen.rounded_corner_margin),
                    top = dimensionResource(id = R.dimen.rounded_corner_margin),
                    end = dimensionResource(id = R.dimen.rounded_corner_margin),
                    bottom = dimensionResource(id = R.dimen.rounded_corner_margin),
                )
                .testTag(PeekingClosableTwoButtonBottomSheetsRowCloseButtonTestTag)
        ) {
            Icon(
                painter = painterResource(R.drawable.ic_x_to_close),
                contentDescription = stringResource(id = R.string.toggle_bottom_sheet_button),
                tint = MaterialTheme.colors.secondary,
            )
        }

        Button(
            onClick = leftButtonOnClick,
            modifier = Modifier
                .constrainAs(leftButton) {
                    top.linkTo(bottomSheetCloseButton.top)
                    bottom.linkTo(bottomSheetCloseButton.bottom)
                    start.linkTo(parent.start)
                    end.linkTo(rightButton.start)
                }
                .padding(
                    start = dimensionResource(R.dimen.button_horizontal_margin),
                    top = dimensionResource(R.dimen.button_vertical_margin),
                    end = dimensionResource(R.dimen.button_horizontal_margin),
                    bottom = dimensionResource(R.dimen.button_vertical_margin),
                )
                .testTag(PeekingClosableTwoButtonBottomSheetsRowLeftButtonTestTag),
        ) {
            Icon(
                painter = painterResource(leftButtonsIconID),
                contentDescription = stringResource(R.string.left_button),
                modifier = Modifier.padding(end = 8.dp),
            )
            Text(
                text = stringResource(leftButtonTextID),
                color = MaterialTheme.colors.onPrimary,
                fontSize = 14.sp
            )
        }

        Button(
            onClick = rightButtonOnClick,
            modifier = Modifier
                .constrainAs(rightButton) {
                    top.linkTo(bottomSheetCloseButton.top)
                    bottom.linkTo(bottomSheetCloseButton.bottom)
                    end.linkTo(bottomSheetCloseButton.start)
                }
                .padding(
                    start = dimensionResource(R.dimen.button_horizontal_margin),
                    top = dimensionResource(R.dimen.button_vertical_margin),
                    end = dimensionResource(R.dimen.button_horizontal_margin),
                    bottom = dimensionResource(R.dimen.button_vertical_margin),
                )
                .testTag(PeekingClosableTwoButtonBottomSheetsRowRightButtonTestTag),
        ) {
            Text(
                text = stringResource(rightButtonTextID),
                color = MaterialTheme.colors.onPrimary,
                fontSize = 14.sp
            )
        }
    }
}

@OptIn(ExperimentalMaterialApi::class)
internal fun standardToggleBottomSheet(
    scope: CoroutineScope,
    sheetState: BottomSheetState,
): () -> Unit {
    return {
        scope.launch {
            if(sheetState.isCollapsed) {
                sheetState.expand()
            } else {
                sheetState.collapse()
            }
        }
    }
}

//========================================================================================
@OptIn(ExperimentalMaterialApi::class)
@Preview
@Composable
fun PeekingClosableTwoButtonBottomSheetsRowPreview(
) {
    CommonTheme {
        PeekingClosableTwoButtonBottomSheetsRow(
            scope = rememberCoroutineScope(),
            sheetState = rememberBottomSheetState(
                initialValue = BottomSheetValue.Expanded,
                animationSpec = spring(dampingRatio = Spring.DampingRatioHighBouncy)
            ),
            leftButtonOnClick = { /* Left button click handler */ },
            rightButtonOnClick = { /* Right button click handler */ },
            modifier = Modifier,
        )
    }
}