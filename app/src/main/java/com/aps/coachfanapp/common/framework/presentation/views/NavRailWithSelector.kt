package com.aps.coachfanapp.common.framework.presentation.views

import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.Icon
import androidx.compose.material.MaterialTheme
import androidx.compose.material.NavigationRail
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.tooling.preview.PreviewParameter
import androidx.compose.ui.tooling.preview.PreviewParameterProvider
import androidx.navigation.NavHostController
import androidx.navigation.compose.currentBackStackEntryAsState
import com.aps.coachfanapp.R
import com.aps.coachfanapp.common.framework.presentation.NavItem
import com.aps.coachfanapp.common.framework.presentation.values.CommonTheme
import com.aps.coachfanapp.common.util.ALog
import com.aps.coachfanapp.common.util.composableutils.ExampleScreenDestination

@ExperimentalMaterialApi
@Composable
fun NavRailWithSelector(
    leftPaneNavController: NavHostController?,
    rightPaneNavController: NavHostController?,
    navItems: List<NavItem>,
) {
    NavigationRail {
        navItems.forEach { navItem ->
            val currentDestination =
                leftPaneNavController?.currentBackStackEntryAsState()?.value?.destination?.route ?:
                "invalid_destination"

            NavRailItemWithSelector(
                icon = {
                    Icon(
                        painter = painterResource(navItem.iconDrawableID),
                        contentDescription = stringResource(id = navItem.contentDescriptionID)
                    )
                },
                label = { Text(stringResource(navItem.nameStringID)) },
                selected = currentDestination == navItem.route.route,
                onClick = {
                    navItem.onItemClickNavigation(
                        true,
                        navItem,
                        leftPaneNavController,
                        rightPaneNavController,
                    )
                },
                selectedContentColor = MaterialTheme.colors.primaryVariant,
                unselectedContentColor = MaterialTheme.colors.primary,
                contentDescriptionID = navItem.contentDescriptionID,
            )
        }
    }
}

// Preview =========================================================================================
@OptIn(ExperimentalMaterialApi::class)
@Preview
@Composable
fun NavRailWithSelectorPreview(
    @PreviewParameter(
        NavRailWithSelectorParamsProvider::class
    ) params: NavRailWithSelectorParams
) {
    CommonTheme {
        NavRailWithSelector(
            leftPaneNavController = params.leftPaneNavController,
            rightPaneNavController = params.rightPaneNavController,
            navItems = params.navItems,
        )
    }
}


class NavRailWithSelectorParamsProvider :
    PreviewParameterProvider<NavRailWithSelectorParams> {
    private val dummyItemClick: (Boolean, NavItem, NavHostController?, NavHostController?) -> Unit = {
            _, _, _, _ ->
            ALog.w("NavRailWithSelector", ".emptyItemClick(): " +
                    "This item click should never be called as it is just a dummy.")
        }

    override val values: Sequence<NavRailWithSelectorParams>
        get() = sequenceOf(
            NavRailWithSelectorParams(
                leftPaneNavController = null,
                rightPaneNavController = null,
                navItems = listOf(
                    NavItem(
                        nameStringID = R.string.home_screen_bottom_nav_menu_1,
                        route = ExampleScreenDestination.Card1,
                        iconDrawableID = R.drawable.ic_search_yellow_24,
                        onItemClickNavigation = dummyItemClick,
                        contentDescriptionID = R.string.home_screen_bottom_nav_menu_1_content_description,
                    ),
                    NavItem(
                        nameStringID = R.string.home_screen_bottom_nav_menu_2,
                        route = ExampleScreenDestination.Card2,
                        iconDrawableID = R.drawable.ic_calendar_yellow_24,
                        onItemClickNavigation = dummyItemClick,
                        contentDescriptionID = R.string.home_screen_bottom_nav_menu_2_content_description,
                    ),
                    NavItem(
                        nameStringID = R.string.home_screen_bottom_nav_menu_3,
                        route = ExampleScreenDestination.Card3,
                        iconDrawableID = R.drawable.ic_message_yellow_24,
                        onItemClickNavigation = dummyItemClick,
                        contentDescriptionID = R.string.home_screen_bottom_nav_menu_3_content_description,
                    ),
                    NavItem(
                        nameStringID = R.string.home_screen_bottom_nav_menu_4,
                        route = ExampleScreenDestination.Card4,
                        iconDrawableID = R.drawable.ic_car_yellow_24,
                        onItemClickNavigation = dummyItemClick,
                        contentDescriptionID = R.string.home_screen_bottom_nav_menu_4_content_description,
                    ),
                    NavItem(
                        nameStringID = R.string.home_screen_bottom_nav_menu_5,
                        route = ExampleScreenDestination.Card5,
                        iconDrawableID = R.drawable.ic_account_box_yellow_24,
                        onItemClickNavigation = dummyItemClick,
                        contentDescriptionID = R.string.home_screen_bottom_nav_menu_5_content_description,
                    ),
                )
            )
        )
}

data class NavRailWithSelectorParams(
    val leftPaneNavController: NavHostController?,
    val rightPaneNavController: NavHostController?,
    val navItems: List<NavItem>,
)