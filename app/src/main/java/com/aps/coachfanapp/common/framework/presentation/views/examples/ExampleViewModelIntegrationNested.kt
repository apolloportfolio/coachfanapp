package com.aps.coachfanapp.common.framework.presentation.views.examples

import android.annotation.SuppressLint
import android.app.Activity
import android.os.Parcelable
import androidx.compose.animation.core.Spring
import androidx.compose.animation.core.spring
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.material.BottomSheetValue
import androidx.compose.material.Button
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.Icon
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Scaffold
import androidx.compose.material.SnackbarDuration
import androidx.compose.material.SnackbarHost
import androidx.compose.material.SnackbarHostState
import androidx.compose.material.Text
import androidx.compose.material.rememberBottomSheetScaffoldState
import androidx.compose.material.rememberBottomSheetState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.movableContentOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.paint
import androidx.compose.ui.res.dimensionResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewmodel.compose.viewModel
import com.aps.coachfanapp.R
import com.aps.coachfanapp.common.framework.presentation.values.CommonTheme
import com.aps.coachfanapp.common.framework.presentation.views.CustomSnackBar
import com.aps.coachfanapp.common.framework.presentation.views.DialogState
import com.aps.coachfanapp.common.framework.presentation.views.ShowToastIfNecessary
import com.aps.coachfanapp.common.framework.presentation.views.SnackBarState
import com.aps.coachfanapp.common.framework.presentation.views.ToastState
import com.aps.coachfanapp.common.util.ALog
import com.aps.coachfanapp.common.util.ARandomGenerator
import kotlinx.parcelize.Parcelize

private const val TAG = "ExampleViewModelIntegrationNested"
private const val LOG_ME = true

// https://dev.to/mahendranv/using-viewmodel-livedata-with-jetpack-compose-31h8
@Composable
fun ExampleViewModelIntegrationNested(
    activity: Activity?,
    viewModel: ExampleViewModelNested = viewModel(),
) {
    if(LOG_ME)ALog.d(TAG, ": Recomposition")
    val currentViewState by viewModel.getCurrentViewStateOrNewAsLiveData().observeAsState()
    ExampleViewModelIntegrationNestedTopContent(
        activity,
        viewModel,
        currentViewState!!,
    )
}

@Composable
fun ExampleViewModelIntegrationNestedTopContent(
    activity: Activity?,
    viewModel: ExampleViewModelNested,
    currentViewState: ExampleViewStateNested,
) {
    if(LOG_ME)ALog.d("ExampleViewModelIntegrationNestedTopContent", ": Recomposition start")
    val updateSnackBarInViewModel: (SnackBarState) -> Unit  = { viewModel.setSnackBar(it)}
    val onButtonClickFromParentComposable = {
        if(LOG_ME)ALog.d(
            TAG, "onButtonClickFromParentComposable(): " +
                                "")
        viewModel.setToastState(
            ToastState(
            true,
            "ExampleViewModelIntegrationNestedTopContent!" + ARandomGenerator.generateRandomString(10),
            updateToastInViewModel = { viewModel.setToastState(it) },
        )
        )
    }

    ExampleViewModelIntegrationNestedBottomContent(
        activity,
        currentViewState,
        updateSnackBarInViewModel,
        onButtonClickFromParentComposable,
    )
    if(LOG_ME)ALog.d("ExampleViewModelIntegrationNestedTopContent", ": Recomposition end")
}

@SuppressLint("UnusedMaterialScaffoldPaddingParameter", "UnrememberedMutableState")
@OptIn(ExperimentalMaterialApi::class)
@Composable
fun ExampleViewModelIntegrationNestedBottomContent(
    activity: Activity?,
    currentViewState: ExampleViewStateNested,
    updateSnackBarInViewModel: (SnackBarState) -> Unit,
    onButtonClickFromParentComposable: () -> Unit,
) {
    if(LOG_ME)ALog.d("ExampleViewModelIntegrationNestedBottomContent", ": Recomposition start")
//    val currentViewState = viewModel.getCurrentViewStateOrNewAsLiveData().observeAsState().value!!

    var snackBarCounter by remember { mutableStateOf(1) }
    var snackBarState by remember {
        mutableStateOf(
            SnackBarState(
                show = false,
                message = "Snackbar nr $snackBarCounter",
                actionLabel = "action",
                iconDrawableRes = R.drawable.ic_launcher,
                onClickAction = {},
                onDismissAction = {},
                duration = SnackbarDuration.Indefinite,
                updateSnackBarInViewModel = {},
            )
        )
    }

    var toastCounter: Int by remember { mutableStateOf(1) }
    var toastState by remember {
        mutableStateOf(
            currentViewState.toastState
        )
    }

    var dialogCounter by remember { mutableStateOf(1) }
    var dialogState by remember {
        mutableStateOf(
            DialogState(
                show = false,
                onDismissRequest = {},
                title = "Dialog nr $dialogCounter",
                text = "Dialog's text.",
                leftButtonText = "Dismiss",
                leftButtonOnClick = {},
                rightButtonText = "Confirm",
                rightButtonOnClick = {},
                updateDialogInViewModel = {},
            )
        )
    }

    var buttonClickCounter by remember { mutableStateOf(0)}



    val sheetState = rememberBottomSheetState(
        initialValue = BottomSheetValue.Collapsed,
        animationSpec = spring(dampingRatio = Spring.DampingRatioHighBouncy)
    )

    val scaffoldState = rememberBottomSheetScaffoldState( bottomSheetState = sheetState )
    val customSnackBarHost: @Composable (SnackbarHostState) -> Unit = movableContentOf { snackbarHostState ->
        SnackbarHost(
            hostState = snackbarHostState
        ) {
            CustomSnackBar(
                iconDrawableResID = snackBarState?.iconDrawableRes,
                message = snackBarState?.message ?: "Error",
                actionMessage = snackBarState?.actionLabel,
                onActionClick = {
                    if(LOG_ME)ALog.d(TAG, "(): SnackBar clicked.")
                },
            )
        }
    }

    val showToastWithoutViewModel: ()->Unit by remember {
        mutableStateOf(
            {
                if(LOG_ME)ALog.d(
                    TAG, ".showToastWithoutViewModel(): " +
                                        "Changing toastStatus for the $toastCounter time.")
                toastState = ToastState(
                    true,
                    "Toast nr $toastCounter",
                    updateToastInViewModel = {},
                )
                toastCounter++
            }
        )
    }

    val showToastThroughViewModel: ()->Unit by remember {
        mutableStateOf(
            {
                if(LOG_ME)ALog.d(
                    TAG, ".showToastThroughViewModel(): " +
                        "Changing toastStatus for the $toastCounter time.")
                // Works
//                viewModel.setToast(
//                    ToastState(
//                        true,
//                        "Toast nr $toastCounter",
//                        updateToastInViewModel = { viewModel.setToast(it) },
//                    )
//                )

                // Works because we change composable function's parameter's value which triggers recomposition.
//                currentViewState.toastState = ToastState(
//                        true,
//                        "Toast nr $toastCounter",
//                        updateToastInViewModel = { viewModel.setToast(it) },
//                    )

                // Triggered recomposition
//                currentViewState.toastState = currentViewState.toastState?.apply {
//                    show = true
//                    message = "Toast by changing currentViewState.toastState"
//                }

                // Triggered recomposition
//                currentViewState.toastState = currentViewState.toastState?.copy()


//                currentViewState.toastState = currentViewState.toastState?.shallowCopy()
                toastCounter++
            }
        )
    }

    val showSnackBar: ()->Unit by remember {
        mutableStateOf(
            {
                snackBarState = SnackBarState(
                    show = true,
                    message = "Snackbar nr $snackBarCounter",
                    actionLabel = "action",
                    iconDrawableRes = R.drawable.ic_launcher,
                    onClickAction = {},
                    onDismissAction = {},
                    duration = SnackbarDuration.Indefinite,
                    updateSnackBarInViewModel = {},
                )
                snackBarCounter++
            }
        )
    }

    val showDialog: ()->Unit by remember {
        mutableStateOf(
            {
                dialogState = DialogState(
                    show = true,
                    onDismissRequest = {},
                    title = "Dialog nr $dialogCounter",
                    text = "Dialog's text.",
                    leftButtonText = "Dismiss",
                    leftButtonOnClick = {},
                    rightButtonText = "Confirm",
                    rightButtonOnClick = {},
                    updateDialogInViewModel = {},
                )
                dialogCounter++
            }
        )
    }

    val onButtonClick: ()->Unit =
        {
            if(LOG_ME)ALog.d(
                TAG, "onButtonClick: " +
                    "Button clicked for the $buttonClickCounter time.")
//            showToastWithoutViewModel()
            showToastThroughViewModel()
            buttonClickCounter++
        }

    var firstComposition by rememberSaveable { mutableStateOf(true) }
    var buttonClicked by remember { mutableStateOf(false) }
    LaunchedEffect(buttonClicked) {
        if(firstComposition) {
            if(LOG_ME)ALog.d(
                TAG, "LaunchedEffect(searchFiltersUpdated): " +
                    "This is the first composition.")
            firstComposition = false
        } else {
//            onButtonClick()
            onButtonClickFromParentComposable()
        }
        buttonClicked = false
    }

    Scaffold(
        modifier = Modifier
            .paint(painterResource(id = R.drawable.default_background))
            .fillMaxWidth()
            .height(dimensionResource(id = R.dimen.search_entities1_bottom_sheet_unveiled_height))
            .wrapContentHeight(Alignment.CenterVertically),
        snackbarHost = customSnackBarHost,
    ) {
        Box {
            Column {
                Button(
                    onClick = {
                          buttonClicked = true
//                        onButtonClick()
                    },
                    modifier = Modifier
                        .padding(
                            start = dimensionResource(R.dimen.button_horizontal_margin),
                            top = dimensionResource(R.dimen.button_vertical_margin),
                            end = dimensionResource(R.dimen.button_horizontal_margin),
                            bottom = dimensionResource(R.dimen.button_vertical_margin),
                        ),
                ) {
                    Icon(
                        painter = painterResource(R.drawable.ic_launcher),
                        contentDescription = "Click Me!",
                        modifier = Modifier.padding(end = 8.dp),
                    )
                    Text(
                        text = "Click Me!",
                        color = MaterialTheme.colors.onPrimary,
                        fontSize = 14.sp
                    )
                }
            }


//            ShowSnackBarIfNecessary(
//                snackBarState,
//                scaffoldState,
//            )

            ShowToastIfNecessary(
                activity = activity,
                toastState = currentViewState.toastState
            )

//            if(toastState?.show == true) {
//                if(LOG_ME)ALog.d(TAG, ": " +
//                                        "Showing toast nr $toastCounter")
//                Toast.makeText(
//                    LocalContext.current,
//                    toastState?.message,
//                    toastState?.duration ?: Toast.LENGTH_LONG,
//                ).show()
//                toastState = ToastState(
//                    false,
//                    "Toast nr $toastCounter",
//                    updateToastInViewModel = { viewModel.setToast(it) },
//                )
//                LaunchedEffect(key1 = toastState) {
//                    if(LOG_ME)ALog.d(TAG, ".LaunchedEffect(): " +
//                                            "Invoking toastStatus.updateToastInViewModel")
//                    toastState?.updateToastInViewModel?.invoke(toastState)
//                }
//            }

//            ShowDialogIfNecessary(
//                dialogState,
//            )
        }
    }
    if(LOG_ME)ALog.d("ExampleViewModelIntegrationNestedBottomContent", ": Recomposition end")
}

class ExampleViewModelNested: ViewModel() {
    private val _viewState: MutableLiveData<ExampleViewStateNested> = MutableLiveData()
    val viewState: LiveData<ExampleViewStateNested>
        get() = _viewState


    private fun initNewViewState(): ExampleViewStateNested {
        val newInstance: ExampleViewStateNested = ExampleViewStateNested()
        newInstance.snackBarState = SnackBarState(
            updateSnackBarInViewModel = {
                setSnackBar(it)
            }
        )
        newInstance.toastState = ToastState(
            updateToastInViewModel = {
                setToast(it)
            }
        )
        newInstance.dialogState = DialogState(
            updateDialogInViewModel = {
                setDialog(it)
            }
        )

        return newInstance
    }

    fun getCurrentViewStateOrNew(): ExampleViewStateNested {
        return viewState.value ?: initNewViewState()
    }

    fun getCurrentViewStateOrNewAsLiveData(): LiveData<ExampleViewStateNested>{
        if(viewState.value == null) setViewState(initNewViewState())
        return viewState
    }

    private fun setViewState(viewState: ExampleViewStateNested){
        _viewState.value = viewState
    }

    private fun postViewState(viewState: ExampleViewStateNested){
        _viewState.postValue(viewState)
    }
    fun setSnackBar(snackBarState: SnackBarState?) {
        if(LOG_ME) ALog.d(
            TAG, "setSnackBar(): " +
                    "Setting snackBarStatus to ${snackBarState?.message}, " +
                    "visibility == ${snackBarState?.show}"
        )
        snackBarState?.updateSnackBarInViewModel = { status ->
            setSnackBar(status)
        }
        val update = getCurrentViewStateOrNew()
        update.snackBarState = snackBarState
        setViewState(update)
    }

    fun setToast(toastState: ToastState?) {
        if(LOG_ME) ALog.d(
            TAG, "setToast(): " +
                    "Setting toastStatus to ${toastState?.message}, " +
                    "visibility == ${toastState?.show}"
        )
        toastState?.updateToastInViewModel = { state ->
            setToast(state)
        }
        val update = getCurrentViewStateOrNew()
        update.toastState = toastState
        setViewState(update)
    }

    fun setSnackBarState(snackBarState: SnackBarState?) {
        if(LOG_ME) ALog.d(
            TAG, "setSnackBarState(): " +
                    "Setting snackBarStatus to ${snackBarState?.message}, " +
                    "visibility == ${snackBarState?.show}"
        )
        val updatedViewState = getViewStateCopy()
        updatedViewState.snackBarState = snackBarState?.apply {
            updateSnackBarInViewModel = { state ->
                setSnackBarState(state)
            }
        }
        this._viewState.value = updatedViewState
        setViewState(updatedViewState)
    }

    fun setDialogState(dialogState: DialogState?) {
        if(LOG_ME) ALog.d(
            TAG, "setDialogState(): " +
                    "Setting dialogState to ${dialogState?.title}, " +
                    "visibility == ${dialogState?.show}"
        )
        val updatedViewState = getViewStateCopy()
        updatedViewState.dialogState = dialogState?.apply {
            updateDialogInViewModel = { state ->
                setDialogState(state)
            }
        }
        setViewState(updatedViewState)
    }


    fun createNewViewState(): ExampleViewStateNested {
        if(LOG_ME)ALog.d(
            TAG, ".createNewViewState(): " +
                "Method start")
        val newInstance: ExampleViewStateNested = ExampleViewStateNested()
        newInstance.snackBarState = SnackBarState(
            updateSnackBarInViewModel = {
                setSnackBarState(it)
            }
        )
        newInstance.toastState = ToastState(
            updateToastInViewModel = {
                setToastState(it)
            }
        )
        newInstance.dialogState = DialogState(
            updateDialogInViewModel = {
                setDialogState(it)
            }
        )
        if(LOG_ME) {
            ALog.d(
                TAG, ".createNewViewState(): " +
                    "newInstance == $newInstance")
            ALog.d(
                TAG, ".createNewViewState(): " +
                    "Method end")
        }

        return newInstance
    }

    fun getViewStateCopy(): ExampleViewStateNested {
        return if(_viewState.value == null) {
            _viewState.value = createNewViewState()
            _viewState.value!!
        } else {
            _viewState.value!!.copy()
        }
    }
    
    fun setToastState(toastState: ToastState?) {
        val updatedViewState = getViewStateCopy()
        if(LOG_ME) ALog.d(
            TAG, "setToastState(): " +
                    "Setting toastState from \n${updatedViewState.toastState} " +
                    "to \n$toastState"
        )
        updatedViewState.toastState = toastState?.apply {
            updateToastInViewModel = { state ->
                setToastState(state)
            }
        }
//        if(LOG_ME)ALog.d(TAG, "setToastState(): " +
//                "Before change: _viewState.value == ${_viewState.value}")
        setViewState(updatedViewState)
//        if(LOG_ME)ALog.d(TAG, "setToastState(): " +
//                                "After change: _viewState.value == ${_viewState.value}")
    }

    fun setDialog(dialogState: DialogState?) {
        if(LOG_ME) ALog.d(
            TAG, "setDialog(): " +
                    "Setting dialogStatus to ${dialogState?.title}, " +
                    "visibility == ${dialogState?.show}"
        )
        dialogState?.updateDialogInViewModel = { status ->
            setDialog(status)
        }
        val update = getCurrentViewStateOrNew()
        update.dialogState = dialogState
        setViewState(update)
    }
}

@Parcelize
data class ExampleViewStateNested(
    var snackBarState: SnackBarState? = null,
    var toastState: ToastState? = null,
    var dialogState: DialogState? = null,
): Parcelable


//==============================================================================================
@Preview
@Composable
fun ExampleViewModelIntegrationNestedPreview() {
    CommonTheme {
        ExampleViewModelIntegrationNested(null)
    }
}