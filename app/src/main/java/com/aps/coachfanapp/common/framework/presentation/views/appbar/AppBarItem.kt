package com.aps.coachfanapp.common.framework.presentation.views.appbar

import androidx.annotation.StringRes
import androidx.compose.ui.graphics.vector.ImageVector

data class AppBarItem(
    @StringRes
    val nameRes: Int,
    val icon: ImageVector? = null,
    val showAppBarItemAsAction: ShowAppBarItemAsAction = ShowAppBarItemAsAction.IF_NECESSARY,
    val doAction: () -> Unit,
) {
    // allow 'calling' the action like a function
    operator fun invoke() = doAction()
}