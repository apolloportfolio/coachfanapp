package com.aps.coachfanapp.common.util.calendarviewhighlight

import android.content.Context
import android.graphics.Color
import android.text.style.ForegroundColorSpan
import com.prolificinteractive.materialcalendarview.DayViewFacade
import com.prolificinteractive.materialcalendarview.CalendarDay
import com.prolificinteractive.materialcalendarview.DayViewDecorator

// https://github.com/Thanvandh/Date-Range-Highlight/blob/master/app/src/main/java/info/androidramp/daterangehighlight/EventDecorator.java
class EventDecorator(context: Context, drawable: Int, calendarDays1: List<CalendarDay>) :
    DayViewDecorator {
    var context: Context = context
    private val drawable: Int = drawable
    private val dates: HashSet<CalendarDay> = HashSet(calendarDays1)
    override fun shouldDecorate(day: CalendarDay): Boolean {
        return dates.contains(day)
    }

    override fun decorate(view: DayViewFacade) {
        // apply drawable to dayView
        view.setSelectionDrawable(context.getResources().getDrawable(drawable))
        // white text color
        view.addSpan(ForegroundColorSpan(Color.WHITE))
    }

}