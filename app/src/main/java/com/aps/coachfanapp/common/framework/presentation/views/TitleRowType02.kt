package com.aps.coachfanapp.common.framework.presentation.views

import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.material.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.dimensionResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.aps.coachfanapp.R
import com.aps.coachfanapp.common.framework.presentation.values.CommonTheme
import com.aps.coachfanapp.common.util.composableutils.fallbackPainterResource

private const val TAG = "TitleRowType02"
private const val LOG_ME = true

@Composable
fun TitleRowType02(
    titleString: String?,
    subtitleString: String? = null,
    textAlign: TextAlign = TextAlign.Start,
    onClick: () -> Unit = {},
    composableBackground: DrawnBackground? = null,
    backgroundDrawableId: Int = R.drawable.list_item_example_background_1,
    titleFontSize: Int = 24,
    subtitleFontSize: Int = 18,
    leftPictureDrawableId: Int? = null,
    leftImageTint: Color = MaterialTheme.colors.onSurface,
    titleColor: Color = MaterialTheme.colors.onSurface,
    subtitleColor: Color = MaterialTheme.colors.onSurface,
) {
    TitleRowWithSubtitleAndPictures(
        titleString = titleString,
        subtitleString = subtitleString,
        textAlign = textAlign,
        onClick = onClick,
        leftPictureItemRef = null,
        rightPictureItemRef = null,
        leftPicturePainter =
            if(leftPictureDrawableId != null) {
                fallbackPainterResource(id = leftPictureDrawableId)
           } else { null },
        rightPicturePainter = null,
        composableBackground = composableBackground,
        backgroundDrawableId = backgroundDrawableId,
        cardPaddingValues = PaddingValues(
            start = dimensionResource(id = R.dimen.list_tile_horizontal_margin),
            end = dimensionResource(id = R.dimen.list_tile_horizontal_margin),
            top = dimensionResource(id = R.dimen.list_tile_vertical_margin),
            bottom = dimensionResource(id = R.dimen.list_tile_vertical_margin),
        ),
        internalPaddingValues = PaddingValues(4.dp, 3.dp, 4.dp, 3.dp),
        titleFontSize = titleFontSize,
        subtitleFontSize = subtitleFontSize,
        leftImageTint = leftImageTint,
        rightImageTint = MaterialTheme.colors.onSurface,
        titleColor = titleColor,
        subtitleColor = subtitleColor,
    )
}

@Preview
@Composable
fun TitleRowType02Preview() {
    CommonTheme {
        TitleRowType02(
            titleString = "The Title",
            subtitleString = "The Subtitle",
            textAlign = TextAlign.Start,
            onClick = {},
            backgroundDrawableId = R.drawable.list_item_example_background_1,
            titleFontSize = 24,
            subtitleFontSize = 18,
            leftPictureDrawableId = R.drawable.ic_launcher,
        )
    }
}