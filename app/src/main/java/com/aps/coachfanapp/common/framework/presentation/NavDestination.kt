package com.aps.coachfanapp.common.framework.presentation

interface NavDestination {
    val route: String
}