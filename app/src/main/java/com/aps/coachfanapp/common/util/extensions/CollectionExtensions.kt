package com.aps.coachfanapp.common.util.extensions

fun Collection<*>?.getSizeString(): String {
    return this?.size.toString() ?: "null"
}
