package com.aps.coachfanapp.common.framework.presentation.views

import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.ExperimentalLayoutApi
import androidx.compose.foundation.layout.FlowRow
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.CornerSize
import androidx.compose.material.Chip
import androidx.compose.material.ChipDefaults
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.aps.coachfanapp.R
import com.aps.coachfanapp.common.framework.presentation.values.CommonTheme

@OptIn(ExperimentalLayoutApi::class, ExperimentalMaterialApi::class)
@Composable
fun TagsRowType01(
    tagsInOneString: String,
    composableBackground: DrawnBackground? = null,
    backgroundDrawableId: Int = R.drawable.list_item_example_background_1,
    modifier: Modifier = Modifier,
) {
//    BoxWithBackground(
//        backgroundDrawableId = backgroundDrawableId,
//        composableBackground = composableBackground,
//    ) {
        val paddingValues = remember {
            PaddingValues(start = 2.dp, top = 4.dp, end = 2.dp, bottom = 4.dp)
        }
        val h1 = remember {
            TextStyle(
                fontFamily = FontFamily.Default,
                fontWeight = FontWeight.Bold,
                fontSize = 16.sp
            )
        }


        FlowRow(
            modifier = modifier
                .padding(paddingValues)
                .fillMaxWidth()
                .testTag("TagsRow"),
            horizontalArrangement = Arrangement.Center
        ) {
            val tagsSplit = tagsInOneString.split(", ")
            tagsSplit.forEachIndexed { index, tag ->
                Chip(
                    onClick = {},
                    modifier = Modifier.padding(paddingValues),
                    enabled = true,
                    interactionSource = remember { MutableInteractionSource() },
                    shape = MaterialTheme.shapes.small.copy(CornerSize(percent = 50)),
                    border = null,
                    colors = ChipDefaults.chipColors(),
                    leadingIcon = null,
                ) {
                    Text(
                        text = tag,
                        style = h1,
                        color = MaterialTheme.colors.primary,
                        modifier = Modifier
                            .padding(paddingValues),
                    )

                }
            }
        }
//    }
}

@Preview
@Composable
private fun TagsRowType01Preview1() {
    CommonTheme {
        val tagsList = listOf("communication", "leadership", "teamwork")
        TagsRowType01(
            modifier = Modifier,
            tagsInOneString = tagsList.joinToString(","),
        )
    }
}