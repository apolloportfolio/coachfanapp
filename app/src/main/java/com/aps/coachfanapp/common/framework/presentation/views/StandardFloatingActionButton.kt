package com.aps.coachfanapp.common.framework.presentation.views

import androidx.compose.foundation.Image
import androidx.compose.material.FloatingActionButton
import androidx.compose.runtime.Composable
import androidx.compose.ui.layout.ContentScale
import com.aps.coachfanapp.common.util.ALog
import com.aps.coachfanapp.common.util.composableutils.fallbackPainterResource

private const val TAG = "StandardFloatingActionButton"
private const val LOG_ME = true

@Composable
fun StandardFloatingActionButton(
    floatingActionButtonDrawableId: Int?,
    floatingActionButtonOnClick: (() -> Unit)?,
    contentDescription: String?,
): (@Composable () -> Unit)? {
    if(floatingActionButtonDrawableId != null) {
        return {
            FloatingActionButton(
                onClick = floatingActionButtonOnClick ?: {
                    ALog.w(
                        "StandardFloatingActionButton", ".floatingActionButtonOnClick(): " +
                                "Button clicked but onClick() not implemented"
                    )
                },
            ) {
                Image (
                    painter = fallbackPainterResource(id = floatingActionButtonDrawableId),
                    contentDescription = contentDescription,
                    contentScale = ContentScale.FillBounds
                )
            }
        }
    } else {
        if(LOG_ME)ALog.d(TAG, ": " +
                                "floatingActionButtonDrawableId == null")
        return null
    }
}