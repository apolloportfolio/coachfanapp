package com.aps.coachfanapp.common.framework.presentation.views

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Button
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.aps.coachfanapp.R
import com.aps.coachfanapp.common.util.ALog

private const val TAG = "HelloWorldComposable"
private const val LOG_ME = true
@Composable
fun HelloWorldComposable(onClick: () -> Unit = {}) {
    BoxWithBackground(
        backgroundDrawableId = R.drawable.example_background_1
    ) {
        HelloWorldContent(onClick)
    }
}

@Composable
internal fun HelloWorldContent(onClick: () -> Unit = {}) {
    LaunchedEffect(Unit) {
        if(LOG_ME)ALog.w(TAG,
            ": Created")
    }
    Column(
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Center
    ) {
        Text(
            text = "Hello World, Jetpack Compose!",
            style = MaterialTheme.typography.h5,
            modifier = Modifier.padding(16.dp)
        )

        Button(onClick = onClick) {
            Text(text = "Click me please.")
        }
    }
}

// Preview =========================================================================================
@Preview
@Composable
internal fun HelloWorldComposablePreview() {
    HelloWorldComposable()
}