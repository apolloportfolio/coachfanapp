package com.aps.coachfanapp.common.util.extensions


import com.aps.coachfanapp.common.util.ALog
import com.ceylonlabs.imageviewpopup.ImagePopup
import com.google.firebase.storage.StorageReference

private const val TAG = "ImagePopupExtensions"
private const val LOG_ME = true

fun ImagePopup.initiatePopupWithGlideAndFirestore(imageRef : StorageReference) {
    val methodName: String = "initiatePopupWithGlideAndFirestore"
    if (LOG_ME) ALog.d(TAG, "Method start: $methodName")
    try {
        this.initiatePopupWithGlide(imageRef.path)
    } catch (e: Exception) {
        ALog.e(TAG, methodName, e)
    } finally {
        if (LOG_ME) ALog.d(TAG, "Method end: $methodName")
    }
}