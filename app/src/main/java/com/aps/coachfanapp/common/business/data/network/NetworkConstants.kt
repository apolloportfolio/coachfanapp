package com.aps.coachfanapp.common.business.data.network

object NetworkConstants {

    const val NETWORK_TIMEOUT = 30000L
}