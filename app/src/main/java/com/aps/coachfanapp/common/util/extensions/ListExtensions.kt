package com.aps.coachfanapp.common.util.extensions

fun <T> List<T>?.convertToString(): String {
    if (this == null) {
        return "null"
    }

    if(this.isEmpty())return "Empty"

    val stringBuilder = StringBuilder()
    for (item in this) {
        stringBuilder.append(item.toString())
        stringBuilder.append("\n")
    }
    return stringBuilder.toString()
}