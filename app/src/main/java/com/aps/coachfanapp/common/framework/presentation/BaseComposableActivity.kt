package com.aps.coachfanapp.common.framework.presentation

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.navigation.ui.AppBarConfiguration
import com.afollestad.materialdialogs.MaterialDialog
import com.aps.coachfanapp.common.business.domain.state.*
import com.aps.coachfanapp.common.util.ALog
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview

private const val TAG = "BaseComposableActivity"
private const val LOG_ME = true

abstract class BaseComposableActivity : ComponentActivity() {
    var appBarConfiguration: AppBarConfiguration? = null
    var dialogInView: MaterialDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onStart() {
        super.onStart()

    }

    override fun onResume() {
        super.onResume()

    }

    override fun onPause() {
        super.onPause()

    }

    override fun onStop() {
        super.onStop()

    }

    override fun onRestart() {
        super.onRestart()

    }

    override fun onDestroy() {
        super.onDestroy()

    }


    /* Legacy code for Views
    override fun onResponseReceived(
        response: Response,
        stateMessageCallback: StateMessageCallback
    ) {
        val methodName: String = "onResponseReceived"
        when(response.uiComponentType){

            is UIComponentType.SnackBar -> {
                val messageId: Int? = response.messageId
                val onDismissCallback: TodoCallback? = response.uiComponentType.onDismissCallback
                val actionTextId: Int = response.uiComponentType.actionTextId
                val actionTextString: String? = response.uiComponentType.actionTextString;
                val actionOnClickListener: View.OnClickListener = response.uiComponentType.actionOnClickListener;
                response.message?.let { messageText ->
                    if(LOG_ME) ALog.d(
                        TAG,
                        "$methodName(): Displaying SnackBar with messageText = $messageText and actionTextString = $actionTextString"
                    )
                    displaySnackBar(
                        isDisplayed = true,
                        messageId = messageId,
                        messageText = messageText,
                        actionTextId = actionTextId,
                        actionTextString = actionTextString,
                        actionOnClickListener = actionOnClickListener,
                        onDismissCallback = onDismissCallback,
                        stateMessageCallback = stateMessageCallback,
                        dismissSnackbarOnClick = true
                    )
                }
            }

            is UIComponentType.AreYouSureDialog -> {
                response.message?.let {
                    if(LOG_ME) ALog.d(
                        TAG,
                        "$methodName(): Displaying AreYouSureDialog with message = $it"
                    )
                    dialogInView = displayAreYouSureDialog(
                        title = response.uiComponentType.titleID,
                        message = it,
                        callback = response.uiComponentType.callback,
                        stateMessageCallback = stateMessageCallback
                    )
                }
            }

            is UIComponentType.Toast -> {
                response.message?.let {
                    if(LOG_ME) ALog.d(TAG, "$methodName(): Displaying Toast with message = $it")
                    displayToast(
                        message = it,
                        stateMessageCallback = stateMessageCallback
                    )
                }
            }

            is UIComponentType.Dialog -> {
                if(LOG_ME) ALog.d(TAG, "$methodName(): Displaying Dialog")
                displayDialog(
                    title = response.uiComponentType.titleID,
                    response = response,
                    stateMessageCallback = stateMessageCallback,
                    okButtonCallback = response.uiComponentType.okButtonCallback,
                )
            }

            is UIComponentType.None -> {
                if(LOG_ME) ALog.d(
                    TAG,
                    "$methodName(): Displaying None with message ${response.message}"
                )
                stateMessageCallback.removeMessageFromStack()
            }
        }
    }

    @Deprecated("Unused with Jetpack Compose.")
    override fun displaySnackBar(
        isDisplayed: Boolean,
        messageId: Int?,
        messageText: String?,
        actionTextId: Int,
        actionTextString: String?,
        actionOnClickListener: View.OnClickListener,
        onDismissCallback: TodoCallback?,
        stateMessageCallback: StateMessageCallback,
        anchorViewId: Int?,
        dismissSnackbarOnClick: Boolean,
    ) {

    }

    private fun displayDialog(
        title: Int?,
        response: Response,
        stateMessageCallback: StateMessageCallback,
        okButtonCallback: OkButtonCallback?,
    ){
        response.message?.let { message ->

            dialogInView = when (response.messageType) {

                is MessageType.Error -> {
                    displayErrorDialog(
                            title = title,
                            message = message,
                            stateMessageCallback = stateMessageCallback,
                            okButtonCallback = okButtonCallback,
                    )
                }

                is MessageType.Success -> {
                    displaySuccessDialog(
                            title = title,
                            message = message,
                            stateMessageCallback = stateMessageCallback,
                            okButtonCallback = okButtonCallback,
                    )
                }

                is MessageType.Info -> {
                    displayInfoDialog(
                            title = title,
                            message = message,
                            stateMessageCallback = stateMessageCallback,
                            okButtonCallback = okButtonCallback,
                    )
                }

                else -> {
                    // do nothing
                    stateMessageCallback.removeMessageFromStack()
                    null
                }
            }
        }?: stateMessageCallback.removeMessageFromStack()
    }

    override fun hideSoftKeyboard() {
        if (currentFocus != null) {
            val inputMethodManager = getSystemService(
                INPUT_METHOD_SERVICE
            ) as InputMethodManager
            inputMethodManager
                    .hideSoftInputFromWindow(currentFocus!!.windowToken, 0)
        }
    }

    private fun displaySuccessDialog(
        title: Int?,
        message: String?,
        stateMessageCallback: StateMessageCallback,
        okButtonCallback: OkButtonCallback?,
    ): MaterialDialog {
        return MaterialDialog(this)
                .show{
                    title(title ?: R.string.text_success)
                    message(text = message)
                    positiveButton(R.string.text_ok){
                        stateMessageCallback.removeMessageFromStack()
                        dismiss()
                        okButtonCallback?.proceed()
                    }
                    onDismiss {
                        dialogInView = null
                    }
                    cancelable(false)
                }
    }

    private fun displayErrorDialog(
        title: Int?,
        message: String?,
        stateMessageCallback: StateMessageCallback,
        okButtonCallback: OkButtonCallback?,
    ): MaterialDialog {
        return MaterialDialog(this)
                .show{
                    title(title ?: R.string.error)
                    message(text = message)
                    positiveButton(R.string.text_ok){
                        stateMessageCallback.removeMessageFromStack()
                        dismiss()
                        okButtonCallback?.proceed()
                    }
                    onDismiss {
                        dialogInView = null
                    }
                    cancelable(false)
                }
    }

    private fun displayInfoDialog(
        title: Int?,
        message: String?,
        stateMessageCallback: StateMessageCallback,
        okButtonCallback: OkButtonCallback?,
    ): MaterialDialog {
        return MaterialDialog(this)
                .show{
                    title(title ?: R.string.text_info)
                    message(text = message)
                    positiveButton(R.string.text_ok){
                        stateMessageCallback.removeMessageFromStack()
                        dismiss()
                        okButtonCallback?.proceed()
                    }
                    onDismiss {
                        dialogInView = null
                    }
                    cancelable(false)
                }
    }

    private fun displayAreYouSureDialog(
        title: Int,
        message: String,
        callback: AreYouSureCallback,
        stateMessageCallback: StateMessageCallback
    ): MaterialDialog {
        return MaterialDialog(this)
                .show{
                    title(title)
                    message(text = message)
                    negativeButton(R.string.text_cancel){
                        stateMessageCallback.removeMessageFromStack()
                        callback.cancel()
                        dismiss()
                    }
                    positiveButton(R.string.text_yes){
                        stateMessageCallback.removeMessageFromStack()
                        callback.proceed()
                        dismiss()
                    }
                    onDismiss {
                        dialogInView = null
                    }
                    cancelable(false)
                }
    }

    override fun displayProgressBar(isDisplayed: Boolean) {
        val mainProgressBar = findViewById<ProgressBar>(R.id.main_progress_bar)
        if(isDisplayed){
            mainProgressBar.visible()
            if(LOG_ME) ALog.d(TAG, "displayProgressBar(): Displaying main_progress_bar")
        } else {
            mainProgressBar.gone()
            if (LOG_ME) ALog.d(TAG, "displayProgressBar(): Hiding main_progress_bar")
        }
    }
    */

    // Method opens bundle provided as a parameter in onCreate() and then takes main
    // entity from it to set it in view model.
    @ExperimentalCoroutinesApi
    @FlowPreview
    fun <EntityType, EntityViewState : ViewState<EntityType>> setMainEntityInViewModelFromBundle(
        viewModel : BaseViewModelCompose<EntityType, EntityViewState>,
        mainEntityTag : String,
    ) {
        val methodName: String = "setMainEntityInViewModelFromBundle"
        if (LOG_ME) ALog.d(TAG, "Method start: $methodName")
        try {
            val bundle: Bundle? = intent.extras
            bundle?.let {
                if(LOG_ME) ALog.d(TAG, ".$methodName(): bundle != null")
                if(LOG_ME) ALog.d(
                    TAG, ".$methodName(): " +
                            "bundle == $bundle"
                )
                val entity : EntityType? = it.get(mainEntityTag) as EntityType?
                if(entity == null)if(LOG_ME) ALog.w(
                    TAG, ".setMainEntityInViewModelFromBundle(): " +
                            "entity == null"
                )
                entity?.let { mainEntity ->
                    if(LOG_ME) ALog.d(TAG, ".$methodName(): mainEntity != null")
                    viewModel.setMainEntity(mainEntity)
                }
            }
        } catch (e: Exception) {
            ALog.e(TAG, methodName, e)
        } finally {
            if (LOG_ME) ALog.d(TAG, "Method end: $methodName")
        }
    }
}