package com.aps.coachfanapp.common.util

import android.annotation.TargetApi
import android.os.Parcel
import java.io.ByteArrayOutputStream
import java.io.ObjectOutputStream
import java.io.Serializable

private const val TAG = "ObjectSizeCalculator"
private const val LOG_ME = true

@TargetApi(26)
object ObjectSizeCalculator {
    private fun getFirstObjectReference(o: Any): Any? {
        val objectType = o.javaClass.typeName

        if (objectType.substring(objectType.length - 2) == "[]") {
            try {
                return if (objectType == "java.lang.Object[]")
                    (o as Array<Any>)[0]
                else if (objectType == "int[]")
                    (o as IntArray)[0]
                else
                    throw RuntimeException("Not Implemented !")
            } catch (e: IndexOutOfBoundsException) {
                return null
            }

        }

        return o
    }

    // https://stackoverflow.com/questions/45318157/bundle-size-in-bytes
    fun getBundleSizeInBytes(o: Any?): Int {
        if (LOG_ME) ALog.d(TAG, "Method start: getBundleSizerInBytes()")
        try {
            val parcel = Parcel.obtain()
            parcel.writeValue(o)

            val bytes = parcel.marshall()
            parcel.recycle()

            return bytes.size
        } catch (e: Exception) {
            ALog.e(TAG, "getBundleSizerInBytes()", e)
        } finally {
            if (LOG_ME) ALog.d(TAG, "Method end: getBundleSizerInBytes()")
        }
        return -1
    }

    fun getObjectSizeInBytes(o: Any?): Int {
        val STRING_JAVA_TYPE_NAME = "java.lang.String"

        if (o == null)
            return 0

        val objectType = o.javaClass.typeName
        val isArray = objectType.substring(objectType.length - 2) == "[]"

        val objRef = getFirstObjectReference(o)
        if (objRef != null && objRef !is Serializable) {
            var message = "Object must be serializable for measuring it's memory footprint using this method !" +
                "\n objRef(${objRef.javaClass.simpleName}) == \n $objRef"

            if(LOG_ME)ALog.d(TAG, ".getObjectSizeInBytes(): $message")
            return 0
        //            throw RuntimeException(message)
        }

        try {
            val baos = ByteArrayOutputStream()
            val oos = ObjectOutputStream(baos)
            oos.writeObject(o)
            oos.close()
            val bytes = baos.toByteArray()

            var i = bytes.size - 1
            var j = 0
            while (i != 0) {
                if (objectType !== STRING_JAVA_TYPE_NAME) {
                    if (bytes[i].toInt() == 112)
                        return if (isArray)
                            j - 4
                        else
                            j
                } else {
                    if (bytes[i].toInt() == 0)
                        return j - 1
                }
                i--
                j++
            }
        } catch (e: Exception) {
            if(LOG_ME)ALog.e(TAG, ".getObjectSizeInBytes(): " +
                                    "Exception:", e)
            return -1
        }

        return -1
    }

}