package com.aps.coachfanapp.common.framework.presentation

import android.view.View
import com.aps.coachfanapp.common.business.domain.state.DialogInputCaptureCallback
import com.aps.coachfanapp.common.business.domain.state.Response
import com.aps.coachfanapp.common.business.domain.state.StateMessageCallback
import com.aps.coachfanapp.common.util.TodoCallback


interface UIController {

    fun displayProgressBar(isDisplayed: Boolean)

    fun displaySnackBar(
        isDisplayed: Boolean,
        messageId: Int?,
        messageText: String?,
        actionTextId: Int,
        actionTextString: String?,
        actionOnClickListener: View.OnClickListener,
        onDismissCallback: TodoCallback?,
        stateMessageCallback: StateMessageCallback,
        anchorViewId: Int? = null,
        dismissSnackbarOnClick: Boolean = true,
    )

    fun hideSoftKeyboard()

    fun displayInputCaptureDialog(title: String, callback: DialogInputCaptureCallback)

    fun onResponseReceived(
        response: Response,
        stateMessageCallback: StateMessageCallback
    )

}


















