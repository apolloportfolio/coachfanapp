package com.aps.coachfanapp.common.framework.presentation.views.appbar

import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material.icons.filled.Settings
import androidx.compose.material.icons.filled.Share
import androidx.compose.material3.CenterAlignedTopAppBar
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.material3.rememberTopAppBarState
import androidx.compose.runtime.Composable
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import com.aps.coachfanapp.R
import com.aps.coachfanapp.common.framework.presentation.values.CommonTheme
import com.aps.coachfanapp.common.framework.presentation.views.BoxWithBackground

// References:
// https://developer.android.com/jetpack/compose/components/app-bars
// https://foso.github.io/Jetpack-Compose-Playground/material/topappbar/
// https://stackoverflow.com/questions/65373652/how-can-toolbar-with-overflow-menu-be-created-with-jetpack-compose
// https://fvilarino.medium.com/creating-a-reusable-actions-menu-in-jetpack-compose-95aec8eeb493
@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun StandardAppBar(
    titleId: Int?,
    navigateBack: (() -> Unit)? = null,
    appBarItems: List<AppBarItem>,
) {
    BoxWithBackground(
        backgroundDrawableId = R.drawable.example_background_1
    ) {
//        Text(
//            text = "BoxWithBackground"
//        )
        val titleString = if(titleId != null) {
            stringResource(titleId)
        } else {
            ""
        }
        val navigationIcon: @Composable () -> Unit = if(navigateBack != null) {
            {
                IconButton(onClick = navigateBack) {
                    Icon(
                        imageVector = Icons.Filled.ArrowBack,
                        contentDescription = "Localized description",
                        tint = MaterialTheme.colors.primary,
                    )
                }
            }
        } else {
            {}
        }
        val scrollBehavior = TopAppBarDefaults.pinnedScrollBehavior(rememberTopAppBarState())

        CenterAlignedTopAppBar(
            colors = TopAppBarDefaults.topAppBarColors(
                containerColor = Color.Transparent,
                titleContentColor = MaterialTheme.colors.primary,
            ),
            title = {
                Text(
                    titleString,
                    maxLines = 1,
                    overflow = TextOverflow.Ellipsis,
                    color = MaterialTheme.colors.primary,
                )
            },
            navigationIcon = navigationIcon,
            actions = {
                TraditionalActionMenu(appBarItems)
            },
            scrollBehavior = scrollBehavior,
        )
    }
}

// Preview =========================================================================================
@Preview
@Composable
fun HomeScreenAppBarPreview() {
    CommonTheme {
        val appBarItems: MutableList<AppBarItem> = mutableListOf()

        appBarItems.add(
            AppBarItem(
                nameRes = R.string.share_app,
                icon = Icons.Filled.Share,
                showAppBarItemAsAction = ShowAppBarItemAsAction.NEVER_OVERFLOW,
                doAction = {},
            )
        )

        appBarItems.add(
            AppBarItem(
                nameRes = R.string.settings,
                icon = Icons.Filled.Settings,
                showAppBarItemAsAction = ShowAppBarItemAsAction.IF_NECESSARY,
                doAction = {},
            )
        )

        StandardAppBar(
            R.string.settings,
            {},
            appBarItems,
        )
    }
}