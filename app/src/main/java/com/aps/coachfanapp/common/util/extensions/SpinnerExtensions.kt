package com.aps.coachfanapp.common.util.extensions

import com.aps.coachfanapp.common.util.ALog


private const val TAG = "SpinnerExtensions"
private const val LOG_ME = true

fun androidx.appcompat.widget.AppCompatSpinner.setSelection(value: String) {
    val methodName: String = "setSelection"
    if (LOG_ME) ALog.d(TAG, "Method start: $methodName: value == $value")
    try {
        getIndex(value)?.let { this.setSelection(it) }
    } catch (e: Exception) {
        ALog.e(TAG, methodName, e)
    } finally {
        if (LOG_ME) ALog.d(TAG, "Method end: $methodName")
    }
}

fun androidx.appcompat.widget.AppCompatSpinner.getIndex(value: String): Int? {
    val methodName: String = "getIndex"
    if (LOG_ME) ALog.d(TAG, "Method start: $methodName: value == $value")
    try {
        for(i in 0 until this.count) {
            if(this.getItemAtPosition(i).toString().equals(value, false)) {
                return i
            }
        }
    } catch (e: Exception) {
        ALog.e(TAG, methodName, e)
    } finally {
        if (LOG_ME) ALog.d(TAG, "Method end: $methodName")
    }
    return null
}
