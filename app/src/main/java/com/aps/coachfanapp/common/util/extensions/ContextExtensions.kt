package com.aps.coachfanapp.common.util.extensions

import android.app.Activity
import android.content.ClipData
import android.content.Context
import android.content.Intent
import android.content.pm.LabeledIntent
import android.content.pm.PackageManager
import android.content.pm.ResolveInfo
import android.net.Uri
import android.os.Build
import android.text.ClipboardManager
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.fragment.app.Fragment




// Method opens an app that is an email client, on it's mailbox
// https://stackoverflow.com/questions/8099017/how-to-open-the-default-mail-inbox-from-android-code/69043615#69043615
fun Context.openMailbox(
    chooserTitle: String,
    showNoEmailClientMessage : () -> Unit,
) {
    val emailIntent = Intent(Intent.ACTION_VIEW, Uri.parse("mailto:"))

    val resInfo = packageManager.queryIntentActivities(emailIntent, 0)
    if (resInfo.isNotEmpty()) {
        // First create an intent with only the package name of the first registered email app
        // and build a picked based on it
        val intentChooser = packageManager.getLaunchIntentForPackage(
            resInfo.first().activityInfo.packageName
        )
        val openInChooser = Intent.createChooser(intentChooser, chooserTitle)

        // Then create a list of LabeledIntent for the rest of the registered email apps
        val emailApps = resInfo.toLabeledIntentArray(packageManager)

        // Add the rest of the email apps to the picker selection
        openInChooser.putExtra(Intent.EXTRA_INITIAL_INTENTS, emailApps)
        startActivity(openInChooser)
    } else {
        showNoEmailClientMessage.invoke()
    }
}
private fun List<ResolveInfo>.toLabeledIntentArray(packageManager: PackageManager): Array<LabeledIntent> = map {
    val packageName = it.activityInfo.packageName
    val intent = packageManager.getLaunchIntentForPackage(packageName)
    LabeledIntent(intent, packageName, it.loadLabel(packageManager), it.icon)
}.toTypedArray()




fun Context.copyTextToClipboard(text: String) {
    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB) {
        val clipboard = getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
        clipboard.text = text
    } else {
        val clipboard =
            getSystemService(Context.CLIPBOARD_SERVICE) as android.content.ClipboardManager
        val clip = ClipData.newPlainText("Copied Text", text)
        clipboard.setPrimaryClip(clip)
    }
}

fun Context.shareTextWithOtherApp(text : String, chooserTitle : String) {
    val shareIntent = Intent()
    shareIntent.action = Intent.ACTION_SEND
    shareIntent.type="text/plain"
    shareIntent.putExtra(Intent.EXTRA_TEXT, text);
    startActivity(Intent.createChooser(shareIntent, chooserTitle))
}


fun Context.showToast(text : String) {
    Toast.makeText(
        this,
        text,
        Toast.LENGTH_LONG
    ).show()
}

fun Context.showToast(textId : Int) {
    Toast.makeText(
        this,
        getString(textId),
        Toast.LENGTH_LONG
    ).show()
}

fun Fragment.hideKeyboard() {
    view?.let { activity?.hideKeyboard(it) }
}

fun Activity.hideKeyboard() {
    hideKeyboard(currentFocus ?: View(this))
}

fun Context.hideKeyboard(view: View) {
    val inputMethodManager = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
    inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
}

// Reference: https://stackoverflow.com/questions/11229219/android-how-to-get-application-name-not-package-name
fun Context.getApplicationName(): String? {
    val applicationInfo = this.applicationInfo
    val stringId = applicationInfo.labelRes
    return if (stringId == 0) applicationInfo.nonLocalizedLabel.toString() else this.getString(
        stringId
    )
}