package com.aps.coachfanapp.common.util.extensions

import com.aps.coachfanapp.common.util.ALog
import com.google.android.material.slider.RangeSlider
import java.lang.IllegalStateException
import java.math.BigDecimal
import java.math.MathContext

private const val TAG = "RangeSliderExtensions"
private const val LOG_ME = false
private const val THRESHOLD = .0001         // Must be equal to private field BaseSlider.THRESHOLD

fun RangeSlider.valueLeft() = values[0]
fun RangeSlider.valueRight() = values[1]

fun RangeSlider.smartSetLeftValue(value : Float?) {
    val methodName: String = "smartSetLeftValue()"
    if (LOG_ME) ALog.d(TAG, "Method start: $methodName")
    try {
        if(LOG_ME) ALog.d(
            TAG, ".$methodName(): " +
                "value == $value    " +
                "valueFrom == $valueFrom    " +
                "valueTo == $valueTo    " +
                "stepSize == $stepSize")
        if(value == null) throw IllegalStateException("$TAG.$methodName: value == null")
        else if(value < valueFrom) {
            throw IllegalStateException("$TAG.$methodName: value < valueFrom")
        } else if(value > valueTo) {
            throw IllegalStateException("$TAG.$methodName: value > valueTo")
        } else {
            if(stepSize > 0.0f && !valueLandsOnTickP(value)) {
                val valueToSet = makeValueLandOnTick(value)
                if(LOG_ME) ALog.d(TAG, ".$methodName(): Changing $value to $valueToSet and setting it.")
                valueToSet?.let { setRightValue(it) }
            } else {
                if(LOG_ME) ALog.d(TAG, ".$methodName(): Setting value to $value")
                setLeftValue(value)
            }
        }
    } catch (e: Exception) {
        ALog.e(TAG, methodName, e)
    } finally {
        if (LOG_ME) ALog.d(TAG, "Method end: $methodName")
    }
}

fun RangeSlider.smartSetRightValue(value : Float?) {
    val methodName: String = "smartSetRightValue()"
    if (LOG_ME) ALog.d(TAG, "Method start: $methodName")
    try {
        if(LOG_ME) ALog.d(
            TAG, ".$methodName(): " +
                "value == $value    " +
                "valueFrom == $valueFrom    " +
                "valueTo == $valueTo    +" +
                "stepSize == $stepSize")
        if(value == null) throw IllegalStateException("$TAG.$methodName: value == null")
        else if(value < valueFrom) {
            throw IllegalStateException("$TAG.$methodName: value < valueFrom")
        } else if(value > valueTo) {
            throw IllegalStateException("$TAG.$methodName: value > valueTo")
        } else {
            if(stepSize > 0.0f && !valueLandsOnTickP(value)) {
                val valueToSet = makeValueLandOnTick(value)
                if(LOG_ME) ALog.d(TAG, ".$methodName(): Changing $value to $valueToSet and setting it.")
                valueToSet?.let { setRightValue(it) }
            } else {
                setRightValue(value)
            }
        }
    } catch (e: Exception) {
        ALog.e(TAG, methodName, e)
    } finally {
        if (LOG_ME) ALog.d(TAG, "Method end: $methodName")
    }
}

fun RangeSlider.setLeftValue(value : Float) {
    setValues(value, values[1])
}

fun RangeSlider.setRightValue(value : Float) {
    setValues(values[0], value)
}

private fun RangeSlider.valueLandsOnTickP(value: Float): Boolean {
    // Check that the value is a multiple of stepSize given the offset of valueFrom
    // We're using BigDecimal here to avoid floating point rounding errors.
    val potentialTickValue = BigDecimal(java.lang.Float.toString(value))
        .subtract(BigDecimal(java.lang.Float.toString(valueFrom)))
        .divide(BigDecimal(java.lang.Float.toString(stepSize)), MathContext.DECIMAL64)
        .toDouble()

    // If the potentialTickValue is a whole number, it means the value lands on a tick.
    return Math.abs(Math.round(potentialTickValue) - potentialTickValue) < THRESHOLD
}

private fun RangeSlider.makeValueLandOnTick(value : Float) : Float? {
    val methodName: String = "makeValueLandOnTick()"
    if (LOG_ME) ALog.d(TAG, "Method start: $methodName")
    var result : Float? = null
    try {
        val valueInt = value.toInt()
        val stepSizeInt = stepSize.toInt()
        val modulo = valueInt % stepSizeInt
        var resultInt = stepSizeInt*(valueInt/stepSizeInt)
        if(LOG_ME) ALog.d(
            TAG, ".$methodName(): " +
                "valueInt == $valueInt    " +
                "stepSizeInt == $stepSizeInt    " +
                "modulo == $modulo    +" +
                "resultInt == $resultInt")
        if(modulo >= stepSize/2) {
            resultInt += stepSizeInt
            if(LOG_ME) ALog.d(TAG, ".$methodName(): modulo is bigger than half of step size: resultInt == $resultInt")
        }
        if(resultInt < valueFrom){
            resultInt += stepSizeInt
            if(LOG_ME) ALog.d(TAG, ".$methodName(): resultInt was smaller than valueFrom: resultInt == $resultInt")
        }
        if(resultInt > valueTo){
            resultInt -= stepSizeInt
            if(LOG_ME) ALog.d(TAG, ".$methodName(): resultInt was bigger than valueTo: resultInt == $resultInt")
        }
        if(resultInt >= valueFrom && resultInt <= valueTo) {
            result = resultInt.toFloat()
            if(LOG_ME) ALog.d(TAG, ".$methodName(): resultInt is within bounds returning result == $result")
        } else {
            if (LOG_ME) ALog.d(TAG, ".$methodName(): resultInt is within bounds returning null")
        }
    } catch (e: Exception) {
        ALog.e(TAG, methodName, e)
    } finally {
        if (LOG_ME) ALog.d(TAG, "Method end: $methodName")
    }
    return result
}