package com.aps.coachfanapp.common.framework.presentation

interface PermissionMediaProvider {
    fun getDescriptionID(isPermanentlyDeclined: Boolean): Int

    fun getDialogTitleID(isPermanentlyDeclined: Boolean): Int

    fun getImageID(isPermanentlyDeclined: Boolean): Int

    fun getPermissionName(): String
}