package com.aps.coachfanapp.common.framework.presentation.views

import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Switch
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.res.dimensionResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.semantics.contentDescription
import androidx.compose.ui.semantics.semantics
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.aps.coachfanapp.R
import com.aps.coachfanapp.common.framework.presentation.values.CommonTheme

const val SWITCH_TEST_TAG = "SWITCH_TEST_TAG"
@Composable
fun RowWithTextAndSwitch(
    textID: Int,
    switchState: Boolean,
    onSwitchStateChanged: (Boolean) -> Unit,
) {
    Row(
        modifier = Modifier
            .fillMaxWidth()
            .padding(
                start = dimensionResource(R.dimen.button_vertical_margin),
                top = dimensionResource(R.dimen.rounded_corner_margin),
                end = dimensionResource(R.dimen.button_horizontal_margin),
                bottom = dimensionResource(R.dimen.button_vertical_margin),
            ),
        verticalAlignment = Alignment.CenterVertically
    ) {
        val switchName = stringResource(textID)
        var isSwitchOn by remember { mutableStateOf(false) }
        val on = stringResource(id = androidx.compose.ui.R.string.on)
        val off = stringResource(id = androidx.compose.ui.R.string.off)
        val getSwitchStateAsString: (Boolean) -> String = { isOn ->
            if (isOn) on else off
        }
        Text(
            text = switchName,
            modifier = Modifier
                .weight(1f)
                .semantics {
                    contentDescription = switchName
                },
        )

        Switch(
            checked = switchState,
            onCheckedChange = {
                onSwitchStateChanged(it)
                isSwitchOn = it
            },
            modifier = Modifier
                .testTag(SWITCH_TEST_TAG + switchName)
                .padding(start = 8.dp)
                .semantics {
                    contentDescription = switchName + " " + getSwitchStateAsString(isSwitchOn)
                },
        )
    }
}

@Preview
@Composable
fun RowWithTextAndSwitchPreview() {
    CommonTheme {
        RowWithTextAndSwitch(
            textID = R.string.switch_1,
            switchState = false,
            onSwitchStateChanged = {},
        )
    }
}