package com.aps.coachfanapp.common.business.domain.state

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.aps.coachfanapp.common.util.ALog

private const val TAG = "StateEventManager"
private const val LOG_ME = true

/**
 * - Keeps track of active StateEvents in DataChannelManager
 * - Keeps track of whether the progress bar should show or not based on a boolean
 *      value in each StateEvent (shouldDisplayProgressBar)
 */
class StateEventManager {

    private val activeStateEvents: HashMap<String, StateEvent> = HashMap()

    private val _shouldDisplayProgressBar: MutableLiveData<Boolean> = MutableLiveData()

    val shouldDisplayProgressBar: LiveData<Boolean>
            get() = _shouldDisplayProgressBar

    fun getActiveJobNames(): MutableSet<String>{
        return activeStateEvents.keys
    }

    fun clearActiveStateEventCounter(){
        if(LOG_ME) ALog.d(TAG, ".clearActiveStateEventCounter(): Clear active state events")
//        EspressoIdlingResource.clear()
        activeStateEvents.clear()
        syncNumActiveStateEvents()
    }

    fun addStateEvent(stateEvent: StateEvent){
//        EspressoIdlingResource.increment()
        activeStateEvents.put(stateEvent.eventName(), stateEvent)
        syncNumActiveStateEvents()
    }

    fun removeStateEvent(stateEvent: StateEvent?){
        if(LOG_ME)ALog.d(
            TAG, ".removeStateEvent(): " +
                "remove state event: ${stateEvent?.eventName()}")
        stateEvent?.let {
//            EspressoIdlingResource.decrement()
        }
        activeStateEvents.remove(stateEvent?.eventName())
        syncNumActiveStateEvents()
    }

    fun isStateEventActive(stateEvent: StateEvent): Boolean{
        if(LOG_ME)ALog.d(
            TAG, ".isStateEventActive(): " +
                                "is state event active? " +
                                "${activeStateEvents.containsKey(stateEvent.eventName())}")
        return activeStateEvents.containsKey(stateEvent.eventName())
    }

    private fun syncNumActiveStateEvents(){
        val methodName: String = "syncNumActiveStateEvents"
        if (LOG_ME) ALog.d(TAG, "Method start: $methodName")
        try {
            var shouldDisplayProgressBar = false
            for(stateEvent in activeStateEvents.values){
                if (LOG_ME) ALog.d(TAG, "$methodName(): stateEvent ${stateEvent.eventName()}")
                if(stateEvent.shouldDisplayProgressBar()){
                    if (LOG_ME) ALog.d(TAG, "$methodName(): stateEvent ${stateEvent.eventName()} makes progress bar visible")
                    shouldDisplayProgressBar = true
                }
            }
            if (LOG_ME) ALog.d(TAG, "$methodName(): shouldDisplayProgressBar == $shouldDisplayProgressBar")
            _shouldDisplayProgressBar.value = shouldDisplayProgressBar
        } catch (e: Exception) {
            ALog.e(TAG, methodName, e)
            return
        } finally {
            if (LOG_ME) ALog.d(TAG, "Method end: $methodName")
        }
    }

    fun printStateEvents(): String {
        val stringBuilder = StringBuilder()

        activeStateEvents.forEach { (key, value) ->
            stringBuilder.appendLine("$key: $value")
        }
        return stringBuilder.toString()
    }
}

















