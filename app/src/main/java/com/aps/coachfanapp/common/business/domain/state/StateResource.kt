package com.aps.coachfanapp.common.business.domain.state

import android.view.View
import com.aps.coachfanapp.R
import com.aps.coachfanapp.common.util.ALog
import com.aps.coachfanapp.common.util.TodoCallback

private const val TAG = "StateResource"
private const val LOG_ME = true

data class StateMessage(val response: Response)

data class Response(
    val messageId: Int?,
    val message: String?,
    val uiComponentType: UIComponentType,
    val messageType: MessageType
)

sealed class UIComponentType{

    abstract fun equalsByValue(otherComponent: UIComponentType): Boolean

    class Toast: UIComponentType() {
        override fun equalsByValue(otherComponent: UIComponentType): Boolean {
            if(otherComponent !is Toast)return false
            if(LOG_ME) ALog.d("Toast", "equalsByValue(): Toasts can't be compared.")
            return false
        }
    }

    class Dialog(
        val okButtonCallback: OkButtonCallback? = null,
        val titleID: Int? = null,
    ): UIComponentType() {
        override fun equalsByValue(otherComponent: UIComponentType): Boolean {
            if(otherComponent !is Dialog)return false
            return titleID == otherComponent.titleID
        }

        override fun toString(): String {
            return "Dialog with titleID: $titleID"
        }
    }

    class AreYouSureDialog(
        val titleID: Int = R.string.are_you_sure,
        val callback: AreYouSureCallback
    ): UIComponentType() {
        override fun equalsByValue(otherComponent: UIComponentType): Boolean {
            if(otherComponent !is AreYouSureDialog)return false
            return titleID == otherComponent.titleID
        }

        override fun toString(): String {
            return "AreYouSureDialog with titleID: $titleID"
        }
    }

    class SnackBar(
        val onDismissCallback: TodoCallback? = null,
        var actionTextId: Int,
        val actionTextString: String? = null,
        val actionOnClickListener: View.OnClickListener
    ): UIComponentType() {
        override fun equalsByValue(otherComponent: UIComponentType): Boolean {
            if(otherComponent !is SnackBar)return false
            return actionTextId == otherComponent.actionTextId &&
                    actionTextString == otherComponent.actionTextString
        }

        override fun toString(): String {
            return "SnackBar with actionTextString == $actionTextString"
        }
    }

    class None: UIComponentType() {
        override fun equalsByValue(otherComponent: UIComponentType): Boolean {
            return otherComponent is None
        }
    }
}

sealed class MessageType{

    class Success: MessageType()

    class Error: MessageType()

    class Info: MessageType()

    class None: MessageType()
}


interface StateMessageCallback{

    fun removeMessageFromStack()
}


interface AreYouSureCallback {

    fun proceed()

    fun cancel()
}


interface OkButtonCallback {
    fun proceed()
}

interface SnackbarUndoCallback {

    fun undo()
}

class SnackbarUndoListener
    constructor(
        private val snackbarUndoCallback: SnackbarUndoCallback?
    ): View.OnClickListener {

    override fun onClick(v: View?) {
        snackbarUndoCallback?.undo()
    }

}


interface DialogInputCaptureCallback {

    fun onTextCaptured(text: String)
}















