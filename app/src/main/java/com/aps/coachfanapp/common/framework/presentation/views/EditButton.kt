package com.aps.coachfanapp.common.framework.presentation.views

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.material.DropdownMenu
import androidx.compose.material.DropdownMenuItem
import androidx.compose.material.Icon
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.material.TextButton
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Edit
import androidx.compose.material.icons.filled.Feedback
import androidx.compose.material.icons.filled.Note
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.aps.coachfanapp.R
import com.aps.coachfanapp.common.framework.presentation.values.CommonTheme

@Composable
fun EditButton(
    buttonString: String = stringResource(id = R.string.edit),
    option1String: String,
    option2String: String,
    onEditButtonClick: () -> Unit,
    onClickOption1: () -> Unit,
    onClickOption2: () -> Unit,
    contentPadding: PaddingValues = PaddingValues(4.dp, 4.dp, 4.dp, 4.dp),
    composableBackground: DrawnBackground? = null,
    backgroundDrawableId: Int? = R.drawable.list_item_example_background_1,
    modifier: Modifier = Modifier,
) {
    var expanded by remember { mutableStateOf(false) }
    val mediumPadding = remember { PaddingValues(8.dp, 8.dp, 8.dp, 8.dp) }
    BoxWithBackground(
        backgroundDrawableId = backgroundDrawableId,
        composableBackground = composableBackground,
        modifier = modifier
            .fillMaxWidth()
            .padding(mediumPadding),
    ) {
        TextButton(
            onClick = { onEditButtonClick() },
            modifier = Modifier
                .fillMaxWidth()
                .height(48.dp)
                .background(MaterialTheme.colors.primary)
                .padding(mediumPadding),
            contentPadding = mediumPadding,
            elevation = null,
            shape = MaterialTheme.shapes.medium
        ) {
            Row(
                verticalAlignment = Alignment.CenterVertically,
                horizontalArrangement = Arrangement.SpaceBetween,
                modifier = Modifier.fillMaxSize()
            ) {
                Text(
                    text = buttonString,
                    style = TextStyle(
                        fontFamily = FontFamily.Default,
                        fontWeight = FontWeight.Bold,
                        fontSize = 14.sp,
                        color = Color.White
                    ),
                    color = MaterialTheme.colors.onPrimary
                )
                Icon(
                    imageVector = Icons.Default.Edit,
                    contentDescription = "Edit",
                    tint = MaterialTheme.colors.onPrimary,
                )
            }
        }

        DropdownMenu(
            expanded = expanded,
            onDismissRequest = { expanded = false },
            modifier = Modifier.background(MaterialTheme.colors.primary)
        ) {
            DropdownMenuItem(
                content = {
                    Row(
                        verticalAlignment = Alignment.CenterVertically,
                        horizontalArrangement = Arrangement.spacedBy(8.dp)
                    ) {
                        Icon(
                            imageVector = Icons.Default.Note,
                            contentDescription = option1String,
                            tint = MaterialTheme.colors.onPrimary,
                        )
                        Text(
                            text = option1String,
                            color = MaterialTheme.colors.onPrimary,
                        )
                    }
                },
                onClick = {
                    onClickOption2()
                    expanded = false
                })

            DropdownMenuItem(
                content = {
                    Row(
                        verticalAlignment = Alignment.CenterVertically,
                        horizontalArrangement = Arrangement.spacedBy(8.dp)
                    ) {
                        Icon(
                            imageVector = Icons.Default.Feedback,
                            contentDescription = option2String,
                            tint = MaterialTheme.colors.onPrimary,
                        )
                        Text(
                            text = option2String,
                            color = MaterialTheme.colors.onPrimary,
                        )
                    }
                },
                onClick = {
                    expanded = false
                    onClickOption1()
                }
            )

        }
    }
}

@Preview
@Composable
private fun EditButtonPreview1() {
    CommonTheme {
        EditButton(
            option1String = "Option 1",
            option2String = "Option 2",
            onEditButtonClick = {},
            onClickOption1 = {},
            onClickOption2 = {})
    }
}