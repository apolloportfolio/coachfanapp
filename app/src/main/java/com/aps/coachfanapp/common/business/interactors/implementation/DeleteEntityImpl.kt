package com.aps.coachfanapp.common.business.interactors.implementation


import com.aps.coachfanapp.R
import com.aps.coachfanapp.common.business.data.cache.CacheResponseHandler
import com.aps.coachfanapp.common.business.data.cache.StandardCacheDataSource
import com.aps.coachfanapp.common.business.data.network.StandardNetworkDataSource
import com.aps.coachfanapp.common.business.data.util.safeApiCall
import com.aps.coachfanapp.common.business.data.util.safeCacheCall
import com.aps.coachfanapp.common.business.domain.state.*
import com.aps.coachfanapp.common.business.interactors.abstraction.DeleteEntity
import com.aps.coachfanapp.common.util.UniqueID
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class DeleteEntityImpl<
        Entity,
        CacheDataSource: StandardCacheDataSource<Entity>,
        NetworkDataSource: StandardNetworkDataSource<Entity>,
        ViewState: com.aps.coachfanapp.common.business.domain.state.ViewState<Entity>>
@Inject constructor(
    private val cacheDataSource: CacheDataSource,
    private val networkDataSource: NetworkDataSource
): DeleteEntity<
        Entity,
        CacheDataSource,
        NetworkDataSource,
        ViewState> 
{
    override fun deleteEntity(
        entity: Entity,
        stateEvent: StateEvent,
        getId: (entity: Entity) -> UniqueID?
    ): Flow<DataState<ViewState>?> = flow {

        val cacheResult = safeCacheCall(Dispatchers.IO){
            cacheDataSource.deleteEntity(getId(entity))
        }

        val response = object: CacheResponseHandler<ViewState, Int>(
            response = cacheResult,
            stateEvent = stateEvent
        ){
            override suspend fun handleSuccess(resultObj: Int): DataState<ViewState>? {
                return if(resultObj > 0){
                    DataState.data(
                        response = Response(
                            messageId = R.string.error,
                            message = DELETE_ENTITY_SUCCESS,
                            uiComponentType = UIComponentType.None(),
                            messageType = MessageType.Success()
                        ),
                        data = null,
                        stateEvent = stateEvent
                    )
                }
                else{
                    DataState.data(
                        response = Response(
                            messageId = R.string.error,
                            message = DELETE_ENTITY_FAILED,
                            uiComponentType = UIComponentType.Toast(),
                            messageType = MessageType.Error()
                        ),
                        data = null,
                        stateEvent = stateEvent
                    )
                }
            }
        }.getResult()

        emit(response)

        // update network
        if(response?.stateMessage?.response?.message.equals(DELETE_ENTITY_SUCCESS)){
            // delete from 'entities' node
            safeApiCall(Dispatchers.IO){
                networkDataSource.deleteEntity(getId(entity))
            }

            // insert into 'deletes' node
            safeApiCall(Dispatchers.IO){
                networkDataSource.insertDeletedEntity(entity)
            }
        }
    }

    companion object{
        val DELETE_ENTITY_SUCCESS = "Successfully deleted entity."
        val DELETE_ENTITY_PENDING = "Delete pending..."
        val DELETE_ENTITY_FAILED = "Failed to delete entity."
        val DELETE_ARE_YOU_SURE = "Are you sure you want to delete this?"
    }
}