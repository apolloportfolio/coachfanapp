package com.aps.coachfanapp.common.framework.presentation.views

import android.annotation.SuppressLint
import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.BadgedBox
import androidx.compose.material.Icon
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.semantics.contentDescription
import androidx.compose.ui.semantics.semantics
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavHostController
import androidx.navigation.compose.rememberNavController
import com.aps.coachfanapp.R
import com.aps.coachfanapp.common.framework.presentation.NavItem
import com.aps.coachfanapp.common.framework.presentation.values.CommonTheme
import com.aps.coachfanapp.common.util.composableutils.ExampleScreenDestination

internal const val DRAWER_CONTENT_DESCRIPTION_TAG = "Drawer"

// Reference: https://www.youtube.com/watch?v=JLICaBEiJS0
@Composable
fun StandardNavigationDrawer(
    isDualPane: Boolean,

    titleId: Int,
    contentDescriptionId: Int,
    titleBackgroundId: Int = R.drawable.list_item_example_background_1,

    navItems: List<NavItem>,
    leftPaneNavController: NavHostController,
    rightPaneNavController: NavHostController,

    @SuppressLint("ModifierParameter") modifier: Modifier = Modifier,
    navItemsListBackgroundId: Int = R.drawable.list_item_example_background_1,
    navItemTextStyle: TextStyle = TextStyle(fontSize = 18.sp),
    navItemBackgroundId: Int = R.drawable.list_item_example_background_1,
) {
    Column {
        StandardNavigationDrawerHeader(
            titleId,
            contentDescriptionId,
            titleBackgroundId,
        )
        StandardNavigationDrawerBody(
            isDualPane = isDualPane,
            navItems = navItems,
            leftPaneNavController = leftPaneNavController,
            rightPaneNavController = rightPaneNavController,
            modifier = modifier,
            backgroundId = navItemsListBackgroundId,
            navItemTextStyle = navItemTextStyle,
            navItemBackgroundId = navItemBackgroundId,
        )
    }
}

@Composable
fun StandardNavigationDrawerHeader(
    titleId: Int,
    contentDescriptionId: Int,
    backgroundId: Int = R.drawable.list_item_example_background_1,
) {
    BoxWithBackground(
        backgroundDrawableId = backgroundId,
    ) {
        val headerContentDescription = stringResource(id = contentDescriptionId)
        Text(
            modifier = Modifier
                .fillMaxWidth()
                .wrapContentHeight()
                .semantics {
                    contentDescription = headerContentDescription
                },
            textAlign = TextAlign.Center,
            text = stringResource(titleId),
            fontSize = 40.sp,
            color = MaterialTheme.colors.onSurface,
        )
    }
}

@Composable
fun StandardNavigationDrawerBody(
    isDualPane: Boolean,
    navItems: List<NavItem>,
    leftPaneNavController: NavHostController,
    rightPaneNavController: NavHostController,

    modifier: Modifier = Modifier,
    backgroundId: Int = R.drawable.list_item_example_background_1,
    navItemTextStyle: TextStyle = TextStyle(fontSize = 18.sp),
    navItemBackgroundId: Int = R.drawable.list_item_example_background_1,
) {
    Box(modifier = Modifier
        .fillMaxWidth()
        .wrapContentHeight()
    ) {

        Image(
            painterResource(id = backgroundId),
            contentDescription = "",
            contentScale = ContentScale.FillBounds,
            modifier = Modifier.matchParentSize()
        )

        LazyColumn(
            contentPadding = PaddingValues(horizontal = 16.dp, vertical = 8.dp),
            verticalArrangement = Arrangement.spacedBy(8.dp),
            modifier = Modifier.fillMaxWidth(),
        ) {
            items(navItems) { item ->
                StandardDrawerBodyListItem(
                    isDualPane = isDualPane,
                    navItem = item,
                    leftPaneNavController = leftPaneNavController,
                    rightPaneNavController = rightPaneNavController,
                    navItemBackgroundId = navItemBackgroundId,
                )
            }
        }

    }

}

@Composable
internal fun StandardDrawerBodyListItem(
    isDualPane: Boolean,
    navItem: NavItem,
    leftPaneNavController: NavHostController,
    rightPaneNavController: NavHostController,

    navItemBackgroundId: Int = R.drawable.list_item_example_background_1,
    navItemTextStyle: TextStyle = TextStyle(fontSize = 18.sp),
) {
    Box(modifier = Modifier
        .fillMaxWidth()
        .wrapContentHeight()
    ) {
        val itemContentDescription =
            "$DRAWER_CONTENT_DESCRIPTION_TAG ${stringResource(id = navItem.contentDescriptionID)}"

        Image(
            painterResource(id = navItemBackgroundId),
            contentDescription = "",
            contentScale = ContentScale.FillBounds,
            modifier = Modifier.matchParentSize()
        )

        Row(
            modifier = Modifier
                .fillMaxWidth()
                .clickable {
                    navItem.onItemClickNavigation(
                        isDualPane,
                        navItem,
                        leftPaneNavController,
                        rightPaneNavController,
                    )
                }
                .padding(16.dp),
        ) {
            if(navItem.badgeCount > 0) {
                BadgedBox(
                    badge = {
                        Text(
                            text = navItem.badgeCount.toString(),
                            color = MaterialTheme.colors.primary,
                        )
                    },
                ) {
                    Icon(
                        painter = painterResource(navItem.iconDrawableID),
                        contentDescription = itemContentDescription
                    )
                }
            } else {
                Icon(
                    painter = painterResource(navItem.iconDrawableID),
                    contentDescription = itemContentDescription
                )
            }
            Text(
                text = stringResource(navItem.nameStringID),
                style = navItemTextStyle,
                modifier = Modifier
                    .weight(1f)
                    .semantics{
                        contentDescription = itemContentDescription
                    },
                color = MaterialTheme.colors.onSurface,
            )
        }

    }
}

// Preview==========================================================================================
@Preview
@Composable
internal fun StandardNavigationDrawerPreview() {
    CommonTheme {
        val onItemClickNavigationListScreens: (
            Boolean,
            NavItem,
            NavHostController?,
            NavHostController?,
        ) -> Unit = remember {
            { isDualPane, navItem, leftNavController, rightNavController ->
                if(isDualPane) {

                } else {

                }
            }
        }

        val leftPaneNavController = rememberNavController()
        val rightPaneNavController = rememberNavController()

        StandardNavigationDrawer(
            isDualPane = false,
            titleId = R.string.settings,
            contentDescriptionId = R.string.settings,
            titleBackgroundId = R.drawable.example_background_1,
            navItems = listScreensNavItems(onItemClickNavigationListScreens),
            leftPaneNavController = leftPaneNavController,
            rightPaneNavController = rightPaneNavController,
            modifier = Modifier,
            navItemsListBackgroundId = R.drawable.example_background_1,
            navItemTextStyle = TextStyle(fontSize = 18.sp),
            navItemBackgroundId = R.drawable.example_background_1,
        )
    }
}

internal fun listScreensNavItems(
    onItemClickNavigation: (Boolean, NavItem, NavHostController?, NavHostController?) -> Unit,
): List<NavItem> {
    return listOf(
        NavItem(
            nameStringID = R.string.home_screen_bottom_nav_menu_1,
            route = ExampleScreenDestination.Card1,
            iconDrawableID = R.drawable.ic_search_yellow_24,
            onItemClickNavigation = onItemClickNavigation,
            contentDescriptionID = R.string.home_screen_bottom_nav_menu_1_content_description,
        ),
        NavItem(
            nameStringID = R.string.home_screen_bottom_nav_menu_2,
            route = ExampleScreenDestination.Card2,
            iconDrawableID = R.drawable.ic_calendar_yellow_24,
            onItemClickNavigation = onItemClickNavigation,
            contentDescriptionID = R.string.home_screen_bottom_nav_menu_2_content_description,
        ),
        NavItem(
            nameStringID = R.string.home_screen_bottom_nav_menu_3,
            route = ExampleScreenDestination.Card3,
            iconDrawableID = R.drawable.ic_message_yellow_24,
            onItemClickNavigation = onItemClickNavigation,
            contentDescriptionID = R.string.home_screen_bottom_nav_menu_3_content_description,
        ),
        NavItem(
            nameStringID = R.string.home_screen_bottom_nav_menu_4,
            route = ExampleScreenDestination.Card4,
            iconDrawableID = R.drawable.ic_car_yellow_24,
            onItemClickNavigation = onItemClickNavigation,
            contentDescriptionID = R.string.home_screen_bottom_nav_menu_4_content_description,
        ),
        NavItem(
            nameStringID = R.string.home_screen_bottom_nav_menu_5,
            route = ExampleScreenDestination.Card5,
            iconDrawableID = R.drawable.ic_account_box_yellow_24,
            onItemClickNavigation = onItemClickNavigation,
            contentDescriptionID = R.string.home_screen_bottom_nav_menu_5_content_description,
        ),
    )
}

@Preview
@Composable
internal fun StandardNavigationDrawerHeaderPreview() {
    CommonTheme {
        StandardNavigationDrawerHeader(
            titleId = R.string.settings,
            contentDescriptionId = R.string.settings,
            backgroundId = R.drawable.example_background_1,
        )
    }
}