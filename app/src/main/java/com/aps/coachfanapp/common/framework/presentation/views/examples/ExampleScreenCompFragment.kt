package com.aps.coachfanapp.common.framework.presentation.views.examples

import android.Manifest
import android.app.Activity
import android.location.Location
import android.widget.Toast
import androidx.compose.animation.core.Spring
import androidx.compose.animation.core.spring
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.semantics.contentDescription
import androidx.compose.ui.semantics.semantics
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.tooling.preview.PreviewParameter
import androidx.compose.ui.tooling.preview.PreviewParameterProvider
import com.aps.coachfanapp.R
import com.aps.coachfanapp.common.business.domain.state.StateEventTracker
import com.aps.coachfanapp.common.framework.presentation.PermissionHandlingData
import com.aps.coachfanapp.common.framework.presentation.TAG_PERMISSION_ASKING
import com.aps.coachfanapp.common.framework.presentation.layouts.LazyColumnWithPullToRefresh
import com.aps.coachfanapp.common.framework.presentation.values.CommonTheme
import com.aps.coachfanapp.common.framework.presentation.views.*
import com.aps.coachfanapp.common.util.ALog
import com.aps.coachfanapp.common.util.DeviceLocation
import com.aps.coachfanapp.common.util.GeoLocationUtilities
import com.aps.coachfanapp.common.util.PreviewEntity
import com.aps.coachfanapp.common.util.PreviewEntityFactory

private const val TAG = "ExampleScreenCompFragment"
private const val LOG_ME = true
const val HomeScreenComposableFragment1TestTag = TAG

@OptIn(ExperimentalMaterialApi::class)
@Composable
fun ExampleScreenCompFragment(
    launchStateEvent: (ExampleScreenStateEvent) -> Unit = {},

    activity: Activity? = null,
    permissionHandlingData: PermissionHandlingData = PermissionHandlingData(),

    stateEventTracker: StateEventTracker,

    deviceLocation: DeviceLocation?,
    showProfileStatusBar: Boolean,
    finishVerification: () -> Unit,
    initialSearchQuery: String = "",
    entities: List<PreviewEntity>?,
    onSearchQueryUpdate: (String) -> Unit,
    onListItemClick: (PreviewEntity) -> Unit,

    onSwipeLeft: () -> Unit = {},
    onSwipeRight: () -> Unit = {},
    onSwipeUp: () -> Unit = {},
    onSwipeDown: () -> Unit = {},
    onSwipeRightFromComposableSide: () -> Unit = {},
    onSwipeLeftFromComposableSide: () -> Unit = {},
    onSwipeDownFromComposableSide: () -> Unit = {},
    onSwipeUpFromComposableSide: () -> Unit = {},

    floatingActionButtonDrawableId: Int? = null,
    floatingActionButtonOnClick: (() -> Unit)? = null,
    floatingActionButtonContentDescription: String? = null,
    snackBarState: SnackBarState?,
    toastState: ToastState?,
    dialogState: DialogState?,
    progressIndicatorState: ProgressIndicatorState?,
    bottomSheetActions: ExampleScreenComposableFragmentBottomSheetActions,

    isPreview: Boolean = false,
) {
    if(LOG_ME) ALog.d(TAG, "(): Recomposition start.")

    val scope = rememberCoroutineScope()
    val sheetState = rememberBottomSheetState(
        initialValue = BottomSheetValue.Collapsed,
//        initialValue = BottomSheetValue.Expanded,
        animationSpec = spring(dampingRatio = Spring.DampingRatioHighBouncy)
    )
    val launchInitStateEvent: () -> Unit = {
        if(activity != null) {
            if(LOG_ME)ALog.d(TAG, "launchInitStateEvent(): " +
                    "activity != null, launching state event in view model.")
        } else {
            if(LOG_ME)ALog.w(TAG, "launchInitStateEvent(): " +
                    "activity == null, couldn't set initial state event!")
        }
    }

    val permissionsRequiredInFragment = mutableSetOf(
        Manifest.permission.ACCESS_COARSE_LOCATION,
        Manifest.permission.ACCESS_FINE_LOCATION,
    )

    if(LOG_ME)ALog.d(
        TAG_PERMISSION_ASKING+TAG, "(): " +
                            "permissionsRequiredInFragment = ${permissionsRequiredInFragment.joinToString()}")
    SwipeableFragmentWithBottomSheetAndFAB(
        launchInitStateEvent = launchInitStateEvent,
        activity = activity,
        permissionHandlingData = permissionHandlingData,
        permissionsRequiredInFragment = permissionsRequiredInFragment,
        backgroundDrawableId = R.drawable.default_background,
        onSwipeLeft = onSwipeLeft,
        onSwipeRight = onSwipeRight,
        onSwipeUp = onSwipeUp,
        onSwipeDown = onSwipeDown,
        onSwipeRightFromComposableSide = onSwipeRightFromComposableSide,
        onSwipeLeftFromComposableSide = onSwipeLeftFromComposableSide,
        onSwipeDownFromComposableSide = onSwipeDownFromComposableSide,
        onSwipeUpFromComposableSide = onSwipeUpFromComposableSide,
        leftSideThreshold = 0.15f,
        floatingActionButtonDrawableId = floatingActionButtonDrawableId,
        floatingActionButtonOnClick = floatingActionButtonOnClick,
        floatingActionButtonContentDescription = floatingActionButtonContentDescription,
        snackBarState = snackBarState,
        toastState = toastState,
        dialogState = dialogState,
        progressIndicatorState = progressIndicatorState,
        sheetState = sheetState,
        sheetContent = ExampleScreenCompFragmentBottomSheet(
            scope = scope,
            sheetState = sheetState,
            leftButtonOnClick = bottomSheetActions.leftButtonOnClick,
            rightButtonOnClick = bottomSheetActions.rightButtonOnClick,
        ),
        content = {
            ExampleScreenComposableFragmentContent(
                stateEventTracker = stateEventTracker,
                deviceLocation = deviceLocation,
                showProfileStatusBar,
                finishVerification,
                initialSearchQuery,
                entities,
                onSearchQueryUpdate,
                onListItemClick,
                launchInitStateEvent = launchInitStateEvent,
                isPreview = isPreview,
            )
        },
        isPreview = isPreview,
    )
    if(LOG_ME) ALog.d(TAG, "(): Recomposition end.")
}

@Composable
internal fun ExampleScreenComposableFragmentContent(
    stateEventTracker: StateEventTracker,
    deviceLocation: DeviceLocation?,
    showProfileStatusBar: Boolean,
    finishVerification: () -> Unit,
    initialSearchQuery: String = "",
    entities: List<PreviewEntity>?,
    onSearchQueryUpdate: (String) -> Unit,
    onListItemClick: (PreviewEntity) -> Unit,
    launchInitStateEvent: () -> Unit,

    isPreview: Boolean = false,
) {
    if(LOG_ME) ALog.d(TAG, "(): Recomposition start.")
    val fragmentsContentDescription = stringResource(id = R.string.home_screen_card1_content_description)
    BoxWithBackground(
        backgroundDrawableId = R.drawable.default_background,
        modifier = Modifier
            .testTag(HomeScreenComposableFragment1TestTag)
            .fillMaxWidth()
            .wrapContentHeight(),
    ) {
        Column(
            modifier = Modifier
                .semantics {
                    contentDescription = fragmentsContentDescription
                },
            verticalArrangement = Arrangement.Center,
        ) {
            var searchQuery by remember { mutableStateOf(initialSearchQuery) }
            var searchQueryUpdated by remember { mutableStateOf(false) }

            SearchBarWithClearButton(
                searchQuery = searchQuery,
                onSearchQueryChange = { query -> searchQuery = query },
                onSearchBackClick = {
                    searchQuery = ""
                    searchQueryUpdated = true
                },
                onSearchClick = {
                    searchQueryUpdated = true
                }
            )

            if(LOG_ME)ALog.d(TAG, "(): " +
                    "showProfileStatusBar = $showProfileStatusBar")
            if(showProfileStatusBar) {
                if(LOG_ME)ALog.d(TAG, "(): " +
                        "Showing profile completion bar")
                ProfileStatusBar(finishVerification)
            }

            if(entities?.isNotEmpty() == true) {
                LazyColumnWithPullToRefresh(
                    onRefresh = launchInitStateEvent,
                    isRefreshing = stateEventTracker.isRefreshing,
                ) {
                    itemsIndexed(entities) { index, item ->
                        ListItemType03(
                            index = index,
                            itemTitleString = item.name,
                            itemPriceString = "",
                            itemLikesString = "",
                            itemDistanceString = GeoLocationUtilities.distanceFormattedInKM(
                                deviceLocation?.latitude,
                                deviceLocation?.longitude,
                                item.latitude,
                                item.longitude,
                            ),
                            lblSmallShortAttribute3String = "",
                            itemDescriptionString = item.description,
                            onListItemClick = { onListItemClick(item) },
                            getItemsRating = { null },
                            itemRef = if(!isPreview) {
//                                item.picture1FirebaseImageRef
                                null
                            } else {
                                null
                            },
                        )
                    }
                }
            } else {
                TipContentIsUnavailable(stringResource(id = R.string.home_screen_fragment1_list_is_empty_tip))
            }

            var firstComposition by rememberSaveable { mutableStateOf(true) }
            LaunchedEffect(searchQueryUpdated) {
                if(firstComposition) {
                    if(LOG_ME)ALog.d(TAG, "LaunchedEffect(searchFiltersUpdated): " +
                            "This is the first composition.")
                    firstComposition = false
                } else {
                    if(searchQueryUpdated) {
                        if(LOG_ME)ALog.d(TAG, "(): LaunchedEffect: searchQueryUpdated")
                        onSearchQueryUpdate(searchQuery)
                    }
                }
                searchQueryUpdated = false
            }

            if(LOG_ME) ALog.d(TAG, "(): Recomposition start.")
        }
    }

}


data class ExampleScreenComposableFragmentBottomSheetActions(
    val leftButtonOnClick: () -> Unit,
    val rightButtonOnClick: () -> Unit,
)



//========================================================================================
@Preview
@Composable
fun HomeScreenComposableFragment1Preview(
    @PreviewParameter(
        HomeScreenComposableFragment1ParamsProvider::class
    ) params: HomeScreenComposableFragment1Params
) {
    CommonTheme {
        // Start Interactive Mode to actually see SnackBar
        val snackBarState = remember{
            SnackBarState(
                show = true,
                message = "Snackbar",
                actionLabel = "action",
                iconDrawableRes = R.drawable.ic_launcher,
                onClickAction = {},
                onDismissAction = {},
                duration = SnackbarDuration.Indefinite,
                updateSnackBarInViewModel = {},
            )
        }
        snackBarState.onDismissAction = { snackBarState.show = false }
        snackBarState.onClickAction = { snackBarState.show = false }

        val toastState = ToastState(
            show = true,
            message = "This is a Toast from Preview!",
            duration = Toast.LENGTH_LONG,
            updateToastInViewModel = {},
        )

        val dialogState = DialogState(
            show = false,
            onDismissRequest = {},
            title = "Dialog's Title",
            text = "Dialog's text.",
            leftButtonText = "Dismiss",
            leftButtonOnClick = {},
            rightButtonText = "Confirm",
            rightButtonOnClick = {},
            updateDialogInViewModel = {},
        )

        val progressIndicatorState = ProgressIndicatorState(
            show = false,
            progress = 0.7f,
            type = ProgressIndicatorType.DeterminateCircularProgressIndicator,
            updateProgressIndicatorStateInViewModel = {},
        )

        val bottomSheetActions = ExampleScreenComposableFragmentBottomSheetActions(
            leftButtonOnClick = {},
            rightButtonOnClick = {},
        )

        ExampleScreenCompFragment(
            stateEventTracker = StateEventTracker(),
            deviceLocation = params.deviceLocation,
            showProfileStatusBar = params.showProfileStatusBar,
            finishVerification = params.finishVerification,
            initialSearchQuery = params.initialSearchQuery,
            entities = params.entities,
            onSearchQueryUpdate = params.onSearchQueryUpdate,
            onListItemClick = params.onListItemClick,
            onSwipeLeft = params.onSwipeLeft,
            onSwipeRight = params.onSwipeRight,
            onSwipeUp = params.onSwipeUp,
            onSwipeDown = params.onSwipeDown,
            floatingActionButtonDrawableId = params.floatingActionButtonDrawableId,
            floatingActionButtonOnClick = params.floatingActionButtonOnClick,
            floatingActionButtonContentDescription = params.floatingActionButtonContentDescription,
            snackBarState = snackBarState,
            toastState = toastState,
            dialogState = dialogState,
            bottomSheetActions = bottomSheetActions,
            progressIndicatorState = progressIndicatorState,
            isPreview = true,
        )
    }
}


class HomeScreenComposableFragment1ParamsProvider :
    PreviewParameterProvider<HomeScreenComposableFragment1Params> {

    override val values: Sequence<HomeScreenComposableFragment1Params>
        get() = sequenceOf(
            HomeScreenComposableFragment1Params(
                deviceLocation = DeviceLocation(
                    locationPermissionGranted = true,
                    location = Location("Lublin").apply {
                        latitude = 51.2465
                        longitude = 22.5684
                    }
                ),
                showProfileStatusBar = true,
                finishVerification = {},
                initialSearchQuery = "",
                entities = PreviewEntityFactory.createPreviewEntitiesList(),
                onSearchQueryUpdate = {},
                onListItemClick = {},
                onSwipeLeft = {},
                onSwipeRight = {},
                onSwipeUp = {},
                onSwipeDown = {},
                floatingActionButtonDrawableId = null,
                floatingActionButtonOnClick = {},
                floatingActionButtonContentDescription = null,
            )
        )
}

data class HomeScreenComposableFragment1Params(
    val deviceLocation: DeviceLocation?,
    val showProfileStatusBar: Boolean,
    val finishVerification: () -> Unit,
    val initialSearchQuery: String = "",
    val entities: List<PreviewEntity>?,
    val onSearchQueryUpdate: (String) -> Unit,
    val onListItemClick: (PreviewEntity) -> Unit,
    val onSwipeLeft: () -> Unit = {},
    val onSwipeRight: () -> Unit = {},
    val onSwipeUp: () -> Unit = {},
    val onSwipeDown: () -> Unit = {},
    val floatingActionButtonDrawableId: Int? = null,
    val floatingActionButtonOnClick: (() -> Unit)? = null,
    val floatingActionButtonContentDescription: String? = null,
)