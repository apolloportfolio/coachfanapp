package com.aps.coachfanapp.common.util

import android.location.Location

data class DeviceLocation(
    var locationPermissionGranted : Boolean? = null,
    var location: Location,
    ) : Location(location), java.io.Serializable {
    override fun toString(): String {
        return super.toString() + "    locationPermissionGranted == $locationPermissionGranted"
    }

    companion object {
        val PreviewDeviceLocation = DeviceLocation(
            locationPermissionGranted = true,
            location = Location("Lublin").apply {
                latitude = 51.2465
                longitude = 22.5684
            }
        )
    }

}