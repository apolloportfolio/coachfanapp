package com.aps.coachfanapp.common.business.interactors.abstraction


import com.aps.coachfanapp.common.business.data.cache.StandardCacheDataSource
import com.aps.coachfanapp.common.business.data.network.StandardNetworkDataSource
import com.aps.coachfanapp.common.business.domain.state.DataState
import kotlinx.coroutines.flow.Flow

interface DoNothingAtAll<
        Entity,
        CacheDataSource: StandardCacheDataSource<Entity>,
        NetworkDataSource: StandardNetworkDataSource<Entity>,
        ViewState: com.aps.coachfanapp.common.business.domain.state.ViewState<Entity>> {
    fun doNothing(): Flow<DataState<ViewState>?>
}