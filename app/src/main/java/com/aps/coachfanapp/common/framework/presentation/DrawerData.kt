package com.aps.coachfanapp.common.framework.presentation

import androidx.compose.foundation.layout.ColumnScope
import androidx.compose.material.DrawerDefaults
import androidx.compose.runtime.Composable
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.Shape
import androidx.compose.ui.unit.Dp

data class DrawerData(
    val drawerContent: @Composable (ColumnScope.() -> Unit)? = null,
//    val drawerContent: @Composable (() -> Unit)? = null,
    val drawerShape: Shape? = null,
    val drawerElevation: Dp = DrawerDefaults.Elevation,
    val drawerBackgroundColor: Color? = null,
    val drawerContentColor: Color? = null,
    val drawerScrimColor: Color? = null,
)