package com.aps.coachfanapp.common.util

import android.util.Log
import java.io.Serializable
import java.util.*

private const val TAG = "UserUniqueID"
private const val LOG_ME = true

class UserUniqueID(
    firestoreDocumentID: String,
    var firestoreIdInDocument : String? = null,
) : UniqueID(firestoreDocumentID), Serializable {
    constructor() : this("-666")


    override fun <T: UniqueID> equalByValueTo(other: T): Boolean {
        val methodName: String = "equalByValueTo"
        if (LOG_ME) ALog.d(TAG, "Method start: $methodName")
        try {
            val otherId = other as UserUniqueID
            if(firestoreIdInDocument == null) {
                if(LOG_ME)ALog.d(TAG, ".$methodName(): firestoreIdInDocument == null")
                return false
            }
            if(otherId.firestoreIdInDocument == null) {
                if(LOG_ME)ALog.d(TAG, ".$methodName(): otherId.firestoreIdInDocument == null")
                return false
            }
            val firestoreDocumentIDsMatch = firestoreDocumentID.compareTo(other.firestoreDocumentID) == 0
            val firestoreIdInDocumentsMatch = firestoreIdInDocument!!.compareTo(otherId.firestoreIdInDocument!!) == 0
            Log.d(
                TAG, "equalByValueTo: firestoreDocumentIDsMatch == $firestoreDocumentIDsMatch\n" +
                    "firestoreIdInDocumentsMatch == $firestoreIdInDocumentsMatch\n" +
                    "this == $this\n" +
                    "other == $other")
            return firestoreDocumentIDsMatch && firestoreIdInDocumentsMatch
        } catch (e: Exception) {
            ALog.e(TAG, methodName, e)
        } finally {
            if (LOG_ME) ALog.d(TAG, "Method end: $methodName")
        }
        return false
    }

    companion object {
        val INVALID_ID: UserUniqueID = UserUniqueID("-666", "-666")
        fun generateFakeRandomID(
            randomString: String = UUID.randomUUID().toString()
        ): UserUniqueID {
            return UserUniqueID(
                UniqueID.INVALID_ID.firestoreDocumentID + "_" + randomString,
                randomString
            )
        }
    }
}