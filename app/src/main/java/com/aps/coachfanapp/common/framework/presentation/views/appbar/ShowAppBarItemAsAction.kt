package com.aps.coachfanapp.common.framework.presentation.views.appbar

enum class ShowAppBarItemAsAction {
    NEVER_OVERFLOW, IF_NECESSARY, ALWAYS_OVERFLOW, NOT_SHOWN
}