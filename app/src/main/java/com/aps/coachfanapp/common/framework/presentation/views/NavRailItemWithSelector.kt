package com.aps.coachfanapp.common.framework.presentation.views

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.scaleIn
import androidx.compose.animation.scaleOut
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.ColumnScope
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Icon
import androidx.compose.material.MaterialTheme
import androidx.compose.material.NavigationRailItem
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.semantics.contentDescription
import androidx.compose.ui.semantics.semantics
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.tooling.preview.PreviewParameter
import androidx.compose.ui.tooling.preview.PreviewParameterProvider
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import com.aps.coachfanapp.R
import com.aps.coachfanapp.common.framework.presentation.values.CommonTheme


internal const val NAV_RAIL_CONTENT_DESCRIPTION_TAG = "NavRail"

@Composable
fun ColumnScope.NavRailItemWithSelector(
    icon: @Composable () -> Unit,
    label: @Composable () -> Unit,
    selected: Boolean,
    onClick: () -> Unit,
    selectedContentColor: Color,
    unselectedContentColor: Color,
    contentDescriptionID: Int,
) {
    Box(
        modifier = Modifier
            .weight(1f, fill = false),
        contentAlignment = Alignment.Center,
    ) {
        val itemContentDescription =
            "$NAV_RAIL_CONTENT_DESCRIPTION_TAG ${stringResource(id = contentDescriptionID)}"

        Column {
            AnimatedVisibility(
                visible = selected,
                enter = scaleIn(),
                exit = scaleOut(),
            ) {
                Selector()
            }
        }
        Column {
            NavigationRailItem(
                icon = icon,
                label = label,
                selected = selected,
                onClick = onClick,
                selectedContentColor = selectedContentColor,
                unselectedContentColor = unselectedContentColor,
                modifier = Modifier
                    .semantics {
                        contentDescription = itemContentDescription
                    }
            )
        }
    }
}

private val SELECTOR_DEFAULT_SIZE = 50.dp
private val SelectorShape = RoundedCornerShape(percent = 15)
@Composable
fun Selector(size: Dp = SELECTOR_DEFAULT_SIZE, color: Color = MaterialTheme.colors.secondary) {
    Spacer(
        modifier = Modifier
            .size(size)
            .background(color, SelectorShape)
    )
}

// Preview =========================================================================================
@Preview
@Composable
fun NavRailItemWithSelectorPreview(
    @PreviewParameter(
        NavRailItemWithSelectorParamsProvider::class
    ) params: NavRailItemWithSelectorParams
) {
    CommonTheme {
        Column {
            NavRailItemWithSelector(
                icon = params.icon,
                label = params.label,
                selected = params.selected,
                onClick = params.onClick,
                selectedContentColor = params.selectedContentColor,
                unselectedContentColor = params.unselectedContentColor,
                contentDescriptionID = R.string.preview_content_description,
            )
        }
    }
}

class NavRailItemWithSelectorParamsProvider :
    PreviewParameterProvider<NavRailItemWithSelectorParams> {
    private val selectedContentColor = Color(0xFF550163)
    private val unselectedContentColor = Color(0xFF6905A7)

    override val values: Sequence<NavRailItemWithSelectorParams>
        get() = sequenceOf(
            NavRailItemWithSelectorParams(
                icon = {
                    Icon(
                        painter = painterResource(R.drawable.ic_search_yellow_24),
                        contentDescription = stringResource(id = R.string.home_screen_bottom_nav_menu_1_content_description)
                    )
                },
                label = { Text(stringResource(R.string.home_screen_bottom_nav_menu_1)) },
                selected = true,
                onClick = { },
                selectedContentColor = selectedContentColor,
                unselectedContentColor = unselectedContentColor,
            ),
            NavRailItemWithSelectorParams(
                icon = {
                    Icon(
                        painter = painterResource(R.drawable.ic_calendar_yellow_24),
                        contentDescription = stringResource(id = R.string.home_screen_bottom_nav_menu_2_content_description)
                    )
                },
                label = { Text(stringResource(R.string.home_screen_bottom_nav_menu_2)) },
                selected = false,
                onClick = { },
                selectedContentColor = selectedContentColor,
                unselectedContentColor = unselectedContentColor,
            ),
            NavRailItemWithSelectorParams(
                icon = {
                    Icon(
                        painter = painterResource(R.drawable.ic_message_yellow_24),
                        contentDescription = stringResource(id = R.string.home_screen_bottom_nav_menu_3_content_description)
                    )
                },
                label = { Text(stringResource(R.string.home_screen_bottom_nav_menu_3)) },
                selected = false,
                onClick = { },
                selectedContentColor = selectedContentColor,
                unselectedContentColor = unselectedContentColor,
            ),
            NavRailItemWithSelectorParams(
                icon = {
                    Icon(
                        painter = painterResource(R.drawable.ic_car_yellow_24),
                        contentDescription = stringResource(id = R.string.home_screen_bottom_nav_menu_4_content_description)
                    )
                },
                label = { Text(stringResource(R.string.home_screen_bottom_nav_menu_4)) },
                selected = false,
                onClick = { },
                selectedContentColor = selectedContentColor,
                unselectedContentColor = unselectedContentColor,
            ),
            NavRailItemWithSelectorParams(
                icon = {
                    Icon(
                        painter = painterResource(R.drawable.ic_account_box_yellow_24),
                        contentDescription = stringResource(id = R.string.home_screen_bottom_nav_menu_5_content_description)
                    )
                },
                label = { Text(stringResource(R.string.home_screen_bottom_nav_menu_5)) },
                selected = false,
                onClick = { },
                selectedContentColor = selectedContentColor,
                unselectedContentColor = unselectedContentColor,
            ),
        )
}

data class NavRailItemWithSelectorParams(
    val icon: @Composable () -> Unit,
    val label: @Composable () -> Unit,
    val selected: Boolean,
    val onClick: () -> Unit,
    val selectedContentColor: Color,
    val unselectedContentColor: Color,
)