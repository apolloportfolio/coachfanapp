package com.aps.coachfanapp.common.framework.presentation.views

import android.annotation.SuppressLint
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.material.Button
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.semantics.contentDescription
import androidx.compose.ui.semantics.semantics
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.aps.coachfanapp.R
import com.aps.coachfanapp.common.framework.presentation.values.CommonTheme

@Composable
fun RowWithButtonAndBackButton(
    buttonIcon: ImageVector = Icons.Default.Add,
    buttonString: String,
    buttonOnClick: () -> Unit,
    buttonColor: Color = MaterialTheme.colors.primary,
    buttonTextColor: Color = MaterialTheme.colors.onPrimary,
    buttonTextStyle: TextStyle = TextStyle(
        fontFamily = FontFamily.Default,
        fontWeight = FontWeight.Bold,
        fontSize = 14.sp,
    ),
    onBackButtonClick: () -> Unit,
    internalPaddingValues: PaddingValues = PaddingValues(0.dp, 0.dp, 0.dp, 0.dp),
    composableBackground: DrawnBackground? = null,
    backgroundDrawableId: Int? = R.drawable.list_item_example_background_1,
    @SuppressLint("ModifierParameter") modifier: Modifier = Modifier,
) {
    BoxWithBackground(
        modifier = modifier
            .fillMaxWidth()
            .padding(internalPaddingValues)
            .wrapContentHeight(),
        backgroundDrawableId = backgroundDrawableId,
        composableBackground = composableBackground,
    ) {
        Row(
            modifier = modifier
                .fillMaxWidth()
                .height(56.dp),
            horizontalArrangement = Arrangement.SpaceBetween,
            verticalAlignment = Alignment.CenterVertically
        ) {
            Button(
                onClick = buttonOnClick,
                modifier = Modifier
                    .fillMaxWidth(0.8F)
                    .height(56.dp),

                ) {
                Icon(
                    imageVector = buttonIcon,
                    contentDescription = null,
                )
                Spacer(modifier = Modifier.width(8.dp))
                Text(
                    text = buttonString,
                    style = buttonTextStyle,
                    modifier = Modifier.weight(1f),
                    color = buttonTextColor,
                )
            }

            // Back Button
            val backButtonContentDescription = stringResource(id = R.string.back_button)
            IconButton(
                onClick = onBackButtonClick,
                modifier = Modifier
                    .padding(internalPaddingValues)
                    .width(56.dp)
                    .height(56.dp)
                    .semantics { contentDescription = backButtonContentDescription },
            ) {
                Icon(
                    imageVector = Icons.Default.ArrowBack,
                    contentDescription = null,
                    tint = buttonColor,
                )
            }
        }
    }
}

@Preview
@Composable
private fun RowWithButtonAndBackButtonPreview1() {
    CommonTheme {
        RowWithButtonAndBackButton(
            buttonIcon = Icons.Default.Add,
            buttonString = "Add to cart",
            buttonOnClick = {},
            buttonColor = MaterialTheme.colors.primary,
            buttonTextColor = MaterialTheme.colors.onPrimary,
            buttonTextStyle = TextStyle(
                fontFamily = FontFamily.Default,
                fontWeight = FontWeight.Bold,
                fontSize = 14.sp,
            ),
            onBackButtonClick = {},
            internalPaddingValues = PaddingValues(0.dp, 0.dp, 0.dp, 0.dp),
            composableBackground = null,
            backgroundDrawableId = null,
            modifier = Modifier,
        )
    }
}

@Preview
@Composable
private fun RowWithButtonAndBackButtonPreview2() {
    CommonTheme {
        RowWithButtonAndBackButton(
            buttonIcon = Icons.Default.Add,
            buttonString = "Add to cart",
            buttonOnClick = {},
            buttonColor = MaterialTheme.colors.primary,
            buttonTextColor = MaterialTheme.colors.onPrimary,
            buttonTextStyle = TextStyle(
                fontFamily = FontFamily.Default,
                fontWeight = FontWeight.Bold,
                fontSize = 14.sp,
            ),
            onBackButtonClick = {},
            internalPaddingValues = PaddingValues(0.dp, 0.dp, 0.dp, 0.dp),
            composableBackground = null,
            backgroundDrawableId = R.drawable.list_item_example_background_1,
            modifier = Modifier,
        )
    }
}

@Preview
@Composable
private fun RowWithButtonAndBackButtonPreview3() {
    CommonTheme {
        RowWithButtonAndBackButton(
            buttonIcon = Icons.Default.Add,
            buttonString = "Add to cart",
            buttonOnClick = {},
            buttonColor = MaterialTheme.colors.primary,
            buttonTextColor = MaterialTheme.colors.onPrimary,
            buttonTextStyle = TextStyle(
                fontFamily = FontFamily.Default,
                fontWeight = FontWeight.Bold,
                fontSize = 14.sp,
            ),
            onBackButtonClick = {},
            internalPaddingValues = PaddingValues(0.dp, 0.dp, 0.dp, 0.dp),
            composableBackground = BackgroundsWithGradientsImpl.backgroundVerticalGradient(
                colors = listOf(
                    MaterialTheme.colors.primary,
                    MaterialTheme.colors.surface,
                ),
                radius = null,
                shape = null,
                alpha = 0.6f,
            ),
            backgroundDrawableId = null,
            modifier = Modifier,
        )
    }
}

@Preview
@Composable
private fun RowWithButtonAndBackButtonPreview4() {
    CommonTheme {
        RowWithButtonAndBackButton(
            buttonIcon = Icons.Default.Add,
            buttonString = "Add to cart",
            buttonOnClick = {},
            buttonColor = MaterialTheme.colors.primary,
            buttonTextColor = MaterialTheme.colors.onPrimary,
            buttonTextStyle = TextStyle(
                fontFamily = FontFamily.Default,
                fontWeight = FontWeight.Bold,
                fontSize = 14.sp,
            ),
            onBackButtonClick = {},
            internalPaddingValues = PaddingValues(0.dp, 0.dp, 0.dp, 0.dp),
            composableBackground = BackgroundsWithGradientsImpl.backgroundVerticalGradient(
                colors = listOf(
                    MaterialTheme.colors.primary,
                    MaterialTheme.colors.surface,
                ),
                radius = null,
                shape = null,
                alpha = 0.6f,
            ),
            backgroundDrawableId = R.drawable.list_item_example_background_1,
            modifier = Modifier,
        )
    }
}