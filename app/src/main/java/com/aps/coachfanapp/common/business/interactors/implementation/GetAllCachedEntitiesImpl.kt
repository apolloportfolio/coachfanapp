package com.aps.coachfanapp.common.business.interactors.implementation

import com.aps.coachfanapp.R
import com.aps.coachfanapp.common.business.data.cache.CacheResponseHandler
import com.aps.coachfanapp.common.business.data.cache.StandardCacheDataSource
import com.aps.coachfanapp.common.business.data.network.StandardNetworkDataSource
import com.aps.coachfanapp.common.business.data.util.safeCacheCall
import com.aps.coachfanapp.common.business.domain.state.*
import com.aps.coachfanapp.common.business.interactors.abstraction.GetAllCachedEntities
import com.aps.coachfanapp.common.util.ALog
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

private const val TAG = "GetAllCachedEntitiesImpl"
private const val LOG_ME = true

class GetAllCachedEntitiesImpl <
        Entity,
        CacheDataSource: StandardCacheDataSource<Entity>,
        NetworkDataSource: StandardNetworkDataSource<Entity>,
        ViewState: com.aps.coachfanapp.common.business.domain.state.ViewState<Entity>>
@Inject constructor(
    private val cacheDataSource: CacheDataSource
): GetAllCachedEntities<
        Entity,
        CacheDataSource,
        NetworkDataSource,
        ViewState>
{

    override fun getAllCachedEntities(
        stateEvent: StateEvent,
        returnViewState: ViewState
    ): Flow<DataState<ViewState>?> = flow {
        val methodName: String = "getAllCachedEntities"
        if (LOG_ME) ALog.d(TAG, "Method start: $methodName")
        try {
            val cacheResult = safeCacheCall(Dispatchers.IO){
                if (LOG_ME) ALog.d(TAG, "$methodName: Making safeCacheCall to get all entities from cache.")
                cacheDataSource.getAllEntities()
            }

            val response = object: CacheResponseHandler<ViewState, List<Entity>>(
                response = cacheResult,
                stateEvent = stateEvent
            ){
                override suspend fun handleSuccess(resultObj: List<Entity>): DataState<ViewState>? {
                    if (LOG_ME) ALog.d("CacheResponseHandler", "handleSuccess(): Method start")
                    if(resultObj.isNotEmpty()){
                        ALog.d("CacheResponseHandler", "handleSuccess(): resultObj.isNotEmpty")
                        returnViewState.mainEntity = resultObj[0]
                        returnViewState.listOfEntities = ArrayList(resultObj)
                        if (LOG_ME) {
                            if(LOG_ME)ALog.d(TAG, ".$methodName(): returnViewState.mainEntity == " + returnViewState.mainEntity.toString())
                            ALog.d("CacheResponseHandler", "handleSuccess(): returnViewState.listOfEntities.size() == " + (returnViewState.listOfEntities as ArrayList<Entity>).size)
                            ALog.d("CacheResponseHandler", "handleSuccess(): Method end")
                        }
                        return DataState.data(
                            response = Response(
                                messageId = R.string.error,
                                message = SEARCH_ENTITIES_SUCCESS,
                                uiComponentType = UIComponentType.None(),
                                messageType = MessageType.Success()
                            ),
                            data = returnViewState,
                            stateEvent = stateEvent
                        )
                    } else {
                        //ALog.d("GetAllCachedEntitiesImpl", "getAllCachedEntities(): resultObj.isEmpty")
                        returnViewState.noCachedEntity = true
                        if (LOG_ME) {
                            ALog.d("CacheResponseHandler", "handleSuccess(): resultObj.isEmpty")
                            ALog.d("CacheResponseHandler", "handleSuccess(): Method end")
                        }
                        return DataState.data(
                            response = Response(
                                messageId = R.string.error,
                                message = SEARCH_ENTITIES_FAILED,
                                uiComponentType = UIComponentType.None(),
                                messageType = MessageType.Success()
                            ),
                            data = returnViewState,
                            stateEvent = stateEvent
                        )
                    }

                }
            }.getResult()

            if (LOG_ME) {
                if(LOG_ME)ALog.d(TAG, "$methodName(): response == " + response.toString())
                ALog.d(TAG, "$methodName: Emitting response")
            }
            emit(response)
        } catch (e: Exception) {
            ALog.e(TAG, methodName, e)
        } finally {
            if (LOG_ME) ALog.d(TAG, "Method end: $methodName")
        }
    }

    companion object{
        const val SEARCH_ENTITIES_SUCCESS = "Successfully retrieved list of all cache entities."
        const val SEARCH_ENTITIES_FAILED = "Failed to retrieve the list of all cache entities."
    }
}