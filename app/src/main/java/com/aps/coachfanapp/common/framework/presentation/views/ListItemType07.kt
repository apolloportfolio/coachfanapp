package com.aps.coachfanapp.common.framework.presentation.views

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.foundation.layout.wrapContentWidth
import androidx.compose.material.Card
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.graphics.DefaultAlpha
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.constraintlayout.compose.ConstrainScope
import androidx.constraintlayout.compose.ConstraintLayout
import com.aps.coachfanapp.R
import com.aps.coachfanapp.common.framework.presentation.values.CommonTheme
import com.aps.coachfanapp.common.util.ALog
import com.aps.coachfanapp.common.util.composableutils.fallbackPainterResource
import com.google.firebase.storage.StorageReference

private const val TAG = "ListItemType07"
private const val LOG_ME = true

@Composable
fun ListItemType07(
    index: Int,
    itemTitleString: String?,
    itemDetail1String: String?,
    itemDetail2String: String?,
    onListItemClick: () -> Unit,
    itemRef: StorageReference?,
    itemDrawableId: Int? = null,
    padding: PaddingValues = PaddingValues(12.dp, 6.dp, 12.dp, 6.dp),
    composableBackground: DrawnBackground? = null,
    backgroundDrawableId: Int? = R.drawable.list_item_example_background_1,
    imageContentDescription: String = "",
    imageTint: Color? = MaterialTheme.colors.primary,
    itemTitleWidthFraction: Float = 0.5f,
) {
    if(LOG_ME)ALog.d(TAG, "ListItemType01(): " +
            "index == $index, itemTitleString == $itemTitleString\n")
    val transparentColor = remember {
        Color(0x00000000)
    }
    Card(
        modifier = Modifier
            .testTag(index.toString())
            .fillMaxWidth()
            .padding(padding)
            .background(transparentColor)
            .clickable { onListItemClick() },
    ) {
        BoxWithBackground(
            backgroundDrawableId = backgroundDrawableId,
            composableBackground = composableBackground,
        ) {
            ConstraintLayout(
                modifier = Modifier
                    .fillMaxWidth()
                    .wrapContentHeight()
                    .padding(padding)
            ) {

                val (itemThumbnail, itemTitle, itemDetail1, itemDetail2,
                ) = createRefs()
                val pictureIsPresent = itemRef != null || itemDrawableId != null

                val itemThumbnailConstraints: ConstrainScope.() -> Unit = {
                    top.linkTo(parent.top)
                    bottom.linkTo(parent.bottom)
                    start.linkTo(parent.start)
                }

                val itemTitleConstraints: ConstrainScope.() -> Unit = {
                    top.linkTo(parent.top)
                    bottom.linkTo(parent.bottom)
                    start.linkTo(
                        if (pictureIsPresent) {
                            itemThumbnail.end
                        } else {
                            parent.start
                        }
                    )
//                    end.linkTo(itemDetail1.start)
                }

                val itemDetail1Constraints: ConstrainScope.() -> Unit = {
                    top.linkTo(parent.top)
                    bottom.linkTo(parent.bottom)
//                    start.linkTo(itemTitle.end)
                    end.linkTo(itemDetail2.start)
                }

                val itemDetail2Constraints: ConstrainScope.() -> Unit = {
                    top.linkTo(parent.top)
                    bottom.linkTo(parent.bottom)
//                    start.linkTo(itemDetail1.end)
                    end.linkTo(parent.end)
                }

                // Image to the start.
                if(itemDrawableId != null) {
                    Image(
                        painter = fallbackPainterResource(id = itemDrawableId),
                        contentDescription = imageContentDescription,
                        modifier = Modifier
                            .constrainAs(itemThumbnail, itemThumbnailConstraints)
                            .height(56.dp)
                            .width(56.dp)
                            .background(transparentColor)
                            .padding(padding),
                        alignment = Alignment.Center,
                        contentScale = ContentScale.Fit,
                        alpha = DefaultAlpha,
                        colorFilter = if(imageTint != null) { ColorFilter.tint(imageTint) } else {null},
                    )
                } else if(itemRef != null) {
                    ImageFromFirebase(
                        itemRef,
                        modifier = Modifier
                            .constrainAs(itemThumbnail, itemThumbnailConstraints)
                            .height(56.dp)
                            .width(56.dp)
                            .background(transparentColor)
                            .padding(padding),
                        contentDescription = null,
                        contentScale = ContentScale.Fit
                    )
                }

                // Item's title
                Text(
                    modifier = Modifier
//                        .fillMaxWidth(itemTitleWidthFraction)
                        .wrapContentWidth()
                        .constrainAs(itemTitle, itemTitleConstraints)
                        .padding(
                            start = 12.dp,
                            end = 12.dp,
                        ),
                    text = itemTitleString ?: "",
                    maxLines = 2,
                    textAlign = TextAlign.Start,
                    style = TextStyle(
                        color = MaterialTheme.colors.onSurface,
                        fontWeight = FontWeight.ExtraBold,
                    ),
                    overflow = TextOverflow.Ellipsis,
                )

                // Item's detail 1
                Text(
                    modifier = Modifier
                        .wrapContentWidth()
                        .constrainAs(itemDetail1, itemDetail1Constraints)
                        .padding(
                            start = 12.dp,
                            end = 12.dp,
                        )
                        .background(transparentColor),
                    text = itemDetail1String ?: "",
                    textAlign = TextAlign.Start,
                    style = TextStyle(
                        color = MaterialTheme.colors.onSurface,
                        fontWeight = FontWeight.Normal,
                    ),
                    maxLines = 2,
                    overflow = TextOverflow.Ellipsis,
                )

                // Item's detail 2
                Text(
                    modifier = Modifier
                        .wrapContentWidth()
                        .constrainAs(itemDetail2, itemDetail2Constraints)
                        .padding(
                            start = 12.dp,
                            end = 12.dp,
                        )
                        .background(transparentColor),
                    text = itemDetail2String ?: "",
                    textAlign = TextAlign.Start,
                    style = TextStyle(
                        color = MaterialTheme.colors.onSurface,
                        fontWeight = FontWeight.Normal,
                    ),
                    maxLines = 1,
                    overflow = TextOverflow.Ellipsis,
                )

            }
        }
    }
}


// Preview =========================================================================================
@Preview
@Composable
private fun ListItemType07Preview1() {
    CommonTheme {
        ListItemType07(
            index = 0,
            itemTitleString = "Gamepad X",
            itemDetail1String = "2",
            itemDetail2String = "99.98$",
            onListItemClick = {},
            itemRef = null,
            itemDrawableId = R.drawable.example_user_profile_picture_0,
            padding = PaddingValues(4.dp, 3.dp, 4.dp, 3.dp),
            composableBackground = BackgroundsOfLayoutsImpl.backgroundListItemType01(
                colors = null,
                brush = null,
                shape = null,
                alpha = 0.6f,
            ),
            backgroundDrawableId = R.drawable.list_item_example_background_1,
            imageContentDescription = "",
            imageTint = null,
        )
    }
}
@Preview
@Composable
private fun ListItemType07Preview2() {
    CommonTheme {
        ListItemType07(
            index = 0,
            itemTitleString = "Gamepad X",
            itemDetail1String = "2",
            itemDetail2String = "99.98$",
            onListItemClick = {},
            itemRef = null,
            itemDrawableId = R.drawable.example_user_profile_picture_0,
            padding = PaddingValues(4.dp, 3.dp, 4.dp, 3.dp),
            composableBackground = BackgroundsOfLayoutsImpl.backgroundListItemType01(
                colors = null,
                brush = null,
                shape = null,
                alpha = 0.6f,
            ),
            backgroundDrawableId = null,
            imageContentDescription = "",
            imageTint = null,
        )
    }
}
@Preview
@Composable
private fun ListItemType07Preview3() {
    CommonTheme {
        ListItemType07(
            index = 0,
            itemTitleString = "Gamepad X",
            itemDetail1String = "2",
            itemDetail2String = "99.98$",
            onListItemClick = {},
            itemRef = null,
            itemDrawableId = R.drawable.example_user_profile_picture_0,
            padding = PaddingValues(4.dp, 3.dp, 4.dp, 3.dp),
            composableBackground = null,
            backgroundDrawableId = R.drawable.list_item_example_background_1,
            imageContentDescription = "",
            imageTint = null,
        )
    }
}
@Preview
@Composable
private fun ListItemType07Preview4() {
    CommonTheme {
        ListItemType07(
            index = 0,
            itemTitleString = "Artisanal Cheese Collection",
            itemDetail1String = "2",
            itemDetail2String = "99.98$",
            onListItemClick = {},
            itemRef = null,
            itemDrawableId = R.drawable.example_user_profile_picture_0,
            padding = PaddingValues(4.dp, 3.dp, 4.dp, 3.dp),
            composableBackground = null,
            backgroundDrawableId = null,
            imageContentDescription = "",
            imageTint = null,
        )
    }
}