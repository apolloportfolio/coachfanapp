package com.aps.coachfanapp.common.util;


import android.location.Location;

import com.google.android.gms.maps.model.LatLng;

import java.math.RoundingMode;
import java.text.DecimalFormat;


public class GeoLocationUtilities {

    private double radLat;  // latitude in radians
    private double radLon;  // longitude in radians

    private double degLat;  // latitude in degrees
    private double degLon;  // longitude in degrees

    private static final double MIN_LAT = Math.toRadians(-90d);  // -PI/2
    private static final double MAX_LAT = Math.toRadians(90d);   //  PI/2
    private static final double MIN_LON = Math.toRadians(-180d); // -PI
    private static final double MAX_LON = Math.toRadians(180d);  //  PI

    public static final char UNIT_MILES = 'M';
    public static final char UNIT_KM = 'K';
    public static final char UNIT_SEA_MILES = 'N';

    public static final double BEARING_NORTH = 0d;
    public static final double BEARING_NORTH_EAST = 45d;
    public static final double BEARING_EAST = 90d;
    public static final double BEARING_SOUTH_EAST = 135d;
    public static final double BEARING_SOUTH = 180d;
    public static final double BEARING_SOUTH_WEST = 225d;
    public static final double BEARING_WEST = 270d;
    public static final double BEARING_NORTH_WEST = 315d;

    public static final double EARTH_DIAMETER_KM = 6371;

    /**
     * @param latitude the latitude, in degrees.
     * @param longitude the longitude, in degrees.
     */
    public static GeoLocationUtilities fromDegrees(double latitude, double longitude) {
        GeoLocationUtilities result = new GeoLocationUtilities();
        result.radLat = Math.toRadians(latitude);
        result.radLon = Math.toRadians(longitude);
        result.degLat = latitude;
        result.degLon = longitude;
        result.checkBounds();
        return result;
    }

    /**
     * @param latitude the latitude, in radians.
     * @param longitude the longitude, in radians.
     */
    public static GeoLocationUtilities fromRadians(double latitude, double longitude) {
        GeoLocationUtilities result = new GeoLocationUtilities();
        result.radLat = latitude;
        result.radLon = longitude;
        result.degLat = Math.toDegrees(latitude);
        result.degLon = Math.toDegrees(longitude);
        result.checkBounds();
        return result;
    }

    private void checkBounds() {
        if (radLat < MIN_LAT || radLat > MAX_LAT ||
                radLon < MIN_LON || radLon > MAX_LON)
            throw new IllegalArgumentException();
    }

    /**
     * @return the latitude, in degrees.
     */
    public double getLatitudeInDegrees() {
        return degLat;
    }

    /**
     * @return the longitude, in degrees.
     */
    public double getLongitudeInDegrees() {
        return degLon;
    }

    /**
     * @return the latitude, in radians.
     */
    public double getLatitudeInRadians() {
        return radLat;
    }

    /**
     * @return the longitude, in radians.
     */
    public double getLongitudeInRadians() {
        return radLon;
    }

    @Override
    public String toString() {
        return "(" + degLat + "\u00B0, " + degLon + "\u00B0) = (" +
                radLat + " rad, " + radLon + " rad)";
    }

    /**
     * Computes the great circle distance between this GeoLocation instance
     * and the location argument.
     * @param radius the radius of the sphere, e.g. the average radius for a
     * spherical approximation of the figure of the Earth is approximately
     * 6371.01 kilometers.
     * @return the distance, measured in the same unit as the radius
     * argument.
     */
    public double distanceTo(GeoLocationUtilities location, double radius) {
        return Math.acos(Math.sin(radLat) * Math.sin(location.radLat) +
                Math.cos(radLat) * Math.cos(location.radLat) *
                        Math.cos(radLon - location.radLon)) * radius;
    }

    /**
     * <p>Computes the bounding coordinates of all points on the surface
     * of a sphere that have a great circle distance to the point represented
     * by this GeoLocation instance that is less or equal to the distance
     * argument.</p>
     * <p>For more information about the formulae used in this method visit
     * <a href="http://JanMatuschek.de/LatitudeLongitudeBoundingCoordinates">
     * http://JanMatuschek.de/LatitudeLongitudeBoundingCoordinates</a>.</p>
     * @param distance the distance from the point represented by this
     * GeoLocation instance. Must me measured in the same unit as the radius
     * argument.
     * @param radius the radius of the sphere, e.g. the average radius for a
     * spherical approximation of the figure of the Earth is approximately
     * 6371.01 kilometers.
     * @return an array of two GeoLocation objects such that:<ul>
     * <li>The latitude of any point within the specified distance is greater
     * or equal to the latitude of the first array element and smaller or
     * equal to the latitude of the second array element.</li>
     * <li>If the longitude of the first array element is smaller or equal to
     * the longitude of the second element, then
     * the longitude of any point within the specified distance is greater
     * or equal to the longitude of the first array element and smaller or
     * equal to the longitude of the second array element.</li>
     * <li>If the longitude of the first array element is greater than the
     * longitude of the second element (this is the case if the 180th
     * meridian is within the distance), then
     * the longitude of any point within the specified distance is greater
     * or equal to the longitude of the first array element
     * <strong>or</strong> smaller or equal to the longitude of the second
     * array element.</li>
     * </ul>
     */
    public GeoLocationUtilities[] boundingCoordinates(double distance, double radius) {

        if (radius < 0d || distance < 0d)
            throw new IllegalArgumentException();

        // angular distance in radians on a great circle
        double radDist = distance / radius;

        double minLat = radLat - radDist;
        double maxLat = radLat + radDist;

        double minLon, maxLon;
        if (minLat > MIN_LAT && maxLat < MAX_LAT) {
            double deltaLon = Math.asin(Math.sin(radDist) /
                    Math.cos(radLat));
            minLon = radLon - deltaLon;
            if (minLon < MIN_LON) minLon += 2d * Math.PI;
            maxLon = radLon + deltaLon;
            if (maxLon > MAX_LON) maxLon -= 2d * Math.PI;
        } else {
            // a pole is within the distance
            minLat = Math.max(minLat, MIN_LAT);
            maxLat = Math.min(maxLat, MAX_LAT);
            minLon = MIN_LON;
            maxLon = MAX_LON;
        }

        return new GeoLocationUtilities[]{fromRadians(minLat, minLon),
                fromRadians(maxLat, maxLon)};
    }

    public static double distance(double lat1, double lon1, double lat2, double lon2, char unit) {
        double theta = lon1 - lon2;
        double dist = Math.sin(deg2rad(lat1)) * Math.sin(deg2rad(lat2)) + Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * Math.cos(deg2rad(theta));
        dist = Math.acos(dist);
        dist = rad2deg(dist);
        dist = dist * 60 * 1.1515;
        if (unit == UNIT_KM) {
            dist = dist * 1.609344;
        } else if (unit == UNIT_SEA_MILES) {
            dist = dist * 0.8684;
        }
        return (dist);
    }

    public static String distanceFormattedInKM(
            Double lat1, Double lon1, Double lat2, Double lon2
    ) {
        if(lat1 == null || lon1 == null || lat2 == null || lon2 == null) return "";
        double itemsDistanceFromDevice = distance(
                lat1,
                lon1,
                lat2,
                lon2,
                GeoLocationUtilities.UNIT_KM
        );
        DecimalFormat decimalFormatDistance = new DecimalFormat("#");
        decimalFormatDistance.setRoundingMode(RoundingMode.CEILING);

        return decimalFormatDistance.format(itemsDistanceFromDevice) + " km";
    }

    /*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
    /*::  This function converts decimal degrees to radians             :*/
    /*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
    private static double deg2rad(double deg) {
        return (deg * Math.PI / 180.0);
    }

    /*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
    /*::  This function converts radians to decimal degrees             :*/
    /*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
    private static double rad2deg(double rad) {
        return (rad * 180.0 / Math.PI);
    }
    
    public static LatLng calculateCoordinatesFromBearingAndDistance(
            Double szerokoscPoczatkuStopnie, Double dlugoscPoczatkuStopnie, Double odlegloscKm, double bearingStopnie) {
        boolean loguj = false;
        LatLng result = null;
        double szerokoscMiastaRadiany = Math.toRadians(szerokoscPoczatkuStopnie);
        double dlugoscMiastaRadiany = Math.toRadians(dlugoscPoczatkuStopnie);
        double bearingRadiany = Math.toRadians(bearingStopnie);
        double dist = odlegloscKm/ GeoLocationUtilities.EARTH_DIAMETER_KM;

        double szerokoscCeluRadiany = Math.asin( Math.sin(szerokoscMiastaRadiany)*Math.cos(dist) +
                Math.cos(szerokoscMiastaRadiany)*Math.sin(dist)*Math.cos(bearingRadiany));
        double dlugoscCeluRadiany = dlugoscMiastaRadiany + Math.atan2(Math.sin(bearingRadiany)*Math.sin(dist)*Math.cos(szerokoscMiastaRadiany),
                Math.cos(dist)-Math.sin(szerokoscMiastaRadiany)*Math.sin(szerokoscCeluRadiany));

        double szerokoscCeluStopnie = Math.toDegrees(szerokoscCeluRadiany);
        double dlugoscCeluStopnie = Math.toDegrees(dlugoscCeluRadiany);
        result = new LatLng(szerokoscCeluStopnie, dlugoscCeluStopnie);
        return result;
    }

    public static Location calculateCoordinatesFromBearingAndDistance(
            Location originPoint, Double odlegloscKm, double bearingStopnie) {
        boolean loguj = false;
        Location result = new Location(originPoint);
        double szerokoscMiastaRadiany = Math.toRadians(originPoint.getLatitude());
        double dlugoscMiastaRadiany = Math.toRadians(originPoint.getLongitude());
        double bearingRadiany = Math.toRadians(bearingStopnie);
        double dist = odlegloscKm/ GeoLocationUtilities.EARTH_DIAMETER_KM;

        double szerokoscCeluRadiany = Math.asin( Math.sin(szerokoscMiastaRadiany)*Math.cos(dist) +
                Math.cos(szerokoscMiastaRadiany)*Math.sin(dist)*Math.cos(bearingRadiany));
        double dlugoscCeluRadiany = dlugoscMiastaRadiany + Math.atan2(Math.sin(bearingRadiany)*Math.sin(dist)*Math.cos(szerokoscMiastaRadiany),
                Math.cos(dist)-Math.sin(szerokoscMiastaRadiany)*Math.sin(szerokoscCeluRadiany));

        double szerokoscCeluStopnie = Math.toDegrees(szerokoscCeluRadiany);
        double dlugoscCeluStopnie = Math.toDegrees(dlugoscCeluRadiany);
        result.setLatitude(szerokoscCeluStopnie);
        result.setLongitude(dlugoscCeluStopnie);
        return result;
    }

    // http://www.movable-type.co.uk/scripts/latlong.html
    // http://stackoverflow.com/questions/10119479/calculating-lat-and-long-from-bearing-and-distance
    public static LatLng calculateCoordinatesStraightToRightOnMap(
            Double szerokoscPoczatkuStopnie, Double dlugoscPoczatkuStopnie, Double odlegloscKm) {
        LatLng result = null;
        double szerokoscMiastaRadiany = Math.toRadians(szerokoscPoczatkuStopnie);
        double dlugoscMiastaRadiany = Math.toRadians(dlugoscPoczatkuStopnie);
        double bearingRadiany = Math.toRadians(BEARING_EAST);
        double dist = odlegloscKm/GeoLocationUtilities.EARTH_DIAMETER_KM;

        double szerokoscCeluRadiany = Math.asin( Math.sin(szerokoscMiastaRadiany)*Math.cos(dist) +
                Math.cos(szerokoscMiastaRadiany)*Math.sin(dist)*Math.cos(bearingRadiany));
        double dlugoscCeluRadiany = dlugoscMiastaRadiany + Math.atan2(Math.sin(bearingRadiany)*Math.sin(dist)*Math.cos(szerokoscMiastaRadiany),
                Math.cos(dist)-Math.sin(szerokoscMiastaRadiany)*Math.sin(szerokoscCeluRadiany));

        double szerokoscCeluStopnie = Math.toDegrees(szerokoscCeluRadiany);
        double dlugoscCeluStopnie = Math.toDegrees(dlugoscCeluRadiany);
        result = new LatLng(szerokoscPoczatkuStopnie, dlugoscCeluStopnie);
        return result;
    }

    // http://www.movable-type.co.uk/scripts/latlong.html
    // http://stackoverflow.com/questions/10119479/calculating-lat-and-long-from-bearing-and-distance
    public static LatLng calculateCoordinatesStraightToLeftOnMap(
            Double szerokoscPoczatkuStopnie, Double dlugoscPoczatkuStopnie, Double odlegloscKm) {
        boolean loguj = false;
        LatLng result = null;
        double szerokoscMiastaRadiany = Math.toRadians(szerokoscPoczatkuStopnie);
        double dlugoscMiastaRadiany = Math.toRadians(dlugoscPoczatkuStopnie);
        double bearingRadiany = Math.toRadians(BEARING_WEST);
        double dist = odlegloscKm/GeoLocationUtilities.EARTH_DIAMETER_KM;

        double szerokoscCeluRadiany = Math.asin( Math.sin(szerokoscMiastaRadiany)*Math.cos(dist) +
                Math.cos(szerokoscMiastaRadiany)*Math.sin(dist)*Math.cos(bearingRadiany));
        double dlugoscCeluRadiany = dlugoscMiastaRadiany + Math.atan2(Math.sin(bearingRadiany)*Math.sin(dist)*Math.cos(szerokoscMiastaRadiany),
                Math.cos(dist)-Math.sin(szerokoscMiastaRadiany)*Math.sin(szerokoscCeluRadiany));

        double szerokoscCeluStopnie = Math.toDegrees(szerokoscCeluRadiany);
        double dlugoscCeluStopnie = Math.toDegrees(dlugoscCeluRadiany);
        result = new LatLng(szerokoscPoczatkuStopnie, dlugoscCeluStopnie);
        return result;
    }

    // http://www.movable-type.co.uk/scripts/latlong.html
    // http://stackoverflow.com/questions/10119479/calculating-lat-and-long-from-bearing-and-distance
    public static LatLng calculateCoordinatesStraightToUpOnMap(
            Double szerokoscPoczatkuStopnie, Double dlugoscPoczatkuStopnie, Double odlegloscKm) {
        LatLng result = null;
        double szerokoscMiastaRadiany = Math.toRadians(szerokoscPoczatkuStopnie);
        double dlugoscMiastaRadiany = Math.toRadians(dlugoscPoczatkuStopnie);
        double bearingRadiany = Math.toRadians(BEARING_NORTH);
        double dist = odlegloscKm/GeoLocationUtilities.EARTH_DIAMETER_KM;

        double szerokoscCeluRadiany = Math.asin( Math.sin(szerokoscMiastaRadiany)*Math.cos(dist) +
                Math.cos(szerokoscMiastaRadiany)*Math.sin(dist)*Math.cos(bearingRadiany));
        double dlugoscCeluRadiany = dlugoscMiastaRadiany + Math.atan2(Math.sin(bearingRadiany)*Math.sin(dist)*Math.cos(szerokoscMiastaRadiany),
                Math.cos(dist)-Math.sin(szerokoscMiastaRadiany)*Math.sin(szerokoscCeluRadiany));

        double szerokoscCeluStopnie = Math.toDegrees(szerokoscCeluRadiany);
        double dlugoscCeluStopnie = Math.toDegrees(dlugoscCeluRadiany);
        result = new LatLng(szerokoscCeluStopnie, dlugoscPoczatkuStopnie);
        return result;
    }

    public static LatLng roundCoordinatesToNMetres(
            Double longitude,
            Double lattitude, int n) {
        LatLng result = null;
        try {
            double multiplier = 0;
            if(n < 0.1) {
                multiplier = Math.pow(10, 8);
            } else if(n < 1) {
                multiplier = Math.pow(10, 7);
            } else if(n < 10) {
                multiplier = Math.pow(10, 6);
            } else if(n < 10) {
                multiplier = Math.pow(10, 5);
            } else if(n < 100) {
                multiplier = Math.pow(10, 4);
            } else if(n < 1000) {
                multiplier = Math.pow(10, 3);
            } else if(n < 10000) {
                multiplier = Math.pow(10, 2);
            } else if(n < 100000) {
                multiplier = Math.pow(10, 1);
            } else {
                multiplier = Math.pow(10, 0);
            }
            double lat = Math.round(longitude*multiplier)/multiplier;
            double lon = Math.round(lattitude*multiplier)/multiplier;
            result = new LatLng(lat, lon);
        } catch(Exception e) {

        }
        return result;
    }

    public static Location latLngToLocation(LatLng latLng) {
        Location result = new Location("GeoLocationUtilities");
        result.setLatitude(latLng.latitude);
        result.setLongitude(latLng.longitude);
        return result;
    }

    public static Location generateLocationAroundPoint(Location point, double radiusKM) {
        double bearing = ARandomGenerator.getRandomInteger(0, 360);
        return calculateCoordinatesFromBearingAndDistance(point, radiusKM, bearing);
    }
}

