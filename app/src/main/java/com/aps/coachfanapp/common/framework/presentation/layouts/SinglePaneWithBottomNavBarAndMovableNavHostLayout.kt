package com.aps.coachfanapp.common.framework.presentation.layouts

import android.annotation.SuppressLint
import androidx.compose.material.DrawerDefaults
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Scaffold
import androidx.compose.material.contentColorFor
import androidx.compose.material.rememberScaffoldState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.navigation.NavHostController
import com.aps.coachfanapp.common.business.domain.state.StateEvent
import com.aps.coachfanapp.common.framework.presentation.DrawerData
import com.aps.coachfanapp.common.framework.presentation.NavItem
import com.aps.coachfanapp.common.util.ALog
import com.aps.coachfanapp.common.framework.presentation.views.BottomNavigationBar

private const val TAG = "SinglePaneWithBottomNavBarAndMovableNavHostLayout"
private const val LOG_ME = true

const val TAG_DRAWER_SHOWING = "DRAWER_SHOWING: "

@SuppressLint("UnusedMaterialScaffoldPaddingParameter")
@OptIn(ExperimentalMaterialApi::class)
@Composable
fun SinglePaneWithBottomNavBarAndMovableNavHostLayout(
    launchStateEvent: (StateEvent) -> Unit = {},
    launchStateEventSetDrawerOpen: (Boolean?) -> Unit,

    leftPaneNavController: NavHostController,
    rightPaneNavController: NavHostController,
    navItems: List<NavItem>,
    navHost: @Composable () -> Unit,

    topBar:  @Composable () -> Unit = {},
    drawerData: DrawerData? = null,

    isDrawerOpen: Boolean? = null,
) {
    if(LOG_ME) ALog.d(TAG, "(): Recomposition start.")
    val drawerBackgroundColor = drawerData?.drawerBackgroundColor ?: MaterialTheme.colors.surface
    val scaffoldState = rememberScaffoldState()

    Scaffold(
        scaffoldState = scaffoldState,
        topBar = topBar,
        drawerContent = drawerData?.drawerContent,
        drawerGesturesEnabled = scaffoldState.drawerState.isOpen,
        drawerShape = drawerData?.drawerShape ?: MaterialTheme.shapes.large,
        drawerElevation = drawerData?.drawerElevation ?: DrawerDefaults.Elevation,
        drawerBackgroundColor = drawerBackgroundColor,
        drawerContentColor = drawerData?.drawerContentColor ?: contentColorFor(drawerBackgroundColor),
        drawerScrimColor = drawerData?.drawerScrimColor ?: DrawerDefaults.scrimColor,

        bottomBar = {
            BottomNavigationBar(
                items = navItems,
                navController = leftPaneNavController,
                onItemClick = { navItem: NavItem ->
                    navItem.onItemClickNavigation(
                        false,
                        navItem,
                        leftPaneNavController,
                        rightPaneNavController,
                    )
                }
            )
        },
    ) {it
        navHost()
    }

    LaunchedEffect(isDrawerOpen) {
        if(LOG_ME)ALog.d(TAG_DRAWER_SHOWING+TAG,
            "SinglePaneWithBottomNavBarAndMovableNavHostLayout()." +
                    "LaunchedEffect($isDrawerOpen): Managing drawer state.")
        isDrawerOpen?.run {
            if(isDrawerOpen) {
                if(LOG_ME)ALog.d(TAG_DRAWER_SHOWING+TAG,
                    "SinglePaneWithBottomNavBarAndMovableNavHostLayout()." +
                            "LaunchedEffect($isDrawerOpen): viewState demands opening drawer.")

                if(scaffoldState.drawerState.isClosed) {
                    if(LOG_ME)ALog.d(TAG_DRAWER_SHOWING+TAG,
                        "SinglePaneWithBottomNavBarAndMovableNavHostLayout()." +
                                "LaunchedEffect($isDrawerOpen): Drawer was closed. Opening it.")

                    scaffoldState.drawerState.open()
                    launchStateEventSetDrawerOpen(null)
//                    launchStateEvent(HomeScreenStateEvent.SetDrawerOpen(null))
                }
            } else {
                if(LOG_ME)ALog.d(TAG_DRAWER_SHOWING+TAG,
                    "SinglePaneWithBottomNavBarAndMovableNavHostLayout()." +
                            "LaunchedEffect($isDrawerOpen): viewState demands closing drawer.")

                if(scaffoldState.drawerState.isOpen) {
                    if(LOG_ME)ALog.d(TAG_DRAWER_SHOWING+TAG,
                        "SinglePaneWithBottomNavBarAndMovableNavHostLayout()." +
                                "LaunchedEffect($isDrawerOpen): Drawer was open. Closing it.")
                    scaffoldState.drawerState.close()
                    launchStateEventSetDrawerOpen(null)
//                    launchStateEvent(HomeScreenStateEvent.SetDrawerOpen(null))
                }
            }
        }

    }
    if(LOG_ME) ALog.d(TAG, "(): Recomposition end.")
}