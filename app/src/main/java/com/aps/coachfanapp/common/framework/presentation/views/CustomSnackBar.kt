package com.aps.coachfanapp.common.framework.presentation.views


import android.os.Parcelable
import androidx.annotation.DrawableRes
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Icon
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Snackbar
import androidx.compose.material.SnackbarDuration
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalLayoutDirection
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.semantics.Role
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.LayoutDirection
import androidx.compose.ui.unit.dp
import androidx.constraintlayout.compose.ConstraintLayout
import com.aps.coachfanapp.R
import com.aps.coachfanapp.common.framework.presentation.layouts.CustomSnackBarSwipeableWithGradient
import com.aps.coachfanapp.common.framework.presentation.values.CommonTheme
import com.aps.coachfanapp.common.util.ALog
import kotlinx.android.parcel.Parcelize
import java.io.Serializable

private const val TAG = "CustomSnackBar"
private const val LOG_ME = true
// Reference: https://www.youtube.com/watch?v=WLQzjtIUVmY

@Preview
@Composable
fun CustomSnackBarSwipeableWithGradientPreview() {
    CommonTheme {
        Box(modifier = Modifier
            .fillMaxWidth()
        ) {
            Image(
                painterResource(id = R.drawable.example_background_1),
                contentDescription = "",
                contentScale = ContentScale.FillBounds,
                modifier = Modifier.matchParentSize()
            )

            CustomSnackBarSwipeableWithGradient(
                iconDrawableResID = R.drawable.ic_launcher,
                message = "CustomSnackBarPreview",
                actionMessage = "Action!",
                onActionClick = {
                    if(LOG_ME)ALog.d(TAG, "(): SnackBar clicked.")
                },
                colors = listOf(
                    MaterialTheme.colors.primary,
                    MaterialTheme.colors.primaryVariant
                ),
                textColor = MaterialTheme.colors.secondaryVariant,
            )
        }

    }
}

@Preview
@Composable
fun CustomSnackBarPreview() {
    CommonTheme {
        CustomSnackBar(
            iconDrawableResID = R.drawable.ic_launcher,
            message = "CustomSnackBarPreview",
            actionMessage = "Action!",
            onActionClick = {
                if(LOG_ME)ALog.d(TAG, "(): SnackBar clicked.")
            },
            isRtl = false,
            contentColor = MaterialTheme.colors.onPrimary,
            containerColor = MaterialTheme.colors.primary,
        )
    }
}




@Preview
@Composable
fun CustomSnackBarInvertedPreview() {
    CommonTheme {
        CustomSnackBarInverted(
            iconDrawableResID = R.drawable.ic_launcher,
            message = "CustomSnackBarPreview",
            actionMessage = "Action!",
            isRtl = false,
            contentColor = MaterialTheme.colors.primary,
            containerColor = MaterialTheme.colors.onPrimary,
        )
    }
}

@Preview
@Composable
fun CustomSnackBarOptionalPreview() {
    CommonTheme {
        CustomSnackBarInverted(
            iconDrawableResID = R.drawable.ic_launcher,
            message = "CustomSnackBarPreview",
            actionMessage = "Action!",
            isRtl = false,
            contentColor = MaterialTheme.colors.primary,
            containerColor = MaterialTheme.colors.onPrimary,
        )
    }
}

@Composable
fun CustomSnackBar(
    @DrawableRes iconDrawableResID: Int?,
    message: String,
    actionMessage: String? = null,
    onActionClick: () -> Unit,
    isRtl: Boolean = false,
    contentColor: Color = MaterialTheme.colors.onPrimary,
    containerColor: Color = MaterialTheme.colors.primary,
) {
    if(LOG_ME)ALog.d(TAG, "CustomSnackBar(): Recomposition start")
    Snackbar(
        actionOnNewLine = false,
        shape = MaterialTheme.shapes.small,
        backgroundColor = containerColor,
        contentColor = contentColor,
        elevation = 6.dp,
        action = {
            actionMessage?.let {
                Text(
                    text = actionMessage,
                    color = contentColor,
                    modifier = Modifier
                        .background(Color.Transparent)
                        .padding(
                            start = 4.dp,
                            end = 4.dp,
                            top = 4.dp,
                            bottom = 4.dp,
                        )
                        .clickable { onActionClick() },
                )
            }
        }
    ) {
        CompositionLocalProvider(
            LocalLayoutDirection provides
                    if (isRtl) LayoutDirection.Rtl else LayoutDirection.Ltr
        ) {
            Row(
                horizontalArrangement= Arrangement.Start,
                verticalAlignment= Alignment.CenterVertically,
            ) {
                if(iconDrawableResID != null) {
                    Icon(
                        painter = painterResource(id = iconDrawableResID),
                        contentDescription = null,
                        modifier = Modifier
                            .padding(
                                start = 4.dp,
                                end = 4.dp,
                                top = 4.dp,
                                bottom = 4.dp,
                            ),
                        tint = contentColor,
                    )
                }

                Text(
                    text = message,
                    color = contentColor,
                )
            }
        }
    }
    if(LOG_ME)ALog.d(TAG, "CustomSnackBar(): Recomposition end")
}



@Composable
fun CustomSnackBarInverted(
    @DrawableRes iconDrawableResID: Int?,
    message: String,
    actionMessage: String? = null,
    isRtl: Boolean = false,
    contentColor: Color = MaterialTheme.colors.primary,
    containerColor: Color = MaterialTheme.colors.onPrimary,
) {
    Snackbar(
        backgroundColor = containerColor,
        action = {
            actionMessage?.let {
                Text(
                    text = actionMessage,
                    color = contentColor,
                    modifier = Modifier
                        .background(Color.Transparent)
                        .padding(
                            start = 4.dp,
                            end = 4.dp,
                            top = 4.dp,
                            bottom = 4.dp,
                        )
                )
            }
        }
    ) {
        CompositionLocalProvider(
            LocalLayoutDirection provides
                    if (isRtl) LayoutDirection.Rtl else LayoutDirection.Ltr
        ) {
            Row(
                horizontalArrangement= Arrangement.Start,
                verticalAlignment= Alignment.CenterVertically,
            ) {
                if(iconDrawableResID != null) {
                    Icon(
                        painterResource(id = iconDrawableResID),
                        contentDescription = null,
                        modifier = Modifier
                            .padding(
                                start = 4.dp,
                                end = 4.dp,
                                top = 4.dp,
                                bottom = 4.dp,
                            ),
                        tint = contentColor,
                    )
                }

                Text(
                    text = message,
                    color = contentColor,
                )
            }
        }
    }
}

@Deprecated(message = "This composable is just for research purposes.")
@Composable
fun CustomSnackBarOptional(
    @DrawableRes backgroundDrawableResID: Int = R.drawable.example_background_1,
    @DrawableRes iconDrawableResID: Int?,
    message: String,
    actionMessage: String? = null,
    onAction: () -> Unit = {},
    isRtl: Boolean = false,
    contentColor: Color = MaterialTheme.colors.primary,
    containerColor: Color = MaterialTheme.colors.onPrimary,
) {
    Box(
        modifier = Modifier
            .fillMaxWidth()
    ) {
        Image(
            painterResource(id = backgroundDrawableResID),
            contentDescription = "",
            contentScale = ContentScale.FillBounds,
            modifier = Modifier
                .matchParentSize()
                .padding(
                    start = 4.dp,
                    end = 4.dp,
                    top = 4.dp,
                    bottom = 4.dp,
                )
        )
        SnackBarContent(
            backgroundDrawableResID,
            iconDrawableResID,
            message,
            actionMessage,
            onAction,
            isRtl,
            contentColor,
            containerColor,
        )
    }
}

@Deprecated(message = "This composable is just for research purposes.")
@Composable
internal fun SnackBarContent(
    @DrawableRes backgroundDrawableResID: Int = R.drawable.example_background_1,
    @DrawableRes iconDrawableResID: Int?,
    message: String,
    actionMessage: String? = null,
    onAction: () -> Unit = {},
    isRtl: Boolean = false,
    contentColor: Color = MaterialTheme.colors.primary,
    containerColor: Color = MaterialTheme.colors.onPrimary,
) {
    Snackbar(
        backgroundColor = containerColor,
    ) {
        CompositionLocalProvider(
            LocalLayoutDirection provides
                    if (isRtl) LayoutDirection.Rtl else LayoutDirection.Ltr
        ) {
            ConstraintLayout(
                modifier = Modifier
                    .fillMaxWidth()
            ) {
                val (
                    iconRef,
                    messageRef,
                    actionRef,
                ) = createRefs()

                if(iconDrawableResID != null) {
                    Icon(
                        painter = painterResource(id = iconDrawableResID),
                        contentDescription = null,
                        modifier = Modifier
                            .padding(
                                start = 4.dp,
                                end = 4.dp,
                                top = 4.dp,
                                bottom = 4.dp,
                            )
                            .constrainAs(iconRef) {
                                top.linkTo(parent.top)
                                bottom.linkTo(parent.bottom)
                                start.linkTo(parent.start)
                            },
                        tint = contentColor,
                    )
                } else {
                    if(LOG_ME) ALog.d(TAG, ": " +
                            "drawableRes == null")
                }
                Text(
                    text = message,
                    color = contentColor,
                    modifier = Modifier
                        .constrainAs(messageRef) {
                            top.linkTo(parent.top)
                            bottom.linkTo(parent.bottom)
                            start.linkTo(iconRef.end)
                        }
                )
                actionMessage?.let {
                    Text(
                        text = actionMessage,
                        color = contentColor,
                        modifier = Modifier
                            .background(Color.Transparent)
                            .padding(
                                start = 4.dp,
                                end = 4.dp,
                                top = 4.dp,
                                bottom = 4.dp,
                            )
                            .constrainAs(actionRef) {
                                top.linkTo(parent.top)
                                bottom.linkTo(parent.bottom)
                                end.linkTo(parent.end)
                            }
                            .clickable(
                                role = Role.Button,
                                onClick = onAction
                            )
                    )
                }
            }
        }
    }
}

@Parcelize
data class SnackBarState(
    var show: Boolean = false,
    var message: String = "",
    var actionLabel: String? = null,
    @DrawableRes var iconDrawableRes: Int? = null,
    var onClickAction: () -> Unit = {},
    var onDismissAction: () -> Unit = {},
    var duration: SnackbarDuration = SnackbarDuration.Short,
    var updateSnackBarInViewModel: (SnackBarState?) -> Unit,
) : Parcelable, Serializable{
    override fun toString(): String {
        return buildString {
            appendLine("SnackBarState:")
            appendLine("show: $show")
            appendLine("message: $message")
            appendLine("actionLabel: $actionLabel")
            appendLine("iconDrawableRes: $iconDrawableRes")
            appendLine("onClickAction: Function<Unit>")
            appendLine("onDismissAction: Function<Unit>")
            appendLine("duration: $duration")
            appendLine("updateSnackBarInViewModel: Function<SnackBarState?>")
        }
    }
}