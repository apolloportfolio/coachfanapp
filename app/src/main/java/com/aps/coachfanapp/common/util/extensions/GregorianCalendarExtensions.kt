package com.aps.coachfanapp.common.util.extensions

import java.util.*

fun GregorianCalendar?.toLogString() : String {
    return this?.let {
        "${this.get(GregorianCalendar.DAY_OF_MONTH)}." +
                "${this.get(GregorianCalendar.MONTH)+1}." +
                "${this.get(GregorianCalendar.YEAR)} " +
                "${this.get(GregorianCalendar.HOUR)}:" +
                "${this.get(GregorianCalendar.MINUTE)}:" +
                "${this.get(GregorianCalendar.SECOND)}    " +
                "${this.timeInMillis}\n" +
//                "timezone == ${this.timeZone}\n" +
//                "$this" +
                ""
    } ?: "null"
}