package com.aps.coachfanapp.common.framework.presentation.views

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.material.Card
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.AddShoppingCart
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.LocalContentColor
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.geometry.Size
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.graphics.DefaultAlpha
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.layout.onGloballyPositioned
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.res.*
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.tooling.preview.PreviewParameter
import androidx.compose.ui.tooling.preview.PreviewParameterProvider
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.toSize
import androidx.constraintlayout.compose.ConstraintLayout
import com.aps.coachfanapp.common.framework.presentation.values.CommonTheme
import com.aps.coachfanapp.common.util.ALog
import com.aps.coachfanapp.common.util.PreviewEntityFactory
import com.aps.coachfanapp.common.util.composableutils.fallbackPainterResource
import com.aps.coachfanapp.common.util.composableutils.getDrawableIdInPreview
import com.google.firebase.storage.StorageReference
import com.gowtham.ratingbar.RatingBar
import com.gowtham.ratingbar.RatingBarStyle
import kotlin.math.roundToInt
import com.aps.coachfanapp.R

private const val TAG = "ListItemType02"
private const val LOG_ME = true

@Composable
fun ListItemType02(
    index: Int,
    itemTitleString: String?,
    itemDescription: String?,
    onListItemClick: () -> Unit,
    onItemsButtonClick: () -> Unit = {},
    getItemsRating: () -> Float? = { null },
    itemRef: StorageReference?,
    itemDrawableId: Int? = null,
    composableBackground: DrawnBackground? = null,
    backgroundDrawableId: Int = R.drawable.list_item_example_background_1,
    imageContentDescription: String = "",
    imageTint: Color? = MaterialTheme.colors.primary,
    buttonTint: Color? = MaterialTheme.colors.primary,
    buttonContentDescription: String? = null,
    buttonImageVector: ImageVector? = Icons.Default.AddShoppingCart,
    itemDescriptionFontWeight: FontWeight = FontWeight.Bold,
) {
    if(LOG_ME)ALog.d(TAG, "ListItemType02(): " +
            "index == $index, itemTitleString == $itemTitleString\n")
    val transparentColor = remember {
        Color(0x00000000)
    }
    val startMargin = remember { 4 }.dp
    val endMargin = remember { 4 }.dp
    val topMargin = dimensionResource(id = R.dimen.list_item_vertical_margin)
    val bottomMargin = dimensionResource(id = R.dimen.list_item_vertical_margin)
    Card(
        modifier = Modifier
            .testTag(index.toString())
            .fillMaxWidth()
            .padding(
                start = dimensionResource(id = R.dimen.list_tile_horizontal_margin),
                end = dimensionResource(id = R.dimen.list_tile_horizontal_margin),
                top = dimensionResource(id = R.dimen.list_tile_vertical_margin),
                bottom = dimensionResource(id = R.dimen.list_tile_vertical_margin)
            )
            .background(transparentColor)
            .clickable { onListItemClick() },
    ) {
        BoxWithBackground(
            backgroundDrawableId = backgroundDrawableId,
            composableBackground = composableBackground,
        ) {

            ConstraintLayout(
                modifier = Modifier
                    .fillMaxWidth()
                    .wrapContentHeight()
                    .padding(
                        start = startMargin,
                        end = endMargin,
                        top = topMargin,
                        bottom = bottomMargin,
                    )
            ) {

                val (itemThumbnail, itemTitle, itemButton, description, userRatingBar,
                ) = createRefs()

                val itemsRating = remember { getItemsRating() }
                val thumbnailPresent = itemRef != null || itemDrawableId != null

                // Image to the start.
                if(itemRef != null) {
                    ImageFromFirebase(
                        itemRef,
                        modifier = Modifier
                            .constrainAs(itemThumbnail) {
                                top.linkTo(parent.top)
                                bottom.linkTo(parent.bottom)
                                start.linkTo(parent.start)
                                end.linkTo(itemTitle.start)
                            }
                            .height(56.dp)
                            .width(56.dp)
                            .background(transparentColor)
                            .padding(
                                start = startMargin,
                                end = endMargin,
                                top = topMargin,
                                bottom = bottomMargin
                            ),
                        contentDescription = null,
                        contentScale = ContentScale.Fit
                    )
                } else if(itemDrawableId != null) {
                    Image(
                        painter = fallbackPainterResource(id = itemDrawableId),
                        contentDescription = imageContentDescription,
                        modifier = Modifier
                            .constrainAs(itemThumbnail) {
                                top.linkTo(parent.top)
                                bottom.linkTo(parent.bottom)
                                start.linkTo(parent.start)
                                end.linkTo(itemTitle.start)
                            }
                            .height(56.dp)
                            .width(56.dp)
                            .background(transparentColor)
                            .padding(
                                start = startMargin,
                                end = endMargin,
                                top = topMargin,
                                bottom = bottomMargin
                            ),
                        alignment = Alignment.Center,
                        contentScale = ContentScale.Fit,
                        alpha = DefaultAlpha,
                        colorFilter = if(imageTint != null) { ColorFilter.tint(imageTint) } else {null},
                    )
                }


                // Item's title
                Text(
                    modifier = Modifier
                        .constrainAs(itemTitle) {
                            top.linkTo(parent.top)
                            start.linkTo(itemThumbnail.end)
                        }
                        .fillMaxWidth(0.7f)
                        .padding(
                            start = startMargin,
                            top = topMargin,
                            end = dimensionResource(id = R.dimen.margin_between_text_views),
                        ),
                    text = itemTitleString ?: "",
                    maxLines = 1,
                    textAlign = TextAlign.Start,
                    style = TextStyle(
                        color = MaterialTheme.colors.onSurface,
                        fontWeight = FontWeight.Bold
                    ),
                    overflow = TextOverflow.Ellipsis,
                )


                if(buttonImageVector != null) {
                    IconButton(
                        onClick = { onItemsButtonClick() },
                        modifier = Modifier
                            .constrainAs(itemButton) {
                                top.linkTo(parent.top)
                                bottom.linkTo((parent.bottom))
                                end.linkTo(parent.end)
                            }
                            .height(56.dp)
                            .width(56.dp)
                            .background(transparentColor)
                            .padding(
                                start = startMargin,
                                end = endMargin,
                                top = topMargin,
                                bottom = bottomMargin
                            ),
                    ) {
                        Icon(
                            imageVector = buttonImageVector,
                            contentDescription = buttonContentDescription,
                            tint = buttonTint ?: LocalContentColor.current,
                        )
                    }
                }


                Text(
                    modifier = Modifier
                        .fillMaxWidth(
                            if(itemsRating != null) {
                                0.2f
                            } else {
                                0.7f
                            }
                        )
                        .constrainAs(description) {
                            if(itemsRating != null) {
                                start.linkTo(userRatingBar.end)
                                centerVerticallyTo(userRatingBar)
                            } else {
//                                start.linkTo(parent.start)
                                start.linkTo(
                                    if(thumbnailPresent) {
                                        itemThumbnail.end
                                    } else {
                                        parent.start
                                    }
                                )
//                                end.linkTo(itemButton.start)
                                top.linkTo(itemTitle.bottom)
                                bottom.linkTo(parent.bottom)
                            }
                        }
                        .padding(
                            start = if(itemsRating != null) {
                                0.dp
                            } else {
                                startMargin
                           },
                            end = endMargin,
                            top = topMargin,
                            bottom = bottomMargin
                        )
                        .background(transparentColor),
                    text = itemDescription ?: "",
                    textAlign = if(itemsRating != null) { TextAlign.Center } else { TextAlign.Start },
                    style = TextStyle(
                        color = MaterialTheme.colors.onSurface,
                        fontWeight = itemDescriptionFontWeight
                    ),
                    maxLines = if(itemsRating != null) { 1 } else { 3 },
                )

                if(itemsRating != null) {
                    val maximumNumberOfIndicators = remember { 5 }
                    val fractionOfParentsWidth = remember { 0.25f }
                    val spaceBetweenIndicators = remember { 1.dp }
                    var indicatorSize = remember { 14 }
                    RatingBar(
                        size = indicatorSize.dp,
                        spaceBetween = spaceBetweenIndicators,
                        numOfStars = maximumNumberOfIndicators,
                        value = itemsRating,
                        style = RatingBarStyle.Fill(),
                        onValueChange = {},
                        onRatingChanged = {
                            ALog.d("TAG", "onRatingChanged: $it")
                        },
                        isIndicator = true,
                        modifier = Modifier
                            .padding(
                                top = topMargin,
                                bottom = bottomMargin,
                            )
                            .constrainAs(userRatingBar) {
                                top.linkTo(itemTitle.bottom)
                                bottom.linkTo(parent.bottom)
                                start.linkTo(itemThumbnail.end)
                            }
                            .fillMaxWidth(fractionOfParentsWidth)
                            .wrapContentWidth()
                            .onGloballyPositioned {
                                //here u can access the parent layout coordinate size
                                val parentSize =
                                    it.parentLayoutCoordinates?.size?.toSize() ?: Size.Zero
                                val spaceLeftForIndicators =
                                    parentSize.width - maximumNumberOfIndicators * spaceBetweenIndicators.value
                                indicatorSize =
                                    (spaceLeftForIndicators * fractionOfParentsWidth / maximumNumberOfIndicators).roundToInt()
                                ALog.d(
                                    "ListItemType02", ": " +
                                            "indicatorSize == $indicatorSize"
                                )
                            },
                    )
                }
            }
        }
    }
}


// Preview =========================================================================================
data class ListItemType02Params(
    val itemTitleString: String?,
    val lblNextToRatingString: String?,
    val onListItemClick: () -> Unit,
    val onItemsButtonClick: () -> Unit,
    val getItemsRating: () -> Float?,
    val ratingBarColor: Color,
)

class ListItemType02ParamsProvider : PreviewParameterProvider<ListItemType02Params> {
    override val values: Sequence<ListItemType02Params> = sequenceOf(
        ListItemType02Params(
            itemTitleString = "Lorem ipsum dolor sit amet",
            lblNextToRatingString = "12.99USD",
            onListItemClick = {},
            onItemsButtonClick = {},
            getItemsRating = { 5f },
            ratingBarColor = Color(0xFFFFCC01)
        ),
    )
}

@Preview
@Composable
fun ListItemType02Preview1(
    @PreviewParameter(
        ListItemType02ParamsProvider::class
    ) params: ListItemType02Params
) {
    CommonTheme {
        ListItemType02(
            index = 0,
            itemTitleString = params.itemTitleString,
            itemDescription = params.lblNextToRatingString,
            onListItemClick = params.onListItemClick,
            onItemsButtonClick = params.onListItemClick,
            getItemsRating = params.getItemsRating,
            itemRef = null,
            itemDrawableId = R.drawable.example_user_profile_picture_0,
            imageTint = null,
            backgroundDrawableId = R.drawable.example_background_1,
            composableBackground = null,
        )
    }
}

@Preview
@Composable
fun ListItemType02Preview2(
    @PreviewParameter(
        ListItemType02ParamsProvider::class
    ) params: ListItemType02Params
) {
    CommonTheme {
        ListItemType02(
            index = 0,
            itemTitleString = params.itemTitleString,
            itemDescription = params.lblNextToRatingString,
            onListItemClick = params.onListItemClick,
            onItemsButtonClick = params.onListItemClick,
            getItemsRating = params.getItemsRating,
            itemRef = null,
            itemDrawableId = R.drawable.example_user_profile_picture_0,
            imageTint = null,
            backgroundDrawableId = R.drawable.example_background_1,
            composableBackground = BackgroundsOfLayoutsImpl.backgroundListItemType01(
                colors = null,
                brush = null,
                shape = null,
                alpha = 0.6f,
            ),
        )
    }
}

@Preview
@Composable
fun ListItemType02Preview3(
    @PreviewParameter(
        ListItemType02ParamsProvider::class
    ) params: ListItemType02Params
) {
    CommonTheme {
        ListItemType02(
            index = 0,
            itemTitleString = params.itemTitleString,
            itemDescription = params.lblNextToRatingString,
            onListItemClick = params.onListItemClick,
            onItemsButtonClick = params.onListItemClick,
            getItemsRating = { null },
            itemRef = null,
            itemDrawableId = null,
            imageTint = null,
            backgroundDrawableId = R.drawable.example_background_1,
            composableBackground = null,
        )
    }
}

@Preview
@Composable
fun ListItemType02Preview4(
    @PreviewParameter(
        ListItemType02ParamsProvider::class
    ) params: ListItemType02Params
) {
    CommonTheme {
        ListItemType02(
            index = 0,
            itemTitleString = params.itemTitleString,
            itemDescription = params.lblNextToRatingString,
            onListItemClick = params.onListItemClick,
            onItemsButtonClick = params.onListItemClick,
            getItemsRating = { null },
            itemRef = null,
            itemDrawableId = null,
            imageTint = null,
            backgroundDrawableId = R.drawable.example_background_1,
            composableBackground = null,
            buttonImageVector = null,
        )
    }
}

@Preview
@Composable
fun ListItemType02Preview5(
    @PreviewParameter(
        ListItemType02ParamsProvider::class
    ) params: ListItemType02Params
) {
    CommonTheme {
        val defaultColors = listOf(
            MaterialTheme.colors.primary,
            MaterialTheme.colors.surface,
        )
        ListItemType02(
            index = 0,
            itemTitleString = params.itemTitleString,
            itemDescription = params.lblNextToRatingString,
            onListItemClick = {},
            onItemsButtonClick = {},
            getItemsRating = { null },
            itemRef = null,
            backgroundDrawableId = R.drawable.example_background_1,
            composableBackground = BackgroundsWithGradientsImpl.backgroundVerticalGradient(
                colors = defaultColors,
                radius = null,
                shape = null,
                alpha = null,
            ),
            itemDrawableId = R.drawable.example_user_profile_picture_0,
            imageTint = null,
            buttonTint = MaterialTheme.colors.secondary,
            itemDescriptionFontWeight = FontWeight.Normal,
            buttonImageVector = null,
        )
    }
}

@Preview
@Composable
fun ListItemType02Preview6(
    @PreviewParameter(
        ListItemType02ParamsProvider::class
    ) params: ListItemType02Params
) {
    CommonTheme {
        val currentlyShownTrainingCourses = PreviewEntityFactory.createPreviewEntitiesList()
        val course = currentlyShownTrainingCourses[0]
        val isPreview = true
        val index = 0
        ListItemType02(
            index = index,
            itemTitleString = course.name,
            itemDescription = course.description,
            onListItemClick = {},
            onItemsButtonClick = {},
            getItemsRating = { null },
            itemRef = if(isPreview) {
                null
            } else {
                null
            },
            backgroundDrawableId = R.drawable.example_background_1,
            composableBackground = BackgroundsOfLayoutsImpl.backgroundListItemType01(
                colors = null,
                brush = null,
                shape = null,
                alpha = 0.6f,
            ),
            itemDrawableId = if(isPreview) {
                getDrawableIdInPreview("elms_example_training_course_", index)
            } else { null },
            imageTint = null,
            buttonTint = MaterialTheme.colors.secondary,
            itemDescriptionFontWeight = FontWeight.Normal,
            buttonImageVector = null,
        )
    }
}