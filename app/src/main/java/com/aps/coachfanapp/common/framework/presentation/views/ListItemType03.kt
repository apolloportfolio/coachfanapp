package com.aps.coachfanapp.common.framework.presentation.views

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.material.Card
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.geometry.Size
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.graphics.DefaultAlpha
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.layout.onGloballyPositioned
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.res.*
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.tooling.preview.PreviewParameter
import androidx.compose.ui.tooling.preview.PreviewParameterProvider
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.toSize
import androidx.constraintlayout.compose.ConstrainScope
import androidx.constraintlayout.compose.ConstraintLayout
import androidx.constraintlayout.compose.Dimension
import com.aps.coachfanapp.common.framework.presentation.values.CommonTheme
import com.aps.coachfanapp.common.util.ALog
import com.aps.coachfanapp.common.util.composableutils.fallbackPainterResource
import com.google.firebase.storage.StorageReference
import com.gowtham.ratingbar.RatingBar
import com.gowtham.ratingbar.RatingBarStyle
import kotlin.math.roundToInt
import com.aps.coachfanapp.R

private const val TAG = "ListItemType03"
private const val LOG_ME = true

@Composable
fun ListItemType03(
    index: Int,
    itemTitleString: String?,
    itemPriceString: String?,
    itemLikesString: String?,
    itemDistanceString: String?,
    lblSmallShortAttribute3String: String?,
    itemDescriptionString: String?,
    onListItemClick: () -> Unit,
    getItemsRating: () -> Float?,
    itemRef: StorageReference?,
    itemDrawableId: Int? = null,
    imageContentDescription: String = "",
    composableBackground: DrawnBackground? = null,
    backgroundDrawableId: Int? = R.drawable.list_item_example_background_1,
    imageTint: Color? = MaterialTheme.colors.primary,
) {
    if(LOG_ME)ALog.d(TAG, "ListItemType03(): " +
            "index == $index, itemTitleString == $itemTitleString\n")
    val transparentColor = remember {
        Color(0x00000000)
    }
    val startMargin = remember { 4 }.dp
    val endMargin = remember { 4 }.dp
    val topMargin = dimensionResource(id = R.dimen.list_item_vertical_margin)
    val bottomMargin = dimensionResource(id = R.dimen.list_item_vertical_margin)
    Card(
        modifier = Modifier
            .testTag(index.toString())
            .fillMaxWidth()
            .padding(
                start = dimensionResource(id = R.dimen.list_tile_horizontal_margin),
                end = dimensionResource(id = R.dimen.list_tile_horizontal_margin),
                top = dimensionResource(id = R.dimen.list_tile_vertical_margin),
                bottom = dimensionResource(id = R.dimen.list_tile_vertical_margin)
            )
            .background(transparentColor)
            .clickable { onListItemClick() },
    ) {
        BoxWithBackground(
            backgroundDrawableId = backgroundDrawableId,
            composableBackground = composableBackground,
        ) {

            ConstraintLayout(
                modifier = Modifier
                    .fillMaxWidth()
                    .wrapContentHeight()
                    .padding(
                        start = startMargin,
                        end = endMargin,
                        top = topMargin,
                        bottom = bottomMargin,
                    )
            ) {

                val itemsRating = remember { getItemsRating() }

                val (itemThumbnail, itemTitle, itemPrice, itemLikes,
                    itemDistance, lblTopRightCorner, itemDescription,
                    itemRatingBar,
                ) = createRefs()

                val itemThumbnailConstrain: ConstrainScope.() -> Unit = {
                    top.linkTo(parent.top)
                    start.linkTo(parent.start)
                    end.linkTo(parent.end)
                }

                val itemTitleConstrain: ConstrainScope.() -> Unit = {
                    if(itemDrawableId != null) {
                        top.linkTo(itemThumbnail.bottom)
                        start.linkTo(parent.start)
                    } else {
                        if(itemDistanceString != null) {
                            top.linkTo(itemDistance.bottom)
                            start.linkTo(parent.start)
                        } else if(lblSmallShortAttribute3String != null) {
                            top.linkTo(lblTopRightCorner.bottom)
                            start.linkTo(parent.start)
                        } else {
                            top.linkTo(itemThumbnail.bottom)
                            start.linkTo(parent.start)
                        }
                    }
                }

                val lblTopRightCornerConstrain: ConstrainScope.() -> Unit = {
                    top.linkTo(parent.top)
                    end.linkTo(parent.end)
                }

                val itemPriceConstrain: ConstrainScope.() -> Unit = {
                    top.linkTo(itemTitle.top)
                    end.linkTo(parent.end)
                }

                val itemDistanceConstrain: ConstrainScope.() -> Unit = {
                    top.linkTo(parent.top)
                    start.linkTo(parent.start)
                }

                val itemDescriptionConstrain: ConstrainScope.() -> Unit = {
                    top.linkTo(itemTitle.bottom)
                    start.linkTo(itemRatingBar.end)
                    end.linkTo(parent.end)
                    bottom.linkTo(parent.bottom)
                    width = Dimension.fillToConstraints
                }

                val itemDescriptionTextAlignment: TextAlign = remember {
                    if(itemsRating != null){
                        TextAlign.End
                    } else {
                        TextAlign.Start
                    }
                }

                if(itemRef != null) {
                    ImageFromFirebase(
                        itemRef,
                        modifier = Modifier
                            .constrainAs(itemThumbnail, itemThumbnailConstrain)
                            .height(200.dp)
                            .background(transparentColor)
                            .padding(
                                start = startMargin,
                                end = endMargin,
                                top = topMargin,
                                bottom = bottomMargin
                            )
                            .fillMaxWidth(),
                        contentDescription = imageContentDescription,
                        contentScale = ContentScale.Fit
                    )
                } else if(itemDrawableId != null) {
                    Image(
                        painter = fallbackPainterResource(id = itemDrawableId),
                        modifier = Modifier
                            .constrainAs(itemThumbnail, itemThumbnailConstrain)
                            .height(200.dp)
                            .background(transparentColor)
                            .padding(
                                start = startMargin,
                                end = endMargin,
                                top = topMargin,
                                bottom = bottomMargin
                            )
                            .fillMaxWidth(),
//                        alignment = Alignment.Center,
                        contentScale = ContentScale.Fit,
                        contentDescription = imageContentDescription,
                        alpha = DefaultAlpha,
                        colorFilter = if(imageTint != null) { ColorFilter.tint(imageTint) } else {null},
                    )
                }

                Text(
                    modifier = Modifier
                        .constrainAs(itemTitle, itemTitleConstrain)
                        .fillMaxWidth(0.7f)
                        .padding(
                            start = startMargin,
                            top = topMargin,
                            end = dimensionResource(id = R.dimen.margin_between_text_views),
                        ),
                    text = itemTitleString ?: "",
                    maxLines = 1,
                    textAlign = TextAlign.Start,
                    style = TextStyle(
                        color = MaterialTheme.colors.onSurface,
                        fontWeight = FontWeight.Bold
                    ),
                    overflow = TextOverflow.Ellipsis,
                )

                Text(
                    modifier = Modifier
                        .constrainAs(itemPrice, itemPriceConstrain)
                        .fillMaxWidth(0.25f)
                        .padding(
                            end = endMargin,
                            top = topMargin
                        ),
                    text = itemPriceString ?: "",
                    textAlign = TextAlign.End,
                    maxLines = 1,
                    style = TextStyle(
                        color = MaterialTheme.colors.onSurface,
                        fontWeight = FontWeight.Bold
                    ),
                    overflow = TextOverflow.Ellipsis,
                )

                if(itemsRating != null) {
                    Text(
                        modifier = Modifier
                            .fillMaxWidth(0.1f)
                            .constrainAs(itemLikes) {
                                start.linkTo(itemRatingBar.end)
                                end.linkTo(itemDescription.start)
                                centerVerticallyTo(itemRatingBar)
                            }
                            .padding(
                                end = endMargin,
                                top = topMargin,
                                bottom = bottomMargin
                            )
                            .background(transparentColor),
                        text = itemLikesString ?: "",
                        textAlign = TextAlign.Center,
                        style = TextStyle(
                            color = MaterialTheme.colors.onSurface,
                            fontWeight = FontWeight.Bold
                        ),
                        maxLines = 1,
                    )
                }

                Text(
                    modifier = Modifier
//                        .fillMaxWidth(0.2f)
                        .wrapContentWidth()
                        .constrainAs(itemDistance, itemDistanceConstrain)
                        .padding(
                            start = startMargin,
                            end = dimensionResource(id = R.dimen.margin_between_text_views),
                            top = topMargin,
                            bottom = bottomMargin
                        )
                        .background(transparentColor),
                    text = itemDistanceString ?: "",
                    textAlign = TextAlign.Center,
                    style = TextStyle(
                        color = MaterialTheme.colors.onSurface,
                        fontWeight = FontWeight.Bold
                    ),
                    maxLines = 1,
                )

                Text(
                    modifier = Modifier
                        .constrainAs(lblTopRightCorner, lblTopRightCornerConstrain)
                        .wrapContentWidth()
//                        .fillMaxWidth(0.2f)
                        .padding(
                            start = startMargin,
                            top = topMargin,
                            end = endMargin,
                            bottom = bottomMargin
                        ),
                    text = lblSmallShortAttribute3String ?: "",
                    textAlign = TextAlign.End,
                    style = TextStyle(
                        color = MaterialTheme.colors.onSurface,
                        fontWeight = FontWeight.Bold
                    ),
                    maxLines = 1,
                    overflow = TextOverflow.Ellipsis
                )

                Text(
                    modifier = Modifier
                        .constrainAs(itemDescription, itemDescriptionConstrain)
                        .padding(
                            start = startMargin,
                            end = endMargin,
                            top = topMargin,
                            bottom = bottomMargin
                        ),
                    text = itemDescriptionString ?: stringResource(id = R.string.example_text_medium),
                    textAlign = itemDescriptionTextAlignment,
                    maxLines = 3,
                    style = TextStyle(
                        color = MaterialTheme.colors.onSurface,
                        fontWeight = FontWeight.Normal
                    ),
                    overflow = TextOverflow.Ellipsis
                )


                if(itemsRating != null) {
                    val maximumNumberOfIndicators = remember { 5 }
                    val fractionOfParentsWidth = remember { 0.25f }
                    val spaceBetweenIndicators = remember { 1.dp }
                    var indicatorSize = remember { 14 }
                    RatingBar(
                        size = indicatorSize.dp,
                        spaceBetween = spaceBetweenIndicators,
                        numOfStars = maximumNumberOfIndicators,
                        value = itemsRating,
                        style = RatingBarStyle.Fill(),
                        onValueChange = {},
                        onRatingChanged = {
                            ALog.d("TAG", "onRatingChanged: $it")
                        },
                        isIndicator = true,
                        modifier = Modifier
                            .padding(
                                top = topMargin,
                                bottom = bottomMargin,
                            )
                            .constrainAs(itemRatingBar) {
                                top.linkTo(itemTitle.bottom)
                                bottom.linkTo(lblTopRightCorner.top)
                                start.linkTo(parent.start)
                            }
                            .fillMaxWidth(fractionOfParentsWidth)
                            .wrapContentWidth()
                            .onGloballyPositioned {
                                //here u can access the parent layout coordinate size
                                val parentSize =
                                    it.parentLayoutCoordinates?.size?.toSize() ?: Size.Zero
                                val spaceLeftForIndicators =
                                    parentSize.width - maximumNumberOfIndicators * spaceBetweenIndicators.value
                                indicatorSize =
                                    (spaceLeftForIndicators * fractionOfParentsWidth / maximumNumberOfIndicators).roundToInt()
                                ALog.d(
                                    "ListItemType03", ": " +
                                            "indicatorSize == $indicatorSize"
                                )
                            },
                    )
                }
            }
        }
    }
}


// Preview =========================================================================================
data class ListItemType03Params(
    val itemTitleString: String?,
    val lblBigAttributeString: String?,
    val lblSmallShortAttribute1String: String?,
    val lblSmallShortAttribute2String: String?,
    val lblSmallShortAttribute3String: String?,
    val lblSmallLongAttributeString: String?,
    val onListItemClick: () -> Unit,
    val getItemsRating: () -> Float?,
    val ratingBarColor: Color,
)

class ListItemType03ParamsProvider : PreviewParameterProvider<ListItemType03Params> {
    override val values: Sequence<ListItemType03Params> = sequenceOf(
        ListItemType03Params(
            itemTitleString = "Lorem ipsum dolor sit amet",
            lblBigAttributeString = "120zł",
            lblSmallShortAttribute1String = "999",
            lblSmallShortAttribute2String = "14.4 km",
            lblSmallShortAttribute3String = "999",
            lblSmallLongAttributeString = "Lorem ipsum dolor sit amet, consectetur adipiscing elit." +
                    "Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
            onListItemClick = {},
//            getItemsRating = { 3.5f },
            getItemsRating = { 5f },
            ratingBarColor = Color(0xFFFFCC01)
        ),
    )
}

@Preview
@Composable
fun ListItemType03Preview1(
    @PreviewParameter(
        ListItemType03ParamsProvider::class
    ) params: ListItemType03Params
) {
    CommonTheme {
        ListItemType03(
            index = 0,
            itemTitleString = params.itemTitleString,
            itemPriceString = params.lblBigAttributeString,
            itemLikesString = params.lblSmallShortAttribute1String,
            itemDistanceString = params.lblSmallShortAttribute2String,
            lblSmallShortAttribute3String = params.lblSmallShortAttribute3String,
            itemDescriptionString = params.lblSmallLongAttributeString,
            onListItemClick = params.onListItemClick,
            getItemsRating = params.getItemsRating,
            itemRef = null,
            itemDrawableId = R.drawable.example_user_profile_picture_0,
            imageTint = null,
        )
    }
}

@Preview
@Composable
fun ListItemType03Preview2(
    @PreviewParameter(
        ListItemType03ParamsProvider::class
    ) params: ListItemType03Params
) {
    CommonTheme {
        ListItemType03(
            index = 0,
            itemTitleString = params.itemTitleString,
            itemPriceString = params.lblBigAttributeString,
            itemLikesString = params.lblSmallShortAttribute1String,
            itemDistanceString = params.lblSmallShortAttribute2String,
            lblSmallShortAttribute3String = params.lblSmallShortAttribute3String,
            itemDescriptionString = params.lblSmallLongAttributeString,
            onListItemClick = params.onListItemClick,
            getItemsRating = params.getItemsRating,
            itemRef = null,
            itemDrawableId = R.drawable.example_user_profile_picture_0,
            imageTint = null,
            composableBackground = BackgroundsWithGradientsImpl.backgroundRadiantGradientFromCenterToOutside(
                null, null, null, 0.5f,
            )
        )
    }
}

@Preview
@Composable
fun ListItemType03Preview3(
    @PreviewParameter(
        ListItemType03ParamsProvider::class
    ) params: ListItemType03Params
) {
    CommonTheme {
        ListItemType03(
            index = 0,
            itemTitleString = params.itemTitleString,
            itemPriceString = "",
            itemLikesString = "",
            itemDistanceString = "300km",
            lblSmallShortAttribute3String = "",
            itemDescriptionString = params.lblSmallLongAttributeString,
            onListItemClick = {},
            getItemsRating = { null },
            itemRef = null,
            itemDrawableId = R.drawable.example_user_profile_picture_0,
            imageTint = null,
            backgroundDrawableId = null,
            composableBackground = BackgroundsWithGradientsImpl.backgroundRadiantGradientFromCenterToOutside(
                null, null, null, 0.5f,
            )
        )
    }
}

@Preview
@Composable
fun ListItemType03Preview4(
    @PreviewParameter(
        ListItemType03ParamsProvider::class
    ) params: ListItemType03Params
) {
    CommonTheme {
        ListItemType03(
            index = 0,
            itemTitleString = params.itemTitleString,
            itemPriceString = "",
            itemLikesString = "",
            itemDistanceString = "300km",
            lblSmallShortAttribute3String = "",
            itemDescriptionString = params.lblSmallLongAttributeString,
            onListItemClick = {},
            getItemsRating = { null },
            itemRef = null,
            itemDrawableId = null,
            imageTint = null,
            backgroundDrawableId = null,
            composableBackground = BackgroundsWithGradientsImpl.backgroundRadiantGradientFromCenterToOutside(
                null, null, null, 0.5f,
            )
        )
    }
}