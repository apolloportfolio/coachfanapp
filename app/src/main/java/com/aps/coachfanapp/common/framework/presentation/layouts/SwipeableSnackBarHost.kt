package com.aps.coachfanapp.common.framework.presentation.layouts

import androidx.annotation.DrawableRes
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.gestures.Orientation
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.offset
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Card
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.FractionalThreshold
import androidx.compose.material.Icon
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Snackbar
import androidx.compose.material.SnackbarHost
import androidx.compose.material.SnackbarHostState
import androidx.compose.material.Text
import androidx.compose.material.rememberSwipeableState
import androidx.compose.material.swipeable
import androidx.compose.runtime.Composable
import androidx.compose.runtime.DisposableEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.geometry.Size
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.onSizeChanged
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.aps.coachfanapp.common.util.ALog

private const val TAG = "SwipeableSnackBarHost"
private const val LOG_ME = true

enum class SwipeDirection {
    Left,
    Initial,
    Right,
}

@OptIn(ExperimentalMaterialApi::class)
@Composable
fun SwipeableSnackBarHost(
    hostState: SnackbarHostState,
    snackBar: @Composable () -> Unit,
) {
    if (hostState.currentSnackbarData == null) { return }
    var size by remember { mutableStateOf(Size.Zero) }
    val swipeableState = rememberSwipeableState(SwipeDirection.Initial)
    val width = remember(size) {
        if (size.width == 0f) {
            1f
        } else {
            size.width
        }
    }
    if (swipeableState.isAnimationRunning) {
        DisposableEffect(Unit) {
            onDispose {
                when (swipeableState.currentValue) {
                    SwipeDirection.Right,
                    SwipeDirection.Left -> {
                        hostState.currentSnackbarData?.dismiss()
                    }
                    else -> {
                        return@onDispose
                    }
                }
            }
        }
    }
    val offset = with(LocalDensity.current) {
        swipeableState.offset.value.toDp()
    }
    SnackbarHost(
        hostState,
        snackbar = {
            Box(
                modifier = Modifier.offset(x = offset)
            ) {
                snackBar()
            }
//            CustomSnackBarSwipeableWithGradient(
//                iconDrawableResID = snackBarState?.iconDrawableRes,
//                message = snackBarState?.message ?: "Error",
//                actionMessage = snackBarState?.actionLabel,
//                onActionClick = {
//                    if(LOG_ME) ALog.d(TAG, "(): SnackBar clicked.")
//                    snackBarState?.onClickAction
//                },
//                colors = listOf(
//                    MaterialTheme.colors.primary,
//                    MaterialTheme.colors.primaryVariant
//                ),
//                textColor = MaterialTheme.colors.secondaryVariant,
//                modifier = Modifier.offset(x = offset)
//            )
        },
        modifier = Modifier
            .onSizeChanged { size = Size(it.width.toFloat(), it.height.toFloat()) }
            .swipeable(
                state = swipeableState,
                anchors = mapOf(
                    -width to SwipeDirection.Left,
                    0f to SwipeDirection.Initial,
                    width to SwipeDirection.Right,
                ),
                thresholds = { _, _ -> FractionalThreshold(0.3f) },
                orientation = Orientation.Horizontal
            )
    )
}

@Composable
fun CustomSnackBarSwipeableWithGradient(
    @DrawableRes iconDrawableResID: Int?,
    message: String,
    actionMessage: String? = null,
    onActionClick: () -> Unit,
    colors: List<androidx.compose.ui.graphics.Color> = listOf(
        MaterialTheme.colors.primary,
        MaterialTheme.colors.primaryVariant
    ),
    textColor: Color = MaterialTheme.colors.secondaryVariant,
) {
    if(LOG_ME)ALog.d(TAG, "CustomSnackBarSwipeableWithGradient(): Recomposition start")
    Snackbar(
        backgroundColor = Color.Transparent,
        elevation = 0.dp,
    ) {
        Box(
            modifier = Modifier
                .fillMaxWidth()
                .wrapContentHeight()
                .padding(
                    start = 6.dp,
                    end = 6.dp,
                    top = 12.dp,
                    bottom = 6.dp,
                ),
        ) {
            Column(
                modifier = Modifier
                    .padding(
                        top = 16.dp
                    )
                    .fillMaxWidth()
                    .clip(RoundedCornerShape(10.dp))
                    .background(
                        Brush.verticalGradient(
                            colors = colors
                        )
                    )
                    .padding(
                        start = 78.dp,
                        top = 8.dp,
                        bottom = 12.dp,
                        end = 8.dp,
                    ),
                horizontalAlignment = Alignment.Start
            ) {
                Row(
                    modifier = Modifier.fillMaxWidth(),
                    horizontalArrangement = Arrangement.SpaceBetween
                ) {
                    Text(
                        text = message,
                        color = textColor,
                        fontStyle = FontStyle.Normal,
                        fontSize = 15.sp,
                        modifier = Modifier
                            .padding(
                                start = 6.dp,
                                end = 6.dp,
                                top = 6.dp,
                                bottom = 6.dp,
                            )
                    )

                    Spacer(modifier = Modifier.padding(horizontal = 6.dp))

                    if(actionMessage != null) {
                        Text(
                            text = actionMessage,
                            color = textColor,
                            fontStyle = FontStyle.Italic,
                            fontSize = 15.sp,
                            fontWeight = FontWeight.Bold,
                            modifier = Modifier
                                .padding(
                                    start = 6.dp,
                                    end = 6.dp,
                                    top = 6.dp,
                                    bottom = 6.dp,
                                )
                                .clickable(
                                    interactionSource = MutableInteractionSource(),
                                    indication = null,
                                    onClick = onActionClick,
                                )
                        )
                    }
                }

            }

            Column(
                modifier = Modifier
                    .padding(start = 16.dp,),
                horizontalAlignment = Alignment.CenterHorizontally,
            ) {
                Card(
                    elevation = 6.dp,
                    shape = RoundedCornerShape(8.dp),
                ) {
                    if(iconDrawableResID != null) {
                        Icon(
                            painter = painterResource(id = iconDrawableResID),
//                            contentScale = ContentScale.Crop,
                            contentDescription = null,
                            modifier = Modifier
                                .size(50.dp),
                            tint = textColor,
                        )
                    }
                }
            }
        }
    }
    if(LOG_ME)ALog.d(TAG, "CustomSnackBarSwipeableWithGradient(): Recomposition end")
}