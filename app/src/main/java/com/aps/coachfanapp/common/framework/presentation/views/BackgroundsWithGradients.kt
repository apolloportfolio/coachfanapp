package com.aps.coachfanapp.common.framework.presentation.views

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.BoxWithConstraints
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.wrapContentSize
import androidx.compose.material.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.geometry.Size
import androidx.compose.ui.geometry.center
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.RadialGradientShader
import androidx.compose.ui.graphics.RectangleShape
import androidx.compose.ui.graphics.Shader
import androidx.compose.ui.graphics.ShaderBrush
import androidx.compose.ui.graphics.Shape
import androidx.compose.ui.graphics.TileMode
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.aps.coachfanapp.common.framework.presentation.values.CommonTheme

// References:
// https://developer.android.com/jetpack/compose/graphics/draw/brush
// https://stackoverflow.com/questions/67687797/how-to-create-oval-gradient-in-jetpack-compose

interface BackgroundsWithGradients {
    @Composable
    fun backgroundRadiantGradientTopLeftToBottomRight(
        colors: List<Color>?,
        radius: Float?,
        shape: Shape?,
        /*@FloatRange(from = 0.0, to = 1.0)*/
        alpha: Float?,
    ): DrawnBackground
    @Composable
    fun backgroundRadiantGradientTopRightToBottomLeft(
        colors: List<Color>?,
        radius: Float?,
        shape: Shape?,
        /*@FloatRange(from = 0.0, to = 1.0)*/
        alpha: Float?,
    ): DrawnBackground

    @Composable
    fun backgroundRadiantGradientBottomLeftToTopRight(
        colors: List<Color>?,
        radius: Float?,
        shape: Shape?,
        /*@FloatRange(from = 0.0, to = 1.0)*/
        alpha: Float?,
    ): DrawnBackground

    @Composable
    fun backgroundRadiantGradientBottomRightToTopLeft(
        colors: List<Color>?,
        radius: Float?,
        shape: Shape?,
        /*@FloatRange(from = 0.0, to = 1.0)*/
        alpha: Float?,
    ): DrawnBackground

    @Composable
    fun backgroundRadiantGradientFromCenterToOutside(
        colors: List<Color>?,
        radius: Float?,
        shape: Shape?,
        /*@FloatRange(from = 0.0, to = 1.0)*/
        alpha: Float?,
    ): DrawnBackground

    @Composable
    fun backgroundHorizontalGradient(
        colors: List<Color>?,
        radius: Float?,
        shape: Shape?,
        /*@FloatRange(from = 0.0, to = 1.0)*/
        alpha: Float?,
    ): DrawnBackground

    @Composable
    fun backgroundVerticalGradient(
        colors: List<Color>?,
        radius: Float?,
        shape: Shape?,
        /*@FloatRange(from = 0.0, to = 1.0)*/
        alpha: Float?,
    ): DrawnBackground

}
internal object BackgroundsWithGradientsImpl: BackgroundsWithGradients {
    @Composable
    override fun backgroundRadiantGradientTopLeftToBottomRight(
        colors: List<Color>?,
        radius: Float?,
        shape: Shape?,
        /*@FloatRange(from = 0.0, to = 1.0)*/
        alpha: Float?,
    ): DrawnBackground {
        return { modifier: Modifier, contentAlignment: Alignment, content: @Composable () -> Unit ->
            val defaultColors = listOf(
                MaterialTheme.colors.primary,
                MaterialTheme.colors.primaryVariant,
            )
            val defaultShape = RectangleShape
            val defaultAlpha = 1.0f

            BoxWithConstraints(
                modifier = modifier
                    .wrapContentSize(),
                contentAlignment = contentAlignment,
            ) {
                val brush = Brush.radialGradient(
                    colors = colors ?: defaultColors,
                    center = Offset.Zero,
                    radius = radius ?: 1000f,
                )
                Box(
                    modifier = modifier
                        .wrapContentSize()
                        .background(
                            brush = brush,
                            shape = shape ?: defaultShape,
                            alpha = alpha ?: defaultAlpha,
                        )
                ) {
                    content()
                }
            }
        }
    }
    @Composable
    override fun backgroundRadiantGradientTopRightToBottomLeft(
        colors: List<Color>?,
        radius: Float?,
        shape: Shape?,
        /*@FloatRange(from = 0.0, to = 1.0)*/
        alpha: Float?,
    ): DrawnBackground {
        return { modifier: Modifier, contentAlignment: Alignment, content: @Composable () -> Unit ->
            val defaultColors = listOf(
                MaterialTheme.colors.primary,
                MaterialTheme.colors.primaryVariant,
            )
            val defaultShape = RectangleShape
            val defaultAlpha = 1.0f

            BoxWithConstraints(
                modifier = modifier
                    .wrapContentSize(),
                contentAlignment = contentAlignment,
            ) {
                val brush = remember {
                    object : ShaderBrush() {
                        override fun createShader(size: Size): Shader {
                            return RadialGradientShader(
                                center = Offset(size.width, 0.0f),
                                radius = radius ?: 1000f,
                                colors = colors ?: defaultColors,
                                colorStops = null,
                                tileMode = TileMode.Clamp
                            )
                        }
                    }
                }
                Box(
                    modifier = modifier
                        .wrapContentSize()
                        .background(
                            brush = brush,
                            shape = shape ?: defaultShape,
                            alpha = alpha ?: defaultAlpha,
                        )
                ) {
                    content()
                }
            }
        }
    }

    @Composable
    override fun backgroundRadiantGradientBottomLeftToTopRight(
        colors: List<Color>?,
        radius: Float?,
        shape: Shape?,
        /*@FloatRange(from = 0.0, to = 1.0)*/
        alpha: Float?,
    ): DrawnBackground {
        return { modifier: Modifier, contentAlignment: Alignment, content: @Composable () -> Unit ->
            val defaultColors = listOf(
                MaterialTheme.colors.primary,
                MaterialTheme.colors.primaryVariant,
            )
            val defaultShape = RectangleShape
            val defaultAlpha = 1.0f

            BoxWithConstraints(
                modifier = modifier
                    .wrapContentSize(),
                contentAlignment = contentAlignment,
            ) {
                val brush = remember {
                    object : ShaderBrush() {
                        override fun createShader(size: Size): Shader {
                            return RadialGradientShader(
                                center = Offset(0.0f, size.height),
                                radius = radius ?: 1000f,
                                colors = colors ?: defaultColors,
                                colorStops = null,
                                tileMode = TileMode.Clamp
                            )
                        }
                    }
                }
                Box(
                    modifier = modifier
                        .wrapContentSize()
                        .background(
                            brush = brush,
                            shape = shape ?: defaultShape,
                            alpha = alpha ?: defaultAlpha,
                        )
                ) {
                    content()
                }
            }
        }
    }

    @Composable
    override fun backgroundRadiantGradientBottomRightToTopLeft(
        colors: List<Color>?,
        radius: Float?,
        shape: Shape?,
        /*@FloatRange(from = 0.0, to = 1.0)*/
        alpha: Float?,
    ): DrawnBackground {
        return { modifier: Modifier, contentAlignment: Alignment, content: @Composable () -> Unit ->
            val defaultColors = listOf(
                MaterialTheme.colors.primary,
                MaterialTheme.colors.primaryVariant,
            )
            val defaultShape = RectangleShape
            val defaultAlpha = 1.0f

            BoxWithConstraints(
                modifier = modifier
                    .wrapContentSize(),
                contentAlignment = contentAlignment,
            ) {
                val brush = remember {
                    object : ShaderBrush() {
                        override fun createShader(size: Size): Shader {
                            return RadialGradientShader(
                                center = Offset(size.width, size.height),
                                radius = radius ?: 1000f,
                                colors = colors ?: defaultColors,
                                colorStops = null,
                                tileMode = TileMode.Clamp
                            )
                        }
                    }
                }
                Box(
                    modifier = modifier
                        .wrapContentSize()
                        .background(
                            brush = brush,
                            shape = shape ?: defaultShape,
                            alpha = alpha ?: defaultAlpha,
                        )
                ) {
                    content()
                }
            }
        }
    }

    @Composable
    override fun backgroundRadiantGradientFromCenterToOutside(
        colors: List<Color>?,
        radius: Float?,
        shape: Shape?,
        /*@FloatRange(from = 0.0, to = 1.0)*/
        alpha: Float?,
    ): DrawnBackground {
        return { modifier: Modifier, contentAlignment: Alignment, content: @Composable () -> Unit ->
            val defaultColors = listOf(
                MaterialTheme.colors.primary,
                MaterialTheme.colors.primaryVariant,
            )
            val defaultShape = RectangleShape
            val defaultAlpha = 1.0f

            BoxWithConstraints(
                modifier = modifier
                    .wrapContentSize(),
                contentAlignment = contentAlignment,
            ) {
                val brush = remember {
                    object : ShaderBrush() {
                        override fun createShader(size: Size): Shader {
                            val biggerDimension = maxOf(size.height, size.width)
                            return RadialGradientShader(
                                colors = colors ?: defaultColors,
                                center = size.center,
                                radius = biggerDimension / 2f,
                                colorStops = null
                            )
                        }
                    }
                }
                Box(
                    modifier = modifier
                        .wrapContentSize()
                        .background(
                            brush = brush,
                            shape = shape ?: defaultShape,
                            alpha = alpha ?: defaultAlpha,
                        ),
                ) {
                    content()
                }
            }
        }
    }

    @Composable
    override fun backgroundHorizontalGradient(
        colors: List<Color>?,
        radius: Float?,
        shape: Shape?,
        /*@FloatRange(from = 0.0, to = 1.0)*/
        alpha: Float?,
    ): DrawnBackground {
        return { modifier: Modifier, contentAlignment: Alignment, content: @Composable () -> Unit ->
            val defaultColors = listOf(
                MaterialTheme.colors.primary,
                MaterialTheme.colors.primaryVariant,
            )
            val defaultShape = RectangleShape
            val defaultAlpha = 1.0f

            BoxWithConstraints(
                modifier = modifier
                    .wrapContentSize(),
                contentAlignment = contentAlignment,
            ) {
                val brush = Brush.horizontalGradient(
                    colors = colors ?: defaultColors,
                )
                Box(
                    modifier = modifier
                        .wrapContentSize()
                        .background(
                            brush = brush,
                            shape = shape ?: defaultShape,
                            alpha = alpha ?: defaultAlpha,
                        )
                ) {
                    content()
                }
            }
        }
    }

    @Composable
    override fun backgroundVerticalGradient(
        colors: List<Color>?,
        radius: Float?,
        shape: Shape?,
        /*@FloatRange(from = 0.0, to = 1.0)*/
        alpha: Float?,
    ): DrawnBackground {
        return { modifier: Modifier, contentAlignment: Alignment, content: @Composable () -> Unit ->
            val defaultColors = listOf(
                MaterialTheme.colors.primary,
                MaterialTheme.colors.primaryVariant,
            )
            val defaultShape = RectangleShape
            val defaultAlpha = 1.0f

            BoxWithConstraints(
                modifier = modifier
                    .wrapContentSize(),
                contentAlignment = contentAlignment,
            ) {
                val brush = Brush.verticalGradient(
                    colors = colors ?: defaultColors,
                )
                Box(
                    modifier = modifier
                        .wrapContentSize()
                        .background(
                            brush = brush,
                            shape = shape ?: defaultShape,
                            alpha = alpha ?: defaultAlpha,
                        )
                ) {
                    content()
                }
            }
        }
    }

}
@Preview
@Composable
private fun BackgroundRadiantGradientTopLeftToBottomRightPreviewScreen() {
    CommonTheme {
        val backgrounds: BackgroundsWithGradients = BackgroundsWithGradientsImpl
        BoxWithBackground(
            modifier = Modifier
                .fillMaxWidth()
                .fillMaxHeight(),
            composableBackground = backgrounds.backgroundRadiantGradientTopLeftToBottomRight(null, null, null, null)
        ) {}
    }
}
@Preview
@Composable
private fun BackgroundRadiantGradientTopLeftToBottomRightPreviewRow() {
    CommonTheme {
        val backgrounds: BackgroundsWithGradients = BackgroundsWithGradientsImpl
        BoxWithBackground(
            modifier = Modifier
                .fillMaxWidth()
                .height(56.dp),
            composableBackground = backgrounds.backgroundRadiantGradientTopLeftToBottomRight(null, null, null, null)
        ) {}
    }
}
@Preview
@Composable
private fun BackgroundRadiantGradientTopRightToBottomLeftPreviewScreen() {
    CommonTheme {
        val backgrounds: BackgroundsWithGradients = BackgroundsWithGradientsImpl
        BoxWithBackground(
            modifier = Modifier
                .fillMaxWidth()
                .fillMaxHeight(),
            composableBackground = backgrounds.backgroundRadiantGradientTopRightToBottomLeft(null, null, null, null)
        ) {}
    }
}
@Preview
@Composable
private fun BackgroundRadiantGradientTopRightToBottomLeftPreviewRow() {
    CommonTheme {
        val backgrounds: BackgroundsWithGradients = BackgroundsWithGradientsImpl
        BoxWithBackground(
            modifier = Modifier
                .fillMaxWidth()
                .height(56.dp),
            composableBackground = backgrounds.backgroundRadiantGradientTopRightToBottomLeft(null, null, null, null)
        ) {}
    }
}
@Preview
@Composable
private fun BackgroundRadiantGradientBottomLeftToTopRightPreviewScreen() {
    CommonTheme {
        val backgrounds: BackgroundsWithGradients = BackgroundsWithGradientsImpl
        BoxWithBackground(
            modifier = Modifier
                .fillMaxWidth()
                .fillMaxHeight(),
            composableBackground = backgrounds.backgroundRadiantGradientBottomLeftToTopRight(null, null, null, null)
        ) {}
    }
}
@Preview
@Composable
private fun BackgroundRadiantGradientBottomLeftToTopRightPreviewRow() {
    CommonTheme {
        val backgrounds: BackgroundsWithGradients = BackgroundsWithGradientsImpl
        BoxWithBackground(
            modifier = Modifier
                .fillMaxWidth()
                .height(56.dp),
            composableBackground = backgrounds.backgroundRadiantGradientBottomLeftToTopRight(null, null, null, null)
        ) {}
    }
}
@Preview
@Composable
private fun BackgroundRadiantGradientBottomRightToTopLeftPreviewScreen() {
    CommonTheme {
        val backgrounds: BackgroundsWithGradients = BackgroundsWithGradientsImpl
        BoxWithBackground(
            modifier = Modifier
                .fillMaxWidth()
                .fillMaxHeight(),
            composableBackground = backgrounds.backgroundRadiantGradientBottomRightToTopLeft(null, null, null, null)
        ) {}
    }
}
@Preview
@Composable
private fun BackgroundRadiantGradientBottomRightToTopLeftPreviewRow() {
    CommonTheme {
        val backgrounds: BackgroundsWithGradients = BackgroundsWithGradientsImpl
        BoxWithBackground(
            modifier = Modifier
                .fillMaxWidth()
                .height(56.dp),
            composableBackground = backgrounds.backgroundRadiantGradientBottomRightToTopLeft(null, null, null, null)
        ) {}
    }
}
@Preview
@Composable
private fun BackgroundRadiantGradientFromCenterToOutsidePreviewScreen() {
    CommonTheme {
        val backgrounds: BackgroundsWithGradients = BackgroundsWithGradientsImpl
        BoxWithBackground(
            modifier = Modifier
                .fillMaxWidth()
                .fillMaxHeight(),
            composableBackground = backgrounds.backgroundRadiantGradientFromCenterToOutside(null, null, null, null)
        ) {}
    }
}
@Preview
@Composable
private fun BackgroundRadiantGradientFromCenterToOutsidePreviewRow() {
    CommonTheme {
        val backgrounds: BackgroundsWithGradients = BackgroundsWithGradientsImpl
        BoxWithBackground(
            modifier = Modifier
                .fillMaxWidth()
                .height(56.dp),
            composableBackground = backgrounds.backgroundRadiantGradientFromCenterToOutside(null, null, null, null)
        ) {}
    }
}
@Preview
@Composable
private fun BackgroundHorizontalGradientPreviewScreen() {
    CommonTheme {
        val backgrounds: BackgroundsWithGradients = BackgroundsWithGradientsImpl
        BoxWithBackground(
            modifier = Modifier
                .fillMaxWidth()
                .fillMaxHeight(),
            composableBackground = backgrounds.backgroundHorizontalGradient(null, null, null, null)
        ) {}
    }
}
@Preview
@Composable
private fun BackgroundHorizontalGradientPreviewRow() {
    CommonTheme {
        val backgrounds: BackgroundsWithGradients = BackgroundsWithGradientsImpl
        BoxWithBackground(
            modifier = Modifier
                .fillMaxWidth()
                .height(56.dp),
            composableBackground = backgrounds.backgroundHorizontalGradient(null, null, null, null)
        ) {}
    }
}
@Preview
@Composable
private fun BackgroundVerticalGradientPreviewScreen() {
    CommonTheme {
        val backgrounds: BackgroundsWithGradients = BackgroundsWithGradientsImpl
        BoxWithBackground(
            modifier = Modifier
                .fillMaxWidth()
                .fillMaxHeight(),
            composableBackground = backgrounds.backgroundVerticalGradient(null, null, null, null)
        ) {}
    }
}
@Preview
@Composable
private fun BackgroundVerticalGradientPreviewRow() {
    CommonTheme {
        val backgrounds: BackgroundsWithGradients = BackgroundsWithGradientsImpl
        BoxWithBackground(
            modifier = Modifier
                .fillMaxWidth()
                .height(56.dp),
            composableBackground = backgrounds.backgroundVerticalGradient(null, null, null, null)
        ) {}
    }
}