package com.aps.coachfanapp.common.util

import android.provider.MediaStore

import android.provider.DocumentsContract

import android.content.ContentUris
import android.content.Context
import android.database.Cursor
import android.net.Uri

import android.os.Environment

import android.os.Build

private const val TAG = "GetFilePathFromDevice"
private const val LOG_ME = true

// https://stackoverflow.com/questions/33011148/picking-image-from-gallery-and-set-to-imageview
object GetFilePathFromDevice {
    /**
     * Get file path from URI
     *
     * @param context context of Activity
     * @param uri     uri of file
     * @return path of given URI
     */
    fun getPath(context: Context, uri: Uri): String? {
        val methodName: String = "getPath"
        if (LOG_ME) ALog.d(TAG, "Method start: $methodName")
        try {
            val isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT
            // DocumentProvider
            if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
                // ExternalStorageProvider
                if (isExternalStorageDocument(uri)) {
                    val docId = DocumentsContract.getDocumentId(uri)
                    val split = docId.split(":".toRegex()).toTypedArray()
                    val type = split[0]
                    if ("primary".equals(type, ignoreCase = true)) {
                        return Environment.getExternalStorageDirectory().toString() + "/" + split[1]
                    }
                } else if (isDownloadsDocument(uri)) {
                    val id = DocumentsContract.getDocumentId(uri)
                    val contentUri: Uri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"),
                        java.lang.Long.valueOf(id)
                    )
                    return getDataColumn(context, contentUri, null, null)
                } else if (isMediaDocument(uri)) {
                    val docId = DocumentsContract.getDocumentId(uri)
                    val split = docId.split(":".toRegex()).toTypedArray()
                    val type = split[0]
                    var contentUri: Uri? = null
                    if ("image" == type) {
                        contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI
                    } else if ("video" == type) {
                        contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI
                    } else if ("audio" == type) {
                        contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI
                    }
                    val selection = "_id=?"
                    val selectionArgs = arrayOf(split[1])
                    return getDataColumn(context, contentUri, selection, selectionArgs)
                }
            } else if ("content".equals(uri.scheme, ignoreCase = true)) {
                // Return the remote address
                return if (isGooglePhotosUri(uri)) uri.lastPathSegment else getDataColumn(
                    context,
                    uri,
                    null,
                    null
                )
            } else if ("file".equals(uri.scheme, ignoreCase = true)) {
                return uri.path
            }
        } catch (e: Exception) {
            ALog.e(TAG, methodName, e)
        } finally {
            if (LOG_ME) ALog.d(TAG, "Method end: $methodName")
        }
        return null
    }

    private fun getDataColumn(
        context: Context,
        uri: Uri?,
        selection: String?,
        selectionArgs: Array<String>?
    ): String? {
        var cursor: Cursor? = null
        val column = "_data"
        val projection = arrayOf(column)
        val methodName: String = "getDataColumn"
        if (LOG_ME) ALog.d(TAG, "Method start: $methodName")
        try {
            uri?.let { uri ->
                cursor = context.contentResolver.query(uri!!, projection, selection, selectionArgs, null)
                cursor?.let{ cursor ->
                    if (cursor.moveToFirst()) {
                        val index: Int = cursor.getColumnIndexOrThrow(column)
                        return cursor.getString(index)
                    }
                } ?: if(LOG_ME) ALog.d(TAG, ".$methodName(): cursor == null")else{}
            } ?: if(LOG_ME) ALog.d(TAG, ".$methodName(): uri == null")else{}
        } catch (e: Exception) {
            ALog.e(TAG, methodName, e)
        } finally {
            cursor?.let { it.close() }
            if (LOG_ME) ALog.d(TAG, "Method end: $methodName")
        }
        return null
    }

    fun isExternalStorageDocument(uri: Uri): Boolean {
        return "com.android.externalstorage.documents" == uri.getAuthority()
    }

    fun isDownloadsDocument(uri: Uri): Boolean {
        return "com.android.providers.downloads.documents" == uri.getAuthority()
    }

    fun isMediaDocument(uri: Uri): Boolean {
        return "com.android.providers.media.documents" == uri.getAuthority()
    }

    fun isGooglePhotosUri(uri: Uri): Boolean {
        return "com.google.android.apps.photos.content" == uri.getAuthority()
    }
}