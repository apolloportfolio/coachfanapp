package com.aps.coachfanapp.common.util

import com.aps.coachfanapp.common.util.extensions.toLogString
import com.aps.coachfanapp.core.di.SimpleDateFormatForFirestore
import com.aps.coachfanapp.core.di.SimpleDateFormatForUser
import com.aps.coachfanapp.core.di.SimpleDateFormatJustYear
import com.google.firebase.Timestamp
import java.security.InvalidParameterException
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

private const val TAG = "DateUtil"
private const val LOG_ME = true

@Singleton
class DateUtil constructor(
    @SimpleDateFormatForFirestore private val dateFormatForFirestore: SimpleDateFormat,
    @SimpleDateFormatForUser private val dateFormatForUser: SimpleDateFormat,
    @SimpleDateFormatJustYear private val dateFormatJustYear: SimpleDateFormat,
)
{
    fun removeTimeFromDateString(sd: String): String{
        return sd.substring(0, sd.indexOf(" "))
    }

    fun convertFirebaseTimestampToStringData(timestamp: Timestamp): String{
        return dateFormatForFirestore.format(timestamp.toDate())
    }

    fun convertFirebaseTimestampToStringDateForUser(timestamp: Timestamp): String{
        val timestampAsDate = timestamp.toDate()
        if(LOG_ME) ALog.d(TAG, ".convertFirebaseTimestampToStringDateForUser(): timestampAsDate == $timestampAsDate")
        return dateFormatForUser.format(timestamp.toDate())
    }

    fun convertFirebaseTimestampToStringWithJustYear(timestamp: Timestamp): String{
        return dateFormatJustYear.format(timestamp.toDate())
    }

    // Date format: "2019-07-23 HH:mm:ss"
    fun convertStringDateToFirebaseTimestamp(date: String): Timestamp{
        return try {
            Timestamp(dateFormatForFirestore.parse(date)!!)
        } catch (e: Exception) {
            if(LOG_ME)ALog.e(TAG, ".convertStringDateToFirebaseTimestamp(): " +
                                    "Exception: ", e)
            now()
        }
    }

    // dates format looks like this: "2019-07-23 HH:mm:ss"
    fun getCurrentTimestamp(): String {
        return dateFormatForFirestore.format(Date())
    }

    // dates format looks like this: "2019-07-23 HH:mm:ss"
    fun getCurrentTimestamp(now: Date): String {
        return dateFormatForFirestore.format(now)
    }

    // dates format looks like this: "2019-07-23 HH:mm:ss"
    fun getFutureTimestamp(daysFromNow: Int): String {
        val now = Date()
        now.time += daysFromNow* MILLISECONDS_IN_DAY
        return dateFormatForFirestore.format(now)
    }

    // dates format looks like this: "2019-07-23 HH:mm:ss"
    fun getFutureTimestamp(now: Date, daysFromNow: Int): String {
//        if(LOG_ME)ALog.d(TAG, ".getFutureTimestamp(): daysFromNow == $daysFromNow")
//        if(LOG_ME)ALog.d(TAG, ".getFutureTimestamp(): now before: ${dateFormatForFirestore.format(now)}")
//        if(LOG_ME)ALog.d(TAG, ".getFutureTimestamp(): now.time before: ${now.time}")
        now.time += daysFromNow* MILLISECONDS_IN_DAY
//        if(LOG_ME)ALog.d(TAG, ".getFutureTimestamp(): now after: ${dateFormatForFirestore.format(now)}")
//        if(LOG_ME)ALog.d(TAG, ".getFutureTimestamp(): now.time after: ${now.time}")
        return dateFormatForFirestore.format(now)
    }

    // dates format looks like this: "2019-07-23 HH:mm:ss"
    fun getPastTimestamp(now: Date, daysFromNow: Int): String {
        now.time -= daysFromNow* MILLISECONDS_IN_DAY
        return dateFormatForFirestore.format(now)
    }

    fun convertGregorianCalendarToTimestamp(gregorianCalendar: GregorianCalendar?) : Timestamp? {
        gregorianCalendar?.let { return Timestamp(Date(gregorianCalendar.timeInMillis)) }
        return null
    }

    fun now() : Timestamp {
        return Timestamp.now()
    }

    fun dateIsBeforeNow(
        firestoreTimestampAsString: String? = null,
        nowOffsetInDays: Int = 0,
    ): Boolean {
        val methodName: String = "dateIsBeforeNow"
        if (LOG_ME) ALog.d(TAG, "Method start: $methodName")
        var result = false
        try {
            val firestoreTimestamp = if(firestoreTimestampAsString != null) {
                convertStringDateToFirebaseTimestamp(firestoreTimestampAsString)
            } else {
                Timestamp.now()
            }
            val date = firestoreTimestamp.toDate()
            val now = Date()
            val nowWithOffSet = Date(now.time + nowOffsetInDays* MILLISECONDS_IN_DAY)
            if(LOG_ME) ALog.d(
                TAG, ".$methodName(): \n" +
                    "firestoreTimestampAsString == $firestoreTimestampAsString\n" +
                    "date == $date\n" +
                    "now == $now\n" +
                    "nowOffsetInDays == $nowOffsetInDays\n" +
                    "nowWithOffSet == $nowWithOffSet\n" +
                    "result == $result"
            )
            result = date.before(nowWithOffSet)
        } catch (e: Exception) {
            ALog.e(TAG, methodName, e)
        } finally {
            if (LOG_ME) ALog.d(TAG, "Method end: $methodName")
        }
        return result
    }

    fun dateIsAfterNow(
        firestoreTimestampAsString: String? = null,
        nowOffsetInDays: Int = 0,
    ): Boolean {
        val methodName: String = "dateIsAfterNow"
        if (LOG_ME) ALog.d(TAG, "Method start: $methodName")
        var result = false
        try {
            val firestoreTimestamp = if(firestoreTimestampAsString != null) {
                convertStringDateToFirebaseTimestamp(firestoreTimestampAsString)
            } else {
                Timestamp.now()
            }
            val date = firestoreTimestamp.toDate()
            val now = Date()
            val nowWithOffSet = Date(now.time + nowOffsetInDays* MILLISECONDS_IN_DAY)
            if(LOG_ME) ALog.d(
                TAG, ".$methodName(): \n" +
                    "firestoreTimestampAsString == $firestoreTimestampAsString\n" +
                    "date == $date\n" +
                    "now == $now\n" +
                    "nowOffsetInDays == $nowOffsetInDays\n" +
                    "nowWithOffSet == $nowWithOffSet\n" +
                    "result == $result"
            )
            result = date.after(nowWithOffSet)
        } catch (e: Exception) {
            ALog.e(TAG, methodName, e)
        } finally {
            if (LOG_ME) ALog.d(TAG, "Method end: $methodName")
        }
        return result
    }

    fun daysPassedSinceDateToNow(firestoreTimestamp: Timestamp): Int {
        val millisecondsBetweenDates = Date().time - firestoreTimestamp.toDate().time
        return (millisecondsBetweenDates/1000/60/60/24).toInt()
    }

    fun addDays(
        gregorianCalendar: GregorianCalendar,
        numberOfDaysToAdd: Int,
    ): GregorianCalendar {
        return gregorianCalendar.apply {
            timeInMillis += numberOfDaysToAdd* MILLISECONDS_IN_DAY
        }
    }

    companion object {
        private val dateFormatForFirestore = SimpleDateFormat("yyyy-MM-dd hh:mm:ss a", Locale.ENGLISH)
        const val MILLISECONDS_IN_DAY: Long = 24*60*60*1000
        init {
            dateFormatForFirestore.timeZone = TimeZone.getTimeZone("UTC-7") // match firestore
        }

        fun getDateUtilForPreview(): DateUtil {
            val dateFormatForFirestore = SimpleDateFormat("yyyy-MM-dd hh:mm:ss a", Locale.ENGLISH)
            val dateFormatForUser = SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH)
            val dateFormatJustYear = SimpleDateFormat("yyyy", Locale.ENGLISH)
            return DateUtil(dateFormatForFirestore, dateFormatForUser, dateFormatJustYear)
        }

        fun getPastTimestampAsStringForPreview(
            nrOfDaysInPast: Int = 6,
            specificTimestamp: Timestamp? = null,
        ): String {
            val dateUtil = getDateUtilForPreview()
            return if(specificTimestamp != null) {
                dateUtil.convertFirebaseTimestampToStringData(specificTimestamp)
            } else {
                dateUtil.getPastTimestamp(Date(), nrOfDaysInPast)
            }
        }

        // https://stackoverflow.com/questions/57445264/simpledateformat-returns-wrong-time-from-firestore-timestamp-why
        fun convertGregorianCalendarToTimestamp(gregorianCalendar: GregorianCalendar?) : Timestamp? {
            val methodName: String = "convertGregorianCalendarToTimestamp"
            if (LOG_ME) ALog.d(TAG, "Method start: $methodName")
            if(LOG_ME) ALog.d(TAG, ".$methodName(): gregorianCalendar:\n${gregorianCalendar.toLogString()}")
            return gregorianCalendar?.let {
                val seconds = it.timeInMillis/1000
                val nanoseconds = 1000*it.timeInMillis%1000
                if(LOG_ME) ALog.d(TAG, ".$methodName(): ${it.timeInMillis}    $seconds    $nanoseconds")
                val timestamp = Timestamp(seconds, nanoseconds.toInt())
                if(LOG_ME) ALog.d(TAG, ".$methodName(): timestamp:\n${timestamp.toLogString()}")
                if (LOG_ME) ALog.d(TAG, "Method end: $methodName")
                timestamp
            }
//            gregorianCalendar?.let { return Timestamp(Date(gregorianCalendar.timeInMillis)) }
//            return null
        }

        fun convertTimestampToGregorianCalendar(timestamp : Timestamp?) : GregorianCalendar? {
            timestamp?.let {
                val gregorianCalendar = GregorianCalendar()
                gregorianCalendar.timeInMillis = timestamp.seconds*1000 + timestamp.nanoseconds
                return gregorianCalendar
            }
            return null
        }

        fun convertStringDateToFirebaseTimestamp(date: String): Timestamp{
            return Timestamp(dateFormatForFirestore.parse(date))
        }

        fun convertFirebaseTimestampToStringData(timestamp: Timestamp): String{
            return dateFormatForFirestore.format(timestamp.toDate())
        }

        fun sortTwoDates(date1 : Timestamp, date2 : Timestamp) : Pair<Timestamp, Timestamp> {
            return if(date1 >= date2) {
                Pair(date2, date1)
            } else {
                Pair(date1, date2)
            }
        }

        fun getNumberOfDaysBetweenDates(date1 : GregorianCalendar, date2 : GregorianCalendar) : Long {
            if(date2.before(date1))throw InvalidParameterException("date2 should be before date1")
            else {
                val differenceInMillis = date2.timeInMillis-date1.timeInMillis
                return (TimeUnit.DAYS).convert(differenceInMillis, TimeUnit.MILLISECONDS)
            }
        }

        fun getNumberOfDaysBetweenDates(date1 : Timestamp, date2 : Timestamp) : Long {
            val date1Millis = date1.seconds * 1000 + date1.nanoseconds / 1000000
            val date2Millis = date2.seconds * 1000 + date2.nanoseconds / 1000000
            if(date1Millis>date2Millis) {
//                throw InvalidParameterException("date1 should be before date2")
                if(LOG_ME) ALog.d(
                    TAG, ".getNumberOfDaysBetweenDates(): " +
                        "date1 is after date2")
                val differenceInMillis = date1Millis - date2Millis
                return -1*(TimeUnit.DAYS).convert(differenceInMillis, TimeUnit.MILLISECONDS)
            }
            else {
                if(LOG_ME) ALog.d(
                    TAG, ".getNumberOfDaysBetweenDates(): " +
                        "date2 is after date1")
                val differenceInMillis = date2Millis - date1Millis
                return (TimeUnit.DAYS).convert(differenceInMillis, TimeUnit.MILLISECONDS)
            }
        }

        fun now() : Timestamp {
            return Timestamp.now()
        }
    }
}