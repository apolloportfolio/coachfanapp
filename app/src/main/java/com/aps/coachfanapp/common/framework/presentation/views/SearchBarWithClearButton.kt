package com.aps.coachfanapp.common.framework.presentation.views

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.material.TextField
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalSoftwareKeyboardController
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.semantics.contentDescription
import androidx.compose.ui.semantics.semantics
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.aps.coachfanapp.common.framework.presentation.values.CommonTheme
import com.aps.coachfanapp.R

@OptIn(ExperimentalComposeUiApi::class)
@Composable
fun SearchBarWithClearButton(
    searchQuery: String,
    onSearchQueryChange: (String) -> Unit,
    onSearchBackClick: () -> Unit,
    onSearchClick: () -> Unit,
    composableBackground: DrawnBackground? = null,
    backgroundDrawableId: Int? = R.drawable.list_item_example_background_1,
) {
    val searchQueryEditTextContentDescription = stringResource(
        id = (R.string.search_query_edittext_content_description)
    )
    val searchButtonContentDescription = stringResource(
        id = (R.string.search_button_content_description)
    )
    val searchBackButtonContentDescription = stringResource(
        id = (R.string.search_back_button_content_description)
    )

    val softwareKeyboardController = LocalSoftwareKeyboardController.current


    BoxWithBackground(
        backgroundDrawableId = backgroundDrawableId,
        composableBackground = composableBackground,
    ) {

        Row(
            modifier = Modifier
                .fillMaxWidth()
                .padding(horizontal = 16.dp, vertical = 8.dp),
            verticalAlignment = Alignment.CenterVertically
        ) {
            IconButton(
                onClick = {
                    onSearchBackClick()
                    onSearchQueryChange("") // Clear the searchQuery text
                    softwareKeyboardController?.hide() // Hide the on-screen keyboard
                },
                modifier = Modifier
                    .size(48.dp)
                    .background(Color.Transparent)
                    .semantics {
                        contentDescription = searchBackButtonContentDescription
                    }
            ) {
                Icon(
                    painter = painterResource(R.drawable.ic_back),
                    contentDescription = stringResource(R.string.hide_options),
                    modifier = Modifier.size(24.dp),
                    tint = MaterialTheme.colors.secondary,
                )
            }

            TextField(
                value = searchQuery,
                onValueChange = onSearchQueryChange,
                modifier = Modifier
                    .weight(1f)
                    .padding(horizontal = 8.dp)
                    .height(48.dp)
                    .background(Color.Transparent)
                    .semantics {
                        contentDescription = searchQueryEditTextContentDescription
                    },
                textStyle = TextStyle(fontSize = 14.sp),
                placeholder = { Text(text = stringResource(R.string.search)) },
            )

            IconButton(
                onClick = {
                    onSearchClick()
                    softwareKeyboardController?.hide() // Hide the on-screen keyboard
                },
                modifier = Modifier
                    .size(48.dp)
                    .background(Color.Transparent)
                    .semantics {
                        contentDescription = searchButtonContentDescription
                    }
            ) {
                Icon(
                    painter = painterResource(R.drawable.ic_search_yellow_24),
                    contentDescription = stringResource(R.string.search),
                    modifier = Modifier.size(24.dp),
                    tint = MaterialTheme.colors.secondary,
                )
            }
        }
    }
}

// Preview =========================================================================================
@Preview
@Composable
fun SearchBarWithClearButtonPreview1() {
    CommonTheme {
        SearchBarWithClearButton(
            searchQuery = "",
            onSearchQueryChange = {},
            onSearchBackClick = {},
            onSearchClick = {},
        )
    }
}

@Preview
@Composable
fun SearchBarWithClearButtonPreview2() {
    CommonTheme {
        SearchBarWithClearButton(
            searchQuery = "",
            onSearchQueryChange = {},
            onSearchBackClick = {},
            onSearchClick = {},
            composableBackground = BackgroundsOfLayoutsImpl.backgroundScreen01(
                colors = null,
                brush = null,
                shape = null,
                alpha = 0.6f,
            ),
        )
    }
}