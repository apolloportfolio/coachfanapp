package com.aps.coachfanapp.common.util.composableutils

import android.annotation.SuppressLint
import androidx.compose.runtime.Composable
import androidx.compose.ui.platform.LocalContext
import com.aps.coachfanapp.common.util.ALog

@SuppressLint("DiscouragedApi")
@Composable
fun getDrawableIdInPreview(prefix: String, suffix: Int): Int? {
    val context = LocalContext.current // Use LocalContext to get the current context
    return try {
        context.resources.getIdentifier("$prefix$suffix", "drawable", context.packageName)
    } catch (e: Exception) {
        ALog.e("getDrawableIdInPreview", ".getDrawableIdInPreview(): " +
                                "Exception: ", e)
        null
    }
}