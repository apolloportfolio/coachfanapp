package com.aps.coachfanapp.common.business.interactors.implementation

import com.aps.coachfanapp.R
import com.aps.coachfanapp.common.business.data.cache.CacheResponseHandler
import com.aps.coachfanapp.common.business.data.cache.StandardCacheDataSource
import com.aps.coachfanapp.common.business.data.network.StandardNetworkDataSource
import com.aps.coachfanapp.common.business.data.util.safeCacheCall
import com.aps.coachfanapp.common.business.domain.state.*
import com.aps.coachfanapp.common.business.interactors.abstraction.GetEntity
import com.aps.coachfanapp.common.util.ALog
import com.aps.coachfanapp.common.util.DateUtil
import com.aps.coachfanapp.common.util.UniqueID
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

private const val TAG = "GetEntityImpl"
private const val LOG_ME = true

class GetEntityImpl<
        Entity,
        CacheDataSource: StandardCacheDataSource<Entity>,
        NetworkDataSource: StandardNetworkDataSource<Entity>,
        ViewState: com.aps.coachfanapp.common.business.domain.state.ViewState<Entity>>
@Inject constructor(
    val cacheDataSource: CacheDataSource,
    private val networkDataSource: NetworkDataSource,
    private val dateUtil: DateUtil
): GetEntity<
        Entity,
        CacheDataSource,
        NetworkDataSource,
        ViewState>
{

    override fun getEntity(
        id: UniqueID,
        stateEvent: StateEvent,
        returnViewState: ViewState,
        updateReturnViewState: (ViewState, Entity?)->ViewState,
        getUpdateDate: (Entity)->String?,
        setUpdateDate: (Entity, String)->Entity,
    ): Flow<DataState<ViewState>?> = flow {
        val cacheResult = safeCacheCall(Dispatchers.IO){
            syncEntity(id, getUpdateDate, setUpdateDate)
        }

        val response = object: CacheResponseHandler<ViewState, Entity>(
            response = cacheResult,
            stateEvent = stateEvent
        ){
            override suspend fun handleSuccess(resultObj: Entity): DataState<ViewState>? {
                var message: String? = GET_ENTITY_SUCCESS
                var uiComponentType: UIComponentType? = UIComponentType.None()
                ALog.d("GetEntityImpl", "getEntity().handleSuccess(): ")


                if(resultObj == null){
                    ALog.d("GetEntityImpl", "getEntity(): resultObj == null")
                    message = GET_ENTITY_NO_MATCHING_RESULTS
                    uiComponentType = UIComponentType.Toast()
                } else {
                    ALog.d("GetEntityImpl", "getEntity(): resultObj != null")
                    returnViewState.mainEntity = resultObj
                    updateReturnViewState(returnViewState, resultObj)
                }
                return DataState.data(
                    response = Response(
                        messageId = R.string.error,
                        message = message,
                        uiComponentType = uiComponentType as UIComponentType,
                        messageType = MessageType.Success()
                    ),
                    data = returnViewState,
                    stateEvent = stateEvent
                )
            }
        }.getResult()

        emit(response)
    }

    private suspend fun syncEntity(
            entityId: UniqueID,
            getUpdateDate: (Entity)->String?,
            setUpdateDate: (Entity, String)->Entity
    ): Entity? {
        val methodName: String = "syncEntity"
        if (LOG_ME) ALog.d(TAG, "Method start: $methodName")
        try {
            val cachedEntity = cacheDataSource.getEntityById(entityId)
            val networkEntity = networkDataSource.getEntityById(entityId)// ?: return null
            if(networkEntity == null) {
                if (LOG_ME) ALog.w(TAG,  "$methodName(): networkEntity == null")
                return null
            }

            if(cachedEntity == null) {
                if (LOG_ME) ALog.d(TAG,  "$methodName(): cachedEntity == null && networkEntity != null")
                cacheDataSource.insertEntity(networkEntity!!)
                return networkEntity
            } else {
                if (LOG_ME) ALog.d(TAG,  "$methodName(): cachedEntity != null && networkEntity != null")
                val networkEntityUpdatedAt = getUpdateDate(networkEntity!!)
                val cacheEntityUpdatedAt = getUpdateDate(cachedEntity)
                if(networkEntityUpdatedAt != null && cacheEntityUpdatedAt != null) {
                    if (LOG_ME) ALog.d(TAG,  "$methodName(): networkEntityUpdatedAt != null && cacheEntityUpdatedAt != null")
                    if(dateUtil.convertStringDateToFirebaseTimestamp(networkEntityUpdatedAt) > dateUtil.convertStringDateToFirebaseTimestamp(cacheEntityUpdatedAt)) {
                        if(LOG_ME) ALog.d(TAG, "$methodName(): network entity is up to date. Updating cache entity")
                        return updateCacheEntity(networkEntity)
                    } else if(dateUtil.convertStringDateToFirebaseTimestamp(networkEntityUpdatedAt) == dateUtil.convertStringDateToFirebaseTimestamp(cacheEntityUpdatedAt)) {
                        if(LOG_ME) ALog.d(TAG, "$methodName(): Both entities have the same update date")
                        return networkEntity
                    } else {
                        if(LOG_ME) ALog.d(TAG, "$methodName(): cache entity is up to date. Updating network entity")
                        return updateNetworkEntity(cachedEntity)
                    }
                } else if(networkEntityUpdatedAt == null && cacheEntityUpdatedAt != null) {
                    if(LOG_ME) ALog.d(TAG, "$methodName(): network entity has no update date but cache has. Updating network entity")
                    return updateNetworkEntity(cachedEntity)
                } else if(networkEntityUpdatedAt != null && cacheEntityUpdatedAt == null) {
                    if(LOG_ME) ALog.d(TAG, "$methodName(): cache entity has no update date but network has. Updating cache entity")
                    return updateCacheEntity(networkEntity)
                } else {
                    if(LOG_ME) ALog.d(TAG, "$methodName(): ")
                    setUpdateDate(networkEntity, dateUtil.getCurrentTimestamp())
                    updateCacheEntity(networkEntity)
                    return updateNetworkEntity(networkEntity)
                }
            }
        } catch (e: Exception) {
            ALog.e(TAG, methodName, e)
            return null
        } finally {
            if (LOG_ME) ALog.d(TAG, "Method end: $methodName")
        }
    }

    private suspend fun updateNetworkEntity(entity: Entity): Entity {
        safeCacheCall(Dispatchers.IO){
            cacheDataSource.insertOrUpdateEntity(entity)
        }// retain network timestamp
        return entity
    }

    private suspend fun updateCacheEntity(entity: Entity): Entity {
        safeCacheCall(Dispatchers.IO){
            cacheDataSource.insertOrUpdateEntity(entity)
        }// retain network timestamp
        return entity
    }

    companion object{
        const val GET_ENTITY_SUCCESS = "Successfully got entity."
        const val GET_ENTITY_NO_MATCHING_RESULTS = "There are no entities that match that query."
        const val SEARCH_ENTITIES_FAILED = "Failed to retrieve the list of entities."
    }
}