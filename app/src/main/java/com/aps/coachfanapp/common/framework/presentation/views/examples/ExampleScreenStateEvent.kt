package com.aps.coachfanapp.common.framework.presentation.views.examples

import android.content.Context
import com.aps.coachfanapp.core.util.ApplicationContextProvider
import com.aps.coachfanapp.R
import com.aps.coachfanapp.common.business.domain.state.StateEvent
import com.aps.coachfanapp.common.business.domain.state.StateMessage
import com.aps.coachfanapp.core.business.domain.model.entities.ProjectUser
import java.io.File

sealed class ExampleScreenStateEvent: StateEvent {


	class UpdateMainEntityEvent(
        val mainEntity: ProjectUser,
        val context: Context,
    ): ExampleScreenStateEvent(){

        override fun errorInfo(): String {
            return context.getString(R.string.homeScreen_state_event_update_main_entity_error)
        }

        override fun eventName(): String {
            return "UpdateMainEntityEvent"
        }

        override fun shouldDisplayProgressBar() = true
    }

    class CreateStateMessageEvent(
        val stateMessage: StateMessage,
        val context: Context,
    ): ExampleScreenStateEvent(){

        override fun errorInfo(): String {
            return context.getString(R.string.homeScreen_state_event_create_state_message_error)
        }

        override fun eventName(): String {
            return "CreateStateMessageEvent"
        }

        override fun shouldDisplayProgressBar() = false
    }

    class None(
        val stateMessage: StateMessage,
        val context: Context,
    ): ExampleScreenStateEvent(){

        override fun errorInfo(): String {
            return context.getString(R.string.homeScreen_state_event_none_error)
        }

        override fun eventName(): String {
            return "None"
        }

        override fun shouldDisplayProgressBar() = false
    }

    class SetDrawerOpen(
        val isDrawerOpen: Boolean?,
        val context: Context,
    ) : ExampleScreenStateEvent() {

        override fun errorInfo(): String {
            return context
                .getString(R.string.homeScreen_state_event_set_drawer_open_error) + " $isDrawerOpen"
        }

        override fun eventName(): String {
            return "SetDrawerOpen"
        }

        override fun shouldDisplayProgressBar() = true

    }


    class SaveInFileAndClearSearchedEntities1ListStateEvent(
        val internalDirectory: File,
        val context: Context,
    ) : ExampleScreenStateEvent() {

        override fun errorInfo(): String {
            return context.getString(R.string.homeScreen_state_event_save_and_clear_searched_items_list_error)
        }

        override fun eventName(): String {
            return "SaveInFileAndClearSearchedEntities1ListStateEvent"
        }

        override fun shouldDisplayProgressBar() = true

    }

    class ClearFiltersStateEvent(
        val context: Context,
    ) : ExampleScreenStateEvent() {

        override fun errorInfo(): String {
            return context.getString(R.string.homeScreen_state_event_clear_filters_error)
        }

        override fun eventName(): String {
            return "ClearFiltersStateEvent"
        }

        override fun shouldDisplayProgressBar() = false

    }

    class AskUserAboutLogout(
        val context: Context,
    ) : ExampleScreenStateEvent() {

        override fun errorInfo(): String {
            return context.getString(R.string.homeScreen_state_event_ask_user_about_logout_error)
        }

        override fun eventName(): String {
            return "AskUserAboutLogout"
        }

        override fun shouldDisplayProgressBar() = true

    }

    class LogoutUser(
        val context: Context,
    ) : ExampleScreenStateEvent() {

        override fun errorInfo(): String {
            return context.getString(R.string.homeScreen_state_event_logout_user_error)
        }

        override fun eventName(): String {
            return "LogoutUser"
        }

        override fun shouldDisplayProgressBar() = true

    }

    class CheckGooglePayAvailability(
        val context: Context,
        val continueFlag: Boolean?,
    ) : ExampleScreenStateEvent() {

        override fun errorInfo(): String {
            return context.getString(R.string.homeScreen_state_event_check_google_pay_availability_error)
        }

        override fun eventName(): String {
            return "CheckGooglePayAvailability"
        }

        override fun shouldDisplayProgressBar() = true

    }

    class GetMerchantName(
        val continueFlag: Boolean?,
        val context: Context,
    ) : ExampleScreenStateEvent() {

        override fun errorInfo(): String {
            return context.getString(R.string.homeScreen_state_event_get_merchant_name_error)
        }

        override fun eventName(): String {
            return "GetMerchantName"
        }

        override fun shouldDisplayProgressBar() = true

    }

    class ShowDialogSayingThatGooglePayIsUnavailable(
        val context: Context,
    ): ExampleScreenStateEvent() {

        override fun errorInfo(): String {
            return context.getString(R.string.homeScreen_state_event_show_dialog_saying_that_google_pay_is_unavailable_error)
        }

        override fun eventName(): String {
            return "ShowDialogSayingThatGooglePayIsUnavailable"
        }

        override fun shouldDisplayProgressBar() = false

    }

    class DownloadExchangeRates(
        val currencies: ArrayList<String>,
        val continueFlag: Boolean?,
        val context: Context,
    ): ExampleScreenStateEvent() {

        override fun errorInfo(): String {
            return context.getString(R.string.homeScreen_state_event_download_exchange_rates_error)
        }

        override fun eventName(): String {
            return "DownloadExchangeRates"
        }

        override fun shouldDisplayProgressBar() = true

    }

    class InitiateGooglePayPaymentProcess(
        val context: Context,
    ): ExampleScreenStateEvent() {

        override fun errorInfo(): String {
            return context.getString(R.string.homeScreen_state_event_initialize_google_pay_process_error)
        }

        override fun eventName(): String {
            return "InitiateGooglePayPaymentProcess"
        }

        override fun shouldDisplayProgressBar() = true

    }

    class GetGatewayNameAndMerchantID(
        val continueFlag: Boolean?,
        val context: Context,
    ): ExampleScreenStateEvent() {

        override fun errorInfo(): String {
            return context.getString(R.string.homeScreen_state_event_get_gateway_name_and_merchant_id_error)
        }

        override fun eventName(): String {
            return "GetGatewayNameAndMerchantID"
        }

        override fun shouldDisplayProgressBar() = true

    }

    class RateApplication(
        val rateAppFunction : () -> (Unit),
        val context: Context,
    ): ExampleScreenStateEvent(){

        override fun errorInfo(): String {
            return context.getString(R.string.homeScreen_state_event_navigate_to_rate_app_error)
        }

        override fun eventName(): String {
            return "RateApplication"
        }

        override fun shouldDisplayProgressBar() = false
    }

    class Navigate(
        val navigationFunction : () -> (Unit),
        val context: Context,
    ): ExampleScreenStateEvent(){

        override fun errorInfo(): String {
            return context.getString(R.string.homeScreen_state_event_navigate_error)
        }

        override fun eventName(): String {
            return "Navigate"
        }

        override fun shouldDisplayProgressBar() = false
    }

    class RecommendApp(
        val recommendationFunction : () -> (Unit),
        val context: Context,
    ) : ExampleScreenStateEvent() {

        override fun errorInfo(): String {
            return context.getString(R.string.homeScreen_state_event_recommend_app_error)
        }

        override fun eventName(): String {
            return "RecommendApp"
        }

        override fun shouldDisplayProgressBar() = false

    }

    class ReportProblem(
        val reportProblemFunction : () -> (Unit),
        val context: Context,
    ) : ExampleScreenStateEvent() {

        override fun errorInfo(): String {
            return context.getString(R.string.homeScreen_state_event_report_problem_error)
        }

        override fun eventName(): String {
            return "ReportProblem"
        }

        override fun shouldDisplayProgressBar() = false

    }

    class NotifyUserAboutLackOfEmailAppInstalled(
        val context: Context,
    ) : ExampleScreenStateEvent() {

        override fun errorInfo(): String {
            return context.getString(R.string.homeScreen_state_event_notify_that_some_no_email_app_is_installed_error)
        }

        override fun eventName(): String {
            return "NotifyUserAboutLackOfEmailAppInstalled"
        }

        override fun shouldDisplayProgressBar() = false

    }

    class GetUsersRating(
        val context: Context,
    ) : ExampleScreenStateEvent() {

        override fun errorInfo(): String {
            return context.getString(R.string.homeScreen_state_event_get_users_rating_error)
        }

        override fun eventName(): String {
            return "GetUsersRating"
        }

        override fun shouldDisplayProgressBar() = true

    }

}
