package com.aps.coachfanapp.common.framework.presentation.views

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.tooling.preview.PreviewParameter
import androidx.compose.ui.tooling.preview.PreviewParameterProvider
import androidx.compose.ui.unit.sp
import com.aps.coachfanapp.common.framework.presentation.values.CommonTheme

@Composable
fun TipContentIsUnavailable(
    tip: String,
    modifier: Modifier = Modifier.fillMaxSize(),
) {
    Box(
        modifier = modifier,
        contentAlignment = Alignment.Center
    ) {
        Text(
            text = tip,
            textAlign = TextAlign.Center,
            fontSize = 28.sp,
            maxLines = 3,
            fontWeight = FontWeight.Bold,
            color = MaterialTheme.colors.onSurface,
            style = MaterialTheme.typography.h4
        )
    }
}

data class TipWhenListIsEmptyParams(
    val tip: String = "List is empty"
)

@Preview
@Composable
fun TipWhenListIsEmptyPreview(
    @PreviewParameter(
        TipWhenListIsEmptyParamsProvider::class
    ) params: TipWhenListIsEmptyParams
) {
    CommonTheme {
        TipContentIsUnavailable(params.tip)
    }
}

class TipWhenListIsEmptyParamsProvider : PreviewParameterProvider<TipWhenListIsEmptyParams> {
    override val values: Sequence<TipWhenListIsEmptyParams> = sequenceOf(
        TipWhenListIsEmptyParams("No items found"),
        TipWhenListIsEmptyParams("Empty list"),
        TipWhenListIsEmptyParams("For now the list is empty. \nAdd an item to it or try again later."),
    )
}
