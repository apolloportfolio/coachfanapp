package com.aps.coachfanapp.common.framework.presentation.views

import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Icon
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Phone
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.aps.coachfanapp.R
import com.aps.coachfanapp.common.framework.presentation.values.CommonTheme

@Composable
fun RowWithLabelAndValueType01(
    icon: ImageVector?,
    label: String,
    value: String,
    tint: Color = MaterialTheme.colors.onSurface,
    borderWidth: Dp = 2.dp,
    cornerRadius: Dp = 8.dp,
    composableBackground: DrawnBackground? = null,
    backgroundDrawableId: Int = R.drawable.list_item_example_background_1,
    leftImageTint: Color = MaterialTheme.colors.onSurface,
    labelTextStyle: TextStyle = TextStyle(
        fontFamily = FontFamily.Default,
        fontWeight = FontWeight.SemiBold,
        fontSize = 14.sp
    ),
    valueTextStyle: TextStyle = TextStyle(
        fontFamily = FontFamily.Default,
        fontWeight = FontWeight.Normal,
        fontSize = 16.sp
    ),
    modifier: Modifier = Modifier,
) {
    val transparentColor = remember {
        Color(0x00000000)
    }

    BoxWithBackground(
        backgroundDrawableId = backgroundDrawableId,
        composableBackground = composableBackground,
    ) {
        Row(
            verticalAlignment = Alignment.CenterVertically,
            modifier = Modifier
                .fillMaxWidth()
                .background(
                    color = transparentColor,
                    shape = RoundedCornerShape(cornerRadius)
                )
                .border(
                    width = borderWidth,
                    color = MaterialTheme.colors.onBackground,
                    shape = RoundedCornerShape(cornerRadius)
                )
                .padding(8.dp)
        ) {
            icon?.let {
                Icon(
                    imageVector = icon,
                    contentDescription = label,
                    tint = tint,
                    modifier = Modifier
                        .padding(8.dp),
                )
            }
            Spacer(modifier = Modifier.width(8.dp))
            Column {
                Text(
                    text = label,
                    style = labelTextStyle,
                    color = tint,
                )
                Text(
                    text = value,
                    style = valueTextStyle,
                    color = tint,
                )
            }
        }
    }
}


// Preview =========================================================================================
@Preview
@Composable
internal fun RowWithLabelAndValueType01Preview1() {
    CommonTheme {
        RowWithLabelAndValueType01(
            icon = Icons.Default.Phone,
            label = "PreviewLabel: ",
            value = "preview value",
            tint = MaterialTheme.colors.onBackground,
            borderWidth = 2.dp,
            cornerRadius = 8.dp,
            backgroundDrawableId = R.drawable.list_item_example_background_1,
            composableBackground = BackgroundsOfLayoutsImpl.backgroundTitleType01(
                colors = null,
                brush = null,
                shape = null,
                alpha = null,
            ),
            leftImageTint = MaterialTheme.colors.onSurface,
        )
    }
}