package com.aps.coachfanapp.common.business.data.cache

import com.aps.coachfanapp.R
import com.aps.coachfanapp.common.business.data.cache.CacheErrors.CACHE_DATA_NULL
import com.aps.coachfanapp.common.business.domain.state.*

abstract class CacheResponseHandler <ViewState, Data>(
    private val response: CacheResult<Data?>,
    private val stateEvent: StateEvent?
){
    suspend fun getResult(): DataState<ViewState>? {

        return when(response){

            is CacheResult.GenericError -> {
                DataState.error(
                    response = Response(
                        messageId = R.string.error,
                        message = "${stateEvent?.errorInfo()}\n\nReason: ${response.errorMessage}",
                        uiComponentType = UIComponentType.Dialog(),
                        messageType = MessageType.Error()
                    ),
                    stateEvent = stateEvent
                )
            }

            is CacheResult.Success -> {
                if(response.value == null){
                    DataState.error(
                        response = Response(
                            messageId = R.string.error,
                            message = "${stateEvent?.errorInfo()}\n\nReason: ${CACHE_DATA_NULL}.",
                            uiComponentType = UIComponentType.Dialog(),
                            messageType = MessageType.Error()
                        ),
                        stateEvent = stateEvent
                    )
                }
                else{
                    handleSuccess(resultObj = response.value)
                }
            }

        }
    }

    abstract suspend fun handleSuccess(resultObj: Data): DataState<ViewState>?

    override fun toString(): String {
        return when(response){

            is CacheResult.GenericError -> {
                "CacheResult.GenericError: " +
                        "response == " + response.toString() + ";    " +
                        "stateEvent == " + stateEvent.toString()
            }

            is CacheResult.Success -> {
                if(response.value == null){
                    "CacheResult.Success: " +
                            "response == " + response.toString() + ";    " +
                            "stateEvent == " + stateEvent.toString() + ";    response.value == null"
                }
                else{
                    "CacheResult.Success: " +
                            "response == " + response.toString() + ";    " +
                            "stateEvent == " + stateEvent.toString() + ";    " +
                            "response.value = " + response.value.toString()
                }
            }

        }
    }
}