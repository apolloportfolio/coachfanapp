package com.aps.coachfanapp.common.framework.presentation.layouts

import android.location.Location
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.LazyListScope
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.pullrefresh.PullRefreshIndicator
import androidx.compose.material.pullrefresh.pullRefresh
import androidx.compose.material.pullrefresh.rememberPullRefreshState
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.aps.coachfanapp.common.framework.presentation.values.CommonTheme
import com.aps.coachfanapp.common.framework.presentation.views.ListItemType03
import com.aps.coachfanapp.common.util.ALog
import com.aps.coachfanapp.common.util.DeviceLocation
import com.aps.coachfanapp.common.util.GeoLocationUtilities
import com.aps.coachfanapp.common.util.PreviewEntityFactory

private const val TAG = "LazyColumnWithPullToRefresh"
private const val LOG_ME = true
const val LazyColumnWithPullToRefreshTestTag = TAG

// Reference: https://google.github.io/accompanist/swiperefresh/
@OptIn(ExperimentalMaterialApi::class)
@Composable
fun LazyColumnWithPullToRefresh(
    modifier: Modifier = Modifier,
    contentPadding: PaddingValues = PaddingValues(0.dp, 0.dp, 0.dp, 0.dp),
    verticalArrangement: Arrangement.Vertical = Arrangement.spacedBy(0.dp),
    onRefresh: () -> Unit = {},
    isRefreshing: Boolean,
    content: LazyListScope.() -> Unit
) {

    val pullRefreshState = rememberPullRefreshState(isRefreshing, { onRefresh() })
    Box(modifier = modifier
        .pullRefresh(pullRefreshState)
    ) {
        LazyColumn(
            contentPadding = contentPadding,
            verticalArrangement = verticalArrangement,
            modifier = Modifier
                .testTag(LazyColumnWithPullToRefreshTestTag)
                .fillMaxWidth(),
        ) {
            content()
        }
        PullRefreshIndicator(
            isRefreshing,
            pullRefreshState,
            Modifier
                .align(Alignment.Center)
                .background(color = Color.Transparent),
        )
    }
}

// Preview =========================================================================================
@Preview(
    showSystemUi = true,
)
@Composable
fun LazyColumnWithPullToRefreshPreview() {
    CommonTheme {
        val entities = PreviewEntityFactory.createPreviewEntitiesList()
        val deviceLocation = DeviceLocation(
            locationPermissionGranted = true,
            location = Location("Lublin").apply {
                latitude = 51.2465
                longitude = 22.5684
            }
        )
        LazyColumnWithPullToRefresh(
            isRefreshing = false,
            content = {
                itemsIndexed(entities) { index, item ->
                    ListItemType03(
                        index = index,
                        itemTitleString = item.name,
                        itemPriceString = "",
                        itemLikesString = "",
                        itemDistanceString = GeoLocationUtilities.distanceFormattedInKM(
                            deviceLocation.latitude,
                            deviceLocation.longitude,
                            item.latitude,
                            item.longitude,
                        ),
                        lblSmallShortAttribute3String = "",
                        itemDescriptionString = item.description,
                        onListItemClick = {
                            if(LOG_ME) ALog.d(TAG, "onListItemClick(): ${item.name}")
                        },
                        getItemsRating = { null },
                        itemRef = null,
                    )
                }
            }
        )
    }
}