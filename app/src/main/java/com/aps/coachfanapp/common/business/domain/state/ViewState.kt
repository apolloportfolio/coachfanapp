package com.aps.coachfanapp.common.business.domain.state

import android.os.Parcelable
import com.aps.coachfanapp.common.framework.presentation.PermissionHandlingData
import com.aps.coachfanapp.common.framework.presentation.views.DialogState
import com.aps.coachfanapp.common.framework.presentation.views.ProgressIndicatorState
import com.aps.coachfanapp.common.framework.presentation.views.SnackBarState
import com.aps.coachfanapp.common.framework.presentation.views.ToastState

abstract class ViewState<T>(
    var mainEntity: T? = null,
    var listOfEntities: List<T>? = ArrayList<T>(),
    var isUpdatePending: Boolean? = null,
    var isInternetAvailable: Boolean? = null,
    var isNetworkRepositoryAvailable: Boolean = true,
    var noCachedEntity: Boolean? = null,

    var snackBarState: SnackBarState? = null,
    var toastState: ToastState? = null,
    var dialogState: DialogState? = null,
    var progressIndicatorState: ProgressIndicatorState? = null,

    var permissionHandlingData: PermissionHandlingData = PermissionHandlingData(),

    var stateEventTracker: StateEventTracker = StateEventTracker(),

    var isDrawerOpen: Boolean? = null,
): Parcelable {

    override fun toString(): String {
        val instance = this
        return buildString {
            appendLine("ViewState(${System.identityHashCode(instance)})(${instance.hashCode()}):")
            appendLine("mainEntity: $mainEntity")
            appendLine("listOfEntities: $listOfEntities")
            appendLine("isUpdatePending: $isUpdatePending")
            appendLine("isInternetAvailable: $isInternetAvailable")
            appendLine("isNetworkRepositoryAvailable: $isNetworkRepositoryAvailable")
            appendLine("noCachedEntity: $noCachedEntity")
            appendLine("snackBarState: $snackBarState")
            appendLine("toastState: $toastState")
            appendLine("dialogState: $dialogState")
            appendLine("progressIndicatorState: $progressIndicatorState")
            appendLine("permissionHandlingData: $permissionHandlingData")
            appendLine("stateEventTracker: $stateEventTracker")
            appendLine("isDrawerOpen: $isDrawerOpen")
        }
    }

    abstract fun copy(): ViewState<T>
}