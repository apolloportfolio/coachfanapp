package com.aps.coachfanapp.common.framework.presentation.views

import android.annotation.SuppressLint
import android.app.Activity
import android.content.pm.PackageManager
import android.location.Location
import android.widget.Toast
import androidx.activity.compose.rememberLauncherForActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.compose.animation.core.Spring
import androidx.compose.animation.core.spring
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.paint
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.res.dimensionResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.tooling.preview.PreviewParameter
import androidx.compose.ui.tooling.preview.PreviewParameterProvider
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.compose.ui.zIndex
import androidx.core.app.ActivityCompat.shouldShowRequestPermissionRationale
import androidx.core.content.ContextCompat
import com.aps.coachfanapp.R
import com.aps.coachfanapp.common.business.domain.state.StateEventTracker
import com.aps.coachfanapp.common.framework.presentation.PermissionHandlingData
import com.aps.coachfanapp.common.framework.presentation.TAG_PERMISSION_ASKING
import com.aps.coachfanapp.common.framework.presentation.layouts.CustomSnackBarSwipeableWithGradient
import com.aps.coachfanapp.common.framework.presentation.layouts.SwipeableSnackBarHost
import com.aps.coachfanapp.common.framework.presentation.openAppSettings
import com.aps.coachfanapp.common.framework.presentation.values.CommonTheme
import com.aps.coachfanapp.common.framework.presentation.views.*
import com.aps.coachfanapp.common.framework.presentation.views.examples.ExampleScreenCompFragmentBottomSheet
import com.aps.coachfanapp.common.framework.presentation.views.examples.ExampleScreenComposableFragmentContent
import com.aps.coachfanapp.common.util.ALog
import com.aps.coachfanapp.common.util.DeviceLocation
import com.aps.coachfanapp.common.util.PreviewEntityFactory
import com.aps.coachfanapp.core.framework.presentation.ProjectPermissionMediaProvider
import java.util.Locale

private const val TAG = "SwipeableFragmentWithBottomSheetAndFAB"
private const val LOG_ME = true
const val SwipeableFragmentWithBottomSheetAndFABTestTag = TAG

@SuppressLint("CoroutineCreationDuringComposition")
@OptIn(ExperimentalMaterialApi::class)
@Composable
fun SwipeableFragmentWithBottomSheetAndFAB(
    launchInitStateEvent: () -> Unit = {},

    activity: Activity? = null,
    permissionHandlingData: PermissionHandlingData = PermissionHandlingData(),
    permissionsRequiredInFragment: MutableSet<String> = mutableSetOf(),

    backgroundDrawableId: Int,
    onSwipeLeft: () -> Unit = {},
    onSwipeRight: () -> Unit = {},
    onSwipeUp: () -> Unit = {},
    onSwipeDown: () -> Unit = {},
    onSwipeRightFromComposableSide: () -> Unit = {},
    onSwipeLeftFromComposableSide: () -> Unit = {},
    onSwipeDownFromComposableSide: () -> Unit = {},
    onSwipeUpFromComposableSide: () -> Unit = {},

    leftSideThreshold: Float = 0.0f,
    rightSideThreshold: Float = 0.0f,
    topSideThreshold: Float = 0.0f,
    bottomSideThreshold: Float = 0.0f,

    floatingActionButtonDrawableId: Int?,
    floatingActionButtonOnClick: (() -> Unit)?,
    floatingActionButtonContentDescription: String?,
    snackBarState: SnackBarState?,
    toastState: ToastState?,
    dialogState: DialogState?,
    progressIndicatorState: ProgressIndicatorState?,
    sheetState: BottomSheetState,
    sheetContent: @Composable (ColumnScope.() -> Unit),
    content: @Composable () -> Unit,

    isPreview: Boolean = false,
) {
    if(LOG_ME) ALog.d(TAG, "(): Recomposition start.")

    val scaffoldState = rememberBottomSheetScaffoldState( bottomSheetState = sheetState )
    val floatingActionButton = StandardFloatingActionButton(
        floatingActionButtonDrawableId,
        floatingActionButtonOnClick,
        floatingActionButtonContentDescription
    )

    val snackBar: @Composable () -> Unit = {
        CustomSnackBarSwipeableWithGradient(
            iconDrawableResID = snackBarState?.iconDrawableRes,
            message = snackBarState?.message ?: "Error",
            actionMessage = snackBarState?.actionLabel,
            onActionClick = {
                if(LOG_ME) ALog.d(TAG, "(): SnackBar clicked.")
                snackBarState?.onClickAction
            },
            colors = listOf(
                MaterialTheme.colors.primary,
                MaterialTheme.colors.primaryVariant
            ),
            textColor = MaterialTheme.colors.secondaryVariant,
        )
    }

    val customSnackBarHost: @Composable (SnackbarHostState) -> Unit = movableContentOf { snackbarHostState ->
        SwipeableSnackBarHost(
            hostState = snackbarHostState,
            snackBar = snackBar,
        )
    }

    BottomSheetScaffold(
        sheetContent = sheetContent,
        modifier = Modifier
            .paint(painterResource(id = backgroundDrawableId))
            .fillMaxWidth()
            .fillMaxHeight()
            .height(dimensionResource(id = R.dimen.search_entities1_bottom_sheet_unveiled_height))
            .wrapContentHeight(Alignment.CenterVertically)
            .testTag(SwipeableFragmentWithBottomSheetAndFABTestTag),
        sheetPeekHeight = dimensionResource(id = R.dimen.bottom_sheet_peek_height),
        scaffoldState = scaffoldState,
        floatingActionButton = floatingActionButton,
        snackbarHost = customSnackBarHost,
    ) {
        Box {
            var permissionsAsked by remember { mutableStateOf(false) }
            val askForPermissions: () -> Unit = remember {
                {
                    if(LOG_ME)ALog.d(TAG_PERMISSION_ASKING+TAG, "(): " +
                            "Adding following permissions to viewModel: " +
                            permissionsRequiredInFragment.joinToString()
                    )
                    permissionHandlingData.addRequiredPermissionsInViewModel(
                        activity,
                        permissionsRequiredInFragment
                    )
                    permissionsAsked = true
                }
            }

            if(LOG_ME)ALog.d(TAG_PERMISSION_ASKING+TAG, "(): " +
                    "permissionsAsked == $permissionsAsked")
            LaunchEffectsInComposableLifecycle(
                onFirstComposition = {
                    if(LOG_ME)ALog.d(TAG_PERMISSION_ASKING+TAG, "(): " +
                            "Asking for permissions on first composition.")
                    askForPermissions()
                },
                onBroughtBackIntoView = {
                    if(LOG_ME)ALog.d(TAG_PERMISSION_ASKING+TAG, "(): " +
                            "Asking for permissions on being brought back into view.")
                    askForPermissions()
                },
                onDisposeComposable = {
                    if(LOG_ME)ALog.d(TAG_PERMISSION_ASKING+TAG, "(): " +
                            "Setting permissionsAsked to false")
                    permissionsAsked = false
                }
            )

            if(permissionsAsked) {
                if(LOG_ME)ALog.d(TAG_PERMISSION_ASKING+TAG, "(): " +
                        "Permissions were asked. Showing permission dialogs if necessary.")
                ShowPermissionDialogsIfNecessary(
                    activity = activity,
                    permissionHandlingData = permissionHandlingData,
                    modifier = Modifier.zIndex(1f).align(Alignment.Center),
                )
            } else {
                if(LOG_ME)ALog.d(TAG_PERMISSION_ASKING+TAG, "(): " +
                        "Permissions weren't asked yet.")
            }

            SwipeableBox(
                onDragLeft = onSwipeLeft,
                onDragRight = onSwipeRight,
                onDragUp = onSwipeUp,
                onDragDown = onSwipeDown,
                onDragLeftFromComposableRightSide = onSwipeLeftFromComposableSide,
                onDragRightFromComposableLeftSide = onSwipeRightFromComposableSide,
                onDragUpFromComposableBottomSide = onSwipeUpFromComposableSide,
                onDragDownFromComposableTopSide = onSwipeDownFromComposableSide,
                leftSideThreshold = leftSideThreshold,
                rightSideThreshold = rightSideThreshold,
                topSideThreshold = topSideThreshold,
                bottomSideThreshold = bottomSideThreshold,
            ) {
                if(activity == null) {
                    if(LOG_ME)ALog.e(TAG, "(): " +
                            "activity == null, It should not be like that unless it's a preview.")
                    ShowContent(
                        content,
                        launchInitStateEvent,
                    )
                } else {
                    if(permissionHandlingData.allNecessaryPermissionsAreGranted(activity)) {
                        ShowContent(
                            content,
                            launchInitStateEvent,
                        )
                    } else {
                        if(LOG_ME)ALog.d(TAG_PERMISSION_ASKING+TAG, "(): " +
                                "Blocking content due to lack of permissions.")
                        ContentUnavailableDueToLackOfPermissions(
                            activity,
                            permissionHandlingData,
                        )
                    }
                }
            }

            ShowSnackBarIfNecessary(
                snackBarState,
                scaffoldState,
            )

            ShowToastIfNecessary(
                activity = activity,
                toastState = toastState,
            )

            ShowDialogIfNecessary(
                dialogState = dialogState,
                modifier = Modifier
                    .align(Alignment.Center)
                    .zIndex(1f),
            )

            ShowProgressIndicatorIfNecessary(
                progressIndicatorState = progressIndicatorState,
                modifier = Modifier
                    .align(Alignment.Center)
                    .zIndex(0.9f)
            )
        }
    }
    if (LOG_ME) ALog.d(TAG, "(): Recomposition end.")
}

@Composable
internal fun ShowContent(
    content: @Composable () -> Unit,
    launchInitStateEvent: () -> Unit,
) {
    content()

    LaunchEffectsInComposableLifecycle(
        onFirstComposition = {
            launchInitStateEvent()
        },
        onBroughtBackIntoView = {
            launchInitStateEvent()
        },
    )
}

@Composable
internal fun ContentUnavailableDueToLackOfPermissions(
    activity: Activity?,
    permissionHandlingData: PermissionHandlingData,
) {
    var notGrantedPermissions = mutableSetOf<String>()
    if(activity == null) {
        notGrantedPermissions = permissionHandlingData.requiredPermissions
    } else {
        for(permission in permissionHandlingData.requiredPermissions) {
            if(ContextCompat.checkSelfPermission(
                    activity,
                    permission,
                ) != PackageManager.PERMISSION_GRANTED) {
                notGrantedPermissions.add(permission)
            }
        }
    }

    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(
                start = 16.dp,
                end = 16.dp
            ),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally,
    ) {
        Text(
            text = "Content Unavailable",
            color = MaterialTheme.colors.error,
            fontSize = 20.sp,
            fontWeight = FontWeight.Bold,
            modifier = Modifier.padding(bottom = 16.dp)
        )

        Text(
            text = "The following permissions are required to access this content:",
            fontSize = 16.sp,
            modifier = Modifier.padding(bottom = 16.dp)
        )

        notGrantedPermissions.forEach { permission ->
            Text(
                text = "- ${convertPermissionToUserFriendlyString(permission)} -",
                fontSize = 14.sp,
                style = MaterialTheme.typography.body1.copy(fontWeight = FontWeight.Bold),
                modifier = Modifier
                    .padding(bottom = 8.dp)
                    .clickable {
                        activity?.openAppSettings(null)
                    }
            )
        }

        Spacer(modifier = Modifier.height(16.dp))

        Text(
            text = "Please grant the necessary permissions in your device settings.",
            fontSize = 16.sp
        )
    }
}

internal fun convertPermissionToUserFriendlyString(permission: String): String {
    val trimmedPermissionName = deleteCharactersUntilLastDot(permission)
    val words = trimmedPermissionName.split("_").map { perm ->
        perm.lowercase(Locale.ROOT)
            .replaceFirstChar { if (it.isLowerCase()) it.titlecase(Locale.ROOT) else it.toString() } }
    return words.joinToString(" ")
}

internal fun deleteCharactersUntilLastDot(input: String): String {
    val dotIndex = input.lastIndexOf('.')
    return if (dotIndex != -1) {
        input.substring(dotIndex + 1)
    } else {
        input
    }
}




// References:
// https://www.youtube.com/watch?v=D3JCtaK8LSU
// https://github.com/philipplackner/PermissionsGuideCompose
@Composable
internal fun ShowPermissionDialogsIfNecessary(
    activity: Activity?,
    permissionHandlingData: PermissionHandlingData,
    modifier: Modifier,
) {
    if(activity == null) {
        if(LOG_ME)ALog.w(TAG_PERMISSION_ASKING+TAG, "ShowPermissionDialogsIfNecessary(): " +
                "activity == null")
        return
    }

    if(LOG_ME)ALog.d(TAG_PERMISSION_ASKING+TAG, "ShowPermissionDialogsIfNecessary(): " +
            "requiredPermissions == ${permissionHandlingData.requiredPermissions.joinToString()}")

    var multiplePermissionsResultLauncherLaunched by remember {
        mutableStateOf(false)
    }

    val multiplePermissionsResultLauncher = rememberLauncherForActivityResult(
        contract = ActivityResultContracts.RequestMultiplePermissions(),
        onResult = { perms ->
            val methodName = "ShowPermissionDialogsIfNecessary().onResult()"
            if (LOG_ME) ALog.d(TAG_PERMISSION_ASKING+TAG,
                "Method start: $methodName")
            try {
                val deniedPermissions = mutableSetOf<String>()
                val grantedPermissions = mutableSetOf<String>()
                for(permission in perms.keys) {
                    if(perms[permission] == true) {
                        if(LOG_ME)ALog.d(TAG_PERMISSION_ASKING+TAG, ".$methodName(): " +
                                "Adding granted permission: $permission")
                        grantedPermissions.add(permission)
                    } else if(perms[permission] == null) {
                        val permissionIsGranted = ContextCompat.checkSelfPermission(
                            activity,
                            permission,
                        ) == PackageManager.PERMISSION_GRANTED
                        if(LOG_ME)ALog.d(TAG, ".$methodName(): " +
                                "perms[$permission] == null but " +
                                "permissionIsGranted == $permissionIsGranted")
                        if(permissionIsGranted) {
                            grantedPermissions.add(permission)
                        } else {
                            deniedPermissions.add(permission)
                        }
                    } else {
                        deniedPermissions.add(permission)
                        if(LOG_ME)ALog.d(TAG_PERMISSION_ASKING+TAG, ".$methodName(): " +
                                "Adding denied permission: $permission")
                    }
                }
                permissionHandlingData.addGrantedAndDeniedPermissionsInViewModel(
                    grantedPermissions,
                    deniedPermissions,
                )
                multiplePermissionsResultLauncherLaunched = true
            } catch (e: Exception) {
                ALog.e(TAG_PERMISSION_ASKING+TAG, methodName, e)
            } finally {
                if (LOG_ME) ALog.d(TAG_PERMISSION_ASKING+TAG,
                    "Method end: $methodName")
            }
        }
    )
    val launchMultiplePermissionsResultLauncher: Boolean = permissionHandlingData.permissionsToRequestWithDialogs.isNotEmpty()

    if(multiplePermissionsResultLauncherLaunched) {
        for(permission in permissionHandlingData.permissionsToRequestWithDialogs) {
            if(LOG_ME)ALog.d(TAG_PERMISSION_ASKING+TAG, ": " +
                    "Showing permission dialog for $permission")
            PermissionDialog(
                permissionMediaProvider =
                ProjectPermissionMediaProvider.choosePermissionMediaProvider(permission),
                isPermanentlyDeclined = !shouldShowRequestPermissionRationale(
                    activity,
                    permission,
                ),
                onDismiss = { permissionHandlingData.dismissPermissionDialog(permission) },
                onOkClick = {
                    permissionHandlingData.dismissPermissionDialog(permission)
                    multiplePermissionsResultLauncher.launch(
                        arrayOf(permission)
                    )
                },
                onGoToAppSettingsClick = {
                    activity.openAppSettings(null)
                },
                modifier = modifier,
            )
        }
    }


    LaunchedEffect(key1 = launchMultiplePermissionsResultLauncher) {
        if(LOG_ME)ALog.d(TAG_PERMISSION_ASKING+TAG, ": " +
                "Launching multiplePermissionsResultLauncher")
        if(!multiplePermissionsResultLauncherLaunched){
            multiplePermissionsResultLauncher.launch(
                permissionHandlingData.requiredPermissions.toTypedArray()
            )
        }
    }
}


@OptIn(ExperimentalMaterialApi::class)
@Composable
internal fun ShowSnackBarIfNecessary(
    snackBarState: SnackBarState?,
    scaffoldState: BottomSheetScaffoldState,
) {
    var firstComposition by rememberSaveable { mutableStateOf(true) }
//    if(LOG_ME) ALog.d(TAG, "ShowSnackBarIfNecessary():" +
//            "\n snackBarState == $snackBarState")

    LaunchedEffect(snackBarState) {
        if(firstComposition) {
//            if(LOG_ME)ALog.d(TAG, "ShowSnackBarIfNecessary(): This is the first composition.")
            firstComposition = false
        } else {
            if(snackBarState?.show == true) {
//                if(LOG_ME) ALog.d(TAG, "ShowSnackBarIfNecessary(): " +
//                        "LaunchedEffect")
                val result = scaffoldState.snackbarHostState.showSnackbar(
                    message = "LaunchedEffect1",
                    duration = snackBarState.duration,
                )
                if(LOG_ME) ALog.d(TAG, "ShowSnackBarIfNecessary(): " +
                        "result == $result")

                when (result) {
                    SnackbarResult.Dismissed -> {
//                        if(LOG_ME) ALog.d(TAG, "ShowSnackBarIfNecessary(): " +
//                                "SnackbarResult.Dismissed")
                        snackBarState.onDismissAction()
                        snackBarState.show = false
                        snackBarState.updateSnackBarInViewModel.invoke(
                            snackBarState
                        )
                    }
                    SnackbarResult.ActionPerformed -> {
//                        if(LOG_ME) ALog.d(TAG, "ShowSnackBarIfNecessary(): " +
//                                "SnackbarResult.ActionPerformed")
                        snackBarState.onClickAction()
                        snackBarState.show = false
                        snackBarState.updateSnackBarInViewModel.invoke(
                            snackBarState
                        )
                    }
                }
            } else {
                scaffoldState.snackbarHostState.currentSnackbarData?.dismiss()
                snackBarState?.updateSnackBarInViewModel?.invoke(
                    snackBarState
                )
            }
        }
    }
}

@Composable
internal fun ShowToastIfNecessary(
    activity: Activity?,
    toastState: ToastState?,
) {
//    if(LOG_ME)ALog.d(TAG, "ShowToastIfNecessary(): Recomposition start")
    var firstComposition by rememberSaveable { mutableStateOf(true) }
    var resetToast by remember { mutableStateOf(false) }
    var toastDisplayed by remember { mutableStateOf(false) }


    toastState?.let {
        if(it.show && !toastDisplayed) {
//            if(LOG_ME)ALog.d(TAG, "ShowToastIfNecessary(): " +
//                    "Showing toast. toastState == $toastState")
            toastDisplayed = true
            if(activity != null) {
                Toast.makeText(
                    activity,
                    it.message,
                    it.duration,
                ).show()
            }
//            if(LOG_ME)ALog.d(TAG, "ShowToastIfNecessary(): Before show change: " +
//                    "viewState?.toastState?.show == ${viewState?.toastState}")
            it.show = false
            it.message = ""
//            if(LOG_ME)ALog.d(TAG, "ShowToastIfNecessary(): After toastState change: " +
//                    "viewState?.toastState?.show == ${viewState?.toastState}")
            resetToast = true
        }
    }

    LaunchedEffect(resetToast) {
//        if(LOG_ME) ALog.d(TAG, "ShowToastIfNecessary().LaunchedEffect(resetToast): " +
//                "toastState == $toastState")
        if(firstComposition) {
//            if(LOG_ME)ALog.d(TAG, "ShowToastIfNecessary().LaunchedEffect(resetToast): " +
//                    "This is the first composition.")
            firstComposition = false
        } else {
            if(resetToast) {
//                if(LOG_ME)ALog.d(TAG, "ShowToastIfNecessary().LaunchedEffect(resetToast): " +
//                        "Resetting toast in viewModel.")
                toastState?.updateToastInViewModel?.invoke(
                    ToastState(
                        show = false,
                        message = "",
                        updateToastInViewModel = toastState.updateToastInViewModel
                    )
                )
//                if(LOG_ME)ALog.d(TAG, "ShowToastIfNecessary().LaunchedEffect(resetToast): " +
//                        "Toast in viewModel was reset.")
            }
        }
        resetToast = false
        toastDisplayed = false
    }


//    if(LOG_ME)ALog.d(TAG, "ShowToastIfNecessary(): Recomposition end")
}

@Composable
fun ShowDialogIfNecessary(
    dialogState: DialogState?,
    modifier: Modifier,
) {
//    if(LOG_ME)ALog.d(TAG, "ShowDialogIfNecessary(): Recomposition start")
    var firstComposition by rememberSaveable { mutableStateOf(true) }
    var resetDialog by remember { mutableStateOf(false) }

//    if(LOG_ME)ALog.d(TAG, "ShowDialogIfNecessary():\n" +
//            "resetDialog == $resetDialog\n" +
//            "dialogState == $dialogState"
//    )

    var showDialog by remember { mutableStateOf(false) }
    showDialog = dialogState?.show == true
    if (showDialog) {
        Box(modifier = modifier) {
            CustomDialog(
                dialogState = dialogState,
                onDialogDismiss = {
                    resetDialog = true
                }
            )
        }
    }

    LaunchedEffect(resetDialog) {
//        if(LOG_ME)ALog.d(TAG, "ShowDialogIfNecessary().LaunchedEffect(resetDialog): Recomposition start")
        if(firstComposition) {
//            if(LOG_ME)ALog.d(TAG, "ShowDialogIfNecessary().LaunchedEffect(resetDialog): " +
//                    "This is the first composition.")
            firstComposition = false
            resetDialog = false
        } else {
            if (resetDialog) {
                dialogState?.updateDialogInViewModel?.invoke(
                    DialogState()
                )
            }
            resetDialog = false
        }
//        if(LOG_ME)ALog.d(TAG, "ShowDialogIfNecessary().LaunchedEffect(resetDialog): Recomposition end")
    }

//    if(LOG_ME)ALog.d(TAG, "ShowDialogIfNecessary(): Recomposition end")
}


// Preview =========================================================================================
@OptIn(ExperimentalMaterialApi::class)
@Preview(
    showSystemUi = true,
)
@Composable
fun SwipeableFragmentWithBottomSheetAndFABPreview(
    @PreviewParameter(SwipeableFragmentWithBottomSheetAndFABParamsProvider::class) params: SwipeableFragmentWithBottomSheetAndFABParams
) {
    val scope = rememberCoroutineScope()
    val sheetState = rememberBottomSheetState(
        initialValue = BottomSheetValue.Collapsed,
//        initialValue = BottomSheetValue.Expanded,
        animationSpec = spring(dampingRatio = Spring.DampingRatioHighBouncy)
    )
    val sheetContent = ExampleScreenCompFragmentBottomSheet(
        scope = scope,
        sheetState = sheetState,
        leftButtonOnClick = {},
        rightButtonOnClick = {},
    )
//    val sheetContent: @Composable ColumnScope.() -> Unit = {}


    CommonTheme {
        // Start Interactive Mode to actually see SnackBar
        val snackBarState = remember{
            SnackBarState(
                show = true,
                message = "Snackbar",
                actionLabel = "action",
                iconDrawableRes = R.drawable.ic_launcher,
                onClickAction = {},
                onDismissAction = {},
                duration = SnackbarDuration.Indefinite,
                updateSnackBarInViewModel = {},
            )
        }
        snackBarState.onDismissAction = { snackBarState.show = false }
        snackBarState.onClickAction = { snackBarState.show = false }

        val toastState = ToastState(
            show = false,
            message = "This is a Toast!",
            duration = Toast.LENGTH_LONG,
            updateToastInViewModel = {},
        )

        val dialogState = DialogState(
            show = false,
            onDismissRequest = {},
            title = "Dialog's Title",
            text = "Dialog's text.",
            leftButtonText = "Dismiss",
            leftButtonOnClick = {},
            rightButtonText = "Confirm",
            rightButtonOnClick = {},
            updateDialogInViewModel = {},
        )

        val progressIndicatorState = ProgressIndicatorState(
            show = true,
            progress = 0.7f,
            type = ProgressIndicatorType.DeterminateCircularProgressIndicator,
            updateProgressIndicatorStateInViewModel = {},
        )

        SwipeableFragmentWithBottomSheetAndFAB(
            backgroundDrawableId = params.backgroundDrawableId,
            onSwipeLeft = params.onSwipeLeft,
            onSwipeRight = params.onSwipeRight,
            onSwipeUp = params.onSwipeUp,
            onSwipeDown = params.onSwipeDown,
            floatingActionButtonDrawableId = params.floatingActionButtonDrawableId,
            floatingActionButtonOnClick = params.floatingActionButtonOnClick,
            floatingActionButtonContentDescription = params.floatingActionButtonContentDescription,
            snackBarState = snackBarState,
            toastState = toastState,
            dialogState = dialogState,
            progressIndicatorState = progressIndicatorState,
            sheetState = sheetState,
            sheetContent = sheetContent,
            content = {
                ExampleScreenComposableFragmentContent(
                    stateEventTracker = StateEventTracker(),
                    deviceLocation = DeviceLocation(
                        locationPermissionGranted = true,
                        location = Location("Lublin").apply {
                            latitude = 51.2465
                            longitude = 22.5684
                        }
                    ),
                    showProfileStatusBar = true,
                    finishVerification = {},
                    initialSearchQuery = "",
                    entities = PreviewEntityFactory.createPreviewEntitiesList(),
                    onSearchQueryUpdate = {},
                    onListItemClick = {},
                    launchInitStateEvent = {},
                    isPreview = true,
                )
            },
            isPreview = true,
        )
    }
}

class SwipeableFragmentWithBottomSheetAndFABParamsProvider:
    PreviewParameterProvider<SwipeableFragmentWithBottomSheetAndFABParams> {

    @OptIn(ExperimentalMaterialApi::class)
    override val values: Sequence<SwipeableFragmentWithBottomSheetAndFABParams> = sequenceOf(
        SwipeableFragmentWithBottomSheetAndFABParams(
            backgroundDrawableId = R.drawable.default_background,
            onSwipeLeft = { },
            onSwipeRight = { },
            onSwipeUp = { },
            onSwipeDown = { },
            floatingActionButtonDrawableId = R.drawable.ic_search_yellow_24,
            floatingActionButtonOnClick = { },
            floatingActionButtonContentDescription = "FAB",
            sheetState = null,
            content = null,
        )
    )


    override val count: Int = values.count()
}

class SwipeableFragmentWithBottomSheetAndFABParams @OptIn(ExperimentalMaterialApi::class) constructor(
    val backgroundDrawableId: Int,
    val onSwipeLeft: () -> Unit = {},
    val onSwipeRight: () -> Unit = {},
    val onSwipeUp: () -> Unit = {},
    val onSwipeDown: () -> Unit = {},
    val floatingActionButtonDrawableId: Int?,
    val floatingActionButtonOnClick: (() -> Unit)?,
    val floatingActionButtonContentDescription: String?,
    val sheetState: BottomSheetState?,
    val content: (@Composable () -> Unit)?,
)