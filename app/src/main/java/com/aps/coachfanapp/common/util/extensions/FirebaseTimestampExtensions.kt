package com.aps.coachfanapp.common.util.extensions

import com.google.firebase.Timestamp
import java.text.SimpleDateFormat
import java.time.Instant
import java.time.ZoneId
import java.time.format.DateTimeFormatter
import java.util.*

fun Timestamp?.toLogString() : String {
    return this?.let{
        val sdf = SimpleDateFormat("dd.MM.yyyy hh:mm:ss a z Z", Locale.getDefault())
        sdf.timeZone = TimeZone.getTimeZone("UTC+1")
        "${sdf.format(it.toDate())}    ${it.seconds}.${it.nanoseconds}"

//        val gregorianCalendar = GregorianCalendar()
//        gregorianCalendar.timeInMillis = 1000*it.seconds
//        "${sdf.format(gregorianCalendar)}    ${it.seconds}.${it.nanoseconds}"

//        val instant : Instant = Instant.ofEpochSecond( it.seconds, it.nanoseconds.toLong())
//        val formatter: DateTimeFormatter = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.SHORT)
//            .withLocale(Locale.getDefault())
//            .withZone(ZoneId.systemDefault())
//        "${formatter.format(instant)}    ${it.seconds}.${it.nanoseconds}"

//        val localDateTime : LocalDateTime = LocalDateTime.ofEpochSecond(it.seconds, it.nanoseconds, ZoneOffset.UTC)
//        val localDateTimeFormatter = DateTimeFormatter.ofPattern("dd.MM.yyyy hh:mm:ss a")
//        val localDateTimeMillis = localDateTime.atZone(ZoneId.systemDefault()).toInstant().toEpochMilli()
//        "${localDateTimeFormatter.format(localDateTime)}    ${localDateTimeMillis}    ${it.seconds}.${it.nanoseconds}"

        val instant : Instant = Instant.ofEpochSecond( it.seconds, it.nanoseconds.toLong())
        val localDateTimeFormatter = DateTimeFormatter.ofPattern("dd.MM.yyyy hh:mm:ss a")
            .withLocale(Locale.getDefault())
            .withZone(ZoneId.systemDefault())
        "${localDateTimeFormatter.format(instant)}    ${instant.toEpochMilli()}    ${it.seconds}.${it.nanoseconds}"
    } ?: "null"
}