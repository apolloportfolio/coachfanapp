package com.aps.coachfanapp.common

import android.app.Application
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview

private const val TAG = "BaseApplication"
private const val LOG_ME = true

@FlowPreview
@ExperimentalCoroutinesApi
open class BaseApplication : Application(){

    override fun onCreate() {
        super.onCreate()
    }
}