package com.aps.coachfanapp.common.util.composableutils

import android.content.res.Resources
import android.util.TypedValue
import androidx.annotation.DrawableRes
import androidx.compose.runtime.Composable
import androidx.compose.runtime.ReadOnlyComposable
import androidx.compose.runtime.key
import androidx.compose.runtime.remember
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.ImageBitmap
import androidx.compose.ui.graphics.painter.BitmapPainter
import androidx.compose.ui.graphics.painter.ColorPainter
import androidx.compose.ui.graphics.painter.Painter
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.imageResource
import androidx.compose.ui.res.painterResource
import com.aps.coachfanapp.common.util.ALog

private const val TAG = "fallbackPainterResource"

/**
 * Workaround for issue: https://issuetracker.google.com/issues/230166331
 * Copied from `androidx.compose.ui.res.PainterResources.android.kt`.
 *
 * Only rasterized image handling is altered, VectorDrawable requests are
 * passed through to the original Compose code. See comments inside for modifications.
 */
@Composable
fun fallbackPainterResource(@DrawableRes id: Int): Painter {
    val context = LocalContext.current
    val res = resources()
    val value = remember { TypedValue() }
    res.getValue(id, value, true)
    val path = value.string
    // Assume .xml suffix implies loading a VectorDrawable resource
    return if (path?.endsWith(".xml") == true) {
        /**
         * ***** MODIFIED *****
         * Original code:
         *   val imageVector = loadVectorResource(context.theme, res, id, value.changingConfigurations)
         *   rememberVectorPainter(imageVector)
         *
         * Modified code: calls the original painterResource for handling VectorDrawables.
         * ***** MODIFIED *****
         */
        key(id) { painterResource(id) }     // Reference: https://stackoverflow.com/questions/70171212/vector-drawable-display-issues-for-jetpack-compose
//        painterResource(id = id)
    } else {
        // Otherwise load the bitmap resource
        /**
         * ***** MODIFIED *****
         * Original code: throws throwable.
         * Modified code: falls back to showing a gray color when loading image fails.
         * ***** MODIFIED *****
         */
        val imageBitmap = remember(path, id, context.theme) {
            loadImageBitmapResource(res, id)
        }
        if (imageBitmap != null) {
            BitmapPainter(imageBitmap)
        } else {
            ColorPainter(Color.LightGray)
        }
    }
}

/**
 * Copied from `androidx.compose.ui.res.PainterResources.android.kt`.
 *
 * ***** MODIFIED *****
 * Original code: catches exception, but throws other instead, hiding original cause.
 * Modified code: logs original throwable, and returns null.
 * ***** MODIFIED *****
 */
private fun loadImageBitmapResource(res: Resources, id: Int): ImageBitmap? {
    return try {
        ImageBitmap.imageResource(res, id)
    } catch (throwable: Throwable) {
        ALog.e(
            TAG, ".loadImageBitmapResource(): " +
                                "Failed to load resource $id\nthrowable = $throwable")
        return null
    }
}

/**
 * Copied from `androidx.compose.ui.res.Resources.android.kt`.
 */
@Composable
@ReadOnlyComposable
private fun resources(): Resources {
    LocalConfiguration.current
    return LocalContext.current.resources
}