package com.aps.coachfanapp.common.framework.presentation.views

import android.os.Parcelable
import android.widget.Toast
import kotlinx.android.parcel.Parcelize
import java.io.Serializable

@Parcelize
data class ToastState(
    var show: Boolean = false,
    var message: String = "",
    var duration: Int = Toast.LENGTH_LONG,
    var updateToastInViewModel: (ToastState?) -> Unit,
) : Parcelable, Serializable {

    fun shallowCopy(): ToastState {
        return ToastState(
            this.show,
            this.message,
            this.duration,
            this.updateToastInViewModel,
        )
    }
    override fun toString(): String {
        return buildString {
            appendLine("ToastState: message: $message show: $show")
//            appendLine("duration: $duration")
//            appendLine("updateToastInViewModel: Function<ToastState?>")
        }
    }
}