package com.aps.coachfanapp.common.business.data.network

import com.aps.coachfanapp.R
import com.aps.coachfanapp.core.util.ApplicationContextProvider
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import javax.inject.Inject

class NetworkErrors
@Inject constructor(
    applicationContextProvider: ApplicationContextProvider
) {

    fun isNetworkError(msg: String): Boolean {
        return when {
            msg.contains(UNABLE_TO_RESOLVE_HOST) -> true
            else -> false
        }
    }

    @OptIn(FlowPreview::class, ExperimentalCoroutinesApi::class)
    var UNABLE_TO_RESOLVE_HOST = applicationContextProvider
        .applicationContext("NetworkErrors")
        .getString(R.string.UNABLE_TO_RESOLVE_HOST)

    @OptIn(FlowPreview::class, ExperimentalCoroutinesApi::class)
    var UNABLE_TODO_OPERATION_WO_INTERNET = applicationContextProvider
        .applicationContext("NetworkErrors")
        .getString(R.string.UNABLE_TODO_OPERATION_WO_INTERNET)

    @OptIn(FlowPreview::class, ExperimentalCoroutinesApi::class)
    var ERROR_CHECK_NETWORK_CONNECTION = applicationContextProvider
        .applicationContext("NetworkErrors")
        .getString(R.string.ERROR_CHECK_NETWORK_CONNECTION)

    @OptIn(FlowPreview::class, ExperimentalCoroutinesApi::class)
    var NETWORK_ERROR_UNKNOWN = applicationContextProvider
        .applicationContext("NetworkErrors")
        .getString(R.string.NETWORK_ERROR_UNKNOWN)

    @OptIn(FlowPreview::class, ExperimentalCoroutinesApi::class)
    var NETWORK_ERROR = applicationContextProvider
        .applicationContext("NetworkErrors")
        .getString(R.string.NETWORK_ERROR)

    @OptIn(FlowPreview::class, ExperimentalCoroutinesApi::class)
    var NETWORK_ERROR_TIMEOUT = applicationContextProvider
        .applicationContext("NetworkErrors")
        .getString(R.string.NETWORK_ERROR_TIMEOUT)

    @OptIn(FlowPreview::class, ExperimentalCoroutinesApi::class)
    var NETWORK_DATA_NULL = applicationContextProvider
        .applicationContext("NetworkErrors")
        .getString(R.string.NETWORK_DATA_NULL)
}