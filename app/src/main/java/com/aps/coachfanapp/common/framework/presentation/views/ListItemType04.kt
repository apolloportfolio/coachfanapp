package com.aps.coachfanapp.common.framework.presentation.views

import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Card
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.res.*
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.tooling.preview.PreviewParameter
import androidx.compose.ui.tooling.preview.PreviewParameterProvider
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import androidx.constraintlayout.compose.ConstraintLayout
import com.aps.coachfanapp.common.framework.presentation.values.CommonTheme
import com.aps.coachfanapp.common.util.ALog
import com.aps.coachfanapp.R

private const val TAG = "ListItemType04"
private const val LOG_ME = true

@Composable
fun ListItemType04(
    index: Int,
    itemTitleString: String?,
    itemDetail1: String?,
    itemDetail2: String?,
    onListItemClick: () -> Unit,
    tint: Color = MaterialTheme.colors.primaryVariant,
    borderWidth: Dp = 1.dp,
    cornerRadius: Dp = 4.dp,
    composableBackground: DrawnBackground? = null,
    backgroundDrawableId: Int = R.drawable.list_item_example_background_1,
) {
    if(LOG_ME)ALog.d(TAG, "ListItemType01(): " +
            "index == $index, itemTitleString == $itemTitleString\n")
    val transparentColor = remember {
        Color(0x00000000)
    }
    val startMargin = remember { 4 }.dp
    val endMargin = remember { 4 }.dp
    val topMargin = dimensionResource(id = R.dimen.list_item_vertical_margin)
    val bottomMargin = dimensionResource(id = R.dimen.list_item_vertical_margin)
    Card(
        modifier = Modifier
            .testTag(index.toString())
            .fillMaxWidth()
            .padding(
                start = dimensionResource(id = R.dimen.list_tile_horizontal_margin),
                end = dimensionResource(id = R.dimen.list_tile_horizontal_margin),
                top = dimensionResource(id = R.dimen.list_tile_vertical_margin),
                bottom = dimensionResource(id = R.dimen.list_tile_vertical_margin)
            )
            .background(transparentColor)
            .clickable { onListItemClick() },
    ) {
        BoxWithBackground(
            backgroundDrawableId = backgroundDrawableId,
            composableBackground = composableBackground,
        ) {

            ConstraintLayout(
                modifier = Modifier
                    .fillMaxWidth()
                    .wrapContentHeight()
                    .padding(
                        start = startMargin,
                        end = endMargin,
                        top = topMargin,
                        bottom = bottomMargin,
                    )
            ) {

                val (itemTitle, lblItemDetail1, lblItemDetail2,
                ) = createRefs()

                // Item's title
                Text(
                    modifier = Modifier
                        .constrainAs(itemTitle) {
                            top.linkTo(parent.top)
                            bottom.linkTo(lblItemDetail1.top)
                            start.linkTo(parent.start)
                            end.linkTo(parent.end)
                        }
                        .fillMaxWidth()
                        .padding(
                            start = startMargin,
                            end = endMargin,
                            top = topMargin,
                            bottom = bottomMargin,
                        ),
                    text = itemTitleString ?: "",
                    maxLines = 1,
                    textAlign = TextAlign.Start,
                    style = TextStyle(
                        color = MaterialTheme.colors.onSurface,
                        fontWeight = FontWeight.ExtraBold,
                    ),
                    overflow = TextOverflow.Ellipsis,
                )

                // Item's detail 1
                Text(
                    modifier = Modifier
                        .fillMaxWidth()
                        .constrainAs(lblItemDetail1) {
                            start.linkTo(parent.start)
                            end.linkTo(parent.end)
                            top.linkTo(itemTitle.bottom)
                            bottom.linkTo(lblItemDetail2.top)
                        }
                        .padding(
                            start = startMargin,
                            end = endMargin,
                            top = topMargin,
                            bottom = bottomMargin
                        )
                        .background(
                            color = transparentColor,
                            shape = RoundedCornerShape(cornerRadius)
                        )
                        .border(
                            width = borderWidth,
                            color = tint,
                            shape = RoundedCornerShape(cornerRadius)
                        )
                        .padding(
                            start = startMargin,
                            end = endMargin,
                            top = topMargin,
                            bottom = bottomMargin
                        ),
                    text = itemDetail1 ?: "",
                    textAlign = TextAlign.Start,
                    style = TextStyle(
                        color = MaterialTheme.colors.onSurface,
                        fontWeight = FontWeight.Normal,
                    ),
                    maxLines = 2,
                    overflow = TextOverflow.Ellipsis,
                )

                // Item's description 2
                Text(
                    modifier = Modifier
                        .fillMaxWidth()
                        .constrainAs(lblItemDetail2) {
                            start.linkTo(parent.start)
                            end.linkTo(parent.end)
                            top.linkTo(lblItemDetail1.bottom)
                            bottom.linkTo(parent.bottom)
                        }
                        .padding(
                            start = startMargin,
                            end = endMargin,
                            top = topMargin,
                            bottom = bottomMargin
                        )
                        .background(
                            color = transparentColor,
                            shape = RoundedCornerShape(cornerRadius)
                        )
                        .border(
                            width = borderWidth,
                            color = tint,
                            shape = RoundedCornerShape(cornerRadius)
                        )
                        .padding(
                            start = startMargin,
                            end = endMargin,
                            top = topMargin,
                            bottom = bottomMargin
                        ),
                    text = itemDetail2 ?: "",
                    textAlign = TextAlign.Start,
                    style = TextStyle(
                        color = MaterialTheme.colors.onSurface,
                        fontWeight = FontWeight.Normal,
                    ),
                    maxLines = 2,
                    overflow = TextOverflow.Ellipsis,
                )

            }
        }
    }
}


// Preview =========================================================================================
data class ListItemType04Params(
    val itemTitleString: String?,
    val itemDetail1: String?,
    val itemDetail2: String?,
    val onListItemClick: () -> Unit,
    val onItemsButtonClick: () -> Unit,
    val borderWidth: Dp = 1.dp,
    val cornerRadius: Dp = 4.dp,
)

class ListItemType04ParamsProvider : PreviewParameterProvider<ListItemType04Params> {
    override val values: Sequence<ListItemType04Params> = sequenceOf(
        ListItemType04Params(
            itemTitleString = "Lorem ipsum dolor sit amet",
            itemDetail1 = "123 Main Street,\n" +
                    "Anytown, USA 12345",
            itemDetail2 = "123 Main Street,\n" +
                    "Anytown, USA 12345",
            onListItemClick = {},
            onItemsButtonClick = {},
            borderWidth = 1.dp,
            cornerRadius = 4.dp,
        ),
    )
}

@Preview
@Composable
fun ListItemType04Preview1(
    @PreviewParameter(
        ListItemType04ParamsProvider::class
    ) params: ListItemType04Params
) {
    CommonTheme {
        ListItemType04(
            index = 0,
            itemTitleString = params.itemTitleString,
            itemDetail1 = params.itemDetail1,
            itemDetail2 = params.itemDetail2,
            onListItemClick = params.onListItemClick,
            borderWidth = params.borderWidth,
            cornerRadius = params.cornerRadius,
            tint = MaterialTheme.colors.primaryVariant,
        )
    }
}

@Preview
@Composable
fun ListItemType04Preview2(
    @PreviewParameter(
        ListItemType04ParamsProvider::class
    ) params: ListItemType04Params
) {
    CommonTheme {
        ListItemType04(
            index = 0,
            itemTitleString = params.itemTitleString,
            itemDetail1 = params.itemDetail1,
            itemDetail2 = params.itemDetail2,
            onListItemClick = params.onListItemClick,
            borderWidth = params.borderWidth,
            cornerRadius = params.cornerRadius,
            tint = MaterialTheme.colors.primaryVariant,
            backgroundDrawableId = R.drawable.example_background_1,
            composableBackground = BackgroundsOfLayoutsImpl.backgroundScreen01(
                colors = listOf(
                    MaterialTheme.colors.surface,
                    MaterialTheme.colors.primaryVariant,
                    MaterialTheme.colors.primary,
                ),
                brush = null,
                shape = null,
                alpha = 0.6f,
            )

        )
    }
}