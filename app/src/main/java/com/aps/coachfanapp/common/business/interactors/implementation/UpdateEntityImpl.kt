package com.aps.coachfanapp.common.business.interactors.implementation

import com.aps.coachfanapp.R
import com.aps.coachfanapp.common.business.data.cache.CacheResponseHandler
import com.aps.coachfanapp.common.business.data.cache.StandardCacheDataSource
import com.aps.coachfanapp.common.business.data.network.StandardNetworkDataSource
import com.aps.coachfanapp.common.business.data.util.safeApiCall
import com.aps.coachfanapp.common.business.data.util.safeCacheCall
import com.aps.coachfanapp.common.business.domain.state.*
import com.aps.coachfanapp.common.business.interactors.abstraction.UpdateEntity
import com.aps.coachfanapp.common.util.ALog
import com.aps.coachfanapp.core.business.domain.model.entities.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

private const val TAG = "UpdateEntityImpl"
private const val LOG_ME = true

class UpdateEntityImpl<
        Entity,
        CacheDataSource: StandardCacheDataSource<Entity>,
        NetworkDataSource: StandardNetworkDataSource<Entity>,
        ViewState: com.aps.coachfanapp.common.business.domain.state.ViewState<Entity>>
@Inject constructor(
    private val cacheDataSource: CacheDataSource,
    private val networkDataSource: NetworkDataSource
): UpdateEntity<
        Entity,
        CacheDataSource,
        NetworkDataSource,
        ViewState> {

    override fun updateEntity(
        entity: Entity,
        stateEvent: StateEvent,
        returnViewState: ViewState,
        updateReturnViewState: (ViewState, Entity?)->ViewState,
    ): Flow<DataState<ViewState>?> = flow {
        val methodName: String = "updateEntity"
        if (LOG_ME) ALog.d(TAG, "Method start: $methodName")
        try {
            val cacheResult = safeCacheCall(Dispatchers.IO){
                cacheDataSource.insertOrUpdateEntity(entity)
            }

            val response = object: CacheResponseHandler<ViewState, Entity>(
                response = cacheResult,
                stateEvent = stateEvent
            ){
                override suspend fun handleSuccess(resultObj: Entity): DataState<ViewState>? {
                    var message: String? = UPDATE_ENTITY_SUCCESS
                    var uiComponentType: UIComponentType? = UIComponentType.None()


                    if(resultObj == null){
                        message = GetEntityImpl.GET_ENTITY_NO_MATCHING_RESULTS
                        uiComponentType = UIComponentType.Toast()
                    } else {
                        returnViewState.mainEntity = resultObj
                        updateReturnViewState(returnViewState, resultObj)
                    }
                    return DataState.data(
                        response = Response(
                            messageId = R.string.error,
                            message = message,
                            uiComponentType = uiComponentType as UIComponentType,
                            messageType = MessageType.Success()
                        ),
                        data = returnViewState,
                        stateEvent = stateEvent
                    )
                }
            }.getResult()

            emit(response)

            updateNetwork(response?.stateMessage?.response?.message, entity)
        } catch (e: Exception) {
            ALog.e(TAG, methodName, e)
        } finally {
            if (LOG_ME) ALog.d(TAG, "Method end: $methodName")
        }
    }

    private suspend fun updateNetwork(response: String?, entity: Entity) {
        val methodName: String = "updateNetwork"
        if (LOG_ME) ALog.d(TAG, "Method start: $methodName")
        try {
            if(LOG_ME)ALog.d(TAG, ".$methodName(): response == ${response}")
            if(response.equals(UPDATE_ENTITY_SUCCESS)){
                if(LOG_ME)ALog.d(TAG, ".$methodName(): safeApiCall")
                safeApiCall(Dispatchers.IO){
                    logEntity(entity)
                    if (LOG_ME) ALog.d(TAG, "Inserting or updating entity in network data source.")
                    networkDataSource.insertOrUpdateEntity(entity)
                }
            }
        } catch (e: Exception) {
            ALog.e(TAG, methodName, e)
        } finally {
            if (LOG_ME) ALog.d(TAG, "Method end: $methodName")
        }
    }

    private fun logEntity(entity: Entity) {
        if (LOG_ME) ALog.d(TAG, "Method start: logEntity()")
        try {
            if(entity != null) {
                if(LOG_ME)ALog.d(TAG, "logEntity(): " +
                        "${this.javaClass.simpleName}:\n$entity)")
            } else {
                ALog.w(TAG, "logEntity(): entity == null")
            }
        } catch (e: Exception) {
            ALog.e(TAG, "logEntity()", e)
        } finally {
            if (LOG_ME) ALog.d(TAG, "Method end: logEntity()")
        }
    }

    companion object{
        const val UPDATE_ENTITY_SUCCESS = "Successfully updated entity."
        const val UPDATE_ENTITY_FAILED = "Failed to update entity."
        const val UPDATE_ENTITY_FAILED_PK = "Update failed. Entity is missing primary key."
    }
}