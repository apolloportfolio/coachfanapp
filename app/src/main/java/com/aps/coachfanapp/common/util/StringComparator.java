package com.aps.coachfanapp.common.util;

/**
 * Created by Apollo on 16-08-2016.
 */
public class StringComparator {
    public static boolean stringsAreEqual(String string1, String string2) {
        boolean loguj = false;
        if (string2 == string1) {
            return true;
        }
        if(string1 == null && string2 != null){
            return false;
        } else if(string2 == null && string1 != null){
            return false;
        }
        if (string2 instanceof String) {
            String s = (String)string2;
            int count = string1.length();
            if (s.length() != count) {
                if(loguj)ALog.d("", "StringComparator.stringsAreEqual(): s.length() != count");
                return false;
            }

            // for long Strings until we have array equality intrinsic.
            // Bad benchmarks just push .equals without first getting a
            // hashCode hit (unlike real world use in a Hashtable). Filter
            // out these long strings here. When we get the array equality
            // intrinsic then remove this use of hashCode.
            if (string1.hashCode() != s.hashCode()) {
                if(loguj)ALog.d("", "StringComparator.stringsAreEqual(): string1.hashCode() != s.hashCode()");
                return false;
            }
            for (int i = 0; i < count; ++i) {
                if (string1.charAt(i) != s.charAt(i)) {
                    if(loguj)ALog.d("", "StringComparator.stringsAreEqual(): string1.charAt(" + i + ") == " + string1.charAt(i) + " != s.charAt(" + i + ")  == " + s.charAt(i));
                    return false;
                }
            }
            return true;
        } else {
            if(loguj)ALog.d("", "StringComparator.stringsAreEqual(): Classes do not match.");
            return false;
        }
    }
}
