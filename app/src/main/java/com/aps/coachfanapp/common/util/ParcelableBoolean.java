package com.aps.coachfanapp.common.util;

import android.os.Parcel;
import android.os.Parcelable;

public class ParcelableBoolean implements Parcelable {

    private Boolean value;

    public ParcelableBoolean(Boolean value) {
        this.value = value;
    }

    protected ParcelableBoolean(Parcel in) {
        byte tmpValue = in.readByte();
        value = tmpValue != 0;
    }

    public static final Creator<ParcelableBoolean> CREATOR = new Creator<ParcelableBoolean>() {
        @Override
        public ParcelableBoolean createFromParcel(Parcel in) {
            return new ParcelableBoolean(in);
        }

        @Override
        public ParcelableBoolean[] newArray(int size) {
            return new ParcelableBoolean[size];
        }
    };

    public Boolean getValue() {
        return value;
    }

    public void setValue(Boolean value) {
        this.value = value;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeByte((byte) (value ? 1 : 0));
    }

    @Override
    public int describeContents() {
        return 0;
    }
}

