package com.aps.coachfanapp.ui.theme

import androidx.compose.material.MaterialTheme
import androidx.compose.material.darkColors
import androidx.compose.runtime.Composable
import androidx.compose.ui.graphics.Color

/*
Material Design 2 color system defines a set of colors that are used to create a consistent visual
hierarchy in user interfaces. Here's an explanation of each color and its typical usage:

1. Primary: This is the main color used for branding or to represent the primary actions
in an interface.
It's often the dominant color throughout an app or website.

2. Primary Variant: This color is a variant of the primary color.
It can be used for highlighting certain elements or to provide visual contrast to the primary color.

3. Secondary: Secondary color is used to complement the primary color.
It can be used for secondary actions, accents, or to differentiate certain elements from the primary
ones.

4. Secondary Variant: Similar to the primary variant, this is a variant of the secondary color.
It's used for additional accents or to provide more variation in the interface.

5. Background: Background color is used for the overall background of the interface,
such as the background of cards, screens, or panels.

6. Surface: Surface color is used for the surfaces of components such as cards, dialogs, and sheets.
It provides a visual distinction between the background and the surfaces.

7. Error: Error color is used to indicate errors or alerts in the interface.
It's typically used for error messages, validation indicators, or other critical feedback.

8. On Primary: This color is used for text and icons that are placed on top of the primary color.
It ensures readability and contrast against the primary color.

9. On Secondary: Similar to "On Primary," this color is used for text and icons placed
on top of the secondary color, ensuring readability and contrast.

10. On Background: This color is used for text and icons that are placed on top of the background
color. It ensures readability and contrast against the background.

11. On Surface: Similar to "On Background," this color is used for text and icons that are placed
on top of the surface color, ensuring readability and contrast.

12. On Error: This color is used for text and icons that are placed on top of the error color.
It ensures readability and contrast against the error color, typically used for error messages
or alerts.

By defining these colors and their usage guidelines, Material Design 2 provides a consistent
and visually pleasing experience across different platforms and devices.
*/

private val DarkColorPalette = darkColors(
    primary = Color.White,
    background = DarkGray,
    onBackground = Color.White,
    surface = LightBlue,
    onSurface = DarkGray
)

@Composable
fun CleanArchitectureTheme(darkTheme: Boolean = true, content: @Composable() () -> Unit) {
    MaterialTheme(
        colors = DarkColorPalette,
        typography = Typography,
        shapes = Shapes,
        content = content
    )
}