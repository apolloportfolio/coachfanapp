package com.aps.coachfanapp.feature03.business.interactors

import com.aps.coachfanapp.common.business.data.cache.StandardCacheDataSource
import com.aps.coachfanapp.common.business.data.network.StandardNetworkDataSource
import com.aps.coachfanapp.common.business.interactors.abstraction.DoNothingAtAll
import javax.inject.Inject

// Use cases
class ActivityEntity1DetailsInteractors<
        Entity,
        CacheDataSource: StandardCacheDataSource<Entity>,
        NetworkDataSource: StandardNetworkDataSource<Entity>,
        ViewState: com.aps.coachfanapp.common.business.domain.state.ViewState<Entity>>
@Inject
constructor(

    val doNothingAtAll: DoNothingAtAll<
            Entity,
            CacheDataSource,
            NetworkDataSource,
            ViewState
            >,
)