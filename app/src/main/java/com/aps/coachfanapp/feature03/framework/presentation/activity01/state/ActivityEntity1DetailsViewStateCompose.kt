package com.aps.coachfanapp.feature03.framework.presentation.activity01.state

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.aps.coachfanapp.common.business.domain.state.ViewStateCompose
import com.aps.coachfanapp.core.business.domain.model.entities.ProjectUser
import com.aps.coachfanapp.core.util.Entity1

data class ActivityEntity1DetailsViewStateCompose<T>(
    private val _user: MutableLiveData<ProjectUser?> = MutableLiveData(),
    private val _entity1: MutableLiveData<Entity1?> = MutableLiveData(),
    private val _userWantsToActOnEntity1: MutableLiveData<Boolean?> = MutableLiveData(
        false
    ),
) : ViewStateCompose<T>() {

    val user: LiveData<ProjectUser?> get() = _user
    fun setUser(newValue: ProjectUser?) { _user.value = newValue }

    val entity1: LiveData<Entity1?> get() = _entity1
    fun setEntity1(newValue: Entity1?) { _entity1.value = newValue }

    val userWantsToActOnEntity1: MutableLiveData<Boolean?> get() = _userWantsToActOnEntity1
    fun setUserWantsToActOnEntity1(newValue: Boolean?) {
        _userWantsToActOnEntity1.value = newValue
    }

}

// Parcelable version
//@Parcelize
//data class ActivityEntity1DetailsViewStateCompose<T>(
//    private val _user: ParcelableMutableLiveData<ProjectUser?> = ParcelableMutableLiveData(),
//    private val _entity1: ParcelableMutableLiveData<Entity1?> = ParcelableMutableLiveData(),
//    private val _userWantsToActOnEntity1: ParcelableMutableLiveData<ParcelableBoolean?> = ParcelableMutableLiveData(
//        ParcelableBoolean(false)
//    ),
//) : Parcelable, ViewStateCompose<T>() {
//
//    val user: LiveData<ProjectUser?> get() = _user
//    fun setUser(newValue: ProjectUser?) { _user.value = newValue }
//
//    val entity1: LiveData<Entity1?> get() = _entity1
//    fun setEntity1(newValue: Entity1?) { _entity1.value = newValue }
//
//    val userWantsToActOnEntity1: ParcelableMutableLiveData<ParcelableBoolean?> get() = _userWantsToActOnEntity1
//    fun setUserWantsToActOnEntity1(newValue: Boolean?) {
//        _userWantsToActOnEntity1.value = ParcelableBoolean(newValue)
//    }
//
//}