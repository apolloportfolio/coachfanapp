package com.aps.coachfanapp.feature03.framework.presentation.activity01

import androidx.fragment.app.FragmentFactory
import androidx.lifecycle.ViewModelProvider
import com.aps.coachfanapp.common.framework.presentation.UIController
import com.aps.coachfanapp.common.util.ALog
import com.aps.coachfanapp.common.util.DateUtil
import com.aps.coachfanapp.feature03.framework.presentation.activity01.fragments.ActivityEntity1DetailsFragment1
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import javax.inject.Inject

private const val TAG = "ActivityEntity1DetailsFragmentFactory"
private const val LOG_ME = true

@ExperimentalCoroutinesApi
@FlowPreview
class ActivityEntity1DetailsFragmentFactory
@Inject
constructor(
    private val viewModelFactory: ViewModelProvider.Factory,
    private val dateUtil: DateUtil
): FragmentFactory() {

    lateinit var uiController: UIController
    override fun instantiate(classLoader: ClassLoader, className: String) =
        when(className){

            ActivityEntity1DetailsFragment1::class.java.name -> {
                val fragment = ActivityEntity1DetailsFragment1(viewModelFactory, dateUtil)
                if(::uiController.isInitialized){
                    fragment.setUIController(uiController)
                    if(LOG_ME) ALog.d(TAG, "instantiate(): ActivityEntity1DetailsFragment1.uiController initialized")
                }
                fragment
            }

            else -> {
                if(LOG_ME) ALog.w(TAG, "instantiate(): Unknown class $className, uiController uninitialized")
                super.instantiate(classLoader, className)
            }
        }
}