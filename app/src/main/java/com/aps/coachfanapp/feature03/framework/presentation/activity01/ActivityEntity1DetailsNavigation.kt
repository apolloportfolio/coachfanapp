package com.aps.coachfanapp.feature03.framework.presentation.activity01

import androidx.fragment.app.Fragment
import com.aps.coachfanapp.core.business.domain.model.entities.ProjectUser
import kotlinx.coroutines.ExperimentalCoroutinesApi
import com.aps.coachfanapp.core.util.Entity1

interface ActivityEntity1DetailsNavigation {

    @ExperimentalCoroutinesApi
    fun navigateBack(fragment: Fragment) {
        fragment.activity?.finish()
    }

    @ExperimentalCoroutinesApi
    fun navigateToEntity1ActionActivity(
        fragment: Fragment,
        user: ProjectUser,
        sportsGame : Entity1,
    ) {
//        val intent = Intent(fragment.activity, ActivityEntity1Action::class.java)
//        intent.putExtra(IntentExtras.ENTITY1, entity1 as Parcelable)
//        intent.putExtra(IntentExtras.APP_USER, user as Parcelable)
//        fragment.activity?.startActivity(intent)
    }
}