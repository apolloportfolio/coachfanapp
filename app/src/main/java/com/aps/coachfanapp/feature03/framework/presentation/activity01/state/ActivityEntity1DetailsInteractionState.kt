package com.aps.coachfanapp.feature03.framework.presentation.activity01.state

sealed class ActivityEntity1DetailsInteractionState {
    class EditState: ActivityEntity1DetailsInteractionState() {

        override fun toString(): String {
            return "EditState"
        }
    }

    class DefaultState: ActivityEntity1DetailsInteractionState(){

        override fun toString(): String {
            return "DefaultState"
        }
    }
}