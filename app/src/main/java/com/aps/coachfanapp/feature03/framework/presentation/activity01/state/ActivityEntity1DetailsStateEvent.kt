package com.aps.coachfanapp.feature03.framework.presentation.activity01.state

import android.content.Context
import com.aps.coachfanapp.R
import com.aps.coachfanapp.common.business.domain.state.StateEvent
import com.aps.coachfanapp.common.business.domain.state.StateMessage
import com.aps.coachfanapp.core.util.ApplicationContextProvider
import com.aps.coachfanapp.core.util.Entity1

private const val TAG = "ActivityEntity1DetailsStateEvent"

sealed class ActivityEntity1DetailsStateEvent: StateEvent {

    class UpdateMainEntityEvent(
        val mainEntity: Entity1,
        val context: Context?,
    ): ActivityEntity1DetailsStateEvent(){

        override fun errorInfo(): String {
            return context?.getString(R.string.activity_entity1_details_state_event_update_main_entity_error) ?: "ActivityEntity1DetailsStateEventError"
        }

        override fun eventName(): String {
            return "UpdateMainEntityEvent"
        }

        override fun shouldDisplayProgressBar() = true
    }

    class GetAllCacheEntities(
        val applicationContextProvider: ApplicationContextProvider,
    ): ActivityEntity1DetailsStateEvent(){

        override fun errorInfo(): String {
            return applicationContextProvider.applicationContext(TAG).getString(R.string.activity_entity1_details_state_event_get_all_cache_entities_error)
        }

        override fun eventName(): String {
            return "GetAllCacheEntities"
        }

        override fun shouldDisplayProgressBar() = true
    }

    class Entity1ActionEvent(
        val context: Context?,
    ): ActivityEntity1DetailsStateEvent(){

        override fun errorInfo(): String {
            return context?.getString(R.string.activity_entity1_details_state_event_act_on_entity1_error) ?: "ActivityEntity1DetailsStateEventError"
        }

        override fun eventName(): String {
            return "Entity1ActionEvent"
        }

        override fun shouldDisplayProgressBar() = true
    }


    class CreateStateMessageEvent(
        val stateMessage: StateMessage,
        val applicationContextProvider: ApplicationContextProvider,
    ): ActivityEntity1DetailsStateEvent(){

        override fun errorInfo(): String {
            return applicationContextProvider.applicationContext(TAG).getString(R.string.activity_entity1_details_state_event_create_state_message_error)
        }

        override fun eventName(): String {
            return "CreateStateMessageEvent"
        }

        override fun shouldDisplayProgressBar() = false
    }

    class None(
        val context: Context?,
    ): ActivityEntity1DetailsStateEvent(){

        override fun errorInfo(): String {
            return context?.getString(R.string.activity_entity1_details_state_event_none_error) ?: "ActivityEntity1DetailsStateEventError"
        }

        override fun eventName(): String {
            return "None"
        }

        override fun shouldDisplayProgressBar() = false
    }
}
