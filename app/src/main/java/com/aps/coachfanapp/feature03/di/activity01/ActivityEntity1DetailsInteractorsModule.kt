package com.aps.coachfanapp.feature03.di.activity01

import com.aps.coachfanapp.common.business.interactors.abstraction.DoNothingAtAll
import com.aps.coachfanapp.common.business.interactors.implementation.DoNothingAtAllImpl
import com.aps.coachfanapp.core.business.data.network.abstraction.UserNetworkDataSource
import com.aps.coachfanapp.feature03.framework.presentation.activity01.state.ActivityEntity1DetailsViewState
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import com.aps.coachfanapp.core.util.Entity1
import com.aps.coachfanapp.core.util.Entity1CacheDataSourceImpl
import com.aps.coachfanapp.core.util.Entity1NetworkDataSource

@ExperimentalCoroutinesApi
@FlowPreview
@Module
@InstallIn(SingletonComponent::class)
object ActivityEntity1DetailsInteractorsModule {

    @Provides
    fun provideDoNothingAtAll(
        cacheDataSource: Entity1CacheDataSourceImpl,
        networkDataSource: UserNetworkDataSource
    ): DoNothingAtAll<
            Entity1,
            Entity1CacheDataSourceImpl,
            Entity1NetworkDataSource,
            ActivityEntity1DetailsViewState<Entity1>> {
        return DoNothingAtAllImpl<
                Entity1,
                Entity1CacheDataSourceImpl,
                Entity1NetworkDataSource,
                ActivityEntity1DetailsViewState<Entity1>>()
    }

}