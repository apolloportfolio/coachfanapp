package com.aps.coachfanapp.util

import androidx.test.espresso.idling.CountingIdlingResource
import com.aps.coachfanapp.common.util.ALog


object EspressoIdlingResource {

    private val CLASS_NAME = "EspressoIdlingResource"

    private const val RESOURCE = "_GLOBAL_"

    @JvmField val countingIdlingResource = CountingIdlingResource(RESOURCE)

    fun increment() {
        ALog.d(CLASS_NAME, ".decrement(): INCREMENTING")
        countingIdlingResource.increment()
    }

    fun decrement() {
        if (!countingIdlingResource.isIdleNow) {
            ALog.d(CLASS_NAME, ".decrement(): DECREMENTING")
            countingIdlingResource.decrement()
        }
    }

    fun clear() {
        if (!countingIdlingResource.isIdleNow) {
            decrement()
            clear()
        }
    }
}








