package com.aps.coachfanapp.framework.presentation.activity

import androidx.appcompat.app.AppCompatActivity
import dagger.hilt.android.AndroidEntryPoint

// https://www.youtube.com/watch?v=k4zG93ogWFY
@AndroidEntryPoint
class HiltTestActivity : AppCompatActivity()