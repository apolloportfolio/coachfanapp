package com.aps.coachfanapp.core.framework.datasources

import android.app.Application
import com.aps.coachfanapp.cfahomescreen.business.domain.model.entities.SportsTeam
import com.google.common.reflect.TypeToken
import com.google.gson.Gson
import javax.inject.Inject

class EntityDataFactorySportsTeam
@Inject
constructor(
    override val application: Application
): EntityDataFactory<SportsTeam>(application) {
//    override val testClassLoader: ClassLoader,
//    override val fileNameWithTestData: String): com.aps.catemplateapp.core.framework.datasources.EntityDataFactory<SportsTeam>(testClassLoader, fileNameWithTestData) {


    override fun produceListOfEntities(): List<SportsTeam> {
        return Gson()
            .fromJson(
                getEntitiesFromFile("sportsTeam_test_list.json"),
                object : TypeToken<List<SportsTeam>>() {}.type
            )
    }
}