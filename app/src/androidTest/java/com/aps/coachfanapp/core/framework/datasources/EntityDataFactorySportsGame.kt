package com.aps.coachfanapp.core.framework.datasources

import android.app.Application
import com.aps.coachfanapp.cfahomescreen.business.domain.model.entities.SportsGame
import com.google.common.reflect.TypeToken
import com.google.gson.Gson
import javax.inject.Inject

class EntityDataFactorySportsGame
@Inject
constructor(
    override val application: Application
): EntityDataFactory<SportsGame>(application) {
//    override val testClassLoader: ClassLoader,
//    override val fileNameWithTestData: String): com.aps.catemplateapp.core.framework.datasources.EntityDataFactory<SportsGame>(testClassLoader, fileNameWithTestData) {


    override fun produceListOfEntities(): List<SportsGame> {
        return Gson()
            .fromJson(
                getEntitiesFromFile("sportsGame_test_list.json"),
                object : TypeToken<List<SportsGame>>() {}.type
            )
    }
}