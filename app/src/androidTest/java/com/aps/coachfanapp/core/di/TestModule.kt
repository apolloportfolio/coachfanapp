package com.aps.coachfanapp.core.di

import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import androidx.room.Room
import com.aps.coachfanapp.common.util.AndroidTestUtils
import com.aps.coachfanapp.core.business.domain.model.factories.UserFactory
import com.aps.coachfanapp.core.framework.datasources.EntityDataFactoryUser
import com.aps.coachfanapp.core.framework.datasources.cachedatasources.room.ProjectRoomDatabase
import com.aps.coachfanapp.core.framework.datasources.cachedatasources.room.daos.*
import com.aps.coachfanapp.core.framework.datasources.preferences.PreferenceKeys
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.FirebaseFirestoreSettings
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import javax.inject.Singleton

@ExperimentalCoroutinesApi
@FlowPreview
@Module
@InstallIn(SingletonComponent::class)
object TestModule {

    @JvmStatic
    @Singleton
    @Provides
    fun provideAndroidTestUtils(): AndroidTestUtils {
        return AndroidTestUtils(true)
    }

    @JvmStatic
    @Singleton
    @Provides
    fun provideSharedPreferences(
        application: Application
//        application: HiltTestApplication
    ): SharedPreferences {
        return application
            .getSharedPreferences(
                PreferenceKeys.PROJECT_PREFERENCES,
                Context.MODE_PRIVATE
            )
    }

    @JvmStatic
    @Singleton
    @Provides
    fun provideDatabase(app: Application): ProjectRoomDatabase {
        return Room
            .inMemoryDatabaseBuilder(app, ProjectRoomDatabase::class.java)
            .fallbackToDestructiveMigration()
            .build()
    }

    @JvmStatic
    @Singleton
    @Provides
    fun provideFirestoreSettings(): FirebaseFirestoreSettings {
        return FirebaseFirestoreSettings.Builder()
            .setHost("10.0.2.2:8080")
            .setSslEnabled(false)
            .setPersistenceEnabled(false)
            .build()
    }

    @JvmStatic
    @Singleton
    @Provides
    fun provideFirebaseFirestore(
        firestoreSettings: FirebaseFirestoreSettings
    ): FirebaseFirestore {
        val firestore = FirebaseFirestore.getInstance()
        firestore.firestoreSettings = firestoreSettings
        return firestore
    }

    @JvmStatic
    @Singleton
    @Provides
    fun provideUserDataFactory(
        application: Application,
        userFactory: UserFactory
    ): EntityDataFactoryUser {
        return EntityDataFactoryUser(application)
    }



    @Singleton
    @Provides
    fun provideUserDao(database : ProjectRoomDatabase) : UserDao {
        return database.userDao()
    }

//    @Singleton
//    @Provides
//    fun provideEntity1Dao(database : ProjectRoomDatabase) : Entity1Dao {
//        return database.entity1Dao()
//    }
//
//    @Singleton
//    @Provides
//    fun provideEntity2Dao(database : ProjectRoomDatabase) : Entity2Dao {
//        return database.entity2Dao()
//    }
//
//    @Singleton
//    @Provides
//    fun provideEntity3Dao(database : ProjectRoomDatabase) : Entity3Dao {
//        return database.entity3Dao()
//    }
//
//    @Singleton
//    @Provides
//    fun provideEntity4Dao(database : ProjectRoomDatabase) : Entity4Dao {
//        return database.entity4Dao()
//    }
}















