package com.aps.coachfanapp.core.framework.datasources

import android.app.Application
import com.aps.coachfanapp.cfahomescreen.business.domain.model.entities.SportsTeamPlayer
import com.google.common.reflect.TypeToken
import com.google.gson.Gson
import javax.inject.Inject

class EntityDataFactorySportsTeamPlayer
@Inject
constructor(
    override val application: Application
): EntityDataFactory<SportsTeamPlayer>(application) {
//    override val testClassLoader: ClassLoader,
//    override val fileNameWithTestData: String): com.aps.catemplateapp.core.framework.datasources.EntityDataFactory<SportsTeamPlayer>(testClassLoader, fileNameWithTestData) {


    override fun produceListOfEntities(): List<SportsTeamPlayer>{
        val entities: List<SportsTeamPlayer> = Gson()
            .fromJson(
                getEntitiesFromFile("sportsTeamPlayer_test_list.json"),
                object: TypeToken<List<SportsTeamPlayer>>() {}.type
            )
        return entities
    }
}