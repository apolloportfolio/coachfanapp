package com.aps.coachfanapp.core.util

import com.aps.coachfanapp.common.util.ALog

private const val TAG = "ActivationCodeGenerator"
private const val LOG_ME = true

class ActivationCodeGeneratorPhone {

    companion object {
        private var lastActivationCode : String? = null

        fun getLastActivationCode() = lastActivationCode

        fun generateActivationCode() : String {
            lastActivationCode = RandomStringGenerator.generateRandomString(
                ProjectConstants.PHONE_ACTIVATION_CODE_LENGTH,
                ProjectConstants.PHONE_ACTIVATION_CODE_CHARSET,
            )
            if(LOG_ME) ALog.d(TAG, ".generateActivationCode(): " +
                    "lastActivationCode == $lastActivationCode")
            return lastActivationCode!!
        }
    }
}