package com.aps.coachfanapp.core.di

import com.apollographql.apollo3.ApolloClient
import com.aps.coachfanapp.core.util.ProjectConstants
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
class GraphQLModule {
    @Provides
    @Singleton
    fun provideApolloClient(): ApolloClient {
        return ApolloClient.Builder()
            .serverUrl(ProjectConstants.GRAPHQL_APOLLO_CLIENT_SERVER_URL)
            .build()
    }
}