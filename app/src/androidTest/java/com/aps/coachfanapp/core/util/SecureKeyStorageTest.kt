package com.aps.coachfanapp.core.util

import android.util.Log
import androidx.test.filters.MediumTest
import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner
import androidx.test.platform.app.InstrumentationRegistry
import com.aps.coachfanapp.BaseInstrumentedTest
import com.aps.coachfanapp.core.di.NetworkDataSourcesModule
import com.aps.coachfanapp.core.di.ProductionModule
import com.aps.coachfanapp.core.di.RoomModule
import dagger.hilt.android.testing.HiltAndroidTest
import dagger.hilt.android.testing.UninstallModules
import kotlinx.coroutines.*
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import java.util.*
import kotlin.test.assertEquals
private const val TAG = "SecureKeyStorageTest"

@MediumTest
@ExperimentalCoroutinesApi
@FlowPreview
@RunWith(AndroidJUnit4ClassRunner::class)
@HiltAndroidTest
@UninstallModules(
    RoomModule::class,
    NetworkDataSourcesModule::class,
    ProductionModule::class,
)
class SecureKeyStorageTest : BaseInstrumentedTest() {

    private lateinit var secureKeyStorage: SecureKeyStorage

    init {

    }

    @Before
    override fun runBeforeEveryTest() {
        injectTest()
        context = InstrumentationRegistry.getInstrumentation().targetContext
        secureKeyStorage = SecureKeyStorage()
    }

    @After
    override fun runAfterEveryTest() {
        enableInternetConnection()
    }


    override fun injectTest() {

    }

    @Test
    fun getEmailAddressForEmailVerificationTest():Unit = runBlocking{
        val methodName = "getEmailAddressForEmailVerificationTest"
        Log.d(TAG, ".$methodName(): Method start: $methodName")

        val expectedStringResult = "exampleEmailAddress"
        val actualResult = secureKeyStorage.getEmailAddressForEmailVerification()

        assertEquals(expectedStringResult, actualResult)
        Log.d(TAG, ".$methodName(): Method end: $methodName")
    }

    @Test
    fun getEmailPasswordForEmailVerificationTest():Unit = runBlocking{
        val methodName = "getEmailPasswordForEmailVerificationTest"
        Log.d(TAG, ".$methodName(): Method start: $methodName")

        val expectedStringResult = "examplePassword"
        val actualResult = secureKeyStorage.getEmailPasswordForEmailVerification()

        assertEquals(expectedStringResult, actualResult)
        Log.d(TAG, ".$methodName(): Method end: $methodName")
    }

    @Test
    fun getGatewayNameForProjectTest():Unit = runBlocking{
        val methodName = "getGatewayNameForProjectTest"
        Log.d(TAG, ".$methodName(): Method start: $methodName")

        val expectedStringResult = "exampleGatewayName"
        val actualResult = secureKeyStorage.getGatewayNameForProject()

        assertEquals(expectedStringResult, actualResult)
        Log.d(TAG, ".$methodName(): Method end: $methodName")
    }

    @Test
    fun getGatewayMerchantIDTest():Unit = runBlocking{
        val methodName = "getGatewayMerchantIDTest"
        Log.d(TAG, ".$methodName(): Method start: $methodName")

        val expectedStringResult = "exampleGatewayMerchantId"
        val actualResult = secureKeyStorage.getGatewayMerchantID()

        assertEquals(expectedStringResult, actualResult)
        Log.d(TAG, ".$methodName(): Method end: $methodName")
    }

    @Test
    fun getMerchantNameTest():Unit = runBlocking{
        val methodName = "getMerchantNameTest"
        Log.d(TAG, ".$methodName(): Method start: $methodName")

        val expectedStringResult = "exampleMerchantName"
        val actualResult = secureKeyStorage.getMerchantName()

        assertEquals(expectedStringResult, actualResult)
        Log.d(TAG, ".$methodName(): Method end: $methodName")
    }

    @Test
    fun getDirectTokenizationPublicKeyTest():Unit = runBlocking{
        val methodName = "getDirectTokenizationPublicKeyTest"
        Log.d(TAG, ".$methodName(): Method start: $methodName")

        val expectedStringResult = "exampleDirectTokenizationPublicKey"
        val actualResult = secureKeyStorage.getDirectTokenizationPublicKey()

        assertEquals(expectedStringResult, actualResult)
        Log.d(TAG, ".$methodName(): Method end: $methodName")
    }
}