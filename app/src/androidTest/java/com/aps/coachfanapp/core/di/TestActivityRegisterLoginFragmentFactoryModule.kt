package com.aps.coachfanapp.core.di

import androidx.fragment.app.FragmentFactory
import androidx.lifecycle.ViewModelProvider
import com.aps.coachfanapp.common.util.DateUtil
import com.aps.coachfanapp.feature01.framework.presentation.activity01.TestRegisterLoginActivityFragmentFactory
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityComponent
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import javax.inject.Singleton

@FlowPreview
@ExperimentalCoroutinesApi
@Module
@InstallIn(ActivityComponent::class)
object TestRegisterLoginActivityFragmentFactoryModule {
    @Singleton
    @JvmStatic
    @Provides
    fun provideTestRegisterLoginActivityFragmentFactory(
        viewModelFactory: ViewModelProvider.Factory,
        dateUtil: DateUtil
    ): FragmentFactory {
        return TestRegisterLoginActivityFragmentFactory(viewModelFactory, dateUtil)
    }
}