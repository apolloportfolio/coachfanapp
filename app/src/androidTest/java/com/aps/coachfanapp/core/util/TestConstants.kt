package com.aps.coachfanapp.core.util

import com.aps.coachfanapp.common.util.PromoCodeManager


const val FIRESTORE_TEST_EMAIL = "noalternative@vp.pl"
const val FIRESTORE_TEST_PASSWORD = "passwordasdfggasfgd"
const val TEST_USER_REAL_ID = "6664abea-7584-486b-9f88-87a21870b0ec666"
const val TEST_USER_REAL_FIRESTORE_DOCUMENT_ID = "6664abea-7584-486b-9f88-87a21870b0ec666"
const val TEST_USER_2_REAL_ID = "5554abea-7584-486b-9f88-87a21870b0ec555"
const val TEST_USER_2_REAL_FIRESTORE_DOCUMENT_ID = "5554abea-7584-486b-9f88-87a21870b0ec555"
const val TEST_USER_3_REAL_ID = "4444abea-7584-486b-9f88-87a21870b0ec444"
const val TEST_USER_3_REAL_FIRESTORE_DOCUMENT_ID = "4444abea-7584-486b-9f88-87a21870b0ec444"
const val TEST_USER_4_REAL_ID = "4444abea-7584-486b-9f88-87a21870b0ec333"
const val TEST_USER_4_REAL_FIRESTORE_DOCUMENT_ID = "4444abea-7584-486b-9f88-87a21870b0ec333"
const val TEST_USER_REAL_EMAIL_ADDRESS = "noalternative@vp.pl"
const val TEST_USER_REAL_PASSWORD = "real67password123"
const val TEST_USER_FAKE_EMAIL_ADDRESS = "noalternative67@vp.pl"
const val TEST_USER_FAKE_PASSWORD = "real6password123"
const val TEST_USER_INVALID_EMAIL_ADDRESS_1 = "noalternative67vp.pl"
const val TEST_USER_INVALID_EMAIL_ADDRESS_2 = "noalternative@67@vp.pl"
const val TEST_USER_INVALID_EMAIL_ADDRESS_3 = "a”b(c)d,e:f;gi[j\\k]l@vp.pl"
const val TEST_USER_INVALID_EMAIL_ADDRESS_4 = "abc”test”email@.pl"
const val TEST_USER_INVALID_EMAIL_ADDRESS_5 = "abc is”not\\valid@vp.pl"
const val TEST_USER_INVALID_EMAIL_ADDRESS_6 = "abc\\ is\\”not\\valid@vp.pl"
const val TEST_USER_INVALID_EMAIL_ADDRESS_7 = ".test@vp.pl"
const val TEST_USER_INVALID_EMAIL_ADDRESS_8 = "test@vp..pl"
const val TEST_USER_INVALID_EMAIL_ADDRESS_9 = " noalternative67@vp.pl"
const val TEST_USER_INVALID_EMAIL_ADDRESS_10 = "noalternative67@vp.pl "
const val TEST_USER_INVALID_PASSWORD = ""
const val TEST_USER_REAL_STREET_NAME = "Jagiełły 7"
const val TEST_USER_REAL_PHONE_NUMBER = "+48793666666"

const val TEST_USER_RENTER_REAL_EMAIL_ADDRESS = "renter@vp.pl"
const val TEST_USER_RENTER_REAL_PHONE_NUMBER = "+48792666666"
const val TEST_USER_RENTER_REAL_ID = "6664abea-6666-6666-6666-87a21870b0ec66"
const val TEST_USER_RENTER_REAL_FIRESTORE_DOCUMENT_ID = "6664abea-6666-6666-6666-87a21870b0ec666"

const val TEST_PHONE_ACTIVATION_CODE = "1234"

const val TEST_USER_NAME = "Google"
const val TEST_USER_SURNAME = "Test"
const val TEST_USER_DATE_OF_BIRTH = "13.09.1987"
const val TEST_USER_DATE_OF_BIRTH_IN_FIRESTORE_FORMAT = "1987-09-13 12:00:00 AM"
const val TEST_USER_DATE_OF_BIRTH_DAY = 13
const val TEST_USER_DATE_OF_BIRTH_MONTH = 9
const val TEST_USER_DATE_OF_BIRTH_YEAR = 1987
const val TEST_USER_PERSONAL_ID_NUMBER = "87091363339"                  // https://pesel.cstudios.pl/o-generatorze/generator-on-line
const val TEST_USER_STREET_AND_HOUSE_NUMBER = "Jagiełły 7/15"
const val TEST_USER_CITY = "Koszalin"
const val TEST_USER_ZIP_CODE = "20-281"
const val TEST_USER_COUNTRY = "Poland"
const val TEST_USER_DESCRIPTION = "Just a test renter."
const val TEST_USER_EDITED_DESCRIPTION = "Just a test renter who edited their description."

const val OFFER_GIVER_AVERAGE_RATING = 4.4f
const val OFFER_GIVER_NUMBER_OF_RATINGS = 56

const val TEST_FIRESTORE_ID_IN_DOCUMENT : String = "2474abea-7584-486b-9f88-87a21870b0ec101"
val TEST_USER_INVITATION_CODE = PromoCodeManager.getInviteCode(TEST_FIRESTORE_ID_IN_DOCUMENT)!!

