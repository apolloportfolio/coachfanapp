package com.aps.coachfanapp.core.di

import com.aps.coachfanapp.core.framework.datasources.networkdatasources.firebase.abstraction.*
import com.aps.coachfanapp.core.framework.datasources.networkdatasources.firebase.implementation.*
import com.aps.coachfanapp.core.util.Entity1FirestoreService
import com.aps.coachfanapp.core.util.Entity1FirestoreServiceImpl
import com.aps.coachfanapp.core.util.Entity2FirestoreService
import com.aps.coachfanapp.core.util.Entity2FirestoreServiceImpl
import com.aps.coachfanapp.core.util.Entity3FirestoreService
import com.aps.coachfanapp.core.util.Entity3FirestoreServiceImpl
import com.aps.coachfanapp.core.util.Entity4FirestoreService
import com.aps.coachfanapp.core.util.Entity4FirestoreServiceImpl
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
abstract class FirestoreDAOsModule {

    @Binds
    abstract fun bindUserFirestoreService(implementation: UserFirestoreServiceImpl): UserFirestoreService

    @Binds
    abstract fun bindEntity1FirestoreService(implementation: Entity1FirestoreServiceImpl): Entity1FirestoreService

    @Binds
    abstract fun bindEntity2FirestoreService(implementation: Entity2FirestoreServiceImpl): Entity2FirestoreService

    @Binds
    abstract fun bindEntity3FirestoreService(implementation: Entity3FirestoreServiceImpl): Entity3FirestoreService

    @Binds
    abstract fun bindEntity4FirestoreService(implementation: Entity4FirestoreServiceImpl): Entity4FirestoreService

}