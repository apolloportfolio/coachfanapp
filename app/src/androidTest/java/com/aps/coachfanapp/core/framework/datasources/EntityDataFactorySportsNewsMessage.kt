package com.aps.coachfanapp.core.framework.datasources

import android.app.Application
import com.aps.coachfanapp.cfahomescreen.business.domain.model.entities.SportsNewsMessage
import com.google.common.reflect.TypeToken
import com.google.gson.Gson
import javax.inject.Inject

class EntityDataFactorySportsNewsMessage
@Inject
constructor(
    override val application: Application
): EntityDataFactory<SportsNewsMessage>(application) {
//    override val testClassLoader: ClassLoader,
//    override val fileNameWithTestData: String): com.aps.catemplateapp.core.framework.datasources.EntityDataFactory<SportsNewsMessage>(testClassLoader, fileNameWithTestData) {


    override fun produceListOfEntities(): List<SportsNewsMessage> {
        return Gson()
            .fromJson(
                getEntitiesFromFile("sportsNewsMessage_test_list.json"),
                object : TypeToken<List<SportsNewsMessage>>() {}.type
            )
    }
}