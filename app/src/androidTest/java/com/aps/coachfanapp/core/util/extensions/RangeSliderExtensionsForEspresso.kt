package com.aps.coachfanapp.core.util.extensions

import android.view.View
import androidx.test.espresso.Espresso
import androidx.test.espresso.UiController
import androidx.test.espresso.ViewAction
import androidx.test.espresso.ViewInteraction
import androidx.test.espresso.matcher.BoundedMatcher
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.espresso.matcher.ViewMatchers.isAssignableFrom
import com.google.android.material.slider.RangeSlider
import org.hamcrest.Description
import org.hamcrest.Matcher
import kotlin.reflect.KClass

// Reference: https://stackoverflow.com/questions/65390086/androidx-how-to-test-slider-in-ui-tests-espresso
fun withRangeSliderValueLeft(expectedValue: Float): Matcher<View?> {
    return object : BoundedMatcher<View?, RangeSlider>(RangeSlider::class.java) {
        override fun describeTo(description: Description) {
            description.appendText("expected: $expectedValue")
        }

        override fun matchesSafely(slider: RangeSlider?): Boolean {
            slider?.values?.let{
                return it[0] == expectedValue
            }
            return false
        }
    }
}
fun withRangeSliderValueRight(expectedValue: Float): Matcher<View?> {
    return object : BoundedMatcher<View?, RangeSlider>(RangeSlider::class.java) {
        override fun describeTo(description: Description) {
            description.appendText("expected: $expectedValue")
        }

        override fun matchesSafely(slider: RangeSlider?): Boolean {
            slider?.values?.let {
                return it[1] == expectedValue
            }
            return false
        }
    }
}

fun setRangeSliderValueLeft(value: Float): ViewAction {
    return object : ViewAction {
        override fun getDescription(): String {
            return "Set RangeSlider value[0] to $value"
        }

        override fun getConstraints(): Matcher<View> {
            return ViewMatchers.isAssignableFrom(RangeSlider::class.java)
        }

        override fun perform(uiController: UiController?, view: View) {
            val seekBar = view as RangeSlider
            seekBar.values[0] = value
        }
    }
}

fun setRangeSliderValueRight(value: Float): ViewAction {
    return object : ViewAction {
        override fun getDescription(): String {
            return "Set RangeSlider value[1] to $value"
        }

        override fun getConstraints(): Matcher<View> {
            return ViewMatchers.isAssignableFrom(RangeSlider::class.java)
        }

        override fun perform(uiController: UiController?, view: View) {
            val seekBar = view as RangeSlider
            seekBar.values[1] = value
        }
    }
}

fun <T : Espresso> KClass<T>.getValueFrom(matcher: ViewInteraction): Float? {
    var valueFrom : Float? = null
    matcher.perform(object : ViewAction {
        override fun getConstraints(): Matcher<View> {
            return isAssignableFrom(RangeSlider::class.java)
        }

        override fun getDescription(): String {
            return "valueFrom of the RangeSlider"
        }

        override fun perform(uiController: UiController, view: View) {
            val rangeSlider = view as RangeSlider
            valueFrom = rangeSlider.valueFrom
        }
    })

    return valueFrom
}

fun <T : Espresso> KClass<T>.getValueTo(matcher: ViewInteraction): Float? {
    var valueTo : Float? = null
    matcher.perform(object : ViewAction {
        override fun getConstraints(): Matcher<View> {
            return isAssignableFrom(RangeSlider::class.java)
        }

        override fun getDescription(): String {
            return "valueTo of the RangeSlider"
        }

        override fun perform(uiController: UiController, view: View) {
            val rangeSlider = view as RangeSlider
            valueTo = rangeSlider.valueTo
        }
    })

    return valueTo
}
