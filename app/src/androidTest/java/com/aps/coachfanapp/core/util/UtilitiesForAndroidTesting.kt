package com.aps.coachfanapp.core.util

import android.view.View
import androidx.annotation.IdRes
import androidx.annotation.StringRes
import androidx.test.InstrumentationRegistry.getTargetContext
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.NoMatchingViewException
import androidx.test.espresso.ViewInteraction
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withText
import androidx.test.platform.app.InstrumentationRegistry.getInstrumentation
import androidx.test.uiautomator.UiDevice
import androidx.test.uiautomator.UiObject
import androidx.test.uiautomator.UiSelector
import org.hamcrest.Matcher


class UtilitiesForAndroidTesting {

    companion object {
        // https://stackoverflow.com/questions/35186902/testing-progress-bar-on-android-with-espresso
        fun uiObjectWithId(@IdRes id: Int): UiObject? {
            val resourceId = getTargetContext().resources.getResourceName(id)
            val selector = UiSelector().resourceId(resourceId)
            return UiDevice.getInstance(getInstrumentation()).findObject(selector)
        }

        // https://stackoverflow.com/questions/35186902/testing-progress-bar-on-android-with-espresso
        fun uiObjectWithText(@StringRes stringRes: Int): UiObject? {
            val selector = UiSelector().text(getTargetContext().getString(stringRes))
            return UiDevice.getInstance(getInstrumentation()).findObject(selector)
        }
        fun uiObjectWithText(string: String): UiObject? {
            val selector = UiSelector().text(string)
            return UiDevice.getInstance(getInstrumentation()).findObject(selector)
        }
        fun sthWithText(string:String): ViewInteraction {
            return onView(withText(string)).check(matches(isDisplayed()))
        }

        private const val DEFAULT_WAIT_TIMEOUT : Long = 5000
        private const val DEFAULT_SLEEP_INTERVAL : Long = 100
        fun waitUntilCondition(
            matcher: Matcher<View>,
            timeout: Long = DEFAULT_WAIT_TIMEOUT,
            condition: (View?) -> Boolean
        ) {
            var success = false
            lateinit var exception: NoMatchingViewException
            val loopCount = timeout / DEFAULT_SLEEP_INTERVAL
            (0..loopCount).forEach {
                onView(matcher).check { view, noViewFoundException ->
                    if (condition(view)) {
                        success = true
                        return@check
                    } else {
                        Thread.sleep(DEFAULT_SLEEP_INTERVAL)
                        exception = noViewFoundException
                    }
                }

                if (success) {
                    return
                }
            }
            throw exception
        }
    }
}