package com.aps.coachfanapp.core.util.extensions

import android.view.FrameMetrics
import android.view.FrameMetrics.ANIMATION_DURATION
import androidx.compose.ui.semantics.SemanticsNode
import androidx.compose.ui.semantics.getOrNull
import androidx.compose.ui.test.ExperimentalTestApi
import androidx.compose.ui.test.SemanticsNodeInteraction
import androidx.compose.ui.test.hasTestTag
import androidx.compose.ui.test.junit4.ComposeTestRule
import androidx.compose.ui.test.onChildren
import androidx.compose.ui.test.onLast
import androidx.compose.ui.test.onNodeWithTag
import androidx.compose.ui.test.performClick
import androidx.compose.ui.test.performTouchInput
import androidx.compose.ui.test.printToString
import androidx.compose.ui.test.swipeLeft
import androidx.compose.ui.test.swipeRight
import androidx.compose.ui.test.swipeUp
import com.aps.coachfanapp.common.util.ALog

// Reference: https://stackoverflow.com/questions/73953856/how-to-simulate-a-swipe-gesture-on-a-card-in-a-composable-test
fun ComposeTestRule.performSwipeRight(
    actionDescription : String,
    TAG : String,
    LOG_ME : Boolean,
    methodName : String,

    node: SemanticsNodeInteraction,
    sideThresholdFraction: Float = 0.0f,
) {
    if(LOG_ME) ALog.d(TAG, "$methodName(): $actionDescription")
    try {
        waitForIdle()
        mainClock.autoAdvance = false
        node.performTouchInput {
            swipeRight(
                startX = this.width*sideThresholdFraction,
//                durationMillis = 100,
            )
        }
        mainClock.advanceTimeBy(ANIMATION_DURATION.toLong() + 5L) /*add 5s buffer*/
        mainClock.autoAdvance = true
    } catch (e: Exception) {
        ALog.e(TAG, "$methodName()", e)
    }
}

fun ComposeTestRule.performSwipeLeft(
    actionDescription : String,
    TAG : String,
    LOG_ME : Boolean,
    methodName : String,

    node: SemanticsNodeInteraction,
    sideThresholdFraction: Float = 0.0f,
) {
    if(LOG_ME) ALog.d(TAG, "$methodName(): $actionDescription")
    try {
        waitForIdle()
        mainClock.autoAdvance = false
        node.performTouchInput {
            swipeLeft(
                startX = this.width*(1-sideThresholdFraction)
            )
        }
        mainClock.advanceTimeBy(ANIMATION_DURATION.toLong() + 5L) /*add 5s buffer*/
        mainClock.autoAdvance = true
    } catch (e: Exception) {
        ALog.e(TAG, "$methodName()", e)
    }
}

@OptIn(ExperimentalTestApi::class)
fun ComposeTestRule.waitForDataToLoadIntoLazyColumn(
    actionDescription : String,
    TAG : String,
    LOG_ME : Boolean,
    methodName : String,
    listItemTag: String,
) {
    if (LOG_ME) ALog.d(TAG, ".$methodName(): $actionDescription\n listItemTag == $listItemTag")
    this.waitForIdle()
    this.waitUntilAtLeastOneExists(
        matcher = hasTestTag(listItemTag),
//        timeoutMillis = 100000L,
    )
}

// Method swipes on a LazyColumn until it scrolls to the designated item.
fun  ComposeTestRule.clickOnNthItemInLazyColumn(
    actionDescription : String,
    TAG : String,
    LOG_ME : Boolean,
    methodName : String,
    listItemInTestNumber : Int,
    lazyColumnTag : String,
    firstListItemTag: String,
    waitUntilDataLoadsIntoLazyColumn: Boolean = true,
) {
    if(LOG_ME)ALog.d(TAG, ".$methodName(): $actionDescription")
    if(waitUntilDataLoadsIntoLazyColumn) {
        waitForDataToLoadIntoLazyColumn(
            "Waiting for data to load into LazyColumn to proceed into next step: $actionDescription",
            TAG,
            LOG_ME,
            methodName,
            firstListItemTag,
        )
    }

    val lazyColumn = onNodeWithTag(lazyColumnTag)
    val lazyColumnSize = lazyColumn.fetchSemanticsNode().size

    val timeoutMillis = 10000L
    val startTime = System.currentTimeMillis()
    val endTime = startTime + timeoutMillis
    var timeoutReached = false

    var listItemToClick: SemanticsNode? = null
    var listItems: List<SemanticsNode>

    var lastChild: SemanticsNodeInteraction? = null

    // Swipe the LazyColumn until listItemToClick is loaded.
    outerLoop@while(listItemToClick == null && !timeoutReached) {
        listItems = lazyColumn.onChildren().fetchSemanticsNodes()
        for(child in listItems) {
            val childTag = child.config.getOrNull(
                androidx.compose.ui.semantics.SemanticsProperties.TestTag
            )
            if(childTag == listItemInTestNumber.toString()) {
                listItemToClick = child
                break@outerLoop
            }
        }
        waitForIdle()
        mainClock.autoAdvance = false
        lazyColumn.performTouchInput{
            swipeUp(
                startY = lazyColumnSize.width*0.5f,
                durationMillis = 200L,
            )
        }
        mainClock.advanceTimeBy(FrameMetrics.ANIMATION_DURATION.toLong() + 5L) /*add 5s buffer*/
        mainClock.autoAdvance = true
        waitForIdle()

        lastChild = lazyColumn
            .onChildren()
            .onLast()

        timeoutReached = System.currentTimeMillis() >= endTime
        if(LOG_ME)ALog.d(TAG, ".$methodName(): timeoutReached == $timeoutReached")
    }

    if(listItemToClick != null) {
        if(LOG_ME)ALog.d(TAG, ".$methodName(): " +
                "Performing click on chosen item: \n${listItemToClick}")
        onNodeWithTag(listItemInTestNumber.toString()).performClick()
    } else {
        if(LOG_ME)ALog.e(TAG, ".$methodName(): " +
                "Couldn't swipe to the chosen item! \n" +
                "lazyColumn == ${lazyColumn.printToString()}\n" +
                "lastChild == ${lastChild?.printToString()}"
        )
    }

    // These two alternatives can't work since we are dealing with LazyColumn in which items
    // don't exist until they should be shown on the screen.
    // Alternative 1
//    this.onNodeWithTag(lazyColumnTag).performScrollToIndex(itemsNumber)
//    this.onNodeWithTag(lazyColumnTag).onChildAt(itemsNumber).performClick()

    // Alternative 2
//    this.onNodeWithTag(chosenListItemTag).performScrollTo()
//    this.onNodeWithTag(chosenListItemTag).performClick()
}
