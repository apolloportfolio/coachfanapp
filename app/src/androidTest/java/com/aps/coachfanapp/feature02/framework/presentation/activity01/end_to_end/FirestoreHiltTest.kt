package com.aps.coachfanapp.feature02.framework.presentation.activity01.end_to_end

import android.util.Log
import androidx.test.filters.MediumTest
import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner
import com.aps.coachfanapp.BaseInstrumentedTest
import com.aps.coachfanapp.common.util.DateUtil
import com.aps.coachfanapp.common.util.UserUniqueID
import com.aps.coachfanapp.core.business.domain.model.entities.ProjectUser
import com.aps.coachfanapp.core.di.NetworkDataSourcesModule
import com.aps.coachfanapp.core.di.ProductionModule
import com.aps.coachfanapp.core.di.RoomModule
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.firestore.FirebaseFirestore
import dagger.hilt.android.testing.HiltAndroidTest
import dagger.hilt.android.testing.UninstallModules
import kotlinx.coroutines.*
import kotlinx.coroutines.tasks.await
import org.junit.After
import org.junit.Test
import org.junit.runner.RunWith
import java.text.SimpleDateFormat
import java.util.*

private const val TAG = "FirestoreHiltTest"
private const val LOG_ME = true

@MediumTest
@ExperimentalCoroutinesApi
@FlowPreview
@RunWith(AndroidJUnit4ClassRunner::class)
@HiltAndroidTest
@UninstallModules(
    RoomModule::class,
    NetworkDataSourcesModule::class,
    ProductionModule::class,
)
class FirestoreHiltTest : BaseInstrumentedTest() {
//    @Inject
//    lateinit var firestore: FirebaseFirestore         // Injecting firestore with hilt causes stream closure




    private val dateFormatForFirestore = SimpleDateFormat("yyyy-MM-dd hh:mm:ss a", Locale.ENGLISH)
    private val dateFormatForUser = SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH)
    private val dateFormatJustYear = SimpleDateFormat("yyyy", Locale.ENGLISH)
    override var dateUtil: DateUtil = DateUtil(dateFormatForFirestore, dateFormatForUser, dateFormatJustYear)
    private val TEST_USER_REAL_ID = "666"
    private val TEST_USER_REAL_FIRESTORE_DOCUMENT_ID = "666"
    private val TEST_USER_REAL_EMAIL_ADDRESS = "noalternative@vp.pl"
    private val TEST_USER_REAL_PASSWORD = "real67password123"
    private val firebaseAuth: FirebaseAuth = FirebaseAuth.getInstance()

    init {
        dateFormatForFirestore.timeZone = TimeZone.getTimeZone("UTC-7") // match firestore
        dateFormatForUser.timeZone = TimeZone.getTimeZone("UTC-7")
        dateFormatJustYear.timeZone = TimeZone.getTimeZone("UTC-7")

    }

    @After
    override fun runAfterEveryTest() {
        enableInternetConnection()
    }


    override fun injectTest() {
//        (application.appComponent as TestAppComponent).inject(this)
    }

    @Test
    fun firestoreTest1(): Unit = runBlocking {
        val methodName = "firestoreTest1"
        Log.d(TAG, ".$methodName(): Method start: $methodName")
        val user: ProjectUser = generateTestUserWithIncompleteProfile()
        Log.d(TAG, "Inserting new user entity to userNetworkDataSource")
        registerNewUser(user)

        Log.d(TAG, "Logging user in.")
        loginUser(user)

        // Checking if Firestore allows creation of new documents
        Log.d(TAG, "Checking Firestore availability.")
        testFirestoreAvailability1()
        Log.d(TAG, "Firestore is available for usage.")
        Log.d(TAG, ".$methodName(): Method end: $methodName")
    }

    @Test
    fun firestoreTest2(): Unit  = runBlocking {
        test0()
    }


    private suspend fun test0() = withContext(Dispatchers.IO){
//    fun test0() = runBlocking{
        val methodName = "test0"
        Log.d(TAG, ".$methodName(): Method start: $methodName")
        val user : ProjectUser = generateTestUserWithIncompleteProfile()
        Log.d(TAG, "Inserting new user entity to userNetworkDataSource")
        registerNewUser(user)

        Log.d(TAG, "Logging user in.")
        loginUser(user)

        // Checking if Firestore allows creation of new documents
        Log.d(TAG, "Checking Firestore availability.")
        testFirestoreAvailability1()
        Log.d(TAG, "Firestore is available for usage.")
        Log.d(TAG, ".$methodName(): Method end: $methodName")
    }

    fun generateTestUserWithIncompleteProfile() : ProjectUser {
        val user: ProjectUser = generateRandomEntity()
        user.id = getTestUserUniqueID()
        user.emailAddress = TEST_USER_REAL_EMAIL_ADDRESS
        user.password = TEST_USER_REAL_PASSWORD
        return user
    }

    fun getTestUserUniqueID() : UserUniqueID {
        return UserUniqueID(
            TEST_USER_REAL_ID,
            TEST_USER_REAL_FIRESTORE_DOCUMENT_ID
        )
    }

    fun generateRandomEntity(): ProjectUser {
        return createSingleEntity(
            UserUniqueID.generateFakeRandomID(),
            UUID.randomUUID().toString(),
            UUID.randomUUID().toString(),
            UUID.randomUUID().toString(),
            UUID.randomUUID().toString(),
            UUID.randomUUID().toString(),
            UUID.randomUUID().toString(),
            UUID.randomUUID().toString(),
        )
    }

    fun createSingleEntity(
        id: UserUniqueID?,
        emailAddress: String,
        password: String,
        profilePhotoImageURI: String?,
        name: String?,
        surname: String?,
        description : String?,
        city: String?,
        emailAddressVerified: Boolean = false,
        phoneNumberVerified: Boolean = false,
    ): ProjectUser {
        return ProjectUser(
            id,
            dateUtil.getCurrentTimestamp(),
            dateUtil.getCurrentTimestamp(),
            emailAddress,
            password,
            profilePhotoImageURI,
            name,
            surname,
            description,
            city,
            emailAddressVerified,
            phoneNumberVerified,
        )
    }

    suspend fun registerNewUser(entity: ProjectUser): FirebaseUser? {
        val methodName: String = "registerNewUser"
        Log.d(TAG, "Method start: $methodName")
        return try {
            firebaseAuth.createUserWithEmailAndPassword(entity.emailAddress, entity.password).await()
            firebaseAuth.currentUser
        } catch (e: Exception) {
            Log.e(TAG, methodName, e)
            null
        } finally {
            Log.d(TAG, "Method end: $methodName")
        }
    }

    suspend fun loginUser(entity: ProjectUser): FirebaseUser? {
        val methodName: String = "loginUser"
        Log.d(TAG, "Method start: $methodName")
        return try {
            val auth = firebaseAuth
                .signInWithEmailAndPassword(entity.emailAddress, entity.password)
                .addOnSuccessListener { Log.d("$TAG.$methodName: ", "User login success") }
                .addOnFailureListener { Log.w("$TAG.$methodName: ", "User login failure") }
                .await()
            firebaseAuth.currentUser
        } catch (e: Exception) {
            Log.e(TAG, methodName, e)
            null
        } finally {
            Log.d(TAG, "Method end: $methodName")
        }
    }

    private suspend fun testFirestoreAvailability1() {
        val methodName = "testFirestoreAvailability"
        Log.d(TAG, "Method start: $methodName")
        try {
            val testEntity = hashMapOf(
                "test" to "Yay!"
            )

            if(FirebaseAuth.getInstance().currentUser == null) {
                Log.d(TAG, "$methodName(): User is not logged in")
            } else {
                Log.d(TAG, "$methodName(): User is logged in")
            }

            firestore.collection("test")
                .add(testEntity)
                .addOnSuccessListener { documentReference ->
                    Log.d(TAG, "$methodName(): DocumentSnapshot written with ID: ${documentReference.id}")
                }
                .addOnFailureListener { e ->
                    Log.w(TAG, "$methodName(): Error adding document: $e")
                }
                .await()
            Log.d(TAG, "Method end: $methodName")
        } catch (e: Exception) {
            Log.e(TAG, methodName, e)
            return
        } finally {
            Log.d(TAG, "Method end: $methodName")
        }
    }

    private suspend fun testFirestoreAvailability2() {
        val methodName = "testFirestoreAvailability2"
        Log.d(TAG, "Method start: $methodName")
        try {
            val testEntity = hashMapOf(
                "test" to "Yay!"
            )

            if(FirebaseAuth.getInstance().currentUser == null) {
                Log.d(TAG, "$methodName(): User is not logged in")
            } else {
                Log.d(TAG, "$methodName(): User is logged in")
            }

            FirebaseFirestore.getInstance().collection("test")
                .add(testEntity)
                .addOnSuccessListener { documentReference ->
                    Log.d(TAG, "$methodName(): DocumentSnapshot written with ID: ${documentReference.id}")
                }
                .addOnFailureListener { e ->
                    Log.w(TAG, "$methodName(): Error adding document: $e")
                }
                .await()
            Log.d(TAG, "Method end: $methodName")
        } catch (e: Exception) {
            Log.e(TAG, methodName, e)
            return
        } finally {
            Log.d(TAG, "Method end: $methodName")
        }
    }
}