package com.aps.coachfanapp


import android.app.Application
import android.app.Service
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.os.Build
import android.view.View
import android.widget.DatePicker
import androidx.compose.ui.test.junit4.AndroidComposeTestRule
import androidx.compose.ui.test.onRoot
import androidx.compose.ui.test.printToLog
import androidx.test.InstrumentationRegistry.getTargetContext
import androidx.test.core.app.ApplicationProvider
import androidx.test.espresso.Espresso
import androidx.test.espresso.IdlingPolicies
import androidx.test.espresso.IdlingRegistry
import androidx.test.espresso.IdlingResource
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.rule.ActivityTestRule
import androidx.test.rule.GrantPermissionRule
import com.aps.coachfanapp.cfahomescreen.business.data.cache.abstraction.SportsGameCacheDataSource
import com.aps.coachfanapp.cfahomescreen.business.data.cache.abstraction.SportsNewsMessageCacheDataSource
import com.aps.coachfanapp.cfahomescreen.business.data.cache.abstraction.SportsTeamCacheDataSource
import com.aps.coachfanapp.cfahomescreen.business.data.cache.abstraction.SportsTeamPlayerCacheDataSource
import com.aps.coachfanapp.cfahomescreen.business.data.network.abs.SportsGameNetworkDataSource
import com.aps.coachfanapp.cfahomescreen.business.data.network.abs.SportsNewsMessageNetworkDataSource
import com.aps.coachfanapp.cfahomescreen.business.data.network.abs.SportsTeamNetworkDataSource
import com.aps.coachfanapp.cfahomescreen.business.data.network.abs.SportsTeamPlayerNetworkDataSource
import com.aps.coachfanapp.cfahomescreen.business.domain.model.factories.SportsGameFactory
import com.aps.coachfanapp.cfahomescreen.business.domain.model.factories.SportsNewsMessageFactory
import com.aps.coachfanapp.cfahomescreen.business.domain.model.factories.SportsTeamFactory
import com.aps.coachfanapp.cfahomescreen.business.domain.model.factories.SportsTeamPlayerFactory
import com.aps.coachfanapp.cfahomescreen.framework.datasources.cachedatasources.room.daos.SportsGameDao
import com.aps.coachfanapp.cfahomescreen.framework.datasources.cachedatasources.room.daos.SportsNewsMessageDao
import com.aps.coachfanapp.cfahomescreen.framework.datasources.cachedatasources.room.daos.SportsTeamDao
import com.aps.coachfanapp.cfahomescreen.framework.datasources.cachedatasources.room.daos.SportsTeamPlayerDao
import com.aps.coachfanapp.cfahomescreen.framework.datasources.cachedatasources.room.mappers.SportsGameCacheMapper
import com.aps.coachfanapp.cfahomescreen.framework.datasources.cachedatasources.room.mappers.SportsNewsMessageCacheMapper
import com.aps.coachfanapp.cfahomescreen.framework.datasources.cachedatasources.room.mappers.SportsTeamCacheMapper
import com.aps.coachfanapp.cfahomescreen.framework.datasources.cachedatasources.room.mappers.SportsTeamPlayerCacheMapper
import com.aps.coachfanapp.cfahomescreen.framework.presentation.activity01.ActivityHomeScreen
import com.aps.coachfanapp.common.framework.presentation.IntentExtras
import com.aps.coachfanapp.common.util.ALog
import com.aps.coachfanapp.common.util.DateUtil
import com.aps.coachfanapp.common.util.UserUniqueID
import com.aps.coachfanapp.core.business.data.cache.abstraction.*
import com.aps.coachfanapp.core.business.data.network.abstraction.*
import com.aps.coachfanapp.core.business.domain.model.entities.ProjectUser
import com.aps.coachfanapp.core.business.domain.model.factories.*
import com.aps.coachfanapp.core.framework.datasources.*
import com.aps.coachfanapp.core.framework.datasources.cachedatasources.room.daos.*
import com.aps.coachfanapp.core.framework.datasources.cachedatasources.room.mappers.*
import com.aps.coachfanapp.core.util.EspressoIdlingResourceRule
import com.aps.coachfanapp.core.util.ViewShownIdlingResource
import com.aps.coachfanapp.core.util.extensions.generateAnotherTestSportsGameOwnerWithCompleteProfile
import com.aps.coachfanapp.core.util.extensions.generateTestUserWithCompleteProfile
import com.aps.coachfanapp.core.util.extensions.generateTestUserWithIncompleteProfile
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.firestore.FirebaseFirestore
import dagger.hilt.android.testing.HiltAndroidRule
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.runBlocking
import org.hamcrest.Matcher
import org.hamcrest.Matchers
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.rules.TestRule
import java.io.IOException
import java.net.InetSocketAddress
import java.net.Socket
import java.util.concurrent.TimeUnit
import javax.inject.Inject


private const val TAG = "BaseInstrumentedTest"
private const val LOG_ME = true

@ExperimentalCoroutinesApi
@FlowPreview
abstract class BaseInstrumentedTest {

    @Inject
    open lateinit var dateUtil : DateUtil

    //    @Inject
//    lateinit var firestore: FirebaseFirestore       // Injecting firestore with hilt causes stream closure
    open val firestore = FirebaseFirestore.getInstance()

    open lateinit var context: Context
    val application: Application
            = ApplicationProvider.getApplicationContext<Context>() as Application

    var user : ProjectUser? = null
    var sportsGameOwner : ProjectUser? = null


    abstract fun injectTest()


    @get:Rule(order = 0)
    open var hiltRule = HiltAndroidRule(this)

    @get: Rule(order = 1)
    open val espressoIdlingResourceRule = EspressoIdlingResourceRule()
    
    @get:Rule(order = 2)       // Necessary for launching activity with extras
    open var mActivityRule = ActivityTestRule(
        ActivityHomeScreen::class.java, false, false
    )

    @get:Rule(order = 3)
    var permissionRule_ACCESS_FINE_LOCATION = GrantPermissionRule.grant(
        android.Manifest.permission.ACCESS_FINE_LOCATION
    )

    @get:Rule(order = 4)
    var permissionRule_READ_EXTERNAL_STORAGE  = GrantPermissionRule.grant(
        android.Manifest.permission.READ_EXTERNAL_STORAGE
    )

//    @get:Rule(order = 4)
//    var permissionRule_MANAGE_DOCUMENTS = GrantPermissionRule.grant(
//        android.Manifest.permission.MANAGE_DOCUMENTS
//    )


    // User
    @Inject
    open lateinit var userDao: UserDao
    @Inject
    open lateinit var userCacheMapper: UserCacheMapper
    @Inject
    open lateinit var userFactory: UserFactory
    @Inject
    open lateinit var userDataFactory: EntityDataFactoryUser
    @Inject
    open lateinit var userNetworkDataSource: UserNetworkDataSource
    @Inject
    open lateinit var userCacheDataSource: UserCacheDataSource


    // SportsGame
    @Inject
    open lateinit var sportsGameDao: SportsGameDao
    @Inject
    open lateinit var sportsGameCacheMapper: SportsGameCacheMapper
    @Inject
    open lateinit var sportsGameFactory: SportsGameFactory
    @Inject
    open lateinit var sportsGameDataFactory: EntityDataFactorySportsGame
    @Inject
    open lateinit var sportsGameNetworkDataSource: SportsGameNetworkDataSource
    @Inject
    open lateinit var sportsGameCacheDataSource: SportsGameCacheDataSource


    // SportsTeamPlayer
    @Inject
    open lateinit var sportsTeamPlayerDao: SportsTeamPlayerDao
    @Inject
    open lateinit var sportsTeamPlayerCacheMapper: SportsTeamPlayerCacheMapper
    @Inject
    open lateinit var sportsTeamPlayerFactory: SportsTeamPlayerFactory
    @Inject
    open lateinit var sportsTeamPlayerDataFactory: EntityDataFactorySportsTeamPlayer
    @Inject
    open lateinit var sportsTeamPlayerNetworkDataSource: SportsTeamPlayerNetworkDataSource
    @Inject
    open lateinit var sportsTeamPlayerCacheDataSource: SportsTeamPlayerCacheDataSource


    // SportsTeam
    @Inject
    open lateinit var sportsTeamDao: SportsTeamDao
    @Inject
    open lateinit var sportsTeamCacheMapper: SportsTeamCacheMapper
    @Inject
    open lateinit var sportsTeamFactory: SportsTeamFactory
    @Inject
    open lateinit var sportsTeamDataFactory: EntityDataFactorySportsTeam
    @Inject
    open lateinit var sportsTeamNetworkDataSource: SportsTeamNetworkDataSource
    @Inject
    open lateinit var sportsTeamCacheDataSource: SportsTeamCacheDataSource

    // SportsNewsMessage
    @Inject
    lateinit var sportsNewsMessageDao: SportsNewsMessageDao
    @Inject
    lateinit var sportsNewsMessageCacheMapper: SportsNewsMessageCacheMapper
    @Inject
    lateinit var sportsNewsMessageFactory: SportsNewsMessageFactory
    @Inject
    lateinit var sportsNewsMessageDataFactory: EntityDataFactorySportsNewsMessage
    @Inject
    lateinit var sportsNewsMessageNetworkDataSource: SportsNewsMessageNetworkDataSource
    @Inject
    lateinit var sportsNewsMessageCacheDataSource: SportsNewsMessageCacheDataSource

    init {

    }

    // wait for a certain view to be shown.
    fun waitViewShown(matcher: Matcher<View>) {
        val idlingResource: IdlingResource = ViewShownIdlingResource(matcher,
            ViewMatchers.isDisplayed()
        )
        try {
            IdlingRegistry.getInstance().register(idlingResource)
            Espresso.onView(ViewMatchers.withId(0)).check(ViewAssertions.doesNotExist())
        } finally {
            IdlingRegistry.getInstance().unregister(idlingResource)
        }
    }

    // wait for a date picker view to be shown.
    fun waitViewShownDatePicker() {
        waitViewShown(ViewMatchers.withClassName(Matchers.equalTo(DatePicker::class.java.name)))
    }

    @Before
    open fun runBeforeEveryTest() {
        if(LOG_ME)ALog.d(TAG, "runBeforeEveryTest(): " +
                "")
        IdlingPolicies.setMasterPolicyTimeout(2, TimeUnit.MINUTES);
        IdlingPolicies.setIdlingResourceTimeout(2, TimeUnit.MINUTES);
        hiltRule.inject()
        injectTest()
        context = InstrumentationRegistry.getInstrumentation().targetContext
        grantLocationPermission()
        grantOpenDocumentPermission()
    }

    @After
    open fun runAfterEveryTest() {
        enableInternetConnection()
        user = null
    }

    open fun prepareDataSetWithUserWhoHasIncompleteProfile() = runBlocking {
        val methodName = "prepareDataSetWithUserWhoHasIncompleteProfile()"
        ALog.d(TAG, "Method start: $methodName")
        try {
            ALog.d(TAG, "Creating new user entity")
            user = userFactory.generateTestUserWithIncompleteProfile()
            sportsGameOwner = userFactory.generateAnotherTestSportsGameOwnerWithCompleteProfile()

            prepareDataSetWithUser(user!!, sportsGameOwner!!)
        } catch (e: Exception) {
            ALog.e(TAG, methodName, e)
        } finally {
            ALog.d(TAG, "Method end: $methodName")
        }
    }

    open fun prepareDataSetWithUserWhoHasCompleteProfile() = runBlocking {
        val methodName = "prepareDataSetWithUserWhoHasCompleteProfile()"
        ALog.d(TAG, "Method start: $methodName")
        try {
            ALog.d(TAG, "Creating new user entity")
            user = userFactory.generateTestUserWithCompleteProfile()
            sportsGameOwner = userFactory.generateAnotherTestSportsGameOwnerWithCompleteProfile()

            if(LOG_ME)ALog.d(
                TAG, ".prepareDataSetWithUserWhoHasCompleteProfile(): \n" +
                                    "Before register: user == $user\n" +
                    "Before register: sportsGameOwner == $sportsGameOwner")
            prepareDataSetWithUser(user!!, sportsGameOwner!!)
            if(LOG_ME)ALog.d(
                TAG, ".prepareDataSetWithUserWhoHasCompleteProfile(): \n" +
                    "After register: user == $user\n" +
                    "After register: sportsGameOwner == $sportsGameOwner")
        } catch (e: Exception) {
            ALog.e(TAG, methodName, e)
        } finally {
            ALog.d(TAG, "Method end: $methodName")
        }
    }

    private suspend fun prepareDataSetWithUser(
        user: ProjectUser,
        sportsGameOwner: ProjectUser,
    ) {
        val methodName: String = "prepareDataSetWithUser"
        if (LOG_ME) ALog.d(TAG, "Method start: $methodName")
        try {
            ALog.d(TAG, "Checking Internet connection.")
            checkInternetConnection()
            if(LOG_ME)ALog.d(TAG, "$methodName(): Registering sportsGame owner in Firebase")
            val registeredSportsGameOwner = registerAndLoginUserInFirebase(sportsGameOwner)
            if(LOG_ME)ALog.d(TAG, "$methodName(): Inserting sportsGame owner into databases.")
            insertUserIntoDatabases(registeredSportsGameOwner)
            if(LOG_ME)ALog.d(TAG, "$methodName(): Logging sportsGame owner out.")
            this.sportsGameOwner = registeredSportsGameOwner
            logoutUser(sportsGameOwner)

            if(LOG_ME)ALog.d(TAG, "$methodName(): Registering user in Firebase.")
            val registeredUser = registerAndLoginUserInFirebase(user)
//        clearUsersInDatabases()         // Exceptions here with Firestore security rules enabled
            if(LOG_ME)ALog.d(TAG, "$methodName(): Inserting user into databases.")
            insertUserIntoDatabases(registeredUser)
            if(LOG_ME)ALog.d(TAG, "$methodName(): Populating databases with test data.")
            this.user = registeredUser
            populateDatabases(sportsGameOwner)
        } catch (e: Exception) {
            ALog.e(TAG, methodName, e)
        } finally {
            if (LOG_ME) ALog.d(TAG, "Method end: $methodName")
        }
    }

    suspend fun registerAndLoginUserInFirebase(user: ProjectUser): ProjectUser {
        val methodName = "registerAndLoginUserInFirebase"
        if (LOG_ME) ALog.d(TAG, "Method start: $methodName")
        return try {
            // Logging old user in
            ALog.d(TAG, "Logging user in.")
            var loggedInOldUser: FirebaseUser? = null
            try {
                loggedInOldUser = userNetworkDataSource.loginUser(user)
            }catch (e: Exception){}
            if(loggedInOldUser != null) {
                // Deleting old user account in Firebase
                ALog.d(TAG, "Deleting old user account")
                userNetworkDataSource.deleteAccount(user)
            } else {
                if(LOG_ME)ALog.d(TAG, ".registerAndLoginUserInFirebase(): " +
                                        "loggedInOldUser == null")
            }

            // Registering user in Firebase
            ALog.d(TAG, "Registering user in Firestore")
            var firestoreUser: FirebaseUser = userNetworkDataSource.registerNewUser(user)!!
            if(LOG_ME)ALog.d(
                TAG, "$methodName(): " +
                    "before login: firestoreUser.uid == ${firestoreUser.uid}")

            // Logging user in
            ALog.d(TAG, "Logging user in.")
            firestoreUser = userNetworkDataSource.loginUser(user)!!
            if(LOG_ME)ALog.d(
                TAG, "$methodName(): " +
                    "after login: firestoreUser.uid == ${firestoreUser.uid}")
            user.id = UserUniqueID(firestoreUser.uid, firestoreUser.uid)
            if(LOG_ME)ALog.d(
                TAG, "$methodName(): " +
                    "after login: user.id == ${user.id}")

            user
        } catch (e: Exception) {
            ALog.e(TAG, methodName, e)
            ALog.e(TAG, "$methodName(): user == $user")
            user
        } finally {
            if (LOG_ME) ALog.d(TAG, "Method end: $methodName")
        }
    }

    suspend fun logoutUser(user: ProjectUser) {
        val methodName = "signOutUser"
        if (LOG_ME) ALog.d(TAG, "Method start: $methodName")
        try {
            userNetworkDataSource.logoutUser(user)
        } catch (e: Exception) {
            ALog.e(TAG, methodName, e)
        } finally {
            if (LOG_ME) ALog.d(TAG, "Method end: $methodName")
        }
    }

    suspend fun clearUsersInDatabases() {
        val methodName: String = "clearUsersInDatabases"
        if (LOG_ME) ALog.d(TAG, "Method start: $methodName")
        try {
            // Deleting old User entities in databases
            ALog.d(TAG, "Deleting all entities in userDao")
            userDao.deleteAllEntities()
            ALog.d(TAG, "Deleting all entities in userNetworkDataSource")
            userNetworkDataSource.deleteAllEntities()

        } catch (e: Exception) {
            ALog.e(TAG, methodName, e)
        } finally {
            if (LOG_ME) ALog.d(TAG, "Method end: $methodName")
        }
    }

    suspend fun insertUserIntoDatabases(user: ProjectUser) {
        val methodName: String = "insertUserIntoDatabases"
        if (LOG_ME) ALog.d(TAG, "Method start: $methodName")
        try {
            // Adding User entity to databases
            ALog.d(TAG, "Inserting new user entity to userCacheDataSource")
            userCacheDataSource.insertOrUpdateEntity(user)
            ALog.d(TAG, "Inserting new user entity to userNetworkDataSource")
            userNetworkDataSource.insertOrUpdateEntity(user)
        } catch (e: Exception) {
            ALog.e(TAG, methodName, e)
        } finally {
            if (LOG_ME) ALog.d(TAG, "Method end: $methodName")
        }
    }

    suspend fun clearDatabases(user: ProjectUser) {
        val methodName = "clearDatabases"
        if (LOG_ME) ALog.d(TAG, "Method start: $methodName")
        try {
            // SportsGame
            ALog.d(TAG, "Deleting all entities in sportsGameDao")
            sportsGameDao.deleteAllEntities()
            ALog.d(TAG, "Deleting all entities in sportsGameNetworkDataSource")
            sportsGameNetworkDataSource.deleteAllEntities()

            // SportsTeam
            ALog.d(TAG, "Deleting all entities in sportsTeamDao")
            sportsTeamDao.deleteAllEntities()
            ALog.d(TAG, "Deleting all entities in sportsTeamNetworkDataSource")
            sportsTeamNetworkDataSource.deleteAllEntities()

            // SportsNewsMessage
            ALog.d(TAG, "Deleting all entities in sportsNewsMessageDao")
            sportsNewsMessageDao.deleteAllEntities()
            ALog.d(TAG, "Deleting all entities in sportsNewsMessageNetworkDataSource")
            sportsNewsMessageNetworkDataSource.deleteAllEntities()

            //SportsTeamPlayer
            ALog.d(TAG, "Deleting all entities in sportsTeamPlayerDao")
            sportsTeamPlayerDao.deleteAllEntities()
            ALog.d(TAG, "Deleting all entities in sportsTeamPlayerNetworkDataSource")
            sportsTeamPlayerNetworkDataSource.deleteAllEntities()
        } catch (e: Exception) {
            ALog.e(TAG, methodName, e)
        } finally {
            if (LOG_ME) ALog.d(TAG, "Method end: $methodName")
        }
    }

    private fun populateDatabases(sportsGameOwner: ProjectUser) {
        val methodName = "populateDatabases"
        if (LOG_ME) ALog.d(TAG, "Method start: $methodName")
        try {
            // SportsGame
            ALog.d(TAG, "Inserting sportsGame test data")
            insertSportsGameTestData(sportsGameOwner)

            // SportsTeam
            ALog.d(TAG, "Inserting sportsGame details test data")
            insertSportsTeamTestData()

            // SportsNewsMessage
            insertSportsNewsMessageTestData()

            // SportsTeamPlayer
            insertSportsTeamPlayerTestData()
        } catch (e: Exception) {
            ALog.e(TAG, methodName, e)
        } finally {
            if (LOG_ME) ALog.d(TAG, "Method end: $methodName")
        }
    }

    open fun insertSportsGameTestData(sportsGameOwner: ProjectUser) = runBlocking{
        this.javaClass.classLoader?.let { classLoader ->
            sportsGameDataFactory = EntityDataFactorySportsGame(application)
        }
        val sportsGames = sportsGameDataFactory.produceListOfEntities()
        if(sportsGameOwner.id != null) {
            for(sportsGame in sportsGames) {
                sportsGame.ownerID = sportsGameOwner.id
            }
        }

        sportsGameCacheDataSource.insertEntities(sportsGames)
        for(sportsGame in sportsGames) {
            sportsGameNetworkDataSource.insertOrUpdateEntity(sportsGame)
        }
    }

    open fun insertSportsTeamTestData() = runBlocking{
        val methodName: String = "insertSportsTeamTestData"
        if (LOG_ME) ALog.d(TAG, "Method start: $methodName")
        try {
            this.javaClass.classLoader?.let { classLoader ->
                sportsTeamDataFactory = EntityDataFactorySportsTeam(application)
            }
            val sportsTeams = sportsTeamDataFactory.produceListOfEntities()
            sportsTeamCacheDataSource.insertEntities(sportsTeams)
            sportsTeamNetworkDataSource.insertOrUpdateEntities(sportsTeams)
        } catch (e: Exception) {
            ALog.e(TAG, methodName, e)
        } finally {
            if (LOG_ME) ALog.d(TAG, "Method end: $methodName")
        }        
    }

    open fun insertSportsTeamPlayerTestData() = runBlocking{
        val methodName: String = "insertSportsTeamPlayerTestData"
        if (LOG_ME) ALog.d(TAG, "Method start: $methodName")
        if (LOG_ME) ALog.d(TAG, "Started insertSportsTeamPlayerTestData")
        try {
            if (LOG_ME) ALog.d(TAG, "Started insertSportsTeamPlayerTestData from BaseInstrumentedTest")
            this.javaClass.classLoader?.let { classLoader ->
                sportsTeamPlayerDataFactory = EntityDataFactorySportsTeamPlayer(application)
            }
            val sportsTeamPlayerList = sportsTeamPlayerDataFactory.produceListOfEntities()

            sportsTeamPlayerCacheDataSource.insertEntities(sportsTeamPlayerList)
            sportsTeamPlayerNetworkDataSource.insertOrUpdateEntities(sportsTeamPlayerList)
        } catch (e: Exception) {
            ALog.e(TAG, "$methodName(): Exception occured while inserting rental offer test data.")
            ALog.e(TAG, methodName, e)
        } finally {
            if (LOG_ME) ALog.d(
                TAG, "Method end: $methodName")
        }
    }

    open fun insertSportsNewsMessageTestData() = runBlocking{
        this.javaClass.classLoader?.let { classLoader ->
            sportsNewsMessageDataFactory = EntityDataFactorySportsNewsMessage(application)
        }
        val sportsNewsMessages = sportsNewsMessageDataFactory.produceListOfEntities()
        sportsNewsMessageCacheDataSource.insertEntities(sportsNewsMessages)
    }

    open fun disableInternetConnection() {
        InstrumentationRegistry.getInstrumentation().uiAutomation.executeShellCommand("svc wifi disable")
        InstrumentationRegistry.getInstrumentation().uiAutomation.executeShellCommand("svc data disable")
    }

    open fun enableInternetConnection() {
        InstrumentationRegistry.getInstrumentation().uiAutomation.executeShellCommand("svc wifi enable")
        InstrumentationRegistry.getInstrumentation().uiAutomation.executeShellCommand("svc data enable")
    }

    open fun isConnected(context: Context): Boolean {
        val connectivityManager =
            context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetworkInfo = connectivityManager.activeNetworkInfo
        return activeNetworkInfo != null && activeNetworkInfo.isConnected
    }

    fun checkInternetConnection() : Boolean {
        return try {
            val sock = Socket()
            sock.connect(InetSocketAddress("8.8.8.8", 53), 1500)
            sock.close()
            if(LOG_ME)ALog.d(TAG, ".checkInternetConnection(): Internet is available.")
            true
        } catch (e: IOException) {
            if(LOG_ME)ALog.d(TAG, ".checkInternetConnection(): Internet is not available.")
            false
        }
    }

    open fun checkInternetConnection(context:Context) : Boolean {
        var connectivity : ConnectivityManager?
        var info : NetworkInfo?
        connectivity = context.getSystemService(Service.CONNECTIVITY_SERVICE)
                as ConnectivityManager
        if ( connectivity != null) {
            info = connectivity.activeNetworkInfo
            if (info != null) {
                if (info.state == NetworkInfo.State.CONNECTED) {
                    if(LOG_ME)ALog.d(TAG, ".checkInternetConnection(): Device has Internet connection.")
                    return true
                }
            } else {
                if(LOG_ME)ALog.d(TAG, ".checkInternetConnection(): Device doesn't have Internet connection.")
                return false
            }
        }
        if(LOG_ME)ALog.d(TAG, ".checkInternetConnection(): Device doesn't have Internet connection.")
        return false
    }

    open fun grantLocationPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            with(InstrumentationRegistry.getInstrumentation().uiAutomation) {
                executeShellCommand("appops set " + context.packageName + " android:mock_location allow")
                Thread.sleep(1000)
            }
        }
    }

    open fun grantPhonePermission() {
        // In M+, trying to call a number will trigger a runtime dialog. Make sure
        // the permission is granted before running this test.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            InstrumentationRegistry.getInstrumentation().uiAutomation.executeShellCommand(
                "pm grant " + getTargetContext().packageName
                        + " android.permission.CALL_PHONE"
            )
        }
    }

    open fun grantOpenDocumentPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            InstrumentationRegistry.getInstrumentation().uiAutomation.executeShellCommand(
                "pm grant " + getTargetContext().packageName
                        + " android.permission.MANAGE_DOCUMENTS"
            )
            Thread.sleep(1000)
        }
    }

        open fun startTestActivityWithUserWhoHasIncompleteProfile() : ActivityHomeScreen {
//        val scenario = launchActivity<ActivityHomeScreen>()       // Launches activity without extras
        if(user == null)user = userFactory.generateTestUserWithIncompleteProfile()
        val intent = Intent()
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        intent.putExtra(IntentExtras.APP_USER, user)
        return mActivityRule.launchActivity(intent)
    }

    open fun startTestActivityWithUserWhoHasCompleteProfile() : ActivityHomeScreen {
//        val scenario = launchActivity<ActivityHomeScreen>()       // Launches activity without extras
        if(user == null)user = userFactory.generateTestUserWithCompleteProfile()
        val intent = Intent()
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        intent.putExtra(IntentExtras.APP_USER, user)
        return mActivityRule.launchActivity(intent)
    }

    fun <R : TestRule, A : androidx.activity.ComponentActivity> pauseTestHere(
        tag: String,
        methodName: String,
        composeTestRule: AndroidComposeTestRule<R, A>?,
        printComposeTree: Boolean = true,
    ) {
        if(composeTestRule != null) {
            if(LOG_ME) ALog.w(tag, ".$methodName(): " +
                    "GotHere: composable test paused using root.")
//            if(printComposeTree) composeTestRule.onRoot(useUnmergedTree = true).printToLog(tag)
            if(printComposeTree) composeTestRule.onRoot().printToLog(tag)
            for(i in 0..Long.MAX_VALUE) {
                try {
                    composeTestRule.onRoot().assertExists()
                } catch(e: java.lang.AssertionError) {
//                    if(LOG_ME)ALog.e(TAG, ".$methodName(): " +
//                            "AssertionError")
                } catch (e: java.lang.IllegalStateException) {
//                    if(LOG_ME)ALog.e(TAG, ".$methodName(): " +
//                                            "IllegalStateException: ", e)
                } catch (e: Exception) {
//                    if(LOG_ME)ALog.e(TAG, ".$methodName(): " +
//                            "Exception: ", e)
                }
            }
        }
        if(LOG_ME) ALog.w(tag, ".$methodName(): GotHere: " +
                "non composable test paused by sleeping the thread.")
        Thread.sleep(Long.MAX_VALUE)
    }
}