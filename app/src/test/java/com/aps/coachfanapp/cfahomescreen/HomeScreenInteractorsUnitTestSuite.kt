package com.aps.coachfanapp.cfahomescreen

import com.aps.coachfanapp.common.util.ALog
import org.junit.After
import org.junit.Before
import org.junit.runner.RunWith
import org.junit.runners.Suite

private const val TAG = "HomeScreenInteractorsUnitTestSuite"
private const val LOG_ME = true

@RunWith(Suite::class)
@Suite.SuiteClasses(
    GetSportsGamesAroundUserUnitTest::class,
//    SearchEntities1UnitTest::class,
//    GetEntities2UnitTest::class,
//    GetEntities3UnitTest::class,
//    GetUsersRatingUnitTest::class,
//    LogoutUserUnitTest::class,
//    CheckGooglePayAvailabilityUnitTest::class,
//    DownloadExchangeRatesUnitTest::class,
//    GetMerchantNameUnitTest::class,
//    GetGatewayNameAndMerchantIDUnitTest::class,
)
class HomeScreenInteractorsUnitTestSuite {

    @Before
    fun setUpTestSuite() {
        // Perform setup tasks for the entire suite
        if (LOG_ME) ALog.d(TAG, ".setUpTestSuite(): Setting up the test suite...")
    }

    @After
    fun tearDownTestSuite() {
        if (LOG_ME) ALog.d(TAG, ".setUpTestSuite(): Tearing down the test suite...")
    }
}