package com.aps.coachfanapp.cfahomescreen

import com.aps.coachfanapp.common.util.ALog
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.After
import org.junit.Before
import org.junit.runner.RunWith
import org.junit.runners.Suite

private const val TAG = "CFAHomeScreenUnitTestSuite"
private const val LOG_ME = true

@OptIn(ExperimentalCoroutinesApi::class)
@RunWith(Suite::class)
@Suite.SuiteClasses(
    HomeScreenViewModelComposeTest::class,
    HomeScreenInteractorsUnitTestSuite::class
)
class CFAHomeScreenUnitTestSuite {

    @Before
    fun setUpTestSuite() {
        // Perform setup tasks for the entire suite
        if (LOG_ME) ALog.d(TAG, ".setUpTestSuite(): Setting up the test suite...")
    }

    @After
    fun tearDownTestSuite() {
        if (LOG_ME) ALog.d(TAG, ".setUpTestSuite(): Tearing down the test suite...")
    }
}