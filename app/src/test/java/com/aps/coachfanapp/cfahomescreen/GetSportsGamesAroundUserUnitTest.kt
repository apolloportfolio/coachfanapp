package com.aps.coachfanapp.cfahomescreen

import android.content.Context
import android.location.Location
import android.net.ConnectivityManager
import android.net.NetworkInfo
import com.aps.coachfanapp.cfahomescreen.business.data.cache.abstraction.SportsGameCacheDataSource
import com.aps.coachfanapp.cfahomescreen.business.data.network.abs.SportsGameNetworkDataSource
import com.aps.coachfanapp.cfahomescreen.business.domain.model.entities.SportsGame
import com.aps.coachfanapp.cfahomescreen.business.domain.model.factories.SportsGameFactory
import com.aps.coachfanapp.cfahomescreen.business.interactors.abs.GetSportsGamesAroundUser
import com.aps.coachfanapp.cfahomescreen.business.interactors.impl.GetSportsGamesAroundUserImpl
import com.aps.coachfanapp.cfahomescreen.framework.presentation.activity01.state.HomeScreenStateEvent
import com.aps.coachfanapp.cfahomescreen.framework.presentation.activity01.state.HomeScreenViewState
import com.aps.coachfanapp.common.business.data.network.ApiResponseHandler
import com.aps.coachfanapp.common.business.domain.state.DataState
import com.aps.coachfanapp.common.business.domain.state.StateEvent
import com.aps.coachfanapp.common.util.DeviceLocation
import com.aps.coachfanapp.core.business.domain.model.entities.ProjectUser
import com.aps.coachfanapp.core.di.DependencyContainer
import com.aps.coachfanapp.core.util.ApplicationContextProvider
import com.aps.coachfanapp.core.util.ConnectivityHelper
import kotlinx.coroutines.flow.FlowCollector
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.jupiter.api.Assertions
import org.mockito.Mock
import org.mockito.Mockito.doReturn
import org.mockito.Mockito.mock
import org.mockito.Mockito.`when`
import org.mockito.kotlin.any
import java.io.File

private const val TAG = "GetSportsGamesAroundUserUnitTest"

class GetSportsGamesAroundUserUnitTest {

    // System under test
    lateinit var getSportsGamesAroundUser: GetSportsGamesAroundUser

    // Dependencies
    lateinit var dependencyContainer: DependencyContainer
    private lateinit var sportsGameCacheDataSource: SportsGameCacheDataSource
    lateinit var sportsGameNetworkDataSource: SportsGameNetworkDataSource
    lateinit var entityFactory: SportsGameFactory


    lateinit var location: Location

    lateinit var stateEvent: StateEvent

    lateinit var returnViewState: HomeScreenViewState<ProjectUser>

    lateinit var updateReturnViewState: (HomeScreenViewState<ProjectUser>, List<SportsGame>?) -> HomeScreenViewState<ProjectUser>

    lateinit var onErrorAction: () -> Unit

    lateinit var internalDirectory: File

    lateinit var stateEventErrorMessage: String


    // Mocks for dependencies
    @Mock
    lateinit var mockedSportsGameCacheDataSource: SportsGameCacheDataSource

    @Mock
    lateinit var mockedSportsGameNetworkDataSource: SportsGameNetworkDataSource

    @Mock
    lateinit var mockitoGetSportsGamesAroundUser: GetSportsGamesAroundUser

    lateinit var sportsGamesAroundUser: List<SportsGame>

    @Mock
    lateinit var mockContext: Context

    @Mock
    lateinit var mockApiResponseHandler: ApiResponseHandler<
            HomeScreenViewState<ProjectUser>,
            List<SportsGame>
        >

    @Mock
    lateinit var mockConnectivityManager: ConnectivityManager
    
    @Mock
    lateinit var mockApplicationContextProvider : ApplicationContextProvider
    
    @Mock
    lateinit var mockConnectivityHelper : ConnectivityHelper

    @Mock
    lateinit var mockNetworkInfo: NetworkInfo

    init {
        // Initialize dependencies
        dependencyContainer = DependencyContainer()
        dependencyContainer.build()
        entityFactory = dependencyContainer.sportsGameFactory
        sportsGameCacheDataSource = dependencyContainer.sportsGameCacheDataSource
        sportsGameNetworkDataSource = dependencyContainer.sportsGameNetworkDataSource

        sportsGamesAroundUser = SportsGameFactory.createPreviewEntitiesList()

        mockNetworkInfo = mock(NetworkInfo::class.java)
        `when`(mockNetworkInfo.isConnected).thenReturn(true)

        mockConnectivityManager = mock(ConnectivityManager::class.java)
        `when`(mockConnectivityManager.activeNetworkInfo).thenReturn(mockNetworkInfo)
        
        mockContext = mock(Context::class.java)
        `when`(mockContext.getSystemService(Context.CONNECTIVITY_SERVICE)).thenReturn(mockConnectivityManager)
        
        mockApplicationContextProvider = mock(ApplicationContextProvider::class.java)
        `when`(mockApplicationContextProvider.applicationContext(
            tag = TAG
        )).thenReturn(mockContext)

        mockConnectivityHelper = mock(ConnectivityHelper::class.java)
        `when`(mockConnectivityHelper.getContext()).thenReturn(mockContext)
        `when`(mockConnectivityHelper.internetConnectionIsAvailable(
            context = any()
        )).thenReturn(true)
        
        mockApiResponseHandler = mock(
            ApiResponseHandler::class.java
        ) as ApiResponseHandler<HomeScreenViewState<ProjectUser>, List<SportsGame>>
//        `when`(mockApiResponseHandler.internetConnectionIsAvailable(mockContext)).thenReturn(true)

        mockedSportsGameCacheDataSource = mock(SportsGameCacheDataSource::class.java)
        mockedSportsGameNetworkDataSource = mock(SportsGameNetworkDataSource::class.java)
        runBlocking {
            doReturn(sportsGamesAroundUser).
            `when`(mockedSportsGameNetworkDataSource).searchEntities(any())
        }

        location = DeviceLocation.PreviewDeviceLocation
        stateEventErrorMessage = "Error while getting items around user."
        internalDirectory = File("internalDirectory")
        stateEvent = HomeScreenStateEvent.GetSportsGamesAroundUserStateEvent(internalDirectory)
        returnViewState = HomeScreenViewState()
        updateReturnViewState = {_, _ -> returnViewState}
        onErrorAction = { println("onErrorAction") }



        // Initialize system under test
        getSportsGamesAroundUser = GetSportsGamesAroundUserImpl(
            sportsGameCacheDataSource,
            sportsGameNetworkDataSource,
            entityFactory,
            mockApplicationContextProvider,
            mockConnectivityHelper,
        )

        mockitoGetSportsGamesAroundUser = GetSportsGamesAroundUserImpl(
            mockedSportsGameCacheDataSource,
            mockedSportsGameNetworkDataSource,
            entityFactory,
            mockApplicationContextProvider,
            mockConnectivityHelper,
        )
    }


    @Before
    fun setup() {

    }


    @After
    fun tearDown() {

    }

    @Test
    fun `mockito test success of getSportsGamesAroundUser`() = runBlocking {

        mockitoGetSportsGamesAroundUser.getSportsGamesAroundUser(
            location,
            stateEvent,
            returnViewState,
            updateReturnViewState,
            onErrorAction,
        ).collect(object: FlowCollector<DataState<HomeScreenViewState<ProjectUser>>?> {
            override suspend fun emit(value: DataState<HomeScreenViewState<ProjectUser>>?) {
                Assertions.assertEquals(
                    GetSportsGamesAroundUserImpl.GET_ENTITIES_SUCCESS,
                    value?.stateMessage?.response?.message
                )
            }
        })

    }
}