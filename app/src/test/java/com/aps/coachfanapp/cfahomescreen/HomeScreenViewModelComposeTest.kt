package com.aps.coachfanapp.cfahomescreen

import android.content.Context
import android.location.Location
import android.net.ConnectivityManager
import android.net.NetworkInfo
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.aps.coachfanapp.cfahomescreen.business.data.cache.abstraction.SportsGameCacheDataSource
import com.aps.coachfanapp.cfahomescreen.business.data.network.abs.SportsGameNetworkDataSource
import com.aps.coachfanapp.cfahomescreen.business.domain.model.entities.SportsGame
import com.aps.coachfanapp.cfahomescreen.business.domain.model.factories.SportsGameFactory
import com.aps.coachfanapp.cfahomescreen.business.interactors.HomeScreenInteractors
import com.aps.coachfanapp.cfahomescreen.business.interactors.abs.CheckGooglePayAvailability
import com.aps.coachfanapp.cfahomescreen.business.interactors.abs.DownloadExchangeRates
import com.aps.coachfanapp.cfahomescreen.business.interactors.abs.GetGatewayNameAndMerchantID
import com.aps.coachfanapp.cfahomescreen.business.interactors.abs.GetMerchantName
import com.aps.coachfanapp.cfahomescreen.business.interactors.abs.GetSportsGamesAroundUser
import com.aps.coachfanapp.cfahomescreen.business.interactors.abs.GetSportsTeamPlayers
import com.aps.coachfanapp.cfahomescreen.business.interactors.abs.GetSportsTeams
import com.aps.coachfanapp.cfahomescreen.business.interactors.abs.GetUsersRating
import com.aps.coachfanapp.cfahomescreen.business.interactors.abs.LogoutUser
import com.aps.coachfanapp.cfahomescreen.business.interactors.abs.SearchSportsGames
import com.aps.coachfanapp.cfahomescreen.business.interactors.impl.GetSportsGamesAroundUserImpl
import com.aps.coachfanapp.cfahomescreen.business.interactors.impl.SearchSportsGamesImpl
import com.aps.coachfanapp.cfahomescreen.framework.presentation.activity01.HomeScreenViewModelCompose
import com.aps.coachfanapp.cfahomescreen.framework.presentation.activity01.state.HomeScreenStateEvent
import com.aps.coachfanapp.cfahomescreen.framework.presentation.activity01.state.HomeScreenViewState
import com.aps.coachfanapp.common.business.data.network.ApiResponseHandler
import com.aps.coachfanapp.common.business.interactors.abstraction.DoNothingAtAll
import com.aps.coachfanapp.common.util.DateUtil
import com.aps.coachfanapp.common.util.DeviceLocation
import com.aps.coachfanapp.core.business.data.cache.implementation.UserCacheDataSourceImpl
import com.aps.coachfanapp.core.business.data.network.abstraction.UserNetworkDataSource
import com.aps.coachfanapp.core.business.domain.model.entities.ProjectUser
import com.aps.coachfanapp.core.business.domain.model.factories.UserFactory
import com.aps.coachfanapp.core.di.DependencyContainer
import com.aps.coachfanapp.core.util.ApplicationContextProvider
import com.aps.coachfanapp.core.util.ConnectivityHelper
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.Mockito.mock
import org.mockito.Mockito.`when`
import org.mockito.kotlin.any
import java.io.File

@ExperimentalCoroutinesApi
class HomeScreenViewModelComposeTest {

    @get:Rule
    val testInstantTaskExecutorRule: TestRule = InstantTaskExecutorRule()

    @get:Rule
    val testCoroutineRule = TestCoroutineRule()

    // System under test
    @OptIn(FlowPreview::class)
    lateinit var viewModelCompose: HomeScreenViewModelCompose

    lateinit var dependencyContainer: DependencyContainer
    lateinit var homeScreenInteractors: HomeScreenInteractors<
            ProjectUser,
            UserCacheDataSourceImpl,
            UserNetworkDataSource,
            HomeScreenViewState<ProjectUser>
            >
    lateinit var dateUtil: DateUtil
    lateinit var userFactory: UserFactory
    lateinit var sportsGameFactory: SportsGameFactory
    lateinit var location: Location
    lateinit var stateEventGetSportsGamesAroundUser: HomeScreenStateEvent.GetSportsGamesAroundUserStateEvent
    lateinit var returnViewState: HomeScreenViewState<ProjectUser>
    lateinit var updateReturnViewState: (HomeScreenViewState<ProjectUser>, List<SportsGame>?) -> HomeScreenViewState<ProjectUser>
    lateinit var onErrorAction: () -> Unit
    lateinit var internalDirectory: File
    lateinit var stateEventErrorMessage: String

    @Mock
    lateinit var getSportsGamesAroundUser: GetSportsGamesAroundUser

    @Mock
    lateinit var searchSportsGames: SearchSportsGames

    @Mock
    lateinit var getSportsTeamPlayers: GetSportsTeamPlayers

    @Mock
    lateinit var getSportsTeams: GetSportsTeams

    @Mock
    lateinit var getUsersRating: GetUsersRating

    @Mock
    lateinit var logout: LogoutUser

    @Mock
    lateinit var checkGooglePayAvailability: CheckGooglePayAvailability

    @Mock
    lateinit var downloadExchangeRates: DownloadExchangeRates
            
    @Mock
    private lateinit var getMerchantName: GetMerchantName
    
    lateinit var getGatewayNameAndMerchantID: GetGatewayNameAndMerchantID
            
    lateinit var doNothingAtAll: DoNothingAtAll<
            ProjectUser,
            UserCacheDataSourceImpl,
            UserNetworkDataSource,
            HomeScreenViewState<ProjectUser>>



    @Mock
    lateinit var mockedSportsGameCacheDataSource: SportsGameCacheDataSource

    @Mock
    lateinit var mockedSportsGameNetworkDataSource: SportsGameNetworkDataSource

    lateinit var sportsGamesAroundUser: List<SportsGame>

    @Mock
    lateinit var mockContext: Context

    @Mock
    lateinit var mockApiResponseHandler: ApiResponseHandler<
            HomeScreenViewState<ProjectUser>,
            List<SportsGame>
            >

    @Mock
    lateinit var mockConnectivityManager: ConnectivityManager

    @Mock
    lateinit var mockApplicationContextProvider : ApplicationContextProvider

    @Mock
    lateinit var mockConnectivityHelper : ConnectivityHelper

    @Mock
    lateinit var mockNetworkInfo: NetworkInfo

    @Mock
    private lateinit var viewStateObserver: Observer<HomeScreenViewState<ProjectUser>>

    @OptIn(FlowPreview::class)
    @Before
    fun setUp() {
        // Initialize or mock dependencies
        dependencyContainer = DependencyContainer()
        dependencyContainer.build()
        sportsGameFactory = dependencyContainer.sportsGameFactory
        sportsGamesAroundUser = SportsGameFactory.createPreviewEntitiesList()

        mockNetworkInfo = mock(NetworkInfo::class.java)
        Mockito.`when`(mockNetworkInfo.isConnected).thenReturn(true)

        mockConnectivityManager = mock(ConnectivityManager::class.java)
        Mockito.`when`(mockConnectivityManager.activeNetworkInfo).thenReturn(mockNetworkInfo)

        mockContext = mock(Context::class.java)
        Mockito.`when`(mockContext.getSystemService(Context.CONNECTIVITY_SERVICE)).thenReturn(mockConnectivityManager)

        mockApplicationContextProvider = mock(ApplicationContextProvider::class.java)
        `when`(mockApplicationContextProvider.applicationContext(
            tag = "HomeScreenViewModelComposeTest"
        )).thenReturn(mockContext)

        mockConnectivityHelper = mock(ConnectivityHelper::class.java)
        `when`(mockConnectivityHelper.getContext()).thenReturn(mockContext)
        `when`(mockConnectivityHelper.internetConnectionIsAvailable(
            context = any()
        )).thenReturn(true)

        mockApiResponseHandler = mock(
            ApiResponseHandler::class.java
        ) as ApiResponseHandler<HomeScreenViewState<ProjectUser>, List<SportsGame>>

        mockedSportsGameCacheDataSource = mock(SportsGameCacheDataSource::class.java)
        mockedSportsGameNetworkDataSource = mock(SportsGameNetworkDataSource::class.java)
        runBlocking {
            Mockito.doReturn(sportsGamesAroundUser).
            `when`(mockedSportsGameNetworkDataSource).searchEntities(any())
        }

        location = DeviceLocation.PreviewDeviceLocation
        stateEventErrorMessage = "Error while getting items around user."
        internalDirectory = File("internalDirectory")
        stateEventGetSportsGamesAroundUser = HomeScreenStateEvent.GetSportsGamesAroundUserStateEvent(internalDirectory)
        returnViewState = HomeScreenViewState()
        updateReturnViewState = {_, _ -> returnViewState}
        onErrorAction = { println("onErrorAction") }

        // Instantiate interactors
//        getSportsGamesAroundUser = mock(GetSportsGamesAroundUser::class.java)
        getSportsGamesAroundUser = GetSportsGamesAroundUserImpl(
            mockedSportsGameCacheDataSource,
            mockedSportsGameNetworkDataSource,
            sportsGameFactory,
            mockApplicationContextProvider,
            mockConnectivityHelper,
        )
//        searchSportsGames = mock(SearchSportsGames::class.java)
        searchSportsGames = SearchSportsGamesImpl(
            mockedSportsGameCacheDataSource,
            mockedSportsGameNetworkDataSource,
            mockApplicationContextProvider,
            mockConnectivityHelper,
        )
        getSportsTeamPlayers = mock(GetSportsTeamPlayers::class.java)
//        getSportsTeamPlayers = GetSportsTeamPlayersImpl(
//            mockedSportsTeamPlayerCacheDataSource,
//            mockedSportsTeamPlayerNetworkDataSource,
//            sportsTeamPlayerFactory,
//        )
        getSportsTeams = mock(GetSportsTeams::class.java)
//        getSportsTeams = GetSportsTeamsImpl(
//            mockedSportsTeamCacheDataSource,
//            mockedSportsTeamNetworkDataSource,
//            sportsTeamFactory,
//        )
        getUsersRating = mock(GetUsersRating::class.java)
        logout = mock(LogoutUser::class.java)
        checkGooglePayAvailability = mock(CheckGooglePayAvailability::class.java)
        downloadExchangeRates = mock(DownloadExchangeRates::class.java)
        getMerchantName = mock(GetMerchantName::class.java)
        getGatewayNameAndMerchantID = mock(GetGatewayNameAndMerchantID::class.java)
        doNothingAtAll = mock(DoNothingAtAll::class.java) as DoNothingAtAll<
                ProjectUser,
                UserCacheDataSourceImpl,
                UserNetworkDataSource,
                HomeScreenViewState<ProjectUser>
        >

        homeScreenInteractors = HomeScreenInteractors<
                ProjectUser,
                UserCacheDataSourceImpl,
                UserNetworkDataSource,
                HomeScreenViewState<ProjectUser>
                > (
                    getSportsGamesAroundUser,
                    searchSportsGames,
                    getSportsTeamPlayers,
                    getSportsTeams,
                    getUsersRating,
                    logout,
                    checkGooglePayAvailability,
                    downloadExchangeRates,
                    getMerchantName,
                    getGatewayNameAndMerchantID,
                    doNothingAtAll,
                )

        viewStateObserver = Observer<HomeScreenViewState<ProjectUser>> { viewState ->
            println("viewState changed: ${viewState.searchedEntities1List}")
        }

        // Initialize system under test
        viewModelCompose = HomeScreenViewModelCompose(mockApplicationContextProvider)
        viewModelCompose.interactors = homeScreenInteractors
    }

    @After
    fun tearDown() {
        // do something if required
    }

    @OptIn(FlowPreview::class)
    @Test
    fun `test success of getSportsGamesAroundUser`() {
        testCoroutineRule.runBlockingTest {
            viewModelCompose.viewState.observeForever(viewStateObserver)
            assert(viewModelCompose.viewState.value?.searchedEntities1List == null)
            viewModelCompose.setStateEvent(stateEventGetSportsGamesAroundUser, mockContext)

            assert(viewModelCompose.viewState.value?.searchedEntities1List != null)
        }
    }
}