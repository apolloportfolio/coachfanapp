package com.aps.coachfanapp.cfahomescreen

import android.content.Context
import android.location.Location
import android.net.ConnectivityManager
import android.net.NetworkInfo
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.aps.coachfanapp.cfahomescreen.business.data.cache.abstraction.SportsGameCacheDataSource
import com.aps.coachfanapp.cfahomescreen.business.data.network.abs.SportsGameNetworkDataSource
import com.aps.coachfanapp.cfahomescreen.business.domain.model.entities.SportsGame
import com.aps.coachfanapp.cfahomescreen.business.domain.model.factories.SportsGameFactory
import com.aps.coachfanapp.cfahomescreen.business.interactors.HomeScreenInteractors
import com.aps.coachfanapp.cfahomescreen.business.interactors.abs.CheckGooglePayAvailability
import com.aps.coachfanapp.cfahomescreen.business.interactors.abs.DownloadExchangeRates
import com.aps.coachfanapp.cfahomescreen.business.interactors.abs.GetGatewayNameAndMerchantID
import com.aps.coachfanapp.cfahomescreen.business.interactors.abs.GetMerchantName
import com.aps.coachfanapp.cfahomescreen.business.interactors.abs.GetSportsGamesAroundUser
import com.aps.coachfanapp.cfahomescreen.business.interactors.abs.GetSportsTeamPlayers
import com.aps.coachfanapp.cfahomescreen.business.interactors.abs.GetSportsTeams
import com.aps.coachfanapp.cfahomescreen.business.interactors.abs.GetUsersRating
import com.aps.coachfanapp.cfahomescreen.business.interactors.abs.LogoutUser
import com.aps.coachfanapp.cfahomescreen.business.interactors.abs.SearchSportsGames
import com.aps.coachfanapp.cfahomescreen.business.interactors.impl.GetSportsGamesAroundUserImpl
import com.aps.coachfanapp.cfahomescreen.business.interactors.impl.SearchSportsGamesImpl
import com.aps.coachfanapp.cfahomescreen.framework.presentation.activity01.HomeScreenViewModelCompose
import com.aps.coachfanapp.cfahomescreen.framework.presentation.activity01.state.HomeScreenStateEvent
import com.aps.coachfanapp.cfahomescreen.framework.presentation.activity01.state.HomeScreenViewState
import com.aps.coachfanapp.common.business.data.network.ApiResponseHandler
import com.aps.coachfanapp.common.business.domain.state.DataState
import com.aps.coachfanapp.common.business.domain.state.StateEvent
import com.aps.coachfanapp.common.business.interactors.abstraction.DoNothingAtAll
import com.aps.coachfanapp.common.util.ALog
import com.aps.coachfanapp.common.util.DateUtil
import com.aps.coachfanapp.common.util.DeviceLocation
import com.aps.coachfanapp.core.business.data.cache.implementation.UserCacheDataSourceImpl
import com.aps.coachfanapp.core.business.data.network.abstraction.UserNetworkDataSource
import com.aps.coachfanapp.core.business.domain.model.entities.ProjectUser
import com.aps.coachfanapp.core.business.domain.model.factories.UserFactory
import com.aps.coachfanapp.core.di.DependencyContainer
import com.aps.coachfanapp.core.util.ApplicationContextProvider
import com.aps.coachfanapp.core.util.ConnectivityHelper
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.flow.FlowCollector
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.TestCoroutineScope
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runBlockingTest
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.jupiter.api.Assertions
import org.junit.rules.TestRule
import org.junit.runner.Description
import org.junit.runner.RunWith
import org.junit.runners.Suite
import org.junit.runners.model.Statement
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.Mockito.mock
import org.mockito.Mockito.`when`
import org.mockito.kotlin.any
import java.io.File

//==================================================================================================
private const val TAG = "UnitTestTemplateHomeScreen"
private const val LOG_ME = true
@OptIn(ExperimentalCoroutinesApi::class)
@RunWith(Suite::class)
@Suite.SuiteClasses(
    UnitTestTemplateHomeScreenViewModelCompose::class,
    UnitTestTemplateHomeScreenInteractorsSuite::class
)
class UnitTestTemplateHomeScreen {

    @Before
    fun setUpTestSuite() {
        // Perform setup tasks for the entire suite
        if (LOG_ME) ALog.d(TAG, ".setUpTestSuite(): Setting up the test suite...")
    }

    @After
    fun tearDownTestSuite() {
        if (LOG_ME) ALog.d(TAG, ".setUpTestSuite(): Tearing down the test suite...")
    }
}
//==================================================================================================

//==================================================================================================
@ExperimentalCoroutinesApi
class UnitTestTemplateHomeScreenViewModelCompose {

    @get:Rule
    val testInstantTaskExecutorRule: TestRule = InstantTaskExecutorRule()

    @get:Rule
    val testCoroutineRule = UnitTestTemplateTestCoroutineRule()

    // System under test
    @OptIn(FlowPreview::class)
    lateinit var viewModelCompose: HomeScreenViewModelCompose

    lateinit var dependencyContainer: DependencyContainer
    lateinit var homeScreenInteractors: HomeScreenInteractors<
            ProjectUser,
            UserCacheDataSourceImpl,
            UserNetworkDataSource,
            HomeScreenViewState<ProjectUser>
            >
    lateinit var dateUtil: DateUtil
    lateinit var userFactory: UserFactory
    lateinit var sportsGameFactory: SportsGameFactory
    lateinit var location: Location
    lateinit var stateEventGetSportsGamesAroundUser: HomeScreenStateEvent.GetSportsGamesAroundUserStateEvent
    lateinit var returnViewState: HomeScreenViewState<ProjectUser>
    lateinit var updateReturnViewState: (HomeScreenViewState<ProjectUser>, List<SportsGame>?) -> HomeScreenViewState<ProjectUser>
    lateinit var onErrorAction: () -> Unit
    lateinit var internalDirectory: File
    lateinit var stateEventErrorMessage: String

    @Mock
    lateinit var getSportsGamesAroundUser: GetSportsGamesAroundUser

    @Mock
    lateinit var searchSportsGames: SearchSportsGames

    @Mock
    lateinit var getSportsTeamPlayers: GetSportsTeamPlayers

    @Mock
    lateinit var getSportsTeams: GetSportsTeams

    @Mock
    lateinit var getUsersRating: GetUsersRating

    @Mock
    lateinit var logout: LogoutUser

    @Mock
    lateinit var checkGooglePayAvailability: CheckGooglePayAvailability

    @Mock
    lateinit var downloadExchangeRates: DownloadExchangeRates

    @Mock
    private lateinit var getMerchantName: GetMerchantName

    lateinit var getGatewayNameAndMerchantID: GetGatewayNameAndMerchantID

    lateinit var doNothingAtAll: DoNothingAtAll<
            ProjectUser,
            UserCacheDataSourceImpl,
            UserNetworkDataSource,
            HomeScreenViewState<ProjectUser>>



    @Mock
    lateinit var mockedSportsGameCacheDataSource: SportsGameCacheDataSource

    @Mock
    lateinit var mockedSportsGameNetworkDataSource: SportsGameNetworkDataSource

    lateinit var sportsGamesAroundUser: List<SportsGame>

    @Mock
    lateinit var mockContext: Context

    @Mock
    lateinit var mockApiResponseHandler: ApiResponseHandler<
            HomeScreenViewState<ProjectUser>,
            List<SportsGame>
            >

    @Mock
    lateinit var mockConnectivityManager: ConnectivityManager

    @Mock
    lateinit var mockApplicationContextProvider : ApplicationContextProvider

    @Mock
    lateinit var mockConnectivityHelper : ConnectivityHelper

    @Mock
    lateinit var mockNetworkInfo: NetworkInfo

    @Mock
    private lateinit var viewStateObserver: Observer<HomeScreenViewState<ProjectUser>>

    @OptIn(FlowPreview::class)
    @Before
    fun setUp() {
        // Initialize or mock dependencies
        dependencyContainer = DependencyContainer()
        dependencyContainer.build()
        sportsGameFactory = dependencyContainer.sportsGameFactory
        sportsGamesAroundUser = SportsGameFactory.createPreviewEntitiesList()

        mockNetworkInfo = Mockito.mock(NetworkInfo::class.java)
        Mockito.`when`(mockNetworkInfo.isConnected).thenReturn(true)

        mockConnectivityManager = Mockito.mock(ConnectivityManager::class.java)
        Mockito.`when`(mockConnectivityManager.activeNetworkInfo).thenReturn(mockNetworkInfo)

        mockContext = Mockito.mock(Context::class.java)
        Mockito.`when`(mockContext.getSystemService(Context.CONNECTIVITY_SERVICE)).thenReturn(mockConnectivityManager)

        mockApplicationContextProvider = mock(ApplicationContextProvider::class.java)
        `when`(mockApplicationContextProvider.applicationContext(
            tag = TAG
        )).thenReturn(mockContext)

        mockConnectivityHelper = mock(ConnectivityHelper::class.java)
        `when`(mockConnectivityHelper.getContext()).thenReturn(mockContext)
        `when`(mockConnectivityHelper.internetConnectionIsAvailable(
            context = any()
        )).thenReturn(true)

        mockApiResponseHandler = Mockito.mock(
            ApiResponseHandler::class.java
        ) as ApiResponseHandler<HomeScreenViewState<ProjectUser>, List<SportsGame>>

        mockedSportsGameCacheDataSource = Mockito.mock(SportsGameCacheDataSource::class.java)
        mockedSportsGameNetworkDataSource = Mockito.mock(SportsGameNetworkDataSource::class.java)
        runBlocking {
            Mockito.doReturn(sportsGamesAroundUser).
            `when`(mockedSportsGameNetworkDataSource).searchEntities(any())
        }

        location = DeviceLocation.PreviewDeviceLocation
        stateEventErrorMessage = "Error while getting items around user."
        internalDirectory = File("internalDirectory")
        stateEventGetSportsGamesAroundUser = HomeScreenStateEvent.GetSportsGamesAroundUserStateEvent(internalDirectory)
        returnViewState = HomeScreenViewState()
        updateReturnViewState = {_, _ -> returnViewState}
        onErrorAction = { println("onErrorAction") }

        // Instantiate interactors
//        getSportsGamesAroundUser = mock(GetSportsGamesAroundUser::class.java)
        getSportsGamesAroundUser = GetSportsGamesAroundUserImpl(
            mockedSportsGameCacheDataSource,
            mockedSportsGameNetworkDataSource,
            sportsGameFactory,
            mockApplicationContextProvider,
            mockConnectivityHelper,
        )
//        searchSportsGames = mock(SearchSportsGames::class.java)
        searchSportsGames = SearchSportsGamesImpl(
            mockedSportsGameCacheDataSource,
            mockedSportsGameNetworkDataSource,
            mockApplicationContextProvider,
            mockConnectivityHelper,
        )
        getSportsTeamPlayers = Mockito.mock(GetSportsTeamPlayers::class.java)
//        getSportsTeamPlayers = GetSportsTeamPlayersImpl(
//            mockedSportsTeamPlayerCacheDataSource,
//            mockedSportsTeamPlayerNetworkDataSource,
//            sportsTeamPlayerFactory,
//        )
        getSportsTeams = Mockito.mock(GetSportsTeams::class.java)
//        getSportsTeams = GetSportsTeamsImpl(
//            mockedSportsTeamCacheDataSource,
//            mockedSportsTeamNetworkDataSource,
//            sportsTeamFactory,
//        )
        getUsersRating = Mockito.mock(GetUsersRating::class.java)
        logout = Mockito.mock(LogoutUser::class.java)
        checkGooglePayAvailability = Mockito.mock(CheckGooglePayAvailability::class.java)
        downloadExchangeRates = Mockito.mock(DownloadExchangeRates::class.java)
        getMerchantName = Mockito.mock(GetMerchantName::class.java)
        getGatewayNameAndMerchantID = Mockito.mock(GetGatewayNameAndMerchantID::class.java)
        doNothingAtAll = Mockito.mock(DoNothingAtAll::class.java) as DoNothingAtAll<
                ProjectUser,
                UserCacheDataSourceImpl,
                UserNetworkDataSource,
                HomeScreenViewState<ProjectUser>
                >

        homeScreenInteractors = HomeScreenInteractors<
                ProjectUser,
                UserCacheDataSourceImpl,
                UserNetworkDataSource,
                HomeScreenViewState<ProjectUser>
                > (
            getSportsGamesAroundUser,
            searchSportsGames,
            getSportsTeamPlayers,
            getSportsTeams,
            getUsersRating,
            logout,
            checkGooglePayAvailability,
            downloadExchangeRates,
            getMerchantName,
            getGatewayNameAndMerchantID,
            doNothingAtAll,
        )

        viewStateObserver = Observer<HomeScreenViewState<ProjectUser>> { viewState ->
            println("viewState changed: ${viewState.searchedEntities1List}")
        }

        // Initialize system under test
        viewModelCompose = HomeScreenViewModelCompose(mockApplicationContextProvider)
        viewModelCompose.interactors = homeScreenInteractors
    }

    @After
    fun tearDown() {
        // do something if required
    }

    @OptIn(FlowPreview::class)
    @Test
    fun `test success of getSportsGamesAroundUser`() {
        testCoroutineRule.runBlockingTest {
            viewModelCompose.viewState.observeForever(viewStateObserver)
            assert(viewModelCompose.viewState.value?.searchedEntities1List == null)
            viewModelCompose.setStateEvent(stateEventGetSportsGamesAroundUser, mockContext)

            assert(viewModelCompose.viewState.value?.searchedEntities1List != null)
        }
    }
}
//==================================================================================================

//==================================================================================================
@ExperimentalCoroutinesApi
class UnitTestTemplateTestCoroutineRule : TestRule {

    private val testCoroutineDispatcher = TestCoroutineDispatcher()

    private val testCoroutineScope = TestCoroutineScope(testCoroutineDispatcher)

    override fun apply(base: Statement, description: Description?) = object : Statement() {
        @Throws(Throwable::class)
        override fun evaluate() {
            Dispatchers.setMain(testCoroutineDispatcher)

            base.evaluate()

            Dispatchers.resetMain()
            testCoroutineScope.cleanupTestCoroutines()
        }
    }

    fun runBlockingTest(block: suspend TestCoroutineScope.() -> Unit) =
        testCoroutineScope.runBlockingTest { block() }

}
//==================================================================================================

//==================================================================================================
@RunWith(Suite::class)
@Suite.SuiteClasses(
    UnitTestTemplateInteractorTest::class,
//    SearchEntities1UnitTest::class,
//    GetEntities2UnitTest::class,
//    GetEntities3UnitTest::class,
//    GetUsersRatingUnitTest::class,
//    LogoutUserUnitTest::class,
//    CheckGooglePayAvailabilityUnitTest::class,
//    DownloadExchangeRatesUnitTest::class,
//    GetMerchantNameUnitTest::class,
//    GetGatewayNameAndMerchantIDUnitTest::class,
)
class UnitTestTemplateHomeScreenInteractorsSuite {

    @Before
    fun setUpTestSuite() {
        // Perform setup tasks for the entire suite
        if (LOG_ME) ALog.d(TAG, ".setUpTestSuite(): Setting up the test suite...")
    }

    @After
    fun tearDownTestSuite() {
        if (LOG_ME) ALog.d(TAG, ".setUpTestSuite(): Tearing down the test suite...")
    }
}
//==================================================================================================

//==================================================================================================
class UnitTestTemplateInteractorTest {

    // System under test
    lateinit var getSportsGamesAroundUser: GetSportsGamesAroundUser

    // Dependencies
    lateinit var dependencyContainer: DependencyContainer
    private lateinit var sportsGameCacheDataSource: SportsGameCacheDataSource
    lateinit var sportsGameNetworkDataSource: SportsGameNetworkDataSource
    lateinit var entityFactory: SportsGameFactory


    lateinit var location: Location

    lateinit var stateEvent: StateEvent

    lateinit var returnViewState: HomeScreenViewState<ProjectUser>

    lateinit var updateReturnViewState: (HomeScreenViewState<ProjectUser>, List<SportsGame>?) -> HomeScreenViewState<ProjectUser>

    lateinit var onErrorAction: () -> Unit

    lateinit var internalDirectory: File

    lateinit var stateEventErrorMessage: String


    // Mocks for dependencies
    @Mock
    lateinit var mockedSportsGameCacheDataSource: SportsGameCacheDataSource

    @Mock
    lateinit var mockedSportsGameNetworkDataSource: SportsGameNetworkDataSource

    @Mock
    lateinit var mockitoGetSportsGamesAroundUser: GetSportsGamesAroundUser

    lateinit var sportsGamesAroundUser: List<SportsGame>

    @Mock
    lateinit var mockContext: Context

    @Mock
    lateinit var mockApiResponseHandler: ApiResponseHandler<
            HomeScreenViewState<ProjectUser>,
            List<SportsGame>
            >

    @Mock
    lateinit var mockApplicationContextProvider : ApplicationContextProvider

    @Mock
    lateinit var mockConnectivityHelper : ConnectivityHelper

    @Mock
    lateinit var mockConnectivityManager: ConnectivityManager

    @Mock
    lateinit var mockNetworkInfo: NetworkInfo

    init {
        // Initialize dependencies
        dependencyContainer = DependencyContainer()
        dependencyContainer.build()
        entityFactory = dependencyContainer.sportsGameFactory
        sportsGameCacheDataSource = dependencyContainer.sportsGameCacheDataSource
        sportsGameNetworkDataSource = dependencyContainer.sportsGameNetworkDataSource

        sportsGamesAroundUser = SportsGameFactory.createPreviewEntitiesList()

        mockNetworkInfo = Mockito.mock(NetworkInfo::class.java)
        Mockito.`when`(mockNetworkInfo.isConnected).thenReturn(true)

        mockConnectivityManager = Mockito.mock(ConnectivityManager::class.java)
        Mockito.`when`(mockConnectivityManager.activeNetworkInfo).thenReturn(mockNetworkInfo)

        mockContext = Mockito.mock(Context::class.java)
        Mockito.`when`(mockContext.getSystemService(Context.CONNECTIVITY_SERVICE)).thenReturn(mockConnectivityManager)

        mockApplicationContextProvider = mock(ApplicationContextProvider::class.java)
        `when`(mockApplicationContextProvider.applicationContext(
            tag = TAG
        )).thenReturn(mockContext)

        mockConnectivityHelper = mock(ConnectivityHelper::class.java)
        `when`(mockConnectivityHelper.getContext()).thenReturn(mockContext)
        `when`(mockConnectivityHelper.internetConnectionIsAvailable(
            context = any()
        )).thenReturn(true)

        mockApiResponseHandler = Mockito.mock(
            ApiResponseHandler::class.java
        ) as ApiResponseHandler<HomeScreenViewState<ProjectUser>, List<SportsGame>>
//        `when`(mockApiResponseHandler.internetConnectionIsAvailable(mockContext)).thenReturn(true)

        mockedSportsGameCacheDataSource = Mockito.mock(SportsGameCacheDataSource::class.java)
        mockedSportsGameNetworkDataSource = Mockito.mock(SportsGameNetworkDataSource::class.java)
        runBlocking {
            Mockito.doReturn(sportsGamesAroundUser).
            `when`(mockedSportsGameNetworkDataSource).searchEntities(any())
        }

        location = DeviceLocation.PreviewDeviceLocation
        stateEventErrorMessage = "Error while getting items around user."
        internalDirectory = File("internalDirectory")
        stateEvent = HomeScreenStateEvent.GetSportsGamesAroundUserStateEvent(internalDirectory)
        returnViewState = HomeScreenViewState()
        updateReturnViewState = {_, _ -> returnViewState}
        onErrorAction = { println("onErrorAction") }



        // Initialize system under test
        getSportsGamesAroundUser = GetSportsGamesAroundUserImpl(
            sportsGameCacheDataSource,
            sportsGameNetworkDataSource,
            entityFactory,
            mockApplicationContextProvider,
            mockConnectivityHelper,
        )

        mockitoGetSportsGamesAroundUser = GetSportsGamesAroundUserImpl(
            mockedSportsGameCacheDataSource,
            mockedSportsGameNetworkDataSource,
            entityFactory,
            mockApplicationContextProvider,
            mockConnectivityHelper,
        )
    }


    @Before
    fun setup() {

    }


    @After
    fun tearDown() {

    }

    @Test
    fun `mockito test success of getSportsGamesAroundUser`() = runBlocking {

        mockitoGetSportsGamesAroundUser.getSportsGamesAroundUser(
            location,
            stateEvent,
            returnViewState,
            updateReturnViewState,
            onErrorAction,
        ).collect(object: FlowCollector<DataState<HomeScreenViewState<ProjectUser>>?> {
            override suspend fun emit(value: DataState<HomeScreenViewState<ProjectUser>>?) {
                Assertions.assertEquals(
                    GetSportsGamesAroundUserImpl.GET_ENTITIES_SUCCESS,
                    value?.stateMessage?.response?.message
                )
            }
        })

    }
}
//==================================================================================================