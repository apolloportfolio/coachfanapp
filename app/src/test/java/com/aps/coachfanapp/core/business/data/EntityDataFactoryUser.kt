package com.aps.coachfanapp.core.business.data

import com.aps.coachfanapp.core.business.domain.model.entities.ProjectUser
import com.google.common.reflect.TypeToken
import com.google.gson.Gson

class EntityDataFactoryUser(
    override val testClassLoader: ClassLoader,
    override val fileNameWithTestData: String): EntityDataFactory<ProjectUser>(testClassLoader, fileNameWithTestData) {


    override fun produceListOfEntities(): List<ProjectUser> {
        return Gson()
            .fromJson(
                getEntitiesFromFile(fileNameWithTestData),
                object : TypeToken<List<ProjectUser>>() {}.type
            )
    }
}