package com.aps.coachfanapp.core.business.data.cache.implementation


import com.aps.coachfanapp.cfahomescreen.business.data.cache.abstraction.SportsGameCacheDataSource
import com.aps.coachfanapp.cfahomescreen.business.domain.model.entities.SportsGame
import com.aps.coachfanapp.common.util.DateUtil
import com.aps.coachfanapp.common.util.ParcelableGeoPoint
import com.aps.coachfanapp.common.util.UniqueID
import com.aps.coachfanapp.common.util.UserUniqueID
import com.aps.coachfanapp.core.framework.datasources.cachedatasources.room.daos.PAGINATION_PAGE_SIZE
import javax.inject.Inject

class FakeSportsGameCacheDataSourceImpl
@Inject
constructor(
    private val entitiesData: HashMap<UniqueID, SportsGame>,
    private val dateUtil: DateUtil
): SportsGameCacheDataSource {
    override suspend fun insertOrUpdateEntity(entity: SportsGame): SportsGame? {
        this.insertEntity(entity)
        return this.getEntityById(entity.id)
    }

    override suspend fun insertEntity(entity: SportsGame): Long {
        if(entity.id == null){
            throw NullPointerException("entity.id == null when inserting the entity.")
        }
        if(entity.id!!.firestoreDocumentID == null){
            throw NullPointerException("entity.id.firestoreDocumentID == null when inserting the entity.")
        }
        if(entity.id!!.firestoreDocumentID == FORCE_NEW_EXCEPTION){
            throw Exception("Something went wrong inserting the entity.")
        }
        if(entity.id!!.firestoreDocumentID == FORCE_GENERAL_FAILURE){
            return -1 // fail
        }
        entitiesData[entity.id!!] = entity
        return 1 // success
    }

    override suspend fun deleteEntity(primaryKey: UniqueID?): Int {
        if(primaryKey == null){
            throw NullPointerException("entity.id == null when deleting the entity.")
        }
        if(primaryKey!!.firestoreDocumentID == null){
            throw NullPointerException("entity.id.firestoreDocumentID == null when deleting the entity.")
        }
        if(primaryKey.firestoreDocumentID.equals(FORCE_DELETE_EXCEPTION)){
            throw Exception("Something went wrong deleting the entity.")
        }
        else if(primaryKey.firestoreDocumentID.equals(FORCE_DELETES_EXCEPTION)){
            throw Exception("Something went wrong deleting the entity.")
        }
        return entitiesData.remove(primaryKey)?.let {
            1 // return 1 for success
        }?: - 1 // -1 for failure
    }

    override suspend fun deleteEntities(entities: List<SportsGame>): Int {
        var failOrSuccess = 1
        for(entity in entities){
            if(entitiesData.remove(entity.id) == null){
                failOrSuccess = -1 // mark for failure
            }
        }
        return failOrSuccess
    }

    override suspend fun updateEntity(
        id: UniqueID?,
        created_at: String?,
        updated_at: String?,

        lattitude : Double?,
        longitude: Double?,
        geoLocation: ParcelableGeoPoint?,
        firestoreGeoLocation: Double?,

        picture1URI: String?,
        description: String?,

        city: String?,

        ownerID: UserUniqueID?,

        name: String?,

        switch1: Boolean?,
        switch2: Boolean?,
        switch3: Boolean?,
        switch4: Boolean?,
        switch5: Boolean?,
        switch6: Boolean?,
        switch7: Boolean?,

        picture2URI: String?,
        date: String?,
        location: String?,
        tvChannel: String?,
        ticketPrices: String?,
        currentScore: String?,
        gameBreakdown: String?,
        team1Id: UniqueID?,
        team2Id: UniqueID?,
    ): Int {

        if(id == null){
            throw NullPointerException("entity.id == null when inserting the entity.")
        }
        if(id!!.firestoreDocumentID == null){
            throw NullPointerException("entity.id.firestoreDocumentID == null when inserting the entity.")
        }
        if(id!!.firestoreDocumentID == FORCE_NEW_EXCEPTION){
            throw Exception("Something went wrong inserting the entity.")
        }
        if(id!!.firestoreDocumentID == FORCE_GENERAL_FAILURE){
            return -1 // fail
        }
        val updatedEntity = SportsGame(id,
            created_at,
            updated_at,
            lattitude,
            longitude,
            geoLocation,
            firestoreGeoLocation,
            picture1URI,
            description,
            city,
            ownerID,
            name,
            switch1,
            switch2,
            switch3,
            switch4,
            switch5,
            switch6,
            switch7,
            picture2URI,
            date,
            location,
            tvChannel,
            ticketPrices,
            currentScore,
            gameBreakdown,
            team1Id,
            team2Id,
        )
        return entitiesData[id]?.let {
            entitiesData[id] = updatedEntity
            1 // success
        }?: -1 // nothing to update
    }

    override suspend fun searchEntities(
        query: String,
        filterAndOrder: String,
        page: Int
    ): List<SportsGame> {
        if(query == FORCE_SEARCH_EXCEPTION){
            throw Exception("Something went wrong when searching the cache for entities.")
        }
        val results: ArrayList<SportsGame> = ArrayList()
        for(entity in entitiesData.values){
            if(entity.description?.contains(query) == true){
                results.add(entity)
            }
            else if(entity.description?.contains(query) == true){
                results.add(entity)
            }
            if(results.size > (page * PAGINATION_PAGE_SIZE)){
                break
            }
        }
        return results
    }

    override suspend fun getAllEntities(): List<SportsGame> {
        return ArrayList(entitiesData.values)
    }

    override suspend fun getEntityById(id: UniqueID?): SportsGame? {
        return entitiesData.get(id)
    }

    override suspend fun getNumEntities(): Int {
        return entitiesData.size
    }

    override suspend fun insertEntities(entities: List<SportsGame>): LongArray {
        val results = LongArray(entities.size)
        for((index, entity) in entities.withIndex()){
            results[index] = 1
            entity.id?.let { entitiesData.put(it, entity) }
        }
        return results
    }

    companion object {
        const val FORCE_DELETE_EXCEPTION = "FORCE_DELETE_EXCEPTION"
        const val FORCE_DELETES_EXCEPTION = "FORCE_DELETES_EXCEPTION"
        const val FORCE_UPDATE_EXCEPTION = "FORCE_UPDATE_EXCEPTION"
        const val FORCE_NEW_EXCEPTION = "FORCE_NEW_EXCEPTION"
        const val FORCE_SEARCH_EXCEPTION = "FORCE_SEARCHES_EXCEPTION"
        const val FORCE_GENERAL_FAILURE = "FORCE_GENERAL_FAILURE"
    }
}