package com.aps.coachfanapp.core.business.data.network.implementation

import android.net.Uri
import com.aps.coachfanapp.cfahomescreen.business.data.network.abs.SportsGameNetworkDataSource
import com.aps.coachfanapp.cfahomescreen.business.domain.model.entities.SportsGame
import com.aps.coachfanapp.cfahomescreen.business.interactors.impl.FirestoreSportsGameSearchParameters
import com.aps.coachfanapp.common.util.ALog
import com.aps.coachfanapp.common.util.DateUtil
import com.aps.coachfanapp.common.util.UniqueID
import com.aps.coachfanapp.common.util.UserUniqueID
import com.aps.coachfanapp.core.business.data.util.FakeFirestoreIDGenerator

private const val TAG = "FakeSportsGameNetworkDataSourceImpl"
private const val LOG_ME = true

class FakeSportsGameNetworkDataSourceImpl constructor(
    private val entitiesData: HashMap<UniqueID, SportsGame>,
    private val deletedEntitiesData: HashMap<UniqueID, SportsGame>,
    private val fakeFirestoreIDGenerator: FakeFirestoreIDGenerator,
    private val dateUtil: DateUtil
) : SportsGameNetworkDataSource {
    override suspend fun insertOrUpdateEntity(entity: SportsGame): SportsGame? {
        val methodName: String = "insertOrUpdateEntity"
        if (LOG_ME) ALog.d(TAG, "Method start: $methodName")
        try {
            if(entity.id == null)entity.id = fakeFirestoreIDGenerator.getNewFirestoreId()
            else if(entity.id!!.firestoreDocumentID == null)entity.id = fakeFirestoreIDGenerator.getNewFirestoreId()
            val newEntity = SportsGame(
                entity.id,
                entity.created_at,
                dateUtil.getCurrentTimestamp(),

                entity.latitude,
                entity.longitude,
                entity.geoLocation,
                entity.firestoreGeoLocation,

                entity.picture1URI,
                entity.description,

                entity.city,
                entity.ownerID,

                entity.name,

                entity.switch1,
                entity.switch2,
                entity.switch3,
                entity.switch4,
                entity.switch5,
                entity.switch6,
                entity.switch7,
                entity.picture2URI,
                entity.date,
                entity.location,
                entity.tvChannel,
                entity.ticketPrices,
                entity.currentScore,
                entity.gameBreakdown,
                entity.team1Id,
                entity.team2Id,
            )
            this.entitiesData[newEntity.id!!] = newEntity
            return newEntity
        } catch (e: Exception) {
            ALog.e(TAG, methodName, e)
            return null
        } finally {
            if (LOG_ME) ALog.d(TAG, "Method end: $methodName")
        }
    }

    override suspend fun deleteEntity(primaryKey: UniqueID?) {
        val methodName: String = "deleteEntity"
        if (LOG_ME) ALog.d(TAG, "Method start: $methodName")
        try {
            if (primaryKey != null) {
                this.entitiesData.remove(primaryKey)
            } else {
                ALog.w(TAG, "$methodName(): Failed to delete entity.")
            }
        } catch (e: Exception) {
            ALog.e(TAG, methodName, e)
            return
        } finally {
            if (LOG_ME) ALog.d(TAG, "Method end: $methodName")
        }
    }

    override suspend fun insertDeletedEntity(entity: SportsGame) {
        val methodName: String = "insertDeletedEntity"
        if (LOG_ME) ALog.d(TAG, "Method start: $methodName")
        try {
            if(entity.id == null)entity.id = fakeFirestoreIDGenerator.getNewFirestoreId()
            else if(entity.id!!.firestoreDocumentID == null)entity.id = fakeFirestoreIDGenerator.getNewFirestoreId()
            this.deletedEntitiesData[entity.id!!] = entity
        } catch (e: Exception) {
            ALog.e(TAG, methodName, e)
        } finally {
            if (LOG_ME) ALog.d(TAG, "Method end: $methodName")
        }
    }

    override suspend fun insertDeletedEntities(entities: List<SportsGame>) {
        for(entity in entities){
            if(entity.id == null)entity.id = fakeFirestoreIDGenerator.getNewFirestoreId()
            else if(entity.id!!.firestoreDocumentID == null)entity.id = fakeFirestoreIDGenerator.getNewFirestoreId()
            this.deletedEntitiesData[entity.id!!] = entity
        }
    }

    override suspend fun deleteDeletedEntity(entity: SportsGame) {
        entity.id?.let { this.deletedEntitiesData.remove(it) }
    }

    override suspend fun getDeletedEntities(): List<SportsGame> {
        return ArrayList(this.deletedEntitiesData.values)
    }

    override suspend fun deleteAllEntities() {
        this.deletedEntitiesData.clear()
    }

    override suspend fun searchEntity(entity: SportsGame): SportsGame? {
        return this.entitiesData[entity.id]
    }

    override suspend fun getAllEntities(): List<SportsGame> {
        return ArrayList(this.entitiesData.values)
    }

    override suspend fun insertOrUpdateEntities(entities: List<SportsGame>): List<SportsGame>? {
        for(entity in entities) {
            if(entity.id == null)entity.id = fakeFirestoreIDGenerator.getNewFirestoreId()
            else if(entity.id!!.firestoreDocumentID == null)entity.id = fakeFirestoreIDGenerator.getNewFirestoreId()
            this.entitiesData[entity.id!!] = entity
        }
        return entities
    }

    override suspend fun getEntityById(id: UniqueID): SportsGame? {
        return this.entitiesData[id]
    }

    override suspend fun searchEntities(
        searchParameters: FirestoreSportsGameSearchParameters
    ): List<SportsGame>? {
        TODO("Unavailable in demo project.")
    }

    override suspend fun getUsersEntites1(userID: UserUniqueID): List<SportsGame>? {
        val result = ArrayList<SportsGame>()
        for(entity in entitiesData.values) {
            if(entity.ownerID!!.equalByValueTo(userID))result.add(entity)
        }
        return result
    }

    override suspend fun uploadSportsGamePhotoToFirestore(
        entity: SportsGame,
        entitysPhotoUri: Uri,
        entitysPhotoNumber: Int
    ): String? {
        return "Test requires instrumentation."
    }
}