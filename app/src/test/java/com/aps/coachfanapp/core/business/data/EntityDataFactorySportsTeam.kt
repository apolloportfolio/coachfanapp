package com.aps.coachfanapp.core.business.data

import com.aps.coachfanapp.cfahomescreen.business.domain.model.entities.SportsTeam
import com.google.common.reflect.TypeToken
import com.google.gson.Gson

class EntityDataFactorySportsTeam(
    override val testClassLoader: ClassLoader,
    override val fileNameWithTestData: String): EntityDataFactory<SportsTeam>(testClassLoader, fileNameWithTestData) {


    override fun produceListOfEntities(): List<SportsTeam>{
        val entities: List<SportsTeam> = Gson()
            .fromJson(
                getEntitiesFromFile(fileNameWithTestData),
                object: TypeToken<List<SportsTeam>>() {}.type
            )
        return entities
    }
}