package com.aps.coachfanapp.core.business.data.network.implementation

import com.aps.coachfanapp.cfahomescreen.business.data.network.abs.SportsTeamNetworkDataSource
import com.aps.coachfanapp.cfahomescreen.business.domain.model.entities.SportsTeam
import com.aps.coachfanapp.common.util.ALog
import com.aps.coachfanapp.common.util.DateUtil
import com.aps.coachfanapp.common.util.UniqueID
import com.aps.coachfanapp.common.util.UserUniqueID
import com.aps.coachfanapp.core.business.data.util.FakeFirestoreIDGenerator

private const val TAG = "FakeSportsTeamNetworkDataSourceImpl"
private const val LOG_ME = true

class FakeSportsTeamNetworkDataSourceImpl constructor(
    private val data: HashMap<UniqueID, SportsTeam>,
    private val deletedData: HashMap<UniqueID, SportsTeam>,
    private val fakeFirestoreIDGenerator: FakeFirestoreIDGenerator,
    private val dateUtil: DateUtil
) : SportsTeamNetworkDataSource {
    override suspend fun insertOrUpdateEntity(entity: SportsTeam): SportsTeam? {
        val methodName: String = "insertOrUpdateEntity"
        if (LOG_ME) ALog.d(TAG, "Method start: $methodName")
        try {
            if(entity.id == null)entity.id = fakeFirestoreIDGenerator.getNewFirestoreId()
            else if(entity.id!!.firestoreDocumentID == null)entity.id = fakeFirestoreIDGenerator.getNewFirestoreId()
            val newEntity = SportsTeam(
                entity.id,
                entity.created_at,
                dateUtil.getCurrentTimestamp(),

                entity.picture1URI,

                entity.description,

                entity.ownerID,

                entity.name,
            )
            this.data[newEntity.id!!] = newEntity
            return newEntity
        } catch (e: Exception) {
            ALog.e(TAG, methodName, e)
            return null
        } finally {
            if (LOG_ME) ALog.d(TAG, "Method end: $methodName")
        }
    }

    override suspend fun deleteEntity(primaryKey: UniqueID?) {
        val methodName: String = "deleteEntity"
        if (LOG_ME) ALog.d(TAG, "Method start: $methodName")
        try {
            if (primaryKey != null) {
                this.data.remove(primaryKey)
            } else {
                ALog.w(TAG, "$methodName(): Failed to delete entity.")
            }
        } catch (e: Exception) {
            ALog.e(TAG, methodName, e)
            return
        } finally {
            if (LOG_ME) ALog.d(TAG, "Method end: $methodName")
        }
    }

    override suspend fun insertDeletedEntity(entity: SportsTeam) {
        val methodName: String = "insertDeletedEntity"
        if (LOG_ME) ALog.d(TAG, "Method start: $methodName")
        try {
            if(entity.id == null)entity.id = fakeFirestoreIDGenerator.getNewFirestoreId()
            else if(entity.id!!.firestoreDocumentID == null)entity.id = fakeFirestoreIDGenerator.getNewFirestoreId()
            this.deletedData[entity.id!!] = entity
        } catch (e: Exception) {
            ALog.e(TAG, methodName, e)
        } finally {
            if (LOG_ME) ALog.d(TAG, "Method end: $methodName")
        }
    }

    override suspend fun insertDeletedEntities(entities: List<SportsTeam>) {
        for(entity in entities){
            if(entity.id == null)entity.id = fakeFirestoreIDGenerator.getNewFirestoreId()
            else if(entity.id!!.firestoreDocumentID == null)entity.id = fakeFirestoreIDGenerator.getNewFirestoreId()
            this.deletedData[entity.id!!] = entity
        }
    }

    override suspend fun deleteDeletedEntity(entity: SportsTeam) {
        entity.id?.let { this.deletedData.remove(it) }
    }

    override suspend fun getDeletedEntities(): List<SportsTeam> {
        return ArrayList(this.deletedData.values)
    }

    override suspend fun deleteAllEntities() {
        this.deletedData.clear()
    }

    override suspend fun searchEntity(entity: SportsTeam): SportsTeam? {
        return this.data[entity.id]
    }

    override suspend fun getAllEntities(): List<SportsTeam> {
        return ArrayList(this.data.values)
    }

    override suspend fun insertOrUpdateEntities(entities: List<SportsTeam>): List<SportsTeam>? {
        for(entity in entities) {
            if(entity.id == null)entity.id = fakeFirestoreIDGenerator.getNewFirestoreId()
            else if(entity.id!!.firestoreDocumentID == null)entity.id = fakeFirestoreIDGenerator.getNewFirestoreId()
            this.data[entity.id!!] = entity
        }
        return entities
    }

    override suspend fun getEntityById(id: UniqueID): SportsTeam? {
        return this.data[id]
    }

    override suspend fun getUsersEntities3(userId: UserUniqueID): List<SportsTeam>? {
        val result = ArrayList<SportsTeam>()
        for(entity in data.values) {
            if(entity.ownerID!!.equalByValueTo(userId)) result.add(entity)
        }
        return result
    }
}