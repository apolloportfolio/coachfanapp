package com.aps.coachfanapp.core.business.data

import com.aps.coachfanapp.cfahomescreen.business.domain.model.entities.SportsGame
import com.google.common.reflect.TypeToken
import com.google.gson.Gson

class EntityDataFactorySportsGame(
    override val testClassLoader: ClassLoader,
    override val fileNameWithTestData: String): EntityDataFactory<SportsGame>(testClassLoader, fileNameWithTestData) {


    override fun produceListOfEntities(): List<SportsGame> {

        return Gson()
            .fromJson(
                getEntitiesFromFile(fileNameWithTestData),
                object : TypeToken<List<SportsGame>>() {}.type
            )
    }
}