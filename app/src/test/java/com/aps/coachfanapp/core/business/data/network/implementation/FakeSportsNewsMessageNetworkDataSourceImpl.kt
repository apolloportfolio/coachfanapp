package com.aps.coachfanapp.core.business.data.network.implementation

import com.aps.coachfanapp.cfahomescreen.business.data.network.abs.SportsNewsMessageNetworkDataSource
import com.aps.coachfanapp.cfahomescreen.business.domain.model.entities.SportsNewsMessage
import com.aps.coachfanapp.common.util.ALog
import com.aps.coachfanapp.common.util.DateUtil
import com.aps.coachfanapp.common.util.UniqueID
import com.aps.coachfanapp.common.util.UserUniqueID
import com.aps.coachfanapp.core.business.data.util.FakeFirestoreIDGenerator

private const val TAG = "FakeSportsNewsMessageNetworkDataSourceImpl"
private const val LOG_ME = true
class FakeSportsNewsMessageNetworkDataSourceImpl constructor(
    private val data: HashMap<UniqueID, SportsNewsMessage>,
    private val deletedData: HashMap<UniqueID, SportsNewsMessage>,
    private val fakeFirestoreIDGenerator: FakeFirestoreIDGenerator,
    private val dateUtil: DateUtil
) : SportsNewsMessageNetworkDataSource {
    override suspend fun insertOrUpdateEntity(entity: SportsNewsMessage): SportsNewsMessage? {
        val methodName: String = "insertOrUpdateEntity"
        if (LOG_ME) ALog.d(TAG, "Method start: $methodName")
        try {
            if(entity.id == null)entity.id = fakeFirestoreIDGenerator.getNewFirestoreId()
            else if(entity.id!!.firestoreDocumentID == null)entity.id = fakeFirestoreIDGenerator.getNewFirestoreId()
            val newEntity = SportsNewsMessage(
                entity.id,
                entity.created_at,
                dateUtil.getCurrentTimestamp(),

                entity.latitude,
                entity.longitude,
                entity.geoLocation,
                entity.firestoreGeoLocation,


                entity.picture1URI,
                entity.description,

                entity.title,
                entity.message,
                entity.sportType,
                entity.gameId,
                entity.teamId,
                entity.playerIds,
            )
            this.data[newEntity.id!!] = newEntity
            return newEntity
        } catch (e: Exception) {
            ALog.e(TAG, methodName, e)
            return null
        } finally {
            if (LOG_ME) ALog.d(TAG, "Method end: $methodName")
        }
    }

    override suspend fun deleteEntity(primaryKey: UniqueID?) {
        val methodName: String = "deleteEntity"
        if (LOG_ME) ALog.d(TAG, "Method start: $methodName")
        try {
            if (primaryKey != null) {
                this.data.remove(primaryKey)
            } else {
                ALog.w(TAG, "$methodName(): Failed to delete entity.")
            }
        } catch (e: Exception) {
            ALog.e(TAG, methodName, e)
            return
        } finally {
            if (LOG_ME) ALog.d(TAG, "Method end: $methodName")
        }
    }

    override suspend fun insertDeletedEntity(entity: SportsNewsMessage) {
        val methodName: String = "insertDeletedEntity"
        if (LOG_ME) ALog.d(TAG, "Method start: $methodName")
        try {
            if(entity.id == null)entity.id = fakeFirestoreIDGenerator.getNewFirestoreId()
            else if(entity.id!!.firestoreDocumentID == null)entity.id = fakeFirestoreIDGenerator.getNewFirestoreId()
            this.deletedData[entity.id!!] = entity
        } catch (e: Exception) {
            ALog.e(TAG, methodName, e)
        } finally {
            if (LOG_ME) ALog.d(TAG, "Method end: $methodName")
        }
    }

    override suspend fun insertDeletedEntities(entities: List<SportsNewsMessage>) {
        for(entity in entities){
            if(entity.id == null)entity.id = fakeFirestoreIDGenerator.getNewFirestoreId()
            else if(entity.id!!.firestoreDocumentID == null)entity.id = fakeFirestoreIDGenerator.getNewFirestoreId()
            this.deletedData[entity.id!!] = entity
        }
    }

    override suspend fun deleteDeletedEntity(entity: SportsNewsMessage) {
        entity.id?.let { this.deletedData.remove(it) }
    }

    override suspend fun getDeletedEntities(): List<SportsNewsMessage> {
        return ArrayList(this.deletedData.values)
    }

    override suspend fun deleteAllEntities() {
        this.deletedData.clear()
    }

    override suspend fun searchEntity(entity: SportsNewsMessage): SportsNewsMessage? {
        return this.data[entity.id]
    }

    override suspend fun getAllEntities(): List<SportsNewsMessage> {
        return ArrayList(this.data.values)
    }

    override suspend fun insertOrUpdateEntities(entities: List<SportsNewsMessage>): List<SportsNewsMessage>? {
        for(entity in entities) {
            if(entity.id == null)entity.id = fakeFirestoreIDGenerator.getNewFirestoreId()
            else if(entity.id!!.firestoreDocumentID == null)entity.id = fakeFirestoreIDGenerator.getNewFirestoreId()
            this.data[entity.id!!] = entity
        }
        return entities
    }

    override suspend fun getEntityById(id: UniqueID): SportsNewsMessage? {
        return this.data[id]
    }
}