package com.aps.coachfanapp.core.di


import com.aps.coachfanapp.cfahomescreen.business.data.cache.abstraction.SportsGameCacheDataSource
import com.aps.coachfanapp.cfahomescreen.business.data.cache.abstraction.SportsNewsMessageCacheDataSource
import com.aps.coachfanapp.cfahomescreen.business.data.cache.abstraction.SportsTeamCacheDataSource
import com.aps.coachfanapp.cfahomescreen.business.data.cache.abstraction.SportsTeamPlayerCacheDataSource
import com.aps.coachfanapp.cfahomescreen.business.data.network.abs.SportsGameNetworkDataSource
import com.aps.coachfanapp.cfahomescreen.business.data.network.abs.SportsNewsMessageNetworkDataSource
import com.aps.coachfanapp.cfahomescreen.business.data.network.abs.SportsTeamNetworkDataSource
import com.aps.coachfanapp.cfahomescreen.business.data.network.abs.SportsTeamPlayerNetworkDataSource
import com.aps.coachfanapp.cfahomescreen.business.domain.model.factories.SportsGameFactory
import com.aps.coachfanapp.cfahomescreen.business.domain.model.factories.SportsNewsMessageFactory
import com.aps.coachfanapp.cfahomescreen.business.domain.model.factories.SportsTeamFactory
import com.aps.coachfanapp.cfahomescreen.business.domain.model.factories.SportsTeamPlayerFactory
import com.aps.coachfanapp.common.util.DateUtil
import com.aps.coachfanapp.common.util.UniqueID
import com.aps.coachfanapp.core.business.data.EntityDataFactoryUser
import com.aps.coachfanapp.core.business.data.cache.abstraction.UserCacheDataSource
import com.aps.coachfanapp.core.business.data.cache.implementation.FakeSportsGameCacheDataSourceImpl
import com.aps.coachfanapp.core.business.data.cache.implementation.FakeSportsNewsMessageCacheDataSourceImpl
import com.aps.coachfanapp.core.business.data.cache.implementation.FakeSportsTeamCacheDataSourceImpl
import com.aps.coachfanapp.core.business.data.cache.implementation.FakeSportsTeamPlayerCacheDataSourceImpl
import com.aps.coachfanapp.core.business.data.cache.implementation.FakeUserCacheDataSourceImpl
import com.aps.coachfanapp.core.business.data.cache.implementation.UserCacheDataSourceImpl
import com.aps.coachfanapp.core.business.data.network.abstraction.UserNetworkDataSource
import com.aps.coachfanapp.core.business.data.network.implementation.FakeSportsGameNetworkDataSourceImpl
import com.aps.coachfanapp.core.business.data.network.implementation.FakeSportsNewsMessageNetworkDataSourceImpl
import com.aps.coachfanapp.core.business.data.network.implementation.FakeSportsTeamNetworkDataSourceImpl
import com.aps.coachfanapp.core.business.data.network.implementation.FakeSportsTeamPlayerNetworkDataSourceImpl
import com.aps.coachfanapp.core.business.data.network.implementation.FakeUserNetworkDataSourceImpl
import com.aps.coachfanapp.core.business.data.util.FakeFirestoreIDGenerator
import com.aps.coachfanapp.core.business.domain.model.entities.ProjectUser
import com.aps.coachfanapp.core.business.domain.model.factories.UserFactory
import com.aps.coachfanapp.feature01.business.interactors.registerlogin.RegisterLoginInteractors
import com.aps.coachfanapp.feature01.framework.presentation.activity01.state.RegisterLoginActivityViewState
import java.text.SimpleDateFormat
import java.util.Locale

class DependencyContainer {
    private val dateFormatForFirestore = SimpleDateFormat("yyyy-MM-dd hh:mm:ss a", Locale.ENGLISH)
    private val dateFormatForUser = SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH)
    private val dateFormatJustYear = SimpleDateFormat("yyyy", Locale.ENGLISH)
    val dateUtil = DateUtil(dateFormatForFirestore, dateFormatForUser, dateFormatJustYear)
    lateinit var sportsGameFactory: SportsGameFactory
//    lateinit var sportsGameDataFactory: EntityDataFactorySportsGame
    lateinit var sportsGameCacheDataSource: SportsGameCacheDataSource
    lateinit var sportsGameNetworkDataSource: SportsGameNetworkDataSource

    lateinit var sportsTeamPlayerFactory: SportsTeamPlayerFactory
//    lateinit var sportsTeamPlayerDataFactory: EntityDataFactorySportsTeamPlayer
    lateinit var sportsTeamPlayerCacheDataSource: SportsTeamPlayerCacheDataSource
    lateinit var sportsTeamPlayerNetworkDataSource: SportsTeamPlayerNetworkDataSource

    lateinit var sportsTeamFactory: SportsTeamFactory
//    lateinit var sportsTeamDataFactory: EntityDataFactorySportsTeam
    lateinit var sportsTeamCacheDataSource: SportsTeamCacheDataSource
    lateinit var sportsTeamNetworkDataSource: SportsTeamNetworkDataSource

    lateinit var sportsNewsMessageFactory: SportsNewsMessageFactory
//    lateinit var sportsNewsMessageDataFactory: EntityDataFactorySportsNewsMessage
    lateinit var sportsNewsMessageCacheDataSource: SportsNewsMessageCacheDataSource
    lateinit var sportsNewsMessageNetworkDataSource: SportsNewsMessageNetworkDataSource

    lateinit var userFactory: UserFactory
    lateinit var userDataFactory: EntityDataFactoryUser
    lateinit var userCacheDataSource: UserCacheDataSource
    lateinit var userNetworkDataSource: UserNetworkDataSource

    lateinit var interactorsStart: RegisterLoginInteractors<
            ProjectUser,
            UserCacheDataSourceImpl,
            UserNetworkDataSource,
            RegisterLoginActivityViewState<ProjectUser>
            >

    
    init {
    }

    fun build() {
        // Using entities created for previews
        sportsGameFactory = SportsGameFactory(dateUtil)
        sportsGameCacheDataSource = FakeSportsGameCacheDataSourceImpl(
            entitiesData = produceHashMapOfEntities(
                SportsGameFactory.createPreviewEntitiesList(),
                { it.id!! }
            ),
            dateUtil = dateUtil
        )
        sportsGameNetworkDataSource = FakeSportsGameNetworkDataSourceImpl(
            entitiesData = produceHashMapOfEntities(
                SportsGameFactory.createPreviewEntitiesList(),
                { it.id!! }
            ),
            deletedEntitiesData = HashMap(),
            FakeFirestoreIDGenerator(),
            dateUtil = dateUtil
        )

        sportsTeamPlayerFactory = SportsTeamPlayerFactory(dateUtil)
        sportsTeamPlayerCacheDataSource = FakeSportsTeamPlayerCacheDataSourceImpl(
            entitiesData = produceHashMapOfEntities(
                SportsTeamPlayerFactory.createPreviewEntitiesList(),
                { it.id!! }
            ),
            dateUtil = dateUtil
        )
        sportsTeamPlayerNetworkDataSource = FakeSportsTeamPlayerNetworkDataSourceImpl(
            data = produceHashMapOfEntities(
                SportsTeamPlayerFactory.createPreviewEntitiesList(),
                { it.id!! }
            ),
            deletedData = HashMap(),
            FakeFirestoreIDGenerator(),
            dateUtil = dateUtil
        )

        sportsTeamFactory = SportsTeamFactory(dateUtil)
        sportsTeamCacheDataSource = FakeSportsTeamCacheDataSourceImpl(
            entitiesData = produceHashMapOfEntities(
                SportsTeamFactory.createPreviewEntitiesList(),
                { it.id!! }
            ),
            dateUtil = dateUtil
        )
        sportsTeamNetworkDataSource = FakeSportsTeamNetworkDataSourceImpl(
            data = produceHashMapOfEntities(
                SportsTeamFactory.createPreviewEntitiesList(),
                { it.id!! }
            ),
            deletedData = HashMap(),
            FakeFirestoreIDGenerator(),
            dateUtil = dateUtil,
        )

        sportsNewsMessageFactory = SportsNewsMessageFactory(dateUtil)
        sportsNewsMessageCacheDataSource = FakeSportsNewsMessageCacheDataSourceImpl(
            entitiesData = produceHashMapOfEntities(
                SportsNewsMessageFactory.createPreviewEntitiesList(),
                { it.id!! }
            ),
            dateUtil = dateUtil,
        )
        sportsNewsMessageNetworkDataSource = FakeSportsNewsMessageNetworkDataSourceImpl(
            data = produceHashMapOfEntities(
                SportsNewsMessageFactory.createPreviewEntitiesList(),
                { it.id!! }
            ),
            deletedData = HashMap(),
            FakeFirestoreIDGenerator(),
            dateUtil = dateUtil,
        )



        // Using entities loaded from json files
//        this.javaClass.classLoader?.let { classLoader ->
//            sportsGameDataFactory = EntityDataFactorySportsGame(classLoader, "sportsGame_test_list.json")
//        }
//        sportsGameCacheDataSource = FakeSportsGameCacheDataSourceImpl(
//            entitiesData = sportsGameDataFactory.produceHashMapOfEntities(
//                sportsGameDataFactory.produceListOfEntities(),
//                { it.id!! }
//            ),
//            dateUtil = dateUtil
//        )
//        sportsGameNetworkDataSource = FakeSportsGameNetworkDataSourceImpl(
//            entitiesData = sportsGameDataFactory.produceHashMapOfEntities(
//                sportsGameDataFactory.produceListOfEntities(),
//                { it.id!! }
//            ),
//            deletedEntitiesData = HashMap(),
//            FakeFirestoreIDGenerator(),
//            dateUtil = dateUtil
//        )
//
//        sportsTeamPlayerFactory = SportsTeamPlayerFactory(dateUtil)
//        this.javaClass.classLoader?.let { classLoader ->
//            sportsTeamPlayerDataFactory = EntityDataFactorySportsTeamPlayer(classLoader, "sportsTeamPlayer_test_list.json")
//        }
//        sportsTeamPlayerCacheDataSource = FakeSportsTeamPlayerCacheDataSourceImpl(
//            entitiesData = sportsTeamPlayerDataFactory.produceHashMapOfEntities(
//                sportsTeamPlayerDataFactory.produceListOfEntities(),
//                { it.id!! }
//            ),
//            dateUtil = dateUtil
//        )
//        sportsTeamPlayerNetworkDataSource = FakeSportsTeamPlayerNetworkDataSourceImpl(
//            data = sportsTeamPlayerDataFactory.produceHashMapOfEntities(
//                sportsTeamPlayerDataFactory.produceListOfEntities(),
//                { it.id!! }
//            ),
//            deletedData = HashMap(),
//            FakeFirestoreIDGenerator(),
//            dateUtil = dateUtil
//        )
//
//        sportsTeamFactory = SportsTeamFactory(dateUtil)
//        this.javaClass.classLoader?.let { classLoader ->
//            sportsTeamDataFactory = EntityDataFactorySportsTeam(classLoader, "sportsTeam_test_list.json")
//        }
//        sportsTeamCacheDataSource = FakeSportsTeamCacheDataSourceImpl(
//            entitiesData = sportsTeamDataFactory.produceHashMapOfEntities(
//                sportsTeamDataFactory.produceListOfEntities(),
//                { it.id!! }
//            ),
//            dateUtil = dateUtil
//        )
//        sportsTeamNetworkDataSource = FakeSportsTeamNetworkDataSourceImpl(
//            data = sportsTeamDataFactory.produceHashMapOfEntities(
//                sportsTeamDataFactory.produceListOfEntities(),
//                { it.id!! }
//            ),
//            deletedData = HashMap(),
//            FakeFirestoreIDGenerator(),
//            dateUtil = dateUtil,
//        )
//
//        sportsNewsMessageFactory = SportsNewsMessageFactory(dateUtil = dateUtil)
//        this.javaClass.classLoader?.let { classLoader ->
//            sportsNewsMessageDataFactory = EntityDataFactorySportsNewsMessage(classLoader, "sportsTeam_test_list.json")
//        }
//        sportsNewsMessageCacheDataSource = FakeSportsNewsMessageCacheDataSourceImpl(
//            entitiesData = sportsNewsMessageDataFactory.produceHashMapOfEntities(
//                sportsNewsMessageDataFactory.produceListOfEntities(),
//                { it.id!! }
//            ),
//            dateUtil = dateUtil,
//        )
//        sportsNewsMessageNetworkDataSource = FakeSportsNewsMessageNetworkDataSourceImpl(
//            data = sportsNewsMessageDataFactory.produceHashMapOfEntities(
//                sportsNewsMessageDataFactory.produceListOfEntities(),
//                { it.id!! }
//            ),
//            deletedData = HashMap(),
//            FakeFirestoreIDGenerator(),
//            dateUtil = dateUtil,
//        )

        userFactory = UserFactory(dateUtil)
        this.javaClass.classLoader?.let { classLoader ->
            userDataFactory = EntityDataFactoryUser(classLoader, "user_test_list.json")
        }
        userCacheDataSource = FakeUserCacheDataSourceImpl(
            entitiesData = userDataFactory.produceHashMapOfUsers(
                userDataFactory.produceListOfEntities(),
                { it.id!!.firestoreDocumentID }
            ),
            dateUtil = dateUtil
        )
        val hashMapOfUsers = userDataFactory.produceHashMapOfUsers(
            userDataFactory.produceListOfEntities(),
            { it.id!!.firestoreDocumentID }
        )
        userNetworkDataSource = FakeUserNetworkDataSourceImpl(
            data = hashMapOfUsers,
            deletedData = HashMap(),
            ratings = userDataFactory.produceHashMapOfUsersRatings(
                hashMapOfUsers,
                { it.id!! },
            ),
            fakeFirestoreIDGenerator = FakeFirestoreIDGenerator(),
            dateUtil = dateUtil
        )
    }

    private fun <Entity> produceHashMapOfEntities(entityList: List<Entity>, getId: (Entity)-> UniqueID): HashMap<UniqueID, Entity>{
        val map = HashMap<UniqueID, Entity>()
        for(entity in entityList){
            map[getId(entity)] = entity
        }
        return map
    }
}