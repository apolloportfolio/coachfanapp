package com.aps.coachfanapp.core.business.data.util

import com.aps.coachfanapp.common.util.UniqueID
import com.aps.coachfanapp.common.util.UserUniqueID


class FakeFirestoreIDGenerator {
    fun getNewFirestoreId() = UniqueID((lastFirestoreId++).toString())
    fun getNewUserFirestoreId(): UserUniqueID? = UserUniqueID((lastFirestoreId++).toString(), (lastInDocumentId++).toString())

    companion object {
        @JvmStatic private var lastFirestoreId = 1
        @JvmStatic private var lastInDocumentId = 1
    }
}