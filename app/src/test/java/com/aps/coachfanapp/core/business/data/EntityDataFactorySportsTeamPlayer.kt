package com.aps.coachfanapp.core.business.data

import com.aps.coachfanapp.cfahomescreen.business.domain.model.entities.SportsTeamPlayer
import com.google.common.reflect.TypeToken
import com.google.gson.Gson

class EntityDataFactorySportsTeamPlayer(
    override val testClassLoader: ClassLoader,
    override val fileNameWithTestData: String): EntityDataFactory<SportsTeamPlayer>(testClassLoader, fileNameWithTestData) {


    override fun produceListOfEntities(): List<SportsTeamPlayer>{
        val entities: List<SportsTeamPlayer> = Gson()
            .fromJson(
                getEntitiesFromFile(fileNameWithTestData),
                object: TypeToken<List<SportsTeamPlayer>>() {}.type
            )
        return entities
    }
}