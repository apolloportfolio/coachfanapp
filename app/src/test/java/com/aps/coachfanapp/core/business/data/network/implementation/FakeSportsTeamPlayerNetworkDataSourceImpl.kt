package com.aps.coachfanapp.core.business.data.network.implementation

import com.aps.coachfanapp.cfahomescreen.business.data.network.abs.SportsTeamPlayerNetworkDataSource
import com.aps.coachfanapp.cfahomescreen.business.domain.model.entities.SportsTeamPlayer
import com.aps.coachfanapp.common.util.ALog
import com.aps.coachfanapp.common.util.DateUtil
import com.aps.coachfanapp.common.util.UniqueID
import com.aps.coachfanapp.common.util.UserUniqueID
import com.aps.coachfanapp.core.business.data.util.FakeFirestoreIDGenerator

private const val TAG = "FakeSportsTeamPlayerNetworkDataSourceImpl"
private const val LOG_ME = true

class FakeSportsTeamPlayerNetworkDataSourceImpl constructor(
    private val data: HashMap<UniqueID, SportsTeamPlayer>,
    private val deletedData: HashMap<UniqueID, SportsTeamPlayer>,
    private val fakeFirestoreIDGenerator: FakeFirestoreIDGenerator,
    private val dateUtil: DateUtil
) : SportsTeamPlayerNetworkDataSource {
    override suspend fun insertOrUpdateEntity(entity: SportsTeamPlayer): SportsTeamPlayer? {
        val methodName: String = "insertOrUpdateEntity"
        if (LOG_ME) ALog.d(TAG, "Method start: $methodName")
        try {
            if(entity.id == null)entity.id = fakeFirestoreIDGenerator.getNewFirestoreId()
            else if(entity.id!!.firestoreDocumentID == null)entity.id = fakeFirestoreIDGenerator.getNewFirestoreId()
            val newEntity = SportsTeamPlayer(
                entity.id,
                entity.created_at,
                dateUtil.getCurrentTimestamp(),

                entity.picture1URI,

                entity.description,

                entity.sportsTeamId,

                entity.playerName,
            )
            this.data[newEntity.id!!] = newEntity
            return newEntity
        } catch (e: Exception) {
            ALog.e(TAG, methodName, e)
            return null
        } finally {
            if (LOG_ME) ALog.d(TAG, "Method end: $methodName")
        }
    }

    override suspend fun deleteEntity(primaryKey: UniqueID?) {
        val methodName: String = "deleteEntity"
        if (LOG_ME) ALog.d(TAG, "Method start: $methodName")
        try {
            if (primaryKey != null) {
                this.data.remove(primaryKey)
            } else {
                ALog.w(TAG, "$methodName(): Failed to delete entity.")
            }
        } catch (e: Exception) {
            ALog.e(TAG, methodName, e)
            return
        } finally {
            if (LOG_ME) ALog.d(TAG, "Method end: $methodName")
        }
    }

    override suspend fun insertDeletedEntity(entity: SportsTeamPlayer) {
        val methodName: String = "insertDeletedEntity"
        if (LOG_ME) ALog.d(TAG, "Method start: $methodName")
        try {
            if(entity.id == null)entity.id = fakeFirestoreIDGenerator.getNewFirestoreId()
            else if(entity.id!!.firestoreDocumentID == null)entity.id = fakeFirestoreIDGenerator.getNewFirestoreId()
            this.deletedData[entity.id!!] = entity
        } catch (e: Exception) {
            ALog.e(TAG, methodName, e)
        } finally {
            if (LOG_ME) ALog.d(TAG, "Method end: $methodName")
        }
    }

    override suspend fun insertDeletedEntities(entities: List<SportsTeamPlayer>) {
        for(entity in entities){
            if(entity.id == null)entity.id = fakeFirestoreIDGenerator.getNewFirestoreId()
            else if(entity.id!!.firestoreDocumentID == null)entity.id = fakeFirestoreIDGenerator.getNewFirestoreId()
            this.deletedData[entity.id!!] = entity
        }
    }

    override suspend fun deleteDeletedEntity(entity: SportsTeamPlayer) {
        entity.id?.let { this.deletedData.remove(it) }
    }

    override suspend fun getDeletedEntities(): List<SportsTeamPlayer> {
        return ArrayList(this.deletedData.values)
    }

    override suspend fun deleteAllEntities() {
        this.deletedData.clear()
    }

    override suspend fun searchEntity(entity: SportsTeamPlayer): SportsTeamPlayer? {
        return this.data[entity.id]
    }

    override suspend fun getAllEntities(): List<SportsTeamPlayer> {
        return ArrayList(this.data.values)
    }

    override suspend fun insertOrUpdateEntities(entities: List<SportsTeamPlayer>): List<SportsTeamPlayer>? {
        for(entity in entities) {
            if(entity.id == null)entity.id = fakeFirestoreIDGenerator.getNewFirestoreId()
            else if(entity.id!!.firestoreDocumentID == null)entity.id = fakeFirestoreIDGenerator.getNewFirestoreId()
            this.data[entity.id!!] = entity
        }
        return entities
    }

    override suspend fun getEntityById(id: UniqueID): SportsTeamPlayer? {
        return this.data[id]
    }

    override suspend fun getUsersEntities2(userId: UserUniqueID): List<SportsTeamPlayer>? {
        val result = ArrayList<SportsTeamPlayer>()
        for(entity in data.values) {
            if(entity.sportsTeamId!!.equalByValueTo(userId)) result.add(entity)
        }
        return result
    }
}