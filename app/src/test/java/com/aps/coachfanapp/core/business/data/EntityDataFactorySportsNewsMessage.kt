package com.aps.coachfanapp.core.business.data

import com.aps.coachfanapp.cfahomescreen.business.domain.model.entities.SportsNewsMessage
import com.google.common.reflect.TypeToken
import com.google.gson.Gson

class EntityDataFactorySportsNewsMessage(
    override val testClassLoader: ClassLoader,
    override val fileNameWithTestData: String): EntityDataFactory<SportsNewsMessage>(testClassLoader, fileNameWithTestData) {


    override fun produceListOfEntities(): List<SportsNewsMessage>{
        val entities: List<SportsNewsMessage> = Gson()
            .fromJson(
                getEntitiesFromFile(fileNameWithTestData),
                object: TypeToken<List<SportsNewsMessage>>() {}.type
            )
        return entities
    }
}