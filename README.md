<div class="center-title">![Readme Screenshot](./screenshots/Readme.png)</div>

<div class="center-title">

# Coach Fan App

</div>

<div class="container">

<div class="topleft-column">![Project Screenshot 1](./screenshots/image1.png)</div>

<div class="topcenter-column">

# Project Description

Introducing new Android application designed specifically for sports fans who want to cheer on their favorite teams from the comfort of their own homes. With this app, you'll be able to keep up with live scores and updates from the game, and engage with other fans in real-time through social chat feature. But that's not all - application's unique audio feature allows you to synchronize your phone with the game, so you can hear the roar of the crowd and the chants of the supporters as if you were right there in the stadium. Whether you're unable to attend the event in person or simply want to enhance your viewing experience, this app is the perfect solution for all sports enthusiasts.

</div>

<div class="topright-column">

# My Tech Stack:

*   Kotlin, Java
*   AndroidX
*   Material Design
*   Jetpack Compose
*   Coroutines & Flow
*   Android Architecture Components
*   Dagger Hilt
*   Gradle
*   GitLab
*   JUnit
*   Jupiter
*   Mockito
*   MockK
*   Espresso
*   Web Service Integration (REST, JSON, XML, GSON)
*   Firebase
*   Firestore
*   Room DB
*   REST
*   RESTful APIs
*   Retrofit2
*   GraphQL (Apollo)
*   Glide
*   Glide for Jetpack Compose
*   Coil for Jetpack Compose
*   Google Pay
*   Google Location
*   MVI
*   MVVM
*   MVC
*   Service Locator
*   Clean Architecture
*   Dependency Injection
*   CI/CD
*   Clean Code Principles
*   Agile Software Development Practices
*   Test Driven Development

</div>

</div>

<div class="container">

<div class="left-column">

# Target Customer Persona

The target user of this sports fan Android application is likely someone who is passionate about sports and wants to stay up-to-date with their favorite teams and games. They may be unable to attend the event in person due to various reasons such as work or distance, but they still want to cheer on their team and feel like they're part of the action. They're tech-savvy and own a smartphone or tablet, and they're comfortable using social media and messaging apps to connect with other fans. They value convenience and want a seamless experience when using the app, whether they're checking scores, chatting with other fans, or using the audio feature to enhance their viewing experience. Overall, the average user of this app is a dedicated sports fan who wants to stay connected and engaged with their favorite teams, regardless of where they are.

</div>

<div class="right-column">![Project Screenshot 2](./screenshots/image2.png)</div>

</div>

<div class="container">

<div class="left-column">![Project Screenshot 3](./screenshots/image3.png)</div>

<div class="right-column">

# Problem Statement

1.  Distance from the game: One of the biggest challenges for sports fans is the inability to attend games in person due to distance or other reasons. This application allows users to stay connected to their favorite teams and games from anywhere, giving them a sense of involvement and allowing them to cheer their team on from afar.
2.  Limited access to game updates: Without access to live game updates, fans may feel disconnected from the action and miss out on crucial moments. Presented app provides real-time scores and updates, ensuring that users are always in the know and can stay engaged with the game.
3.  Lack of social interaction: Sports fans thrive on social interaction and camaraderie, which can be difficult to replicate when watching a game alone. That is why this innovative application includes a social chat feature, allowing fans to connect with other fans and share their thoughts and opinions on the game.
4.  Inability to hear crowd noise: For many fans, the sound of the crowd is a crucial aspect of the game-day experience. This app's audio feature allows users to sync their phone with the game and hear the roar of the crowd and chants of the supporters, enhancing their viewing experience and making them feel like they're right there in the stadium.

Overall, the Android application before you solves the challenges that sports fans face when trying to stay connected to their favorite teams and games from afar, providing a seamless and engaging experience that enhances the overall viewing experience.

</div>

</div>

<div class="container">

<div class="right-column">

# Functionalities

1.  Live scores and updates: application provides real-time scores and updates for various sports, allowing users to stay up-to-date with their favorite teams and games.
2.  Audio feature: application includes a unique audio feature that allows users to sync their phone with the game and hear the sounds of the crowd and supporters, enhancing their viewing experience.
3.  Social chat feature: application's social chat feature allows users to connect with other fans, share their thoughts and opinions on the game, and engage in lively discussions.
4.  Personalization options: application allows users to personalize their experience, including setting up custom notifications for specific games, choosing their favorite teams, and accessing customized news and updates.
5.  In-depth game analysis: application provides in-depth game analysis, including player stats, team stats, and play-by-play breakdowns, allowing users to get a deeper understanding of the game and its nuances.
6.  User-friendly interface: application is designed with a user-friendly interface, making it easy for users to navigate and access all of its features and functionalities.

</div>

<div class="left-column">![Project Screenshot 4](./screenshots/image4.png)</div>

</div>

<div class="container">

<div class="right-column">

# Application's Screens

1.  Home screen: This is the main screen of the app and displays live scores, news, and updates for the user's favorite teams and sports.
2.  Game details screen: This screen provides in-depth information about a specific game, including player stats, team stats, and a play-by-play breakdown of the game.
3.  Audio screen: This screen allows users to sync their phone with the game and listen to the sounds of the crowd and supporters, enhancing their viewing experience.
4.  Chat screen: This screen allows users to connect with other fans and engage in real-time discussions about the game.
5.  Personalization screen: This screen allows users to personalize their experience by setting up custom notifications for specific games, choosing their favorite teams, and accessing customized news and updates.
6.  Settings screen: This screen allows users to configure app settings, such as language preferences and notification settings.
7.  Login/Registration screen: This screen allows users to create an account or log in to their existing account.
8.  Profile screen: This screen displays the user's profile information, such as their name, profile picture, and favorite teams.
9.  Search screen: This screen allows users to search for specific teams, games, or players.
10.  News screen: This screen displays the latest news and updates for various sports and teams.

</div>

</div>

<div class="container">

<div class="right-column">

# Disclaimer

In order to provide their clients with protection of trade secrets, protection of user data, legal protection and competitive advantage all commissions of projects of this Developer are by default accompanied by an appropriate Non Disclosure Agreement (NDA). For the same reason all presented: project description, problem statement, functionalities and screen descriptions are for presentation purposes only and may or may not represent an entirety of project.

Due to NDA and obvious copyright and security issues, this GitHub project does not contain entire code of described application.

All included code snippets are for presentation purposes only and should not be used for commercial purposes as they are intellectual property of this application's Developer. In compliance with an appropriate NDA, all presented screenshots of working application have been made after removing any and all graphics that may be considered third party's intellectual property. As such they do not represent a final end product.

The developer does not claim any ownership or affiliation with any of the third-party libraries, technologies, or services used in the project.

While every effort has been made to ensure the accuracy, completeness, and reliability of the presented code, the developer cannot be held responsible for any errors, omissions, or damages that may arise from it's use.

In compliance with an appropriate NDA and in order to provide their clients with all presented: description, target customer persona, problem statement, functionalities, screen descriptions and other items are for presentation purposes only and do not consist an entirety of commisioned project.

Protection of trade secrets, protection of user data, legal protection and protection of competitive advantage of a Client is of utmost importance to the Developer.

</div>

</div>

<div class="container">

<div class="right-column">

# License

Any use of this code is prohibited except of for recruitment purposes only of the Developer who wrote it.

</div>

</div>